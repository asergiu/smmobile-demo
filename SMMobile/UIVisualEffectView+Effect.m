//
//  UIVisualEffectView+Effect.m
//  SMMobile
//
//  Created by Sergej Bogatenko on 2/28/18.
//  Copyright © 2018 ACEP. All rights reserved.
//

#import "UIVisualEffectView+Effect.h"
#import "CustomBlurEffect.h"

@implementation UIVisualEffectView (Effect)

- (void)setupBlurEffectWithCornerRadius:(float)cornerRadius
{
    CustomBlurEffect *blurEffect = [CustomBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    
    self.layer.cornerRadius = cornerRadius;
    self.clipsToBounds = YES;
    self.effect = blurEffect;
    self.alpha = 0.85;
    self.backgroundColor = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha:0.2f];
}

@end
