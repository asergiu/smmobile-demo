//
//  OldEngine.cpp
//  Thickness
//
//  Created by Dgut on 15.08.14.
//  Copyright (c) 2014 Dgut. All rights reserved.
//

#include "SMOldEngine.h"

#include "SMContour.h"
#include "SMGlasses.h"
#include "SMLens.h"

#include "PDMResults.h"
extern CPDMResults Results;

using namespace std;

SMOldEngine::SMOldEngine( void ) :
    glasses ( 0 )
{
    dataSource = [ [ DataSource alloc ] init ];
    
    ILens * pLens;
    // create left Lens
    pLens = Create_ILens();
    leftLens.AttachDispatch( pLens );
    pLens = NULL;
    // create right Lens
    pLens = Create_ILens();
    rightLens.AttachDispatch( pLens );
    pLens = NULL;
    
    // antireflex coating
    IARCoating * pARCoating;
    if(!(pARCoating = Create_IARCoating()))
        throw 0;
    pARCoating->SetR( 0 );
    pARCoating->SetG( 0 );
    pARCoating->SetB( 0 );
    // set lens properties
    leftLens->SetARCoating(pARCoating);
    rightLens->SetARCoating(pARCoating);
    // free data
    pARCoating->Release();
    pARCoating = NULL;
    
    // empty tint
    ITint * pTint;
    if(!(pTint = Create_ITint()))
        throw 0;
    // set lens properties
    leftLens->SetTint(pTint);
    rightLens->SetTint(pTint);
    // free data
    pTint->Release();
    pTint = NULL;
}

SMOldEngine::~SMOldEngine( void )
{
    dataSource = nil;
}

/*void SMOldEngine::Init( void )
{
    leftSSLens.Init();
    rightSSLens.Init();
}*/

void SMOldEngine::SetGlasses( SMGlassesBase * glasses, BOOL dynamic )
{
    SMOldEngine::glasses = glasses;
    
    glasses->GetLeftLensLeftOptions()->Init();
    glasses->GetLeftLensRightOptions()->Init();
    glasses->GetRightLensLeftOptions()->Init();
    glasses->GetRightLensRightOptions()->Init();
    
    double pdClient = 32.;
    if(dynamic) {
        int iPhoto = 0; // Far measure
        CPDMDataResults farResults = Results.GetPhotoResults(iPhoto);
        if(farResults.bDataResults) {
            // PD data
            pdClient = (farResults.LeftPD + farResults.RightPD) / 2.;
            if(pdClient > 40.) {
                pdClient = 40.;
            }
            else if(pdClient < 25.) {
                pdClient = 25.;
            }
        }
    }
    
    SetContour( glasses->GetContour(), glasses->GetScale(), pdClient );
}

void SMOldEngine::SetPower( double power )
{
    SMOldEngine::power = power;
}

void SMOldEngine::SetCyl( double cyl )
{
    SMOldEngine::cyl = cyl;
}

void SMOldEngine::SetAxis( long axis )
{
    SMOldEngine::axis = axis;
}

void SMOldEngine::SetLeftDesign( NSString * design )
{
    leftDesign = design;
}

void SMOldEngine::SetRightDesign( NSString * design )
{
    rightDesign = design;
}

void SMOldEngine::SetContour( SMContour * contour, float scale, double PD )
{
    const GLKVector3 * points = &contour->GetPoints().front();
    int count = contour->GetPoints().size();
    double * xp = new double[ count ];
    double * yp = new double[ count ];
    
    step = contour->GetStep();
    
    for( int i = 0; i < count; i++ )
    {
        xp[ i ] = points[ i ].x * scale * 1000;
        yp[ i ] = points[ i ].z * scale * 1000;
    }
    
    SetContour( xp, yp, count, PD );
    
    delete[] xp;
    delete[] yp;
}

void SMOldEngine::SetContour( const double * xFrame4, const double * yFrame4, int Count, double PD )
{
    // contour
    IContour * Contour = Create_IContour();
    if (Contour) {
        const long LensDiameter = 65;
        double xCenterRight = PD;
        double xCenterLeft = PD;
        double yCenter = 0.;
        int iPoint;
        //int Count = 0;
        int iLeftPoint = 0;
        double ScaleFactor = 1;//1.12;
        int ContourScale = 2;
        const double * xFrame, * yFrame = NULL;
        //Count = sizeof(xFrame4) / sizeof(double);
        xFrame = xFrame4;
        yFrame = yFrame4;
        
        for(iPoint = 0; iPoint < Count; iPoint++){
            if(xFrame[iPoint] < xFrame[iLeftPoint])
                iLeftPoint = iPoint;
        }
        for(iPoint = 0; iPoint < Count; iPoint++){
            double x = 0., y = yFrame[iPoint];
            if(ContourScale == 1)
                x = xFrame[iLeftPoint] + (xFrame[iPoint] - xFrame[iLeftPoint]) * ScaleFactor;
            else if(ContourScale == 2) {
                x = xFrame[iLeftPoint] + (xFrame[iPoint] - xFrame[iLeftPoint]) / ScaleFactor;
                y /= ScaleFactor;
            }
            else
                x = xFrame[iPoint];
            Contour->Add(x, y)->Release();
        }
        {
            int i;
            double minPoint = yFrame[0];
            double maxPoint = yFrame[0];
            
            /*double minX = xFrame[ 0 ];
            double maxX = xFrame[ 0 ];*/
            
            for(i = 1; i < Count; i++) {
                if(yFrame[i] < minPoint)
                    minPoint = yFrame[i];
                else if(yFrame[i] > maxPoint)
                    maxPoint = yFrame[i];
                
                /*if( xFrame[ i ] < minX )
                    minX = xFrame[ i ];
                if( xFrame[ i ] > maxX )
                    maxX = xFrame[ i ];*/
                
                //NSLog( @"%f %f", xFrame[ i ], yFrame[ i ] );
            }
            yCenter = (maxPoint + minPoint) / 2.;
            
            //NSLog( @"%f %f %f", minX, maxX, maxX - minX );
        }
        
        if(Contour->GetCount() > 3) {
            double minDiam = 0.;
            if(rightLens) {
                rightLens->SetCenterX(xCenterRight);
                rightLens->SetCenterY(yCenter);
                rightLens->SetDiameter(LensDiameter);
                rightLens->SetContour(Contour);
                minDiam = rightLens->GetMinDiam();
                if(minDiam > LensDiameter)
                    rightLens->SetDiameter(ceil(minDiam));
            }
            if(leftLens) {
                leftLens->SetCenterX(xCenterLeft);
                leftLens->SetCenterY(yCenter);
                leftLens->SetDiameter(LensDiameter);
                leftLens->SetContour(Contour);
                minDiam = leftLens->GetMinDiam();
                if(minDiam > LensDiameter)
                    leftLens->SetDiameter(ceil(minDiam));
            }
        }
        
        Contour->Release();
        Contour = NULL;
    }
}

double getIndex( NSString * design )
{
    if( ![ design compare : ThicknessLensDesignSpherical15 ] || ![ design compare : ThicknessLensDesignAspheric15 ]  || ![ design compare : ThicknessLensDesignDAspheric15 ] )
        return 1.5;
    if( ![ design compare : ThicknessLensDesignSpherical153 ] || ![ design compare : ThicknessLensDesignAspheric153 ]  || ![ design compare : ThicknessLensDesignDAspheric153 ] )
        return 1.53;
    if( ![ design compare : ThicknessLensDesignSpherical156 ] || ![ design compare : ThicknessLensDesignAspheric156 ]  || ![ design compare : ThicknessLensDesignDAspheric156 ] )
        return 1.56;
    if( ![ design compare : ThicknessLensDesignSpherical159 ] || ![ design compare : ThicknessLensDesignAspheric159 ]  || ![ design compare : ThicknessLensDesignDAspheric159 ] )
        return 1.59;
    if( ![ design compare : ThicknessLensDesignSpherical16 ] || ![ design compare : ThicknessLensDesignAspheric16 ]  || ![ design compare : ThicknessLensDesignDAspheric16 ] )
        return 1.6;
    if( ![ design compare : ThicknessLensDesignSpherical167 ] || ![ design compare : ThicknessLensDesignAspheric167 ]  || ![ design compare : ThicknessLensDesignDAspheric167 ] )
        return 1.67;
    if( ![ design compare : ThicknessLensDesignSpherical174 ] || ![ design compare : ThicknessLensDesignAspheric174 ]  || ![ design compare : ThicknessLensDesignDAspheric174 ] )
        return 1.74;
    
    return -1.f;
}

long getAspheric( NSString * design )
{
    // Spherical
    if( ![ design compare : ThicknessLensDesignSpherical15 ] )
        return 0;
    if( ![ design compare : ThicknessLensDesignSpherical153 ] )
        return 0;
    if( ![ design compare : ThicknessLensDesignSpherical156 ] )
        return 0;
    if( ![ design compare : ThicknessLensDesignSpherical159 ] )
        return 0;
    if( ![ design compare : ThicknessLensDesignSpherical16 ] )
        return 0;
    if( ![ design compare : ThicknessLensDesignSpherical167 ] )
        return 0;
    if( ![ design compare : ThicknessLensDesignSpherical174 ] )
        return 0;
    // Aspheric
    if( ![ design compare : ThicknessLensDesignAspheric15 ] )
        return 1;
    if( ![ design compare : ThicknessLensDesignAspheric153 ] )
        return 1;
    if( ![ design compare : ThicknessLensDesignAspheric156 ] )
        return 1;
    if( ![ design compare : ThicknessLensDesignAspheric159 ] )
        return 1;
    if( ![ design compare : ThicknessLensDesignAspheric16 ] )
        return 1;
    if( ![ design compare : ThicknessLensDesignAspheric167 ] )
        return 1;
    if( ![ design compare : ThicknessLensDesignAspheric174 ] )
        return 1;
    // Double Aspheric
    if( ![ design compare : ThicknessLensDesignDAspheric15 ] )
        return 2;
    if( ![ design compare : ThicknessLensDesignDAspheric153 ] )
        return 2;
    if( ![ design compare : ThicknessLensDesignDAspheric156 ] )
        return 2;
    if( ![ design compare : ThicknessLensDesignDAspheric159 ] )
        return 2;
    if( ![ design compare : ThicknessLensDesignDAspheric16 ] )
        return 2;
    if( ![ design compare : ThicknessLensDesignDAspheric167 ] )
        return 2;
    if( ![ design compare : ThicknessLensDesignDAspheric174 ] )
        return 2;
    
    return 0;
}

void SMOldEngine::Refresh( BOOL dynamic )
{
    [ dataSource updateDataForDrawWithPower : power andCyl : cyl andLeftLensType : leftDesign andRightLensType : rightDesign andPrecalibratedLeft:true andPrecalibratedRight:true andPrecalibratedDiam:65 ];
    
    SetLeft(
            power,
            cyl,
            axis,
            getAspheric( leftDesign ),
            getIndex( leftDesign ),
            [ dataSource getSurfaceDataForDraw : 0 forLeftSide : YES ],
            [ dataSource getSurfaceDataForDraw : 1 forLeftSide : YES ],
            [ dataSource getSurfaceDataForDraw : 2 forLeftSide : YES ], dynamic );
    
    SetRight(
            power,
            cyl,
            axis,
            getAspheric( rightDesign ),
            getIndex( rightDesign ),
            [ dataSource getSurfaceDataForDraw : 0 forLeftSide : NO ],
            [ dataSource getSurfaceDataForDraw : 1 forLeftSide : NO ],
            [ dataSource getSurfaceDataForDraw : 2 forLeftSide : NO ], dynamic );
    
    SurfaceCalculate();
}

void SMOldEngine::SurfaceCalculate( void )
{    
    SurfaceCalculateLens( leftLens.m_p, *glasses->GetLeftLensLeftOptions() );
    SurfaceCalculateLens( rightLens.m_p, *glasses->GetLeftLensRightOptions() );
    SurfaceCalculateLens( leftLens.m_p, *glasses->GetRightLensLeftOptions() );
    SurfaceCalculateLens( rightLens.m_p, *glasses->GetRightLensRightOptions() );
    
    SurfaceCalculateLens2( leftLens.m_p, *glasses->GetLeftLensLeftOptions() );
    SurfaceCalculateLens2( rightLens.m_p, *glasses->GetLeftLensRightOptions() );
    SurfaceCalculateLens2( leftLens.m_p, *glasses->GetRightLensLeftOptions() );
    SurfaceCalculateLens2( rightLens.m_p, *glasses->GetRightLensRightOptions() );
    
    //SurfaceCalculateLens2( leftLens.m_p, *glasses->GetLeftLens() );
    //SurfaceCalculateLens2( rightLens.m_p, *glasses->GetRightLens() );
}

void SMOldEngine::SurfaceCalculateLens( ILens * lens, SMLens & ssLens )
{
    long i;
    double x, y;
    double xMin = 0., xMax = 0.;
    double yMin = 0., yMax = 0.;
    vector<LensSurfPoint> DArr, * pDArr;
    
    // calc left lens surfaces
    CObjPtr<IContour> ContourObj;
    ContourObj.AttachDispatch(lens->GetContour());
    // find contour bounds points
    xMin = 0.; xMax = 0.;
    yMin = 0.; yMax = 0.;
    for(i = 0; i < ContourObj->GetCount(); i++) {
        ContourObj->GetPoint(i, &x, &y);
        if(x < xMin)
            xMin = x;
        if(x > xMax)
            xMax = x;
        if(y < yMin)
            yMin = y;
        if(y > yMax)
            yMax = y;
    }
    // surface points
    
    
    for( int i = 0; i < ContourObj->GetCount(); i++ )
    {
        ContourObj->GetPoint( i, &x, &y );
        
        LensSurfPoint lsp;
        
        lsp.X = x;
        lsp.Y = y;
        lsp.zFront = 0;
        lsp.zRear = 0;
        
        DArr.push_back( lsp );
    }
    
    // calc surface
	CObjPtr<ILensEngine> pEngine;
    if(lens->GetEngineUsageMask() & 2)
        pEngine.AttachDispatch(lens->GetEngine2());
    else
        pEngine.AttachDispatch(lens->GetEngine1());
    if (pEngine.m_p) {
        CObjPtr<ILensSurface> Grid;
        try {
            ILensSurface * pGrid = Create_ILensSurface();
            if (pGrid) {
                Grid.AttachDispatch(pGrid);
            }
            Grid->Clear();
            
            if(!Grid->AddArray(DArr))
                throw 1;
            
            if(!pEngine->CalcSurface(lens, Grid)){
                throw 2;
            }
            DArr.clear();
            pDArr = Grid->GetArray();
            
            int size = pDArr->size();
            float * x = new float[ size ];
            float * y = new float[ size ];
            float * front = new float[ size ];
            float * rear = new float[ size ];
            
            double zFront = 0., zRear = 0.;
            vector<LensSurfPoint>::iterator it;
            int i = 0;
            for (it = pDArr->begin(); it != pDArr->end(); ++it, i++) {
                /*zFront = it->zFront;
                zRear = it->zRear;*/
                
                //NSLog( @"%f\t%f\t%f\t%f\t%i", it->X, it->Y, it->zFront, it->zRear, it->zFront < it->zRear );
                x[ i ] = it->X / 1000.f;
                y[ i ] = it->Y / 1000.f;
                front[ i ] = it->zFront / 1000.f;
                rear[ i ] = it->zRear / 1000.f;
            }
            
            ssLens.SetData( x, y, front, rear, size );
            
            delete[] x;
            delete[] y;
            delete[] front;
            delete[] rear;
        }
        catch(...)
        {
            NSLog( @"why throw?" );
        }
    }
}

void SmartAdd( vector< LensSurfPoint > & arr, LensSurfPoint & lsp )
{
    static const float SKIP_DISTANCE = 1.0f;
    
    for( int i = 0; i < arr.size(); i++ )
        if( GLKVector2Distance( GLKVector2Make( arr[ i ].X, arr[ i ].Y ), GLKVector2Make( lsp.X, lsp.Y ) ) < SKIP_DISTANCE )
        {
            NSLog( @"skip add" );
            return;
        }
    
    arr.push_back( lsp );
}

void SMOldEngine::SurfaceCalculateLens2( ILens * lens, SMLens & ssLens )
{
    double xmin, xmax;
    double ymin, ymax;
    
    CObjPtr< IContour > ContourObj;
    ContourObj.AttachDispatch( lens->GetContour() );
    
    ContourObj->GetPoint( 0, &xmin, &ymin );
    xmax = xmin;
    ymax = ymin;
    
    vector< LensSurfPoint > DArr, * pDArr;
    LensSurfPoint lsp = { 0 };
    
    for( int i = 0; i < ContourObj->GetCount(); i++ )
    {
        double x, y;
        
        ContourObj->GetPoint( i, &x, &y );
        
        if( xmin > x )
            xmin = x;
        if( xmax < x )
            xmax = x;
        if( ymin > y )
            ymin = y;
        if( ymax < y )
            ymax = y;
        
        lsp.X = x;
        lsp.Y = y;
        
        DArr.push_back( lsp );
    }
    
    //double step = 1000. / 88.;
    
    for( double y = ceil( ymin / step ) * step; y <= ymax; y += step )
    {
        vector< FTPOINT > result;
        
        if( !ContourObj->Intersection( ContourObj->GetCenterX(), y, 0., result ) )
            continue;
        
        if( result.size() == 0 )
            continue;
        
        if( result.size() == 1 )
        {
            lsp.X = result[ 0 ].x;
            lsp.Y = result[ 0 ].y;
            DArr.push_back( lsp );
            
            continue;
        }
        
        double curxmin = result[ 1 ].x;
        double curxmax = result[ 0 ].x;
        
        if( curxmin > curxmax )
            swap( curxmin, curxmax );
        
        lsp.Y = y;
        
        lsp.X = curxmin;
        DArr.push_back( lsp );
        
        for( double x = ceil( curxmin / step ) * step; x < curxmax; x += step )
        {
            lsp.X = x;
            DArr.push_back( lsp );
        }
        
        lsp.X = curxmax;
        DArr.push_back( lsp );
    }
    
    for( double x = ceil( xmin / step ) * step; x <= xmax; x += step )
    {
        vector< FTPOINT > result;
        
        if( !ContourObj->Intersection( x, ContourObj->GetCenterY(), 90., result ) )
            continue;
        
        for( int i = 0; i < result.size(); i++ )
        {
            lsp.X = result[ i ].x;
            lsp.Y = result[ i ].y;
            DArr.push_back( lsp );
        }
    }
    
    CObjPtr< ILensEngine > pEngine;
    if( lens->GetEngineUsageMask() & 2 )
        pEngine.AttachDispatch( lens->GetEngine2() );
    else
        pEngine.AttachDispatch( lens->GetEngine1() );
    
    CObjPtr< ILensSurface > Grid;
    ILensSurface * pGrid = Create_ILensSurface();
    Grid.AttachDispatch( pGrid );
    Grid->Clear();
            
    Grid->AddArray( DArr );
    
    pEngine->CalcSurface( lens, Grid );
    
    DArr.clear();
    pDArr = Grid->GetArray();
            
    int size = pDArr->size();
    float * x = new float[ size ];
    float * y = new float[ size ];
    float * front = new float[ size ];
    float * rear = new float[ size ];
    GLKVector3 * normalFront = new GLKVector3[ size ];
    GLKVector3 * normalRear = new GLKVector3[ size ];
    
    vector< LensSurfPoint >::iterator it;
    int i = 0;
    for( it = pDArr->begin(); it != pDArr->end(); ++it, i++ )
    {
        x[ i ] = it->X;
        y[ i ] = it->Y;
        front[ i ] = it->zFront;
        rear[ i ] = it->zRear;
        normalFront[ i ].x = it->xnFront;
        normalFront[ i ].y = it->ynFront;
        normalFront[ i ].z = it->znFront;
        normalRear[ i ].x = it->xnRear;
        normalRear[ i ].y = it->ynRear;
        normalRear[ i ].z = it->znRear;
        
        normalFront[ i ] = GLKVector3Normalize( normalFront[ i ] );
        normalRear[ i ] = GLKVector3Normalize( normalRear[ i ] );
    }
    
    ssLens.SetSurfaceData( x, y, front, rear, normalFront, normalRear, size, step/* / 1000.f*/ );
    
    delete[] x;
    delete[] y;
    delete[] front;
    delete[] rear;
    delete[] normalFront;
    delete[] normalRear;
}

void SMOldEngine::SetLeft( double power, double cyl, long axis, long aspheric, double index, SurfaceData R1, SurfaceData R2, SurfaceData R3, BOOL dynamic )
{
    double Sph = power;
    BOOL bAspheric = aspheric;
    double matIndex = index;
    double matDensity = 1.32;
    double CenterThickness = 2.0;
    double EdgeThickness = 1.0;
    double ZeroThickness = 2.2;
    
    ILens * left = leftLens.m_p;
    left->SetSph( Sph );
    left->SetCyl( cyl );
    left->SetAxis( axis );
    left->SetRefIndex( matIndex );
    if( bAspheric )
        left->SetDesign( wsAspheric.c_str() );
    else
        left->SetDesign( wsClassic.c_str() );
    left->SetPermittedCenterThickness(CenterThickness);
    left->SetPermittedEdgeThickness(EdgeThickness);
    left->SetPermittedZeroThickness(ZeroThickness);
    
    double Diam = left->GetDiameter();
    //NSLog(@"Left Diam = %f\n", Diam);
    if( Diam < 65 ) {
        Diam = 65;
    }
    
    // Engine
    SurfaceData dR1 = R1;
    SurfaceData dR2 = R2;
    SurfaceData dR3 = R3;
    // forward surface
    dR1.power = power;
    dR1.cyl = cyl;
    dR1.axis = axis;
    dR1.bAspheric = aspheric;
    // back surface
    dR2.power = power;
    dR2.cyl = cyl;
    dR2.axis = axis;
    dR2.bAspheric = aspheric;
    // second back surface
    dR3.power = power;
    dR3.cyl = cyl;
    dR3.axis = axis;
    dR3.bAspheric = aspheric;
    // Set engine
    if(dynamic && Diam != 65) {
        leftEngine.SetMaxDiam(80);
        leftEngine.CalibrateSurfaceData(&dR1, &dR2, &dR3, Diam);
    }
    else {
        leftEngine.SetMaxDiam(70);
        leftEngine.SetSurfaceData(&dR1, &dR2, &dR3);
    }
    left->SetEngine2(&leftEngine);
    // Set engine usage mask
    left->SetEngineUsageMask(wsWeightEngine2 | wsSurfaceEngine2 | wsThicknessEngine2 | wsUVProtectionEngine2);
}

void SMOldEngine::SetRight( double power, double cyl, long axis, long aspheric, double index, SurfaceData R1, SurfaceData R2, SurfaceData R3, BOOL dynamic )
{
    double Sph = power;
    BOOL bAspheric = aspheric;
    double matIndex = index;
    double matDensity = 1.32;
    double CenterThickness = 2.0;
    double EdgeThickness = 1.0;
    double ZeroThickness = 2.2;
    
    ILens * right = rightLens.m_p;
    right->SetSph( Sph );
    right->SetCyl( cyl );
    right->SetAxis( axis );
    right->SetRefIndex( matIndex );
    if( bAspheric )
        right->SetDesign( wsAspheric.c_str() );
    else
        right->SetDesign( wsClassic.c_str() );
    right->SetPermittedCenterThickness(CenterThickness);
    right->SetPermittedEdgeThickness(EdgeThickness);
    right->SetPermittedZeroThickness(ZeroThickness);
    
    double Diam = right->GetDiameter();
    //NSLog(@"Right Diam = %f\n", Diam);
    if( Diam < 65 ) {
        Diam = 65;
    }
    
    // Engine
    SurfaceData dR1 = R1;
    SurfaceData dR2 = R2;
    SurfaceData dR3 = R3;
    // forward surface
    dR1.power = power;
    dR1.cyl = cyl;
    dR1.axis = axis;
    dR1.bAspheric = aspheric;
    // back surface
    dR2.power = power;
    dR2.cyl = cyl;
    dR2.axis = axis;
    dR2.bAspheric = aspheric;
    // second back surface
    dR3.power = power;
    dR3.cyl = cyl;
    dR3.axis = axis;
    dR3.bAspheric = aspheric;
    // Set engine
    if(dynamic && Diam != 65) {
        rightEngine.SetMaxDiam(80);
        rightEngine.CalibrateSurfaceData(&dR1, &dR2, &dR3, Diam);
    }
    else {
        rightEngine.SetMaxDiam(70);
        rightEngine.SetSurfaceData(&dR1, &dR2, &dR3);
    }
    right->SetEngine2(&rightEngine);
    // Set engine usage mask
    right->SetEngineUsageMask(wsWeightEngine2 | wsSurfaceEngine2 | wsThicknessEngine2 | wsUVProtectionEngine2);
}
