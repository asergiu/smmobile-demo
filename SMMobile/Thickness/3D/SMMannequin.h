//
//  SMMannequin.h
//  SMMobile
//
//  Created by Dgut on 17.12.14.
//

#ifndef __SMMobile__SMMannequin__
#define __SMMobile__SMMannequin__

#include <GLKit/GLKit.h>

class SMLens;
class SMContour;
class SMModel;

class SMMannequin
{
    SMModel *                           model;
    
    float                               scale;
    
    GLKVector3                          shift;
public:
                                        SMMannequin( SMModel * model );
                                        ~SMMannequin( void );
    
    SMModel * const                     GetModel( void ) const;
    
    void                                SetScale( float scale );
    const float                         GetScale( void ) const;
    
    void                                SetShift( const GLKVector3 & shift );
    
    void                                Draw( float side, float offset );
};

#endif /* defined(__SMMobile__SMMannequin__) */
