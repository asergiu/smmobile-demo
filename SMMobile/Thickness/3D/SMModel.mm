//
//  SSModel.cpp
//  SalesStar
//
//  Created by Dgut on 01.02.13.
//  Copyright (c) 2013 Dgut. All rights reserved.
//

#include "SMModel.h"

using namespace std;

SMMesh::SMMesh( void ) :
    buffer      ( 0 ),
    numVertices ( 0 ),
    mins        ( GLKVector3Make( 0.f, 0.f, 0.f ) ),
    maxs        ( GLKVector3Make( 0.f, 0.f, 0.f ) ),
    indices     ( 0 ),
    numIndices  ( 0 )
{
     glGenBuffers( 1, &buffer );
}

SMMesh::~SMMesh( void )
{
    glDeleteBuffers( 1, &buffer );
    
    delete[] indices;
}

const bool SMMesh::Load( Lib3dsMesh * lib3ds_mesh )
{    
    float ( * normals )[ 3 ] = ( float ( * )[ 3 ] )malloc( 3 * 3 * sizeof( float ) * lib3ds_mesh->nfaces );
    lib3ds_mesh_calculate_vertex_normals( lib3ds_mesh, normals );
    
    Vertex * vertices = new Vertex[ lib3ds_mesh->nfaces * 3 ];
    
    //mins = maxs = GLKVector3Make( lib3ds_mesh->vertices[ 0 ][ 0 ], lib3ds_mesh->vertices[ 0 ][ 1 ], lib3ds_mesh->vertices[ 0 ][ 2 ] );
    
    for( int i = 0; i < lib3ds_mesh->nfaces; i++ )
        for( int j = 0; j < 3; j++ )
        {
            unsigned short index = lib3ds_mesh->faces[ i ].index[ j ];
            
            GLKVector3 v = GLKVector3Make( lib3ds_mesh->vertices[ index ][ 0 ], lib3ds_mesh->vertices[ index ][ 1 ], lib3ds_mesh->vertices[ index ][ 2 ] );
            
            /*mins = GLKVector3Minimum( mins, v );
            maxs = GLKVector3Maximum( maxs, v );*/
            
            vertices[ numVertices++ ] = {
                GLKVector3Make( v.x, v.z, -v.y ),
                GLKVector3Make( normals[ i * 3 + j ][ 0 ], normals[ i * 3 + j ][ 2 ], -normals[ i * 3 + j ][ 1 ] ),
                lib3ds_mesh->texcos ? GLKVector2Make( lib3ds_mesh->texcos[ index ][ 0 ], lib3ds_mesh->texcos[ index ][ 1 ] ) : GLKVector2Make( 0.f, 0.f ),
            };
        }
    
    free( normals );
    
    /*for( int i = 0; i < numVertices; i += 3 )
    {
        for( int j = 0; j < 3; j++ )
        {
            GLKVector3 v1 = GLKVector3Subtract( vertices[ i + ( j + 1 ) % 3 ].position, vertices[ i + j ].position );
            GLKVector3 v2 = GLKVector3Subtract( vertices[ i + ( j + 2 ) % 3 ].position, vertices[ i + j ].position );
            
            GLKVector2 st1 = GLKVector2Subtract( vertices[ i + ( j + 1 ) % 3 ].texCoord0, vertices[ i + j ].texCoord0 );
            GLKVector2 st2 = GLKVector2Subtract( vertices[ i + ( j + 2 ) % 3 ].texCoord0, vertices[ i + j ].texCoord0 );
            
            float coef = 1.f / ( st1.x * st2.y - st2.x * st1.y );
            
            vertices[ i + j ].tangent.x = coef * ( ( v1.x * st2.y )  + ( v2.x * -st1.y ) );
            vertices[ i + j ].tangent.y = coef * ( ( v1.y * st2.y )  + ( v2.y * -st1.y ) );
            vertices[ i + j ].tangent.z = coef * ( ( v1.z * st2.y )  + ( v2.z * -st1.y ) );
        }
    }*/
    
    /*glBindBuffer( GL_ARRAY_BUFFER, buffer );
    glBufferData( GL_ARRAY_BUFFER, sizeof( Vertex ) * numVertices, vertices, GL_STATIC_DRAW );*/
    SetVertices( vertices, numVertices );
    
    delete[] vertices;
    
    return true;
}

void SMMesh::SetVertices( const Vertex * vertices, int numVertices )
{
    SMMesh::numVertices = numVertices;
    
    glBindBuffer( GL_ARRAY_BUFFER, buffer );
    glBufferData( GL_ARRAY_BUFFER, sizeof( Vertex ) * numVertices, vertices, GL_STATIC_DRAW );
    
    mins = maxs = vertices[ 0 ].position;
    
    for( int i = 0; i < numVertices; i++ )
    {
        mins = GLKVector3Minimum( mins, vertices[ i ].position );
        maxs = GLKVector3Maximum( maxs, vertices[ i ].position );
    }
}

void SMMesh::SetIndices( const short * indices, int numIndices )
{
    delete[] SMMesh::indices;
    
    SMMesh::indices = new short[ numIndices ];
    memcpy( SMMesh::indices, indices, sizeof( short ) * numIndices );
    SMMesh::numIndices = numIndices;
}

void SMMesh::Draw( void ) const
{
    glBindBuffer( GL_ARRAY_BUFFER, buffer );
    
    glVertexAttribPointer( GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( GLvoid * )0 );
    glVertexAttribPointer( GLKVertexAttribNormal, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( GLvoid * )12 );
    glVertexAttribPointer( GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( GLvoid * )24 );
    //glVertexAttribPointer( GLKVertexAttribTexCoord1, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( GLvoid * )32 );
    
    if( indices )
        glDrawElements( GL_TRIANGLES, numIndices, GL_UNSIGNED_SHORT, indices );
    else
        glDrawArrays( GL_TRIANGLES, 0, numVertices );
}

const GLKVector3 SMMesh::Size( void ) const
{
    return GLKVector3Subtract( maxs, mins );
}

const GLKVector3 SMMesh::Center( void ) const
{
    return GLKVector3MultiplyScalar( GLKVector3Add( maxs, mins ), 0.5f );
}

SMModel::SMModel( void )
{
}

SMModel::~SMModel( void )
{
    for( map< string, SMMesh * >::iterator i = meshes.begin(); i != meshes.end(); i++ )
        delete i->second;
}

const bool SMModel::Load( const char * filename )
{
    char resDirectory[ 1024 ];
    [ [ [ NSBundle mainBundle ] resourcePath ] getCString : resDirectory
                                                maxLength : sizeof( resDirectory )
                                                 encoding : NSASCIIStringEncoding ];
    
    char path[ 1024 ];
    sprintf( path, "%s/%s", resDirectory, filename );
    
    Lib3dsFile * lib3ds_file = lib3ds_file_open( path );
    if( !lib3ds_file )
        return false;
    
    for( int i = 0; i < lib3ds_file->nmeshes; i++ )
    {
        if( !lib3ds_file->meshes[ i ]->nvertices )
            continue;
        
        SMMesh * m = new SMMesh();
        m->Load( lib3ds_file->meshes[ i ] );
        meshes[ lib3ds_file->meshes[ i ]->name ] = m;
    }
    
    lib3ds_file_free( lib3ds_file );
    
    return true;
}

void SMModel::Draw( const char * name ) const
{
    if( name )
    {
        if( textures.count( name ) )
        {
            GLKTextureInfo * texture = textures.at( name );
            glBindTexture( texture.target, texture.name );
        }
        /*else
            glBindTexture( GL_TEXTURE_2D, 0 );*/
            
        meshes.at( name )->Draw();
    }
    else
        for( map< string, SMMesh * >::const_iterator i = meshes.begin(); i != meshes.end(); i++ )
            Draw( i->first.c_str() );
            //i->second->Draw();
}

SMMesh * const SMModel::GetMesh( const char * name ) const
{
    if( meshes.find( name ) != meshes.end() )
        return meshes.at( name );
    return 0;
}

void SMModel::SetTextureForMesh( const char * name, GLKTextureInfo * texture )
{
    textures[ name ] = texture;
}

void SMModel::SetTexture( GLKTextureInfo * texture )
{
    for( const auto & i : meshes )
        textures[ i.first ] = texture;
}