//
//  SSCommon.h
//  SalesStar
//
//  Created by Dgut on 01.02.13.
//  Copyright (c) 2013 Dgut. All rights reserved.
//

#ifndef __Thickness__SMCommon__
#define __Thickness__SMCommon__

#include <GLKit/GLKit.h>

#include <map>
#include <string>
#include <stack>

class SMModel;
class SMShader;
class SMBuffer;

#define gCommon SMCommon::Instance()

enum SMMatrixMode
{
    SM_MODEL,
    SM_VIEW,
    SM_PROJECTION,
};

class SMCommon
{    
    GLKMatrix4                          modelMatrix;
    GLKMatrix4                          viewMatrix;
    GLKMatrix4                          projMatrix;
    
    std::stack< GLKMatrix4 >            matrixStack;
    
    int                                 oldTime;
    int                                 delta;
    int                                 time;
    
    GLKVector2                          size;
    
    const int                           Milliseconds( void );
    
                                        SMCommon( void );
                                        ~SMCommon( void );
public:
    void                                Tick( void );
    
    const int                           Delta( void ) const;
    const int                           Time( void ) const;
    
    void                                Init( void );
    void                                Clear( void );
    
    SMModel * const                     GetModel( const char * name );
    SMShader * const                    GetShader( const char * name );
    GLKTextureInfo * const              GetTexture( const char * name );
    GLKTextureInfo * const              GetCubemap( const char * name );
    
    /*void                                Resize( const GLKVector2 & size );
    const GLKVector2 &                  GetViewport( void ) const;*/
    
    void                                PushMatrix( SMMatrixMode mode );
    void                                PopMatrix( SMMatrixMode mode );
    
    void                                SetMatrix( SMMatrixMode mode, const GLKMatrix4 & matrix );
    const GLKMatrix4 &                  GetMatrix( SMMatrixMode mode ) const;
    
    void                                MultMatrix( SMMatrixMode mode, const GLKMatrix4 & matrix );
    
    void                                Translate( SMMatrixMode mode, float x, float y, float z );
    void                                Rotate( SMMatrixMode mode, float angle, float x, float y, float z );
    void                                Scale( SMMatrixMode mode, float x, float y, float z );
    void                                Scale( SMMatrixMode mode, float scale );
    
    const GLKMatrix4                    GetMVP( void ) const;
    
    static SMCommon &                   Instance( void );
};

const GLKVector4 ColorCode( int code );
const int ColorDecode( const GLKVector4 & color );

#endif /* defined(__Thickness__SMCommon__) */
