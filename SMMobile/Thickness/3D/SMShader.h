//
//  SSShader.h
//  SalesStar
//
//  Created by Dgut on 01.02.13.
//  Copyright (c) 2013 Dgut. All rights reserved.
//

#ifndef __Thickness__SMShader__
#define __Thickness__SMShader__

#include <GLKit/GLKit.h>

#include <map>
#include <string>

class SMShader
{
    GLuint                              program;
    
    std::map< std::string, GLuint >     uniforms;
    
    const bool                          Compile( GLuint * shader, GLenum type, NSString * file );
    const bool                          Link( void );
public:
                                        SMShader( void );
                                        ~SMShader( void );
    
    const bool                          Load( const char * vertex, const char * fragment );
    
    void                                Bind( void );
    
    void                                ApplyTransform( void );
    
    const GLuint                        GetUniformLocation( const char * name );
    
    void                                Uniform1( const char * name, GLfloat x );
    void                                Uniform1( const char * name, GLint x );
    void                                Uniform2( const char * name, const GLfloat * v );
    void                                Uniform3( const char * name, const GLfloat * v );
    void                                Uniform4( const char * name, const GLfloat * v );
    void                                UniformMatrix3( const char * name, const GLfloat * v );
    void                                UniformMatrix4( const char * name, const GLfloat * v );
};

#endif /* defined(__Thickness__SMShader__) */
