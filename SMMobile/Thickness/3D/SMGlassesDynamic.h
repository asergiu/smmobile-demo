//
//  SMGlassesDynamic.h
//  SMMobile
//
//  Created by Dgut on 09.09.15.
//  Copyright (c) 2015 ACEP. All rights reserved.
//

#ifndef __SMMobile__SMGlassesDynamic__
#define __SMMobile__SMGlassesDynamic__

#include "SMGlasses.h"

class SMGlassesDynamic : public SMGlassesBase
{
    SMModel *                           bridge;
    SMModel *                           frame_left;
    //SMModel *                           frame_right;
    SMModel *                           ear_left;
    //SMModel *                           ear_right;
    
    float                               nativeDbl;
    float                               nativeA;
    float                               nativeB;
    
    float                               dbl;
    float                               a;
    float                               b;
    
    GLKVector3                          axis;
    GLKVector3                          ear;
public:
                                        SMGlassesDynamic( const char * contour, float dbl, SMModel * bridge, SMModel * frame_left/*, SMModel * frame_right*/, SMModel * ear_left/*, SMModel * ear_right*/ );
    virtual                             ~SMGlassesDynamic( void );
    
    void                                SetDBL( float dbl );
    void                                SetA( float a );
    void                                SetB( float b );
    void                                Reset( void );
    
    void                                SetAxis( const GLKVector3 & axis );
    void                                SetEar( const GLKVector3 & ear );
    
    virtual void                        Draw( bool frame, bool ears, bool lens, bool left, bool refraction, float offset, float backgroundOffset, bool split, bool leftOptions, float side, float sideOffset );
};

#endif /* defined(__SMMobile__SMGlassesDynamic__) */
