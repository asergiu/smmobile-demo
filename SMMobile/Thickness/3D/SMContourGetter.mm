//
//  SMContourGetter.cpp
//  SpecSaversVDTM2
//
//  Created by Dgut on 07.11.16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#include "SMContourGetter.h"

using namespace std;

SMContourGetter::SMContourGetter( void )
{
    contours[ 0 ].Load( "contour_girl_rod.txt" );
    contours[ 1 ].Load( "contour_mark_rod.txt" );
}

SMContourGetter::~SMContourGetter( void )
{
}

const vector< CFTPoint > SMContourGetter::GetContour( bool male, float width, float height )
{
    SMContour * contour = contours + ( int )male;
    
    float originalWidth = contour->GetMax().x - contour->GetMin().x;
    float originalHeight = contour->GetMax().z - contour->GetMin().z;
    
    float scaleX = width / originalWidth;
    float scaleY = height / originalHeight;
    
    GLKVector3 center = contour->GetCenter();
    const vector< GLKVector3 > & points = contour->GetPoints();
    
    vector< CFTPoint > result;
    result.reserve( points.size() );
    
    for( int i = 0; i < points.size(); i++ )
    {
        CFTPoint point;
        point.x = ( points[ i ].x - center.x ) * scaleX;
        point.y = ( points[ i ].z - center.z ) * scaleY;
        result.push_back( point );
    }
    
    return result;
}
