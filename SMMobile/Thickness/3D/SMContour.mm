//
//  SSContour.cpp
//  SalesStar
//
//  Created by Dgut on 01.02.13.
//  Copyright (c) 2013 Dgut. All rights reserved.
//

#include "SMContour.h"

using namespace std;

SMContour::SMContour( void )
{
}

SMContour::~SMContour( void )
{
}

const bool SMContour::Load( const char * filename )
{
    char resDirectory[ 1024 ];
    [ [ [ NSBundle mainBundle ] resourcePath ] getCString : resDirectory
                                                maxLength : sizeof( resDirectory )
                                                 encoding : NSASCIIStringEncoding ];
    
    char path[ 1024 ];
    sprintf( path, "%s/%s", resDirectory, filename );
    
    FILE * stream = fopen( path, "rt" );
    if( !stream )
        return false;
    
    center = GLKVector3Make( 0.f, 0.f, 0.f );
    min = GLKVector3Make( FLT_MAX, FLT_MAX, FLT_MAX );
    max = GLKVector3Make( FLT_MIN, FLT_MIN, FLT_MIN );
    
    GLKVector3 point;
    while( fscanf( stream, "%f%f%f", &point.x, &point.y, &point.z ) != EOF )
    {
        points.push_back( point );
        center = GLKVector3Add( center, point );
        
        if( min.x > point.x )
            min.x = point.x;
        if( min.y > point.y )
            min.y = point.y;
        if( min.z > point.z )
            min.z = point.z;
        
        if( max.x < point.x )
            max.x = point.x;
        if( max.y < point.y )
            max.y = point.y;
        if( max.z < point.z )
            max.z = point.z;
    }
    center = GLKVector3DivideScalar( center, points.size() );
    
    fclose( stream );
    
    struct Predicate
    {
        GLKVector3 center;
        
        bool operator()( GLKVector3 const & a, GLKVector3 const & b ) const
        {
            GLKVector3 ac = GLKVector3Subtract( a, center );
            GLKVector3 bc = GLKVector3Subtract( b, center );
            
            //return ac.x * bc.y - ac.y * bc.x > 0;
            return atan2( ac.z, ac.x ) > atan2( bc.z, bc.x );
        }
    };
    
    Predicate predicate = { center };
    
    std::sort( points.begin(), points.end(), predicate );
    
    return true;
}

const std::vector< GLKVector3 > & SMContour::GetPoints( void ) const
{
    return points;
}

const GLKVector3 & SMContour::GetCenter( void ) const
{
    return center;
}

const GLKVector3 & SMContour::GetMin( void ) const
{
    return min;
}

const GLKVector3 & SMContour::GetMax( void ) const
{
    return max;
}

void SMContour::SetStep( float step )
{
    SMContour::step = step;
}

const double SMContour::GetStep( void ) const
{
    return step;
}
