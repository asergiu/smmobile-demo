//
//  SSContour.h
//  SalesStar
//
//  Created by Dgut on 01.02.13.
//  Copyright (c) 2013 Dgut. All rights reserved.
//

#ifndef __Thickness__SMContour__
#define __Thickness__SMContour__

#include <GLKit/GLKit.h>

#include "lib3ds.h"

#include <vector>

class SMContour
{
    std::vector< GLKVector3 >           points;
    GLKVector3                          center;
    GLKVector3                          min;
    GLKVector3                          max;
    double                              step;
public:
                                        SMContour( void );
                                        ~SMContour( void );
    
    const bool                          Load( const char * filename );
    
    const std::vector< GLKVector3 > &   GetPoints( void ) const;
    const GLKVector3 &                  GetCenter( void ) const;
    const GLKVector3 &                  GetMin( void ) const;
    const GLKVector3 &                  GetMax( void ) const;
    
    void                                SetStep( float step );
    const double                        GetStep( void ) const;
};

#endif /* defined(__Thickness__SMContour__) */
