//
//  SSCommon.cpp
//  SalesStar
//
//  Created by Dgut on 01.02.13.
//  Copyright (c) 2013 Dgut. All rights reserved.
//

#include "SMCommon.h"
#include "SMModel.h"
#include "SMShader.h"
#include "SMBuffer.h"

#include <sys/time.h>

using namespace std;

SMCommon::SMCommon( void )
{
}

SMCommon::~SMCommon( void )
{
    Clear();
}

const int SMCommon::Milliseconds( void )
{
    timeval tv;
	static int start = 0;
	
	gettimeofday( &tv, 0 );
	
	if( !start )
	{
		start = tv.tv_sec;
		return tv.tv_usec / 1000;
	}
	
	return ( tv.tv_sec - start ) * 1000 + tv.tv_usec / 1000;
}

void SMCommon::Tick( void )
{
    int newTime;
    
    do
    {
        newTime = Milliseconds();
        delta = newTime - oldTime;
    }
    while( delta < 1 );
    
    oldTime = newTime;
    
    time += delta;
}

const int SMCommon::Delta( void ) const
{
    return delta;
}

const int SMCommon::Time( void ) const
{
    return time;
}

void SMCommon::Init( void )
{
    srand( ::time( 0 ) );
    
    modelMatrix = viewMatrix = projMatrix = GLKMatrix4Identity;
    
    glEnable( GL_DEPTH_TEST );
    glEnable( GL_CULL_FACE );
    
    glEnableVertexAttribArray( GLKVertexAttribPosition );
    glEnableVertexAttribArray( GLKVertexAttribNormal );
    glEnableVertexAttribArray( GLKVertexAttribTexCoord0 );
    //glEnableVertexAttribArray( GLKVertexAttribTexCoord1 );
    
    oldTime = Milliseconds();
    time = 0;
}

void SMCommon::Clear( void )
{
}

SMModel * const SMCommon::GetModel( const char * name )
{
    SMModel * model = new SMModel();
    model->Load( name );
    
    return model;
}

SMShader * const SMCommon::GetShader( const char * name )
{
    SMShader * shader = new SMShader();
    
    char vertex[ 256 ];
    sprintf( vertex, "%s.vsh", name );
    
    char fragment[ 256 ];
    sprintf( fragment, "%s.fsh", name );
    
    shader->Load( vertex, fragment );
    
    return shader;
}

GLKTextureInfo * const SMCommon::GetTexture( const char * name )
{
    NSString * bundlePath = [ [ NSBundle mainBundle ] pathForResource : [ [ NSString alloc ] initWithCString : name encoding : NSASCIIStringEncoding ] ofType : nil ];
    NSDictionary * options = [ NSDictionary dictionaryWithObjectsAndKeys : [ NSNumber numberWithBool : YES ], GLKTextureLoaderOriginBottomLeft, [ NSNumber numberWithBool : NO ], GLKTextureLoaderApplyPremultiplication, nil ];
    GLKTextureInfo * texture = [ GLKTextureLoader textureWithContentsOfFile : bundlePath options : options error : NULL ];
    
    return texture;
}

GLKTextureInfo * const SMCommon::GetCubemap( const char * name )
{
    NSString * bundlePath = [ [ NSBundle mainBundle ] pathForResource : [ [ NSString alloc ] initWithCString : name encoding : NSASCIIStringEncoding ] ofType : nil ];
    NSDictionary * options = [ NSDictionary dictionaryWithObjectsAndKeys : /*[ NSNumber numberWithBool : YES ], GLKTextureLoaderOriginBottomLeft,*/ [ NSNumber numberWithBool : NO ], GLKTextureLoaderApplyPremultiplication, nil ];
    GLKTextureInfo * texture = [ GLKTextureLoader cubeMapWithContentsOfFile : bundlePath options : options error : NULL ];
    
    return texture;
}

/*void SMCommon::Resize( const GLKVector2 & size )
{
    SMCommon::size = size;
}

const GLKVector2 & SMCommon::GetViewport( void ) const
{
    return size;
}*/

void SMCommon::PushMatrix( SMMatrixMode mode )
{
    switch( mode )
    {
        case SM_MODEL:
            matrixStack.push( modelMatrix );
            break;
            
        case SM_VIEW:
            matrixStack.push( viewMatrix );
            break;
            
        case SM_PROJECTION:
            matrixStack.push( projMatrix );
            break;
    }
}

void SMCommon::PopMatrix( SMMatrixMode mode )
{
    switch( mode )
    {
        case SM_MODEL:
            modelMatrix = matrixStack.top();
            matrixStack.pop();
            break;
            
        case SM_VIEW:
            viewMatrix = matrixStack.top();
            matrixStack.pop();
            break;
            
        case SM_PROJECTION:
            projMatrix = matrixStack.top();
            matrixStack.pop();
            break;
    }
}

void SMCommon::SetMatrix( SMMatrixMode mode, const GLKMatrix4 & matrix )
{
    switch( mode )
    {
        case SM_MODEL:
            modelMatrix = matrix;
            break;
            
        case SM_VIEW:
            viewMatrix = matrix;
            break;
            
        case SM_PROJECTION:
            projMatrix = matrix;
            break;
    }
}

const GLKMatrix4 & SMCommon::GetMatrix( SMMatrixMode mode ) const
{
    switch( mode )
    {
        case SM_MODEL:
            return modelMatrix;
            break;
            
        case SM_VIEW:
            return viewMatrix;
            break;
            
        case SM_PROJECTION:
            return projMatrix;
            break;
    }
}

void SMCommon::MultMatrix( SMMatrixMode mode, const GLKMatrix4 & matrix )
{
    switch( mode )
    {
        case SM_MODEL:
            modelMatrix = GLKMatrix4Multiply( modelMatrix, matrix );
            break;
            
        case SM_VIEW:
            viewMatrix = GLKMatrix4Multiply( viewMatrix, matrix );
            break;
            
        case SM_PROJECTION:
            projMatrix = GLKMatrix4Multiply( projMatrix, matrix );
            break;
    }
}

void SMCommon::Translate( SMMatrixMode mode, float x, float y, float z )
{
    switch( mode )
    {
        case SM_MODEL:
            modelMatrix = GLKMatrix4Translate( modelMatrix, x, y, z );
            break;
            
        case SM_VIEW:
            viewMatrix = GLKMatrix4Translate( viewMatrix, x, y, z );
            break;
            
        case SM_PROJECTION:
            projMatrix = GLKMatrix4Translate( projMatrix, x, y, z );
            break;
    }
}

void SMCommon::Rotate( SMMatrixMode mode, float angle, float x, float y, float z )
{
    switch( mode )
    {
        case SM_MODEL:
            modelMatrix = GLKMatrix4Rotate( modelMatrix, angle, x, y, z );
            break;
            
        case SM_VIEW:
            viewMatrix = GLKMatrix4Rotate( viewMatrix, angle, x, y, z );
            break;
            
        case SM_PROJECTION:
            projMatrix = GLKMatrix4Rotate( projMatrix, angle, x, y, z );
            break;
    }
}

void SMCommon::Scale( SMMatrixMode mode, float x, float y, float z )
{
    switch( mode )
    {
        case SM_MODEL:
            modelMatrix = GLKMatrix4Scale( modelMatrix, x, y, z );
            break;
            
        case SM_VIEW:
            viewMatrix = GLKMatrix4Scale( viewMatrix, x, y, z );
            break;
            
        case SM_PROJECTION:
            projMatrix = GLKMatrix4Scale( projMatrix, x, y, z );
            break;
    }
}

void SMCommon::Scale( SMMatrixMode mode, float scale )
{
    Scale( mode, scale, scale, scale );
}

const GLKMatrix4 SMCommon::GetMVP( void ) const
{
    return GLKMatrix4Multiply( GetMatrix( SM_PROJECTION ), GLKMatrix4Multiply( GetMatrix(SM_VIEW ), GetMatrix(SM_MODEL ) ) );
}

SMCommon & SMCommon::Instance( void )
{
    static SMCommon instance;
    return instance;
}

const GLKVector4 ColorCode( int code )
{
    return GLKVector4Make( ( code % 8 ) / 8.f, ( code / 8 % 8 ) / 8.f, ( code / 64 % 8 ) / 8.f, 1.f );
}

const int ColorDecode( const GLKVector4 & color )
{
    if( !color.w )
        return -1;
    return floor( color.x * 8.f + 0.5f ) + floor( color.y * 64.f + 0.5f ) + floor( color.z * 512.f + 0.5f );
}