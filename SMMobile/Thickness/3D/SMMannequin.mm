//
//  SMMannequin.cpp
//  SMMobile
//
//  Created by Dgut on 17.12.14.
//

#include "SMMannequin.h"

#include "SMModel.h"
#include "SMShader.h"

#include "SMCommon.h"
#include "SMThickness3DView.h"

SMMannequin::SMMannequin( SMModel * model ) :
    model       ( model ),
    scale       ( 1.f ),
    shift       ( GLKVector3Make( 0, 0, 0 ) )
{
}

SMMannequin::~SMMannequin( void )
{
}

SMModel * const SMMannequin::GetModel( void ) const
{
    return model;
}

void SMMannequin::SetScale( float scale )
{
    SMMannequin::scale = scale;
}

const float SMMannequin::GetScale( void ) const
{
    return scale;
}

void SMMannequin::SetShift( const GLKVector3 & shift )
{
    SMMannequin::shift = shift;
}

void SMMannequin::Draw( float side, float offset )
{
    GLKVector4 clip = { 0 };
    if( side < 0.f )
    {
        clip.x = -1.f;
        clip.w = offset;
    }
    else if( side > 0.f )
    {
        clip.x = 1.f;
        clip.w = -offset;
    }
    
    thickness3DGetShader( "ThicknessOneLightClip" )->Uniform4( "clip", clip.v );
    
    gCommon.PushMatrix( SM_MODEL );
    gCommon.Scale( SM_MODEL, scale );
    
    if( side )
        thickness3DGetShader( "ThicknessOneLightClip" )->ApplyTransform();
    else
        thickness3DGetShader( "ThicknessOneLight" )->ApplyTransform();
    
    model->Draw();
    
    gCommon.PopMatrix( SM_MODEL );
}