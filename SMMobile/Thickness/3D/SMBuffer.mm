//
//  SSBuffer.cpp
//  SalesStar
//
//  Created by Dgut on 02.02.13.
//  Copyright (c) 2013 Dgut. All rights reserved.
//

#include "SMBuffer.h"

SMBuffer::SMBuffer( int width, int height ) :
    width   ( width ),
    height  ( height )/*,
    data    ( 0 )*/
{
    //data = new unsigned char[ width * height * 4 ];
    
    glGenFramebuffers( 1, &frameBuffer );
    glBindFramebuffer( GL_FRAMEBUFFER, frameBuffer );
    
    glGenRenderbuffers( 1, &colorBuffer );
    glBindRenderbuffer( GL_RENDERBUFFER, colorBuffer );
    glRenderbufferStorage( GL_RENDERBUFFER, GL_RGBA8_OES, width, height );
    glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorBuffer );
    
    glGenRenderbuffers( 1, &depthBuffer );
    glBindRenderbuffer( GL_RENDERBUFFER, depthBuffer );
    glRenderbufferStorage( GL_RENDERBUFFER, GL_DEPTH_COMPONENT24_OES, width, height );
    glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer );
    
    glGenTextures( 1, &texture );
    glBindTexture( GL_TEXTURE_2D, texture );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0 );
    glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0 );
    
    /*GLint defaultFBO;
    glGetIntegerv( GL_FRAMEBUFFER_BINDING_OES, &defaultFBO );
    
    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    
    GLint depthBuffer_;
    glGetFramebufferAttachmentParameteriv( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &depthBuffer_ );
    
    depthBuffer_ = depthBuffer_;*/
}

SMBuffer::~SMBuffer( void )
{
    //delete[] data;
    
    glDeleteFramebuffers( 1, &frameBuffer );
    glDeleteRenderbuffers( 1, &colorBuffer );
    glDeleteRenderbuffers( 1, &depthBuffer );
    //glDeleteTextures( 1, &texture );
}

void SMBuffer::Bind( void )
{
    glGetIntegerv( GL_FRAMEBUFFER_BINDING, ( GLint * )&oldFrameBuffer );
    glBindFramebuffer( GL_FRAMEBUFFER, frameBuffer );
    glViewport( 0, 0, width, height );
    /*glClearColor( 0, 0, 0, 0 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );*/
}

void SMBuffer::Unbind( void )
{
    //glReadPixels( 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data );
    glBindFramebuffer( GL_FRAMEBUFFER, oldFrameBuffer );
}

void SMBuffer::BindTexture( void )
{
    glBindTexture( GL_TEXTURE_2D, texture );
}

const GLKVector2 SMBuffer::GetSize( void ) const
{
    return GLKVector2Make( width, height );
}

/*const GLKVector4 SSBuffer::GetColor( const GLKVector2 & point, const GLKVector2 & size ) const
{
    int x = point.x * width / size.x;
    int y = height - 1 - point.y * height / size.y;
    
    GLKVector4 result = GLKVector4Make( 0.f, 0.f, 0.f, 0.f );
    
    if( x >= 0 && y >= 0 && x < width && y < height )
    {
        result.r = data[ ( y * width + x ) * 4 + 0 ] / 256.f;
        result.g = data[ ( y * width + x ) * 4 + 1 ] / 256.f;
        result.b = data[ ( y * width + x ) * 4 + 2 ] / 256.f;
        result.a = data[ ( y * width + x ) * 4 + 3 ] / 256.f;
    }
    
    return result;
}*/
