//
//  SSModel.h
//  SalesStar
//
//  Created by Dgut on 01.02.13.
//  Copyright (c) 2013 Dgut. All rights reserved.
//

#ifndef __Thicness__SMModel__
#define __Thicness__SMModel__

#include <GLKit/GLKit.h>

#include "lib3ds.h"

#include <map>
#include <string>

class SMMesh
{
    GLuint                              buffer;
    
    int                                 numVertices;
    
    GLKVector3                          mins;
    GLKVector3                          maxs;
    
    short *                             indices;
    int                                 numIndices;
public:
    struct Vertex
    {
        GLKVector3                      position;
        GLKVector3                      normal;
        GLKVector2                      texCoord0;
        //GLKVector3                      tangent;
        
        /*const bool                      operator ==( const Vertex & v ) const
        {
            return !memcmp( this, &v, sizeof( *this ) );//GLKVector3AllEqualToVector3( position, v.position ) && GLKVector3AllEqualToVector3( normal, v.normal ) && GLKVector2AllEqualToVector2( texCoord0, v.texCoord0 );
        }*/
    };
    
                                        SMMesh( void );
                                        ~SMMesh( void );

    const bool                          Load( Lib3dsMesh * lib3ds_mesh );
    void                                SetVertices( const Vertex * vertices, int numVertices );
    void                                SetIndices( const short * indices, int numIndices );

    void                                Draw( void ) const;

    const GLKVector3                    Size( void ) const;
    const GLKVector3                    Center( void ) const;
};

class SMModel
{
    std::map< std::string, SMMesh * >   meshes;
    std::map< std::string, GLKTextureInfo * > textures;
public:
                                        SMModel( void );
                                        ~SMModel( void );
    
    const bool                          Load( const char * filename );
    
    void                                Draw( const char * name = 0 ) const;
    
    SMMesh * const                      GetMesh( const char * name ) const;
    
    void                                SetTextureForMesh( const char * name, GLKTextureInfo * texture );
    void                                SetTexture( GLKTextureInfo * texture );
};

#endif /* defined(__Thicness__SMModel__) */
