//
//  SMContourGetter.hpp
//  SpecSaversVDTM2
//
//  Created by Dgut on 07.11.16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#ifndef SMContourGetter_hpp
#define SMContourGetter_hpp

#include "FTPoint.h"
#include "SMContour.h"

#include <vector>

class SMContourGetter
{
    SMContour                       contours[ 2 ];
public:
                                    SMContourGetter( void );
                                    ~SMContourGetter( void );
    
    const std::vector< CFTPoint >   GetContour( bool male, float width, float height );
};

#endif /* SMContourGetter_hpp */
