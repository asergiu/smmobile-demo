//
//  SMGlassesDynamic.cpp
//  SMMobile
//
//  Created by Dgut on 09.09.15.
//  Copyright (c) 2015 ACEP. All rights reserved.
//

#include "SMGlassesDynamic.h"

#include "SMLens.h"
#include "SMContour.h"
#include "SMModel.h"
#include "SMShader.h"

#include "SMCommon.h"
#include "SMThickness3DView.h"

SMGlassesDynamic::SMGlassesDynamic( const char * contour, float dbl, SMModel * bridge, SMModel * frame_left/*, SMModel * frame_right*/, SMModel * ear_left/*, SMModel * ear_right*/ ) :
    SMGlassesBase   ( contour ),
    nativeDbl       ( dbl ),
    dbl             ( dbl ),
    bridge          ( bridge ),
    frame_left      ( frame_left ),
    //frame_right     ( frame_right ),
    ear_left        ( ear_left )
    //ear_right       ( ear_right )
{
    nativeA = a = SMGlassesBase::contour->GetMax().x - SMGlassesBase::contour->GetMin().x;
    nativeB = b = SMGlassesBase::contour->GetMax().z - SMGlassesBase::contour->GetMin().z;
}

SMGlassesDynamic::~SMGlassesDynamic( void )
{
}

void SMGlassesDynamic::SetDBL( float dbl )
{
    SMGlassesDynamic::dbl = dbl;
}

void SMGlassesDynamic::SetA( float a )
{
    SMGlassesDynamic::a = a;
}

void SMGlassesDynamic::SetB( float b )
{
    SMGlassesDynamic::b = b;
}

void SMGlassesDynamic::Reset( void )
{
    dbl = nativeDbl;
    a = nativeA;
    b = nativeB;
}

void SMGlassesDynamic::SetAxis( const GLKVector3 & axis )
{
    SMGlassesDynamic::axis = axis;
}

void SMGlassesDynamic::SetEar( const GLKVector3 & ear )
{
    SMGlassesDynamic::ear = ear;
}

void SMGlassesDynamic::Draw( bool frame, bool ears, bool lens, bool left, bool refraction, float offset, float backgroundOffset, bool split, bool leftOptions, float side, float sideOffset )
{
    float shiftDbl = ( dbl - nativeDbl ) / 2.f + ( a - nativeA ) / 2.f;
    
    GLKVector3 nativeToEar = ( GLKVector3Subtract( ear, axis ) );
    GLKVector3 toEar = ( GLKVector3Subtract( ear, GLKVector3Add( axis, GLKVector3Make( shiftDbl, 0.f, 0.f ) ) ) );
    
    float nativeAngle = atan2f( nativeToEar.z, nativeToEar.x );
    float angle = atan2f( toEar.z, toEar.x );
    
    float shiftAngle = nativeAngle - angle;
    
    if( frame )
    {
        gCommon.PushMatrix( SM_MODEL );
        gCommon.Scale( SM_MODEL, scale );
        
        glEnable( GL_BLEND );
        
        thickness3DGetShader( "ThicknessOneLight" )->ApplyTransform();
        SMGlassesDynamic::bridge->Draw();
        
        
        gCommon.PushMatrix( SM_MODEL );
        gCommon.Translate( SM_MODEL, shiftDbl, 0.f, 0.f );
        
        thickness3DGetShader( "ThicknessOneLight" )->ApplyTransform();
        SMGlassesDynamic::frame_left->Draw();
        
        gCommon.PopMatrix( SM_MODEL );
        
        gCommon.PushMatrix( SM_MODEL );
        gCommon.Scale( SM_MODEL, -1.f, 1.f, 1.f );
        gCommon.Translate( SM_MODEL, shiftDbl, 0.f, 0.f );
        
        thickness3DGetShader( "ThicknessOneLight" )->ApplyTransform();
        glCullFace( GL_FRONT );
        SMGlassesDynamic::frame_left->Draw();
        glCullFace( GL_BACK );
        
        gCommon.PopMatrix( SM_MODEL );
        
        glDisable( GL_BLEND );
        
        gCommon.PopMatrix( SM_MODEL );
    }
    
    if( ears )
    {
        gCommon.PushMatrix( SM_MODEL );
        gCommon.Scale( SM_MODEL, scale );
        
        if( side >= 0.f )
        {
            gCommon.PushMatrix( SM_MODEL );
            
            gCommon.Translate( SM_MODEL, shiftDbl, 0.f, 0.f );
            gCommon.Translate( SM_MODEL, axis.x, -axis.y, -axis.z );
            gCommon.Rotate( SM_MODEL, -shiftAngle, 0.f, 1.f, 0.f );
            gCommon.Translate( SM_MODEL, -axis.x, axis.y, axis.z );
            
            thickness3DGetShader( "ThicknessOneLight" )->ApplyTransform();
            SMGlassesDynamic::ear_left->Draw();
            
            gCommon.PopMatrix( SM_MODEL );
        }
        if( side <= 0.f )
        {
            gCommon.PushMatrix( SM_MODEL );
            
            gCommon.Scale( SM_MODEL, -1.f, 1.f, 1.f );
            
            gCommon.Translate( SM_MODEL, shiftDbl, 0.f, 0.f );
            gCommon.Translate( SM_MODEL, axis.x, -axis.y, -axis.z );
            gCommon.Rotate( SM_MODEL, -shiftAngle, 0.f, 1.f, 0.f );
            gCommon.Translate( SM_MODEL, -axis.x, axis.y, axis.z );
            
            thickness3DGetShader( "ThicknessOneLight" )->ApplyTransform();
            glCullFace( GL_FRONT );
            SMGlassesDynamic::ear_left->Draw();
            glCullFace( GL_BACK );
            
            gCommon.PopMatrix( SM_MODEL );
        }
        
        gCommon.PopMatrix( SM_MODEL );
    }
    
    if( lens )
    {
        gCommon.PushMatrix( SM_MODEL );
        gCommon.Translate( SM_MODEL, shift.x * scale, shift.y * scale, shift.z * scale );
        gCommon.Translate( SM_MODEL, shiftDbl * scale * ( left ? 1.f : -1.f ), 0.f, 0.f );
        
        SMLens * lens;
        
        if( split )
        {
            if( leftOptions )
            {
                if( left )
                    lens = leftLensLeftOptions;
                else
                    lens = rightLensLeftOptions;
            }
            else
            {
                if( left )
                    lens = leftLensRightOptions;
                else
                    lens = rightLensRightOptions;
            }
        }
        else
        {
            if( left )
                lens = leftLensLeftOptions;
            else
                lens = rightLensRightOptions;
        }
        
        GLKVector3 center = lens->Center();
        
        gCommon.Translate( SM_MODEL, center.x, center.y, center.z );
        gCommon.Scale( SM_MODEL, a / nativeA, b / nativeB, 1.f );
        gCommon.Translate( SM_MODEL, -center.x, -center.y, -center.z );
        
        if( refraction )
            lens->DrawRefraction();
        else
            lens->Draw( offset, backgroundOffset );
        
        gCommon.PopMatrix( SM_MODEL );
    }
}