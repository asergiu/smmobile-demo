//
//  SMGlasses.cpp
//  SMMobile
//
//  Created by Dgut on 15.12.14.
//

#include "SMGlasses.h"

#include "SMLens.h"
#include "SMContour.h"
#include "SMModel.h"
#include "SMShader.h"

#include "SMCommon.h"
#include "SMThickness3DView.h"

SMGlassesBase::SMGlassesBase( const char * contour ) :
    leftLensLeftOptions     ( new SMLens( true ) ),
    leftLensRightOptions    ( new SMLens( true ) ),
    rightLensLeftOptions    ( new SMLens( false ) ),
    rightLensRightOptions   ( new SMLens( false ) ),
    contour                 ( new SMContour() ),
    scale                   ( 1.f ),
    shift                   ( GLKVector3Make( 0, 0, 0 ) )
{
    SMGlassesBase::contour->Load( contour );
}

SMGlassesBase::~SMGlassesBase( void )
{
    delete leftLensLeftOptions;
    delete leftLensRightOptions;
    delete rightLensLeftOptions;
    delete rightLensRightOptions;
    delete contour;
}

SMLens * const SMGlassesBase::GetLeftLensLeftOptions( void ) const
{
    return leftLensLeftOptions;
}

SMLens * const SMGlassesBase::GetLeftLensRightOptions( void ) const
{
    return leftLensRightOptions;
}

SMLens * const SMGlassesBase::GetRightLensLeftOptions( void ) const
{
    return rightLensLeftOptions;
}

SMLens * const SMGlassesBase::GetRightLensRightOptions( void ) const
{
    return rightLensRightOptions;
}

SMContour * const SMGlassesBase::GetContour( void ) const
{
    return contour;
}

void SMGlassesBase::SetScale( float scale )
{
    SMGlassesBase::scale = scale;
}

const float SMGlassesBase::GetScale( void ) const
{
    return scale;
}

void SMGlassesBase::SetShift( const GLKVector3 & shift )
{
    SMGlassesBase::shift = shift;
}

SMGlasses::SMGlasses( const char * contour, SMModel * frame, SMModel * ears ) :
    SMGlassesBase           ( contour ),
    frame                   ( frame ),
    ears                    ( ears )
{
}

SMGlasses::~SMGlasses( void )
{
}

SMModel * const SMGlasses::GetFrame( void ) const
{
    return frame;
}

SMModel * const SMGlasses::GetEars( void ) const
{
    return ears;
}

void SMGlasses::Draw( bool frame, bool ears, bool lens, bool left, bool refraction, float offset, float backgroundOffset, bool split, bool leftOptions, float side, float sideOffset )
{
    if( frame )
    {
        gCommon.PushMatrix( SM_MODEL );
        gCommon.Scale( SM_MODEL, scale );
        
        glEnable( GL_BLEND );
        
        thickness3DGetShader( "ThicknessOneLight" )->ApplyTransform();
        SMGlasses::frame->Draw();
        
        glDisable( GL_BLEND );
        
        gCommon.PopMatrix( SM_MODEL );
    }
    
    if( ears )
    {
        GLKVector4 clip = { 0 };
        if( side < 0.f )
        {
            clip.x = -1.f;
            clip.w = sideOffset;
        }
        else if( side > 0.f )
        {
            clip.x = 1.f;
            clip.w = -sideOffset;
        }
        
        thickness3DGetShader( "ThicknessOneLightClip" )->Uniform4( "clip", clip.v );
        
        gCommon.PushMatrix( SM_MODEL );
        gCommon.Scale( SM_MODEL, scale );
        
        if( side )
            thickness3DGetShader( "ThicknessOneLightClip" )->ApplyTransform();
        else
            thickness3DGetShader( "ThicknessOneLight" )->ApplyTransform();
        
        SMGlasses::ears->Draw();
        
        gCommon.PopMatrix( SM_MODEL );
    }
    
    if( lens )
    {
        //thickness3DGetShader( "ThicknessLens" )->Uniform1( "aspect", float( self.frame.size.width / self.frame.size.height ) );
        
        gCommon.PushMatrix( SM_MODEL );
        //gCommon.Translate( SM_MODEL, 0, ( 0.085f ) * scale, ( 10.645f ) * scale );
        gCommon.Translate( SM_MODEL, shift.x * scale, shift.y * scale, shift.z * scale );
        
        if( split )
        {
            if( leftOptions )
            {
                if( left )
                    refraction ? leftLensLeftOptions->DrawRefraction() : leftLensLeftOptions->Draw( offset, backgroundOffset );
                else
                    refraction ? rightLensLeftOptions->DrawRefraction() : rightLensLeftOptions->Draw( offset, backgroundOffset );
            }
            else
            {
                if( left )
                    refraction ? leftLensRightOptions->DrawRefraction() : leftLensRightOptions->Draw( offset, backgroundOffset );
                else
                    refraction ? rightLensRightOptions->DrawRefraction() : rightLensRightOptions->Draw( offset, backgroundOffset );
            }
        }
        else
        {
            if( left )
                refraction ? leftLensLeftOptions->DrawRefraction() : leftLensLeftOptions->Draw( offset, backgroundOffset );
            else
                refraction ? rightLensRightOptions->DrawRefraction() : rightLensRightOptions->Draw( offset, backgroundOffset );
        }
        
        gCommon.PopMatrix( SM_MODEL );
    }
}