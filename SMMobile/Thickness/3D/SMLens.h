//
//  SSLens.h
//  SalesStar
//
//  Created by Dgut on 01.02.13.
//  Copyright (c) 2013 Dgut. All rights reserved.
//

#ifndef __Thickness__SMLens__
#define __Thickness__SMLens__

#include "SMModel.h"

#include <map>
#include <vector>

#include "IWSLLens.h"

class SMLens
{
    SMMesh *                            front;
    SMMesh *                            rear;
    SMMesh *                            outer;
    SMMesh *                            inner;
    
    GLKTextureInfo *                    white;
    
    bool                                left;
    
    float                               shift;
public:
                                        SMLens( bool left );
                                        ~SMLens( void );
    
    void                                Init( void );
    
    void                                SetData( const float * x, const float * y, const float * front, const float * rear, int count );
    void                                SetSurfaceData( const float * x, const float * y, const float * front, const float * rear, const GLKVector3 * normalFront, const GLKVector3 * normalRear, int count, float step );
    
    void                                Draw( float offset, float backgroundOffset ) const;
    void                                DrawRefraction( void ) const;
    
    const GLKVector3                    Center( void ) const;
};

#endif /* defined(__Thickness__SMLens__) */
