//
//  SSBuffer.h
//  SalesStar
//
//  Created by Dgut on 02.02.13.
//  Copyright (c) 2013 Dgut. All rights reserved.
//

#ifndef __Thickness__SMBuffer__
#define __Thickness__SMBuffer__

#include <GLKit/GLKit.h>

class SMBuffer
{
    int                                 width;
    int                                 height;
    
    //unsigned char *                     data;
    
    GLuint                              frameBuffer;
	GLuint                              colorBuffer;
    GLuint                              depthBuffer;
    GLuint                              texture;
    
    GLuint                              oldFrameBuffer;
public:
                                        SMBuffer( int width, int height );
                                        ~SMBuffer( void );
    
    void                                Bind( void );
    void                                Unbind( void );
    
    void                                BindTexture( void );
    
    const GLKVector2                    GetSize( void ) const;
    
    //const GLKVector4                    GetColor( const GLKVector2 & point, const GLKVector2 & size ) const;
};

#endif /* defined(__Thickness__SMBuffer__) */
