//
//  SMGlasses.h
//  SMMobile
//
//  Created by Dgut on 15.12.14.
//  Copyright (c) 2014 LLC. All rights reserved.
//

#ifndef __SMMobile__SMGlasses__
#define __SMMobile__SMGlasses__

#include <GLKit/GLKit.h>

class SMLens;
class SMContour;
class SMModel;

class SMGlassesBase
{
protected:
    SMLens *                            leftLensLeftOptions;
    SMLens *                            leftLensRightOptions;
    SMLens *                            rightLensLeftOptions;
    SMLens *                            rightLensRightOptions;
    
    SMContour *                         contour;
    
    float                               scale;
    GLKVector3                          shift;
public:
                                        SMGlassesBase( const char * contour );
    virtual                             ~SMGlassesBase( void );
    
    SMLens * const                      GetLeftLensLeftOptions( void ) const;
    SMLens * const                      GetLeftLensRightOptions( void ) const;
    SMLens * const                      GetRightLensLeftOptions( void ) const;
    SMLens * const                      GetRightLensRightOptions( void ) const;
    
    SMContour * const                   GetContour( void ) const;
    
    void                                SetScale( float scale );
    const float                         GetScale( void ) const;
    
    void                                SetShift( const GLKVector3 & shift );
    
    virtual void                        Draw( bool frame, bool ears, bool lens, bool left, bool refraction, float offset, float backgroundOffset, bool split, bool leftOptions, float side, float sideOffset ) = 0;
};

class SMGlasses : public SMGlassesBase
{
    SMModel *                           frame;
    SMModel *                           ears;
public:
                                        SMGlasses( const char * contour, SMModel * frame, SMModel * ears );
    virtual                             ~SMGlasses( void );
    
    SMModel * const                     GetFrame( void ) const;
    SMModel * const                     GetEars( void ) const;
    
    virtual void                        Draw( bool frame, bool ears, bool lens, bool left, bool refraction, float offset, float backgroundOffset, bool split, bool leftOptions, float side, float sideOffset );
};

#endif /* defined(__SMMobile__SMGlasses__) */
