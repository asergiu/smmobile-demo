//
//  OldEngine.h
//  Thickness
//
//  Created by Dgut on 15.08.14.
//  Copyright (c) 2014 Dgut. All rights reserved.
//

#ifndef __Thickness__SMOldEngine__
#define __Thickness__SMOldEngine__

#import "MatrixEngine.h"
#import "DataSource.h"

//#import "SMLens.h"

class SMGlassesBase;
class SMContour;
class SMLens;

class SMOldEngine
{
    CObjPtr< ILens >                    leftLens;
    CObjPtr< ILens >                    rightLens;
    CMatrixEngine                       leftEngine, rightEngine;
    
    double                              power;
    double                              cyl;
    long                                axis;
    
    NSString *                          leftDesign;
    NSString *                          rightDesign;
    
    DataSource *                        dataSource;
    
    SMGlassesBase *                     glasses;
    
    double                              step;
    
    void                                SurfaceCalculate( void );
    void                                SurfaceCalculateLens( ILens * lens, SMLens & ssLens );
    void                                SurfaceCalculateLens2( ILens * lens, SMLens & ssLens );
    
    void                                SetLeft( double power, double cyl, long axis, long aspheric, double index, SurfaceData R1, SurfaceData R2, SurfaceData R3, BOOL dynamic );
    void                                SetRight( double power, double cyl, long axis, long aspheric, double index, SurfaceData R1, SurfaceData R2, SurfaceData R3, BOOL dynamic );
public:
                                        SMOldEngine( void );
                                        ~SMOldEngine( void );
    
    //SMLens                              leftSSLens;
    //SMLens                              rightSSLens;
    
    //void                                Init( void );
    
    void                                SetGlasses( SMGlassesBase * glasses, BOOL dynamic);
    
    void                                SetPower( double power );
    void                                SetCyl( double cyl );
    void                                SetAxis( long axis );
    
    void                                SetLeftDesign( NSString * design );
    void                                SetRightDesign( NSString * design );
    
    void                                SetContour( SMContour * contour, float scale, double PD );
    void                                SetContour( const double * xFrame4, const double * yFrame4, int Count, double PD );
    
    void                                Refresh( BOOL dynamic );
};

double getIndex( NSString * design );
long getAspheric( NSString * design );

#endif /* defined(__Thickness__SMOldEngine__) */
