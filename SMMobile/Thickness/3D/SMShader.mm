//
//  SSShader.cpp
//  SalesStar
//
//  Created by Dgut on 01.02.13.
//  Copyright (c) 2013 Dgut. All rights reserved.
//

#include "SMShader.h"
#include "SMCommon.h"

SMShader::SMShader( void ) :
    program ( 0 )
{
}

SMShader::~SMShader( void )
{
    glDeleteProgram( program );
}

const bool SMShader::Load( const char * vertex, const char * fragment )
{
    GLuint vs, fs;
    NSString * bundlePath;
    
    glDeleteProgram( program );
    program = glCreateProgram();
    
    bundlePath = [ [ NSBundle mainBundle ] pathForResource : [ NSString stringWithCString : vertex encoding : NSASCIIStringEncoding ] ofType : nil ];
    if( !Compile( &vs, GL_VERTEX_SHADER, bundlePath ) )
    {
        NSLog( @"Failed to compile vertex shader" );
        return false;
    }
    
    bundlePath = [ [ NSBundle mainBundle ] pathForResource : [ NSString stringWithCString : fragment encoding : NSASCIIStringEncoding ] ofType : nil ];
    if( !Compile( &fs, GL_FRAGMENT_SHADER, bundlePath ) )
    {
        NSLog( @"Failed to compile fragment shader" );
        return false;
    }
    
    glAttachShader( program, vs );
    glAttachShader( program, fs );
    
    glBindAttribLocation( program, GLKVertexAttribPosition, "position" );
    glBindAttribLocation( program, GLKVertexAttribNormal, "normal" );
    glBindAttribLocation( program, GLKVertexAttribTexCoord0, "texCoord0" );
    //glBindAttribLocation( program, GLKVertexAttribTexCoord1, "tangent" );
    
    if( !Link() )
    {
        NSLog( @"Failed to link program: %d", program );
        
        glDeleteShader( vs );
        glDeleteShader( fs );
        glDeleteProgram( program );
        
        return 0;
    }
    
    glDetachShader( program, vs );
    glDeleteShader( vs );
    
    glDetachShader( program, fs );
    glDeleteShader( fs );
    
    return true;
}

void SMShader::Bind( void )
{
    glUseProgram( program );
}

void SMShader::ApplyTransform( void )
{
    GLKMatrix4 modelViewMatrix = GLKMatrix4Multiply( gCommon.GetMatrix( SM_VIEW ), gCommon.GetMatrix( SM_MODEL ) );
    GLKMatrix4 modelViewProjectionMatrix = GLKMatrix4Multiply( gCommon.GetMatrix( SM_PROJECTION ), modelViewMatrix );
    GLKMatrix3 normalMatrix = GLKMatrix3InvertAndTranspose( GLKMatrix4GetMatrix3( modelViewMatrix ), NULL );
    
    glUseProgram( program );
    
    glUniformMatrix4fv( GetUniformLocation( "modelMatrix" ), 1, GL_FALSE, gCommon.GetMatrix( SM_MODEL ).m );
    glUniformMatrix4fv( GetUniformLocation( "viewMatrix" ), 1, GL_FALSE, gCommon.GetMatrix( SM_VIEW ).m );
    glUniformMatrix4fv( GetUniformLocation( "projectionMatrix" ), 1, GL_FALSE, gCommon.GetMatrix( SM_PROJECTION ).m );
    
    glUniformMatrix4fv( GetUniformLocation( "modelViewMatrix" ), 1, GL_FALSE, modelViewMatrix.m );
    glUniformMatrix4fv( GetUniformLocation( "modelViewProjectionMatrix" ), 1, GL_FALSE, modelViewProjectionMatrix.m );
    glUniformMatrix3fv( GetUniformLocation( "normalMatrix" ), 1, GL_FALSE, normalMatrix.m );
}

const GLuint SMShader::GetUniformLocation( const char * name )
{
    if( !uniforms[ name ] )
    {
        glUseProgram( program );
        uniforms[ name ] = glGetUniformLocation( program, name );
    }
    return uniforms[ name ];
}

void SMShader::Uniform1( const char * name, GLfloat x )
{
    glUseProgram( program );
    glUniform1f( GetUniformLocation( name ), x );
}

void SMShader::Uniform1( const char * name, GLint x )
{
    glUseProgram( program );
    glUniform1i( GetUniformLocation( name ), x );
}

void SMShader::Uniform2( const char * name, const GLfloat * v )
{
    glUseProgram( program );
    glUniform2fv( GetUniformLocation( name ), 1, v );
}

void SMShader::Uniform3( const char * name, const GLfloat * v )
{
    glUseProgram( program );
    glUniform3fv( GetUniformLocation( name ), 1, v );
}

void SMShader::Uniform4( const char * name, const GLfloat * v )
{
    glUseProgram( program );
    glUniform4fv( GetUniformLocation( name ), 1, v );
}

void SMShader::UniformMatrix3( const char * name, const GLfloat * v )
{
    glUseProgram( program );
    glUniformMatrix3fv( GetUniformLocation( name ), 1, GL_FALSE, v );
}

void SMShader::UniformMatrix4( const char * name, const GLfloat * v )
{
    glUseProgram( program );
    glUniformMatrix4fv( GetUniformLocation( name ), 1, GL_FALSE, v );
}

const bool SMShader::Compile( GLuint * shader, GLenum type, NSString * file )
{
    GLint status;
    const GLchar * source;
    
    source = ( GLchar * )[ [ NSString stringWithContentsOfFile : file encoding : NSUTF8StringEncoding error : nil ] UTF8String ];
    if( !source )
    {
        NSLog( @"Failed to load vertex shader" );
        return NO;
    }
    
    *shader = glCreateShader( type );
    glShaderSource( *shader, 1, &source, NULL );
    glCompileShader( *shader );
    
#ifdef DEBUG
    GLint logLength;
    glGetShaderiv( *shader, GL_INFO_LOG_LENGTH, &logLength );
    if( logLength > 0 )
    {
        GLchar * log = ( GLchar * )malloc( logLength );
        glGetShaderInfoLog( *shader, logLength, &logLength, log );
        NSLog( @"Shader compile log:\n%s", log );
        free( log );
    }
#endif
    
    glGetShaderiv( *shader, GL_COMPILE_STATUS, &status );
    if( !status )
    {
        glDeleteShader( *shader );
        return false;
    }
    
    return true;
}

const bool SMShader::Link( void )
{
    GLint status;
    glLinkProgram( program );
    
#ifdef DEBUG
    GLint logLength;
    glGetProgramiv( program, GL_INFO_LOG_LENGTH, &logLength );
    if( logLength > 0 )
    {
        GLchar * log = ( GLchar * )malloc( logLength );
        glGetProgramInfoLog( program, logLength, &logLength, log );
        NSLog( @"Program link log:\n%s", log );
        free( log );
    }
#endif
    
    glGetProgramiv( program, GL_LINK_STATUS, &status );
    if( !status )
        return false;
    
    return true;
}