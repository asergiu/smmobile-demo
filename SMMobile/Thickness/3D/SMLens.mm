//
//  SSLens.cpp
//  SalesStar
//
//  Created by Dgut on 01.02.13.
//  Copyright (c) 2013 Dgut. All rights reserved.
//

#include "SMLens.h"
#include "SMCommon.h"
#include "SMBuffer.h"
#include "SMShader.h"

#include <vector>

#import "SMThickness3DView.h"

using namespace std;

SMLens::SMLens( bool left ) :
    front   ( 0 ),
    rear    ( 0 ),
    outer   ( 0 ),
    inner   ( 0 ),
    white   ( 0 ),
    left    ( left )
{
}

SMLens::~SMLens( void )
{
    delete front;
    delete rear;
    delete outer;
    delete inner;
}

void SMLens::Init( void )
{
    if( !front )
        front = new SMMesh();
    if( !rear )
        rear = new SMMesh();
    if( !outer )
        outer = new SMMesh();
    if( !inner )
        inner = new SMMesh();
    
    if( !white )
        white = thickness3DGetTexture( "white.png" );
}

#define I( x ) ( ( ( x ) + count ) % count )

void SMLens::SetData( const float * x, const float * y, const float * front, const float * rear, int count )
{
    vector< SMMesh::Vertex > vertices;
    
    int firstIndex = 0;
    for( int i = 1; i < count; i++ )
        if( y[ i ] < y[ firstIndex ] )
            firstIndex = i;
    
    float minX = FLT_MAX;
    
    for( int i = 0; i < count; i++ )
    {
        if( minX > x[ i ] - y[ i ] )
        {
            minX = x[ i ] - y[ i ];
            shift = ( front[ i ] + rear[ i ] ) / 2.f;
        }
    }
    
    //NSLog( @"shift %f minX %f", shift, minX );
    
    /*for( int i = 0; i < count; i++ )
    {
        if( i == firstIndex || i - 1 == firstIndex )
            continue;
        
        SSMesh::Vertex a, b, c;
        
        a.position = GLKVector3Make( ( left ? 1.f : -1.f ) * x[ firstIndex ], y[ firstIndex ], front[ firstIndex ] );
        b.position = GLKVector3Make( ( left ? 1.f : -1.f ) * x[ I( i - 1 ) ], y[ I( i - 1 ) ], front[ I( i - 1 ) ] );
        c.position = GLKVector3Make( ( left ? 1.f : -1.f ) * x[ i ], y[ i ], front[ i ] );
        
        vertices.push_back( a );
        
        if( !left )
        {
            vertices.push_back( c );
            vertices.push_back( b );
        }
        else
        {
            vertices.push_back( b );
            vertices.push_back( c );
        }
    }
    
    SSLens::front->SetVertices( &vertices.front(), vertices.size() );
    
    vertices.clear();
    
    for( int i = 0; i < count; i++ )
    {
        if( i == firstIndex || i - 1 == firstIndex )
            continue;
        
        SSMesh::Vertex a, b, c;
        
        a.position = GLKVector3Make( ( left ? 1.f : -1.f ) * x[ firstIndex ], y[ firstIndex ], rear[ firstIndex ] );
        b.position = GLKVector3Make( ( left ? 1.f : -1.f ) * x[ I( i - 1 ) ], y[ I( i - 1 ) ], rear[ I( i - 1 ) ] );
        c.position = GLKVector3Make( ( left ? 1.f : -1.f ) * x[ i ], y[ i ], rear[ i ] );
        
        vertices.push_back( a );
        
        if( !left )
        {
            vertices.push_back( b );
            vertices.push_back( c );
        }
        else
        {
            vertices.push_back( c );
            vertices.push_back( b );
        }
    }
    
    SSLens::rear->SetVertices( &vertices.front(), vertices.size() );*/
    
    vertices.clear();
    
    for( int i = 0; i < count; i++ )
    {
        SMMesh::Vertex a, b, c, d;
        
        a.position = GLKVector3Make( ( left ? 1.f : -1.f ) * x[ i ], y[ i ], front[ i ] );
        b.position = GLKVector3Make( ( left ? 1.f : -1.f ) * x[ i ], y[ i ], rear[ i ] );
        c.position = GLKVector3Make( ( left ? 1.f : -1.f ) * x[ I( i + 1 ) ], y[ I( i + 1 ) ], front[ I( i + 1 ) ] );
        d.position = GLKVector3Make( ( left ? 1.f : -1.f ) * x[ I( i + 1 ) ], y[ I( i + 1 ) ], rear[ I( i + 1 ) ] );
        
        GLKVector2 ap = GLKVector2Normalize( GLKVector2Make( x[ I( i - 1 ) ] - x[ i ], y[ I( i - 1 ) ] - y[ i ] ) );
        GLKVector2 an = GLKVector2Normalize( GLKVector2Make( x[ I( i + 1 ) ] - x[ i ], y[ I( i + 1 ) ] - y[ i ] ) );
        
        a.normal = b.normal = GLKVector3Normalize( GLKVector3Make( ( left ? 1.f : -1.f ) * -( ap.x + an.x ), -( ap.y + an.y ), 0 ) );
        
        GLKVector2 cp = GLKVector2Normalize( GLKVector2Make( x[ i ] - x[ I( i + 1 ) ], y[ i ] - y[ I( i + 1 ) ] ) );
        GLKVector2 cn = GLKVector2Normalize( GLKVector2Make( x[ I( i + 2 ) ] - x[ I( i + 1 ) ], y[ I( i + 2 ) ] - y[ I( i + 1 ) ] ) );
        
        c.normal = d.normal = GLKVector3Normalize( GLKVector3Make( ( left ? 1.f : -1.f ) * -( cp.x + cn.x ), -( cp.y + cn.y ), 0 ) );
        
        if( left )
        {
            vertices.push_back( a );
            vertices.push_back( c );
            vertices.push_back( b );
            
            vertices.push_back( b );
            vertices.push_back( c );
            vertices.push_back( d );
        }
        else
        {
            vertices.push_back( a );
            vertices.push_back( b );
            vertices.push_back( c );
            
            vertices.push_back( b );
            vertices.push_back( d );
            vertices.push_back( c );
        }
    }
    
    SMLens::outer->SetVertices( &vertices.front(), vertices.size() );
    
    vertices.clear();
    
    for( int i = 0; i < count; i++ )
    {
        SMMesh::Vertex a, b, c, d;
        
        a.position = GLKVector3Make( ( left ? 1.f : -1.f ) * x[ i ], y[ i ], front[ i ] );
        b.position = GLKVector3Make( ( left ? 1.f : -1.f ) * x[ i ], y[ i ], rear[ i ] );
        c.position = GLKVector3Make( ( left ? 1.f : -1.f ) * x[ I( i + 1 ) ], y[ I( i + 1 ) ], front[ I( i + 1 ) ] );
        d.position = GLKVector3Make( ( left ? 1.f : -1.f ) * x[ I( i + 1 ) ], y[ I( i + 1 ) ], rear[ I( i + 1 ) ] );
        
        GLKVector2 ap = GLKVector2Normalize( GLKVector2Make( x[ I( i - 1 ) ] - x[ i ], y[ I( i - 1 ) ] - y[ i ] ) );
        GLKVector2 an = GLKVector2Normalize( GLKVector2Make( x[ I( i + 1 ) ] - x[ i ], y[ I( i + 1 ) ] - y[ i ] ) );
        
        a.normal = b.normal = GLKVector3Normalize( GLKVector3Make( ( left ? 1.f : -1.f ) * ( ap.x + an.x ), ( ap.y + an.y ), 0 ) );
        
        GLKVector2 cp = GLKVector2Normalize( GLKVector2Make( x[ i ] - x[ I( i + 1 ) ], y[ i ] - y[ I( i + 1 ) ] ) );
        GLKVector2 cn = GLKVector2Normalize( GLKVector2Make( x[ I( i + 2 ) ] - x[ I( i + 1 ) ], y[ I( i + 2 ) ] - y[ I( i + 1 ) ] ) );
        
        c.normal = d.normal = GLKVector3Normalize( GLKVector3Make( ( left ? 1.f : -1.f ) * ( cp.x + cn.x ), ( cp.y + cn.y ), 0 ) );
        
        if( !left )
        {
            vertices.push_back( a );
            vertices.push_back( c );
            vertices.push_back( b );
            
            vertices.push_back( b );
            vertices.push_back( c );
            vertices.push_back( d );
        }
        else
        {
            vertices.push_back( a );
            vertices.push_back( b );
            vertices.push_back( c );
            
            vertices.push_back( b );
            vertices.push_back( d );
            vertices.push_back( c );
        }
    }
    
    SMLens::inner->SetVertices( &vertices.front(), vertices.size() );
}

void SMLens::SetSurfaceData( const float * x, const float * y, const float * front, const float * rear, const GLKVector3 * normalFront, const GLKVector3 * normalRear, int count, float step )
{
    vector< SMMesh::Vertex > frontVertices;
    vector< SMMesh::Vertex > rearVertices;
    
    vector< short > frontIndices;
    vector< short > rearIndices;
    
    struct SurfaceVertex
    {
        GLKVector2 position;
        int index;
    };
    
    std::vector< SurfaceVertex > surfaceVertices;

    for( int i = 0; i < count; i++ )
    {
        SMMesh::Vertex vertex;
        //vertex.normal = GLKVector3Make( 0.f, 0.f, 0.f );
        
        vertex.position = GLKVector3Make( x[ i ], y[ i ], front[ i ] );
        vertex.normal = normalFront[ i ];
        vertex.texCoord0.x = normalRear[ i ].x;
        vertex.texCoord0.y = normalRear[ i ].y;
        frontVertices.push_back( vertex );
        
        vertex.position = GLKVector3Make( x[ i ], y[ i ], rear[ i ] );
        vertex.normal = normalRear[ i ];
        vertex.texCoord0.x = normalFront[ i ].x;
        vertex.texCoord0.y = normalFront[ i ].y;
        rearVertices.push_back( vertex );
        
        SurfaceVertex surfaceVertex;
        surfaceVertex.position = GLKVector2Make( x[ i ], y[ i ] );
        surfaceVertex.index = i;
        surfaceVertices.push_back( surfaceVertex );
    }
    
    map< pair< int, int >, vector< SurfaceVertex > > surface;
    for( int i = 0; i < count; i++ )
    {
        int x = floor( surfaceVertices[ i ].position.x / step );
        int y = floor( surfaceVertices[ i ].position.y / step );
        
        surface[ pair< int, int >( x, y ) ].push_back( surfaceVertices[ i ] );
        
        if( abs( x * step - surfaceVertices[ i ].position.x ) < 0.001f )
            surface[ pair< int, int >( x - 1, y ) ].push_back( surfaceVertices[ i ] );
        if( abs( y * step - surfaceVertices[ i ].position.y ) < 0.001f )
            surface[ pair< int, int >( x, y - 1 ) ].push_back( surfaceVertices[ i ] );
        if( abs( x * step - surfaceVertices[ i ].position.x ) < 0.001f && abs( y * step - surfaceVertices[ i ].position.y ) < 0.001f )
            surface[ pair< int, int >( x - 1, y - 1 ) ].push_back( surfaceVertices[ i ] );
    }
    
    for( map< pair< int, int >, vector< SurfaceVertex > >::iterator it = surface.begin(); it != surface.end(); it++ )
    {
        vector< SurfaceVertex > & quad = it->second;
        
        if( quad.size() < 3 )
            continue;
        
        GLKVector2 center = { 0 };
        
        for( int i = 0; i < quad.size(); i++ )
            center = GLKVector2Add( center, quad[ i ].position );
        center = GLKVector2DivideScalar( center, quad.size() );
        
        struct Predicate
        {
            GLKVector2 center;
            
            bool less( const GLKVector2 & a, const GLKVector2 & b ) const
            {
                if (a.x - center.x >= 0 && b.x - center.x < 0)
                    return true;
                if (a.x - center.x < 0 && b.x - center.x >= 0)
                    return false;
                if (a.x - center.x == 0 && b.x - center.x == 0) {
                    if (a.y - center.y >= 0 || b.y - center.y >= 0)
                        return a.y > b.y;
                    return b.y > a.y;
                }
                
                // compute the cross product of vectors (center -> a) x (center -> b)
                int det = (a.x - center.x) * (b.y - center.y) - (b.x - center.x) * (a.y - center.y);
                if (det < 0)
                    return true;
                if (det > 0)
                    return false;
                
                // points a and b are on the same line from the center
                // check which point is closer to the center
                int d1 = (a.x - center.x) * (a.x - center.x) + (a.y - center.y) * (a.y - center.y);
                int d2 = (b.x - center.x) * (b.x - center.x) + (b.y - center.y) * (b.y - center.y);
                return d1 > d2;
            }
            
            bool operator()( SurfaceVertex const & a, SurfaceVertex const & b ) const
            {                
                return less( a.position, b.position );
            }
        };
        
        Predicate predicate = { center };
        
        sort( quad.begin(), quad.end(), predicate );
        
        for( int j = 2; j < quad.size(); j++ )
        {
            /*static const float SKIP_DISTANCE = 1.f;
            
            bool skip = false;
            
            if(
               GLKVector2Distance( surfaceVertices[ quad[ 0 ].index ].position, surfaceVertices[ quad[ j - 1 ].index ].position ) < SKIP_DISTANCE ||
               GLKVector2Distance( surfaceVertices[ quad[ 0 ].index ].position, surfaceVertices[ quad[ j ].index ].position ) < SKIP_DISTANCE ||
               GLKVector2Distance( surfaceVertices[ quad[ j ].index ].position, surfaceVertices[ quad[ j - 1 ].index ].position ) < SKIP_DISTANCE )
            {
                NSLog( @"skip" );
                //skip = true;
            }*/
            
            {
                /*SMMesh::Vertex & a = frontVertices[ quad[ 0 ].index ];
                SMMesh::Vertex & b = frontVertices[ quad[ j - 1 ].index ];
                SMMesh::Vertex & c = frontVertices[ quad[ j ].index ];
                
                GLKVector3 ab = GLKVector3Subtract( b.position, a.position );
                GLKVector3 ac = GLKVector3Subtract( c.position, a.position );
                
                GLKVector3 normal = GLKVector3Normalize( GLKVector3CrossProduct( ac, ab ) );
                
                a.normal = GLKVector3Add( a.normal, normal );
                b.normal = GLKVector3Add( b.normal, normal );
                c.normal = GLKVector3Add( c.normal, normal );*/
                
                frontIndices.push_back( quad[ 0 ].index );
                
                if( left )
                {
                    frontIndices.push_back( quad[ j ].index );
                    frontIndices.push_back( quad[ j - 1 ].index );
                }
                else
                {
                    frontIndices.push_back( quad[ j - 1 ].index );
                    frontIndices.push_back( quad[ j ].index );
                }
            }
            
            {
                /*SMMesh::Vertex & a = rearVertices[ quad[ 0 ].index ];
                SMMesh::Vertex & b = rearVertices[ quad[ j - 1 ].index ];
                SMMesh::Vertex & c = rearVertices[ quad[ j ].index ];
                
                GLKVector3 ab = GLKVector3Subtract( b.position, a.position );
                GLKVector3 ac = GLKVector3Subtract( c.position, a.position );
                
                GLKVector3 normal = GLKVector3Normalize( GLKVector3CrossProduct( ab, ac ) );
                
                a.normal = GLKVector3Add( a.normal, normal );
                b.normal = GLKVector3Add( b.normal, normal );
                c.normal = GLKVector3Add( c.normal, normal );*/
                
                rearIndices.push_back( quad[ 0 ].index );
                
                if( left )
                {
                    rearIndices.push_back( quad[ j - 1 ].index );
                    rearIndices.push_back( quad[ j ].index );
                }
                else
                {
                    rearIndices.push_back( quad[ j ].index );
                    rearIndices.push_back( quad[ j - 1 ].index );
                }
            }
        }
    }
    
    /*for( int i = 0; i < count; i++ )
    {
        frontVertices[ i ].normal = GLKVector3Normalize( frontVertices[ i ].normal );
        rearVertices[ i ].normal = GLKVector3Normalize( rearVertices[ i ].normal );
        
        frontVertices[ i ].texCoord0.x = rearVertices[ i ].normal.x;
        frontVertices[ i ].texCoord0.y = rearVertices[ i ].normal.y;
        
        rearVertices[ i ].texCoord0.x = frontVertices[ i ].normal.x;
        rearVertices[ i ].texCoord0.y = frontVertices[ i ].normal.y;
    }*/

    for( int i = 0; i < count; i++ )
    {
        frontVertices[ i ].position = GLKVector3DivideScalar( frontVertices[ i ].position, 1000 );
        rearVertices[ i ].position = GLKVector3DivideScalar( rearVertices[ i ].position, 1000 );
        
        if( !left )
        {
            frontVertices[ i ].position.x *= -1;
            rearVertices[ i ].position.x *= -1;
            
            frontVertices[ i ].normal.x *= -1;
            rearVertices[ i ].normal.x *= -1;
            
            frontVertices[ i ].texCoord0.x *= -1;
            rearVertices[ i ].texCoord0.x *= -1;
        }
    }
    
    SMLens::front->SetVertices( &frontVertices.front(), frontVertices.size() );
    SMLens::front->SetIndices( &frontIndices.front(), frontIndices.size() );
    
    SMLens::rear->SetVertices( &rearVertices.front(), rearVertices.size() );
    SMLens::rear->SetIndices( &rearIndices.front(), rearIndices.size() );
}

void SMLens::Draw( float offset, float backgroundOffset ) const
{
    gCommon.PushMatrix( SM_MODEL );
    gCommon.Translate( SM_MODEL, 0, 0, -shift );
    
    GLKVector3 center = outer->Center();
    GLKMatrix4 rotation = GLKMatrix4Identity;
    
    /*rotation = GLKMatrix4TranslateWithVector3( rotation, center );
    rotation = GLKMatrix4Translate( rotation, -0.0007 * ( left ? 1 : -1 ), 0, 0 );
    rotation = GLKMatrix4Rotate( rotation, -0.07 * ( left ? 1 : -1 ), 0, 0, 1 );
    rotation = GLKMatrix4Rotate( rotation, 0.255 + 0, 1, 0, 0 );
    rotation = GLKMatrix4Rotate( rotation, ( 0.165 + 0 ) * ( left ? 1 : -1 ), 0, 1, 0 );
    rotation = GLKMatrix4TranslateWithVector3( rotation, GLKVector3Negate( center ) );*/
    rotation = GLKMatrix4TranslateWithVector3( rotation, center );
    rotation = GLKMatrix4Translate( rotation, -0.0007 * ( left ? 1 : -1 ), 0, 0 );
    rotation = GLKMatrix4Rotate( rotation, -0.07 * ( left ? 1 : -1 ), 0, 0, 1 );
    rotation = GLKMatrix4Rotate( rotation, 0.198, 1, 0, 0 );
    rotation = GLKMatrix4Rotate( rotation, ( 0.165 + 0 ) * ( left ? 1 : -1 ), 0, 1, 0 );
    rotation = GLKMatrix4TranslateWithVector3( rotation, GLKVector3Negate( center ) );
    
    gCommon.MultMatrix( SM_MODEL, rotation );
    
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( white.target, white.name );
    thickness3DGetShader( "ThicknessOneLight" )->ApplyTransform();
    outer->Draw();
    //inner->Draw();
    
    GLKTextureInfo * cubemap = thickness3DGetCubemap( "environment.png" );
    
    glActiveTexture( GL_TEXTURE3 );
    glBindTexture( cubemap.target, cubemap.name );
    glActiveTexture( GL_TEXTURE2 );
    thickness3DGetBackgroundBuffer()->BindTexture();
    glActiveTexture( GL_TEXTURE1 );
    thickness3DGetInternalBuffer()->BindTexture();
    glActiveTexture( GL_TEXTURE0 );
    thickness3DGetBuffer()->BindTexture();
    
    GLKMatrix4 inverseViewMatrix = GLKMatrix4Invert( gCommon.GetMatrix( SM_VIEW ), 0 );
    
    thickness3DGetShader( "ThicknessLens" )->Uniform1( "specularFactor", 0.7f );
    thickness3DGetShader( "ThicknessLens" )->Uniform1( "specularPower", 60.f );
    thickness3DGetShader( "ThicknessLens" )->Uniform1( "index", 1.5f );
    thickness3DGetShader( "ThicknessLens" )->Uniform1( "offset", offset );
    thickness3DGetShader( "ThicknessLens" )->Uniform1( "offsetBackground", backgroundOffset );
    thickness3DGetShader( "ThicknessLens" )->Uniform1( "internal", 1 );
    thickness3DGetShader( "ThicknessLens" )->Uniform1( "background", 2 );
    thickness3DGetShader( "ThicknessLens" )->Uniform1( "cubemap", 3 );
    thickness3DGetShader( "ThicknessLens" )->UniformMatrix4( "inverseViewMatrix", inverseViewMatrix.m );
    thickness3DGetShader( "ThicknessLens" )->ApplyTransform();
    
    //glDisable( GL_CULL_FACE );
    
    front->Draw();
    rear->Draw();
    
    //glEnable( GL_CULL_FACE );
    
    gCommon.PopMatrix( SM_MODEL );
}

void SMLens::DrawRefraction( void ) const
{
    gCommon.PushMatrix( SM_MODEL );
    gCommon.Translate( SM_MODEL, 0, 0, -shift );
    
    GLKVector3 center = outer->Center();
    GLKMatrix4 rotation = GLKMatrix4Identity;
    
    /*rotation = GLKMatrix4TranslateWithVector3( rotation, center );
    rotation = GLKMatrix4Translate( rotation, -0.0007 * ( left ? 1 : -1 ), 0, 0 );
    rotation = GLKMatrix4Rotate( rotation, -0.07 * ( left ? 1 : -1 ), 0, 0, 1 );
    rotation = GLKMatrix4Rotate( rotation, 0.255 + 0, 1, 0, 0 );
    rotation = GLKMatrix4Rotate( rotation, ( 0.165 + 0 ) * ( left ? 1 : -1 ), 0, 1, 0 );
    rotation = GLKMatrix4TranslateWithVector3( rotation, GLKVector3Negate( center ) );*/
    rotation = GLKMatrix4TranslateWithVector3( rotation, center );
    rotation = GLKMatrix4Translate( rotation, -0.0007 * ( left ? 1 : -1 ), 0, 0 );
    rotation = GLKMatrix4Rotate( rotation, -0.07 * ( left ? 1 : -1 ), 0, 0, 1 );
    rotation = GLKMatrix4Rotate( rotation, 0.198, 1, 0, 0 );
    rotation = GLKMatrix4Rotate( rotation, ( 0.165 + 0 ) * ( left ? 1 : -1 ), 0, 1, 0 );
    rotation = GLKMatrix4TranslateWithVector3( rotation, GLKVector3Negate( center ) );
    
    gCommon.MultMatrix( SM_MODEL, rotation );
    
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( white.target, white.name );
    
    for( int i = 0; i < 5; i++ )
    {
        gCommon.PushMatrix( SM_MODEL );
        
        gCommon.Translate( SM_MODEL, center.x, center.y, center.z );
        gCommon.Scale( SM_MODEL, 1.f - 0.2 * i );
        gCommon.Translate( SM_MODEL, -center.x, -center.y, -center.z );
        
        thickness3DGetShader( "ThicknessOneLight" )->ApplyTransform();
        inner->Draw();
        
        gCommon.PopMatrix( SM_MODEL );
    }
    
    gCommon.PopMatrix( SM_MODEL );
}

const GLKVector3 SMLens::Center( void ) const
{
    return outer->Center();
}