//
//  ThiknessGraficView.m
//  thickness
//
//  Created by Sergey Vishnyov on 01.05.12.
//

#import "ThiknessGraficView.h"
#import "DataSource.h"

@interface ThiknessGraficView()
{
    UIColor * topPattern;
    UIColor * bottomPattern;
}

@end

@implementation ThiknessGraficView

-( id )initWithFrame : ( CGRect )frame
{
    self = [ super initWithFrame : frame ];
    if( self )
    {
        topPattern = [ UIColor colorWithPatternImage : [ UIImage imageNamed : @"top_bg" ] ];
        bottomPattern = [ UIColor colorWithPatternImage : [ UIImage imageNamed : @"bottom_bg" ] ];
    }
    return self;
}

-( void )dealloc
{
}

-( void )bindPatternColor : ( UIColor * )patternColor context : ( CGContextRef )ctx
{
    CGColorRef color = [patternColor CGColor];
    CGColorSpaceRef colorSpace = CGColorGetColorSpace(color);
    CGContextSetFillColorSpace(ctx, colorSpace);
    if (CGColorSpaceGetModel(colorSpace) == kCGColorSpaceModelPattern)
    {
        CGFloat alpha = 1.0f;
        CGContextSetFillPattern(ctx, CGColorGetPattern(color), &alpha);
    }
    else
    {
        CGContextSetFillColor(ctx, CGColorGetComponents(color));
    }
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGFloat centerAxis = 0.5 + self.frame.size.width / 2;
    CGFloat koefY = 7.4f;
    CGFloat koefX = 3.7f;
    
    // Surfaces
    struct SurfaceData left2DSurface[3];
    struct SurfaceData right2DSurface[3];
    // Left surface
    left2DSurface[0] = [ MainDataSource getSurfaceDataForDraw2D : 0 forLeftSide : YES ];
    left2DSurface[1] = [ MainDataSource getSurfaceDataForDraw2D : 1 forLeftSide : YES ];
    left2DSurface[2] = [ MainDataSource getSurfaceDataForDraw2D : 3 forLeftSide : YES ];
    // Right surface
    right2DSurface[0] = [ MainDataSource getSurfaceDataForDraw2D : 0 forLeftSide : NO ];
    right2DSurface[1] = [ MainDataSource getSurfaceDataForDraw2D : 1 forLeftSide : NO ];
    right2DSurface[2] = [ MainDataSource getSurfaceDataForDraw2D : 3 forLeftSide : NO ];

    bool bDrawLeft = (left2DSurface[1].z[0] > 0.);
    bool bDrawRight = (right2DSurface[1].z[0] > 0.);
    
    const int nRightSize = 1 + right2DSurface[0].diam / 5 + ((right2DSurface[0].diam % 5)? 1 : 0);
    const int nLeftSize = 1 + left2DSurface[0].diam / 5 + ((left2DSurface[0].diam % 5)? 1 : 0);
    CGFloat currentX = 0;
    CGFloat currentY = 0;
    CGFloat rightMaxX = centerAxis + koefX * 2 * right2DSurface[0].x[nRightSize - 1];
    CGFloat leftMaxX = centerAxis - koefX * 2 * left2DSurface[0].x[nLeftSize - 1];

    //UIColor *intColor = [UIColor colorWithRed:197/256.0 green:234/256.0 blue:250/256.0 alpha:0.5f];
    UIColor *strokeColor = [UIColor colorWithRed:184/255.0 green:206/255.0 blue:227/255.0 alpha:1];
    
    // Right
    if (bDrawRight) {
        CGContextMoveToPoint(context, centerAxis, 0);
        
        for (int i = 1; i < nRightSize; i++)
        {
            currentX = right2DSurface[0].x[i] * 2.;
            currentY = right2DSurface[0].z[i];
            CGContextAddLineToPoint(context, centerAxis + currentX * koefX, currentY * koefY);
        }
        currentY = right2DSurface[1].z[nRightSize - 1];
        CGContextAddLineToPoint(context, rightMaxX, currentY * koefY);
        for (int i = nRightSize - 2; i >= 0; i--)
        {
            currentX = right2DSurface[1].x[i] * 2.;
            currentY = right2DSurface[1].z[i];
            CGContextAddLineToPoint(context, centerAxis + currentX * koefX, currentY * koefY);
        }
        
        CGContextAddLineToPoint(context, centerAxis, 0);
        
        //CGContextSetFillColorWithColor(context, intColor.CGColor);
        [ self bindPatternColor : topPattern context : context ];
        if (currentY!=0) CGContextSetLineWidth(context, 1.0);
        else CGContextSetLineWidth(context, 0.0);
        CGContextSetStrokeColorWithColor(context, strokeColor.CGColor);
        
        CGContextDrawPath(context, kCGPathFillStroke);
    }
    
    // Left
    if (bDrawLeft) {
        CGContextMoveToPoint(context, centerAxis, 0);
        
        for (int i = 1; i < nLeftSize; i++)
        {
            currentX = left2DSurface[0].x[i] * 2.;
            currentY = left2DSurface[0].z[i];
            CGContextAddLineToPoint(context, centerAxis - currentX * koefX, currentY * koefY);
        }
        currentY = left2DSurface[1].z[nLeftSize - 1];
        CGContextAddLineToPoint(context, leftMaxX, currentY * koefY);
        for (int i = nLeftSize - 2; i >= 0; i--)
        {
            currentX = left2DSurface[1].x[i] * 2.;
            currentY = left2DSurface[1].z[i];
            CGContextAddLineToPoint(context, centerAxis - currentX * koefX, currentY * koefY);
        }
        
        CGContextAddLineToPoint(context, centerAxis, 0);
        [ self bindPatternColor : topPattern context : context ];
        //CGContextSetFillColorWithColor(context, intColor.CGColor);
        
        if (currentY!=0) CGContextSetLineWidth(context, 1.0);
        else CGContextSetLineWidth(context, 0.0);
        
        CGContextSetStrokeColorWithColor(context, strokeColor.CGColor);
        CGContextDrawPath(context, kCGPathFillStroke);
    }
    
    //---------
    //intColor = [ UIColor colorWithRed : 197 / 255. green : 234 / 255. blue : 230 / 256. alpha : 0.5f ];
    //strokeColor = [ UIColor colorWithRed : 22 / 255. green : 148 / 255. blue : 205 / 256. alpha : 1.f ];
    
    float alpha = 1.f;
    
    //CGContextSetFillColorWithColor( context, intColor.CGColor );
    //CGContextSetFillPattern( context, pattern, &alpha );
    [ self bindPatternColor : bottomPattern context : context ];
    CGContextSetStrokeColorWithColor( context, strokeColor.CGColor );
    CGContextSetLineWidth( context, 1.f );
    CGContextSetLineCap( context, kCGLineCapRound );
    CGContextSetLineJoin( context, kCGLineJoinRound );
    
    if (bDrawRight) {
        for (int i = 0; i < nRightSize; i++)
        {
            currentX = right2DSurface[2].x[i] * 2.;
            currentY = right2DSurface[2].z[i];
            ( i ? CGContextAddLineToPoint : CGContextMoveToPoint )( context, centerAxis + currentX * koefX, currentY * koefY );
        }
        for (int i = nRightSize - 1; i >= 0; i--)
        {
            currentX = right2DSurface[1].x[i] * 2.;
            currentY = right2DSurface[1].z[i];
            CGContextAddLineToPoint(context, centerAxis + currentX * koefX, currentY * koefY);
        }
        
        currentY = right2DSurface[2].z[0];
        CGContextAddLineToPoint( context, centerAxis, currentY * koefY );
        
        CGContextDrawPath( context, kCGPathFillStroke );
    }
    
    if (bDrawLeft) {
        for (int i = 0; i < nLeftSize; i++)
        {
            currentX = left2DSurface[2].x[i] * 2.;
            currentY = left2DSurface[2].z[i];
            ( i ? CGContextAddLineToPoint : CGContextMoveToPoint )( context, centerAxis - currentX * koefX, currentY * koefY );
        }
        for (int i = nLeftSize - 1; i >= 0; i--)
        {
            currentX = left2DSurface[1].x[i] * 2.;
            currentY = left2DSurface[1].z[i];
            CGContextAddLineToPoint(context, centerAxis - currentX * koefX, currentY * koefY);
        }
        currentY = left2DSurface[2].z[0];
        CGContextAddLineToPoint( context, centerAxis, currentY * koefY );
        
        CGContextDrawPath( context, kCGPathFillStroke );
    }
    
    //[ self bindPatternColor : bottomPattern context : context ];
    //CGContextFillRect( context, rect );
}

@end
