#pragma once

#include "WSLens/IWSLLens.h"
#include "WSLens/ObjectPointer.h"
#include "SurfaceData.h"

typedef enum SurfaceDataType		// Lens surface data types
{
    sdForward = 0,			// forward surface
    sdBack = 1,				// Back surface
    sdAstigmaticBack = 2,	// Back astigmatic curve
    sdLineBack = 3,         // Back 2D line B
    sdLineBackA = 4         // Back 2D line A
} SurfaceDataType;

class CMatrixEngine : public ILensEngine
{
public:
	CMatrixEngine();
    void SetMaxDiam(long diam);
    bool SetSurfaceData(SurfaceData * pdR1, SurfaceData * pdR2, SurfaceData * pdR3);
    bool CalibrateSurfaceData(SurfaceData * pdR1, SurfaceData * pdR2, SurfaceData * pdR3, long diam);
    SurfaceData GetSurfaceData(SurfaceDataType sdType);

	// ILensEngine interface
	bool CalcSurface(ILens * LensObject, ILensSurface * LensSurface);
	bool CalcWeight(ILens * LensObject, double* ResultWeight);
	bool CalcThickness(ILens * LensObject, double* ResultCenterThickness, double* ResultMinEdgeThickness, double* ResultMaxEdgeThickness);
	bool CalcUVProtection(ILens * LensObject, double* ResultUVProtection);
	unsigned long GetSupportedMethods();
    void GetDepth(ILens* LensObject, double x, double y, double& rear, double& front);
	string GetLastErrorMessage();
	ILensEngine * Clone();

	// IObjectBase interface
	unsigned long AddRef();
	unsigned long Release();
	bool WriteToSteam(ostream * pOutStream);
	bool ReadFromSteam(istream * pInStream);

protected:
    long maxDiam;
    bool bDataPresent;
	double A1, A2, A3, B1, B2, B3, D1, D2, D3;
    double R1, R2, R3;
	struct SurfaceData dR1, dR2, dR3, dRB, dRA;
	bool CalcCubicFunction(SurfaceData * data, double& A, double& B, double& D);
	bool CalcSquareFunction(SurfaceData * data, double& R);
    void CalcSurfaceNormanls(double xDer, double yDer, double zDer, double& xN, double& yN, double& zN);
};
