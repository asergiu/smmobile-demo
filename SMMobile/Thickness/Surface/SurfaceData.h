#pragma once

struct SurfaceData
{
	double power;
    double cyl;
    long axis;
    long bAspheric;
    long diam;
    double x[17];
	double z[17];
};
