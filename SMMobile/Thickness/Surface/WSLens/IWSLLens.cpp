#include "stdafx.h"
#include "IWSLLens.h"

#include "ARCoating.h"
#include "Coating.h"
#include "Coatings.h"
#include "ContourPoint.h"
#include "Contour.h"
#include "DString.h"
#include "DStringCollection.h"
#include "LensSurface.h"
#include "MultifocalSegment.h"
#include "MultifocalSegmentCollection.h"
#include "Progressive.h"
#include "TintPoint.h"
#include "Tint.h"
#include "WSLensEngine.h"
#include "WSPicture.h"
#include "Lens.h"

IARCoating * Create_IARCoating()
{
	IARCoating * pIARCoating = NULL;
	CARCoating * pARCoating = CARCoating::CreateObject();
	if(pARCoating)
		pIARCoating = pARCoating->GetInterface();
	return pIARCoating;
}
ICoating * Create_ICoating()
{
	ICoating * pICoating = NULL;
	CCoating * pCoating = CCoating::CreateObject();
	if(pCoating)
		pICoating = pCoating->GetInterface();
	return pICoating;
}
ICoatings * Create_ICoatings()
{
	ICoatings * pICoatings = NULL;
	CCoatings * pCoatings = CCoatings::CreateObject();
	if(pCoatings)
		pICoatings = pCoatings->GetInterface();
	return pICoatings;
}
IContourPoint * Create_IContourPoint()
{
	IContourPoint * pIContourPoint = NULL;
	CContourPoint * pContourPoint = CContourPoint::CreateObject();
	if(pContourPoint)
		pIContourPoint = pContourPoint->GetInterface();
	return pIContourPoint;

}
IContour * Create_IContour()
{
	IContour * pIContour = NULL;
	CContour * pContour = CContour::CreateObject();
	if(pContour)
		pIContour = pContour->GetInterface();
	return pIContour;
}
IDString * Create_IDString()
{
	IDString * pIDString = NULL;
	CDString * pDString = CDString::CreateObject();
	if(pDString)
		pIDString = pDString->GetInterface();
	return pIDString;
}
IDStringCollection * Create_IDStringCollection()
{
	IDStringCollection * pIDStringCollection = NULL;
	CDStringCollection * pDStringCollection = CDStringCollection::CreateObject();
	if(pDStringCollection)
		pIDStringCollection = pDStringCollection->GetInterface();
	return pIDStringCollection;
}
ILensSurface * Create_ILensSurface()
{
	ILensSurface * pILensSurface = NULL;
	CLensSurface * pLensSurface = CLensSurface::CreateObject();
	if(pLensSurface)
		pILensSurface = pLensSurface->GetInterface();
	return pILensSurface;
}
IMultifocalSegment * Create_IMultifocalSegment()
{
	IMultifocalSegment * pIMultifocalSegment = NULL;
	CMultifocalSegment * pMultifocalSegment = CMultifocalSegment::CreateObject();
	if(pMultifocalSegment)
		pIMultifocalSegment = pMultifocalSegment->GetInterface();
	return pIMultifocalSegment;
}
IMultifocalSegmentCollection * Create_IMultifocalSegmentCollection()
{
	IMultifocalSegmentCollection * pIMultifocalSegmentCollection = NULL;
	CMultifocalSegmentCollection * pMultifocalSegmentCollection = CMultifocalSegmentCollection::CreateObject();
	if(pMultifocalSegmentCollection)
		pIMultifocalSegmentCollection = pMultifocalSegmentCollection->GetInterface();
	return pIMultifocalSegmentCollection;
}
IProgressive * Create_IProgressive()
{
	IProgressive * pIProgressive = NULL;
	CProgressive * pProgressive = CProgressive::CreateObject();
	if(pProgressive)
		pIProgressive = pProgressive->GetInterface();
	return pIProgressive;
}
ITintPoint * Create_ITintPoint()
{
	ITintPoint * pITintPoint = NULL;
	CTintPoint * pTintPoint = CTintPoint::CreateObject();
	if(pTintPoint)
		pITintPoint = pTintPoint->GetInterface();
	return pITintPoint;
}
ITint * Create_ITint()
{
	ITint * pITint = NULL;
	CTint * pTint = CTint::CreateObject();
	if(pTint)
		pITint = pTint->GetInterface();
	return pITint;
}
ILensEngine * Create_ILensEngine()
{
	ILensEngine * pILensEngine = NULL;
	CWSLensEngine * pLensEngine = CWSLensEngine::CreateObject();
	if(pLensEngine)
		pILensEngine = pLensEngine->GetInterface();
	return pILensEngine;
}
IWSPicture * Create_IWSPicture()
{
	IWSPicture * pIWSPicture = NULL;
	CWSPicture * pWSPicture = CWSPicture::CreateObject();
	if(pWSPicture)
		pIWSPicture = pWSPicture->GetInterface();
	return pIWSPicture;
}
ILens * Create_ILens()
{
	ILens * pILens = NULL;
	CLens * pLens = CLens::CreateObject();
	if(pLens)
		pILens = pLens->GetInterface();
	return pILens;
}
