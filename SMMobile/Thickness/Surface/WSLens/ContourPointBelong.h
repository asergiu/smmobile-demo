// ContourPointBelong.h: interface for the CContourPointBelong class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "Massiv.h"
#include <math.h>
#include "Parameters/Asph2.h"
#include "RayTracer.h"
#include "FaceGeom.h"

typedef enum ColorPointMap
{
	ColorLeftContur			= 12,
	ColorRightContur		= 13,
	ColorExternelFon		=  0,
	ColorLeftInternelFon	= 10,
	ColorRightInternelFon	= 11,
	ColorLeftBorder			=  8,
	ColorRightBorder		=  9
}	ColorPointMap;

class CDimension
{
public:
	CDimension(){
		m_picSize = CSize(0, 0);
		m_LeftPupilX = m_LeftPupilY = m_RightPupilX = m_RightPupilY = 0.;
		m_LeftPD = m_RightPD = 0.;
		m_dxPupil = m_dyPupil = 0.;
		m_PD = 0.;

		m_ScaleY = 0.;
		m_MMinPixelByX = 0.;
		m_MMinPixelByY = 0.;

		m_Cosinus = m_Sinus = 0.;

		dim_picSize = CSize(0, 0);
		dim_LeftPupilX = dim_LeftPupilY = dim_RightPupilX = dim_RightPupilY = 0.;
		dim_LeftPD = dim_RightPD = 0.;

		MayBeUsed = FALSE;
	}
	CDimension(CSize picSize, double LeftPD, double RightPD, double LeftPupilX, double LeftPupilY,
		double RightPupilX, double RightPupilY){

		Initialize(picSize, LeftPD, RightPD, LeftPupilX, LeftPupilY, RightPupilX, RightPupilY);
	}
	void Initialize(CSize picSize, double LeftPD, double RightPD, double LeftPupilX, double LeftPupilY,
		double RightPupilX, double RightPupilY){

		if(MayBeUsed == TRUE && CompareData(picSize, LeftPD, RightPD, LeftPupilX, LeftPupilY,
					RightPupilX, RightPupilY) == TRUE) return;

		m_LeftPupilX = LeftPupilX;
		m_LeftPupilY = LeftPupilY;
		m_RightPupilX = RightPupilX;
		m_RightPupilY = RightPupilY;
		m_LeftPD = LeftPD;
		m_RightPD = RightPD;

		double dxPupil = m_LeftPupilX - m_RightPupilX;
		double dyPupil = m_LeftPupilY - m_RightPupilY;

		m_picSize = picSize;
		m_dxPupil = dxPupil;
		m_dyPupil = dyPupil;
		m_PD = m_LeftPD + m_RightPD;

		m_ScaleY = (picSize.cx * 3.) / (picSize.cy * 4.);
		m_MMinPixelByX = m_PD / _hypot(m_dxPupil, m_ScaleY * m_dyPupil);
		m_MMinPixelByY = m_ScaleY * m_MMinPixelByX;

		m_Cosinus = m_dxPupil * m_MMinPixelByX / m_PD;
		m_Sinus = m_dyPupil * m_MMinPixelByY / m_PD;

		MayBeUsed = TRUE;
	}
	void Initialize(CDimension* dim, double LeftPupilX, double LeftPupilY,
		double RightPupilX, double RightPupilY){

		if(!dim->MayBeUsed)
			return;
		if(MayBeUsed == TRUE && CompareData(dim, LeftPupilX, LeftPupilY,
					RightPupilX, RightPupilY) == TRUE) return;

		dim_picSize = dim->m_picSize;
		dim_LeftPupilX = dim->m_LeftPupilX;
		dim_LeftPupilY = dim->m_LeftPupilY;
		dim_RightPupilX = dim->m_RightPupilX;
		dim_RightPupilY = dim->m_RightPupilY;
		dim_LeftPD = dim->m_LeftPD;
		dim_RightPD = dim->m_RightPD;

		m_LeftPupilX = LeftPupilX;
		m_LeftPupilY = LeftPupilY;
		m_RightPupilX = RightPupilX;
		m_RightPupilY = RightPupilY;

		double dxPupil = m_LeftPupilX - m_RightPupilX;
		double dyPupil = m_LeftPupilY - m_RightPupilY;

		m_picSize = dim->m_picSize;
		m_ScaleY = (m_picSize.cx * 3.) / (m_picSize.cy * 4.);

		m_dxPupil = dxPupil;
		m_dyPupil = dyPupil;

		m_PD = _hypot(m_dxPupil * dim->GetMMinPixelByX(), m_dyPupil * dim->GetMMinPixelByY());
		m_LeftPD = m_RightPD = m_PD / 2.;

		m_MMinPixelByX = m_PD / _hypot(m_dxPupil, m_ScaleY * m_dyPupil);
		m_MMinPixelByY = m_ScaleY * m_MMinPixelByX;

		m_Cosinus = m_dxPupil * m_MMinPixelByX / m_PD;
		m_Sinus = m_dyPupil * m_MMinPixelByY / m_PD;

		MayBeUsed = TRUE;
	}

	BOOL CompareData(CSize picSize, double LeftPD, double RightPD, double LeftPupilX, double LeftPupilY,
		double RightPupilX, double RightPupilY){
		if(m_picSize != picSize) return FALSE;
		if(m_LeftPD != LeftPD) return FALSE;
		if(m_RightPD != RightPD) return FALSE;
		if(m_LeftPupilX != LeftPupilX) return FALSE;
		if(m_LeftPupilY != LeftPupilY) return FALSE;
		if(m_RightPupilX != RightPupilX) return FALSE;
		if(m_RightPupilY != RightPupilY) return FALSE;

		return TRUE;
	}
	BOOL CompareData(CDimension* dim, double LeftPupilX, double LeftPupilY,
		double RightPupilX, double RightPupilY){
		if(!dim->MayBeUsed) return FALSE;
		if(!dim->CompareData(dim_picSize, dim_LeftPD, dim_RightPD,
			dim_LeftPupilX, dim_LeftPupilY, dim_RightPupilX, dim_RightPupilY)) return FALSE;
		if(m_LeftPupilX != LeftPupilX) return FALSE;
		if(m_LeftPupilY != LeftPupilY) return FALSE;
		if(m_RightPupilX != RightPupilX) return FALSE;
		if(m_RightPupilY != RightPupilY) return FALSE;

		return TRUE;
	}

	double GetScaleY(){return m_ScaleY;}
	double GetMMinPixelByX(){return m_MMinPixelByX;}
	double GetMMinPixelByY(){return m_MMinPixelByY;}
	double GetCosinus(){return m_Cosinus;}
	double GetSinus(){return m_Sinus;}
	int Width(){return m_picSize.cx;}
	int Height(){return m_picSize.cy;}
//	double RightPD(){return m_RightPD;}
//	double LeftPD(){return m_LeftPD;}
	double PD(){return m_RightPD + m_LeftPD;}
	double GetPupilX(int iLens){
		double X = 0.;

		switch(iLens){
		case 0:		// ->wsRight
			X = m_RightPupilX; break;
		case 1:		// ->wsLeft
			X = m_LeftPupilX; break;
		}

		return X;
	}
	// double GetPupilY(int iLens)
	double GetPupilY(int iLens){
		double Y = 0.;

		switch(iLens){
		case 0:		// ->wsRight
			Y = m_RightPupilY; break;
		case 1:		// ->wsLeft
			Y = m_LeftPupilY; break;
		}

		return Y;
	}
	// BOOL PtInPicture(double X, double Y)
	//  ���������� TRUE ���� ����� ����� ������ ��������
	BOOL PtInPicture(double X, double Y){
		BOOL LiesInPicture = FALSE;
		if(X > 0. && X < (double)(m_picSize.cx-1) && Y > 0. && Y < (double)(m_picSize.cy-1)) LiesInPicture = TRUE;
		return LiesInPicture;
	}

	BOOL MayBeUsed;

protected:
	CSize m_picSize;
	double m_LeftPupilX, m_LeftPupilY, m_RightPupilX, m_RightPupilY;
	double m_LeftPD, m_RightPD;
	double m_dxPupil, m_dyPupil;
	double m_PD;

	double m_ScaleY;
	double m_MMinPixelByX;
	double m_MMinPixelByY;

	double m_Cosinus, m_Sinus;

	CSize dim_picSize;
	double dim_LeftPupilX, dim_LeftPupilY, dim_RightPupilX, dim_RightPupilY;
	double dim_LeftPD, dim_RightPD;
};

class CContourPointBelong
{
public:
	CContourPointBelong();
	virtual ~CContourPointBelong();

	void LoadData(CContourPointBelong* Be);
	void FreeData();

	CSize picSize;
	double m_LeftPupilX, m_LeftPupilY, m_RightPupilX, m_RightPupilY;
	double m_LeftPD, m_RightPD;
	CObjPtr<IContour> lpdispLeftContour, lpdispRightContour;
	double m_LeftOptCenOffsetX, m_LeftOptCenOffsetY, m_RightOptCenOffsetX, m_RightOptCenOffsetY;

	BOOL CalculateContourPointMap();

	CRect GetLensRect();
	int GetStartX(int Y, int iLens);
	int GetEndX(int Y, int iLens);
	int Belong(int X, int Y, int iLens);
	double GetCenterX(int iLens);
	double GetCenterY(int iLens);
	double GetLensRadius(int iLens);
	long GetBorderPointsNumber(int iLens);
	double GetContourCenterX(int iLens);
	double GetContourCenterY(int iLens);

	BOOL MayBeUsed;

protected:
	double m_LeftRadius, m_RightRadius;
	CMassiv Och;
	PUCHAR hOch;
	CRect LenseZon;
	int (*Mss)[4];
	double Xcn[2], Ycn[2];
	double XcContour[2], YcContour[2];

	long BorderPoints[2];

	// Properties
	// ����� ���������
	CSize picSize_Arc;
	double m_LeftPupilX_Arc, m_LeftPupilY_Arc, m_RightPupilX_Arc, m_RightPupilY_Arc;
	double m_LeftPD_Arc, m_RightPD_Arc;
	double m_LeftOptCenOffsetX_Arc, m_LeftOptCenOffsetY_Arc, m_RightOptCenOffsetX_Arc, m_RightOptCenOffsetY_Arc;
	// Methods
	void ClearArchive();
	void SaveToArchive();
	BOOL CompareDataArchive();
};

struct LensCache
{
	double Sph[2];
	double Cyl[2];
	double Add[2];
	long Axis[2];
	long KindOfMat;
	double RefIndex;
	double Density;
	long Design;
	double CenterX[2];
	double CenterY[2];
	double OptToGeomCenterDisplacementX[2];
	double OptToGeomCenterDisplacementY[2];
};

class CRefraction
{
public:
	CRefraction(CContourPointBelong * Be, CDimension * dimPwof, CDimension * dimPwf, CDimension * dimCPwf, CMassiv * mRwof, CMassiv * mGwof, CMassiv * mBwof, ILens * lpdispRightLens, ILens * lpdispLeftLens);
	CRefraction();
	~CRefraction();

	void InitializeRefraction(	CContourPointBelong * Be,
								CDimension * dimPwof,
								CDimension * dimPwf,
								CDimension * dimCPwf,
								CMassiv * mRwof,
								CMassiv * mGwof,
								CMassiv * mBwof,
								ILens * lpdispRightLens,
								ILens * lpdispLeftLens,
								double AverZRightLens,
								double AverZLeftLens);
//	void PrepareRefraction(int iLens);

	double CalculateBaseDioptries(int X, int Y, int iLens);

	void CalculateRefraction(int X, int Y, int iLens, double Dioptries);
	BOOL CalculateRefraction2(int X, int Y, int iLens, double FocalDiopt, FTRECT *BoundRect);

	double GetR();
	double GetG();
	double GetB();

	CRayTracer RayTracer[2];

protected:
	void Initialize();
	double aSTIGMAT(double X, double Y, int iLens);
	
	CFaceGeom Face;
	static CColorMatrix32 MultiRefracMatrix;

	CContourPointBelong * Be;
	CDimension * dimPwof, * dimPwf, * dimCPwf;
	CMassiv * mRwof, * mGwof, * mBwof;
	CObjPtr<ILens> Lens[2];
//	double *pShell2Par;

	double Ao, Bo, PS;
	double AverZLens[2];

	LensCache LnsCache;

	double CosinusAstigmat, SinusAstigmat;

	double m_Red, m_Green, m_Blue;

	CWSLensModel LensModel;
};
