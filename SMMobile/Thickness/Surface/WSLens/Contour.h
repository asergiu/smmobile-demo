#pragma once
// Contour.h : header file
//

#include "ObjectBase.h"
#include <vector>
using std::vector;

const unsigned int MaxCountContourPoint = 1000;

/////////////////////////////////////////////////////////////////////////////
// CContour

class CContour :
	public CObjectEventBase<IContour, IContourEvents>,
	public CObjEventSimpleImpl<IContourPoint, IContourPointEvents>
{
public:
	static CContour* CreateObject();
	IContour * GetInterface();

protected:
	CContour();		   // protected constructor used by dynamic creation

// Attributes
	typedef vector< CObjPtr<IContourPoint> > VContourPoint;
	VContourPoint pt;

public:

// Operations
public:
	unsigned long GetCount();
	double GetCenterX();
	double GetCenterY();
	IContourPoint * Add(double Xvalue, double Yvalue);
	IContourPoint * GetItem(unsigned long Index);
	bool SetItem(unsigned long Index, IContourPoint * newValue);
	bool Clear();
	IContour * Clone();
	bool GetPoint(unsigned long index, double *pXvalue, double *pYvalue);
	bool Intersection(double x, double y, double angle, vector<FTPOINT>& result);

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);
	bool IsFinalClear;

protected:
	virtual ~CContour();

	virtual void FinalRelease()
	{
		IsFinalClear = true;
		Clear();
		delete this;
	}

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->ContourChanged();
			}
		}
	}

protected:
	void ContourPointChanged()
	{
		OnChildChanged();
	}
	void OnChildChanged();
};
