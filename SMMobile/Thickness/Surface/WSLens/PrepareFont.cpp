#include "stdafx.h"

/*
static BOOL CALLBACK _EnumResLangProc(HMODULE, LPCTSTR, 
	LPCTSTR, WORD langid, LONG lParam)
{
	LANGID* plangid = reinterpret_cast< LANGID* >( lParam );
	*plangid = langid;

	return FALSE;
}
*/

/*

Charset Name       Charset Value(hex)  Codepage number
------------------------------------------------------

DEFAULT_CHARSET           1 (x01)
SYMBOL_CHARSET            2 (x02)
OEM_CHARSET             255 (xFF)
ANSI_CHARSET              0 (x00)            1252
RUSSIAN_CHARSET         204 (xCC)            1251
EE_CHARSET              238 (xEE)            1250
GREEK_CHARSET           161 (xA1)            1253
TURKISH_CHARSET         162 (xA2)            1254
BALTIC_CHARSET          186 (xBA)            1257
HEBREW_CHARSET          177 (xB1)            1255
ARABIC_CHARSET          178 (xB2)            1256
SHIFTJIS_CHARSET        128 (x80)             932
HANGEUL_CHARSET         129 (x81)             949
GB2312_CHARSET          134 (x86)             936
CHINESEBIG5_CHARSET     136 (x88)             950 

*/
#ifdef _WINDOWS_
static BYTE GetCharsetByCodePage(int CodePage)
{
	BYTE Charset = DEFAULT_CHARSET;

	switch(CodePage)
	{
	case 1252:
		Charset = ANSI_CHARSET;
		break;
	case 1251:
		Charset = RUSSIAN_CHARSET;
		break;
	case 1250:
		Charset = EASTEUROPE_CHARSET;
		break;
	case 1253:
		Charset = GREEK_CHARSET;
		break;
	case 1254:
		Charset = TURKISH_CHARSET;
		break;
	case 1257:
		Charset = BALTIC_CHARSET;
		break;
	case 1255:
		Charset = HEBREW_CHARSET;
		break;
	case 1256:
		Charset = ARABIC_CHARSET;
		break;
	case 932:
		Charset = SHIFTJIS_CHARSET;
		break;
	case 949:
		Charset = HANGEUL_CHARSET;
		break;
	case 936:
		Charset = GB2312_CHARSET;
		break;
	case 950:
		Charset = CHINESEBIG5_CHARSET;
		break;
	}

	return Charset;
}

BYTE GetCurrentCharset()
{
    return GetCharsetByCodePage(GetACP());
}
#endif

void PrepareFont(LOGFONT *lfFont, int nHeight, int nWeight, int nWidth)
{
/*
	static bool bCharsetMapped = false;
	static BYTE Charset = DEFAULT_CHARSET;

	if(!bCharsetMapped)
	{
		LANGID langid = 0;
		
		::EnumResourceLanguages(NULL, RT_VERSION, MAKEINTRESOURCE(1),
					_EnumResLangProc, reinterpret_cast<LONG>(&langid));

		if(langid != 0)
		{
			int nPrimaryLang = PRIMARYLANGID(langid);
			int	nSubLang = SUBLANGID(langid);

			LCID lcid = MAKELCID(MAKELANGID(nPrimaryLang, nSubLang), SORT_DEFAULT);

			int CodePage = 0;

			char buffer[7];
			if(GetLocaleInfo(lcid, LOCALE_IDEFAULTANSICODEPAGE, buffer, sizeof(buffer)) != 0)
				CodePage = atoi(buffer);

			GetCharsetByCodePage(CodePage);
		}

		bCharsetMapped = true;
	}
*/
    if(lfFont) {
        memset(lfFont, 0, sizeof(LOGFONT));

        lfFont->lfHeight			= 	nHeight;
        lfFont->lfWidth				=   nWidth;
        lfFont->lfEscapement		=   0;
        lfFont->lfOrientation		=   0;
        lfFont->lfWeight			=   nWeight;
        lfFont->lfItalic			=   0;
        lfFont->lfUnderline			=   0;
        lfFont->lfStrikeOut			=   0;
#ifdef _WINDOWS_
        lfFont->lfCharSet			=   GetCharsetByCodePage(GetACP());
        lfFont->lfOutPrecision		=   OUT_DEFAULT_PRECIS;
        lfFont->lfClipPrecision		=   CLIP_DEFAULT_PRECIS;
        lfFont->lfQuality			=   DEFAULT_QUALITY;
        lfFont->lfPitchAndFamily	=   DEFAULT_PITCH|FF_DONTCARE;
#endif
        strcpy(lfFont->lfFaceName, "arial");

    }

}                
