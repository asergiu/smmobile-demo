#pragma once
// LensSurface.h : header file
//

#include "ObjectBase.h"

/////////////////////////////////////////////////////////////////////////////
// CLensSurface

class CLensSurface :
	public CObjectEventBase<ILensSurface, ILensSurfaceEvents>
{
public:
    static CLensSurface* CreateObject();
	ILensSurface * GetInterface();

protected:
	CLensSurface();           // protected constructor used by dynamic creation

// Attributes
	typedef vector<LensSurfPoint> VLensSurfPoint;
	VLensSurfPoint Points;

public:

// Operations
public:
	unsigned long GetCount();
	bool Add(double Xvalue, double Yvalue, double zRearvalue, double zFrontvalue);
	bool GetItem(unsigned long Index, double *Xvalue, double *Yvalue, double *zRearvalue, double *zFrontvalue);
	bool SetItemXY(unsigned long Index, double Xvalue, double Yvalue);
	bool SetItemZ(unsigned long Index, double zRearvalue, double zFrontvalue);
	bool AddArray(const vector<LensSurfPoint>& Elements);
	vector<LensSurfPoint> * GetArray();
	bool Clear();
	ILensSurface * Clone();

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);
	bool IsFinalClear;

protected:
	virtual ~CLensSurface();

	virtual void FinalRelease()
	{
		IsFinalClear = true;
		Clear();
		delete this;
	}

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->LensSurfaceChanged();
			}
		}
	}
};
