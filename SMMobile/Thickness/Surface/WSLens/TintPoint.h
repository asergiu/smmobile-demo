#pragma once
// TintPoint.h : header file
//

#include "ObjectBase.h"

/////////////////////////////////////////////////////////////////////////////
// CTintPoint

class CTintPoint :
	public CObjectEventBase<ITintPoint, ITintPointEvents>
{
public:
    static CTintPoint* CreateObject();
	ITintPoint * GetInterface();

protected:
	CTintPoint();           // protected constructor used by dynamic creation

// Attributes
	unsigned long m_r, m_g, m_b;

public:

// Operations
public:
	unsigned long GetR();
	bool SetR(unsigned long newValue);
	unsigned long GetG();
	bool SetG(unsigned long newValue);
	unsigned long GetB();
	bool SetB(unsigned long newValue);
	bool SetPoint(unsigned long R, unsigned long G, unsigned long B);
	ITintPoint * Clone();

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);

protected:
	virtual ~CTintPoint();

	virtual void FinalRelease()
	{
		delete this;
	}

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->TintPointChanged();
			}
		}
	}
};
