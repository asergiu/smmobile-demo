#pragma once

#include <string>
using std::string;

const string ErrorString[] = {
/*  0 */	"No error",
/*  1 */	"Argument is out of range",
/*  2 */	"Too many items in the collection",
/*  3 */	"Object doesn't implement ILensEngine interface",
/*  4 */	"Engine was not attached to lens object",
/*  5 */	"Insufficient memory",
/*  6 */	"One of the required methods is not supported",
/*  7 */	"Method failed",
/*  8 */	"WSLensEngine error: Can't create WSLens.LensSurface object",
/*  9 */	"WSLensEngine error: Engine was not attached to lens object",
/* 10 */	"WSLensEngine error: One of required method is not supported",
/* 11 */	"WSLensEngine error: Method failed",
/* 12 */	"WSLensEngine error: Bad lens contour",
/* 13 */	"Error while calculating weight: ",
/* 14 */	"Error while calculating thicknesses: ",
/* 15 */	"Error while calculating surface: ",
/* 16 */	"Out of memory",
/* 17 */	"Invalid design GUID",
/* 18 */	"WSLensEngine error: Design not supported",
/* 19 */	"WSLensEngine error: Can't create lens with such parameters",
/* 20 */	"WSLensEngine error: Insufficient memory",
/* 21 */	"WSLensEngine error: Wrong lens surface geometry",
/* 22 */	"Cannot set property when calculation is not suspended",
/* 23 */	"Error locking a global memory object",
/* 24 */	"Color format of this BMP is not supported. Use 8, 16, 24 or 32 bit per pixel non-compressed format.",
/* 25 */	"Can't create object",
/* 26 */	"Operation not allowed (insufficient data)",
/* 27 */	"Previous call not completed",
/* 28 */	"Auto identification initialization failed",
/* 29 */	"No printer",
/* 30 */	"Paper size is too small",
/* 31 */	"Only portrait orientation supported for this print!",
/* 32 */	"Only landscape orientation supported for this print!",
/* 33 */	"Object doesn't implement IPersistStream(Init) interface",
/* 34 */	"Object is not array",
/* 35 */	"Wrong dimension of array (must be 2)",
/* 36 */	"Wrong element size (must be 8 bytes)",
/* 37 */	"Wrong 1st dimension size (must be 4)",
/* 38 */	"Cannot render",
/* 39 */	"Custom print layout not properly configured",
/* 40 */	"Multiple pictures print layout is not properly configured"
/* 41 */	"Error while calculating UV protection: ",
};

const string strError_CalcWeight = ErrorString[13];
const string strError_CalcThicknesses = ErrorString[14];
const string strError_CalcUVProtection = ErrorString[40];
const string strError_NoEngine = ErrorString[4];
const string strError_OutOfRange = ErrorString[1];
const string strError_NotSuspended = ErrorString[22];
const string strError_MethodNotSupported = ErrorString[6];

