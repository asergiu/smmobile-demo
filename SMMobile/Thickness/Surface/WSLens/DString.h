#pragma once
// DString.h : header file
//

#include "ObjectBase.h"
#include <string>
using std::string;

/////////////////////////////////////////////////////////////////////////////
// CDString

class CDString :
	public CObjectEventBase<IDString, IDStringEvents>
{
public:
    static CDString* CreateObject();
	IDString * GetInterface();

protected:
	CDString();           // protected constructor used by dynamic creation

// Attributes
	string m_Text;
	DStringOrigins m_origin;
	unsigned long m_left;
	unsigned long m_top;
	unsigned long m_width;
	unsigned long m_height;
	long m_format;
    string m_FontL;
    LOGFONT m_Font;
    unsigned long m_foreColor;
	unsigned long m_backColor;
	DStringBkModes m_bkMode;
	bool m_outline;
	unsigned long m_outlineColor;

public:

// Operations
public:
	string GetText();
    bool SetText(const char * newValue);
	DStringOrigins GetOrigin();
	bool SetOrigin(DStringOrigins newValue);
	unsigned long GetLeft();
	bool SetLeft(unsigned long newValue);
	unsigned long GetTop();
	bool SetTop(unsigned long newValue);
	unsigned long GetWidth();
	bool SetWidth(unsigned long newValue);
	unsigned long GetHeight();
	bool SetHeight(unsigned long newValue);
	long GetFormat();
	bool SetFormat(long newValue);
#ifndef _WINDOWS_
    string GetFont();
    bool SetFont(const char * newValue);
#else
    LOGFONT GetFont();
    bool SetFont(LOGFONT newValue);
#endif
    unsigned long GetForeColor();
	bool SetForeColor(unsigned long newValue);
	unsigned long GetBackColor();
	bool SetBackColor(unsigned long newValue);
	DStringBkModes GetBkMode();
	bool SetBkMode(DStringBkModes newValue);
	bool GetOutline();
	bool SetOutline(bool newValue);
	unsigned long GetOutlineColor();
	bool SetOutlineColor(unsigned long newValue);
	bool CalcRect(unsigned long* ExactWidth, unsigned long* ExactHeight);
	bool SizeToContent();
	IDString * Clone();

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);

protected:
	virtual ~CDString();

	virtual void FinalRelease()
	{
		delete this;
	}

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->DStringChanged();
			}
		}
	}
};
