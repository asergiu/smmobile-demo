#pragma once

#include <string>
using std::string;
#include <vector>
using std::vector;
#include "iosfwd"
using std::ostream;
using std::istream;

#include "stdafx.h"
#include "FTPoint.h"

#ifndef _WINDOWS_
//#include <X11/Xlib.h>
#endif

#undef AFX_EXT_DATA
#ifdef _WINDOWS_
	#ifdef _EXPORTDLL_
		#define AFX_EXT_DATA __declspec(dllexport)
	#else
		#define AFX_EXT_DATA __declspec(dllimport)
	#endif
	#define DLL_LOCAL
#else
	#ifdef _EXPORTDLL_
		#define DLL_PUBLIC __attribute__ ((visibility("default")))
		#define DLL_LOCAL  __attribute__ ((visibility("hidden")))
	#else
		#define DLL_PUBLIC __attribute__ ((visibility("default")))
	#endif
	#define AFX_EXT_DATA DLL_PUBLIC
#endif


const std::string Object_ARCoating = "{CBEACD25-0460-11D2-BC5D-00AA0009F666}";
const std::string Object_Coating = "{EDBEB765-D091-11D2-920B-444553540000}";
const std::string Object_Coatings = "{EDBEB76A-D091-11D2-920B-444553540000}";
const std::string Object_ContourPoint = "{CBEACD22-0460-11D2-BC5D-00AA0009F666}";
const std::string Object_Contour = "{E0C494F9-0057-11D2-821F-0080489146DD}";
const std::string Object_DString = "{B0CDE7E6-52DD-11D2-821F-444553540000}";
const std::string Object_DStringCollection = "{582A3704-5301-11D2-821F-CF2D35DB105A}";
const std::string Object_LensSurface = "{03897666-32DC-11D2-821F-444553540000}";
const std::string Object_MultifocalSegment = "{D8408965-9083-11D3-9256-008048C2DBB8}";
const std::string Object_MultifocalSegmentCollection = "{E5BFB862-914E-11D3-9256-008048C2DBB8}";
const std::string Object_Progressive = "{FE887FC5-BAAE-11D2-920B-444553540000}";
const std::string Object_TintPoint = "{E0C494FB-0057-11D2-821F-0080489146DD}";
const std::string Object_Tint = "{E0C494F3-0057-11D2-821F-0080489146DD}";
const std::string Object_LensEngine = "{8A89EF15-35D4-11D2-821F-444553540000}";
const std::string Object_WSPicture = "{C6110D22-C101-11D2-BC5E-00AA0009F666}";
const std::string Object_Lens = "{3C584162-0474-11D2-BC5D-00AA0009F666}";
const std::string Control_Spectacles = "{B67DCF66-06A3-11D2-821F-0080489146DD}";

class IObjectBase
{
public:
	virtual unsigned long AddRef() = 0;
	virtual unsigned long Release() = 0;
	virtual bool WriteToSteam(ostream * pOutStream) = 0;
	virtual bool ReadFromSteam(istream * pInStream) = 0;
};

class IControlBase
{
public:
#ifdef _WINDOWS_
	virtual HWND get_ParentWindow() = 0;
	virtual HWND get_Window() = 0;
#else
	/*// Contol properties
	virtual Display * get_Display() = 0;
	virtual Window get_ParentWindow() = 0;
	virtual Window get_Window() = 0;
	// Contol methods
	virtual bool ProcessEvent(XEvent * event) = 0;*/
#endif
	virtual void ShowControl(bool bShow) = 0;
	virtual void DestroyControl() = 0;

	virtual bool WriteToSteam(ostream * pOutStream) = 0;
	virtual bool ReadFromSteam(istream * pInStream) = 0;
};

template <class TEvent>
class IEventPoint
{
public:
	virtual bool Advise(TEvent* pUnkSink, unsigned long * pdwCookie) = 0;
	virtual bool Unadvise(unsigned long dwCookie) = 0;
};

/////////////////////////////////////////////////////////////////////////////
// IARCoating interface classes

class IARCoatingEvents
{
public:
	virtual void ARCoatingChanged() = 0;
};

class IARCoating :
	public IObjectBase,
	public IEventPoint<IARCoatingEvents>
{
public:
	virtual unsigned long GetB() = 0;
	virtual unsigned long GetG() = 0;
	virtual unsigned long GetR() = 0;
	virtual bool SetB(unsigned long newVal) = 0;
	virtual bool SetG(unsigned long newVal) = 0;
	virtual bool SetR(unsigned long newVal) = 0;
	virtual string GetName() = 0;
	virtual bool SetName(const char *) = 0;
	virtual string GetEDPCode() = 0;
	virtual bool SetEDPCode(const char *) = 0;
	virtual string GetInfo() = 0;
	virtual bool SetInfo(const char *) = 0;
	virtual IARCoating * Clone() = 0;
};

/////////////////////////////////////////////////////////////////////////////
// ICoating interface classes

class ICoatingEvents
{
public:
	virtual void CoatingChanged() = 0;
};

class ICoating :
	public IObjectBase,
	public IEventPoint<ICoatingEvents>
{
public:
	virtual string GetName() = 0;
	virtual bool SetName(const char *) = 0;
	virtual string GetEDPCode() = 0;
	virtual bool SetEDPCode(const char *) = 0;
	virtual string GetInfo() = 0;
	virtual bool SetInfo(const char *) = 0;
	virtual ICoating * Clone() = 0;
};

/////////////////////////////////////////////////////////////////////////////
// ICoatings interface classes

class ICoatingsEvents
{
public:
	virtual void CoatingsChanged() = 0;
};

class ICoatings :
	public IObjectBase,
	public IEventPoint<ICoatingsEvents>
{
public:
	virtual string GetName() = 0;
	virtual bool SetName(const char *) = 0;
	virtual string GetEDPCode() = 0;
	virtual bool SetEDPCode(const char *) = 0;
	virtual string GetInfo() = 0;
	virtual bool SetInfo(const char *) = 0;
	virtual ICoating * GetUVProtection() = 0;
	virtual bool SetUVProtection(ICoating *) = 0;
	virtual ICoating * GetScratchResistance() = 0;
	virtual bool SetScratchResistance(ICoating *) = 0;
	virtual ICoating * GetWaterRepellent() = 0;
	virtual bool SetWaterRepellent(ICoating *) = 0;
	virtual ICoating * GetOtherCoating() = 0;
	virtual bool SetOtherCoating(ICoating *) = 0;
	virtual ICoatings * Clone() = 0;
};

/////////////////////////////////////////////////////////////////////////////
// IContourPoint interface classes

class IContourPointEvents
{
public:
	virtual void ContourPointChanged() = 0;
};

class IContourPoint :
	public IObjectBase,
	public IEventPoint<IContourPointEvents>
{
public:
	virtual double GetX() = 0;
	virtual bool SetX(double Xvalue) = 0;
	virtual double GetY() = 0;
	virtual bool SetY(double Yvalue) = 0;
	virtual bool SetPoint(double Xvalue, double Yvalue) = 0;
	virtual IContourPoint * Clone() = 0;
};

/////////////////////////////////////////////////////////////////////////////
// IContour interface classes

class IContourEvents
{
public:
	virtual void ContourChanged() = 0;
};

class IContour :
	public IObjectBase,
	public IEventPoint<IContourEvents>
{
public:
	virtual unsigned long GetCount() = 0;
	virtual double GetCenterX() = 0;
	virtual double GetCenterY() = 0;
	virtual IContourPoint * Add(double Xvalue, double Yvalue) = 0;
	virtual IContourPoint * GetItem(unsigned long Index) = 0;
	virtual bool SetItem(unsigned long Index, IContourPoint * newValue) = 0;
	virtual bool Clear() = 0;
	virtual IContour * Clone() = 0;
	virtual bool GetPoint(unsigned long index, double *pXvalue, double *pYvalue) = 0;
	virtual bool Intersection(double x, double y, double angle, vector<FTPOINT>& result) = 0;
};

/////////////////////////////////////////////////////////////////////////////
// IDString interface classes

class IDStringEvents
{
public:
	virtual void DStringChanged() = 0;
};

typedef enum DStringOrigins		// Corners for DString
{
	wsLeftUpper = 0,		// Left upper corner
	wsLeftLower = 1,		// Left lower corner
	wsRightUpper = 2,		// Right upper corner
	wsRightLower = 3		// Right lower corner
} DStringOrigins;

typedef enum DStringFormats		// DString placement format
{
	wsDStrBottom = 8,		// Specifies bottom-justified text
	wsDStrTop = 0,			// Specifies top-justified text
	wsDStrVCenter = 4,		// Centers text vertically
	wsDStrRight = 2,		// Specifies right-justified text
	wsDStrLeft = 0,			// Specifies left-justified text
	wsDStrHCenter = 1		// Centers text horizontally
} DStringFormats;

typedef enum DStringBkModes		// DString transparency modes
{
	wsTransparent = 0,		// Background remains untouched
	wsOpaque = 1			// Background is filled with the current background color before the text
} DStringBkModes;

class IDString :
	public IObjectBase,
	public IEventPoint<IDStringEvents>
{
public:
	virtual string GetText() = 0;
	virtual bool SetText(const char * newValue) = 0;
	virtual DStringOrigins GetOrigin() = 0;
	virtual bool SetOrigin(DStringOrigins newValue) = 0;
	virtual unsigned long GetLeft() = 0;
	virtual bool SetLeft(unsigned long newValue) = 0;
	virtual unsigned long GetTop() = 0;
	virtual bool SetTop(unsigned long newValue) = 0;
	virtual unsigned long GetWidth() = 0;
	virtual bool SetWidth(unsigned long newValue) = 0;
	virtual unsigned long GetHeight() = 0;
	virtual bool SetHeight(unsigned long newValue) = 0;
	virtual long GetFormat() = 0;
	virtual bool SetFormat(long newValue) = 0;
#ifndef _WINDOWS_
	virtual string GetFont() = 0;
	virtual bool SetFont(const char * newValue) = 0;
#else
	virtual LOGFONT GetFont() = 0;
	virtual bool SetFont(LOGFONT newValue) = 0;
#endif
	virtual unsigned long GetForeColor() = 0;
	virtual bool SetForeColor(unsigned long newValue) = 0;
	virtual unsigned long GetBackColor() = 0;
	virtual bool SetBackColor(unsigned long newValue) = 0;
	virtual DStringBkModes GetBkMode() = 0;
	virtual bool SetBkMode(DStringBkModes newValue) = 0;
	virtual bool GetOutline() = 0;
	virtual bool SetOutline(bool newValue) = 0;
	virtual unsigned long GetOutlineColor() = 0;
	virtual bool SetOutlineColor(unsigned long newValue) = 0;
	virtual bool CalcRect(unsigned long* ExactWidth, unsigned long* ExactHeight) = 0;
	virtual bool SizeToContent() = 0;
	virtual IDString * Clone() = 0;
};

/////////////////////////////////////////////////////////////////////////////
// IDStringCollection interface classes

class IDStringCollectionEvents
{
public:
	virtual void DStringCollectionChanged() = 0;
};

class IDStringCollection :
	public IObjectBase,
	public IEventPoint<IDStringCollectionEvents>
{
public:
	virtual unsigned long GetCount() = 0;
	virtual IDString * GetItem(unsigned long Index) = 0;
	virtual bool SetItem(unsigned long Index, IDString * newValue) = 0;
	virtual bool Clear() = 0;
	virtual IDStringCollection * Clone() = 0;
	virtual bool Add(IDString * newDString) = 0;
};

/////////////////////////////////////////////////////////////////////////////
// ILensSurfaceEvents interface classes

class ILensSurfaceEvents
{
public:
	virtual void LensSurfaceChanged() = 0;
};

struct LensSurfPoint
{
	double X;
	double Y;
	double zRear;
	double zFront;
    double xnRear;
    double ynRear;
    double znRear;
    double xnFront;
    double ynFront;
    double znFront;
};

class ILensSurface :
	public IObjectBase,
	public IEventPoint<ILensSurfaceEvents>
{
public:
	virtual unsigned long GetCount() = 0;
	virtual bool Add(double Xvalue, double Yvalue, double zRearvalue, double zFrontvalue) = 0;
	virtual bool GetItem(unsigned long Index, double *Xvalue, double *Yvalue, double *zRearvalue, double *zFrontvalue) = 0;
	virtual bool SetItemXY(unsigned long Index, double Xvalue, double Yvalue) = 0;
	virtual bool SetItemZ(unsigned long Index, double zRearvalue, double zFrontvalue) = 0;
	virtual bool AddArray(const vector<LensSurfPoint>& Elements) = 0;
	virtual vector<LensSurfPoint> * GetArray() = 0;
	virtual bool Clear() = 0;
	virtual ILensSurface * Clone() = 0;
};

/////////////////////////////////////////////////////////////////////////////
// IMultifocalSegmentEvents interface classes

class IMultifocalSegmentEvents
{
public:
	virtual void MultifocalSegmentChanged() = 0;
};

typedef enum MultifocalSegmentType		// Multifocal segment types
{
	wsRaised = 0,				// Raised segment
	wsEmbedded = 1,				// Embedded segment
	wsFrontSurfLowered = 2,		// Segment shaped by deleting material from front surface
	wsBackSurfLowered = 3,		// Segment shaped by deleting material from back surface
	wsFrontSurfProgressive = 4
} MultifocalSegmentType;

class IMultifocalSegment :
	public IObjectBase,
	public IEventPoint<IMultifocalSegmentEvents>
{
public:
	virtual string GetName() = 0;
	virtual bool SetName(const char * newValue) = 0;
	virtual double GetMainCircCenterX() = 0;
	virtual bool SetMainCircCenterX(double newValue) = 0;
	virtual double GetMainCircCenterY() = 0;
	virtual bool SetMainCircCenterY(double newValue) = 0;
	virtual double GetMainCircRadius() = 0;
	virtual bool SetMainCircRadius(double newValue) = 0;
	virtual double GetBifocalBoundCircCenterX() = 0;
	virtual bool SetBifocalBoundCircCenterX(double newValue) = 0;
	virtual double GetBifocalBoundCircCenterY() = 0;
	virtual bool SetBifocalBoundCircCenterY(double newValue) = 0;
	virtual double GetBifocalBoundCircRadius() = 0;
	virtual bool SetBifocalBoundCircRadius(double newValue) = 0;
	virtual double GetTrifocalBoundCircCenterX() = 0;
	virtual bool SetTrifocalBoundCircCenterX(double newValue) = 0;
	virtual double GetTrifocalBoundCircCenterY() = 0;
	virtual bool SetTrifocalBoundCircCenterY(double newValue) = 0;
	virtual double GetTrifocalBoundCircRadius() = 0;
	virtual bool SetTrifocalBoundCircRadius(double newValue) = 0;
	virtual double GetAddit() = 0;
	virtual bool SetAddit(double newValue) = 0;
	virtual double GetPantoRadius() = 0;
	virtual bool SetPantoRadius(double newValue) = 0;
	virtual MultifocalSegmentType GetSegmentType() = 0;
	virtual bool SetSegmentType(MultifocalSegmentType newValue) = 0;
	virtual unsigned long GetErrorState() = 0;
	virtual double GetTrifocalOpticalCenterShiftX() = 0;
	virtual bool SetTrifocalOpticalCenterShiftX(double newValue) = 0;
	virtual double GetTrifocalOpticalCenterShiftY() = 0;
	virtual bool SetTrifocalOpticalCenterShiftY(double newValue) = 0;
	virtual double GetBifocalOpticalCenterShiftX() = 0;
	virtual bool SetBifocalOpticalCenterShiftX(double newValue) = 0;
	virtual double GetBifocalOpticalCenterShiftY() = 0;
	virtual bool SetBifocalOpticalCenterShiftY(double newValue) = 0;
	virtual double GetTrifocalAdditRatio() = 0;
	virtual bool SetTrifocalAdditRatio(double newValue) = 0;
	virtual IMultifocalSegment * Clone() = 0;
};

/////////////////////////////////////////////////////////////////////////////
// IMultifocalSegmentCollectionEvents interface classes

class IMultifocalSegmentCollectionEvents
{
public:
	virtual void MultifocalSegmentCollectionChanged() = 0;
};

class IMultifocalSegmentCollection :
	public IObjectBase,
	public IEventPoint<IMultifocalSegmentCollectionEvents>
{
public:
	virtual unsigned long GetCount() = 0;
	virtual IMultifocalSegment * GetItem(unsigned long Index) = 0;
	virtual bool SetItem(unsigned long Index, IMultifocalSegment * newValue) = 0;
	virtual IMultifocalSegmentCollection * Clone() = 0;
	virtual bool Clear() = 0;
	virtual bool Add(IMultifocalSegment * newMultifocal) = 0;
};

/////////////////////////////////////////////////////////////////////////////
// IProgressiveEvents interface classes

class IProgressiveEvents
{
public:
	virtual void ProgressiveChanged() = 0;
};

class IProgressive :
	public IObjectBase,
	public IEventPoint<IProgressiveEvents>
{
public:
	virtual double GetFarVisionCenterX() = 0;
	virtual bool SetFarVisionCenterX(double newValue) = 0;
	virtual double GetFarVisionCenterY() = 0;
	virtual bool SetFarVisionCenterY(double newValue) = 0;
	virtual double GetFarVisionMarkerDiam() = 0;
	virtual bool SetFarVisionMarkerDiam(double newValue) = 0;
	virtual double GetNearVisionCenterX() = 0;
	virtual bool SetNearVisionCenterX(double newValue) = 0;
	virtual double GetNearVisionCenterY() = 0;
	virtual bool SetNearVisionCenterY(double newValue) = 0;
	virtual double GetNearVisionMarkerDiam() = 0;
	virtual bool SetNearVisionMarkerDiam(double newValue) = 0;
	virtual IProgressive * Clone() = 0;
};

/////////////////////////////////////////////////////////////////////////////
// ITintPointEvents interface classes

class ITintPointEvents
{
public:
	virtual void TintPointChanged() = 0;
};

class ITintPoint :
	public IObjectBase,
	public IEventPoint<ITintPointEvents>
{
public:
	virtual unsigned long GetR() = 0;
	virtual bool SetR(unsigned long newValue) = 0;
	virtual unsigned long GetG() = 0;
	virtual bool SetG(unsigned long newValue) = 0;
	virtual unsigned long GetB() = 0;
	virtual bool SetB(unsigned long newValue) = 0;
	virtual bool SetPoint(unsigned long R, unsigned long G, unsigned long B) = 0;
	virtual ITintPoint * Clone() = 0;
};

/////////////////////////////////////////////////////////////////////////////
// ITintEvents interface classes

class ITintEvents
{
public:
	virtual void TintChanged() = 0;
};

class ITint :
	public IObjectBase,
	public IEventPoint<ITintEvents>
{
public:
	virtual string GetName() = 0;
	virtual bool SetName(const char * newValue) = 0;
	virtual string GetEDPCode() = 0;
	virtual bool SetEDPCode(const char * newValue) = 0;
	virtual string GetInfo() = 0;
	virtual bool SetInfo(const char * newValue) = 0;
	virtual unsigned long GetCount() = 0;
	virtual ITintPoint * GetItem(unsigned long Index) = 0;
	virtual bool SetItem(unsigned long Index, ITintPoint * newValue) = 0;
	virtual bool Clear() = 0;
	virtual ITintPoint * Add(unsigned long Rvalue, unsigned long Gvalue, unsigned long Bvalue) = 0;
	virtual ITint * Clone() = 0;
};

/////////////////////////////////////////////////////////////////////////////
// ILensEngine

class ILens;

class ILensEngine :
	public IObjectBase
{
public:
	virtual bool CalcSurface(ILens * LensObject, ILensSurface * LensSurface) = 0;
	virtual bool CalcWeight(ILens * LensObject, double* ResultWeight) = 0;
	virtual bool CalcThickness(ILens * LensObject, double* ResultCenterThickness, double* ResultMinEdgeThickness, double* ResultMaxEdgeThickness) = 0;
	virtual bool CalcUVProtection(ILens * LensObject, double* ResultUVProtection) = 0;
	virtual unsigned long GetSupportedMethods() = 0;
	virtual void GetDepth( ILens* LensObject, double x, double y, double & rear, double & front ) = 0;
	virtual string GetLastErrorMessage() = 0;
	virtual ILensEngine * Clone() = 0;
};

/////////////////////////////////////////////////////////////////////////////
// IWSPicture interface classes

class IWSPictureEvents
{
public:
	virtual void PictureChanged() = 0;
};

class IWSPicture :
	public IObjectBase,
	public IEventPoint<IWSPictureEvents>
{
public:
	virtual long GetWidth() = 0;
	virtual long GetHeight() = 0;
	virtual const unsigned char * GetPicture() = 0;
	virtual bool SetPicture(const unsigned char * pNewValue) = 0;
	virtual bool PictureSize(long Width, long Height) = 0;
	virtual long LoadFromFile(const char * FileName) = 0;
	virtual long SaveToFile(const char * FileName) = 0;
    virtual long LoadBMPFromMemory(const unsigned char * pFileData) = 0;
#ifndef _WINDOWS_
	//virtual bool Render(Display* display, Window win, long x, long y, long cx, long cy, long xSrc, long ySrc, long cxSrc, long cySrc) = 0;
#endif
	virtual IWSPicture * Clone() = 0;
};

/////////////////////////////////////////////////////////////////////////////
// ILens interface classes

const std::string wsClassic = "{3DEFACA0-41B1-11d2-821F-444553540000}";
const std::string wsAspheric = "{3DEFACA1-41B1-11d2-821F-444553540000}";
const std::string wsLenticular = "{3DEFACA2-41B1-11d2-821F-444553540000}";

typedef enum EngineUsageMasks		// Engine usage masks
{
	wsWeightEngine1 = 0,			// Use Weight calculation procedure from Engine1
	wsWeightEngine2 = 1,			// Use Weight calculation procedure from Engine2
	wsSurfaceEngine1 = 0,			// Use Surface from Engine1
	wsSurfaceEngine2 = 2,			// Use Surface from Engine2
	wsThicknessEngine1 = 0,			// Use Thickness calculation procedure from Engine1
	wsThicknessEngine2 = 4,			// Use Thickness calculation procedure from Engine2
	wsUVProtectionEngine1 = 0,		// Use UV Protection calculation procedure from Engine1
	wsUVProtectionEngine2 = 8		// Use UV Protection calculation procedure from Engine2
} EngineUsageMasks;

typedef enum KindOfMaterial			// Type of lens material
{
	wsPlastic = 0,					// Plastic material
	wsGlass = 1,					// Mineral glass
	wsPolyCarbonate = 2				// Polycarbonate
} KindOfMaterial;

typedef enum MultifocalType				// Multifocal types
{
	wsSingleVision = 0,				// Single Vision
	wsProgressive = 1,				// Progressive
	wsFT35b = 2,					// FT 35 Bifocal
	wsFT8x35t = 3,					// FT 8x35 Trifocal
	wsFT25b = 4,					// FT 25 Bifocal
	wsRT25b = 5,					// RT 25 Bifocal
	wsFT6x25t = 6,					// FT 6x25 Trifocal
	wsRT6x25t = 7,					// RT 6x25 Trifocal
	wsFT28b = 8,					// FT 28 Bifocal
	wsRT28b = 9,					// RT 28 Bifocal
	wsFT7x28t = 10,					// FT 7x28 Trifocal
	wsRT7x28t = 11,					// RT 7x28 Trifocal
	wsFulsegb = 12,					// Fulsegment Bifocal
	wsFulsegt = 13,					// Fulsegment Trifocal
	wsUltex = 14,					// Ultex
	wsRound25 = 15,					// Round 25
	wsRound28 = 16,					// Round 28
	wsRound38 = 17,					// Round 38
	wsPanto28t = 18,				// Rodenstock Trifolux Panto-trifocal 28/22/8
	wsRT40b = 19,					// Rodenstock Datalit RT 40/25
	wsPanto26b = 20,				// Rodenstock Rodapan Panto-bifocal 26/17
	wsPanto28b = 21,				// Rodenstock Rodalux Panto-bifocal 28/19
	wsPanto32b = 22,				// Rodenstock Pantolux Panto-bifocal 32/21
	wsRT40t = 23,					// Rodenstock Datalit Trifo RT 40/25/10
	wsRound22 = 24,					// Rodenstock Round 22
	wsRT26b = 25,					// Rodenstock C26 Bifolit RT 26/18
	wsFT40b = 26,					// Rupp + Hubracht Hellaplast Telepal FT 40/20
	wsUserDefined = -1				// Multifocal segments defined by Multifocals property
} MultifocalType;

typedef enum SideType				// Lens side
{
	wsRight = 0,					// Right eye of the patient
	wsLeft = 1						// Left eye of the patient
} SideType;

typedef enum SuspendCalcMasks		// Suspend calculation masks
{
	wsSuspendWeightCalc = 1,		// Suspend Weight calculation procedure
	wsSuspendMinDiameterCalc = 2,	// Suspend MinDiameter calculation procedure
	wsSuspendThicknessCalc = 4,		// Suspend Thickness calculation procedure
	wsSuspendUVProtectionCalc = 8	// Suspend UV Protection calculation procedure
} SuspendCalcMasks;

typedef enum EngineSupportMasks		// Engine property support masks
{
	wsWeightSupported = 1,			// Engine supports Weight property
	wsSurfaceSupported = 2,			// Engine supports Surface property
	wsThicknessSupported = 4,		// Engine supports Thickness and Min Diameter property
	wsUVProtectionSupported = 8		// Engine supports UV Protection property
} EngineSupportMasks;

class ILensEvents
{
public:
	virtual void LensChanged() = 0;
};

class ILens :
	public IObjectBase,
	public IEventPoint<ILensEvents>
{
public:
	virtual SideType GetSide() = 0;
	virtual bool SetSide(SideType newValue) = 0;
	virtual double GetSph() = 0;
	virtual bool SetSph(double newValue) = 0;
	virtual double GetCyl() = 0;
	virtual bool SetCyl(double newValue) = 0;
	virtual double GetAddit() = 0;
	virtual bool SetAddit(double newValue) = 0;
	virtual long GetAxis() = 0;
	virtual bool SetAxis(long newValue) = 0;
	virtual KindOfMaterial GetKindOfMaterial() = 0;
	virtual bool SetKindOfMaterial(KindOfMaterial newValue) = 0;
	virtual double GetRefIndex() = 0;
	virtual bool SetRefIndex(double newValue) = 0;
	virtual double GetDensity() = 0;
	virtual bool SetDensity(double newValue) = 0;
	virtual string GetDesign() = 0;
	virtual bool SetDesign(const char * newValue) = 0;
	virtual double GetCenterX() = 0;
	virtual bool SetCenterX(double newValue) = 0;
	virtual double GetCenterY() = 0;
	virtual bool SetCenterY(double newValue) = 0;
	virtual IContour * GetContour() = 0;
	virtual bool SetContour(IContour *) = 0;
	virtual ITint * GetTint() = 0;
	virtual bool SetTint(ITint * newValue) = 0;
	virtual IARCoating * GetARCoating() = 0;
	virtual bool SetARCoating(IARCoating * newValue) = 0;
	virtual long GetDiameter() = 0;
	virtual bool SetDiameter(long newValue) = 0;
	virtual double GetPermittedCenterThickness() = 0;
	virtual bool SetPermittedCenterThickness(double newValue) = 0;
	virtual double GetPermittedEdgeThickness() = 0;
	virtual bool SetPermittedEdgeThickness(double newValue) = 0;
	virtual double GetPermittedZeroThickness() = 0;
	virtual bool SetPermittedZeroThickness(double newValue) = 0;
	virtual long GetErrorState() = 0;
	virtual MultifocalType GetMultifocalType() = 0;
	virtual bool SetMultifocalType(MultifocalType newValue) = 0;
	virtual double GetSegmHeight() = 0;
	virtual bool SetSegmHeight(double newValue) = 0;
	virtual string GetEDPCode() = 0;
	virtual bool SetEDPCode(const char * newValue) = 0;
	virtual ILensEngine * GetEngine1() = 0;
	virtual bool SetEngine1(ILensEngine * newValue) = 0;
	virtual ILensEngine * GetEngine2() = 0;
	virtual bool SetEngine2(ILensEngine * newValue) = 0;
	virtual long GetEngineUsageMask() = 0;
	virtual bool SetEngineUsageMask(long newValue) = 0;
	virtual long GetSuspendCalc() = 0;
	virtual bool SetSuspendCalc(long newValue) = 0;
	virtual double GetWeight() = 0;
	virtual bool SetWeight(double newValue) = 0;
	virtual double GetCenterThickness() = 0;
	virtual bool SetCenterThickness(double newValue) = 0;
	virtual double GetMinEdgeThickness() = 0;
	virtual bool SetMinEdgeThickness(double newValue) = 0;
	virtual double GetMaxEdgeThickness() = 0;
	virtual bool SetMaxEdgeThickness(double newValue) = 0;
	virtual double GetMinDiam() = 0;
	virtual bool SetMinDiam(double newValue) = 0;
	virtual bool GetIsPhotochromic() = 0;
	virtual bool SetIsPhotochromic(bool newValue) = 0;
	virtual ITint * GetPhotochromicTint() = 0;
	virtual bool SetPhotochromicTint(ITint * newValue) = 0;
	virtual double GetUVProtection() = 0;
	virtual bool SetUVProtection(double newValue) = 0;
	virtual ICoatings * GetCoatings() = 0;
	virtual bool SetCoatings(ICoatings * newValue) = 0;
	virtual double GetPhotochromicCarIndex() = 0;
	virtual bool SetPhotochromicCarIndex(double newValue) = 0;
	virtual double GetPhotochromicHeatIndex() = 0;
	virtual bool SetPhotochromicHeatIndex(double newValue) = 0;
	virtual IMultifocalSegmentCollection * GetMultifocal() = 0;
	virtual bool SetMultifocal(IMultifocalSegmentCollection * newValue) = 0;
	virtual IProgressive * GetProgressive() = 0;
	virtual bool SetProgressive(IProgressive * newValue) = 0;
	virtual double GetPrism() = 0;
	virtual bool SetPrism(double newValue) = 0;
	virtual long GetPrismAxis() = 0;
	virtual bool SetPrismAxis(long newValue) = 0;
	virtual bool GetEllipticBlank() = 0;
	virtual bool SetEllipticBlank(bool newValue) = 0;
	virtual double GetEllipticVertToHorizRatio() = 0;
	virtual bool SetEllipticVertToHorizRatio(double newValue) = 0;
	virtual double GetOptToGeomCenterDisplacementX() = 0;
	virtual bool SetOptToGeomCenterDisplacementX(double newValue) = 0;
	virtual double GetOptToGeomCenterDisplacementY() = 0;
	virtual bool SetOptToGeomCenterDisplacementY(double newValue) = 0;
	virtual ILens * Clone() = 0;
};

/////////////////////////////////////////////////////////////////////////////
// ISpectacles interface classes

typedef enum						// Detail level
{
	wsLowDetail = 0,				// Low detail visualization (blue background)
	wsHighDetail = 1				// High detail visualization (picture property must be set, otherwise this setting is ignored)
} DetailLevel;

typedef enum						// Spectacles position
{
	wsFrontal = 0,					// Frontal position (default)
	wsLookLeft = 1,					// Spectacles look left
	wsLookRight = 2,				// Spectacles look right
	wsLookDown = 3,					// Spectacles look down
	wsIsometric = 4,				// Isometric orientation, spectacles are turned 45 degrees around two axes
	wsCurrent = 5,					// Show method will not change position of spectacles
	wsDirect = 6
} StartPosition;

typedef enum						// Frame type
{
	wsNoFrame = 0,					// No frame shown (default)
	wsMetalFrame = 1,				// Metal frame
	wsPlasticFrame = 2,				// Horn-rimmed plastic frame
	wsRimlessFrame = 3				// Rimless frame
} VirtualFrame;

typedef enum						// Speed settings
{
	wsTurnFast = 0,					// Jumping
	wsTurnSlow = 1					// Moving
} SpeedSettings;

typedef enum						// Bevel placing method
{
	wsBevelRelative = 0,			// Bevel relative
	wsBevelFrontDistance = 1,		// Bevel front distance
	wsBevelRearDistance = 2			// Bevel rear distance
} BevelPlacingMethod;

typedef enum						// Bevel type
{
	wsColoredLine = 0,				// Bevel with colored line
	wsGroove = 1,					// Bevel with groove
	wsFacet = 2						// Bevel with facet
} BevelType;

typedef enum						// Design Frame type
{
	wsNoDesign = 0,					// No design (default)
	wsMetalDesign = 1,				// Metal frame
	wsWideMetalDesign = 2,			// Wide metal frame
	wsMetalDualRimDesign = 3,		// Metal dual rim frame
	wsSemiRimlessDesign = 4,		// Semi-rimless metal frame
	wsWideSemiRimlessDesign = 5,	// Wide rim semi-rimless metal frame
	wsRimlessDesign1 = 6,			// Rimless metal frame design 1
	wsRimlessDesign2 = 7,			// Rimless metal frame design 2
	wsRimlessDesign3 = 8,			// Rimless metal frame design 3
	wsRimlessDesign4 = 9,			// Rimless metal frame design 4
	wsPlasticThinDesign = 10,		// Thin rim plastic frame
	wsPlasticWideDesign = 11,		// Wide rim plastic frame
	wsCustomDesign = 12				// custom virtual frame
} VirtualFrameDesign;

typedef enum						// Color Frame type
{
	wsWarmNeutralColor = 1,
	wsWarmBrightColor = 2,
	wsWarmGreenColor = 3,
	wsWarmRedColor = 4,
	wsWarmWoodColor = 5,
	wsWarmDarkColor = 6,
	wsCoolNeutralColor = 7,
	wsCoolBrightColor = 8,
	wsCoolVioletColor = 9,
	wsCoolBlueColor = 10,
	wsCoolDarkColor = 11
} VirtualFrameColor;

typedef enum						// Selects screen aspect ratio
{
	wsScrAspectRation_4to3 = 0,		// 4 to 3 screen aspect ratio
	wsScrAspectRation_5to4 = 1,		// 5 to 4 screen aspect ratio
	wsScrAspectRation_16to10 = 2,	// 16 to 10 screen aspect ratio
	wsScrAspectRation_Auto = 15		// Auto screen aspect ratio (square pixel)
} ScreenAspectRatioModes;

typedef enum						// Anti aliasing modes
{
	wsAANone = 0,					// None anti aliasing
	wsAAHardware = 1,				// Hardware anti aliasing
	wsAASoftware = 2				// Software anti aliasing

} AntialiasingModes;

typedef enum						// Quality modes
{
	wsHighSpeed = 0,				// High speed mode
	wsMediumQuality = 1,			// Medium quality mode
	wsHighQuality = 2				// High quality mode

} ObjectQualityModes;

typedef enum						// Show modes
{
	wsShowCalc = 1,			 	// Calculate scene
	wsShowDraw = 2,				 // Draw scene
	wsShowCalcDraw = 3			 	// Calculate and draw scene

} SceneShowModes;

class ISpectaclesEvents
{
public:
	virtual void Click() = 0;
	virtual void DblClick() = 0;
	virtual void MouseUp(int Button, int Shift, int X, int Y) = 0;
	virtual void TurnCompleted() = 0;
};

class ISpectacles :
	public IControlBase,
	public IEventPoint<ISpectaclesEvents>
{
public:
	// _DSpectacles methods
	virtual bool SpinStart() = 0;
	virtual bool SpinStop() = 0;
	virtual bool Turn(StartPosition PositionNumber, SpeedSettings Speed) = 0;
	virtual long Show(SceneShowModes mode = wsShowCalcDraw) = 0;
	virtual bool RefreshDStrings(bool bShow = true) = 0;

	// _DSpectacles properties
	virtual ILens * GetRightLens() = 0;
	virtual bool SetRightLens(ILens * propVal) = 0;
	virtual ILens * GetLeftLens() = 0;
	virtual bool SetLeftLens(ILens * propVal) = 0;
	virtual DetailLevel GetDetailLevel() = 0;
	virtual bool SetDetailLevel(DetailLevel propVal) = 0;
	virtual StartPosition GetStartPosition() = 0;
	virtual bool SetStartPosition(StartPosition propVal) = 0;
	virtual bool GetCrossSection() = 0;
	virtual bool SetCrossSection(bool propVal) = 0;
	virtual VirtualFrame GetVirtualFrame() = 0;
	virtual bool SetVirtualFrame(VirtualFrame propVal, bool bShow = true) = 0;
	virtual bool GetTemplesOpen() = 0;
	virtual bool SetTemplesOpen(bool propVal) = 0;
	virtual bool SetPicture(IWSPicture * propVal) = 0;
	virtual bool GetTestMode() = 0;
	virtual bool SetTestMode(bool propVal) = 0;
	virtual double GetPixelPerCMX() = 0;
	virtual bool SetPixelPerCMX(double propVal) = 0;
	virtual double GetPixelPerCMY() = 0;
	virtual bool SetPixelPerCMY(double propVal) = 0;
	virtual bool GetZoomed() = 0;
	virtual bool SetZoomed(bool propVal) = 0;
	virtual double GetTestRectHeight() = 0;
	virtual bool SetTestRectHeight(double propVal) = 0;
	virtual double GetTestRectWidth() = 0;
	virtual bool SetTestRectWidth(double propVal) = 0;
	virtual double GetRotationSpeed() = 0;
	virtual bool SetRotationSpeed(double propVal) = 0;
	virtual double GetBevelWidth() = 0;
	virtual bool SetBevelWidth(double propVal) = 0;
	virtual BevelPlacingMethod GetBevelPlacingMethod() = 0;
	virtual bool SetBevelPlacingMethod(BevelPlacingMethod propVal) = 0;
	virtual double GetBevelPosition() = 0;
	virtual bool SetBevelPosition(double propVal) = 0;
	virtual unsigned long GetBevelColor() = 0;
	virtual bool SetBevelColor(unsigned long propVal) = 0;
	virtual IDStringCollection * GetDStringCollection() = 0;
	virtual bool SetDStringCollection(IDStringCollection * propVal) = 0;
	virtual bool IsSpinning() = 0;
	virtual bool GetBevelVisible() = 0;
	virtual bool SetBevelVisible(bool propVal) = 0;
	virtual BevelType GetBevelType() = 0;
	virtual bool SetBevelType(BevelType propVal) = 0;
	virtual double GetBevelHeight() = 0;
	virtual bool SetBevelHeight(double propVal) = 0;
	virtual bool GetBevelPaint() = 0;
	virtual bool SetBevelPaint(bool propVal) = 0;

	// _DSpectaclesEx methods
	virtual bool IsLensesAndFrameSeparated() = 0;
	virtual bool SeparateLensesAndFrame(bool propVal, SpeedSettings Speed) = 0;
	virtual bool GetOrientationAngles(double *x, double *y, double *z) = 0;
	virtual bool SetOrientationAngles(double x, double y, double z, SpeedSettings Speed) = 0;

	// _DSpectaclesEx properties
	virtual VirtualFrameDesign GetVirtualFrameDesign() = 0;
	virtual bool SetVirtualFrameDesign(VirtualFrameDesign propVal, bool bShow = true) = 0;
	virtual VirtualFrameColor GetVirtualFrameColor() = 0;
	virtual bool SetVirtualFrameColor(VirtualFrameColor propVal) = 0;
	virtual bool GetMinMaxThicknessMarkers() = 0;
	virtual bool SetMinMaxThicknessMarkers(bool propVal) = 0;
	virtual bool GetThicknessDisplay() = 0;
	virtual bool SetThicknessDisplay(bool propVal) = 0;
	virtual ScreenAspectRatioModes GetScreenAspectRatio() = 0;
	virtual bool SetScreenAspectRatio(ScreenAspectRatioModes propVal) = 0;
	virtual ObjectQualityModes GetObjectQuality() = 0;
	virtual bool SetObjectQuality(ObjectQualityModes propVal) = 0;
};

#ifndef _WINDOWS_
extern "C" {
#endif
AFX_EXT_DATA IARCoating * Create_IARCoating();
AFX_EXT_DATA ICoating * Create_ICoating();
AFX_EXT_DATA ICoatings * Create_ICoatings();
AFX_EXT_DATA IContourPoint * Create_IContourPoint();
AFX_EXT_DATA IContour * Create_IContour();
AFX_EXT_DATA IDString * Create_IDString();
AFX_EXT_DATA IDStringCollection * Create_IDStringCollection();
AFX_EXT_DATA ILensSurface * Create_ILensSurface();
AFX_EXT_DATA IMultifocalSegment * Create_IMultifocalSegment();
AFX_EXT_DATA IMultifocalSegmentCollection * Create_IMultifocalSegmentCollection();
AFX_EXT_DATA IProgressive * Create_IProgressive();
AFX_EXT_DATA ITintPoint * Create_ITintPoint();
AFX_EXT_DATA ITint * Create_ITint();
AFX_EXT_DATA ILensEngine * Create_ILensEngine();
AFX_EXT_DATA IWSPicture * Create_IWSPicture();
AFX_EXT_DATA ILens * Create_ILens();
#ifdef _WINDOWS_
AFX_EXT_DATA ISpectacles * Create_ISpectacles(HWND hWndParent, int x, int y, int width, int height);
#else
AFX_EXT_DATA ISpectacles * Create_ISpectacles(/*Display* display, Window parent,*/ int x, int y, int width, int height);
#endif
#ifndef _WINDOWS_
}
#endif
