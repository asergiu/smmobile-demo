#pragma once
// WSLensEngine.h : header file
//

#include "ObjectBase.h"
#include "Parameters/Asph2.h"
#include "RayTracer.h"

/////////////////////////////////////////////////////////////////////////////
// CWSLensEngine command target

#define WhatEngineMakesSmth(Mask, BitOfInterest) \
	(!(~(Mask) & (BitOfInterest)) + 1)
#define IsThereSupport(Mask, FunctionBit)\
	((Mask) & (FunctionBit))

// All the OLE-defined 
// FACILITY_ITF codes have a code value which lies in the region 0x0000-0x01FFF.
// While it is legal for the definer to use any code, it is highly recommended
// that only code values in the range 0x0200-0xFFFF be used, as this will reduce the
// possiblity of accidental confusion with any OLE-defined errors.
#define ENGINE_E_FIRST	   MAKE_SCODE(SEVERITY_ERROR, FACILITY_ITF, 0x0500)

#define ENGINE_E_GENERAL	(ENGINE_E_FIRST + 0x0)

class CWSLensEngine :
	public CObjectBase<ILensEngine>
{
public:
	static CWSLensEngine* CreateObject();
	ILensEngine * GetInterface();

protected:
	CWSLensEngine();		   // protected constructor used by dynamic creation

	int nLastErrorCode;
	void SetLastErrorCode(int n);
	int GetLastErrorCode(void);
	void SetLastErrorThrowException(int n, int nErrorCodeOverride = -1);
	void ThrowExceptionWithKnownErrorCode();
	bool asph_weight_m(double *x,double *y,int n_p,double _x0,double _y0,
					   double *th_min, double *th_max, double *th_cen,double density,
					   double *ComputedWeight, double BlankR);//,

// Attributes
protected:
	CWSLensModel LensModel;
	lens_inf Lns_f;

	double m_Sph, m_Cyl, m_Add, m_RefIndex, m_Density,
		
		m_CenterX, m_CenterY,							//���������� ����� �����
		m_BlankCenterX, m_BlankCenterY,					//(��������������) ����� ���������

		m_OptToGeomCenterDisplacementX, m_OptToGeomCenterDisplacementY, 
														//�������������� ����� ��������� ������������
														//����������� ��� ������ ���������

		m_Prism,										//�������� �������������� �����
		m_EllipticVertToHorizRatio,						//�������� ������������� �����

		m_PermittedCenterThickness,
		m_PermittedEdgeThickness,
		m_PermittedZeroThickness,
		m_MinDiam;
	long m_Axis, m_Design, m_KindOfMaterial, m_Diameter;
	double	m_PrismAxis;								//�������� �������������� �����
	BOOL	m_EllipticBlank;							//�������� ������������� �����
	BOOL	m_LeftSide;
	double m_Weight, m_MinEdgeThickness, m_MaxEdgeThickness, m_CenterThickness;

	int nWeAreHere;

// Operations
public:
	bool CalcSurface(ILens * LensObject, ILensSurface * LensSurface);
	bool CalcWeight(ILens * LensObject, double* ResultWeight);
	bool CalcThickness(ILens * LensObject, double* ResultCenterThickness, double* ResultMinEdgeThickness, double* ResultMaxEdgeThickness);
	bool CalcUVProtection(ILens * LensObject, double* ResultUVProtection);
	unsigned long GetSupportedMethods();
	string GetLastErrorMessage();
	ILensEngine * Clone();

	bool PrepareLensData(ILens* LensObj);
	bool WeightByExternalSurface(ILens* LensObj, ILensEngine *LensEngine);
	bool ThicknessAndDiamByExternalSurface(ILens* LensObj, ILensEngine *LensEngine);
	
	void GetDepth( ILens* LensObject, double x, double y, double & rear, double & front );

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);

protected:
	virtual ~CWSLensEngine();

	virtual void FinalRelease()
	{
		delete this;
	}

	//
	// ��� ���� �������
	//

	bool _CalcSurface(ILens* LensObject, ILensSurface* LensSurface);
	bool _CalcWeight(ILens* LensObject, double *pResultWeight);
	bool _CalcThickness(ILens* LensObject, double *ResultCenterThickness, double *ResultMinEdgeThickness, double *ResultMaxEdgeThickness);
	unsigned long _GetSupportedMethods();
	string _GetLastErrorMessage();
	bool _CalcUVProtection(ILens* LensObject, double* ResultUVProtection);
	int ConvertToWSDesign(LPCTSTR lpstrDesign);
	double MaxContR(double *x,double *y,int np,double x0, double y0, double EllipticVertToHorizRatio);
	void _GetDepth( ILens* LensObject, double x, double y, double & rear, double & front );
};
