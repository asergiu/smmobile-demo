#include "stdafx.h"
#include "Asph2.h"

#define W_MIN	0.74
#define GL 	0
#define PL 	1
#define LNCZC (lns->z_coeff)

// ����� � �� ���������� ����� � ���� 
// ��� ���������� ������������
////double der_rad_tor(double x,double y);

//double density[]={2.65,1.32,1.34,1.32,3.2};
//double _n_e[]={1.604,1.502,1.597,1.502,1.706};
//int material[]={GL,PL,PL,PL,GL};

int CWSLensModel::exists(int type,double s1,int d)
{
	int s=(int)(s1*100.);
	switch(type){
		case COSMOLUX:
				if(d==62 && s<=800 && s>=-1000)
					return 1;
				break;
		case COSMOLIT15:
				switch (d){
					case 66:
							if(s<=1000 && s>=0)
								return 1;
							break;
					case 70:
							if((s<=800 && s>=0)||(s>=-1000 && s<=-625))
								return 1;
							break;
					case 74:
							if((s<=600 && s>=0)||(s>=-600 && s<=-325))
								return 1;
							break;
					case 80:
							if((s<=400 && s>=0)||(s>=-300 && s<=0))
								return 1;
							break;
					}
				break;
		case COSMOLIT16:
				if(d==66 && s<=1000 && s>=-1000)
					return 1;
				break;
		case PERFASTAR:
				if(d==66 && s<=2200 && s>=800)
					return 1;
				break;
		case LENTILUX:
				if(d==70 && s<=-625 && s>=-2400)
					return 1;
				break;
		}
	return 0;
}
//*******************************************************************
double CWSLensModel::z__min(double x,double y)
{
	double r=sqrt( x*x+y*y ),del_r;
	if(r>(lns->_r_org)){
		del_r=r-(lns->_r_org);
		if((lns->_type)==LENTILUX&&(lns->_tor)){
			x*=(lns->_r_org)/r;
			y*=(lns->_r_org)/r;
			del_r*=der_rad_tor(x,y)/(LNCZC);
			r=(lns->_r_org);
			}
		else
			del_r*=der_min(r=(lns->_r_org))/(LNCZC);
		}
	else{
		del_r=0;
		}
    if((lns->_type)==COSMOLUX||(lns->_type)==COSMOLIT15||(lns->_type)==COSMOLIT16) {
//		return (del_r+(((lns->A3)*r+(lns->A2))*r+(lns->A1))*r+(lns->A0))
//				*(LNCZC);
		return (del_r+(((lns->A3)*r+(lns->A2))*r/*+(lns->A1)*/)*r+(lns->A0)-0.5)
			*(LNCZC);
    }
    else if((lns->_type)==PERFASTAR) {
		if(r<(lns->_OZ))
			return (del_r+((((lns->A4)*r+(lns->A3))*r+(lns->A2))*r+(lns->A1))*r+(lns->A0))
					*(LNCZC);
		else
			return (del_r+((lns->B2)*r+(lns->B1))*r+(lns->B0))
					*(LNCZC);
    }
    else if((lns->_type)>=LENTILUX) {
		if((lns->_tor)&&(lns->_type)==LENTILUX){
			double temp=(r>EPS?atan2(y,x):0)+(lns->_angle)+(lns->_ang_sh),temp2;
			x=r*cos(temp);                 //////
			y=r*sin(temp);
			if((temp2=(lns->_r)*(lns->_r)-y*y)<0)
				return -1;
			temp=(lns->_R)+sqrt(temp2);
			if((temp2=temp*temp-x*x)<0)
				return -1;
			return (del_r+(lns->_r)+(lns->_R)-sqrt(temp2))*(LNCZC);
			}
		else{
			double t=(lns->_s_r)*(lns->_s_r)-r*r;
			if(t>=0)
				return (del_r+(lns->_s_r)-sqrt(t))*(LNCZC);
			else
				return -1;
			}
    }
	return -1;
}
//*******************************************************************
double CWSLensModel::z_max(double x,double y)
{
	x += RearSurfOffsetX;
	y += RearSurfOffsetY;
	double r=sqrt( x*x+y*y ),del_r;
	if(r>(lns->_r_org)){
		del_r=r-(lns->_r_org);
		if((lns->_type)!=LENTILUX&&(lns->_tor)){
			x*=(lns->_r_org)/r;
			y*=(lns->_r_org)/r;
			del_r*=der_rad_tor(x,y)/(LNCZC);
			r=(lns->_r_org);
			}
		else
			del_r*=der_max(r=(lns->_r_org))/(LNCZC);
		}
	else{
		del_r=0;
		}

    if((lns->_type)!=LENTILUX) {
		if((lns->_tor)){
			double temp=(r>EPS?atan2(y,x):0.)+(lns->_angle)+(lns->_ang_sh),temp2;
			x=r*cos(temp);                 //////
			y=r*sin(temp);
			if((temp2=(lns->_r)*(lns->_r)-y*y)<0)
				return -1;
			temp=(lns->_R)+sqrt(temp2);
			if((temp2=temp*temp-x*x)<0)
				return -1;
			return (del_r+(lns->_d)+(lns->_r)+(lns->_R)-sqrt(temp2))
					*(LNCZC);
			}
		else{
			double t=(lns->_r)*(lns->_r)-r*r;
			if(t>=0)
//			return del_r+(lns->_s_r)+(lns->_d)-sqrt((lns->_s_r)*(lns->_s_r)-r*r);
				return (del_r+(lns->_r)+(lns->_d)-sqrt(t))*(LNCZC);
			else
				return -1;
			}
    }
    else if((lns->_type)==LENTILUX) {
		if(r<(lns->_OZ))
			return (del_r+(lns->_d)+(((lns->A3)*r+(lns->A2))*r+(lns->A1))*r+(lns->A0))
					*(LNCZC);
		else
			return (del_r+(lns->_d)+(((((lns->B5)*r+(lns->B4))*r+(lns->B3))*r+(lns->B2))*r+(lns->B1))*r+(lns->B0))
					*(LNCZC);
    }

	return  -1;
}
//*******************************************************************
double CWSLensModel::der_min(double r)    //����� � �� �ந������� ����� � ����  ���
{                           //������ �����᪨� �����孮�⥩

	if(r<0)
		r=-r;
	if(r>(lns->_r_org))
		r=(lns->_r_org);
    if((lns->_type)==COSMOLUX||(lns->_type)==COSMOLIT15||(lns->_type)==COSMOLIT16) {
		return ((3*(lns->A3)*r+2*(lns->A2))*r+(lns->A1))*(LNCZC);
    }
    else if((lns->_type)==PERFASTAR) {
		if(r<(lns->_OZ))
			return (((4*(lns->A4)*r+3*(lns->A3))*r+2*(lns->A2))*r+(lns->A1))
					*(LNCZC);
		else
			return (2*(lns->B2)*r+(lns->B1))*(LNCZC);
    }
    else if((lns->_type)>=LENTILUX) {
		return (r/sqrt((lns->_s_r)*(lns->_s_r)-r*r))*(LNCZC);
    }
	return 0;
}
//*******************************************************************
int CWSLensModel::tor_der_xy(double x1,double y1,double *dx,double *dy)
							//����� � �� � ��� ��-�� ��� �� �� �⭮��
							//��  � � � ���� �窥
{
	double t1,t2,x=x1,y=y1;
	if((t1=(lns->_r)*(lns->_r)-y*y)<0)
		return 0;
	t2=(lns->_R)+(t1=sqrt(t1));
	if((t2=t2*t2-x*x)<0)
		return 0;
	*dx=(x/(t2=sqrt(t2)))*(LNCZC);
	*dy=(((lns->_R)*y/t1+y)/t2)*(LNCZC);
	return 1;

}
//*******************************************************************
double CWSLensModel::der_rad_tor(double x,double y)  //����� � �� �ந������� ����� � ���� 
									   //��� ���᪨� �����孮�⥩

{
	double r=sqrt( x*x+y*y ),
		   temp=(r>EPS?atan2(y,x):0)+(lns->_angle)+(lns->_ang_sh),cx,cy;
	cx=cos(temp);
	cy=sin(temp);
	x=r*cx;
	y=r*cy;
	if(tor_der_xy(x,y,&x,&y))
		return x*cx+y*cy;
	else
		return 1000;


}
//*******************************************************************
double CWSLensModel::der_max(double r)    //����� � �� �ந������� ����� � ����  ���
							//���孨� �����᪨� �����孮�⥩

{

	if(r>(lns->_diam_org)/2.)
		r=(lns->_diam_org)/2.;
    if((lns->_type)!=LENTILUX) {
		if((lns->_type)!=SPHERIC)
			return (r/sqrt((lns->_s_r)*(lns->_s_r)-r*r))*(LNCZC);
		else
			return (r/sqrt((lns->_r)*(lns->_r)-r*r))*(LNCZC);
    }
    else if((lns->_type)==LENTILUX) {
		if(r<(lns->_OZ))
			return ((3*(lns->A3)*r+2*(lns->A2))*r+(lns->A1))*(LNCZC);
		else
			return ((((5*(lns->B5)*r+4*(lns->B4))*r+3*(lns->B3))*r+2*(lns->B2))*r+(lns->B1))
					*(LNCZC);
    }
	return  0;
}
//*******************************************************************
void CWSLensModel::der_min_xy(double x,double y,double *dx,double *dy)
{ 						// ����� � �� � ��� �ந������ � � � ����
						// �窥 ������ �����孮�� �����
	double r=sqrt(x*x+y*y),cx,cy;
	if(r<EPS){
		cx=1;
		cy=1;
		}
	else{
		cx=x/r;
		cy=y/r;
		}
	if((lns->_type)!=LENTILUX || !(lns->_tor)){
		*dx=der_min(r)*cx;
		*dy=der_min(r)*cy;
		}
	else{
		double temp2=(lns->_angle)+(lns->_ang_sh),temp=(r>EPS?atan2(y,x):M_PI/2)+temp2,dxb,dyb;
		cx=cos(temp2);
		cy=sin(temp2);
		x=r*cos(temp);                 //////
		y=r*sin(temp);
		tor_der_xy(x,y,&dxb,&dyb);
		*dx=cx*dxb+cy*dyb;
		*dy=cx*dyb-cy*dxb;
		}

}


void CWSLensModel::der_max_xy(double x,double y,double *dx,double *dy)
{ 						// ����� � �� � ��� �ந������ � � � ����
						// �窥 ������ �����孮�� �����
	double r=sqrt(x*x+y*y),cx,cy;
	if(r<EPS){
		cx=1;
		cy=1;
		}
	else{
		cx=x/r;
		cy=y/r;
		}
	if((lns->_type)==LENTILUX || !(lns->_tor)){
		*dx=der_max(r)*cx;
		*dy=der_max(r)*cy;
		}
	else{
		double temp2=(lns->_angle)+(lns->_ang_sh),temp=(r>EPS?atan2(y,x):M_PI/2)+temp2,dxb,dyb;
		cx=cos(temp2);
		cy=sin(temp2);
		x=r*cos(temp);                 //////
		y=r*sin(temp);
		tor_der_xy(x,y,&dxb,&dyb);
		*dx=cx*dxb+cy*dyb;
		*dy=cx*dyb-cy*dxb;
		}

}

bool CWSLensModel::IsBackSurfaceShiftValid(double x, double y)
{
	x += RearSurfOffsetX;
	y += RearSurfOffsetY;
	double r = _hypot(x, y);
	if((lns->_type)==LENTILUX)
	{
		if(r + lns->_r_org <(lns->_OZ))
			return true;
	}
	else
	{
		if(r + lns->_r_org < lns->_r * 0.8)
			return true;
	}
	return false;
}

double CWSLensModel::CalcFunc(double x, double y, bool *pArgValid)
{
	double CurRad = _hypot(x, y);
	if(!fRayTracerPrepared || !IsBackSurfaceShiftValid(x, y))//CurRad > lns->_r_org || 
	{
		*pArgValid = false;
		return 0.;
	}
	CLine3D ResLine;
	if(!RayTracer.TraceVertRay(0, 0, ResLine, PrecParam, x, y))
	{
		*pArgValid = false;
		return 0.;
	}
	*pArgValid = true;
	CVec3D ResVec(ResLine.vx, ResLine.vy, ResLine.vz);
	return acos(ReferVect.CosOfAngle(ResVec)); 
/*
	if(!fDataReady || _hypot(x, y) > LensRad)
	{
		*pArgValid = FALSE;
		return 0.;
	}
	CLine3D ResLine;
	if(!RayTracer.TraceVertRay(0, 0, ResLine, PrecParam, x, y))
	{
		*pArgValid = FALSE;
		return 0.;
	}
	*pArgValid = TRUE;
	CVec3D ResVec(ResLine.vx, ResLine.vy, ResLine.vz);
	return acos(RefVect.CosOfAngle(ResVec)); 
*/
//	return 0.;
}

bool CWSLensModel::CalcValues(CFtPointVector &InArray, CDoubleVector &FrontSurf, CDoubleVector &RearSurf)
{
	CFtPointVector::size_type i, NumOfPoints = InArray.size();
	
	RearSurf.resize(NumOfPoints);
	FrontSurf.resize(NumOfPoints);
	for(i = 0; i < NumOfPoints; i++)
	{
		RearSurf[i] = -z_max(InArray[i].x, InArray[i].y);;
		FrontSurf[i] = -z__min(InArray[i].x, InArray[i].y);
		assert(RearSurf[i] <= FrontSurf[i]);
	}

	return true;
}
