#ifndef __ASPH2_H__
#define __ASPH2_H__

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include "Thickness.h"
#include "../RayTracer.h"

//#ifndef M_PI
//const double M_PI = 3.14159265358979323846;
//#endif
const int _diam_table[]={62,80,66,66,70};

//#ifndef EPS
//const double EPS = 1.e-3;
//#endif

const int COSMOLUX		= 0;
const int COSMOLIT15	= 1;
const int COSMOLIT16	= 2;
const int PERFASTAR		= 3;
const int LENTILUX		= 4;
const int SPHERIC		= 5;

const int GL = 0;
const int PL = 1;

struct  lens_inf {
	double _sc_x,_sc_y,_dR,_d,_R,_r,_s_r,_r_main,_r_aux,_dpt,diop_main,
		   diop_aux,_x0,_y0,_angle,_ang_sh,
		   _OZ,A0,A1,A2,A3,A4,B0,B1,B2,B3,B4,B5;
	int _old,_type,_r_org,_diam,_diam_org,_tor,_mat;
	//ᯮᮡ ��� �����; ������� ���⨯�, ��⨭��, �ਧ��� ��筮��
	double z_coeff;
	ThicknessStruct Thickness;
	   };

typedef struct {
	double x, y, z_h, z_l;
	} Pnts;

typedef struct {
	double x, y, z;
	} Pnt;

class CWSLensModel : public CFuncOf2Args, public CLensSurfProducer
{
public:
	CWSLensModel(){DerErrFlg = 0;};

public:
	CRayTracer RayTracer;
	bool fRayTracerPrepared;
	double PrecParam;
	double CalcFunc(double x, double y, bool *pArgValid); // ��������� � ������������ �� CFuncOf2Args
	bool CalcValues(CFtPointVector &InArray, CDoubleVector &FrontSurf, CDoubleVector &RearSurf);
												// ��������� � ������������ �� CLensSurfProducer
	CVec3D ReferVect;	//������ ��� ��������� ����������� ���������� ��� :) � ������������ 
						//������� � ����
	double RearSurfOffsetX, RearSurfOffsetY; // ��������� ������ ������ ����������� ������������ ��������
											 // ��� �������������� ����
	bool IsBackSurfaceShiftValid(double x, double y);

	lens_inf *lns;
	double * aux_mer;
	double * main_mer;
	double _sc_x,_sc_y,_dR,_d,_R,_r,_s_r,_r_main,_r_aux,_dpt,diop_main,diop_aux,_Xo,_Yo,_angle,_ang_sh;
	double _OZ, A0,A1,A2,A3,A4, B0,B1,B2,B3,B4,B5;
	int _type,_r_org,_diam,_diam_org,_tor;
	int ErrFlg;
	int DerErrFlg;

	double z__min(double x,double y);   /* ���� */
	double z_max(double x,double y);   /* ���� */
	double der_min(double r);
	double der_rad_tor(double x,double y);  //�����頥� �ந������� ����� ࠤ���
	double  der_max(double r);
	void der_min_xy(double x,double y,double *dx,double *dy);
	void der_max_xy(double x,double y,double *dx,double *dy);
	int set_par(double index,int mat,double *x,double *y,int np,int type,double sph,double cyl,
					double angle,int r,double scale_x,double scale_y,double x0,double y0,ThicknessStruct * Thickness);
	void rads(double *x,double *y,int np,double x0, double y0,
				double *fR,double *sr,double *sR);
	double asph_weight(double *x,double *y,int n_p,double _x0,double _y0,
					   double *th_min, double *th_max, double *th_cen,double density);
	int tor_der_xy(double x1,double y1,double *dx,double *dy);
	double R(int type,double s,int diam);
	int  exists(int type,double s,int d);               /* ���� */

	void DefineShell2Par(double * buffer);
	int as_len_mark(int design, int mat, double index, double Sph);
	int set_par ( int type,double sph,double cyl,
					double angle,int r,double scale_x,double scale_y,double x0,double y0);
	double z_min(double x,double y);
	double z_max_shell2(double x,double y);
	double R_shell2(int type,double s,int diam);
	double OZ(int type,double s1);
	int base_index(int type,double s1);
	double _b_n(int n,int type,double s);
	double _a_n(int n,int type,double s);
	double a0(int type,double s);
	double a1(int type,double s);
	double a2(int type,double s);
	double a3(int type,double s);
	double a4(int type,double s);
	double b0(int type,double s);
	double b1(int type,double s);
	double b2(int type,double s);
	double b3(int type,double s);
	double b4(int type,double s);
	double b5(int type,double s);
	double DR(int type,double s,int d);
	double D(int type,double s,int d);
	double der_min_shell2(double r);
	double der_max_shell2(double r);
	double tr_ray(double x);
	int tr_prepare(void);
	double trace(double x,double y,double Index);
	double TraceM(double x,double y,double Index);
	double GetLenseRadius();

	double maxR(int Type,double *x,double *y,int np,double x0, double y0);
};

#endif
