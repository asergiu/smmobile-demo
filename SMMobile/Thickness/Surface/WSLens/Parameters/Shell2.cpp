#include "stdafx.h"
#include "Asph2.h"

void CWSLensModel::DefineShell2Par(double * buffer)
{
	main_mer = buffer;
	aux_mer = main_mer + 80;
}

const int places[5][14]={
		{4,4,4,5,5,5,10,8,12,8,8},
		{4,4,4,4,4,4,8,13,8,12,16},
		{4,4,4,4,4,4,6,5,10,8,12,8,8},
		{4,4,5,5,5,5,5,5,5,5,5,4},
		{8,8,8,8,8,8,8,16} // ��� �� ���冷�
	   };

const int dpt_th[5]={8,10,10,22,-24}, step[5]={-1,-1,-1,-1,1};

const int _old=1;

int CWSLensModel::as_len_mark(int design, int mat, double index, double Sph)
{
	int i;
	switch(design){
		case 0:
			return -1;
			break;
		case 1:
			switch(mat){
				case 0:
					if(index<1.55)
						i=1;
					else
						i=2;
					break;
				case 1:
				case 2:
					i=0;
					break;
				default:
					return -1;
					break;
				}
			break;
		case 2:
			if(Sph>0)
				i=3;
			else
				i=4;
			break;
/*
			switch(mat){
				case 0:
					i=3;
					break;
				case 1:
				case 2:
					i=4;
					break;
				default:
					return -1;
					break;
				}
			break;
*/
		default:
			return -1;
		}
	return i;
}

int CWSLensModel::set_par ( int type,double sph,double cyl,
				double angle,int r,double scale_x,double scale_y,double x0,double y0)
{
	double diop_min=sph,diop_max=sph,temp;
	int tor , i ;

	if ( type == -1 ) return ( 0 ) ;

//return ( 1 ) ;

	if(r>40)
		r=40;
	if(r<20)
		r=20;
//return ( 1 ) ;
	for(i=0;i<80;i++){
		main_mer[i]=0;
		aux_mer[i]=0;
		}
	_sc_x=scale_x;
	_sc_y=scale_y;
	_tor=0;
	_angle=angle;
	_Xo=0;
	_Yo=0;
	_type=type;
	if(type==LENTILUX)
		_ang_sh=M_PI/2;
	else
		_ang_sh=0;
	if(fabs(cyl)>EPS){
		tor=1;
		if(cyl<0)
			diop_min+=cyl;
		else
			diop_max+=cyl;
		}
	else
		tor=0;
	diop_main=diop_max;
	diop_aux=diop_min;
	if(diop_max<0&&tor==1){
		temp=diop_min;
		diop_min=diop_max;
		diop_max=temp;
		}
	_r_org=r;
	_diam_org=_diam=r*2;

	if(!exists(type,diop_max,_diam)){
		if(type!=COSMOLIT15)
			_diam=_diam_table[type];
		else{
			if(diop_max>=-EPS){
				int dms[]={66,70,74,80},last_diam=(base_index(type,diop_max)-1)>>1,
						min_dist=1000,min_ind=-1;
				for( i=0;i<=last_diam;i++){
					if(abs(_diam_org-dms[i])<min_dist){
						min_ind=i;
						min_dist=abs(_diam_org-dms[i]);
						}
					}
				_diam=dms[min_ind];
				}
			else if(diop_max>=-3-EPS)
				_diam=80;
			else if(diop_max>=-6-EPS)
				_diam=74;
			else
				_diam=70;
			}
		}
/////////////////////////
//	_r_org=_diam/2;
//	_diam_org=_diam;
////////////////////////////
	if(exists(type,diop_max,_diam)){
		A0=a0(type,diop_max);
		A1=a1(type,diop_max);
		A2=a2(type,diop_max);
		A3=a3(type,diop_max);
		A4=a4(type,diop_max);
		B0=b0(type,diop_max);
		B1=b1(type,diop_max);
		B2=b2(type,diop_max);
		B3=b3(type,diop_max);
		B4=b4(type,diop_max);
		B5=b5(type,diop_max);
		_OZ=OZ(type,diop_max);
		_dR=DR(type,diop_max,_diam);
		_d=D(type,diop_max,_diam);
		_r=R_shell2(type,diop_max,_diam);
		_R=0;
		_s_r=_r;
		if(_diam!=_diam_org){
			if(diop_max>=-EPS)
				_d-=z_max_shell2(_diam_org/2.,0)-z_min(_diam_org/2.,0)-_dR;
			else
				_dR=z_max_shell2(_diam_org/2.,0)-z_min(_diam_org/2.,0);
			}
		if(tor==1){
			double r_max=_r,
				   r_min;
			int i=1,n_0=0,n=places[type][0],n_1=n-1,
					stp=step[type],thr=dpt_th[type],
					s_min=(int)(stp*4.0*(diop_min-thr)),
					s_max=(int)(stp*4.0*(diop_max-thr));
			while(s_max>=n){
				n_0=n;
				n+=places[type][i++];
				n_1=n-1;
				}
			if(type==COSMOLIT15&&_diam!=80&&n_0==32)
				n_1=39;
			if(s_min<=n_1&&s_min>=n_0)
				r_min=R_shell2(type,diop_min,_diam);
			else{
				double diop0=thr+n_0/(4.0*stp),diop1=thr+n_1/(4.0*stp),
						d0=525/R_shell2(type,diop0,_diam),d1=525/R_shell2(type,diop1,_diam),
						b=(d1-d0)/(n_1-n_0),a=d0-b*n_0;
				r_min=525/(a+b*s_min);
				}
			_tor=1;
			_R=fabs(r_min-r_max);
            _r=min(r_min,r_max);
			_s_r=r_max;
			}
		_r_main=_R+_r;  ///����������������������
		_r_aux=_r;
//reset_video();
//printf("R_main=%g,\nr_aux=%g\n",_r_main,_r_aux);
		_dpt=diop_max;
/*
		if(_old){
			m=sph_weight(type,diop_max,_diam,x0,y0,x,y);
			if(_tor){
				temp=tor_weight(x,y,density[type]);
				if(type!=LENTILUX)
					m+=temp;
				else
					m-=temp;
				}
			}
		else{
			m=tor_weight(x,y,density[type]);
			}
*/
		if(tr_prepare()==0){
			return 0;
			}
		_Xo=x0;
		_Yo=y0;
		return 1;
		}
	else
		return 0;
}

const double _n_e[]={1.604,1.502,1.597,1.502,1.706};

double CWSLensModel::z_min(double x,double y)
{
	double r=sqrt( (x-_Xo)*(x-_Xo)+(y-_Yo)*(y-_Yo) ),del_r,tmp;
	if(r>EPS+_diam_org/2.&&!_old){               //��� lentilux+tor
		del_r=r-_diam_org/2.;
		del_r*=der_min_shell2(r=_diam_org/2.);
		}
	else{
		del_r=0;
		}
//	if(!exists(type,s,d))
//		return -1;
//	if((r=sqrt( (x-x0)*(x-x0)+(y-y0)*(y-y0) ))>d/2)
//		return -1;
    if(_type==COSMOLUX||_type==COSMOLIT15||_type==COSMOLIT16) {
		return del_r+((A3*r+A2)*r+A1)*r+A0;
    }
    else if(_type==PERFASTAR) {
		if(r<_OZ)
			return del_r+(((A4*r+A3)*r+A2)*r+A1)*r+A0;
		else
			return del_r+(B2*r+B1)*r+B0;
    }
    else if(_type==LENTILUX) {
		if(_tor){
			double temp=atan2(y-_Yo,x-_Xo)+_angle+_ang_sh;
			x=r*cos(temp);                 //////
			y=r*sin(temp);
			temp=cos(asin(y/_r));
			return _r+_R-(_R+_r*temp)*cos(asin(x/(_R+_r*temp)));
			}
		else{
			tmp=_s_r*_s_r-r*r;
			if(tmp>=0)
				return del_r+_s_r-sqrt(tmp);
			}
    }
	return -1;
}

//double z_max(int type,double s,int d,double x0,double y0,
//				double x,double y)

double CWSLensModel::z_max_shell2(double x,double y)
{
	double r=sqrt( (x-_Xo)*(x-_Xo)+(y-_Yo)*(y-_Yo) ),del_r,tmp;
	if(r>EPS+_diam_org/2.&&!_old){               //��� !lentilux+tor
		del_r=r-_diam_org/2.;
		del_r*=der_min_shell2(r=_diam_org/2.);
		}
	else{
		del_r=0;
		}

//	if(!exists(type,s,d))
//		return -1;
//	if((r=sqrt( (x-x0)*(x-x0)+(y-y0)*(y-y0) ))>d/2)
//		return -1;
    if(_type==COSMOLUX||_type==COSMOLIT15||_type==COSMOLIT16||_type==PERFASTAR) {
		if(_tor){
			double temp=atan2(y-_Yo,x-_Xo)+_angle+_ang_sh;
			x=r*cos(temp);                /////
			y=r*sin(temp);
			temp=cos(asin(y/_r));
			return _d+(_r+_R-(_R+_r*temp)*cos(asin(x/(_R+_r*temp))));
			}
		else{
			tmp=_s_r*_s_r-r*r;
			if(tmp>=0)
				return _s_r+_d-sqrt(tmp);
			else
				return -1;
			}
    }
    else if(_type==LENTILUX) {
		if(r<_OZ)
			return del_r+_d+((A3*r+A2)*r+A1)*r+A0;
		else
			return del_r+_d+((((B5*r+B4)*r+B3)*r+B2)*r+B1)*r+B0;
    }
	return  -1;
}

double CWSLensModel::R_shell2(int type,double s,int diam)
{
	double z,_d=D(type,s,diam),_dR=DR(type,s,diam);

	if(type==COSMOLUX||type==PERFASTAR||type==COSMOLIT16||type==COSMOLIT15){
		z=z_min(diam/2.,0);
		return (2*(z*_d+_dR*_d-z*_dR)-diam*diam/4.0-z*z-_dR*_dR-_d*_d)/(2*(_d-_dR-z));
		}
	else if(type==LENTILUX){
		z=z_max_shell2(diam/2.,0);
		return (2*z*_dR-diam*diam/4.0-z*z-_dR*_dR)/(2*(_dR-z));
		}
	else
		return -1;
}

double CWSLensModel::OZ(int type,double s1)
{
	if(type==PERFASTAR)
		return 21;
	else if(type==LENTILUX){
		int s=(int)(s1*100);
		if(s>=-1000)
			return 35/2.0;
		else if(s>=-1200)
			return 33/2.0;
		else if(s>=-1400)
			return 31/2.0;
		else if(s>=-1600)
			return 29/2.0;
		else if(s>=-1800)
			return 27/2.0;
		else
			return 25/2.0;
		}
	else
		return -1;
}

/*-----------------------------------------------------------------*/
/* ���� ������ -- ⨯ �����:
0 COSMOLUX
1 COSMOLIT15D66
2 COSMOLIT15D70PL
3 COSMOLIT15D70MI
4 COSMOLIT15D74PL
5 COSMOLIT15D74MI
6 COSMOLIT15D80PL
7 COSMOLIT15D80MI
8 COSMOLIT16
9 PERFASTAR
10 LENTILUX
			 ���
COSMOLUX	0
COSMOLIT15	1
COSMOLIT16	2
PERFASTAR	3
LENTILUX	4

*/
const double _a0[5][20]={
		{1e-15, 7.87, 8.53,0.722,-2.93,5.05,5.72,1.24,-4.77,2.76,-0.305,2.86},
		{1e-15, /*6.98,*/3.93,-2.71,12.2,-0.486,-1.1,5.27,-0.361,4.23,6.05,1.01,1.29},
		{1e-15,/*1.65,*/9.45,0.169,6.99,8.23,10.1,11.3,1.48,5.23,-0.486,6.05,1.01,-0.694,3.65},
		{0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0}
	   };

const double _a1[5][20]={
		{1e-2,-2.29,-2.33,-2.30,-2.25,-2.15,-1.95,-1.85,-1.72,-1.85,-1.92,-1.87},
		{1e-2,/*0.881,*/-0.633,-1.03,-1.2,-1.7,-1.91,-2.04,-2.02,-1.87,-1.72,-1.72,-1.94},
		{1e-2,/*-1.44,*/-1.27,-1.51,-1.57,-1.84,-1.93,-1.94,-1.83,-1.79,-1.7,-1.72,-1.72,-1.91,-1.87},
		{0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0}
	   };

const double _a2[5][20]={
		{1e-3,8.96,8.3,7.51,6.68,5.97,5.14,4.64,3.98,3.17,2.58,2.33},
		{1e-3,/*9.02,*/9.65,9.25,8.56,8.21,7.54,6.76,5.81,4.63,3.45,2.61,2.32},
		{1e-3,/*10.2,*/9.44,8.96,8.28,7.79,7.14,6.37,5.64,4.98,4.46,3.45,2.61,1.99,1.95},
		{1e-2,1.856,1.815,1.748,1.674,1.61,1.532,1.462,1.383,1.304,1.11,1.118,1.053},
		{1e-2,1.6874,1.5591,1.4219,1.2828,1.1479,1.0120,0.87784,0.74058}
	   };

const double _a3[5][20]={
		{1e-5,-2.03,-2.4,-2.49,-2.32,-1.79,/*-9.67,-5.53,*/ -1.00 , -0.43 ,0.0175,0.0207,-0.467,-0.593},
		{1e-5,/*7.56,*/0.335,2.04,1.51,-0.0552,-0.91,-1.43,-1.39,-0.659,0.0207,0.143,0.462},
		{1e-5,/*1.3,*/1.67,0.8,0.485,-0.469,-0.936,-1.05,-0.642,-0.477,-0.0432,0.0207,0.143,-0.0248,-0.611},
		{1e-6,-31.19,-28.16,-21.74,-17.10,-14.21,-7.176,-2.391,-9.556,-5.339,-3.441,-3.242,-4.584},
		{1e-5,11.633,8.9437,7.2733,5.8335,4.1231,2.9171,1.6118,0.84115}
	   };
const double _a4[]={1.e-6,5.867,5.547,4.673,4.010,3.642,3.061,2.493,2.341,1.951,1.617,1.276,1.148};

const double _b0[2][20]={
		{1,-24.6311,-25.0683,-24.6369,-23.6144,-22.1030,-20.8964,-20.2705,-18.4919,-15.9582,-13.5039,-11.0591,-8.5389},
		{1,17.9737,17.3635,15.0538,20.2239,21.7631,29.2997,23.8751,11.688}
	   };

const double _b1[2][20]={
		{1,2.2405,2.2877,2.2623,2.1767,2.0390,1.9329,1.8829,1.7197,1.4842,1.2562,1.0301,0.7933},
		{1,-4.9904,-4.8338,-4.2568,-5.2324,-5.4045,-6.7967,-5.1662,-2.1456}
	   };

const double _b2[2][20]={
		{1e-2,-3.0348,-3.2087,-3.2778,-3.1948,-2.9569,-2.8137,-2.8025,-2.5297,-2.0705,-1.6355,-1.23,-0.74676},
		{1,0.5312,0.5162,0.4618,0.5183,0.5142,0.6056,0.428,0.1443}
	   };

const double _b3[]={
		1e-2,-2.3829,-2.3401,-2.1242,-2.2193,-2.1395,-2.4201,-1.5657,-0.32719
	   };

const double _b4[]={
		1e-4,4.916,4.8797,4.4845,4.3678,4.1166,4.5269,2.6539,0.11608
	   };

const double _b5[]={
		1e-6,-3.8889,-3.8965,-3.6220,-3.2755,-3.0284,-3.2579,-1.7252,0.25398
	   };



int CWSLensModel::base_index(int type,double s1)
{
	int i=1,n=places[type][0],s=(int)(step[type]*4.0*(s1-dpt_th[type]));
	while(s>=n){
		n+=places[type][i++];
		}
	return i;

}

double CWSLensModel::_b_n(int n,int type,double s)
{
	int b_ind=base_index(type,s);
	const double (*_b)[20];
	switch(n){
		case 0: _b=_b0; break;
		case 1: _b=_b1; break;
		case 2: _b=_b2; break;
		default: return 0;
		}

	if(type==PERFASTAR||type==LENTILUX)
		return _b[type-PERFASTAR][b_ind]*_b[type-PERFASTAR][0];
	else
		return 0;
}

double CWSLensModel::_a_n(int n,int type,double s)
{
	int b_ind=base_index(type,s);
	const double (*_a)[20];
	switch(n){
		case 0: _a=_a0; break;
		case 1: _a=_a1; break;
		case 2: _a=_a2; break;
		case 3: _a=_a3; break;
		default: return 0;
		}

	//if(type!=COSMOLIT15&&type!=COSMOLIT16)
	return _a[type][b_ind]*_a[type][0];
/*
	else{
		double a=inter_a[type-COSMOLIT15][_index[type-COSMOLIT15][b_ind-1]],
		b=inter_b[type-COSMOLIT15][b_ind-1],
		c=inter_a[type-COSMOLIT15][_index[type-COSMOLIT15][b_ind-1]+1],
		f_a=_a[type][_index[type-COSMOLIT15][b_ind-1]+1]*_a[type][0],
		f_c=_a[type][_index[type-COSMOLIT15][b_ind-1]+2]*_a[type][0];
		return (c-b)*f_a/(c-a)+(b-a)*f_c/(c-a);
		}
*/
}

double CWSLensModel::a0(int type,double s)
{
	return _a_n(0,type,s);
}


double CWSLensModel::a1(int type,double s)
{
	return _a_n(1,type,s);
}

double CWSLensModel::a2(int type,double s)
{
	return _a_n(2,type,s);
}

double CWSLensModel::a3(int type,double s)
{
	return _a_n(3,type,s);
}

double CWSLensModel::a4(int type,double s)
{
	int b_ind=base_index(type,s);
	if(type==PERFASTAR)
		return _a4[b_ind]*_a4[0];
	else
		return 0;
}

double CWSLensModel::b0(int type,double s)
{
	return _b_n(0,type,s);
}

double CWSLensModel::b1(int type,double s)
{
	return _b_n(1,type,s);
}

double CWSLensModel::b2(int type,double s)
{
	return _b_n(2,type,s);
}

double CWSLensModel::b3(int type,double s)
{
	int b_ind=base_index(type,s);

	if(type==LENTILUX)
		return _b3[b_ind]*_b3[0];
	else
		return 0;
}

double CWSLensModel::b4(int type,double s)
{
	int b_ind=base_index(type,s);

	if(type==LENTILUX)
		return _b4[b_ind]*_b4[0];
	else
		return 0;
}

double CWSLensModel::b5(int type,double s)
{
	int b_ind=base_index(type,s);

	if(type==LENTILUX)
		return _b5[b_ind]*_b5[0];
	else
		return 0;
}

const double dr_c[] = {0.4,0.39,0.37,0.42,0.4,0.39,0.43,0.42,0.44,0.43,0.48,0.47,0.49,0.48,0.54,0.54,0.6,0.6,0.67,0.68,0.7,0.78,0.82,0.92,0.96,1.07,1.14,
	1.25,1.34,1.51,1.63,1.83,1.99,2.31,2.37,2.49,2.57,2.64,2.73,2.9,3.02,3.14,3.33,3.47,3.68,3.76,3.97,4.12,4.35,4.51,4.74,4.91,5.15,5.33,5.88,5.77,
	6.02,6.09,6.28,6.54,6.74,7.01,7.21,7.48,7.69,7.95,8.24,8.46,8.75,8.97,9.28,9.51,9.83};
const double dr_c15_66[] = {0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.9,0.9,0.9,0.9,1.,1.,1.,1.,1.1,1.1,1.4,1.2,1.2,1.3,1.3,1.4,1.5,1.6,1.7,1.7,1.8,1.9,2.1,2.3,2.45};
const double dr_c15_70pl[] = {0.7,0.7,0.7,0.7,0.7,0.8,0.8,0.8,0.8,0.8,0.8,0.9,0.9,0.9,1.0,1.0,1.,1.,1.1,1.1,1.2,1.2,1.3,1.3,1.5,1.5,1.6,1.7,1.8,1.9,2.0,2.3, 2.45  
/*,10.42,10.79,11.18,11.66,12.07,12.47,12.89,13.33,13.88,14.34,14.81,15.3,15.94,16.47,17.03,17.61*/};
const double dr_c15_74pl[] = {0.8,0.8,0.8,0.8,0.9,0.9,0.9,0.9,1.,1.,1.1,1.1,1.1,1.2,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.3, 2.45 /*,6.56,6.93,7.3,7.77,8.16,8.54,8.94,9.34,9.86,10.27,10.7,11.14*/};
const double dr_c15_80[] = {1.,1.,1.,1.1,1.1,1.2,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.1,2.2, 2.45 , 2.95,3.29,3.65,4.03,3.94,4.44,4.84,5.26,5.68,6.23,6.67,7.12};
const double dr_c15_70mi[] = {10.42,10.79,11.18,11.66,12.07,12.47,12.89,13.33,13.88,14.34,14.81,15.3,15.94,16.47,17.03,17.61};
const double dr_c15_74mi[] = {6.56,6.93,7.3,7.77,8.16,8.54,8.94,9.34,9.86,10.27,10.7,11.14};
const double dr_c16[] = {0.66,0.71,0.7,0.69,0.69,0.74,0.73,0.71,0.76,0.74,0.79,0.78,0.78,0.77,0.82,0.81,0.83,0.89,0.88,0.87,0.93,0.99,0.99,0.99,1.02,1.1,1.11,
	1.19,1.2,1.29,1.38,1.42,1.52,1.57,1.69,1.76,1.84,1.98,2.08,2.25,2.43,2.32,2.55,2.7,2.93,2.94,3.16,3.38,3.54,3.76,3.92,4.15,4.31,4.48,4.69,4.97,5.18,
	5.46,5.68,5.97,6.19,6.48,6.71,7.01,7.31,7.33,7.63,7.85,8.16,8.39,8.7,8.94,9.26,9.59,9.83,10.17,10.43,10.77,11.04,11.4,11.68};
const double dr_l[] = {1.1,1.4,1.6,1.7,1.9,2.1,2.4,2.6,2.8,3.,3.2,3.5,3.7,3.9,4.1,4.3, 2.8,3.,3.2,3.5,3.7,3.9,4.1,4.3, 2.8,3.,3.2,3.5,3.7,3.9,4.1,4.3, 2.8,2.9,
	3.1,3.4,3.6,3.8,4.,4.2, 2.7,2.9,3.1,3.4,3.6,3.8,4.,4.2, 2.7,2.9,3.1,3.4,3.6,3.8,4.,4.2, 3.5,3.7,4.,4.2,4.4,4.6,4.8,5.1, 4.5,4.7,5.,5.2,5.4,5.6,5.8,6.1};


const double dpt_th_[5]={8,10,10,22,-6.25};


double CWSLensModel::DR(int type,double s,int d)
{
	int i=(int)(-4.0*(s-dpt_th_[type]));
	switch(type){
		case PERFASTAR:
			return 1;
		case COSMOLIT15:
			if(d==80||s>=0){
				switch(d){
					case 66: return dr_c15_66[i];
					case 70: return dr_c15_70pl[i-8];
					case 74: return dr_c15_74pl[i-16];
					case 80: return dr_c15_80[i-24];
					}
				}
			else{
				switch(d){
					case 70: return dr_c15_70mi[i-65];
					case 74: return dr_c15_74mi[i-53];
					}
				}
			break;
		case COSMOLIT16:
			return dr_c16[i];
		case COSMOLUX:
			return dr_c[i];
		case LENTILUX:
			return dr_l[i];
		}
	return 0;
}

const double d_c[] = {6.37,6.18,6.,5.81,5.61,5.43,5.24,5.06,4.87,4.69,4.5,4.32,4.18,4.01,3.83,3.66,3.49,3.4,3.24,3.07,2.92,2.77,2.73,2.59,2.46,2.34,2.23,2.23,2.15,2.07,2.02,1.98,1.96,2.05,1.92,1.8,
	1.7,1.61,1.52,1.44,1.38,1.31,1.26,1.21,1.16,1.12,1.08,1.05,1.02,0.99,0.97,0.95,0.93,0.91,0.89,0.88,0.86,0.85,0.84,0.83,0.82,0.82,0.81,0.8,0.8,0.79,0.79,0.78,0.78,0.77,0.77,0.77,0.77};
const double d_c15_66[] = {11.1,10.9,10.6,10.4,10.1,9.9,9.6,9.4,9.2,8.9,8.7,8.4,8.1,7.9,7.6,7.4,7.1,6.8,6.6,6.3,6.1,5.8,5.6,5.4,5.2,4.9,4.7,4.5,4.3,4.0,3.8,3.6,3.6,3.4,3.2,3.,2.8,2.7,2.5,2.4,2.3};
const double d_c15_70pl[] = {10.2,9.9,9.6,9.3,8.9,8.7,8.4,8.1,7.8,7.5,7.2,6.9,6.7,6.4,6.1,5.8,5.6,5.4,5.1,4.8,4.6,4.3,4.1,3.8,3.8,3.6,3.4,3.1,2.9,2.7,2.6,2.4, 2.3 
	/*,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.*/ };
const double d_c15_74pl[] = {8.5,8.2,7.9,7.6,7.3,7.,6.6,6.3,6.1,5.8,5.5,5.2,4.9,4.6,4.3,4.1,4.,3.8,3.5,3.3,3.,2.8,2.6,2.4, 2.3 /*,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.*/};
const double d_c15_80[] = {6.9,6.5,6.1,5.8,5.4,5.1,4.8,4.4,4.4,4.1,3.8,3.5,3.2,2.9,2.6,2.4, 2.3, 2.25,2.17,2.11,2.07,2.04,2.02,2.,2.,2.,2.,2.,2.};
const double d_c15_70mi[] = {2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.};
const double d_c15_74mi[] = {2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.,2.};
const double d_c16[] = {9.44,9.23,9.02,8.81,8.64,8.43,8.22,8.01,7.78,7.57,7.36,7.15,6.89,6.68,6.47,6.27,6.05,5.85,5.64,5.44,5.26,5.07,4.87,4.68,4.56,4.37,4.19,4.01,3.83,3.66,3.55,3.39,3.23,3.08,2.93,
	2.9,2.77,2.65,2.54,2.45,2.36,2.05,2.,1.95,1.9,1.85,1.8,1.75,1.7,1.65,1.6,1.55,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5};
const double d_p[] = {16.07,15.87,15.67,15.48,15.2,14.99,14.79,14.59,14.29,14.08,13.88,13.67,13.46,13.4,13.19,12.97,12.76,12.54,12.73,12.51,12.29,12.07,11.85,11.92,11.69,11.47,11.25,11.02,10.84,10.62,
	10.39,10.16,9.93,10.05,9.82,9.59,9.35,9.12,9.56,9.32,9.08,8.84,8.6,9.01,8.76,8.52,8.27,8.03,8.4,8.15,7.9,7.65,7.4,7.85,7.6,7.34,7.09};
const double d_l[] = {1.,1.,1.,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,
	0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.7};

double CWSLensModel::D(int type,double s,int d)
{
	int i=(int)(-4.0*(s-dpt_th_[type]));
	switch(type){
		case PERFASTAR:
			return d_p[i];
		case COSMOLIT15:
			if(d==80||s>=0){
				switch(d){
					case 66: return d_c15_66[i];
					case 70: return d_c15_70pl[i-8];
					case 74: return d_c15_74pl[i-16];
					case 80: return d_c15_80[i-24];
					}
				}
			else{
				switch(d){
					case 70: return d_c15_70mi[i-65];
					case 74: return d_c15_74mi[i-53];
					}
				}
			break;
		case COSMOLIT16:
			return d_c16[i];
		case COSMOLUX:
			return d_c[i];
		case LENTILUX:
			return d_l[i];
		}
	return 0;
}

double CWSLensModel::der_min_shell2(double r)
{
	double tmp;
//	DerErrFlg=0;
	if(r>_diam_org/2.)
		r=_diam_org/2.;
	if(_type==COSMOLUX||_type==COSMOLIT15||_type==COSMOLIT16)
		return (3*A3*r+2*A2)*r+A1;//((A3*r+A2)*r+A1)*r+A0;
	else if(_type==PERFASTAR){
		if(r<_OZ)
			return ((4*A4*r+3*A3)*r+2*A2)*r+A1;//(((A4*r+A3)*r+A2)*r+A1)*r+A0;
		else
			return 2*B2*r+B1;//(B2*r+B1)*r+B0;
		}
	else if(_type==LENTILUX&&(tmp=_s_r*_s_r-r*r)>=0)

		return r/sqrt(tmp);//_s_r-sqrt(_s_r*_s_r-r*r);
	DerErrFlg=1;
	return 0;
}

double CWSLensModel::der_max_shell2(double r)
{
	double tmp;
	if(r>_diam_org/2.)
		r=_diam_org/2.;
	if(_type==COSMOLUX||_type==COSMOLIT15||_type==COSMOLIT16||_type==PERFASTAR){
		if((tmp=_s_r*_s_r-r*r)>=0)
			return r/sqrt(tmp);//_s_r+_d-sqrt(_s_r*_s_r-r*r);
		}
	else if(_type==LENTILUX){
		if(r<_OZ){
			return (3*A3*r+2*A2)*r+A1;//_d+((A3*r+A2)*r+A1)*r+A0;
			}
		else{
			return (((5*B5*r+4*B4)*r+3*B3)*r+2*B2)*r+B1;//_d+((((B5*r+B4)*r+B3)*r+B2)*r+B1)*r+B0;
			}
		}
	DerErrFlg=1;
	return  0;
}
/*
typedef struct {
	double pr,pz,  // ���न� �� (� ��⥬� rOz) ����� �� �祪 ��אַ�
				   // (��� �, ��  ��  ����� �  � ���� �����孮�� �����)
		   vr,vz;  // � � �� ����饣� ����� 
	} line;
*/
double CWSLensModel::tr_ray(double x)
{
	double lx=0,ly=1,px=x,py=z_min(x,0),nx=-der_min_shell2(px),ny=1,
		   n_e=_n_e[_type];
		   //���,�� ,��� �� �� �室� � ����� � ��।��� ��஭�
	double sin1=(nx*ly-ny*lx)/(sqrt( (nx*nx+ny*ny)*(lx*lx+ly*ly) )),
		   sin2=sin1/n_e,cos2=sqrt(1-sin2*sin2);
	double t0=0,t1,t_av,m=40,zmx;
	if(py<-0.05||DerErrFlg>0){
		DerErrFlg=0;
		ErrFlg=1;
		return 0;
		}

	lx=nx*cos2-ny*sin2;
	ly=nx*sin2+ny*cos2;


	do{
		t1=(m-py)/ly;
		m*=2;
		if((zmx=z_max_shell2(px+t1*lx,0))<0){
			ErrFlg=1;
			return 0;
			}
	}while(zmx>py+t1*ly);
//		return ( -20. ) ;
//		}
	t_av=(t0+t1)/2;
	while(t1-t0>1e-2){
		if((zmx=z_max_shell2(px+t_av*lx,0))<0){
			ErrFlg=1;
			return 0;
			}
		if(zmx<py+t_av*ly)
			t1=t_av;
		else
			t0=t_av;
		t_av=(t0+t1)/2;
		}
	px+=t_av*lx;
	py+=t_av*ly;
	nx=-der_max_shell2(px);
	if(DerErrFlg>0){
		DerErrFlg=0;
		ErrFlg=1;
		return 13;
		}
	ny=1;
	sin1=(nx*ly-ny*lx)/(sqrt( (nx*nx+ny*ny)*(lx*lx+ly*ly) ));
	sin2=sin1*n_e;
	if(fabs(sin2)>1-0.05){ //EPS
		ErrFlg=1;
		return 0;

//		sin2=(1-EPS)*(sin2>0?1.:-1.);
		}
	cos2=sqrt(1-sin2*sin2);
	lx=nx*cos2-ny*sin2;
	ly=nx*sin2+ny*cos2;
	if(ly<0){
		ErrFlg=1;
		return 0;
		}
    if(fabs(t1=lx*(py-max(_d,_dR))-ly*px)>EPS){
		return 1000*lx/t1;
		}
	else{
		ErrFlg=1;
		return 0;
		}
}

int CWSLensModel::tr_prepare(void)
{
	double sav_s_r=_s_r,tmp;
	int sav_tor=_tor,i,j;
	_tor=0;
	_s_r=_r_main;
	for(i=0;i<=(_diam_org)/2+2;i++){////
//		j=__min(_diam/2,i);
		j=i;
		ErrFlg=0;
		DerErrFlg=0;
        tmp=tr_ray(max(j,2));
		if(ErrFlg>0){//||(fabs(diop_main)>1&&fabs(tmp/diop_main)>1.))
			if(i>1)
				main_mer[i]=main_mer[i-1]+(main_mer[i-1]-main_mer[i-2]);
//				main_mer[i]=main_mer[i-1];
			else	
				main_mer[i]=diop_main;
			}
		else
			main_mer[i]=tmp;//diop_main;
		if(sav_tor==1){
			_s_r=_r_aux;
			ErrFlg=0;
            tmp=tr_ray(max(j,2));
			if(ErrFlg>0){//||(fabs(diop_aux)>1&&fabs(tmp/diop_aux)>1.))
				if(i>0)
					aux_mer[i]=aux_mer[i-1];
				else
					aux_mer[i]=diop_aux;
				}
			else
				aux_mer[i]=tmp;//diop_main;
			_s_r=_r_main;
			}
		}
	_tor=sav_tor;
	_s_r=sav_s_r;
	return 1;
}

double CWSLensModel::trace(double x,double y,double Index)
{
	double r,phi1,phi2,
		   phi,dr,a,b,c;  			   //���न� �� �窨
									   //���न� �� � �� ����饣�
											   //����� 
	int r_i ;
	double r1 , r2 ;

//return ( 0. );
	x=(x-_Xo)*_sc_x;
	y=(y-_Yo)*_sc_y;
//	if((r=sqrt(x*x+y*y))>_r_org)
//		return 0;
	if((r=sqrt(x*x+y*y))>_r_org)
	{
		x*=_r_org/r;
		y*=_r_org/r;
		r=_r_org;
	}
	a=M_PI*r/_r_org;
	if(_type==LENTILUX||_type==PERFASTAR)
		b=1.;
	else{
		if(Index<1.6)
			b=((Index-1.5)*0.65+(1.6-Index)*0.37)/0.1;
		else
			b=((Index-1.6)*0.66+(1.7-Index)*0.65)/0.1;
		}
///
	c=(1-b)*(1+cos(a))/2.+b;
	if(c>0.33+0.66*b)
		c=0.33+0.66*b;
//	if(_type==LENTILUX)
//		c=1;
///
//	return diop_main*(1.-(r/_r_org)); //////////////////
//	phi=r>EPS?fast_atan2f(y,x):M_PI/4+_angle;
	r_i=(int)(r);
	r1=r_i+1.-r;
	r2=r-r_i;

	if(_tor){
		phi=r>EPS?atan2(y,x):M_PI/4+_angle;
		phi1=phi-_angle+_ang_sh;
		if(phi1<0)
			phi1+=2*M_PI;
		phi1/=M_PI;
		if(phi1>0)
			phi1-=(int)(phi1);
		else{
			phi1+=1.-(int)(phi1);
			}
		phi1*=M_PI;
//		phi1=fmod(phi1,M_PI);
		if(phi1>M_PI/2)
			phi1=M_PI-phi1;
#define FRST1
#ifdef FRST
		phi1=(1-cos(2*phi1))/2; // � � ��: phi1/=M_PI/2;
								// ⮣�  ����� ������  -- ��᪨ ᯨ� ���
#else
		phi1/=M_PI/2;
		phi1=(-2*phi1+3)*phi1*phi1;
#endif
		if(r<12)
			phi1*=r/12.;
		phi2=1-phi1;
		{
		double d1=r1*main_mer[r_i]+r2*main_mer[r_i+1],
			   d2=r1*aux_mer[r_i]+r2*aux_mer[r_i+1];
		dr=phi2*d1+phi1*d2;
		}
		}
	else{
		dr=r1*main_mer[r_i]+r2*main_mer[r_i+1];

		}
	dr*=c;

	return dr;

}

double CWSLensModel::TraceM(double x,double y,double Index)
{
	double r,phi1,phi2,
		   phi,dr,c;  			   //���न� �� �窨
									   //���न� �� � �� ����饣�
											   //����� 
	int r_i ;
	double r1 , r2 ;

//return ( 0. );
	x=(x-_Xo)*_sc_x;
	y=(y-_Yo)*_sc_y;
////////////////////////////////////
	c=1.;						  //
////////////////////////////////////


	if((r=sqrt(x*x+y*y))>_r_org+2-EPS)
		return 0;
/*	a=M_PI*r/_r_org;
	if(_type==LENTILUX||_type==PERFASTAR)
		b=1.;
	else{
//		b=((Index-1.5)*0.65+(1.6-Index)*0.7)/0.1;
		if(Index<1.6)
			b=((Index-1.5)*0.6+(1.6-Index)*0.7)/0.1;
		else
			b=((Index-1.6)*0.57+(1.7-Index)*0.6)/0.1;
		}
/// 
//	double HiTh=0.33+0.66*b;
	c=(1.-b)*(1+cos(a))/2.+b;//1.
	if(c>0.33+0.66*b)
		c=0.33+0.66*b;
  */
//	if(_type==LENTILUX)
//		c=1;
///
//	return diop_main*(1.-(r/_r_org)); //////////////////
//	phi=r>EPS?fast_atan2f(y,x):M_PI/4+_angle;
	r_i=(int)(r);
	r1=r_i+1.-r;
	r2=r-r_i;

	if(_tor){
		phi=r>EPS?atan2(y,x):M_PI/4+_angle;
		phi1=phi-_angle+_ang_sh;
		if(phi1<0)
			phi1+=2*M_PI;
		phi1/=M_PI;
		if(phi1>0)
			phi1-=(int)(phi1);
		else{
			phi1+=1.-(int)(phi1);
			}
		phi1*=M_PI;
//		phi1=fmod(phi1,M_PI);
		if(phi1>M_PI/2)
			phi1=M_PI-phi1;
#define FRST1
#ifdef FRST
		phi1=(1-cos(2*phi1))/2; // � � ��: phi1/=M_PI/2;
								// ⮣�  ����� ������  -- ��᪨ ᯨ� ���
#else
		phi1/=M_PI/2;
		phi1=(-2*phi1+3)*phi1*phi1;
#endif
		if(r<12)
			phi1*=r/12.;
		phi2=1-phi1;
		{
		double d1=r1*main_mer[r_i]+r2*main_mer[r_i+1],
			   d2=r1*aux_mer[r_i]+r2*aux_mer[r_i+1];
		dr=phi2*d1+phi1*d2;
		}
		}
	else{
		dr=r1*main_mer[r_i]+r2*main_mer[r_i+1];

		}
	dr*=c;

	return dr;

}

double CWSLensModel::GetLenseRadius()
{
	return (_diam/2.);
}
