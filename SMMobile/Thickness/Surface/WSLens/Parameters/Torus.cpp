#include "stdafx.h"
#include "Asph2.h"

double CWSLensModel::R(int type,double s,int diam)
{
	double z,_d=D(type,s,diam),_dR=DR(type,s,diam);

	if(type==COSMOLUX||type==PERFASTAR||type==COSMOLIT16||type==COSMOLIT15){
		z=z__min(diam/2.,0);
		return (2*(z*(_d)+_dR*(_d)-z*(_dR))-diam*diam/4.0-z*z-(_dR)*(_dR)-(_d)*(_d))/(2*(_d-_dR-z));
		}
	else if(type==LENTILUX){
		z=z_max(diam/2.,0);
		return (2*z*(_dR)-diam*diam/4.0-z*z-_dR*(_dR))/(2*(_dR-z));
		}
	else
		return -1;
}

int CWSLensModel::set_par(double index,int mat,double *x,double *y,int np,int type,double sph,double cyl,
				double angle,int r,double scale_x,double scale_y,double x0,double y0,ThicknessStruct * Thickness)
{
	const int isFirst[6]={1,1,1,1,-1,1};
	const double AsphInds[5]={1.6,1.5,1.6,1.65,1.7};
	const double SphRads[] = { 121., 110. , 100. , 91. , 83. , 76. , 69. ,
							  63. , 57. , 52. , 48. , 44. , 41. , 38. , 35. ,
							  33. , 31. , 29. , 27.4 , 26.1 , 24.8, 23.5 } ;
//	,a,b,c,d,e,f,g,__r_min,i,compRs
	double diop_min=sph,diop_max=sph,temp;
	double compRf;
#ifdef OT_VER
	double compRf,compRs,__r_min;
#endif
	int tor;
	if(r>40)
		r=40;
	if(r<25)
		r=25;

	lns->_sc_x=scale_x;
	lns->_sc_y=scale_y;
	lns->_tor=0;
	lns->_angle=angle;
	lns->_x0=0;
	lns->_y0=0;
	lns->_mat=mat;
	lns->_type=type;
	lns->z_coeff=1;
	lns->Thickness = *Thickness;
	if(type==LENTILUX)
		lns->_ang_sh=M_PI/2;
	else
		lns->_ang_sh=0;
	if(fabs(cyl)>EPS){
		tor=1;
		if(cyl<0)
			diop_min+=cyl;
		else
			diop_max+=cyl;
		}
	else
		tor=0;
	lns->diop_main=diop_max;
	lns->diop_aux=diop_min;
	if(diop_max<-EPS&&tor==1){
		temp=diop_min;
		diop_min=diop_max;
		diop_max=temp;
		}
	lns->_r_org=r;
	lns->_diam_org=lns->_diam=r*2;

	if(type!=SPHERIC && !exists(type,diop_max,lns->_diam)){
		if(type!=COSMOLIT15)
			lns->_diam=_diam_table[type];
		else{
			if(diop_max>=-EPS){
				int dms[]={66,70,74,80},last_diam=(base_index(type,diop_max)-1)>>1,
						min_dist=1000,min_ind=-1,i;
				for(i=0;i<=last_diam;i++){
					if(abs(lns->_diam_org-dms[i])<min_dist){
						min_ind=i;
						min_dist=abs(lns->_diam_org-dms[i]);
						}
					}
				lns->_diam=dms[min_ind];
				}
			else if(diop_max>=-3-EPS)
				lns->_diam=80;
			else if(diop_max>=-6-EPS)
				lns->_diam=74;
			else
				lns->_diam=70;
			}
		}
	if(type==SPHERIC||exists(type,diop_max,lns->_diam)){
		if(type!=SPHERIC){
			lns->A0=a0(type,diop_max);
			lns->A1=a1(type,diop_max);
			lns->A2=a2(type,diop_max);
			lns->A3=a3(type,diop_max);
			lns->A4=a4(type,diop_max);
			lns->B0=b0(type,diop_max);
			lns->B1=b1(type,diop_max);
			lns->B2=b2(type,diop_max);
			lns->B3=b3(type,diop_max);
			lns->B4=b4(type,diop_max);
			lns->B5=b5(type,diop_max);
			lns->_OZ=OZ(type,diop_max);
			lns->_dR=DR(type,diop_max,lns->_diam);
			lns->_d=D(type,diop_max,lns->_diam);
			lns->_r=R(type,diop_max,lns->_diam);
			lns->_R=0;
			lns->_s_r=lns->_r;
			if(type==LENTILUX){
				double b=(((5*(lns->B5)*(lns->_OZ)+4*(lns->B4))*(lns->_OZ)+3*(lns->B3))*(lns->_OZ)+2*(lns->B2))*(lns->_OZ)+(lns->B1),
					   a=(3*(lns->A3)*(lns->_OZ)+2*(lns->A2))*(lns->_OZ)+(lns->A1);
				(lns->B1)+=a-b;
				b=(((((lns->B5)*(lns->_OZ)+(lns->B4))*(lns->_OZ)+(lns->B3))
				   *(lns->_OZ)+(lns->B2))*(lns->_OZ)+(lns->B1))*(lns->_OZ)+(lns->B0);
				a=(((lns->A3)*(lns->_OZ)+(lns->A2))*(lns->_OZ)+(lns->A1))*(lns->_OZ)+(lns->A0);
				lns->B0+=a-b;
				}
			if(type==PERFASTAR){
				double b=2*(lns->B2)*(lns->_OZ)+(lns->B1),
					   a=((4*(lns->A4)*(lns->_OZ)+3*(lns->A3))*(lns->_OZ)+2*(lns->A2))*(lns->_OZ)+(lns->A1);
				lns->B1+=a-b;
				b=((lns->B2)*(lns->_OZ)+(lns->B1))*(lns->_OZ)+(lns->B0);
				a=((((lns->A4)*(lns->_OZ)+(lns->A3))*(lns->_OZ)+(lns->A2))
				   *(lns->_OZ)+(lns->A1))*(lns->_OZ)+(lns->A0);
				lns->B0+=a-b;
				}

/*
			if(type==PERFASTAR){
				double crit=(0.5-lns->B1)/lns->B2;
				if(crit<lns->_OZ)
					crit=lns->_OZ;
				if(r>crit){
					r=crit;
					lns->_r_org=crit;
					lns->_diam_org=crit*2;
					}
				}
*/
			compRf=1000/(isFirst[type]*diop_max/(AsphInds[type]-1)+1000/(lns->_r));
			}
		else{
			int IntPart ;
			double w;
			if(diop_max>=-EPS){
				IntPart=(int)(diop_max+0.001);
				w=diop_max-IntPart;
				compRf=(SphRads[IntPart]*(1-w)+SphRads[IntPart+1]*w);
				}
			else{
				compRf=(SphRads[0]*(20+diop_max)+350*(-diop_max))/20;
				}
			compRf*=0.8*index/1.5 ;
			if (compRf<r*1.1)
				return 0;
//	*Rs = 1./(1./(*Rf) - Sph/(1000.*(n-1.)) ) ;

			lns->_s_r=compRf;
			lns->_r=1000/(-diop_max/(index-1)+1000/compRf);
			lns->_R=0;
			lns->_d=0;
			}
		}
	else
		return 0;
	if(tor==1){
		double r_max=lns->_r,r_min,ui,ua;
		if(type==SPHERIC){
			r_min=1000/(-isFirst[type]*diop_min/(index-1)+1000/(compRf));
			ui=(index-1)*(1000/compRf-1000/r_min);
			ua=(index-1)*(1000/compRf-1000/r_max);
			r_min+=0.00001;
			}
		else{
			r_min=1000/(-isFirst[type]*diop_min/(AsphInds[type]-1)+1000/(compRf));
			ui=(AsphInds[type]-1)*(1000/compRf-1000/r_min);
			ua=(AsphInds[type]-1)*(1000/compRf-1000/r_max);
			r_min+=-0.001;
			}
		lns->_tor=1;
		lns->_R=fabs(r_min-r_max);
        lns->_r=min(r_min,r_max);
		if(type!=SPHERIC)
			lns->_s_r=r_max;
		}
	lns->_d=0;//////////////////////////////////////////////////
	{   //kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk
		lns->_r_main=lns->_R+lns->_r;  ///ииииииииииииииииииииии
		lns->_r_aux=lns->_r;
		lns->_dpt=diop_max;
		if (lns->_r_main<r*1.1||lns->_r_aux<r*1.1)
			return 0;
#ifdef OT_VER
		compRs=1000/(diop_min/(index-1)+1000/(compRf));
//		compRs=1000/(-diop_min/(index-1)+1000/(compRf));
//		printf("diop_max=%5.2f,diop_min=%5.2f,  r_aux=%7.2f,compRs=%7.2f,diff=%7.2f%%\n",
				diop_max,diop_min,/*lns->_r_aux*/__r_min,compRs,100*fabs(__r_min/compRs-1));
#endif
		lns->_x0=x0;
		lns->_y0=y0;
		if(index<1.4||index>2.0){
			index=1.5;
			}
#define corInd(ind,base) (1.2*(ind-base)+base)
		switch(type){
			case COSMOLIT15:
				temp=(corInd(index,1.3)-1)/0.5;//0.3;
//				temp=index-0.5;//0.3;
				break;
			case COSMOLUX:
			case COSMOLIT16:
				temp=(corInd(index,1.3)-1)/0.5;//0.4;
//				temp=index-0.6;//0.4;
				break;
			case PERFASTAR:
				temp=(corInd(index,1.3)-1)/0.5;
//				temp=index-0.5;
				break;
			case LENTILUX:
				temp=(corInd(index,1.5)-1)/0.7;//0.5 ?
//				temp=index-0.7;//0.5 ?
				break;
			default:
				temp=1;
				break;
			}
		if(temp>0.7)
			lns->z_coeff=1/temp;
		if(lns->z_coeff<0.5)
			lns->z_coeff=0.5;
		if(lns->z_coeff>2.8)
			lns->z_coeff=2.8;
        if( (mat>0&&type==COSMOLIT16) || type==COSMOLIT15 )
			lns->z_coeff*=0.85;
		return 1;
		}
}


