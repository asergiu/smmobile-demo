#pragma once

#include "material.h"
#include "IWSLLens.h"
#include "ObjectPointer.h"

BOOL CONTOUR2ILens(LENSE *pLens, ILens *pLensRight, ILens *pLensLeft, int ContourForm, int ContourScale);
