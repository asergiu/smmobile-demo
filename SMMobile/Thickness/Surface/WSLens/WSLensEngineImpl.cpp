#include "stdafx.h"
#include "WSLensEngine.h"
#include <math.h>
#include "Parameters/Asph2.h"
#include "ErrorString.h"

bool CWSLensEngine::_CalcWeight(ILens * LensObject, double *pResultWeight)
{
	long EnUsMask, MyNumber, NumberOfEngineToCalcSurface;
	ILensEngine *pLensEngine = NULL;

	SetLastErrorCode(0);

	CObjPtr<ILens> Lens;
	Lens.AttachDispatch(LensObject);
	Lens.m_p->AddRef(); // because of auto-release in destructor

	*pResultWeight = 0.;

	EnUsMask = Lens->GetEngineUsageMask();

	MyNumber = WhatEngineMakesSmth(EnUsMask, wsWeightEngine2);
	NumberOfEngineToCalcSurface = WhatEngineMakesSmth(EnUsMask, wsSurfaceEngine2);
	if(MyNumber != NumberOfEngineToCalcSurface)
	{
		if(NumberOfEngineToCalcSurface == 2)
			pLensEngine = Lens->GetEngine2();
		else
			pLensEngine = Lens->GetEngine1();
		if(!pLensEngine)
		{
			SetLastErrorCode(9);
			goto Error;
		}
		unsigned long SupportedMethods = pLensEngine->GetSupportedMethods();
		if(!IsThereSupport(SupportedMethods, wsSurfaceSupported)){
			SetLastErrorCode(10);
			goto Error;
		}
		if(!(WeightByExternalSurface(Lens, pLensEngine)))
		{
			goto Error;
		}
		
	}
	else
	{
		if(!(PrepareLensData(Lens)))
		{
			goto Error;
		}
	}
	*pResultWeight = m_Weight;

Error:
	if(pLensEngine)
		pLensEngine->Release();
	if(GetLastErrorCode())
	{
		return false;
	}

	return true;
}

bool CWSLensEngine::_CalcSurface(ILens * LensObject, ILensSurface * LensSurface)
{
	if(nWeAreHere)
		nWeAreHere++;
	else
		nWeAreHere = 1;
	SetLastErrorCode(0);

	CObjPtr<ILensSurface> Grid;
	Grid.AttachDispatch(LensSurface);
	Grid.m_p->AddRef(); // because of auto-release in destructor

	CObjPtr<ILens> Lens;
	Lens.AttachDispatch(LensObject);
	Lens.m_p->AddRef(); // because of auto-release in destructor

	if(!PrepareLensData(Lens))
	{
		return false;
	}

	double LensRadius = Lens->GetDiameter() / 2.;
	double RadiusVector, X, Y, Zf, Zr;

	double YCoeff;
	if(m_EllipticBlank)
	{
		if(m_EllipticVertToHorizRatio <= 0)
		{
			return false;
		}
		YCoeff = 1. / m_EllipticVertToHorizRatio;
	}
	else
		YCoeff = 1.;
	
    vector<LensSurfPoint>::size_type i, cElements = 0;
	try
	{
		vector<LensSurfPoint> * DArr = NULL;
		//vector<LensSurfPoint>::size_type i, cElements = 0;

		DArr = Grid->GetArray();
		if(DArr)
			cElements = DArr->size();
		vector<LensSurfPoint>::iterator it;
		//for(i = 0, it = DArr->begin(); i < cElements; i++, ++it)
        for(i = 0, it = DArr->begin(); it != DArr->end(); ++it, i++)
		{
			X = it->X;
			Y = it->Y;
			double GeomToX = X - m_BlankCenterX, GeomToY = (Y - m_BlankCenterY) * YCoeff;
			RadiusVector = _hypot(GeomToX, GeomToY);
			if(RadiusVector > LensRadius){
				return false;
			}

			X = X - m_CenterX; Y = Y - m_CenterY;
			Zf = -(LensModel.z__min(X, Y));//sqrt(R1*R1-RadiusVector*RadiusVector)-R1;
			Zr = -(LensModel.z_max(X, Y));//sqrt(R2*R2-RadiusVector*RadiusVector)-R2-RD;
//			Grid.SetItemZ(i+1, Zr, Zf);
			it->zRear = Zr;
			it->zFront = Zf;
/*
			ILensSurfacePointDriver Pt(Grid.GetItem(i+1));
			X = Pt.GetX() - Lens.GetCenterX(); Y = Pt.GetY() - Lens.GetCenterY();
			RadiusVector = sqrt(X*X + Y*Y);
			Pt.SetZFront(sqrt(R1*R1-RadiusVector*RadiusVector)-R1);
			Pt.SetZRear(sqrt(R2*R2-RadiusVector*RadiusVector)-R2-RD);
*/
		}
	}
	catch(...)
	{
        nWeAreHere--;
		return false;
	}

	nWeAreHere--;

	return true;
}

void CWSLensEngine::_GetDepth( ILens * LensObject, double x, double y, double & rear, double & front )
{
	SetLastErrorCode(0);
	
	CObjPtr<ILens> Lens;
	Lens.AttachDispatch(LensObject);
	Lens.m_p->AddRef(); // because of auto-release in destructor
	
	if(!PrepareLensData(Lens))
	{
		return;
	}
	
	double LensRadius = Lens->GetDiameter() / 2.;
	double RadiusVector, X, Y, Zf, Zr;
	
	double YCoeff;
	if(m_EllipticBlank)
	{
		if(m_EllipticVertToHorizRatio <= 0)
		{
			return;
		}
		YCoeff = 1. / m_EllipticVertToHorizRatio;
	}
	else
		YCoeff = 1.;
	
	try
	{
		X = x;
		Y = y;
		double GeomToX = X - m_BlankCenterX, GeomToY = (Y - m_BlankCenterY) * YCoeff;
		RadiusVector = _hypot(GeomToX, GeomToY);
		if(RadiusVector > LensRadius){
			return;
		}
    	    
		X = X - m_CenterX; Y = Y - m_CenterY;
		Zf = -(LensModel.z__min(X, Y));//sqrt(R1*R1-RadiusVector*RadiusVector)-R1;
		Zr = -(LensModel.z_max(X, Y));//sqrt(R2*R2-RadiusVector*RadiusVector)-R2-RD;
		//			Grid.SetItemZ(i+1, Zr, Zf);
		rear = Zr;
		front = Zf;
	}
	catch( ... )
	{
	}
}

bool CWSLensEngine::_CalcThickness(	ILens * LensObject,
									double *ResultCenterThickness,
									double *ResultMinEdgeThickness,
									double *ResultMaxEdgeThickness)
{
	long EnUsMask, MyNumber, NumberOfEngineToCalcSurface;
	ILensEngine *pLensEngine = NULL;

	SetLastErrorCode(0);
	*ResultCenterThickness = 0;
	*ResultMinEdgeThickness = 0;
	*ResultMaxEdgeThickness = 0;
//	*ResultMinDiam = 0;

	CObjPtr<ILens> Lens;
	Lens.AttachDispatch(LensObject);
	Lens.m_p->AddRef(); // because of auto-release in destructor

	EnUsMask = Lens->GetEngineUsageMask();
/*
	if(IsThereSupport(EnUsMask, wsWeightEngine2))
	{
		MyNumber = 2;
	}
	else
	{
		MyNumber = 1;
	}
*/
	MyNumber = WhatEngineMakesSmth(EnUsMask, wsWeightEngine2);
	NumberOfEngineToCalcSurface = WhatEngineMakesSmth(EnUsMask, wsSurfaceEngine2);
	if(MyNumber != NumberOfEngineToCalcSurface)
	{
		if(NumberOfEngineToCalcSurface == 2)
			pLensEngine = Lens->GetEngine2();
		else
			pLensEngine = Lens->GetEngine1();
		if(!pLensEngine)
		{
			SetLastErrorCode(9);
			goto Error;
		}
		unsigned long SupportedMethods = pLensEngine->GetSupportedMethods();
		if(!IsThereSupport(SupportedMethods, wsSurfaceSupported)){
			SetLastErrorCode(10);
			goto Error;
		}
		bool fResult;
//		double CalculatedWeight;
		fResult = ThicknessAndDiamByExternalSurface(Lens, pLensEngine);//, pResultWeight
		if(!fResult)
		{
			SetLastErrorCode(10);
			goto Error;
		}
		
	}
	else
	{
		if(!(PrepareLensData(Lens)))
		{
			goto Error;
		}
//		PrepareLensData(&Lens);
	}
	*ResultCenterThickness = m_CenterThickness;
	*ResultMinEdgeThickness = m_MinEdgeThickness;
	*ResultMaxEdgeThickness = m_MaxEdgeThickness;
//	*ResultMinDiam = m_MinDiam;
Error:
	if(pLensEngine)
		pLensEngine->Release();
	if(GetLastErrorCode())
	{
		return false;
	}

	return true;
}

unsigned long CWSLensEngine::_GetSupportedMethods()
{
	return 15;
}


string CWSLensEngine::_GetLastErrorMessage()
{
	return ErrorString[nLastErrorCode];
}

bool CWSLensEngine::_CalcUVProtection(ILens * LensObject, double * ResultUVProtection) 
{
	const int Glass[] = {-35, -25};
	const int Plastic[] = {-15, -10};
//	int Reflection = 1;
//	int sum, sum1, sum2, numb;
//	static int sum_max = 153000;
//	int SumMax = 0;

	CObjPtr<ILens> Lens;
	Lens.AttachDispatch(LensObject);
	Lens.m_p->AddRef(); // because of auto-release in destructor
	
//	������: ���� UV-��������� �������� ���,
//	�� ������ UV � 0, ����� - � 1
	int UV;
	CObjPtr<ICoatings> Coatings;
	Coatings.AttachDispatch(Lens->GetCoatings());
	if(Coatings.m_p)
	{
		CObjPtr<ICoating> UVProtCoating;
		UVProtCoating.AttachDispatch(Coatings->GetUVProtection());
		if(UVProtCoating.m_p)
		{
			UV = 1;	
		}
		else
		{
			UV = 0;	
		}
	}
	else
	{
		UV = 0;	
	}

	if(UV)
	{
		*ResultUVProtection = 98;	
	}
	else
	{
		if(Lens->GetKindOfMaterial() != wsPlastic && Lens->GetIsPhotochromic())
		{
			*ResultUVProtection = 98;
		}
		else
		{
			int MaxSum = 0, CurSum = 0;
			CObjPtr<ITint> Tint;
			Tint.AttachDispatch(Lens->GetTint());
			if(Tint.m_p)
			{
				for(int i = Tint->GetCount(); i > 0; i--)
				{
					CObjPtr<ITintPoint> TintPoint;
					TintPoint.AttachDispatch(Tint->GetItem(i));
					CurSum += TintPoint->GetR() + TintPoint->GetG() + TintPoint->GetB();
					MaxSum += 255 * 3;
				}
			}
			double TranspCoeff;
			if(MaxSum > 0)
				TranspCoeff = CurSum / double(MaxSum);
			else
				TranspCoeff = 1.;

//	������: ���� ��������������� �������� ���, ��� ��� ���������� ��� �� 255,
//	�� ������ Reflection � 1, ����� - � 0
			int Reflection;
			CObjPtr<IARCoating> ARCoating;
			ARCoating.AttachDispatch(Lens->GetARCoating());
			if(ARCoating.m_p)
			{
//				IARCoatingDriver ARCoating(Lens.GetARCoating());
				if(ARCoating->GetR() == 255 && ARCoating->GetG() == 255 && ARCoating->GetB() == 255)
				{
					Reflection = 1;
				}
				else
				{
					Reflection = 0;
				}
			}
			else
			{
				Reflection = 1;
			}

			int MaxDecreaseRange;
			if(Lens->GetKindOfMaterial() != wsPlastic)
				MaxDecreaseRange = Glass[Reflection];
			else
				MaxDecreaseRange = Plastic[Reflection];
			*ResultUVProtection = int(98 + MaxDecreaseRange * TranspCoeff);
			// ��� ���������� �����, ��� ������� ����� �������� �� 98
		}
	}

	return true;
}
