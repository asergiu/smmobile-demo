#ifndef __MATERIAL_H__
#define __MATERIAL_H__

#include "Thickness.h"

struct LENSE {
	short  MAT ;					// -> 0 - Plastic
									//    1 - Glass
									//	  2 - Polycarbonate
	short  MatName ;                //	  Button index in current material settings
									// -> 0 - 1st plastic button
									//    1 - 2nd plastic button
									//    2 - 3rd plastic button
									//	  3 - Polycarbonate button
									//    4 - 1st glass button
									//    5 - 2nd glass button
									//    6 - 3rd glass button
									//    7 - 4th glass button
									//	  8 - 5th glass button
	short  I_degrade ;				// -> 0 - Constant pallette
									//    1 - Degrade pallette
									//    2 - Melting pallette
									//    3 - Catalogue pallette
	float Index ;					// -> Index
	float Density ;					// -> Density
	char information[30] ;			// -> Supplier Name	/ LenseFlag = 1 => material supplier
									//					/ PHOTOCHROM = 10 => photochrome supplier
									//					/ I_degrade = 3 => tint supplier
	char NAME_Standard[20] ;		// -> Tint or Photochrome lens name
	char LenseType[24] ;			// -> Lense Type
	short  LenseFlag ;				// -> 0 - Material index is set in MatName and Material name is set in LenseType field
									//    1 - Material is set in LenseType and supplier is set in information
	unsigned char R[200] , G[200], B[200] ;	// -> Lense Tint
									//    If PHOTOCHROME == 10 / Catalogue photochrome /
									//	  R[0]=G[0]=B[0] = number of followed rgb elements
									//	  R[1]...R[R[0]] |
									//	  G[1]...G[G[0]] |-> rgb elements 
									//	  B[1]...B[B[0]] |
	float r_f , g_f , b_f ,
		  r_e , g_e , b_e ;			// -> Lense Tint for Constant, Degrade & Melting.
	short  PHOTOCHROM ;				// -> 0 - No photochrom
									//    1 - Transition
									//    2 - FotoGray
									//    3 - FotoBrown
									//	 10	- Catalogue photochrome
	float Dpl , Dpr , dDpl , dDpr ;	// -> Dioptries
	float Rc_real ;					// -> Rc
	float SH ;						// -> Segment Height
	short  Itype ;					// -> Position in Menu Type(Bifocal,Trifokal,Progressiv)
	short  Focal ;					// -> 1 - Progressiv
									//    2 - Bifocal
									//    3 - Trifocal
	short  Kind ;					// -> Type Focal

	short  Itip[2] ;				// -> Coating
//     	     [0]  AR                   -> 0 - None
									// -> 1 - Comfort
									// -> 2 - Gold
									// -> 3 - Super
									// -> 10 - see ReflexCoeff(R,G,B)
// 			 [1]  ������� �����, ������������ ���� ��������
									// -> ��� 0x0001 - ���������� ������� �������� UV protection
									// -> ��� 0x0002 - ���������� ������� �������� Scratch resistance
									// -> ��� 0x0004 - ���������� ������� �������� Waterproof

	short  FC_fl ;					// -> Presence photochrome information
	short  fl_aSTIGMAT_Left ;
	short  fl_aSTIGMAT_Right ;
	float Axis_Left , Axis_Right ;
	float Cyl_Left , Cyl_Right ;

	short  Design ;							// -> 0 - Classic ; 1 - Aspheric ; 2 - Linticular ;
	float Rc_real_Left , Rc_real_Right ;	// -> Rc
	float SH_Left , SH_Right ;				// -> Segment Height
	short iForm_Left , iForm_Right ;

	double	AntiReflexCoeffR;
	double	AntiReflexCoeffG;
	double	AntiReflexCoeffB;
	char	AntiReflexName[30];

	int   LenseJob;				// -> 0 - AFF; 1 - CFF;
	ThicknessStruct Thickness;

	UCHAR	InCar;
	UCHAR	Heat;

	char	UseLensesName;		// 99 -  Use names of favourites to decribe lenses in Lens Consulting
	char	LensesName[31];
	
	char 	rezerv[38];
	};

struct DATA_MATERIAL {
	short MAT ;
	float Index ;
	float Density ;
	ThicknessStruct Thickness;
	};

struct LENSE2 {
	short MAT ;						// -> 0 - Plastic
									//    1 - Glass
									//    2 - Polycarbonate
	short MatName ;                  // -> 0 - CR 39
									//    1 - Hi Plastic
									//    2 - Plastic Lite
									//    3 - Crown
									//    4 - Hi Crown
									//    5 - Flint
									//    6 - Hi Flint
	short I_degrade ;				// -> 0 - Constant pallette
									//    1 - Degrade pallette
									//    2 - Melting pallette
									//    3 - Catalogue pallette
	float Index ;					// -> Index
	float Density ;					// -> Density
	char  information[30] ;			// -> Supplier Name
	char  NAME_Standard[20] ;		// -> Lense Name or Photochrom
	char  LenseType[24] ;			// -> Lense Type
	short LenseFlag ;				// -> 0 - Material is set into MatName
									//    1 - Material is set into LenseType
	unsigned char  R[200] , G[200], B[200] ;	// -> Lense Tint
	float r_f , g_f , b_f ,
		  r_e , g_e , b_e ;			// -> Lense Tint for Constant, Degrade & Melting.
	short PHOTOCHROM ;				// -> 0 - No photochrom
									//    1 - Transition
									//    2 - FotoGray
									//    3 - FotoBrown
	float Dpl , Dpr , dDpl , dDpr ;	// -> Dioptries
	float Rc_real ;					// -> Rc
	float SH ;						// -> Segment Hight
	short Itype ;					// -> Position in Menu Type(Bifocal,Trifokal,Progressiv)
	short Focal ;					// -> 1 - Progressiv
									//    2 - Bifocal
									//    3 - Trifocal
	short Kind ;						// -> Type Focal

	short Itip[2] ;					// -> Coating
//     	     [0]  AR                   -> 0 - None
									// -> 1 - Comfort
									// -> 2 - Gold
									// -> 3 - Super
// 			 [1]  UV & Scratch         -> 0 - None
									// -> 1 - UV
									// -> 2 - Scratch
									// -> 3 - UV, Scratch

	short FC_fl ;					// -> Flag about present Fotochrome information
	short fl_aSTIGMAT_Left ;
	short fl_aSTIGMAT_Right ;
	float Axis_Left , Axis_Right ;
	float Cyl_Left , Cyl_Right ;

	short Design ;							// -> 0 - Classic ; 1 - Aspheric ; 2 - Linticular ;
	float Rc_real_Left , Rc_real_Right ;	// -> Rc
	float SH_Left , SH_Right ;				// -> Segment Hight
	short iForm_Left , iForm_Right ;
	char  rezerv[18] ;
	};

const short MaskaOtherCoating[] = {0x0001, 0x0002, 0x0004};

#endif
