#pragma once

//#include "Lens.h"
#include "ContourPointBelong.h"

#define RENDER_START	(WM_USER + 0x101)

enum VariablePresentMap
{
	wsPupilsPWOF		=   1,
	wsPupilsPWF			=   2,
	wsPDs				=   4,
	wsContours			=   8,
	wsLenses			=  16,
	wsPictures			=  32,
	wsOffsets			=  64,
	wsColorMatchTint	= 128,
};

enum ShellMode
{
	wsMakeUsualPicture			= 1,
	wsMakePhotochromePicture	= 2,
	wsThreadedInstall			= 4
};

class ShellThreadParams
{
public:
	ShellThreadParams() { IsBusy = FALSE; };

	BOOL IsBusy;
	long Mode;
	int StartPercent;
	int EndPercent;
};

class CShellLens
{
public:
	CShellLens();
	~CShellLens();

	void SetPupilsPWOF(double PWOFRightPupilX, double PWOFRightPupilY, double PWOFLeftPupilX, double PWOFLeftPupilY);
	void SetPupilsPWF(double PWFRightPupilX, double PWFRightPupilY, double PWFLeftPupilX, double PWFLeftPupilY);
	void SetPDs(double RightPD, double LeftPD);
	void SetContours(IContour * lpdispRight, IContour * lpdispLeft);
	void SetLenses(ILens * lpdispRight, ILens * lpdispLeft);
	void SetPictures(IWSPicture * lpdispPictureWOF, IWSPicture * lpdispPictureWF);
	void SetOptCentOffsets(double RightOptCenOffsetX, double RightOptCenOffsetY, double LeftOptCenOffsetX, double LeftOptCenOffsetY);
	long InstallLens(IWSPicture * lpdispResultPicture, long Mode, int StartPercent, int EndPercent);
	CRect GetLensRect();
	CContourPointBelong * GetBe(){return &Be;}
	string GetLastErrorString(){ return ErrorMsg; };

	void ConvertCoords(int PicWidth, int PicHeight,
		ILens * lpdispLeftLens, ILens * lpdispRightLens,
		IContour * lpdispLeftContour, IContour * lpdispRightContour,
		double LeftPD, double RightPD,
		double PWFLeftPupilX, double PWFLeftPupilY,
		double PWFRightPupilX, double PWFRightPupilY,
		double RightOptCenOffsetX, double RightOptCenOffsetY,
		double LeftOptCenOffsetX, double LeftOptCenOffsetY
		);

protected:
	long VariablePresent;
	double AverZLens[2];
	string ErrorMsg;

	double m_PWOFLeftPupilX, m_PWOFLeftPupilY,
		   m_PWOFRightPupilX, m_PWOFRightPupilY;
	double m_PWFLeftPupilX, m_PWFLeftPupilY,
		   m_PWFRightPupilX, m_PWFRightPupilY;
	double m_LeftPD, m_RightPD;
	double m_LeftOptCenOffsetX, m_LeftOptCenOffsetY, m_RightOptCenOffsetX, m_RightOptCenOffsetY;

	CObjPtr<IContour> lpdispContour[2];
	CObjPtr<ILens> lpdispLens[2];
	CObjPtr<IWSPicture> lpdispPictureWithoutFrame, lpdispPictureWithFrame;

	// ������� ����������
	CContourPointBelong Be;
//	CMassiv mRwof, mGwof, mBwof, mRwf, mGwf, mBwf;
	CDimension dimPwof, dimPwf;
	CDimension dimCPwf; // ��������� ��� ���������� ������
	CDimension dimCContourPwf; // ��������� ��� ������ ��������
//	int WidthWof, HeightWof, WidthWf, HeightWf;

	CRefraction Refraction;

	ShellThreadParams ThreadParams;
	
public:
	//	Timer control variable
	ULONG m_Time;
	void OnStart(IWSPicture * lpdispResultPicture);
	void SIGNAL_COMPLETION(long Percents);
	void ThrowException(long Percents);
};
