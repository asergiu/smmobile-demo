// MultifocalSegment.cpp : implementation file
//

#include "stdafx.h"
#include "MultifocalSegment.h"
#include "math.h"
#include "CirclesCrossPoints.h"

/////////////////////////////////////////////////////////////////////////////
// CMultifocalSegment

CMultifocalSegment* CMultifocalSegment::CreateObject()
{
	CMultifocalSegment *pObject = new CMultifocalSegment;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

IMultifocalSegment * CMultifocalSegment::GetInterface()
{
	return static_cast<IMultifocalSegment*>(this);
}

CMultifocalSegment::CMultifocalSegment() :
	EPS(1e-9), eps(0.001)
{
	m_MainCircCenterX = 0 ;
	m_MainCircCenterY = 0 ;
	m_MainCircRadius  = 0 ;
	m_BifocalBoundCircCenterX = 0 ;
	m_BifocalBoundCircCenterY = 0 ;
	m_BifocalBoundCircRadius = 0 ;
	m_TrifocalBoundCircCenterX = 0 ;
	m_TrifocalBoundCircCenterY = 0 ;
	m_TrifocalBoundCircRadius  = 0  ;
	m_Addit = 0 ;
	m_PantoRadius = 0 ;
	m_SegmentType = wsEmbedded;
	m_TrifocalAdditRatio = 0.5 ;
	m_ErrorState = 0 ;
	m_BifocalOpticalCenterShiftX = 0;
	m_BifocalOpticalCenterShiftY = 0;
	m_TrifocalOpticalCenterShiftX = 0;
	m_TrifocalOpticalCenterShiftY = 0;
}

CMultifocalSegment::~CMultifocalSegment()
{
}

double CMultifocalSegment::GetMainCircCenterX() 
{
	return m_MainCircCenterX ;
}

bool CMultifocalSegment::SetMainCircCenterX(double newValue) 
{
	if( fabs( m_MainCircCenterX - newValue ) < eps ) return true;
	m_MainCircCenterX = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

string CMultifocalSegment::GetName() 
{
	return m_Name;
}

bool CMultifocalSegment::SetName(LPCTSTR lpszNewValue) 
{
	if(lpszNewValue) {
		m_Name = lpszNewValue;
		SetModifiedFlag();
		FireChange();
		return true;
	}
	return false;
}

double CMultifocalSegment::GetMainCircCenterY() 
{
	return m_MainCircCenterY ;
}

bool CMultifocalSegment::SetMainCircCenterY(double newValue) 
{
	if( fabs( m_MainCircCenterY - newValue ) < eps ) return true;
	m_MainCircCenterY = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

double CMultifocalSegment::GetMainCircRadius() 
{
	return m_MainCircRadius ;
}

bool CMultifocalSegment::SetMainCircRadius(double newValue) 
{
	if( fabs( m_MainCircRadius - newValue ) < eps ) return true;
	m_MainCircRadius = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

double CMultifocalSegment::GetBifocalBoundCircCenterX() 
{
	return m_BifocalBoundCircCenterX;
}

bool CMultifocalSegment::SetBifocalBoundCircCenterX(double newValue) 
{
	if( fabs( m_BifocalBoundCircCenterX - newValue ) < eps ) return true;
	m_BifocalBoundCircCenterX = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

double CMultifocalSegment::GetBifocalBoundCircCenterY() 
{
	return m_BifocalBoundCircCenterY;
}

bool CMultifocalSegment::SetBifocalBoundCircCenterY(double newValue) 
{
	if( fabs( m_BifocalBoundCircCenterY - newValue ) < eps ) return true;
	m_BifocalBoundCircCenterY = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

double CMultifocalSegment::GetBifocalBoundCircRadius() 
{
	return m_BifocalBoundCircRadius ;
}

bool CMultifocalSegment::SetBifocalBoundCircRadius(double newValue) 
{
	if( fabs( m_BifocalBoundCircRadius - newValue ) < eps ) return true;
	m_BifocalBoundCircRadius = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

double CMultifocalSegment::GetTrifocalBoundCircCenterX() 
{
	return m_TrifocalBoundCircCenterX ;
}

bool CMultifocalSegment::SetTrifocalBoundCircCenterX(double newValue) 
{
	if( fabs( m_TrifocalBoundCircCenterX - newValue ) < eps ) return true;
	m_TrifocalBoundCircCenterX = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

double CMultifocalSegment::GetTrifocalBoundCircCenterY() 
{
	return m_TrifocalBoundCircCenterY;
}

bool CMultifocalSegment::SetTrifocalBoundCircCenterY(double newValue) 
{
	if( fabs( m_TrifocalBoundCircCenterY - newValue ) < eps ) return true;
	m_TrifocalBoundCircCenterY = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

double CMultifocalSegment::GetTrifocalBoundCircRadius() 
{
	return m_TrifocalBoundCircRadius ;
}

bool CMultifocalSegment::SetTrifocalBoundCircRadius(double newValue) 
{
	if( fabs( m_TrifocalBoundCircRadius  - newValue ) < eps ) return true;
	m_TrifocalBoundCircRadius = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

double CMultifocalSegment::GetAddit() 
{
	return m_Addit ;
}

bool CMultifocalSegment::SetAddit(double newValue) 
{
	if( fabs( m_Addit - newValue ) < eps ) return true;
	m_Addit = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

double CMultifocalSegment::GetPantoRadius() 
{
	return m_PantoRadius ;
}

bool CMultifocalSegment::SetPantoRadius(double newValue) 
{
	if( fabs( m_PantoRadius - newValue ) < eps ) return true;
	m_PantoRadius = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

MultifocalSegmentType CMultifocalSegment::GetSegmentType() 
{
	return m_SegmentType ;
}

bool CMultifocalSegment::SetSegmentType(MultifocalSegmentType newValue) 
{
	if( m_SegmentType == newValue ) return true;

	if(newValue != wsEmbedded && newValue != wsRaised &&
		newValue != wsFrontSurfLowered && newValue != wsBackSurfLowered)
			return false;

	m_SegmentType = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

bool CMultifocalSegment::DoPropExchange(CPropObjectExchange* pPX) 
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	if(pPX->PX_Double( "MainCircCenterX", m_MainCircCenterX ) == false)
		return false;
	if(pPX->PX_Double( "MainCircCenterY", m_MainCircCenterY ) == false)
		return false;
	if(pPX->PX_Double( "MainCircRadius", m_MainCircRadius ) == false)
		return false;
	if(pPX->PX_Double( "BifocalBoundCircCenterX", m_BifocalBoundCircCenterX ) == false)
		return false;
	if(pPX->PX_Double( "BifocalBoundCircCenterY", m_BifocalBoundCircCenterY ) == false)
		return false;
	if(pPX->PX_Double( "BifocalBoundCircRadius", m_BifocalBoundCircRadius ) == false)
		return false;
	if(pPX->PX_Double( "BifocalOpticalCenterShiftX", m_BifocalOpticalCenterShiftX) == false)
		return false;
	if(pPX->PX_Double( "BifocalOpticalCenterShiftY", m_BifocalOpticalCenterShiftY) == false)
		return false;
	if(pPX->PX_Double( "BifocalBoundCircRadius", m_BifocalBoundCircRadius ) == false)
		return false;
	if(pPX->PX_Double( "TrifocalBoundCircCenterX", m_TrifocalBoundCircCenterX ) == false)
		return false;
	if(pPX->PX_Double( "TrifocalBoundCircCenterY", m_TrifocalBoundCircCenterY ) == false)
		return false;
	if(pPX->PX_Double( "TrifocalBoundCircRadius", m_TrifocalBoundCircRadius ) == false)
		return false;
	if(pPX->PX_Double( "TrifocalOpticalCenterShiftX", m_TrifocalOpticalCenterShiftX) == false)
		return false;
	if(pPX->PX_Double( "TrifocalOpticalCenterShiftY", m_TrifocalOpticalCenterShiftY) == false)
		return false;
	if(pPX->PX_Double( "Addit", m_Addit ) == false)
		return false;
	if(pPX->PX_Double( "TrifocalAdditRatio", m_TrifocalAdditRatio ) == false)
		return false;
	if(pPX->PX_Double( "PantoRadius", m_PantoRadius ) == false)
		return false;
	if(pPX->PX_Long( "SegmentType", (long&)m_SegmentType ) == false)
		return false;
	if(pPX->PX_String( "Name", m_Name, "" ) == false)
		return false;

	return true;
}

void CMultifocalSegment::DataValidate()
{
	BOOL PantoPresent ;
	BOOL BifocalPresent ;
	BOOL TrifocalPresent ;

	if( m_Addit < -4 || m_Addit > 4 ) { 
		m_ErrorState = 1 ;
		return ; 
	}
	if( m_TrifocalAdditRatio < 0 || m_TrifocalAdditRatio > 1 ) {
		m_ErrorState = 1 ;
		return ; 
	}
	if( m_PantoRadius > m_MainCircRadius ) {
		m_ErrorState = 3 ;
		return ; 
	}
	if( m_SegmentType == wsFrontSurfLowered && m_Addit > 0 ) {
		m_ErrorState = 2 ;
		return ;
	}
	if( m_SegmentType == wsBackSurfLowered && m_Addit > 0 ) {
		m_ErrorState = 2 ;
		return ;
	}
	if( m_SegmentType == wsRaised && m_Addit < 0 ) {
		m_ErrorState = 2 ;
		return ;
	}
	if( m_SegmentType == wsEmbedded && m_Addit < 0 ) {
		m_ErrorState = 2 ;
		return ;
	}

	BifocalPresent = (m_BifocalBoundCircRadius > EPS);
	TrifocalPresent = (BifocalPresent && m_TrifocalBoundCircRadius > EPS);
	PantoPresent = (m_PantoRadius > EPS && m_PantoRadius <= m_MainCircRadius);

	double lb = _hypot(m_BifocalBoundCircCenterX - m_MainCircCenterX,
						m_BifocalBoundCircCenterY - m_MainCircCenterY);
	double lt = _hypot(m_TrifocalBoundCircCenterX - m_MainCircCenterX,
						m_TrifocalBoundCircCenterY - m_MainCircCenterY);

	if(  BifocalPresent &&
		(lb + m_BifocalBoundCircRadius < m_MainCircRadius ||
			lb - m_MainCircRadius > m_BifocalBoundCircRadius ||
				lb + m_MainCircRadius < m_BifocalBoundCircRadius)){
		m_ErrorState = 4 ;
		return ;
	}

	if(TrifocalPresent &&
		(lt + m_TrifocalBoundCircRadius < m_MainCircRadius ||
			lt - m_MainCircRadius > m_TrifocalBoundCircRadius ||
				lt + m_MainCircRadius < m_TrifocalBoundCircRadius)){
		m_ErrorState = 5 ;
		return ;
	}

	// ������ ����:
	// - ����� ����������� �����. �  ������. ���-��� ����� ��� ������� ���-��
	// - ����� ����������� ������. � ������� ���-��� ����� ��� �����. ���-��
	if(TrifocalPresent)
	{
		CCirclesCrossPoints ccp;
		ccp.Calculate(m_BifocalBoundCircCenterX, m_BifocalBoundCircCenterY, m_BifocalBoundCircRadius,
						m_TrifocalBoundCircCenterX, m_TrifocalBoundCircCenterY, m_TrifocalBoundCircRadius);
        int i = 0;
        for(i = 0; i < ccp.nPoints; i++)
			if(_hypot(ccp.X[i] - m_MainCircCenterX, ccp.Y[i] - m_MainCircCenterY) < m_MainCircRadius)
			{
				m_ErrorState = 6;
				return;
			}

		ccp.Calculate(m_TrifocalBoundCircCenterX, m_TrifocalBoundCircCenterY, m_TrifocalBoundCircRadius,
						m_MainCircCenterX, m_MainCircCenterY, m_MainCircRadius);
		for(i = 0; i < ccp.nPoints; i++)
			if(_hypot(ccp.X[i] - m_BifocalBoundCircCenterX, ccp.Y[i] - m_BifocalBoundCircCenterY) < m_BifocalBoundCircRadius)
			{
				m_ErrorState = 6;
				return;
			}
	}

	m_ErrorState = 0 ;
}

// 0 -	all data correct
// 1 -	Addit, TrifocalAdditRatio values are out of valid range
// 2 -	Addit and SegmentType values mismatch
// 3 -	PantoRadius value greater then MainSegmentCircleRadius value 
// 4 -	bifocal boundary circle does not intersect  main segment circle 
// 5 -	trifocal boundary circle does not intersect main segment circle 
// 6 -	trifocal zone can not be placed on upper side of bifocal zone 
unsigned long CMultifocalSegment::GetErrorState() 
{
	DataValidate();
	return m_ErrorState;
}

double CMultifocalSegment::GetTrifocalOpticalCenterShiftX() 
{
	return m_TrifocalOpticalCenterShiftX ;
}

bool CMultifocalSegment::SetTrifocalOpticalCenterShiftX(double newValue) 
{
	if( fabs( m_TrifocalOpticalCenterShiftX - newValue ) < eps ) return true;
	m_TrifocalOpticalCenterShiftX = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

double CMultifocalSegment::GetTrifocalOpticalCenterShiftY() 
{
	return m_TrifocalOpticalCenterShiftX ;
}

bool CMultifocalSegment::SetTrifocalOpticalCenterShiftY(double newValue) 
{
	if( fabs( m_TrifocalOpticalCenterShiftY - newValue ) < eps ) return true;
	m_TrifocalOpticalCenterShiftY = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

double CMultifocalSegment::GetBifocalOpticalCenterShiftX() 
{
	return m_BifocalOpticalCenterShiftX ;
}

bool CMultifocalSegment::SetBifocalOpticalCenterShiftX(double newValue) 
{
	if( fabs( m_BifocalOpticalCenterShiftX - newValue ) < eps ) return true;
	m_BifocalOpticalCenterShiftX = newValue ;

	SetModifiedFlag();
	FireChange();
	return true;
}

double CMultifocalSegment::GetBifocalOpticalCenterShiftY() 
{
	return m_BifocalOpticalCenterShiftY ;
}

bool CMultifocalSegment::SetBifocalOpticalCenterShiftY(double newValue) 
{
	if( fabs( m_BifocalOpticalCenterShiftY - newValue ) < eps ) return true;
	m_BifocalOpticalCenterShiftY = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

double CMultifocalSegment::GetTrifocalAdditRatio() 
{
	return m_TrifocalAdditRatio;
}

bool CMultifocalSegment::SetTrifocalAdditRatio(double newValue) 
{
	if( fabs( m_TrifocalAdditRatio - newValue ) < eps ) return true;
	m_TrifocalAdditRatio = newValue;

	SetModifiedFlag();
	FireChange();
	return true;
}

IMultifocalSegment * CMultifocalSegment::Clone() 
{
	CMultifocalSegment *pNewObject = CMultifocalSegment::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}
