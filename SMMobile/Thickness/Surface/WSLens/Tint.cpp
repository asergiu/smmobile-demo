// Tint.cpp : implementation file
//

#include "stdafx.h"
#include "Tint.h"
#include "TintPoint.h"
#include "StringConvert.h"

/////////////////////////////////////////////////////////////////////////////
// CTint

CTint* CTint::CreateObject()
{
	CTint *pObject = new CTint;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

ITint * CTint::GetInterface()
{
	return static_cast<ITint*>(this);
}

CTint::CTint()
{
	IsFinalClear = false;
}

CTint::~CTint()
{
}

unsigned long CTint::GetCount()
{
	return pt.size();
}

ITintPoint * CTint::GetItem(unsigned long Index) 
{
	VTintPoint::size_type nCount = pt.size();
	if(!TestRange(Index, 1, nCount, 1))
		return NULL;
	if(pt[Index-1])
		pt[Index-1]->AddRef();

	return pt[Index-1];
}

bool CTint::SetItem(unsigned long Index, ITintPoint * newValue)
{
	VTintPoint::size_type nCount = pt.size();
	if(!TestRange(Index, 1, nCount, 1))
		return false;
	
	if(pt[Index-1])
	{
		UnadviseConnection(/*pt[Index-1],*/ Index-1);
	}
	
	pt[Index-1] = newValue;
	
	if(pt[Index-1])
	{
		AdviseConnection(pt[Index-1], Index-1);
	}

	FireChange();
	SetModifiedFlag();
	return true;
}

bool CTint::Clear() 
{
	VTintPoint::size_type i, nCount = pt.size();
	for(i = 0; i < nCount; i++){
		UnadviseConnection(/*pt[i],*/ i);
	}

	pt.clear();

	if(!IsFinalClear){
		FireChange();
		SetModifiedFlag();
	}
	return true;
}

ITintPoint * CTint::Add(unsigned long Rvalue, unsigned long Gvalue, unsigned long Bvalue)
{
	VTintPoint::size_type nCount = pt.size();
    if(	!TestRangeMin0(Rvalue, 255, 1)	||
        !TestRangeMin0(Gvalue, 255, 1)	||
        !TestRangeMin0(Bvalue, 255, 1)	||
		!TestRange(nCount+1, 1, Max_Count, 2)) return NULL;
	
	CTintPoint * pNewObj = NULL;
	ITintPoint * pNewPt = NULL;

	pNewObj = CTintPoint::CreateObject();
	if(pNewObj) {
		pNewPt = pNewObj->GetInterface();
		if(pNewPt) {
			pNewPt->SetPoint(Rvalue, Gvalue, Bvalue);
			pt.push_back(pNewPt);

			AdviseConnection(pt[nCount], nCount);

			FireChange();
			SetModifiedFlag();
		}
		else
			pNewObj->Release();
	}
	return pNewPt;
}

void CTint::OnChildChanged()
{
	FireChange();
	SetModifiedFlag();
}

ITint * CTint::Clone() 
{
	CTint *pNewObject = CTint::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}

string CTint::GetName() 
{
	return m_Name;
}

bool CTint::SetName(LPCTSTR lpszNewValue) 
{
	if(lpszNewValue) {
		m_Name = lpszNewValue;
		SetModifiedFlag();
		FireChange();
		return true;
	}
	return false;
}

string CTint::GetEDPCode() 
{
	return m_EDPCode;
}

bool CTint::SetEDPCode(LPCTSTR lpszNewValue) 
{
	if(lpszNewValue) {
		m_EDPCode = lpszNewValue;
		SetModifiedFlag();
		FireChange();
		return true;
	}
	return false;
}

string CTint::GetInfo() 
{
	return m_Info;
}

bool CTint::SetInfo(LPCTSTR lpszNewValue) 
{
	if(lpszNewValue) {
		m_Info = lpszNewValue;
		SetModifiedFlag();
		FireChange();
		return true;
	}
	return false;
}

bool CTint::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	string PropName;
	unsigned long i, nLoadCount;

	if(pPX->IsLoading()){
		Clear();
		if(pPX->PX_Long("Count", nLoadCount, 0) == false)
			return false;
		for(i = 0; i < nLoadCount; i++) {
			PropName = "TintPoint" + toString(i);
			CTintPoint * ptObject = CTintPoint::CreateObject();
			if(ptObject) {
				CObjPtr<ITintPoint> ptTemp;
				ptTemp.AttachDispatch(ptObject->GetInterface());
				if(PX_WSLObject<CTintPoint, ITintPoint>(pPX, PropName.c_str(), ptTemp, NULL) == false)
					return false;
				if(ptTemp)
					Add(ptTemp->GetR(), ptTemp->GetG(), ptTemp->GetB())->Release();
			}
			else
				return false;
		}
	}
	else {
		nLoadCount = GetCount();
		if(pPX->PX_Long("Count", nLoadCount) == false)
			return false;
		for(i = 0; i < nLoadCount; i++)
		{
			PropName = "TintPoint" + toString(i);
			if(PX_WSLObject<CTintPoint, ITintPoint>(pPX, PropName.c_str(), pt[i], NULL) == false)
				return false;
		}
	}

	if(pPX->PX_String("Name", m_Name, "") == false)
		return false;
	if(pPX->PX_String("EDPCode", m_EDPCode, "") == false)
		return false;
	if(pPX->PX_String("Info", m_Info, "") == false)
		return false;

	return true;
}
