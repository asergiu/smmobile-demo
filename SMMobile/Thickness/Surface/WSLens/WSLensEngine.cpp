// WSLensEngine.cpp : implementation file
//

#include "stdafx.h"
#include "WSLensEngine.h"
#include <math.h>
#include "Parameters/Asph2.h"

void CWSLensEngine::SetLastErrorCode(int n)
{
	nLastErrorCode = n;
}

int CWSLensEngine::GetLastErrorCode(void)
{
	return nLastErrorCode;
}

/////////////////////////////////////////////////////////////////////////////
// CWSLensEngine

CWSLensEngine* CWSLensEngine::CreateObject()
{
	CWSLensEngine *pObject = new CWSLensEngine;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

ILensEngine * CWSLensEngine::GetInterface()
{
	return static_cast<ILensEngine*>(this);
}

CWSLensEngine::CWSLensEngine()
{
	nWeAreHere = 0;
	nLastErrorCode = 0;
}

CWSLensEngine::~CWSLensEngine()
{
}

bool CWSLensEngine::CalcWeight(ILens * LensObject, double *pResultWeight)
{
	return _CalcWeight(LensObject, pResultWeight);
}

bool CWSLensEngine::CalcSurface(ILens * LensObject, ILensSurface * LensSurface)
{
	return _CalcSurface(LensObject, LensSurface);
}

void CWSLensEngine::GetDepth( ILens* LensObject, double x, double y, double & rear, double & front )
{
	return _GetDepth( LensObject, x, y, rear, front );
}

bool CWSLensEngine::CalcThickness(	ILens * LensObject,
									double *ResultCenterThickness,
									double *ResultMinEdgeThickness,
									double *ResultMaxEdgeThickness)
{
	return _CalcThickness(	LensObject,
							ResultCenterThickness,
							ResultMinEdgeThickness,
							ResultMaxEdgeThickness);
}

unsigned long CWSLensEngine::GetSupportedMethods()
{
	return _GetSupportedMethods();
}


string CWSLensEngine::GetLastErrorMessage()
{
	return _GetLastErrorMessage();
}

bool CWSLensEngine::CalcUVProtection(ILens * LensObject, double * ResultUVProtection)
{
	return _CalcUVProtection(LensObject, ResultUVProtection);
}

int CWSLensEngine::ConvertToWSDesign(LPCTSTR lpstrDesign)
{
	if(stricmp(lpstrDesign, wsClassic.c_str()) == 0)
		return 0;
	if(stricmp(lpstrDesign, wsAspheric.c_str()) == 0)
		return 1;
	if(stricmp(lpstrDesign, wsLenticular.c_str()) == 0)
		return 2;

	return -1;
}

/////////////////////////////////////////////////////////////////////////////
// Persistence

bool CWSLensEngine::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	return true;
}

ILensEngine * CWSLensEngine::Clone() 
{
	CWSLensEngine *pNewObject = CWSLensEngine::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}
