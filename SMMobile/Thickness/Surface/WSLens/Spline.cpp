#include "stdafx.h"
#include <math.h>
#include <stdlib.h>
//#include <malloc.h>
#include "Spline.h"
#include <float.h>
#include <assert.h>

int CSpline::AdvArrComp(const void *Elem1,const void *Elem2)
{
	if(((AdvArrType *)Elem1)->Phi > ((AdvArrType *)Elem2)->Phi)
		return 1;
	else
		return -1;
}

FType CSpline::Azimut(FType Y, FType X)
{
	if(_hypot(Y, X) < EPS)
		return 0.;
	FType Temp = atan2(Y, X), Temp1 = 2.0 * M_PI;
	if(Temp >= 0)
		return Temp;
	else
		return Temp + Temp1;
}

FType CSpline::Azimut(SIZE sz)
{
	return Azimut(sz.cy, sz.cx);
}

FType CSpline::Azimut(FTPOINT pt)
{
	return Azimut(pt.y, pt.x);
}

FType CSpline::Hypot(SIZE sz)
{
	return _hypot(sz.cx, sz.cy);
}

FType CSpline::Hypot(FTPOINT pt)
{
	return _hypot(pt.x, pt.y);
}

FType CSpline::Hypot(FTPOINT pt1, FTPOINT pt2)
{
	return _hypot(pt1.x - pt2.x, pt1.y - pt2.y);
}

CSpline::CSpline()
{
	SplExists				= FALSE;
	nPolyNodes				= MAX_POLY_NODES;
	Arr						= NULL;
	ArrX					= NULL;
	ArrY					= NULL;
	PolyNodes				= NULL;
	FloatPolyNodes			= NULL;
	MakeProportionalSegment	= TRUE;
	Dist = 0.;
}

CSpline::~CSpline()
{
	if(Arr != NULL)
		free(Arr);

	if(ArrX != NULL)
		free(ArrX);

	if(ArrY != NULL)
		free(ArrY);

	if(PolyNodes != NULL)
		free(PolyNodes);

	if(FloatPolyNodes != NULL)
		free(FloatPolyNodes);
}

void CSpline::DestroySpline (void)
{
	SplX.DestroyCompSpline();
	SplY.DestroyCompSpline();

	if(SplExists == TRUE){
		if(Arr != NULL) free(Arr);
		if(ArrX != NULL) free(ArrX);
		if(ArrY != NULL) free(ArrY);
		if(PolyNodes != NULL) free(PolyNodes);
		if(FloatPolyNodes != NULL) free(FloatPolyNodes);
		Arr = NULL;  ArrX = NULL;  ArrY = NULL;  PolyNodes = NULL; FloatPolyNodes = NULL;
		SplExists = FALSE;
	}
}

void CSpline::DuplicateSpline(int nCoordIn, FTPOINT * CoordFloatIn)
{
	if(nCoordIn < 3)
		return;

	nPolyNodes = nCoordIn;

	if(PolyNodes)
		delete []PolyNodes;
	PolyNodes = new POINT[nPolyNodes + 1];
	if(!PolyNodes)
		return;

	if(FloatPolyNodes)
		delete []FloatPolyNodes;
	FloatPolyNodes = new CFTPoint[nPolyNodes + 1];
	if(!FloatPolyNodes)
	{
		delete []PolyNodes; PolyNodes = NULL;
		return;
	}
	for(int i = 0; i < nPolyNodes; i++)
	{
		PolyNodes[i].x=Round(CoordFloatIn[i].x);
		PolyNodes[i].y=Round(CoordFloatIn[i].y);
		FloatPolyNodes[i].x=CoordFloatIn[i].x;
		FloatPolyNodes[i].y=CoordFloatIn[i].y;


		if(i == 0)
		{
			PolyNodes[nPolyNodes]=PolyNodes[0];
			FloatPolyNodes[nPolyNodes]=FloatPolyNodes[0];
		}
	}
	SplExists = TRUE;
}

void CSpline::CreateSpline(int nCoordIn, FTPOINT * CoordFloatIn)
{
	int i, j;
	FTPOINT ptCmFloat;

	FTPOINT *NonDoubledPoints = NULL, *CoordFloat = NULL;
	int NumOfNonDoubledPoints = 0, nCoord;

	if(nCoordIn < 3)
		return;

	// ��������� �� ����������� �����
	NonDoubledPoints = (FTPOINT *)calloc(nCoordIn, sizeof(FTPOINT));
    assert(NonDoubledPoints);
	if(!NonDoubledPoints)
		return;

	for(i = 0; i < nCoordIn; i++)
	{
		BOOL NonRepeatPoint = FALSE;
		for(j = 0; j < NumOfNonDoubledPoints; j++)
		{
			if(fabs(NonDoubledPoints[j].x - CoordFloatIn[i].x) < EPS &&
				fabs(NonDoubledPoints[j].y - CoordFloatIn[i].y) < EPS)
			{
				NonRepeatPoint = TRUE;
				break;
			}
		}

		if(NonRepeatPoint == FALSE)
			NonDoubledPoints[NumOfNonDoubledPoints++] = CoordFloatIn[i];
	}

	nCoord = NumOfNonDoubledPoints;
	CoordFloat = NonDoubledPoints;
	if(NumOfNonDoubledPoints < 3)
	{
//		ASSERT(0);
		goto ErrorCreate;
	}

	ptCmFloat.x = ptCmFloat.y = 0.;
	for(i = 0; i < nCoord; i++){
		ptCmFloat.x += CoordFloat[i].x;
		ptCmFloat.y += CoordFloat[i].y;
	}
	ptCmFloat.x /= nCoord;  ptCmFloat.y /= nCoord;
	
	if(Arr)
		free(Arr);
	Arr = (AdvArrType *)malloc(sizeof(AdvArrType) * nCoord);
	if(ArrX)
		free(ArrX);
	ArrX = (PFTPOINT)malloc(sizeof(FTPOINT) * nCoord);
	if(ArrY)
		free(ArrY);
	ArrY = (PFTPOINT)malloc(sizeof(FTPOINT) * nCoord);
	if(Arr == NULL || ArrX == NULL || ArrY == NULL)
	{
ErrorCreate:
		if(Arr != NULL) free(Arr);
		if(ArrX != NULL) free(ArrX);
		if(ArrY != NULL) free(ArrY);
		if(CoordFloat != NULL) free(NonDoubledPoints);
		Arr = NULL;  ArrX = NULL;  ArrY = NULL;  NonDoubledPoints = NULL;
		return;
	}

	for(i = 0; i < nCoord; i++)
	{
		Arr[i].x = CoordFloat[i].x;
		Arr[i].y = CoordFloat[i].y;
		Arr[i].Phi = Azimut(CoordFloat[i].y - ptCmFloat.y, CoordFloat[i].x - ptCmFloat.x);
	}

	qsort(Arr, nCoord, sizeof(AdvArrType), AdvArrComp);

	Dist = 0.f;
	for(i = 0; i < nCoord; i++)
	{
		j = (i < nCoord - 1) ? (i + 1) : 0;
		Arr[i].t = Dist;
		Dist += (FType)_hypot(Arr[j].x - Arr[i].x, Arr[j].y - Arr[i].y);
	}
	
	for(i = 0; i < nCoord; i++)
	{
		ArrX[i].x = ArrY[i].x = Arr[i].t;
		ArrX[i].y = (FType)Arr[i].x;
		ArrY[i].y = (FType)Arr[i].y;
	}
	
	SplX.SplExists = SplX.CreateCompSpline(ArrX, nCoord, Dist);
	if(SplX.SplExists == FALSE)
		goto ErrorCreate;
	SplY.SplExists = SplY.CreateCompSpline(ArrY, nCoord, Dist);
	if(SplY.SplExists == FALSE)
	{
		SplX.DestroyCompSpline();
		goto ErrorCreate;
	}

	if(PolyNodes)
		delete []PolyNodes;
	PolyNodes = (nPolyNodes >= 3) ? (new POINT[nPolyNodes + 1]) : NULL;

	if(FloatPolyNodes)
		delete []FloatPolyNodes;
	FloatPolyNodes = (nPolyNodes >= 3) ? (new CFTPoint[nPolyNodes + 1]) : NULL;

	if(!PolyNodes || !FloatPolyNodes)
	{
		if(PolyNodes)
		{
			delete []PolyNodes;
			PolyNodes = NULL;
		}
		if(FloatPolyNodes)
		{
			delete []FloatPolyNodes;
			FloatPolyNodes = NULL;
		}
		SplX.DestroyCompSpline();
		SplY.DestroyCompSpline();
		goto ErrorCreate;
	}

	if(MakeProportionalSegment)
	{
		FType StepT, CurT;

		for(i = 0, CurT = 0.f, StepT = Dist / nPolyNodes; i < nPolyNodes; i++, CurT += StepT)
		{
			FloatPolyNodes[i].x = SplX.ComputeCompSpline(CurT);
			FloatPolyNodes[i].y = SplY.ComputeCompSpline(CurT);
			PolyNodes[i].x = Round(FloatPolyNodes[i].x);
			PolyNodes[i].y = Round(FloatPolyNodes[i].y);

			if(i == 0)
			{
				FloatPolyNodes[nPolyNodes] = FloatPolyNodes[0];
				PolyNodes[nPolyNodes] = PolyNodes[0];
			}
		}
	}
	else
	{
		if(DisposeSplinePoint() == FALSE)
		{
			SplX.DestroyCompSpline();
			SplY.DestroyCompSpline();
			delete []PolyNodes; PolyNodes = NULL;
			delete []FloatPolyNodes; FloatPolyNodes = NULL;
			goto ErrorCreate;
		}
	}

	free(CoordFloat);

	SplExists = TRUE;
}
void CSpline::CreateSpline(int nCoord, FTPOINT * CoordFloat, int nOut)
{
	nPolyNodes = nOut;
	CreateSpline(nCoord, CoordFloat);
}

void CSpline::RecreateSpline(int nNewPointsNumber)
{
	if(!SplExists)
		return;

	if(PolyNodes)
		delete []PolyNodes;
	if(FloatPolyNodes)
		delete []FloatPolyNodes;

	nPolyNodes = nNewPointsNumber;

	PolyNodes = new POINT[nPolyNodes + 1];
	FloatPolyNodes = new CFTPoint[nPolyNodes + 1];
	if(PolyNodes)
		DisposeSplinePoint();
}

BOOL CSpline::DisposeSplinePoint()
{
	int i;
	PSFTPOINT PolyLine;
	FType StepT, CurT;
	static int nPolyLine = 200;
	BOOL ErrorFlag = TRUE;
	int nPoint = 0;

	PolyLine = (PSFTPOINT)malloc(sizeof(SFTPOINT) * (nPolyLine+1));
	if(PolyLine != NULL)
	{
		for(i = 0, CurT = 0.f, StepT = Dist/nPolyLine; i < nPolyLine; i++, CurT += StepT)
		{
			PolyLine[i].x = SplX.ComputeCompSpline(CurT);
			PolyLine[i].y = SplY.ComputeCompSpline(CurT);
			PolyLine[i].SelectedFlag = FALSE;

			if(i == 0) PolyLine[nPolyLine] = PolyLine[0];
		}
		
		// ���������� ��������� � ��������� ����� ������
		double yMin, yMax;
		int iMin, iMax;
		
		yMin = yMax = PolyLine[0].y;
		iMin = iMax = 0;
		for(i = 1; i < nPolyLine; i++)
		{
			if(PolyLine[i].y < yMin){yMin = PolyLine[i].y; iMin = i;}
			if(PolyLine[i].y > yMax){yMax = PolyLine[i].y; iMax = i;}
		}

		if(iMax != iMin)
		{
			// ���������� �����, ������� ���������� ������ ������ ������ � �����
			double yDelta, dy2;
			int iLeft, iRight;

			// ���������� 1-��� �����
			yDelta = PolyLine[iMax].y - PolyLine[iMin].y;
			dy2 = PolyLine[iMin].y + yDelta / 2.;
			i = (iMin == nPolyLine - 1)? 0:(iMin + 1);
			iLeft = -1;
			while(i != iMax)
			{
				if(fabs(PolyLine[i].y - dy2) < yDelta)
				{
					iLeft = i;
					yDelta = fabs(PolyLine[i].y - dy2);
				}

				i = (i == nPolyLine - 1)? 0:(i + 1);
			}
			
			// ���������� 2-��� �����
			yDelta = PolyLine[iMax].y - PolyLine[iMin].y;
			i = (iMax == nPolyLine - 1)? 0:(iMax + 1);
			iRight = -1;
			while(i != iMin)
			{
				if(fabs(PolyLine[i].y - dy2) < yDelta)
				{
					iRight = i;
					yDelta = fabs(PolyLine[i].y - dy2);
				}

				i = (i == nPolyLine - 1)? 0:(i + 1);
			}
			
			// ��������� � ��������� ����� ��������� ����������
			// ������ � ����� ����� ��������� ����������
			PolyLine[iMin].SelectedFlag = PolyLine[iMax].SelectedFlag = TRUE;
			if(iLeft >= 0)
				PolyLine[iLeft].SelectedFlag = TRUE;
			if(iRight >= 0)
				PolyLine[iRight].SelectedFlag = TRUE;

			// ���������� NextPoint ��� ��������� ����� � ������������ ����� ����� ���������
			nPoint = 0;
			i = (iMin == nPolyLine - 1)? 0:(iMin + 1);
			int iPrev = iMin;
			while(1)
			{
				if(PolyLine[i].SelectedFlag)
				{
					PolyLine[iPrev].NextPoint = i;
					CalculatePointH(PolyLine, nPolyLine, iPrev, i);
					iPrev = i;
					nPoint++;
				}

				if(i == iMin)
					break;

				i = (i == nPolyLine - 1)? 0:(i + 1);
			}

			while(nPoint != nPolyNodes)
			{
				double hMax = -1.;
				int ihMax = -1;
				for(i = 0; i < nPolyLine; i++)
				{
					if(PolyLine[i].SelectedFlag)
						continue;
					if(PolyLine[i].h > hMax)
						{hMax = PolyLine[i].h; ihMax = i;}
				}
				if(ihMax == -1)
					{ErrorFlag = FALSE; break;}
				i = ihMax;
				int PreviousPoint = -1;
				while(PreviousPoint == -1)
				{
					i = (i == 0)? (nPolyLine-1):(i - 1);
					if(!PolyLine[i].SelectedFlag)
						continue;
					PreviousPoint = i;
				}
				PolyLine[ihMax].SelectedFlag = TRUE;
				PolyLine[ihMax].NextPoint = PolyLine[PreviousPoint].NextPoint;
				PolyLine[PreviousPoint].NextPoint = ihMax;
				CalculatePointH(PolyLine, nPolyLine, PreviousPoint, ihMax);
				CalculatePointH(PolyLine, nPolyLine, ihMax, PolyLine[ihMax].NextPoint);
				nPoint++;
			}
		}
		else
			ErrorFlag = FALSE;
		
		if(ErrorFlag)
		{
			int j;
			for(i = 0, j = iMin; i < nPolyNodes; i++)
			{
				PolyNodes[i].x = Round(PolyLine[j].x);
				PolyNodes[i].y = Round(PolyLine[j].y);
				FloatPolyNodes[i].x = PolyLine[j].x;
				FloatPolyNodes[i].y = PolyLine[j].y;
				j = PolyLine[j].NextPoint;

				if(i == 0)
				{
					PolyNodes[nPolyNodes] = PolyNodes[0];
					FloatPolyNodes[nPolyNodes] = FloatPolyNodes[0];
				}
			}
		}

		free(PolyLine);
	}
	else
		ErrorFlag = FALSE;

	return ErrorFlag;
}

void CSpline::CalculatePointH(PSFTPOINT PolyLine, int nPolyLine, int sPoint, int ePoint)
{
	if(PolyLine == NULL) return;
	if(sPoint < 0 || sPoint >= nPolyLine) return;
	if(ePoint < 0 || ePoint >= nPolyLine) return;
	if(abs(ePoint - sPoint) < 2) return;

	int i;

	double a = _hypot(PolyLine[ePoint].x - PolyLine[sPoint].x, PolyLine[ePoint].y - PolyLine[sPoint].y);
	i = (sPoint == nPolyLine - 1)? 0:(sPoint + 1);
	while(i != ePoint){
		double b = _hypot(PolyLine[i].x - PolyLine[sPoint].x, PolyLine[i].y - PolyLine[sPoint].y);
		double c = _hypot(PolyLine[ePoint].x - PolyLine[i].x, PolyLine[ePoint].y - PolyLine[i].y);
		double CosAlfa = (a * a + b * b - c * c) / (2. * a * b);
		PolyLine[i].h = b * (1. - CosAlfa * CosAlfa);
		i = (i == nPolyLine - 1)? 0:(i + 1);
	}
}

// ���������� ����� �� _������������_ �������
// ������� ���������� �������
BOOL CSpline::DecreasePointsNumber(int nNewPointsNumber)
{
	if(!SplExists || nNewPointsNumber < 3 || nNewPointsNumber >= nPolyNodes)
		return FALSE;

	int i;
	PSFTPOINT PolyLine;
	PolyLine = (PSFTPOINT)calloc(nPolyNodes, sizeof(SFTPOINT));
	if(PolyLine == NULL)
		return FALSE;

	// ��������� ����������
	for(i = 0; i < nPolyNodes; i++)
	{
		PolyLine[i].x = FloatPolyNodes[i].x;
		PolyLine[i].y = FloatPolyNodes[i].y;
		PolyLine[i].SelectedFlag = TRUE;
		PolyLine[i].NextPoint = GetNextIndex(i);
		PolyLine[i].PrevPoint = GetPrevIndex(i);
	}
	for(i = 0; i < nPolyNodes; i++)
		CalculatePointH(PolyLine, nPolyNodes, GetPrevIndex(i), GetNextIndex(i));

	// ���� ����������� �����
	int nPoints = nPolyNodes;
    while(nPoints > nNewPointsNumber)
	{
        // ���� ������� h
		double min_h = DBL_MAX;
		int min_idx;
		for(i = 0; i < nPolyNodes; i++)
		{
			if(PolyLine[i].SelectedFlag && PolyLine[i].h < min_h)
			{
				min_h = PolyLine[i].h;
				min_idx = i;
			}
		}

		// ���������� min_idx
        PolyLine[min_idx].SelectedFlag = FALSE;
		int iPrev = PolyLine[min_idx].PrevPoint;
		int iNext = PolyLine[min_idx].NextPoint;
        PolyLine[iPrev].NextPoint = iNext;
		PolyLine[iNext].PrevPoint = iPrev;

		CalculatePointH(PolyLine, nPolyNodes, PolyLine[iPrev].PrevPoint, iNext);
		CalculatePointH(PolyLine, nPolyNodes, iPrev, PolyLine[iNext].NextPoint);

		nPoints--;
	}

	FTPOINT *pOut = new FTPOINT[nNewPointsNumber];
	if(!pOut)
	{
		free(PolyLine);
		return FALSE;
	}
    
	int j;
	for(i = 0, j = 0; i < nPolyNodes; i++)
	{
		if(PolyLine[i].SelectedFlag)
		{
            assert(j < nNewPointsNumber);
			pOut[j].x = PolyLine[i].x;
			pOut[j].y = PolyLine[i].y;
			j++;
		}
	}

	DuplicateSpline(nNewPointsNumber, pOut);

	delete []pOut;
	free(PolyLine);

	return TRUE;
}

//int SplineError;

int Comparator(const void *First,const void *Second)
{
	FType Val1=((PFTPOINT)First)->x,
		  Val2=((PFTPOINT)Second)->x;
	if(Val1>=Val2)
		return 1;
	else
		return -1;
}

BOOL CCompSpline::CreateCompSpline(FTPOINT *Points,int Size,FType Period)
{
FTPOINT *PointsPointer,*PointsPointerBis;
FType **Matrix,/**Result*/Temp,/*Temp0,*/Temp1,Temp2,**CurLine,*CurItem;
int i,j,k;
FType *Result;
LPBYTE RawData;
	
	SplineError=SPL_OK;
	if(Size<2){
		SplineError=SPL_ILLEGAL_POINTS;
		return FALSE;
		}
	qsort(Points,Size,sizeof(FTPOINT),Comparator);
	
	// �������� ����� ����
	// 1. 4*Size ���������� �� ������
	// 2. 4*Size ����� �� (4*Size+1) ��������� ���� FType
	int Size1 = 4*Size*sizeof(FType *);
	int Size2 = 4*Size*(4*Size+1)*sizeof(FType);

	RawData = (LPBYTE)calloc(Size1 + Size2, 1);
	if(RawData == NULL)
	{
		SplineError=SPL_NOT_ENOUGH_MEMORY;
		return FALSE;
	}

//	Matrix=(FType **)calloc(4*Size,sizeof(*Matrix));
	Matrix=(FType **)RawData;
//	if(Matrix==(FType **)NULL){
//		SplineError=SPL_NOT_ENOUGH_MEMORY;
//		return FALSE;
//		}
	Result=(FType *)calloc(4*Size,sizeof(*Result));
	if(Result==(FType *)NULL){
//		free(Matrix);
		free(RawData);
		SplineError=SPL_NOT_ENOUGH_MEMORY;
		return FALSE;
		}
	for(i=0;i<4*Size;i++){
//		Matrix[i]=(FType *)calloc(4*Size+1,sizeof(FType));
		Matrix[i]=(FType *)(RawData + Size1 + i * (4*Size+1) * sizeof(FType));
//		if(Matrix[i]==(FType *)NULL){
//			for(j=i-1;j>=0;j--){
//				free(Matrix[j]);
//				}
//			free(Result);
//			free(Matrix);
//			SplineError=SPL_NOT_ENOUGH_MEMORY;
//			return FALSE;
//			}
		}
	PointsPointerBis=PointsPointer=Points;
	PointsPointerBis++;
	CurLine=Matrix;
	for(i=0;i<Size;i++){
		Temp=(*PointsPointer).x;
		CurItem=(*CurLine)+4*i+3;
		*CurItem--=1;
		*CurItem--=Temp;
		*CurItem--=Temp*Temp;
		*CurItem--=Temp*Temp*Temp;

		(*CurLine)[4*Size]=(*PointsPointer++).y;//Points[i].y;
		if((j=i+1)==Size){
			PointsPointerBis=Points;
			j=0;
			Temp1=Period;
			}
		else{
			Temp1=(*PointsPointerBis).x;
			}
		Temp2=Points[j].x;
		CurLine++;
		CurItem=(*CurLine)+4*i+3;
		*CurItem--=1;
		*CurItem--=Temp1;
		*CurItem--=Temp1*Temp1;
		*CurItem--=Temp1*Temp1*Temp1;
		(*CurLine)[4*Size]=(*PointsPointerBis++).y;//Points[j].y;
		CurLine++;

		CurItem=(*CurLine)+4*j+2;
		*CurItem--=1;
		*CurItem--=2*Temp2;
		*CurItem=3*Temp2*Temp2;
		CurItem=(*CurLine)+4*i+2;
		*CurItem--=-1;
		*CurItem--=-2*Temp1;
		*CurItem=-3*Temp1*Temp1;
		CurLine++;

		CurItem=(*CurLine)+4*j;
		*CurItem++=6*Temp2;
		*CurItem=2;
		CurItem=(*CurLine)+4*i;
		*CurItem++=-6*Temp1;
		*CurItem=-2;
		CurLine++;
		}
	Size*=4;
	for(i=0;i<Size;i++){
		FType Max=-1,LocTemp;
		int IndMax=-1;
		for(j=i;j<Size;j++){
			if((LocTemp=fabs(Matrix[j][i]))>Max){
				Max=LocTemp;
				IndMax=j;
				}
			}
		if(Max>EPS){
			if(IndMax!=i){
				FType *LocTemp1=Matrix[IndMax];
				Matrix[IndMax]=Matrix[i];
				Matrix[i]=LocTemp1;
				}
			}
		else{
//			for(j=0;j<Size;j++){
//				free(Matrix[j]);
//				}
			free(Result);
//			free(Matrix);
			free(RawData);
			SplineError=SPL_NO_SOLUTION;
			return FALSE;
			}
		for(j=i+1;j<Size;j++){
			if(fabs(Matrix[j][i])>EPS,1){	/////////////////////
				FType Coeff=-Matrix[j][i]/Matrix[i][i];
				for(k=i+1;k<=Size;k++){
					Matrix[j][k]+=Coeff*Matrix[i][k];
					}
				}
			}
		}
	Result[Size-1]=Matrix[Size-1][Size]/Matrix[Size-1][Size-1];
	CurLine=Matrix+Size-2;
	for(i=Size-2;i>=0;i--){
		FType Summ,*CurRes;
		CurRes=Result+Size;
		CurItem=(*CurLine)+Size;
		Summ=*CurItem--;//Matrix[i][Size];
		for(j=i+1;j<Size;j++){
			Summ-=*CurItem--**--CurRes;
			}
		*--CurRes=Summ/ *CurItem;
		CurLine--;
		}

//	for(j=0;j<Size;j++){
//		free(Matrix[j]);
//		}
//	free(Matrix);
	free(RawData);
	LastUsedCoeffs=PolyCoeffs=Result;
	CurItem=(FType *)calloc(Size/4,sizeof(FType));
	if(CurItem==(FType *)NULL){
		free(Result);
		SplineError=SPL_NOT_ENOUGH_MEMORY;
		return FALSE;
		}
	X=CurItem;
	k=Size/4;
	NumPoints=k;
	PointsPointer=Points;
	for(j=0;j<k;j++){
		*CurItem++=(*PointsPointer++).x;
		}
	LastUsedSegment=0;
	return TRUE;
}

//---------------------------------------------------------------------------

FType CCompSpline::ComputeCompSpline(FType Argument)
{
	FType *Coeffs,Result;
	int i,j,k;
	
	j = NumPoints;
	k = LastUsedSegment;
	if(Argument >= X[k])
	{
		if(k == j - 1 || Argument <= X[k+1])
		{
			Coeffs = LastUsedCoeffs;
		}
		else
		{
			while(k < j && Argument > X[k])
				k++;
			k--;
			Coeffs = PolyCoeffs + 4 * k;
			LastUsedSegment = k;
			LastUsedCoeffs = Coeffs;
		}
	}
	else
	{
		for(i = 0; i < k && Argument > X[i]; i++)
			;
		if(i > 0)
			i--;
		Coeffs = PolyCoeffs + 4 * i;
		LastUsedSegment = i;
		LastUsedCoeffs = Coeffs;
	}
	
	Result = *Coeffs++ * Argument;
	Result = (Result + *Coeffs++) * Argument;
	Result = (Result + *Coeffs++) * Argument;
	
	return Result + *Coeffs++;
}
//---------------------------------------------------------------------------
void CCompSpline::DestroyCompSpline(void)
{
	if(SplExists == FALSE) return;
	if(X != NULL) free(X);
	if(PolyCoeffs != NULL) free(PolyCoeffs);
	X = NULL;  PolyCoeffs = NULL;
	SplExists = FALSE;
}


//---------------------------------------------------------------------------

// ��������� ���������� ����� ���������
double CSpline::SplineDistance(CSpline &InSpline)
{
	const int numPhi = 90;
	const double stepPhi = (2. * M_PI) / (double)numPhi;
	
	if(!SplExists || !InSpline.SplExists)
		return DBL_MAX;

	CSpline *Spline[2] = {this, &InSpline};

	// ����� ����
	FTPOINT mc[2];
	// ������� ����� ������� � ������� �����
	int idxp[2];
	// ������� ����� ������� � ������� �����
	int idxn[2];
	// �������
	FType phip[2], phin[2];
	// �������
	FType rp[2], rn[2];

    int i = 0;
    for(i = 0; i < 2; i++)
	{
		mc[i] = Spline[i]->MassCenter();
		idxn[i] = Spline[i]->GetPrevIndex(0);
		phin[i] = Azimut(Spline[i]->FloatPolyNodes[idxn[i]] - mc[i]) - 2 * M_PI;
	}

mc[1] = mc[0];

	double phi, r[2], sum = 0., phit;
	for(int iPhi = 0; iPhi < numPhi; iPhi++)
	{
        phi = iPhi * stepPhi;

		for(i = 0; i < 2; i++)
		{
			// ���� ������ ����� � �������� > phi, ������� � phin
			// ������������, ��� ��������� phip < phi < phin
            while(phi > phin[i])
			{
				idxn[i] = Spline[i]->GetNextIndex(idxn[i]);
				idxp[i] = Spline[i]->GetPrevIndex(idxn[i]);
                phit = phin[i];
				phin[i] = Azimut(Spline[i]->FloatPolyNodes[idxn[i]] - mc[i]);
                if(phin[i] < phit)
					phin[i] += 2 * M_PI;
				phip[i] = Azimut(Spline[i]->FloatPolyNodes[idxp[i]] - mc[i]);
				rn[i] = Hypot(Spline[i]->FloatPolyNodes[idxn[i]] - mc[i]);
				rp[i] = Hypot(Spline[i]->FloatPolyNodes[idxp[i]] - mc[i]);
			}

			// ��������� phip < phi < phin
			if(phi < phip[i])
				phip[i] -= 2. * M_PI;

			r[i] = rp[i] + (rn[i] - rp[i]) * (phi - phip[i]) / (phin[i] - phip[i]);
		}

		sum += (r[1] - r[0]) * (r[1] - r[0]);
  
	}

	return sqrt(sum);
}

double CSpline::SplineDistance(int nCoordIn, double *t_param)
{
	double retval = DBL_MAX;

	FTPOINT *Points = new FTPOINT[nCoordIn];
	if(!Points)
		return retval;

	int i;
 	for(i = 0; i < nCoordIn; i++)
	{
		Points[i].x = SplX.ComputeCompSpline(t_param[i]);
		Points[i].y = SplY.ComputeCompSpline(t_param[i]);
	}

	CSpline Spline;
	Spline.CreateSpline(nCoordIn, Points, 60);

	if(Spline.SplExists)
		retval = SplineDistance(Spline);

	delete []Points;

	return retval;
}

// ��������� ����� ���� �������
FTPOINT CSpline::MassCenter()
{
	FTPOINT pt = {0., 0.};
	if(SplExists)
	{
		FType *px = &FloatPolyNodes[0].x, *py = &FloatPolyNodes[0].y;
		FType cx = 0., cy = 0.;
		for(int i = nPolyNodes; i > 0; i--, px++, py++)
		{
			cx += *px++;
			cy += *py++;
		}

		pt.x = cx / nPolyNodes;
		pt.y = cy / nPolyNodes;
	}

	return pt;
}

double CSpline::GetOneMoreApproxPoint(int nCoordIn, double *t_param, double *distance)
{
	FTPOINT *Points = new FTPOINT[nCoordIn];
	if(!Points)
		return DBL_MAX;

	int i;
 	for(i = 0; i < nCoordIn; i++)
	{
		Points[i].x = SplX.ComputeCompSpline(t_param[i]);
		Points[i].y = SplY.ComputeCompSpline(t_param[i]);
	}

	double retval = DBL_MAX;
	GetOneMoreApproxPoint(nCoordIn, Points, distance, &retval);

	delete []Points;

	return retval;
}

FTPOINT CSpline::GetOneMoreApproxPoint(int nCoordIn, FTPOINT *CoordFloatIn, double *distance, double *t_param)
{
	FTPOINT pt = {0., 0.};
	if(SplExists && nCoordIn >= 2)
	{
		const int nDiv = 100;
		
		FTPOINT *pNewPoints = new FTPOINT[nCoordIn + 1];
		if(!pNewPoints)
			return pt;
		memcpy(pNewPoints, CoordFloatIn, sizeof(FTPOINT) * nCoordIn);
		FType StepT, CurT;
		double dist, min_dist = DBL_MAX, min_t = DBL_MAX;
/*		
		{
			CSpline TestSpline;
			TestSpline.CreateSpline(nCoordIn, pNewPoints, 60);
			if(TestSpline.SplExists)
				min_dist = SplineDistance(TestSpline);
		}
*/		
		int i;
	 	for(i = 0, CurT = 0., StepT = Dist / nDiv; i < nDiv; i++, CurT += StepT)
		{
			pNewPoints[nCoordIn].x = SplX.ComputeCompSpline(CurT);
			pNewPoints[nCoordIn].y = SplY.ComputeCompSpline(CurT);
			
			CSpline TestSpline;
			TestSpline.CreateSpline(nCoordIn + 1, pNewPoints, 60);
			if(TestSpline.SplExists)
			{
				dist = SplineDistance(TestSpline);
				if(dist < min_dist)
				{
					min_dist = dist;
					min_t = CurT;
                    pt = pNewPoints[nCoordIn];
				}
			}
		}
		delete []pNewPoints;
		
		if(distance)
			*distance = min_dist;
		if(t_param)
			*t_param = min_t;
	}

	return pt;
}

int CSpline::GetLeastSignificantPointIndex(int nCoordIn, FTPOINT *CoordFloatIn, int nEceptPointIndex)
{
	if(!SplExists || nCoordIn <= 3)
		return -1;
	
	int i, iMin = -1;
	FTPOINT *Points = new FTPOINT[nCoordIn - 1];
	if(!Points)
		return -1;
	memcpy(Points, CoordFloatIn, sizeof(FTPOINT) * (nCoordIn - 1));
	FTPOINT *pIn = CoordFloatIn + nCoordIn - 1;
	FTPOINT *pOut = Points + nCoordIn - 2;
	double dist, min_dist = DBL_MAX;
	for(i = 0; i < nCoordIn; i++)
	{
		CSpline TestSpline;
        TestSpline.CreateSpline(nCoordIn - 1, Points, 60);
		if(TestSpline.SplExists)
		{
			dist = SplineDistance(TestSpline);
			if(dist < min_dist && (nEceptPointIndex == -1 || (nEceptPointIndex != nCoordIn - 1 - i)))
			{
				min_dist = dist;
                iMin = nCoordIn - 1 - i;
			}
		}
		if(i != nCoordIn - 1)
			*pOut-- = *pIn--;	
	}
	delete []Points;

	return iMin;
}

#if defined(_DEBUG) && defined(DRAW_SPLINE)
void DrawNumber(double n)
{
	extern CWnd *_picWnd;
	CString s;
	s.Format("%f", n);
	CClientDC dc(_picWnd);
	dc.ExtTextOut(0, 0, ETO_OPAQUE, NULL, s, NULL);
}

void WaitUserInput()
{
	MSG msg;
	do
	{
		::PeekMessage(&msg, 0, 0, 0, PM_REMOVE);
	   	::TranslateMessage(&msg);
		::DispatchMessage(&msg);
	}
	while(msg.message != WM_CHAR);
}

void DrawSpline(CSpline &TargetSpline, BOOL bWait = TRUE)
{
	extern CWnd *_picWnd;
	extern int y_up[2];
	extern short dxField[2], dyField[2];
	int GetPicDy (int picDy, int Yup);
	void ConvertPicToWnd (CPoint * WndPoint, CPoint PicPoint, CSize picSize);

//	_picWnd->Invalidate();
//	_picWnd->UpdateWindow();

	CClientDC dc(_picWnd);
	int i;
	CPen pen(PS_SOLID, 1, 0x00ffffff), *oldpen, 
			pents(PS_SOLID, 1, 0x000000ff);
	oldpen = dc.SelectObject(&pen);

	CSize picSize(dxField[1], GetPicDy(dyField[1], y_up[1]));

//TargetSpline
	{
		dc.SelectObject(&pents);
		for(i = 0; i < TargetSpline.nPolyNodes; i++)
		{
			CPoint ptIn(TargetSpline.PolyNodes[i]), ptOut;
//			ConvertPicToWnd(&ptOut, ptIn, picSize);
			ptOut = ptIn;
			if(i == 0)
				dc.MoveTo(ptOut.x, ptOut.y);
			else
				dc.LineTo(ptOut.x, ptOut.y);
		}
		CPoint ptIn(TargetSpline.PolyNodes[0]), ptOut;
//		ConvertPicToWnd(&ptOut, ptIn, picSize);
		ptOut = ptIn;
		dc.LineTo(ptOut.x, ptOut.y);
	}
//

	dc.SelectObject(oldpen);

	if(bWait)
		WaitUserInput();
}

void DrawSpline(CSpline &TargetSpline, int nCoord, FTPOINT *CoordFloat, int ind, BOOL bWait = TRUE)
{
	extern CWnd *_picWnd;
	extern int y_up[2];
	extern short dxField[2], dyField[2];
	int GetPicDy (int picDy, int Yup);
	void ConvertPicToWnd (CPoint * WndPoint, CPoint PicPoint, CSize picSize);

	CSpline Spline;
	Spline.CreateSpline(nCoord, CoordFloat);
	if(!Spline.SplExists)
		return;

	_picWnd->Invalidate();
	_picWnd->UpdateWindow();

	CClientDC dc(_picWnd);
	int i;
	CPen pen(PS_SOLID, 1, 0x00ffffff), *oldpen, 
			pen2(PS_SOLID, 1, 0x0000ffff),
			pents(PS_SOLID, 1, 0x000000ff);
	oldpen = dc.SelectObject(&pen);

	CSize picSize(dxField[1], GetPicDy(dyField[1], y_up[1]));

//TargetSpline
	{
		dc.SelectObject(&pents);
		for(i = 0; i < TargetSpline.nPolyNodes; i++)
		{
			CPoint ptIn(TargetSpline.PolyNodes[i]), ptOut;
			ConvertPicToWnd(&ptOut, ptIn, picSize);
			if(i == 0)
				dc.MoveTo(ptOut.x, ptOut.y);
			else
				dc.LineTo(ptOut.x, ptOut.y);
		}
		CPoint ptIn(TargetSpline.PolyNodes[0]), ptOut;
		ConvertPicToWnd(&ptOut, ptIn, picSize);
		dc.LineTo(ptOut.x, ptOut.y);
	}
//
	dc.SelectObject(&pen);
	for(i = 0; i < Spline.nPolyNodes; i++)
	{
		CPoint ptIn(Spline.PolyNodes[i]), ptOut;
		ConvertPicToWnd(&ptOut, ptIn, picSize);
		if(i == 0)
			dc.MoveTo(ptOut.x, ptOut.y);
		else
			dc.LineTo(ptOut.x, ptOut.y);
	}
	CPoint ptIn(Spline.PolyNodes[0]), ptOut;
	ConvertPicToWnd(&ptOut, ptIn, picSize);
	dc.LineTo(ptOut.x, ptOut.y);

	for(i = 0; i < nCoord; i++)
	{
		if(i == ind)
			dc.SelectObject(&pen2);
		else
			dc.SelectObject(&pen);
		CPoint ptIn, ptOut;
		ptIn.x = Round(CoordFloat[i].x);
		ptIn.y = Round(CoordFloat[i].y);
		ConvertPicToWnd(&ptOut, ptIn, picSize);
        dc.MoveTo(ptOut.x - 5, ptOut.y);
        dc.LineTo(ptOut.x + 5, ptOut.y);
        dc.MoveTo(ptOut.x, ptOut.y - 5);
        dc.LineTo(ptOut.x, ptOut.y + 5);
	}
	
	dc.SelectObject(oldpen);

	if(bWait)
		WaitUserInput();
}

void DrawSpline(CSpline &TargetSpline, int nCoord, double *t_param, int ind, BOOL bWait)
{
	FTPOINT *Points = new FTPOINT[nCoord];
	if(!Points)
		return;

	int i;
 	for(i = 0; i < nCoord; i++)
	{
		Points[i].x = TargetSpline.SplX.ComputeCompSpline(t_param[i]);
		Points[i].y = TargetSpline.SplY.ComputeCompSpline(t_param[i]);
	}

	DrawSpline(TargetSpline, nCoord, Points, ind, bWait);

	delete []Points;
}
#else
void DrawNumber(double n);
void WaitUserInput();
void DrawSpline(CSpline &TargetSpline, int nCoord, FTPOINT *CoordFloat, int ind, BOOL bWait = TRUE);
void DrawSpline(CSpline &TargetSpline, int nCoord, double *t_param, int ind, BOOL bWait);
#endif

//void CSpline::CreateSplineAdv(int nCoord, FTPOINT *CoordFloat, int nOut)
//{
//	// ������� �������� ������, ������� ����� ����������
//	CSpline TargetSpline;
//	TargetSpline.CreateSpline(nCoord, CoordFloat, 60);
//	if(!TargetSpline.SplExists)
//		return;
//
//	int i;
//	int iFix, j, k;
//	FType StepT, CurT;
//
//	FTPOINT *Points = new FTPOINT[nOut];
//	if(!Points)
//		return;
//	FTPOINT *PointsAux = new FTPOINT[nOut - 1];
//	if(!PointsAux)
//		return;
//	
//	// �������� ����������� �����
//	const int nNumInitPoints = 2;
//
//	int nPoints = nNumInitPoints;
// 	for(i = 0, CurT = 0., StepT = TargetSpline.Dist / (double)nNumInitPoints; i < nNumInitPoints; i++, CurT += StepT)
//	{
//		Points[i].x = TargetSpline.SplX.ComputeCompSpline(CurT);
//		Points[i].y = TargetSpline.SplY.ComputeCompSpline(CurT);
//	}
//
//	// ��������� ��� ������ ��������� � ������� 3-2-1
//	double distance = 0.;
//	
//	do
//	{
//		for(i = 0; i < 5; i++)
//		{
//			for(iFix = nPoints; iFix >= 0; iFix--)
//			{
//				for(j = 0, k = 0; j <= nPoints; j++)
//				{
//					if(j != iFix)
//						PointsAux[k++] = Points[j];
//				}
//				Points[iFix] = TargetSpline.GetOneMoreApproxPoint(nPoints, PointsAux, &distance);
//			}
//
//			DrawSpline(TargetSpline, nPoints + 1, Points, -1, FALSE);
//			DrawNumber(distance);
//			WaitUserInput();
//
//		}
//	}
//	while(++nPoints < nOut);
//
///*
//	nPoints = 3;
//
//	while(nPoints < nOut)
//	{
//		Points[nPoints] = TargetSpline.GetOneMoreApproxPoint(nPoints, Points);
//		nPoints++;
//		DrawSpline(TargetSpline, nPoints, Points, nPoints - 1);
//
//		Points[nPoints] = TargetSpline.GetOneMoreApproxPoint(nPoints, Points);
//		nPoints++;
//		DrawSpline(TargetSpline, nPoints, Points, nPoints - 1);
//
//		iMin = TargetSpline.GetLeastSignificantPointIndex(nPoints, Points);
//		memmove(Points + iMin, Points + iMin + 1, sizeof(FTPOINT) * (nPoints - iMin - 1));
//		nPoints--;
//		DrawSpline(TargetSpline, nPoints, Points, -1);
//	}
//*/
//	delete []PointsAux;
//	delete []Points;
//
//	return;
//}

#define CheckT(t) if(t<0.)t+=Dist;else if(t>Dist)t-=Dist;

double CSpline::CalcTDist(double t0, double t1, double T, double direction)
{
    double t = (t1 - t0) * _copysign(1., direction);
	if(t < 0)
		t += T;
	return t;
}

void CSpline::AdjustPointsForMinDistance(int nCoordIn, double *t_param, double *distance)
{ 
	if(!SplExists || nCoordIn < 3)
		return;
	
	const int nDiv = 400;
	const FType StepT = Dist / nDiv;
	const FType delta_t = Dist / nCoordIn / 2.;

	int i, nc = 0;
	bool bCorr;

	double f, f1, f2, CurStep, t_limit;
	
	do
	{
		nc = 0;
		for(i = 0; i < nCoordIn; i++)
		{
			bCorr = false;
			// �������� �������� �-��� ����� � ������ �� t[i]
			f = SplineDistance(nCoordIn, t_param);
			t_param[i] -= StepT;
			CheckT(t_param[i]);
			f1 = SplineDistance(nCoordIn, t_param);
			t_param[i] += 2 * StepT;
			CheckT(t_param[i]);
			f2 = SplineDistance(nCoordIn, t_param);
			t_param[i] -= StepT;
			CheckT(t_param[i]);
			CurStep = _copysign(StepT, f1 - f);
			if(CurStep > 0.)
			{
				f1 = f2;

				t_limit = t_param[(i + 1) % nCoordIn];
			}
			else
			{
                t_limit = t_param[(i - 1 + nCoordIn) % nCoordIn];
			}			

			// f - ���������� �������� �� t_param[i]
			// f1 - ��������� ��������
			if(f1 < f)
			{
				t_param[i] += CurStep;
				CheckT(t_param[i]);
				if(CalcTDist(t_param[i], t_limit, Dist, CurStep) < delta_t)
				{
					t_param[i] -= CurStep;
					CheckT(t_param[i]);
					continue;
				}
			}
			while(f1 < f)
			{
				bCorr = true;
				f = f1;
				t_param[i] += CurStep;
				CheckT(t_param[i]);
				if(CalcTDist(t_param[i], t_limit, Dist, CurStep) < delta_t)
					break;
				f1 = SplineDistance(nCoordIn, t_param);
			}
			if(bCorr)
			{
				t_param[i] -= CurStep;
				CheckT(t_param[i]);
				nc++;
			}
			
//			DrawSpline(*this, nCoordIn, t_param, i, FALSE);
//			DrawNumber(f);
//			WaitUserInput();

		}
	}
	while(nc > 0);

	if(distance)
		*distance = f;
}

void CSpline::CreateSplineAdv(int nCoord, FTPOINT *CoordFloat, int nOut)
{
	// ������� �������� ������, ������� ����� ����������
//	CSpline TargetSpline;
	CreateSpline(nCoord, CoordFloat, 60);
	if(!SplExists)
		return;

	int i;
	FType StepT, CurT;

	double *t_param = new double[nOut];
	if(!t_param)
		return;

	double distance = 0., min_dist = DBL_MAX, phase = 0., min_phase;

//	for(phase = 0.; phase < Dist / (double)nOut; phase += Dist / 100.)
//	{
		for(i = 0, CurT = phase, StepT = Dist / (double)nOut; i < nOut; i++, CurT += StepT)
			t_param[i] = CurT;
		AdjustPointsForMinDistance(nOut, t_param, &distance);
		if(distance < min_dist)
		{
			min_dist = distance;
			min_phase = phase;
		}

//		DrawSpline(*this, nOut, t_param, -1, FALSE);
//		DrawNumber(distance);
//		WaitUserInput();

//	}

	SplExists = FALSE;
	nPolyNodes = nOut;

	if(PolyNodes)
		delete []PolyNodes;
	PolyNodes = (nPolyNodes >= 3) ? (new POINT[nOut + 1]) : NULL;

	if(FloatPolyNodes)
		delete []FloatPolyNodes;
	FloatPolyNodes = (nPolyNodes >= 3) ? (new CFTPoint[nOut + 1]) : NULL;

	if(!PolyNodes || !FloatPolyNodes)
	{
		if(PolyNodes)
		{
			delete []PolyNodes;
			PolyNodes = NULL;
		}
		if(FloatPolyNodes)
		{
			delete []FloatPolyNodes;
			FloatPolyNodes = NULL;
		}
		SplX.DestroyCompSpline();
		SplY.DestroyCompSpline();
	}
	else
	{
		FType StepT, CurT;

		for(i = 0, CurT = 0.f, StepT = Dist / nPolyNodes; i < nPolyNodes; i++, CurT += StepT)
		{
			FloatPolyNodes[i].x = SplX.ComputeCompSpline(t_param[i]);
			FloatPolyNodes[i].y = SplY.ComputeCompSpline(t_param[i]);
			PolyNodes[i].x = Round(FloatPolyNodes[i].x);
			PolyNodes[i].y = Round(FloatPolyNodes[i].y);

			if(i == 0)
			{
				FloatPolyNodes[nPolyNodes] = FloatPolyNodes[0];
				PolyNodes[nPolyNodes] = PolyNodes[0];
			}
		}

		SplExists = TRUE;
	}

/*
	int nPoints = 3;
	double distance = 0.;
	
	do
	{
	 	for(i = 0, CurT = 0., StepT = TargetSpline.Dist / (double)nPoints; i < nPoints; i++, CurT += StepT)
			t_param[i] = CurT;

		TargetSpline.AdjustPointsForMinDistance(nPoints, t_param, &distance);

		DrawSpline(TargetSpline, nPoints, t_param, -1, FALSE);
		DrawNumber(distance);
		WaitUserInput();

		nPoints++;
	}
	while(nPoints <= nOut);
*/	
/*
	// �������� ����������� �����
	const int nNumInitPoints = 2;

	int nPoints = nNumInitPoints;
 	for(i = 0, CurT = 0., StepT = TargetSpline.Dist / (double)nNumInitPoints; i < nNumInitPoints; i++, CurT += StepT)
		t_param[i] = CurT;

	double distance = 0.;
	
	do
	{
		t_param[nPoints] = TargetSpline.GetOneMoreApproxPoint(nPoints, t_param, &distance);
		nPoints++;

		DrawSpline(TargetSpline, nPoints, t_param, -1, FALSE);
		DrawNumber(distance);
		WaitUserInput();

		TargetSpline.AdjustPointsForMinDistance(nPoints, t_param, &distance);

		DrawSpline(TargetSpline, nPoints, t_param, -1, FALSE);
		DrawNumber(distance);
		WaitUserInput();
	}
	while(nPoints < nOut);
*/
	delete []t_param;

	return;
}

void CSpline::ProjectPointToSpline(FTPOINT *Point)
{
	if(!SplExists)
		return;

	int i;
	FTPOINT mc = MassCenter();
/*		
	FTPOINT segmc, min_point;
	double d, min_dist = DBL_MAX;

	for(i = 0; i < nPolyNodes; i++)
	{
		segmc.x = (FloatPolyNodes[i].x + FloatPolyNodes[i+1].x) * 0.5;
		segmc.y = (FloatPolyNodes[i].y + FloatPolyNodes[i+1].y) * 0.5;
        d = Hypot(segmc, *Point);
		if(d < min_dist)
		{
			min_dist = d;
            min_point = segmc;            			
		}
	}

	*Point = min_point;
*/
	// ������ �������, ������� ���������� ��� �� ������ ���� �� Point
	// ��������� ������ (����) a1 * x + b1 * y + c1 = 0
	FType a1 = - (Point->y - mc.y);
	FType b1 = Point->x - mc.x;
	FType c1 = - (a1 * mc.x + b1 * mc.y);
        
	FType a2, b2, c2, D, dot_prod;
	FTPOINT cross_point;
	for(i = 0; i < nPolyNodes; i++)
	{
        // ��������� ������� a2 * x + b2 * y + c2 = 0
        a2 = - (FloatPolyNodes[i+1].y - FloatPolyNodes[i].y);
		b2 = FloatPolyNodes[i+1].x - FloatPolyNodes[i].x;
		c2 = - (a2 * FloatPolyNodes[i].x + b2 * FloatPolyNodes[i].y);

		D = a1 * b2 - a2 * b1;
		if(fabs(D) < EPS)
			continue; // �����������

        cross_point.x = (b1 * c2 - b2 * c1) / D;
		cross_point.y = (c1 * a2 - c2 * a1) / D;

		// 1. ����� ������ ������ ������ ������� (��������� ������������ �������� < 0)
        dot_prod = (FloatPolyNodes[i].x - cross_point.x) * (FloatPolyNodes[i+1].x - cross_point.x) +
						(FloatPolyNodes[i].y - cross_point.y) * (FloatPolyNodes[i+1].y - cross_point.y);

		if(dot_prod > EPS)
			continue;

		// 2. ����� ������ ������ �� ����������� ������� �� ������ ���� � ����� �����
		dot_prod = (Point->x - mc.x) * (cross_point.x - mc.x) +
						(Point->y - mc.y) * (cross_point.y - mc.y);

        if(dot_prod > EPS)
			break;
	}
//	ASSERT(i < nPolyNodes);
	if(i == nPolyNodes)
		return;

	*Point = cross_point;
}

void CSpline::GetBoundRect(FTRECT *rect)
{
	if(!SplExists)
		return;

	rect->left = rect->top = DBL_MAX;
    rect->right = rect->bottom = -DBL_MAX;

	FType x, y;

	for(int i = 0; i < nPolyNodes; i++)
	{
		x = FloatPolyNodes[i].x;
		y = FloatPolyNodes[i].y;

        if(x < rect->left)
			rect->left = x;
		if(x > rect->right)
			rect->right = x;
		if(y < rect->top)
			rect->top = y;
		if(y > rect->bottom)
			rect->bottom = y;
	}
}

#define noPRINTMATRIX

#if defined(PRINTMATRIX) && defined(_DEBUG)
static void PrintMatrix(CIntMatrix &argument, LPCTSTR string)
{
//	return;

	static COLORREF ColArray[] = 
	{ 
		//  R,		G,		B
		RGB(0,		0,		0),		// 0
		RGB(127,	127,	127),	// 1
		RGB(0,		255,	0),		// 2
		RGB(255,	0,		0),		// 3
		RGB(255,	255,	255),	// 4
		RGB(0,		0,		255),	// 5
		RGB(255,	255,	255),	// 6
		RGB(255,	255,	255),	// 7
 		RGB(255,	255,	255),	// 8
		RGB(255,	255,	127),	// 9
		RGB(31,	    127,	127),	// 10
		RGB(63,		127,	127),	// 11
		RGB(95,		127,	127),	// 12
		RGB(127,	127,	127),	// 13
		RGB(159,	127,	127),	// 14
		RGB(191,	127,	127),	// 15
		RGB(223,	127,	127),	// 16
		RGB(255,	127,	127),	// 17
	};

	COLORREF cf;
	CColorMatrix32 matrix, matrixlarge;
	RGBQUAD rgb;
	rgb.rgbReserved = 0;
	matrix.Create(argument.GetY(),argument.GetX(),TRUE);
//	int YYY;
	RECT rcMatrix = argument.GetRect();
	for(int y=rcMatrix.top;y<=rcMatrix.bottom;y++)
		for(int x=rcMatrix.left;x<=rcMatrix.right;x++)
		{
/*
			YYY = ((argument[y][x] & 8) > 0);
			rgb.rgbRed = (((argument[y][x] & 1)>0)+YYY)*127;
			rgb.rgbGreen = (((argument[y][x] & 2)>0)+YYY)*127;
			rgb.rgbBlue = (((argument[y][x] & 4)>0)+YYY)*127;
*/
			if(argument[y][x] >= 50 && argument[y][x] <= 230)
			{
                int phi = argument[y][x] - 50;
//				cf = 0x00808080 + phi * (0x00ffffff - 0x808080) / 180;
//				rgb.rgbRed = GetRValue(cf);
//				rgb.rgbGreen = GetGValue(cf);
//				rgb.rgbBlue = GetBValue(cf);
				rgb.rgbRed =
				rgb.rgbGreen =
				rgb.rgbBlue = Round(0x80 + phi * (0xff - 0x80) / 180.);
			}
			else if(argument[y][x] < 9/*sizeof(ColArray)/sizeof(ColArray[0])*/)
			{
				cf = ColArray[argument[y][x]];
				rgb.rgbRed = GetRValue(cf);
				rgb.rgbGreen = GetGValue(cf);
				rgb.rgbBlue = GetBValue(cf);
			}
			else
			{
				rgb.rgbRed =
				rgb.rgbGreen =
				rgb.rgbBlue = 255;
			}
			matrix[y-rcMatrix.top][x-rcMatrix.left] = rgb;
		}
	extern CWnd *_picWnd;
	CClientDC dc(_picWnd);
//	RescaleBitmap32(matrix, CSize(matrix.GetX() * 2, matrix.GetY() * 2), matrixlarge);
/*
	matrixlarge.Create(matrix.GetY() * 2, matrix.GetX() * 2);
    for(y = 0; y < matrix.GetY(); y++)
		for(int x = 0; x < matrix.GetX(); x++)
		{
            matrixlarge[2*y+0][2*x+0] =
			matrixlarge[2*y+0][2*x+1] =
			matrixlarge[2*y+1][2*x+0] =
			matrixlarge[2*y+1][2*x+1] = matrix[y][x];
		}


	DrawColorMatrixInDC(&dc,matrixlarge,0,0);
*/
	DrawColorMatrixInDC(&dc,matrix,0,0);
	AfxMessageBox(string);
}
#else
static void PrintMatrix(CIntMatrix &argument, LPCTSTR string){}
#endif

BOOL CSpline::PtInSpline(FTPOINT point, double kx, double ky)
{
	if(!SplExists)
		return FALSE;

	FTRECT ftrcBound;
	GetBoundRect(&ftrcBound);

	if(point.x < ftrcBound.left || point.x > ftrcBound.right ||
					point.y < ftrcBound.top || point.y > ftrcBound.bottom)
		return FALSE;

	double OffsetX, OffsetY;

	ftrcBound.right -= (OffsetX = ftrcBound.left);
	ftrcBound.bottom -= (OffsetY = ftrcBound.top);
    ftrcBound.left = ftrcBound.top = 0.;

	ftrcBound.right *= kx;
	ftrcBound.bottom *= ky;

    RECT rcBound;
	rcBound.left = 0;
	rcBound.top = 0;
	rcBound.right = (int)(ftrcBound.right + 1.);
	rcBound.bottom = (int)(ftrcBound.bottom + 1.);;

	CIntMatrix matrix;
    if(!matrix.Create(&rcBound, TRUE))
		return FALSE;

	int i, x1, y1, x2, y2, pattern = 1;
	for(i = 0; i < nPolyNodes; i++)
	{
		x1 = Round((FloatPolyNodes[i].x - OffsetX) * kx);
		y1 = Round((FloatPolyNodes[i].y - OffsetY) * ky);
		x2 = Round((FloatPolyNodes[i + 1].x - OffsetX) * kx);
		y2 = Round((FloatPolyNodes[i + 1].y - OffsetY) * ky);

        matrix.Line(x1, y1, x2, y2, pattern, FALSE); 
	}

	FTPOINT mc = MassCenter();
	int fill_pattern = 0, filler = 1;
	matrix.Fill_While(Round((mc.x - OffsetX) * kx), Round((mc.y - OffsetY) * ky), fill_pattern, filler);

	return (matrix[Round((point.y - OffsetY) * ky)][Round((point.x - OffsetX) * kx)] != 0);
}
