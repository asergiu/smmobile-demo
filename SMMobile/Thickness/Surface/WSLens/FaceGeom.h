#pragma once

#include "RayTracer.h"

class CFaceGeom
{
public:

	BOOL FindIntersectionPoint(CLine3D &Line, CPoint3D &Point);

protected:
	static CPoint3D ptUpFacePlane1, ptUpFacePlane2;
    static CPlane BrowPlane;
    static CPlane FacePlane;
};
