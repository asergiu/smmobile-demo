#include "stdafx.h"
#include "CONTOUR2ILens.h"
#include <math.h>

const double PD = 67.2;
const long LensDiameter = 70;
const double xFrame1[] = {11.071, 12.744, 14.289, 16.349, 19.567, 24.845, 35.273, 45.829, 52.652, 54.455, 56.128, 59.282, 61.535, 62.565, 63.466, 64.509, 64.496, 64.496, 64.496, 64.110, 63.209, 61.664, 59.797, 58.059, 56.901, 54.583, 52.137, 47.374, 41.324, 33.986, 31.282, 27.459, 24.717, 22.271, 21.112, 20.082, 19.052, 17.508, 15.963, 13.774, 11.200,  9.011,  8.625,  8.496,  8.882,  9.912};
const double yFrame1[] = {33.278, 35.080, 36.239, 37.011, 37.655, 38.170, 38.620, 38.298, 37.397, 36.882, 35.981, 33.793, 31.733, 30.445, 28.772, 26.197, 22.464, 18.731, 14.997, 11.779,  9.462,  6.629,  4.055,  2.124,  1.609,  1.094,  0.708,  0.321,  0.000,  0.064,  0.193,  0.579,  1.094,  1.737,  2.252,  3.025,  3.926,  5.857,  8.174, 12.294, 17.958, 24.137, 25.940, 28.128, 30.124, 31.862};
const double xFrame2[] = {12.943, 14.271, 15.879, 17.819, 20.032, 22.415, 24.947, 27.506, 30.010, 32.718, 35.524, 38.082, 40.497, 43.095, 45.746, 48.211, 50.577, 52.728, 54.619, 56.225, 57.468, 58.434, 59.012, 59.215, 59.031, 58.323, 57.044, 55.137, 52.954, 50.624, 48.146, 45.709, 43.238, 40.687, 37.962, 35.074, 32.369, 29.893, 27.337, 24.783, 22.294, 19.883, 17.598, 15.407, 13.474, 12.155, 11.547, 11.276, 11.340, 11.932};
const double yFrame2[] = {11.750,  9.480,  7.355,  5.490,  3.938,  2.658,  1.692,  0.984,  0.476,  0.132,  0.000,  0.079,  0.324,  0.820,  1.638,  2.652,  4.001,  5.723,  7.615,  9.704, 11.933, 14.414, 17.202, 19.939, 22.580, 25.134, 27.454, 29.497, 31.130, 32.438, 33.485, 34.191, 34.745, 35.140, 35.274, 35.327, 35.340, 35.159, 34.678, 33.968, 33.104, 32.020, 30.600, 28.906, 26.912, 24.608, 22.172, 19.566, 16.847, 14.223};
const double xFrame3[] = {10.358, 11.221, 12.258, 13.719, 15.724, 18.259, 21.185, 24.255, 27.123, 29.982, 32.950, 35.693, 38.344, 41.160, 43.978, 46.791, 49.837, 52.989, 55.816, 57.852, 59.069, 59.689, 59.968, 60.100, 60.197, 60.256, 60.130, 59.451, 57.747, 54.909, 51.504, 48.236, 45.344, 42.607, 39.837, 37.177, 34.559, 31.887, 29.069, 26.130, 23.165, 20.102, 16.975, 13.939, 11.371,  9.651,  8.808,  8.647,  8.979,  9.591};
const double yFrame3[] = {11.845,  8.947,  6.230,  3.810,  2.120,  1.255,  0.806,  0.580,  0.440,  0.240,  0.065,  0.022,  0.009,  0.032,  0.280,  0.531,  0.704,  1.062,  1.755,  3.213,  5.700,  8.581, 11.423, 14.269, 17.031, 19.964, 23.022, 25.581, 27.372, 28.470, 29.029, 29.395, 29.631, 29.753, 29.883, 30.012, 30.089, 30.035, 29.955, 29.836, 29.677, 29.619, 29.454, 28.924, 27.806, 25.882, 23.191, 20.163, 17.351, 14.687};
const double xFrame4[] = { 8.500,  8.762,  9.023, 10.331, 11.638, 13.208, 14.254, 15.823, 17.654, 19.485, 21.838, 24.715, 28.377, 33.608, 40.669, 50.085, 55.838, 60.285, 63.946, 65.777, 66.562, 68.131, 68.654, 69.700, 70.223, 70.485, 70.223, 69.700, 69.177, 68.915, 68.654, 67.869, 66.823, 66.038, 64.992, 63.685, 61.331, 58.192, 52.700, 45.377, 37.792, 32.300, 29.423, 24.454, 20.531, 17.392, 15.823, 14.515, 13.208, 12.162, 11.115, 10.331,  9.546,  9.023,  9.023};
const double yFrame4[] = {23.277, 26.677, 27.462, 28.769, 29.815, 30.077, 30.600, 31.123, 31.385, 31.908, 32.169, 32.692, 33.215, 33.738, 34.000, 33.738, 33.215, 32.692, 32.169, 31.908, 31.385, 30.600, 30.077, 28.769, 27.200, 21.708, 15.431, 12.292, 10.200,  8.892,  7.323,  5.492,  3.923,  2.877,  2.092,  1.831,  1.308,  0.785,  0.262,  0.000,  0.262,  0.785,  1.308,  2.092,  2.877,  3.923,  4.708,  5.754,  7.062,  8.631, 11.246, 13.338, 15.954, 17.523, 19.615};

double calc_center(const double * Points, int nPoint)
{
	int i;
	double minPoint = Points[0];
	double maxPoint = Points[0];
	for(i = 1; i < nPoint; i++) {
		if(Points[i] < minPoint)
			minPoint = Points[i];
		else if(Points[i] > maxPoint)
			maxPoint = Points[i];
	}
	return (maxPoint - minPoint) / 2.;
}

BOOL CONTOUR2ILens(LENSE *pLens, ILens *pLensRight, ILens *pLensLeft, int ContourForm, int ContourScale)
{
	if(!pLens || !pLensRight || !pLensLeft)
		return FALSE;

	IContour * Contour = Create_IContour();
	int iPoint;
	double xCenterRight = pLens->Rc_real_Right;
	double xCenterLeft = pLens->Rc_real_Left;
	double yCenter = 0.;
	if(Contour) {
		int Count = 0;
		int iLeftPoint = 0;
//		double ScaleFactor = 1.33;
		double ScaleFactor = 1.12;
		const double * xFrame, * yFrame = NULL;
		switch(ContourForm) {
			case 0:	// Default
				Count = sizeof(xFrame1) / sizeof(double);
				xFrame = xFrame1;
				yFrame = yFrame1;
				break;
			case 1:	// Round
				Count = sizeof(xFrame2) / sizeof(double);
				xFrame = xFrame2;
				yFrame = yFrame2;
				break;
			case 2:	// Rectangular
				Count = sizeof(xFrame3) / sizeof(double);
				xFrame = xFrame3;
				yFrame = yFrame3;
				break;
			case 3:	// New
				Count = sizeof(xFrame4) / sizeof(double);
				xFrame = xFrame4;
				yFrame = yFrame4;
				break;
		}

		for(iPoint = 0; iPoint < Count; iPoint++){
			if(xFrame[iPoint] < xFrame[iLeftPoint])
				iLeftPoint = iPoint;
		}
		for(iPoint = 0; iPoint < Count; iPoint++){
			double x = 0., y = yFrame[iPoint];
			if(ContourScale == 1)
				x = xFrame[iLeftPoint] + (xFrame[iPoint] - xFrame[iLeftPoint]) * ScaleFactor;
			else if(ContourScale == 2)
				x = xFrame[iLeftPoint] + (xFrame[iPoint] - xFrame[iLeftPoint]) / ScaleFactor;
			else
				x = xFrame[iPoint];
			Contour->Add(x, y)->Release();
		}
		yCenter = calc_center(yFrame, Count);

		if(Contour->GetCount() > 3) {
			double minDiam = 0.;
			if(pLensRight) {
				pLensRight->SetCenterX(xCenterRight);
				pLensRight->SetCenterY(yCenter);
				pLensRight->SetDiameter(LensDiameter);
				pLensRight->SetContour(Contour);
				minDiam = pLensRight->GetMinDiam();
				if(minDiam > LensDiameter)
					pLensRight->SetDiameter(ceil(minDiam));
			}
			if(pLensLeft) {
				pLensLeft->SetCenterX(xCenterLeft);
				pLensLeft->SetCenterY(yCenter);
				pLensLeft->SetDiameter(LensDiameter);
				pLensLeft->SetContour(Contour);
				minDiam = pLensLeft->GetMinDiam();
				if(minDiam > LensDiameter)
					pLensLeft->SetDiameter(ceil(minDiam));
			}
		}
		Contour->Release();
		Contour = NULL;
	}

	return TRUE;
}
