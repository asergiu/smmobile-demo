#pragma once
// Coating.h : header file
//

#include "ObjectBase.h"
#include <string>
using std::string;

/////////////////////////////////////////////////////////////////////////////
// CCoating

class CCoating :
	public CObjectEventBase<ICoating, ICoatingEvents>
{
public:
    static CCoating* CreateObject();
	ICoating * GetInterface();

protected:
	CCoating();           // protected constructor used by dynamic creation

// Attributes
	string m_Name, m_EDPCode, m_Info;

public:

// Operations
public:
	string GetName();
	bool SetName(LPCTSTR newVal);
	string GetEDPCode();
	bool SetEDPCode(LPCTSTR newVal);
	string GetInfo();
	bool SetInfo(LPCTSTR newVal);
	ICoating * Clone();

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);

protected:
	virtual ~CCoating();

	virtual void FinalRelease()
	{
		delete this;
	}

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->CoatingChanged();
			}
		}
	}
};
