// Coating.cpp : implementation file
//

#include "stdafx.h"
#include "Coating.h"

/////////////////////////////////////////////////////////////////////////////
// CCoating

CCoating* CCoating::CreateObject()
{
	CCoating *pObject = new CCoating;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

ICoating * CCoating::GetInterface()
{
	return static_cast<ICoating*>(this);
}

CCoating::CCoating()
{
	m_Name = "";
	m_EDPCode = "";
	m_Info = "";
}

CCoating::~CCoating()
{
}

string CCoating::GetName() 
{
	return m_Name;
}

bool CCoating::SetName(LPCTSTR lpszNewValue) 
{
	m_Name = lpszNewValue;
	SetModifiedFlag();
	FireChange();
	return true;
}

string CCoating::GetEDPCode() 
{
	return m_EDPCode;
}

bool CCoating::SetEDPCode(LPCTSTR lpszNewValue) 
{
	m_EDPCode = lpszNewValue;
	SetModifiedFlag();
	FireChange();
	return true;
}

string CCoating::GetInfo() 
{
	return m_Info;
}

bool CCoating::SetInfo(LPCTSTR lpszNewValue) 
{
	m_Info = lpszNewValue;
	SetModifiedFlag();
	FireChange();
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// Persistence

bool CCoating::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	if(pPX->PX_String("Name", m_Name, "") == false)
		return false;
	if(pPX->PX_String("EDPCode", m_EDPCode, "") == false)
		return false;
	if(pPX->PX_String("Info", m_Info, "") == false)
		return false;

	return true;
}

ICoating * CCoating::Clone() 
{
	CCoating *pNewObject = CCoating::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}
