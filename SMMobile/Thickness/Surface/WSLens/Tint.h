#pragma once
// Tint.h : header file
//

#include "ObjectBase.h"
#include <vector>
using std::vector;

const unsigned int Max_Count = 200;

/////////////////////////////////////////////////////////////////////////////
// CTint

class CTint :
	public CObjectEventBase<ITint, ITintEvents>,
	public CObjEventSimpleImpl<ITintPoint, ITintPointEvents>
{
public:
    static CTint* CreateObject();
	ITint * GetInterface();

protected:
	CTint();           // protected constructor used by dynamic creation

// Attributes
	string m_Name, m_EDPCode, m_Info;
	typedef vector< CObjPtr<ITintPoint> > VTintPoint;
	VTintPoint pt;

public:

// Operations
public:
	string GetName();
	bool SetName(LPCTSTR);
	string GetEDPCode();
	bool SetEDPCode(LPCTSTR);
	string GetInfo();
	bool SetInfo(LPCTSTR);
	unsigned long GetCount();
	ITintPoint * GetItem(unsigned long Index);
	bool SetItem(unsigned long Index, ITintPoint * newValue);
	bool Clear();
	ITintPoint * Add(unsigned long Rvalue, unsigned long Gvalue, unsigned long Bvalue);
	ITint * Clone();

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);
	bool IsFinalClear;

protected:
	virtual ~CTint();

	virtual void FinalRelease()
	{
		IsFinalClear = true;
		Clear();
		delete this;
	}

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->TintChanged();
			}
		}
	}

protected:
	void TintPointChanged()
	{
		OnChildChanged();
	}
    void OnChildChanged();
};
