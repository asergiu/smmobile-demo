#pragma once
// ARCoating.h : header file
//

#include "ObjectBase.h"
#include <string>
using std::string;

/////////////////////////////////////////////////////////////////////////////
// CARCoating

class CARCoating :
	public CObjectEventBase<IARCoating, IARCoatingEvents>
{
public:
    static CARCoating* CreateObject();
	IARCoating * GetInterface();

protected:
	CARCoating();           // protected constructor used by dynamic creation

	// Attributes
	string m_Name, m_EDPCode, m_Info;
	unsigned long m_r, m_g, m_b;

public:

// Operations
public:
	unsigned long GetB();
	unsigned long GetG();
	unsigned long GetR();
	bool SetB(unsigned long newVal);
	bool SetG(unsigned long newVal);
	bool SetR(unsigned long newVal);
	string GetName();
	bool SetName(LPCTSTR newVal);
	string GetEDPCode();
	bool SetEDPCode(LPCTSTR newVal);
	string GetInfo();
	bool SetInfo(LPCTSTR newVal);
	IARCoating * Clone();

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);

protected:
	
	virtual ~CARCoating();

	virtual void FinalRelease()
	{
		delete this;
	}

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->ARCoatingChanged();
			}
		}
	}
};
