// LensSurface.cpp : implementation file
//

#include "stdafx.h"
#include "LensSurface.h"
#include "StringConvert.h"

/////////////////////////////////////////////////////////////////////////////
// CLensSurface

CLensSurface* CLensSurface::CreateObject()
{
	CLensSurface *pObject = new CLensSurface;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

ILensSurface * CLensSurface::GetInterface()
{
	return static_cast<ILensSurface*>(this);
}

CLensSurface::CLensSurface()
{
	IsFinalClear = false;
}

CLensSurface::~CLensSurface()
{
}

unsigned long CLensSurface::GetCount() 
{
	return Points.size();
}

bool CLensSurface::Add(double Xvalue, double Yvalue, double zRearvalue, double zFrontvalue) 
{
	LensSurfPoint newPt;
	newPt.X = Xvalue;
	newPt.Y = Yvalue;
	newPt.zRear = zRearvalue;
	newPt.zFront = zFrontvalue;

	Points.push_back(newPt);

	FireChange();
	SetModifiedFlag();
	return true;
}

bool CLensSurface::Clear() 
{
	Points.clear();

	if(!IsFinalClear){
		FireChange();
		SetModifiedFlag();
	}
	return true;
}

bool CLensSurface::GetItem(unsigned long Index, double *Xvalue, double *Yvalue, double *zRearvalue, double *zFrontvalue) 
{
	VLensSurfPoint::size_type nCount = Points.size();
	if(!TestRange(Index, 1, nCount, 1))
		return false;
	LensSurfPoint Pt;
	Pt = Points[Index - 1];
	*Xvalue = Pt.X;
	*Yvalue = Pt.Y;
	*zRearvalue = Pt.zRear;
	*zFrontvalue = Pt.zFront;
	return true;
}

bool CLensSurface::SetItemXY(unsigned long Index, double Xvalue, double Yvalue) 
{
	VLensSurfPoint::size_type nCount = Points.size();
	if(!TestRange(Index, 1, nCount, 1))
		return false;
	vector<LensSurfPoint>::iterator it = Points.begin();
	it += Index - 1;
	it->X = Xvalue;
	it->Y = Yvalue;
	return true;
}

bool CLensSurface::SetItemZ(unsigned long Index, double zRearvalue, double zFrontvalue) 
{
	VLensSurfPoint::size_type nCount = Points.size();
	if(!TestRange(Index, 1, nCount, 1))
		return false;
	vector<LensSurfPoint>::iterator it = Points.begin();
	it += Index - 1;
	it->zRear = zRearvalue;
	it->zFront = zFrontvalue;
	return true;
}

bool CLensSurface::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	string PropName;
	unsigned long i, nLoadCount;
	LensSurfPoint Pt;

	if(pPX->IsLoading()){
		Clear();
		if(pPX->PX_Long("Count", nLoadCount, 0) == false)
			return false;
		for(i = 0; i < nLoadCount; i++) {
			PropName = "LensSurfacePoint" + toString(i);
			if(pPX->PX_Double((PropName + "X").c_str(), Pt.X) == false)
				return false;
			if(pPX->PX_Double((PropName + "Y").c_str(), Pt.Y) == false)
				return false;
			if(pPX->PX_Double((PropName + "zRear").c_str(), Pt.zRear) == false)
				return false;
			if(pPX->PX_Double((PropName + "zFront").c_str(), Pt.zFront) == false)
				return false;
			Add(Pt.X, Pt.Y, Pt.zRear, Pt.zFront);
		}
	}
	else {
		nLoadCount = GetCount();
		if(pPX->PX_Long("Count", nLoadCount) == false)
			return false;
		for(i = 0; i < nLoadCount; i++) {
			Pt = Points[i];
			PropName = "LensSurfacePoint" + toString(i);
			if(pPX->PX_Double((PropName + "X").c_str(), Pt.X) == false)
				return false;
			if(pPX->PX_Double((PropName + "Y").c_str(), Pt.Y) == false)
				return false;
			if(pPX->PX_Double((PropName + "zRear").c_str(), Pt.zRear) == false)
				return false;
			if(pPX->PX_Double((PropName + "zFront").c_str(), Pt.zFront) == false)
				return false;
		}
	}

	return true;
}

ILensSurface * CLensSurface::Clone() 
{
	CLensSurface *pNewObject = CLensSurface::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}

bool CLensSurface::AddArray(const vector<LensSurfPoint>& Elements)
{
	if(Elements.size() > 0) {
		Points.assign(Elements.begin(), Elements.end());
		FireChange();
		SetModifiedFlag();
	}

	return true;
}

vector<LensSurfPoint> * CLensSurface::GetArray()
{
	return &Points;
}
