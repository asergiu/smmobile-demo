#pragma once

#include <assert.h>

template <class T>
class CObjPtr
{
public:
	// Constructors
	CObjPtr()
	{
		m_p = NULL;
	}
	CObjPtr(T* lp)
	{
		m_p = lp;
		if ((m_p = lp) != NULL)
			m_p->AddRef();
	}
	CObjPtr(const CObjPtr<T>& lp)
	{
		if ((m_p = lp.m_p) != NULL)
			m_p->AddRef();
	}
	~CObjPtr()
	{
		ReleaseDispatch();
	}

	// Operators
	operator T*() const
	{
		return (T*)m_p;
	}
	T& operator*() const
	{
		assert(m_p != NULL);
		return *m_p;
	}
	T** operator&()
	{
//        assert(m_p != NULL);
		return &m_p;
	}
	T* operator->() const
	{
		assert(m_p != NULL);
		return m_p;
	}
	T* operator=(T* lp)
	{
		if (lp != NULL)
			lp->AddRef();
		ReleaseDispatch();
		m_p = lp;
		return lp;
	}
	T* operator=(const CObjPtr<T>& lp)
	{
		if (lp.m_p != NULL)
			lp.m_p->AddRef();
		ReleaseDispatch();
		m_p = lp.m_p;
		return lp.m_p;
	}
	bool operator!() const
	{
		return (m_p == NULL);
	}
	bool operator==(T* pT) const
	{
		return m_p == pT;
	}

	// Attributes
	T* m_p;

	// Operations
	//bool CreateDispatch()
	//{
	//	assert(m_p == NULL);
	//	m_bAutoRelease = true;  // good default is to auto-release
	//	m_p = T::CreateObject();
	//	assert(m_p != NULL);
	//	return true;
	//}
	void AttachDispatch(T* pT)
	{
		assert(pT != NULL);
		ReleaseDispatch();  // detach previous
		m_p = pT;
	}
	T* DetachDispatch()
	{
		T* pT = m_p;
		m_p = NULL;    // detach without Release
		return pT;
	}
	// detach and get ownership of m_lpDispatch
	void ReleaseDispatch()
	{
		if (m_p!= NULL)
		{
			m_p->Release();
			m_p = NULL;
		}
	}
};
