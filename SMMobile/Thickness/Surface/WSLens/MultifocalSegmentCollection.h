#pragma once
// MultifocalSegmentCollection.h : header file
//

#include "ObjectBase.h"
#include <vector>
using std::vector;

const unsigned int Max_MultifocalSegment_Count = 10;

/////////////////////////////////////////////////////////////////////////////
// CMultifocalSegmentCollection

class CMultifocalSegmentCollection :
	public CObjectEventBase<IMultifocalSegmentCollection, IMultifocalSegmentCollectionEvents>,
	public CObjEventSimpleImpl<IMultifocalSegment, IMultifocalSegmentEvents>
{
public:
    static CMultifocalSegmentCollection* CreateObject();
	IMultifocalSegmentCollection * GetInterface();

protected:
	CMultifocalSegmentCollection();           // protected constructor used by dynamic creation

// Attributes
	typedef vector< CObjPtr<IMultifocalSegment> > VMultifocalSegment;
	VMultifocalSegment pt;

public:

// Operations
public:
	unsigned long GetCount();
	IMultifocalSegment * GetItem(unsigned long Index);
	bool SetItem(unsigned long Index, IMultifocalSegment * newValue);
	IMultifocalSegmentCollection * Clone();
	bool Clear();
	bool Add(IMultifocalSegment * newMultifocal);

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);
	bool IsFinalClear;

// Implementation
protected:
	virtual ~CMultifocalSegmentCollection();

	virtual void FinalRelease()
	{
		IsFinalClear = true;
		Clear();
		delete this;
	}

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->MultifocalSegmentCollectionChanged();
			}
		}
	}

protected:
	void MultifocalSegmentChanged()
	{
		OnChildChanged();
	}
    void OnChildChanged();
};
