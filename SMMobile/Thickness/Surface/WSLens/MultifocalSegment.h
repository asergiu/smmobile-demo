#pragma once
// MultifocalSegment.h : header file
//

#include "ObjectBase.h"
#include <string>
using std::string;

/////////////////////////////////////////////////////////////////////////////
// CMultifocalSegment

class CMultifocalSegment :
	public CObjectEventBase<IMultifocalSegment, IMultifocalSegmentEvents>
{
public:
    static CMultifocalSegment* CreateObject();
	IMultifocalSegment * GetInterface();

protected:
	CMultifocalSegment();           // protected constructor used by dynamic creation

// Attributes
	string m_Name ;
	double m_MainCircCenterX ;
	double m_MainCircCenterY ;
	double m_MainCircRadius  ;
	double m_BifocalBoundCircCenterX ;
	double m_BifocalBoundCircCenterY ;
	double m_BifocalBoundCircRadius  ;
	double m_TrifocalBoundCircCenterX ;
	double m_TrifocalBoundCircCenterY ;
	double m_TrifocalBoundCircRadius  ;
	double m_Addit ;
	double m_PantoRadius ;
	MultifocalSegmentType m_SegmentType ;
	unsigned long m_ErrorState ;
	double m_TrifocalOpticalCenterShiftX ;
	double m_TrifocalOpticalCenterShiftY ;
	double m_BifocalOpticalCenterShiftX ;
	double m_BifocalOpticalCenterShiftY ;
	double m_TrifocalAdditRatio ;

public:

// Operations
public:
	string GetName();
	bool SetName(LPCTSTR newValue);
	double GetMainCircCenterX();
	bool SetMainCircCenterX(double newValue);
	double GetMainCircCenterY();
	bool SetMainCircCenterY(double newValue);
	double GetMainCircRadius();
	bool SetMainCircRadius(double newValue);
	double GetBifocalBoundCircCenterX();
	bool SetBifocalBoundCircCenterX(double newValue);
	double GetBifocalBoundCircCenterY();
	bool SetBifocalBoundCircCenterY(double newValue);
	double GetBifocalBoundCircRadius();
	bool SetBifocalBoundCircRadius(double newValue);
	double GetTrifocalBoundCircCenterX();
	bool SetTrifocalBoundCircCenterX(double newValue);
	double GetTrifocalBoundCircCenterY();
	bool SetTrifocalBoundCircCenterY(double newValue);
	double GetTrifocalBoundCircRadius();
	bool SetTrifocalBoundCircRadius(double newValue);
	double GetAddit();
	bool SetAddit(double newValue);
	double GetPantoRadius();
	bool SetPantoRadius(double newValue);
	MultifocalSegmentType GetSegmentType();
	bool SetSegmentType(MultifocalSegmentType newValue);
	unsigned long GetErrorState();
	double GetTrifocalOpticalCenterShiftX();
	bool SetTrifocalOpticalCenterShiftX(double newValue);
	double GetTrifocalOpticalCenterShiftY();
	bool SetTrifocalOpticalCenterShiftY(double newValue);
	double GetBifocalOpticalCenterShiftX();
	bool SetBifocalOpticalCenterShiftX(double newValue);
	double GetBifocalOpticalCenterShiftY();
	bool SetBifocalOpticalCenterShiftY(double newValue);
	double GetTrifocalAdditRatio();
	bool SetTrifocalAdditRatio(double newValue);
	IMultifocalSegment * Clone();

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);
	void DataValidate() ;
	const double EPS;
	const double eps;

protected:
	virtual ~CMultifocalSegment();

	virtual void FinalRelease()
	{
		delete this;
	}

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->MultifocalSegmentChanged();
			}
		}
	}
};
