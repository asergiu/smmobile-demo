#pragma once
// ContourPoint.h : header file
//

#include "ObjectBase.h"

/////////////////////////////////////////////////////////////////////////////
// CContourPoint

class CContourPoint :
	public CObjectEventBase<IContourPoint, IContourPointEvents>
{
public:
    static CContourPoint* CreateObject();
	IContourPoint * GetInterface();

protected:
	CContourPoint();           // protected constructor used by dynamic creation

// Attributes
	double m_x, m_y;

public:

// Operations
public:
	double GetX();
	bool SetX(double Xvalue);
	double GetY();
	bool SetY(double Yvalue);
	bool SetPoint(double Xvalue, double Yvalue);
	IContourPoint * Clone();

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);

protected:
	virtual ~CContourPoint();

	virtual void FinalRelease()
	{
		delete this;
	}

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->ContourPointChanged();
			}
		}
	}
};
