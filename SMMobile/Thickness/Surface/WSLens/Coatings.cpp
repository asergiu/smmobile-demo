// Coatings.cpp : implementation file
//

#include "stdafx.h"
#include "Coatings.h"


void CCoatings::OnChildChanged() 
{
	FireChange();
	SetModifiedFlag();
}
/////////////////////////////////////////////////////////////////////////////
// CCoatings

CCoatings* CCoatings::CreateObject()
{
	CCoatings *pObject = new CCoatings;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

ICoatings * CCoatings::GetInterface()
{
	return static_cast<ICoatings*>(this);
}

CCoatings::CCoatings()
{
	m_Name = "";
	m_EDPCode = "";
	m_Info = "";
}

CCoatings::~CCoatings()
{
	CEventImpl_UVProtection::UnadviseConnection(/*lpdispUVProtection,*/ 0);
	CEventImpl_ScratchResistance::UnadviseConnection(/*lpdispScratchResistance,*/ 1);
	CEventImpl_WaterRepellent::UnadviseConnection(/*lpdispWaterRepellent,*/ 2);
	CEventImpl_OtherCoating::UnadviseConnection(/*lpdispOtherCoating,*/ 3);
}

/////////////////////////////////////////////////////////////////////////////
// CCoatings message handlers

string CCoatings::GetName() 
{
	return m_Name;
}

bool CCoatings::SetName(LPCTSTR lpszNewValue) 
{
	m_Name = lpszNewValue;
	SetModifiedFlag();
	FireChange();
	return true;
}

string CCoatings::GetEDPCode() 
{
	return m_EDPCode;
}

bool CCoatings::SetEDPCode(LPCTSTR lpszNewValue) 
{
	m_EDPCode = lpszNewValue;
	SetModifiedFlag();
	FireChange();
	return true;
}

string CCoatings::GetInfo() 
{
	return m_Info;
}

bool CCoatings::SetInfo(LPCTSTR lpszNewValue) 
{
	m_Info = lpszNewValue;
	SetModifiedFlag();
	FireChange();
	return true;
}

ICoating* CCoatings::GetUVProtection() 
{
	if(lpdispUVProtection)
		lpdispUVProtection->AddRef();
	return lpdispUVProtection;
}

bool CCoatings::SetUVProtection(ICoating* newValue) 
{
	CEventImpl_UVProtection::UnadviseConnection(/*lpdispUVProtection,*/ 0);
	lpdispUVProtection = newValue;
	CEventImpl_UVProtection::AdviseConnection(lpdispUVProtection, 0);

	FireChange();
	SetModifiedFlag();
	return true;
}

ICoating* CCoatings::GetScratchResistance() 
{
	if(lpdispScratchResistance)
		lpdispScratchResistance->AddRef();
	return lpdispScratchResistance;
}

bool CCoatings::SetScratchResistance(ICoating* newValue) 
{
	CEventImpl_ScratchResistance::UnadviseConnection(/*lpdispScratchResistance,*/ 1);
	lpdispScratchResistance = newValue;
	CEventImpl_ScratchResistance::AdviseConnection(lpdispScratchResistance, 1);

	FireChange();
	SetModifiedFlag();
	return true;
}

ICoating* CCoatings::GetWaterRepellent() 
{
	if(lpdispWaterRepellent)
		lpdispWaterRepellent->AddRef();
	return lpdispWaterRepellent;
}

bool CCoatings::SetWaterRepellent(ICoating* newValue) 
{
	CEventImpl_WaterRepellent::UnadviseConnection(/*lpdispWaterRepellent,*/ 2);
	lpdispWaterRepellent = newValue;
	CEventImpl_WaterRepellent::AdviseConnection(lpdispWaterRepellent, 2);

	FireChange();
	SetModifiedFlag();
	return true;
}

ICoating* CCoatings::GetOtherCoating() 
{
	if(lpdispOtherCoating)
		lpdispOtherCoating->AddRef();
	return lpdispOtherCoating;
}

bool CCoatings::SetOtherCoating(ICoating* newValue) 
{
	CEventImpl_OtherCoating::UnadviseConnection(/*lpdispOtherCoating,*/ 3);
	lpdispOtherCoating = newValue;
	CEventImpl_OtherCoating::AdviseConnection(lpdispOtherCoating, 3);

	FireChange();
	SetModifiedFlag();
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// Persistence

bool CCoatings::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	CEventImpl_UVProtection::UnadviseConnection(/*lpdispUVProtection,*/ 0);
	if(PX_WSLObject<CCoating, ICoating>(pPX, "UVProtection", lpdispUVProtection, NULL) == false)
		return false;
	CEventImpl_UVProtection::AdviseConnection(lpdispUVProtection, 0);

	CEventImpl_ScratchResistance::UnadviseConnection(/*lpdispScratchResistance,*/ 1);
	if(PX_WSLObject<CCoating, ICoating>(pPX, "UVProtection", lpdispScratchResistance, NULL) == false)
		return false;
	CEventImpl_ScratchResistance::AdviseConnection(lpdispScratchResistance, 1);

	CEventImpl_WaterRepellent::UnadviseConnection(/*lpdispWaterRepellent,*/ 2);
	if(PX_WSLObject<CCoating, ICoating>(pPX, "UVProtection", lpdispWaterRepellent, NULL) == false)
		return false;
	CEventImpl_WaterRepellent::AdviseConnection(lpdispWaterRepellent, 2);

	CEventImpl_OtherCoating::UnadviseConnection(/*lpdispOtherCoating,*/ 3);
	if(PX_WSLObject<CCoating, ICoating>(pPX, "UVProtection", lpdispOtherCoating, NULL) == false)
		return false;
	CEventImpl_OtherCoating::AdviseConnection(lpdispOtherCoating, 3);

	return true;
}

ICoatings * CCoatings::Clone() 
{
	CCoatings *pNewObject = CCoatings::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}
