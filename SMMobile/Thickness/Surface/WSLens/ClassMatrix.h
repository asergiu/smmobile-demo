#pragma once

#include <assert.h>

#include <list>
#define WIDTHBYTES(i)   ((unsigned)((i+31)&(~31))/8)  /* ULONG aligned ! */

template<class T> class CMatrix;

inline int __sign(double a)
{
	const double eps = 1e-10;
	if(a > eps)
		return 1;
	else
	{
		if(a < -eps)
			return -1;
		else
			return 0;
	}
}

template<class T>
class CMatrixRow
{
public:
	T *pRow;
	LONG LBound;
	LONG UBound;

	operator T*()
	{
		return pRow;
	}

	T &operator[](char nIndex)
	{
		assert(nIndex >= LBound && nIndex <= UBound);
		return pRow[nIndex];
	}

	T &operator[](short nIndex)
	{
		assert(nIndex >= LBound && nIndex <= UBound);
		return pRow[nIndex];
	}

	T &operator[](int nIndex)
	{
		assert(nIndex >= LBound && nIndex <= UBound);
		return pRow[nIndex];
	}
	
	T &operator[](long nIndex)
	{
		assert(nIndex >= LBound && nIndex <= UBound);
		return pRow[nIndex];
	}
};

template<class T>
class CMatrix
{
public:
	CMatrix()
	{
		m_Content = NULL;
	}
	
	~CMatrix()
	{
		Destroy();
	}

	BOOL Create(int DimY = 1, int DimX = 1, BOOL fZeroInit = FALSE)
	{
		if(DimY < 1 || DimX < 1)
			return FALSE;

		RECT rc = { 0, 0, DimX - 1, DimY - 1 };

		return CMatrix<T>::Create(&rc, fZeroInit);
	}

	BOOL Create(LPCRECT pRect, BOOL fZeroInit = FALSE)
	{
		int i, SizeMatrix;
		char *Pointer2;
		CMatrixRow<T> *MemBlock, *Pointer1;

		int SizeX = pRect->right - pRect->left + 1;
		int SizeY = pRect->bottom - pRect->top + 1;
		int ItemSize = sizeof(T);
		
		Destroy();

		m_Rect = *pRect;
		m_SizeY = SizeY;
		m_SizeX = SizeX;
		SizeMatrix = (SizeX * SizeY) * ItemSize + SizeY * sizeof(CMatrixRow<T>);

		if((MemBlock = (CMatrixRow<T> *)calloc(1, SizeMatrix)) == NULL)
		{
			m_Content = NULL;
			return FALSE;
		}
		
		if(fZeroInit)
			memset(MemBlock, 0, SizeMatrix);

		Pointer1 = MemBlock;
		Pointer2 = (char *)Pointer1 + SizeY * sizeof(CMatrixRow<T>) - pRect->left * ItemSize;
		
		ItemSize *= SizeX;

		for(i = SizeY; i != 0; i--, Pointer1++, Pointer2 += ItemSize)
		{
			Pointer1->pRow = (T *)Pointer2;
			Pointer1->LBound = m_Rect.left;
			Pointer1->UBound = m_Rect.right;
		}
		
		m_Content = MemBlock - pRect->top;
		
		return TRUE;
	}

	void Destroy()
	{
		if(m_Content != NULL)
			free(m_Content + m_Rect.top);
		m_Content = NULL;
	}

	int GetX()
	{
		return m_SizeX;
	}
	
	int	GetY()
	{
		return m_SizeY;
	}
	
	RECT GetRect()
	{
		return m_Rect;
	}

	CMatrixRow<T>& operator[](int nIndex)
	{
		assert(m_Content && nIndex >= m_Rect.top && nIndex <= m_Rect.bottom);
		return m_Content[nIndex];
	}

	CMatrixRow<T>* operator+(int nIndex)
	{
		return m_Content + nIndex;
	}

	CMatrix& operator=(const CMatrix &InMatrix)
	{
		if(!m_Content || !EqualRect(&m_Rect, &InMatrix.m_Rect))
			Create(&InMatrix.m_Rect, FALSE);

		CMatrixRow<T>	*Line1 = m_Content + m_Rect.top,
						*Line2 = InMatrix.m_Content + m_Rect.top;
		
		int Len = m_SizeX * sizeof(T);
		for(int i = m_SizeY; i > 0; i--, Line1++, Line2++)
		{
			memcpy(*Line1 + m_Rect.left, *Line2 + m_Rect.left, Len);
		}
		
		return *this;
	}

	void CopyPartMatr(CMatrix &SourceMatrix, LPCRECT pSourceRect, LPPOINT pDestBasePoint)
	{
		int Len = (pSourceRect->right - pSourceRect->left + 1) * sizeof(T),
			SizeY = pSourceRect->bottom - pSourceRect->top + 1;
		
		CMatrixRow<T>	*Line1 = m_Content + pDestBasePoint->y,
						*Line2 = SourceMatrix.m_Content + pSourceRect->top;
		
		for(int i = SizeY; i; i--, Line1++, Line2++)
		{
			memcpy(*Line1 + pDestBasePoint->x, *Line2 + pSourceRect->left, Len);
		}
	}

	void ZeroInit()
	{
		memset(m_Content + m_Rect.top + m_SizeY, 0, m_SizeX * m_SizeY * sizeof(T));
	}

	BOOL IsReady()
	{
		return(m_Content != NULL);
	}

	void Fill(T& Element)
	{
		T *Line;
		
		int i, j;
		for(i = 0; i < m_SizeY; i++)
		{
			Line = *(m_Content + m_Rect.top + i); 
			for(j = m_Rect.left; j < m_Rect.left + m_SizeX; j++)
				Line[j] = Element;
		}
	}

	// �������� ����� ��������� pattern
	void Line(POINT pt1, POINT pt2, T &pattern, BOOL bCheckBounds = TRUE)
	{
		return Line(pt1.x, pt1.y, pt2.x, pt2.y, pattern, bCheckBounds);
	}
	void Line(int x1, int y1, int x2, int y2, T &pattern, BOOL bCheckBounds = TRUE)
	{
		int dx, dy, ico = 0, i, j, ii, jj;
		int xb = m_Rect.left,
			xe = m_Rect.right,
			yb = m_Rect.top,
			ye = m_Rect.bottom;

 		dx = abs((jj = x1) - x2);
		dy = abs((ii = y1) - y2);
		
		if(!bCheckBounds || (y1 >= yb && y1 <= ye && x1 >= xb && x1 <= xe))
			m_Content[y1][x1] = pattern;
		if(!bCheckBounds || (y2 >= yb && y2 <= ye && x2 >= xb && x2 <= xe))
			m_Content[y2][x2] = pattern;

		if(dy <= dx)
		{
			for(j = 1; j <= dx; j++)
			{
				ico += dy;
				jj = jj + __sign(x2 - x1);
				if(ico < dx)
				{
					if(!bCheckBounds || (ii >= yb && ii <= ye && jj >= xb && jj <= xe))
						m_Content[ii][jj] = pattern;
				}
				else
				{
					ico -= dx;
					ii = ii + __sign(y2 - y1);
					if(!bCheckBounds || (ii >= yb && ii <= ye && jj >= xb && jj <= xe))
						m_Content[ii][jj] = pattern;
				}
			}
		}
		else
		{
			for(i = 1; i <= dy; i++)
			{
				ico += dx;
				ii = ii + __sign(y2 - y1);
				if(ico < dy)
				{
					if(!bCheckBounds || (ii >= yb && ii <= ye && jj >= xb && jj <= xe))
						m_Content[ii][jj] = pattern;
				}
				else
				{
					ico -= dy;
					jj = jj + __sign(x2 - x1);
					if(!bCheckBounds || (ii >= yb && ii <= ye && jj >= xb && jj <= xe))
						m_Content[ii][jj] = pattern;
				}
			}
		}
	}

	// ������� filler �� ������� pattern
	void Fill_While(POINT init_point, T &pattern, T &filler)
	{
		Fill_While(init_point.x, init_point.y, pattern, filler);
	}
	void Fill_While(int x, int y, T &pattern, T &filler)
	{
		int xb = m_Rect.left,
			xe = m_Rect.right,
			yb = m_Rect.top,
			ye = m_Rect.bottom;
		
		if(!(y > yb && y < ye && x > xb && x < xe))
			return;

//		PointList List;
		std::list<POINT> List;
		int curX, curY, baseX = x, baseY = y;
		POINT pt;
		BOOL fEmpty;
		RECT rect;

		rect.left = rect.right = baseX;
		rect.top = rect.bottom = baseY;

		do
		{
			curX = baseX; curY = baseY;
			if(curY < rect.top)
				rect.top = curY;
			if(curY > rect.bottom)
				rect.bottom = curY;
			// ������� ��������� � ����� ������ ����� ������,
			// ���� ��� �� ���������
			if(m_Content[curY - 1][curX] == pattern)
			{
				pt.x = curX; pt.y = curY - 1;
//				List.AddHead(pt);
				List.push_front(pt);
			}
			// ����� ��������� � ����� ������ ����� �����,
			// ���� ��� �� ���������
			if(m_Content[curY + 1][curX] == pattern)
			{
				pt.x = curX; pt.y = curY + 1;
//				List.AddHead(pt);
				List.push_front(pt);
			}
			// �������� ������� �����
			if(m_Content[curY][curX] == pattern)
				m_Content[curY][curX] = filler;
			// ���� ������
			curX++;
			while(m_Content[curY][curX] == pattern)
			{
				m_Content[curY][curX] = filler;
				// �����
				if(m_Content[curY + 1][curX] == pattern && !(m_Content[curY + 1][curX - 1] == pattern))
				{
					pt.x = curX; pt.y = curY + 1;
//					List.AddHead(pt);
					List.push_front(pt);
				}
				// ������
				if(m_Content[curY - 1][curX] == pattern && !(m_Content[curY - 1][curX - 1] == pattern))
				{
					pt.x = curX; pt.y = curY - 1;
//					List.AddHead(pt);
					List.push_front(pt);
				}
				curX++;
			}
			if(curX > rect.right)
				rect.right = curX - 1;
			
			// ���� �����
			curX = baseX - 1; curY = baseY;
			while(m_Content[curY][curX] == pattern)
			{
				m_Content[curY][curX] = filler;
				// �����
				if(m_Content[curY + 1][curX] == pattern && !(m_Content[curY + 1][curX + 1] == pattern))
				{
					pt.x = curX; pt.y = curY + 1;
//					List.AddHead(pt);
					List.push_front(pt);
				}
				// ������
				if(m_Content[curY - 1][curX] == pattern && !(m_Content[curY - 1][curX + 1] == pattern))
				{
					pt.x = curX; pt.y = curY - 1;
//					List.AddHead(pt);
					List.push_front(pt);
				}
				curX--;
			}
			if(curX < rect.left)
				rect.left = curX + 1;
			
//			if(!(fEmpty = List.IsEmpty()))
			if(!(fEmpty = List.empty()))
			{
//				pt = List.RemoveHead();
				pt = List.front();
				List.pop_front();
				baseX = pt.x; baseY = pt.y;
			}
		} while(!fEmpty);
		
	//	InflateRect(pRC, 1, 1);
	}

	// ������� �� �������
	void Fill_Until(POINT init_point, T &pattern, T &filler)
	{
		Fill_Until(init_point.x, init_point.y, pattern, filler);
	}
	void Fill_Until(int x, int y, T &pattern, T &filler)
	{
		int xb = m_Rect.left,
			xe = m_Rect.right,
			yb = m_Rect.top,
			ye = m_Rect.bottom;

		if(!(y > yb && y < ye && x > xb && x < xe))
			return;

//		PointList List;
		std::list<POINT> List;
		int curX, curY, baseX = x, baseY = y;
		POINT pt;
		BOOL fEmpty;
		RECT rect;
 		rect.left = rect.right = baseX;
		rect.top = rect.bottom = baseY;

		m_Content[y][x] = filler;

		do
		{
			curX = baseX; curY = baseY;
			if(curY < rect.top)
				rect.top = curY;
			if(curY > rect.bottom)
				rect.bottom = curY;
			// ������� ��������� � ����� ������ ����� ������,
			// ���� ��� �� ���������
			if(!(m_Content[curY - 1][curX] == pattern))
			{
				pt.x = curX; pt.y = curY - 1;
//				List.AddHead(pt);
				List.push_front(pt);
			}
			// ����� ��������� � ����� ������ ����� �����,
			// ���� ��� �� ���������
			if(!(m_Content[curY + 1][curX] == pattern))
			{
				pt.x = curX; pt.y = curY + 1;
//				List.AddHead(pt);
				List.push_front(pt);
			}
			// �������� ������� �����
			if(!(m_Content[curY][curX] == pattern))
				m_Content[curY][curX] = filler;
			// ���� ������
			curX++;
			while(!(m_Content[curY][curX] == pattern))
			{
				m_Content[curY][curX] = filler;
				// �����
				if(!(m_Content[curY + 1][curX] == pattern) && m_Content[curY + 1][curX - 1] == pattern)
				{
					pt.x = curX; pt.y = curY + 1;
//					List.AddHead(pt);
					List.push_front(pt);
				}
				// ������
				if(!(m_Content[curY - 1][curX] == pattern) && m_Content[curY - 1][curX - 1] == pattern)
				{
					pt.x = curX; pt.y = curY - 1;
//					List.AddHead(pt);
					List.push_front(pt);
				}
				curX++;
			}
			if(curX > rect.right)
				rect.right = curX - 1;
			// ���� �����
			curX = baseX - 1; curY = baseY;
			while(!(m_Content[curY][curX] == pattern))
			{
				m_Content[curY][curX] = filler;
				// �����
				if(!(m_Content[curY + 1][curX] == pattern) && m_Content[curY + 1][curX + 1] == pattern)
				{
					pt.x = curX; pt.y = curY + 1;
//					List.AddHead(pt);
					List.push_front(pt);
				}
				// ������
				if(!(m_Content[curY - 1][curX] == pattern) && m_Content[curY - 1][curX + 1] == pattern)
				{
					pt.x = curX; pt.y = curY - 1;
//					List.AddHead(pt);
					List.push_front(pt);
				}
				curX--;
			}
			if(curX < rect.left)
				rect.left = curX + 1;
//			if(!(fEmpty = List.IsEmpty()))
			if(!(fEmpty = List.empty()))
			{
//				pt = List.RemoveHead();
				pt = List.front();
				List.pop_front();
				baseX = pt.x; baseY = pt.y;
			}
		} while(!fEmpty);
	//	InflateRect(pRC, 1, 1);
	}

protected:
	CMatrixRow<T> *m_Content;

	int m_SizeY;
	int m_SizeX;
	RECT m_Rect;
};

typedef struct tagTriplet
{
	BYTE cB;
	BYTE cG;
	BYTE cR;
} Triplet;

typedef CMatrix<int>			CIntMatrix;
typedef CMatrix<unsigned char>	CByteMatrix;
typedef CMatrix<unsigned short> CWordMatrix;
typedef CMatrix<float> 			CFloatMatrix;
typedef CMatrix<double>			CDoubleMatrix;
typedef	CMatrix<RGBQUAD>		CColorMatrix32;
typedef CMatrix<Triplet>		CColorMatrix;

#ifdef _WINDOWS_
inline HBITMAP Matrix32ToBitmap(CColorMatrix32 &Matrix)
{
	if(!Matrix.IsReady())
		return NULL;

	HDC hdcScreen;
	HBITMAP hBmp;	

	int dx = Matrix.GetX();
	int dy = Matrix.GetY();

	if((hdcScreen = GetDC(NULL)) == NULL)
		return NULL;

	if((hBmp = CreateCompatibleBitmap(hdcScreen, dx, dy)) == NULL)
	{
		ReleaseDC(NULL, hdcScreen);
		return NULL;
	}

	BITMAPINFOHEADER bmHeader;
	memset(&bmHeader, 0, sizeof(BITMAPINFOHEADER));
	bmHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmHeader.biWidth = dx;
	bmHeader.biHeight = -dy;
	bmHeader.biPlanes = 1;
	bmHeader.biBitCount = 32;
	bmHeader.biCompression = BI_RGB;

	RECT rcMatrix = Matrix.GetRect();
	BOOL bRes = (SetDIBits(hdcScreen, hBmp, 0, dy, &Matrix[rcMatrix.top][rcMatrix.left], (LPBITMAPINFO)&bmHeader, DIB_RGB_COLORS) > 0);

	ReleaseDC(NULL, hdcScreen);

	return (bRes) ? hBmp : NULL;
};

inline BOOL BitmapToMatrix32(HBITMAP hBitmap, CColorMatrix32 &Matrix)
{
	if(!hBitmap)
		return FALSE;

	BITMAP bmInfo;
	GetObject(hBitmap, sizeof(bmInfo), &bmInfo);

	HDC hdcScreen;

	int dx = bmInfo.bmWidth;
	int dy = bmInfo.bmHeight;

	if(!Matrix.Create(dy, dx, TRUE))
		return FALSE;

	if((hdcScreen = GetDC(NULL)) == NULL)
		return NULL;

	BITMAPINFOHEADER bmHeader;
	memset(&bmHeader, 0, sizeof(BITMAPINFOHEADER));
	bmHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmHeader.biWidth = dx;
	bmHeader.biHeight = -dy;
	bmHeader.biPlanes = 1;
	bmHeader.biBitCount = 32;
	bmHeader.biCompression = BI_RGB;

	RECT rcMatrix = Matrix.GetRect();
	BOOL bRes = (GetDIBits(hdcScreen, hBitmap, 0, dy, &Matrix[rcMatrix.top][rcMatrix.left], (LPBITMAPINFO)&bmHeader, DIB_RGB_COLORS) > 0);

	ReleaseDC(NULL, hdcScreen);

	return bRes;
}
#else
/*
inline long Matrix32ToBitmap(CColorMatrix32 &Matrix)
{
	return 0;
}
inline BOOL BitmapToMatrix32(long hBitmap, CColorMatrix32 &Matrix)
{
	return FALSE;
}
*/
#endif

inline BOOL BitmapToMatrix32(const unsigned char * hBitmap, CColorMatrix32 &Matrix)
{
    if(!hBitmap)
        return FALSE;
    
    BITMAPINFOHEADER bmiHeader = *((LPBITMAPINFOHEADER)hBitmap);
    if(!(bmiHeader.biBitCount == 24))
        return FALSE;
    
    int dx = bmiHeader.biWidth;
    int dy = bmiHeader.biHeight;
    
    if(!Matrix.Create(dy, dx, TRUE))
        return FALSE;
    
    int LineLen = WIDTHBYTES(bmiHeader.biWidth*bmiHeader.biBitCount);
    RECT rcMatrix = Matrix.GetRect();
    memcpy(&Matrix[rcMatrix.top][rcMatrix.left], hBitmap + bmiHeader.biSize, LineLen * (int)bmiHeader.biHeight);
    
    return TRUE;
}
