#include "stdafx.h"
#include "LENSE2ILens.h"
#include "UsefulThings.h"

char *GetTypeName( int Focal , int Kind, int *place = NULL);

//#define CString string

// ����������� ��������� LENSE � ������ ILens
// ��������� ���� ������� ILens �� �����������:
//		CenterX, CenterY
//		Contour
//		Diameter
//		EDPCode
//		EllipticBlank, EllipticVertToHorizRatio
//		Engine1, Engine2, EngineUsageMask
//		Multifocal (user-defined)
//		OptToGeomCenterDisplacementX, OptToGeomCenterDisplacementY
//		Prism, PrismAxis
BOOL LENSE2ILens(LENSE *pLens, ILens *pLensObject, BOOL bIsLeft)
{
	if(!pLens || !pLensObject)
		return FALSE;

	int i;

	try
	{
		// addit
		pLensObject->SetAddit((bIsLeft) ? pLens->dDpr : pLens->dDpl);
		// anti-reflex
//		extern int __coating[4];
		const static double ARCoeff[4][3] = {
												{1., 1., 1.},
												{0.5, 0.3, 0.9},
												{0.857, 0.619, 0.3},
												{0.25, 0.25, 0.25}
											};
		double ar_r, ar_g, ar_b;
		//CString ARName;
		if(pLens->Itip[0] != 10)
		{
			ar_r = ARCoeff[pLens->Itip[0]][0];
			ar_g = ARCoeff[pLens->Itip[0]][1];
			ar_b = ARCoeff[pLens->Itip[0]][2];
			//ARName.LoadString(__coating[pLens->Itip[0]]);
		}
		else
		{
			ar_r = pLens->AntiReflexCoeffR;
			ar_g = pLens->AntiReflexCoeffG;
			ar_b = pLens->AntiReflexCoeffB;
			//ARName = pLens->AntiReflexName;
		}
		IARCoating * pARCoating;
		if(!(pARCoating = Create_IARCoating()))
			throw 0;
		if(ar_r != 1. || ar_g != 1. || ar_b != 1)
		{
			pARCoating->SetR(Round(ar_r * 255));
			pARCoating->SetG(Round(ar_g * 255));
			pARCoating->SetB(Round(ar_b * 255));
			//pARCoating->SetName(ARName);
		}
		pLensObject->SetARCoating(pARCoating);
		pARCoating->Release();
		// axis
		pLensObject->SetAxis((bIsLeft) ? Round(pLens->Axis_Right) : Round(pLens->Axis_Left));
		// coatings
//		extern int __spcoating[3];
		ICoatings * pCoatings;
		if(!(pCoatings = Create_ICoatings()))
			throw 0;
        if(pLens->Itip[1] > 0)
		{
			//CString strCoatingsName;
			for(i = 0; i < 3; i++)
			{
				if(pLens->Itip[1] & MaskaOtherCoating[i])
				{
				    ICoating * pCoating;
					if(!(pCoating = Create_ICoating()))
						throw 0;
					/*CString CoatName;
					CoatName.LoadString(__spcoating[i]);
					pCoating->SetName(CoatName);
					if(!CoatName.IsEmpty())
					{
						if(!strCoatingsName.IsEmpty())
							strCoatingsName += ", ";
						strCoatingsName += CoatName;
					}*/

					switch(i)
					{
					case 0:
						pCoatings->SetUVProtection(pCoating);
						break;
					case 1:
						pCoatings->SetScratchResistance(pCoating);
						break;
					case 2:
						pCoatings->SetWaterRepellent(pCoating);
						break;
					default:
						ASSERT(0);
					}

					pCoating->Release();
				}
			}

			/*if(!strCoatingsName.IsEmpty())
				pCoatings->SetName(strCoatingsName);*/

		}
		pLensObject->SetCoatings(pCoatings);
		pCoatings->Release();
		// cyl
		pLensObject->SetCyl((bIsLeft) ? pLens->Cyl_Right : pLens->Cyl_Left);
		// density
		pLensObject->SetDensity(pLens->Density);
		// design
		switch(pLens->Design)
		{
		case 0:
			pLensObject->SetDesign(wsClassic.c_str());
			break;
		case 1:
			pLensObject->SetDesign(wsAspheric.c_str());
			break;
		case 2:
			pLensObject->SetDesign(wsLenticular.c_str());
			break;
		default:
			ASSERT(0);
		}
        // photochrome flag
        pLensObject->SetIsPhotochromic(pLens->PHOTOCHROM > 0);
        /*// kind of material
		extern CString __material[];*/

		pLensObject->SetKindOfMaterial((KindOfMaterial)pLens->MAT);
		/*CString strMaterial;
		if(pLens->LenseFlag == 1)
			strMaterial = pLens->LenseType;
		else
			strMaterial = __material[pLens->MatName];*/
		//AddPropertyToLensEDPCode(pLensObject, _T('M'), strMaterial);
		// supplier
		/*if(pLens->I_degrade == 3 || pLens->LenseFlag == 1 || pLens->PHOTOCHROM == 10)
			AddPropertyToLensEDPCode(pLensObject, _T('S'), pLens->information);
        // multifocal type
		if(pLens->dDpr != 0. || pLens->dDpl != 0.)
			AddPropertyToLensEDPCode(pLensObject, _T('F'), GetTypeName(pLens->Focal, pLens->Kind));*/
		if(pLens->Focal == 1)
		{
			pLensObject->SetMultifocalType(wsProgressive);
            pLensObject->SetProgressive(NULL);
		}
		else if(pLens->Focal == 2 || pLens->Focal == 3)
		{
			BOOL bIsTrifocal = (pLens->Focal == 3);
			int mf_type = wsSingleVision;
			switch(pLens->Kind)
			{
			case 1:					// --> FT 28
				mf_type = (bIsTrifocal) ? wsFT7x28t : wsFT28b;
				break;
			case 2:					// --> FT 35
				mf_type = (bIsTrifocal) ? wsFT8x35t : wsFT35b;
				break;
			case 5:					// --> FT 25
				mf_type = (bIsTrifocal) ? wsFT6x25t : wsFT25b;
				break;
			case 6:					// --> RT 25
				mf_type = (bIsTrifocal) ? wsRT6x25t : wsRT25b;
				break;
			case 7:					// --> RT 28
				mf_type = (bIsTrifocal) ? wsRT7x28t : wsRT28b;
				break;
			case 3:					// --> RT 334 mm  Fulseg
				mf_type = (bIsTrifocal) ? wsFulsegt : wsFulsegb;
				break;
			case 4:					// --> RT 35 mm  Ultex
				mf_type = wsUltex;
				break;
			case 8:					// --> Round 25
				mf_type = wsRound25;
				break;
			case 9:					// --> Round 28
				mf_type = wsRound28;
				break;
			case 10:				// --> Round 38
				mf_type = wsRound38;
				break;
			}
			pLensObject->SetMultifocalType((MultifocalType)mf_type);
		}
		else
			pLensObject->SetMultifocalType(wsSingleVision);
		// thicknesses
		pLensObject->SetPermittedCenterThickness(pLens->Thickness.CenMin);
		pLensObject->SetPermittedEdgeThickness(pLens->Thickness.EdgeMin);
		pLensObject->SetPermittedZeroThickness(pLens->Thickness.IntegralMin);
        // car index
		pLensObject->SetPhotochromicCarIndex(pLens->InCar / 100.);
		// heat index
		pLensObject->SetPhotochromicHeatIndex(pLens->Heat / 100.);
		// photochrome tints
		if(pLens->PHOTOCHROM > 0)
		{
			int r0, g0, b0, r1, g1, b1;
			switch(pLens->PHOTOCHROM)
			{
			case 1:
				r0 = 243; r1 = 146;
				g0 = 227; g1 = 146;
				b0 = 215; b1 = 146;
				break;
			case 2:
				r0 = 219; r1 = 121;
				g0 = 219; g1 = 121;
				b0 = 219; b1 = 121;
				break;
			case 3:
				r0 = 243; r1 = 142;
				g0 = 227; g1 = 89;
				b0 = 215; b1 = 73;
				break;
			case 10:
				r0 = pLens->R[1]; r1 = pLens->R[2];
				g0 = pLens->G[1]; g1 = pLens->G[2];
				b0 = pLens->B[1]; b1 = pLens->B[2];
				break;
			}

			ITint * pTintDark, * pTintBright;
			if(!(pTintDark = Create_ITint()))
				throw 0;
			if(!(pTintBright = Create_ITint()))
				throw 0;

            pTintDark->Add(r0, g0, b0)->Release();
            pTintBright->Add(r1, g1, b1)->Release();

			pTintDark->SetName(pLens->NAME_Standard);
			pTintBright->SetName(pLens->NAME_Standard);

			pLensObject->SetTint(pTintDark);
			pLensObject->SetPhotochromicTint(pTintBright);

			pTintDark->Release();
			pTintBright->Release();
		}
		// refraction index
		pLensObject->SetRefIndex(pLens->Index);
		// segment height
		pLensObject->SetSegmHeight((bIsLeft) ? pLens->SH_Right : pLens->SH_Left);
		// side
		pLensObject->SetSide((bIsLeft) ? wsLeft : wsRight);
		// sph
		pLensObject->SetSph((bIsLeft) ? pLens->Dpr : pLens->Dpl);
		// suspendcalc
		pLensObject->SetSuspendCalc(0);
        // tint
		ITint * pTint;
		if(!(pTint = Create_ITint()))
			throw 0;

//		extern int farbe[5];
		/*CString strTintName;
		strTintName.LoadString(farbe[0]);
		pTint->SetName(strTintName);*/

		if(pLens->I_degrade > 0 || pLens->r_f != 255 || pLens->g_f != 255 || pLens->b_f != 255 ||
									pLens->r_e != 255 || pLens->g_e != 255 || pLens->b_e != 255)
		{
			//strTintName.LoadString(farbe[1]);
			switch(pLens->I_degrade)
			{
			case 0: // constant
				pTint->Add(Round(pLens->r_f), Round(pLens->g_f), Round(pLens->b_f))->Release();
				break;
			case 1: // degrade
				pTint->Add(Round(pLens->r_f), Round(pLens->g_f), Round(pLens->b_f))->Release();
				pTint->Add(255, 255, 255);
				break;
			case 2: // melting
				pTint->Add(Round(pLens->r_f), Round(pLens->g_f), Round(pLens->b_f))->Release();
				pTint->Add(Round(pLens->r_e), Round(pLens->g_e), Round(pLens->b_e))->Release();
				break;
			case 3: // catalog
				for(i = 0; i < 200; i++)
					pTint->Add(pLens->R[i], pLens->G[i], pLens->B[i])->Release();
				//strTintName = pLens->NAME_Standard;
				break;
			}

			//pTint->SetName(strTintName);
			pLensObject->SetTint(pTint);
		}
		else if(pLens->PHOTOCHROM == 0)
			pLensObject->SetTint(pTint);

		pTint->Release();
	}
	catch(...)
	{
		/*TCHAR   szCause[255];
		CString strFormatted;

		strFormatted = "Error converting LENSE structure to ILens object: ";
		ex->GetErrorMessage(szCause, 255);
		strFormatted += szCause;
		strFormatted += "\n";

		TRACE0(strFormatted);
	
		e->Delete();*/

		return FALSE;
	}

	return TRUE;
}