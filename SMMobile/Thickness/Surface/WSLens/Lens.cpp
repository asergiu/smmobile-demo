// Lens.cpp : implementation file
//
#include "stdafx.h"
#include "Lens.h"
#include "Parameters/Asph2.h"
#include "Parameters/Thickness.h"
#include "Tint.h"
#include "ARCoating.h"
#include "Contour.h"
#include "Progressive.h"
#include "WSLensEngine.h"
#include "Coatings.h"
#include "MultifocalSegmentCollection.h"
#include "ErrorString.h"

#define M_PI        3.14159265358979323846

/////////////////////////////////////////////////////////////////////////////
// CLens

CLens* CLens::CreateObject()
{
	CLens *pObject = new CLens;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

ILens * CLens::GetInterface()
{
	return static_cast<ILens*>(this);
}

CLens::CLens()
{
	InvalidateComputableParams();

	m_Side = wsRight;
	m_Sph = 0.;
	m_Cyl = 0.;
	m_Add = 0.;
	m_Axis = 0;
	m_KindOfMaterial = wsPlastic;
	m_RefIndex = 1.501;
	m_Density = 1.32;
	m_Design = wsClassic;
	m_Diameter = 70;
	m_CenterX = m_Diameter/2. + 5.;
	m_CenterY = m_Diameter/2.;
	m_IsPhotochromic = false;
	m_PhotochromicCarIndex = 0.2;
	m_PhotochromicHeatIndex = 0.8;

	lpdispTint = NULL;
	CTint *pTint = CTint::CreateObject();
	if(pTint){
		lpdispTint.AttachDispatch(pTint->GetInterface());
		CObjEventSimpleImpl<ITint, ITintEvents>::AdviseConnection(lpdispTint, 0);
	}

	lpdispARCoating = NULL;
	CARCoating *pARCoating = CARCoating::CreateObject();
	if(pARCoating){
		lpdispARCoating.AttachDispatch(pARCoating->GetInterface());
		CObjEventSimpleImpl<IARCoating, IARCoatingEvents>::AdviseConnection(lpdispARCoating, 1);
	}

	lpdispContour = NULL;
	CContour *pContour = CContour::CreateObject();
	if(pContour){
		lpdispContour.AttachDispatch(pContour->GetInterface());
		CObjEventSimpleImpl<IContour, IContourEvents>::AdviseConnection(lpdispContour, 2);
	}

	lpdispPhotochromicTint = NULL;

	lpdispCoatings = NULL;
	lpdispMultifocal = NULL;
	lpdispProgressive = NULL;
	CProgressive *pProgressive = CProgressive::CreateObject();
	if(pProgressive){
		lpdispProgressive.AttachDispatch(pProgressive->GetInterface());
		CObjEventSimpleImpl<IProgressive, IProgressiveEvents>::AdviseConnection(lpdispProgressive, 6);
	}

	m_Prism = 0.;
	m_PrismAxis = 0;
	
	m_EllipticBlank = false;
	m_EllipticVertToHorizRatio = 1.;
	m_OptToGeomCenterDisplacementX = 0.;
	m_OptToGeomCenterDisplacementY = 0.;

	CContour * pDefContourObj = CContour::CreateObject();
	if(pDefContourObj) {
		pDefContour.AttachDispatch(pDefContourObj->GetInterface());
		if(pDefContour){
			const double Radius = 20.;
			for(int i = 0; i < 36; i++){
				double alfa = (double)i * M_PI / 18.;
				double x = Radius * cos(alfa);
				double y = Radius * sin(alfa);
				pDefContour->Add(x + m_CenterX, y + m_CenterY)->Release();
				lpdispContour->Add(x + m_CenterX, y + m_CenterY)->Release();
			}
		}
	}

	pLensEngine1 = NULL;
	CWSLensEngine * pLensEngineObj = CWSLensEngine::CreateObject();
	if(pLensEngineObj) {
		pLensEngine1.AttachDispatch(pLensEngineObj->GetInterface());
	}

	pLensEngine2 = NULL;

	m_PermittedCenterThickness = 1.5;
	m_PermittedEdgeThickness = .6;
	m_PermittedZeroThickness = 2.2;

	m_Weight = 0.;
	m_CenterThickness = 0.;
	m_MinEdgeThickness = 0.;
	m_MaxEdgeThickness = 0.;
	m_UVProtection = 0.;
	m_MinDiam = 0.;

	m_ErrorState = 0;

	m_segmHeight = 23.;
	m_multifocalType = wsSingleVision;

	m_engineUsageMask = 0;
	m_SuspendCalc = 0;
}

CLens::~CLens()
{
}

void CLens::Clear()
{
	CObjEventSimpleImpl<ITint, ITintEvents>::UnadviseConnection(/*lpdispTint,*/ 0);
	CObjEventSimpleImpl<IARCoating, IARCoatingEvents>::UnadviseConnection(/*lpdispARCoating,*/ 1);
	CObjEventSimpleImpl<IContour, IContourEvents>::UnadviseConnection(/*lpdispContour,*/ 2);
	CObjEventSimpleImpl<ITint, ITintEvents>::UnadviseConnection(/*lpdispPhotochromicTint,*/ 3);
	CObjEventSimpleImpl<ICoatings, ICoatingsEvents>::UnadviseConnection(/*lpdispCoatings,*/ 4);
	CObjEventSimpleImpl<IMultifocalSegmentCollection, IMultifocalSegmentCollectionEvents>::UnadviseConnection(/*lpdispCoatings,*/ 5);
	CObjEventSimpleImpl<IProgressive, IProgressiveEvents>::UnadviseConnection(/*lpdispCoatings,*/ 6);
}

//--------------------------- Side -------------------------------- Side --------------------------------
SideType CLens::GetSide() 
{
	return m_Side;
}
bool CLens::SetSide(SideType nNewValue) 
{
	if(!TestRangeSetError(nNewValue, 0, 1, 1, 2))
		return false;
	if(m_Side != nNewValue){
		m_Side = nNewValue;
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}
//--------------------------- Sph --------------------------------- Sph ---------------------------------
double CLens::GetSph() 
{
	return m_Sph;
}
bool CLens::SetSph(double newValue) 
{
#ifdef NOT_FOR_COMMERCIAL_USE
	if(!TestRangeSetError(newValue, -3., 3., 1, 2))
		return false;
#else
	if(!TestRangeSetError(newValue, -20., 20., 1, 2))
		return false;
#endif
	if(m_Sph != newValue){
		m_Sph = newValue;
//		DesignControl();
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}
//--------------------------- Cyl --------------------------------- Cyl ---------------------------------
double CLens::GetCyl() 
{
	return m_Cyl;
}
bool CLens::SetCyl(double newValue) 
{
#ifdef NOT_FOR_COMMERCIAL_USE
	if(!TestRangeSetError(newValue, -3., 3., 1, 2))
		return false;
#else
	if(!TestRangeSetError(newValue, -10., 10., 1, 2))
		return false;
#endif
	if(m_Cyl != newValue){
		m_Cyl = newValue;
//		DesignControl();
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}
//--------------------------- Add --------------------------------- Add ---------------------------------
double CLens::GetAddit() 
{
	return m_Add;
}
bool CLens::SetAddit(double newValue) 
{
	if(!TestRangeSetError(newValue, 0., 4., 1, 2))
		return false;
	if(m_Add != newValue){
		m_Add = newValue;
//		DesignControl();
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}
//--------------------------- Axis -------------------------------- Axis --------------------------------
long CLens::GetAxis() 
{
	return m_Axis;
}
bool CLens::SetAxis(long nNewValue) 
{
	if(!TestRangeSetError(nNewValue, 0, 180, 1, 2))
		return false;
	if(m_Axis != nNewValue){
		m_Axis = nNewValue;
//		DesignControl();
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}
//--------------------------- Kind of Material -------------------- Kind of Material---------------------
KindOfMaterial CLens::GetKindOfMaterial() 
{
	return m_KindOfMaterial;
}
bool CLens::SetKindOfMaterial(KindOfMaterial nNewValue) 
{
#ifdef NOT_FOR_COMMERCIAL_USE
	if(!TestRangeSetError(nNewValue, 0, 0, 1, 2))
		return false;
#else
	if(!TestRangeSetError(nNewValue, 0, 2, 1, 2))
		return false;
#endif
	if(m_KindOfMaterial != nNewValue){
		m_KindOfMaterial = nNewValue;
//		DesignControl();
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}
//--------------------------- Refractive Index -------------------- Refractive Index --------------------
double CLens::GetRefIndex() 
{
	return m_RefIndex;
}
bool CLens::SetRefIndex(double newValue) 
{
	if(!TestRangeSetError(newValue, 1., 2., 1, 5))
		return false;
	if(m_RefIndex != newValue){
		m_RefIndex = newValue;
//		DesignControl();
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}
//--------------------------- Density ----------------------------- Density -----------------------------
double CLens::GetDensity() 
{
	return m_Density;
}
bool CLens::SetDensity(double newValue) 
{
	if(!TestRangeSetError(newValue, .2, 10., 1, 6))
		return false;
	if(m_Density != newValue){
		m_Density = newValue;
//		DesignControl();
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}
//--------------------------- Design ------------------------------ Design ------------------------------
string CLens::GetDesign() 
{
	return m_Design;
}
bool CLens::SetDesign(LPCTSTR lpszNewValue) 
{
	SetError(0);
	bool bClassic = stricmp(wsClassic.c_str(), lpszNewValue) == 0;
	bool bAspheric = stricmp(wsAspheric.c_str(), lpszNewValue) == 0;
	bool bLenticular = stricmp(wsLenticular.c_str(), lpszNewValue) == 0;

	if(!bClassic && !bAspheric && !bLenticular) {
		SetError(12);
		return false;
	}
	
    if(stricmp(lpszNewValue, m_Design.c_str())){
		m_Design = lpszNewValue;
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
    }
	return true;
}
//--------------------------- CenterX ----------------------------- CenterX -----------------------------
double CLens::GetCenterX() 
{
	return m_CenterX;
}
bool CLens::SetCenterX(double newValue) 
{
	SetError(0);
	if(m_CenterX != newValue){
		m_CenterX = newValue;
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}
//--------------------------- CenterY ----------------------------- CenterY -----------------------------
double CLens::GetCenterY() 
{
	return m_CenterY;
}
bool CLens::SetCenterY(double newValue) 
{
	SetError(0);
	if(m_CenterY != newValue){
		m_CenterY = newValue;
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}
//--------------------------- Contour ----------------------------- Contour -----------------------------
IContour * CLens::GetContour() 
{
	if(lpdispContour)
		lpdispContour->AddRef();
	return lpdispContour;
}
bool CLens::SetContour(IContour * newValue) 
{
	SetError(0);
	if(lpdispContour){
		CObjEventSimpleImpl<IContour, IContourEvents>::UnadviseConnection(/*lpdispContour,*/ 2);
	}

	lpdispContour = newValue;

	if(newValue){
		CObjEventSimpleImpl<IContour, IContourEvents>::AdviseConnection(lpdispContour, 2);
	}

	InvalidateComputableParams();
	FireChange();
	SetModifiedFlag();
	return true;
}
//--------------------------- Tint -------------------------------- Tint --------------------------------
ITint * CLens::GetTint() 
{
	if(lpdispTint)
		lpdispTint->AddRef();
	return lpdispTint;
}
bool CLens::SetTint(ITint * newValue) 
{
	SetError(0);
	if(lpdispTint){
		CObjEventSimpleImpl<ITint, ITintEvents>::UnadviseConnection(/*lpdispTint,*/ 0);
	}

	lpdispTint = newValue;
	if(newValue){
		CObjEventSimpleImpl<ITint, ITintEvents>::AdviseConnection(lpdispTint, 0);
	}

	InvalidateComputableParams();
	FireChange();
	SetModifiedFlag();
	return true;
}
//--------------------------- AR Coating --------------------------- AR Coating --------------------------
IARCoating * CLens::GetARCoating() 
{
	if(lpdispARCoating)
		lpdispARCoating->AddRef();
	return lpdispARCoating;
}
bool CLens::SetARCoating(IARCoating * newValue) 
{
	SetError(0);
	if(lpdispARCoating){
		CObjEventSimpleImpl<IARCoating, IARCoatingEvents>::UnadviseConnection(/*lpdispARCoating,*/ 1);
	}

	lpdispARCoating = newValue;
	if(newValue){
		CObjEventSimpleImpl<IARCoating, IARCoatingEvents>::AdviseConnection(lpdispARCoating, 1);
	}

	InvalidateComputableParams();
	FireChange();
	SetModifiedFlag();
	return true;
}
//--------------------------- Engine1 ---------------------------- Engine1 ----------------------------
ILensEngine * CLens::GetEngine1() 
{
	return (pLensEngine1)?	pLensEngine1->AddRef(),
							pLensEngine1
							:
							NULL;
}

bool CLens::SetEngine1(ILensEngine * newValue) 
{
	SetError(0);
	pLensEngine1 = newValue;

	InvalidateComputableParams();
	FireChange();
	SetModifiedFlag();
	return true;
}

//--------------------------- Engine2 ---------------------------- Engine2 ----------------------------
ILensEngine * CLens::GetEngine2() 
{
	return (pLensEngine2)?	pLensEngine2->AddRef(),
							pLensEngine2
							:
							NULL;
}

bool CLens::SetEngine2(ILensEngine * newValue) 
{
	SetError(0);
	pLensEngine2 = newValue;

	InvalidateComputableParams();
	FireChange();
	SetModifiedFlag();
	return true;
}

//--------------------- EngineUsageMask ------------------- EngineUsageMask ----------------------------
long CLens::GetEngineUsageMask()
{
	return m_engineUsageMask;
}
bool CLens::SetEngineUsageMask(long newValue)
{
	SetError(0);
	m_engineUsageMask = newValue;

	InvalidateComputableParams();
	FireChange();
	SetModifiedFlag();
	return true;
}
//--------------------------- Diameter ---------------------------- Diameter ----------------------------
bool CLens::SetDiameter(long nNewValue) 
{
	SetError(0);
	if(nNewValue > 0){
		if(m_Diameter != nNewValue){
			m_Diameter = nNewValue;
			InvalidateComputableParams();
			FireChange();
			SetModifiedFlag();
		}
	}
	else
	{
		SetError(2);
		return false;
	}
	return true;
}
long CLens::GetDiameter() 
{
	return m_Diameter;
}
//--------------------------- Permitted Center Thickness ---------- Permitted Center Thickness ----------
double CLens::GetPermittedCenterThickness() 
{
	return m_PermittedCenterThickness;
}
bool CLens::SetPermittedCenterThickness(double newValue) 
{
	SetError(0);
	if(newValue > 0.){
		if(m_PermittedCenterThickness != newValue){
			m_PermittedCenterThickness = newValue;
			InvalidateComputableParams();
			FireChange();
			SetModifiedFlag();
		}
	}
	else
	{
		SetError(2);
		return false;
	}
	return true;
}
//--------------------------- Permitted Edge Thickness ------------ Permitted Edge Thickness ------------
double CLens::GetPermittedEdgeThickness() 
{
	return m_PermittedEdgeThickness;
}
bool CLens::SetPermittedEdgeThickness(double newValue) 
{
	SetError(0);
	if(newValue > 0.){
		if(m_PermittedEdgeThickness != newValue){
			m_PermittedEdgeThickness = newValue;
			InvalidateComputableParams();
			FireChange();
			SetModifiedFlag();
		}
	}
	else
	{
		SetError(2);
		return false;
	}
	return true;
}
//--------------------------- Permitted Zero Thickness ------------ Permitted Zero Thickness ------------
double CLens::GetPermittedZeroThickness() 
{
	return m_PermittedZeroThickness;
}
bool CLens::SetPermittedZeroThickness(double newValue) 
{
	SetError(0);
	if(newValue > 0.){
		if(m_PermittedZeroThickness != newValue){
			m_PermittedZeroThickness = newValue;
			InvalidateComputableParams();
			FireChange();
			SetModifiedFlag();
		}
	}
	else
	{
		SetError(2);
		return false;
	}
	return true;
}
//--------------------------- Weight ------------------------------ Weight ------------------------------
double CLens::GetWeight() 
{
	SetError(0);
	if((m_SuspendCalc & wsSuspendWeightCalc) > 0) return m_Weight;

	if(m_NeedToComputeWeight){
		ILensEngine * pLensEngine = NULL;
		int NumEng = 0;
		if(m_engineUsageMask & wsWeightEngine2){
			pLensEngine = pLensEngine2;
			NumEng = 2;
		}
		else {
			pLensEngine = pLensEngine1;
			NumEng = 1;
		}

		if(pLensEngine){
			try {
				unsigned long SupportedMethods;
				SupportedMethods = pLensEngine->GetSupportedMethods();
				if(!(SupportedMethods & wsWeightSupported)) {
					SetError(9 + NumEng);
					m_ErrorString = strError_MethodNotSupported;
					return 0.;
				}

				bool bResult = pLensEngine->CalcWeight(GetInterface(), &m_Weight);

				if(!bResult){
					SetError(9 + NumEng);
					m_ErrorString = pLensEngine->GetLastErrorMessage();
					return 0.;
				}
			}
			catch(...)
			{	
				SetError(9 + NumEng);
				m_ErrorString = strError_CalcWeight;
				return 0.;
			}
		}
		else {
			SetError(9 + NumEng);
			m_ErrorString = strError_NoEngine;
			return 0.;
		}
		m_NeedToComputeWeight = false;
	}

	return m_Weight;
}

bool CLens::SetWeight(double newValue) 
{
	if((m_SuspendCalc & wsSuspendWeightCalc) > 0){
		SetError(0);
		if(newValue > 0.)
		{
			if(m_Weight != newValue)
			{
				m_Weight = newValue;
				FireChange();
				SetModifiedFlag();
			}
			return true;
		}
		else
		{
			SetError(2);
			m_ErrorString = strError_OutOfRange;
			return false;
		}
	}
	SetError(13);
	m_ErrorString = strError_NotSuspended;
	return false;
}
//--------------------------- Center Thickness -------------------- Center Thickness --------------------
double CLens::GetCenterThickness() 
{
	SetError(0);

	if((m_SuspendCalc & wsSuspendThicknessCalc) > 0) return m_CenterThickness;

	CalcThickness(	&m_CenterThickness,
					&m_MinEdgeThickness,
					&m_MaxEdgeThickness);

	return m_CenterThickness;
}

bool CLens::SetCenterThickness(double newValue) 
{
	if((m_SuspendCalc & wsSuspendThicknessCalc) > 0){
		SetError(0);
		if(newValue > 0.)
		{
			if(m_CenterThickness  != newValue)
			{
				m_CenterThickness  = newValue;
				FireChange();
				SetModifiedFlag();
			}
			return true;
		}
		else
		{
			SetError(2);
			m_ErrorString = strError_OutOfRange;
			return false;
		}
	}
	SetError(13);
	m_ErrorString = strError_NotSuspended;
	return false;
}
//--------------------------- Minimal Edge Thickness -------------- Minimal Edge Thickness --------------
double CLens::GetMinEdgeThickness() 
{
	SetError(0);

	if((m_SuspendCalc & wsSuspendThicknessCalc) > 0) return m_MinEdgeThickness;

	CalcThickness(	&m_CenterThickness,
					&m_MinEdgeThickness,
					&m_MaxEdgeThickness);

	return m_MinEdgeThickness;
}

bool CLens::SetMinEdgeThickness(double newValue) 
{
	if((m_SuspendCalc & wsSuspendThicknessCalc) > 0){
		SetError(0);
		if(newValue > 0.)
		{
			if(m_MinEdgeThickness != newValue)
			{
				m_MinEdgeThickness = newValue;
				FireChange();
				SetModifiedFlag();
			}
			return true;
		}
		else
		{
			SetError(2);
			m_ErrorString = strError_OutOfRange;
			return false;
		}
	}
	SetError(13);
	m_ErrorString = strError_NotSuspended;
	return false;
}
//--------------------------- Maximum Edge Thickness -------------- Maximum Edge Thickness --------------
double CLens::GetMaxEdgeThickness() 
{
	SetError(0);

	if((m_SuspendCalc & wsSuspendThicknessCalc) > 0) return m_MaxEdgeThickness;

	CalcThickness(	&m_CenterThickness,
					&m_MinEdgeThickness,
					&m_MaxEdgeThickness);

	return m_MaxEdgeThickness;
}

bool CLens::SetMaxEdgeThickness(double newValue) 
{
	if((m_SuspendCalc & wsSuspendThicknessCalc) > 0){
		SetError(0);
		if(newValue > 0.)
		{
			if(m_MaxEdgeThickness != newValue)
			{
				m_MaxEdgeThickness = newValue;
				FireChange();
				SetModifiedFlag();
			}
			return true;
		}
		else
		{
			SetError(2);
			m_ErrorString = strError_OutOfRange;
			return false;
		}
	}
	SetError(13);
	m_ErrorString = strError_NotSuspended;
	return false;
}
//--------------------------- Minimal Diameter -------------------- Minimal Diameter --------------------
double CLens::GetMinDiam() 
{
	SetError(0);

	if((m_SuspendCalc & wsSuspendMinDiameterCalc) > 0) return m_MinDiam;

	CalcDiam(&m_MinDiam);

	return m_MinDiam;
}

bool CLens::SetMinDiam(double newValue) 
{
	if((m_SuspendCalc & wsSuspendMinDiameterCalc) > 0){
		SetError(0);
		if(newValue > 0.)
		{
			if(m_MinDiam != newValue)
			{
				m_MinDiam = newValue;
				FireChange();
				SetModifiedFlag();
			}
			return true;
		}
		else
		{
			SetError(2);
			m_ErrorString = strError_OutOfRange;
			return false;
		}
	}
	SetError(13);
	m_ErrorString = strError_NotSuspended;
	return false;
}
//--------------------------- Multifocal type -------------------- Multifocal type ----------------------
MultifocalType CLens::GetMultifocalType()
{
	return m_multifocalType;
}
bool CLens::SetMultifocalType(MultifocalType newValue)
{
	if(!TestRangeSetError(newValue, -1, 26, 1, 2))
		return false;

	m_multifocalType = newValue;

	InvalidateComputableParams();
	FireChange();
	SetModifiedFlag();
	return true;
}
//--------------------------- Segment height -------------------- Segment height ------------------------
double CLens::GetSegmHeight()
{
	return m_segmHeight;
}
bool CLens::SetSegmHeight(double newValue)
{
	if(!TestRangeSetError(newValue, 5., 35., 1, 2))
		return false;

	m_segmHeight = newValue;

	FireChange();
	SetModifiedFlag();
	return true;
}
//--------------------------- EDP code -------------------- EDP code ------------------------------------
string CLens::GetEDPCode()
{
	return m_EDPCode;
}
bool CLens::SetEDPCode(LPCTSTR newValue)
{
	SetError(0);
	m_EDPCode = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}
//--------------------------- Clone -------------------- Clone ------------------------------------------
ILens * CLens::Clone() 
{
	CLens *pNewObject = CLens::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in)) {
                pNewObject->SetEngine2(pLensEngine2);
				return pNewObject->GetInterface();
            }
		pNewObject->Release();
	}
	return NULL;
}
//--------------------------- Error State ------------------------- Error State -------------------------
//	Error codes:
//	0 - all data correct, the lens calculated successfully.
//	1 -	a lens with given parameters can't be built by the 
//			Lens component due to physical, geometrical and
//			optical restrictions; (eg., current Design is not
//			supported for the given combination of other parameters).
//	2 -	Sph, Cyl, Axis or Addit values beyond valid range;
//	3 -	Contour not set, or insufficient number of ContourPoints, 
//	4 -	OpticalCenter not set or invalid
//	5 -	RefIndex not set or beyond valid range
//	6 -	Density not set or beyond valid range
//	7 -	Color settings invalid
//	8 -	ARCoating settings invalid
//	9 - Memory allocate invalid
// 10 - Engine1 reported an error
// 11 - Engine2 reported an error
// 12 � Invalid design GUID
// 13 � Can not set property when caclulation is not suspended

long CLens::GetErrorState() 
{
	return m_ErrorState;
}

void CLens::SetError(int nError)
{
	if(nError > 0){
		m_Weight = 0.;
		m_CenterThickness = 0.;
		m_MinEdgeThickness = 0.;
		m_MaxEdgeThickness = 0.;
		m_UVProtection = 0;
		m_MinDiam = 0.;
	}
	m_ErrorState = nError;
	m_ErrorString.clear();
}

void CLens::OnChildChanged() 
{
	SetError(0);
	InvalidateComputableParams();
	FireChange();
	SetModifiedFlag();
}
/*
int CLens::DesignControl()
{
	int iForm, Quit;
	
	if ( m_Design == 0 )
		return 0;

	double Shell2Par[160];
	DefineShell2Par(Shell2Par);
	
	iForm = as_len_mark(m_Design, m_KindOfMaterial, m_RefIndex, m_Sph);
	while(1){
		Quit = set_par ( iForm, m_Sph, m_Cyl,
						-m_Axis*M_PI/180., 40, 1, 1, 1, 1);
		if ( Quit == 0 && iForm == 0 ) { iForm = 1 ; continue ; }
		break ;
	}
	
	if(Quit == 0){
		// bad thing
		SetError(1);
		SetDesign(0);
		return ( -1 ) ;
	}

	return ( 0 ) ;
}
*/

bool CLens::CalcThickness(double *ResultCenterThickness, double *ResultMinEdgeThickness,
							double *ResultMaxEdgeThickness)
{
	SetError(0);

	if(m_NeedToComputeThicknesses){
		ILensEngine *pLensEngine = NULL;
		int NumEng = 0;
		if(m_engineUsageMask & wsThicknessEngine2){
			pLensEngine = pLensEngine2;
			NumEng = 2;
		}
		else {
			pLensEngine = pLensEngine1;
			NumEng = 1;
		}

		if(pLensEngine){
			try {
				unsigned long SupportedMethods = pLensEngine->GetSupportedMethods();
				if(!(SupportedMethods & wsThicknessSupported)) {
					SetError(9 + NumEng);
					m_ErrorString = strError_MethodNotSupported;
					return false;
				}

				bool bResult = pLensEngine->CalcThickness(	GetInterface(),
													ResultCenterThickness,
													ResultMinEdgeThickness,
													ResultMaxEdgeThickness);
				if(!bResult){
					SetError(9 + NumEng);
					m_ErrorString = pLensEngine->GetLastErrorMessage();
					return false;
				}
			}
			catch(...)
			{	
				SetError(9 + NumEng);
				m_ErrorString = strError_CalcThicknesses;
				return false;
			}
		}
		else {
			SetError(9 + NumEng);
			m_ErrorString = strError_NoEngine;
			return false;
		}
		m_NeedToComputeThicknesses = false;
	}
	return true;
}

void CSpecEllipseBlank::UpdateInternalData()
{
	if(SizeA > Diff)
		SizeB = SizeA - Diff;
	else
		SizeB = 0;

	if(SizeA > 0)
		SizeASqRec = 1. / (SizeA * SizeA);
	else
		SizeASqRec = 0.;

	if(SizeB > 0)
		SizeBSqRec = 1. / (SizeB * SizeB);
	else
		SizeBSqRec = 0.;
}

double CSpecEllipseBlank::FindSpecEllipseSize(double x, double y)
{
	double BigDiam, SmallDiam = SizeA;
	bool CurTry = TrySpecEllipseSize(x, y);
	if(CurTry)
		return SizeA; 
	while(!CurTry)//����������� �����, ���� ������ �� ������ ��������� �������� �����
	{
		SmallDiam = SizeA;
		SizeA = 2 * SizeA;
		BigDiam = SizeA;

		UpdateInternalData();
		CurTry = TrySpecEllipseSize(x, y);
	}
	while(BigDiam - SmallDiam > 1e-8)
	{
		SizeA = (SmallDiam + BigDiam) * 0.5;
		UpdateInternalData();
		CurTry = TrySpecEllipseSize(x, y);
		if(CurTry)
		{
			BigDiam = SizeA;
		}
		else
		{
			SmallDiam = SizeA;
		}
	}
	SizeA = BigDiam;
	return SizeA;
}

bool CSpecEllipseBlank::TrySpecEllipseSize(double x, double y)
{
	double Res = x * x * SizeASqRec + y * y * SizeBSqRec;
	if(Res <= 1.)
		return true;
	else
		return false;
}

bool CLens::CalcDiam(double *MinDiam)
{
	if(m_NeedToComputeDiam)
	{
		bool NewEllipseProcessing = false;
		double NewEllipseHorizMinusVert = 0.;

		if(!lpdispContour) {
			*MinDiam = 0;
			SetError(3);
			return false;
		}
		unsigned long ContourCount = lpdispContour->GetCount();

		if(ContourCount < 3 || ContourCount > 1000 /*|| xContour == NULL || yContour == NULL*/){
			*MinDiam = 0;
			SetError(3);
			return false;
		}
		bool fProgressiveDataFound = false;
		double YCoeff, BlankCenterX, BlankCenterY;
		double	HorizDiff, VertDiff;
		if(m_multifocalType == wsProgressive)
		{
			if(lpdispProgressive)
			{
				HorizDiff = -lpdispProgressive->GetFarVisionCenterX();
				VertDiff = -lpdispProgressive->GetFarVisionCenterY() +
					0.5 * lpdispProgressive->GetFarVisionMarkerDiam();

				fProgressiveDataFound = true;
			}
		}	
		if(!fProgressiveDataFound)
		{
			HorizDiff = m_OptToGeomCenterDisplacementX;
			VertDiff = m_OptToGeomCenterDisplacementY;
		}
		BlankCenterX = m_CenterX + HorizDiff;
		BlankCenterY = m_CenterY + VertDiff;
		if(m_EllipticBlank)
		{
			if(m_EllipticVertToHorizRatio > 10.)
			{
				NewEllipseProcessing = true;
				NewEllipseHorizMinusVert = (-m_EllipticVertToHorizRatio + 100.) * 0.5;
			}
			else
				YCoeff = 1. / m_EllipticVertToHorizRatio;
		}
		else
			YCoeff = 1.;
		double MaxR;
		unsigned long i;
		double x, y;
		CSpecEllipseBlank SpecEllipseBlank(NewEllipseHorizMinusVert);

		for(i = 0; i < ContourCount; i++)
		{
			lpdispContour->GetPoint(i, &x, &y);
			double xOffs = x - BlankCenterX, yOffs = y - BlankCenterY, CurR;
			if(m_EllipticBlank && NewEllipseProcessing)
				CurR = SpecEllipseBlank.FindSpecEllipseSize(xOffs, yOffs);
			else
				CurR = _hypot(xOffs, yOffs * YCoeff);

			if(i == 0 || CurR > MaxR)
				MaxR = CurR;
		}
		*MinDiam = MaxR * 2.;
		m_NeedToComputeDiam = false;
	}
	return true;
}
//--------------------------- Suspend Calc -------------------- Suspend Calc -----------------------------------
long CLens::GetSuspendCalc()
{
	return m_SuspendCalc;
}
bool CLens::SetSuspendCalc(long newValue)
{
	m_SuspendCalc = newValue;

	if((m_SuspendCalc & wsSuspendWeightCalc) == 0) m_NeedToComputeWeight = true;
	if((m_SuspendCalc & wsSuspendMinDiameterCalc) == 0) m_NeedToComputeDiam = true;
	if((m_SuspendCalc & wsSuspendThicknessCalc) == 0) m_NeedToComputeThicknesses = true;
	if((m_SuspendCalc & wsSuspendUVProtectionCalc) == 0) m_NeedToComputeUVProtection = true;

	FireChange();
	SetModifiedFlag();
	return true;
}

//--------------------------- IsPhotochromic -------------------- IsPhotochromic --------------------
bool CLens::GetIsPhotochromic() 
{
	return m_IsPhotochromic;
}
bool CLens::SetIsPhotochromic(bool bNewValue) 
{
	SetError(0);
	m_IsPhotochromic = bNewValue;

	InvalidateComputableParams();
	FireChange();
	SetModifiedFlag();
	return true;
}
//--------------------- PhotochromicTint ------------------ PhotochromicTint --------------------------------
ITint * CLens::GetPhotochromicTint() 
{
	if(lpdispPhotochromicTint)
		lpdispPhotochromicTint->AddRef();
	return lpdispPhotochromicTint;
}
bool CLens::SetPhotochromicTint(ITint * newValue) 
{
	SetError(0);
	if(lpdispPhotochromicTint){
		CObjEventSimpleImpl<ITint, ITintEvents>::UnadviseConnection(/*lpdispPhotochromicTint,*/ 3);
	}

	lpdispPhotochromicTint = newValue;
	if(newValue){
		CObjEventSimpleImpl<ITint, ITintEvents>::AdviseConnection(lpdispPhotochromicTint, 3);
	}

	FireChange();
	SetModifiedFlag();
	return true;
}
//--------------------------- UVProtection -------------------- UVProtection --------------------
double CLens::GetUVProtection() 
{
	SetError(0);

	if((m_SuspendCalc & wsSuspendUVProtectionCalc) > 0) return m_UVProtection;

	m_UVProtection = CalcUVProtection();

	return m_UVProtection;
}

bool CLens::SetUVProtection(double newValue) 
{
	if((m_SuspendCalc & wsSuspendUVProtectionCalc) > 0){
		SetError(0);
		if(newValue >= 0. && newValue <= 100.)
		{
			if(m_UVProtection != newValue)
			{
				m_UVProtection = newValue;
				FireChange();
				SetModifiedFlag();
			}
			return true;
		}
		else
		{
			SetError(2);
			m_ErrorString = strError_OutOfRange;
			return false;
		}
	}
	SetError(13);
	m_ErrorString = strError_NotSuspended;
	return false;
}

double CLens::CalcUVProtection()
{

	SetError(0);
	double ResultUVProtection;
	if(m_NeedToComputeUVProtection){
		ILensEngine *pLensEngine;
		int NumEng;
		if(m_engineUsageMask & wsUVProtectionEngine2){
			pLensEngine = pLensEngine2;
			NumEng = 2;
		}
		else {
			pLensEngine = pLensEngine1;
			NumEng = 1;
		}

		if(pLensEngine){
			try {
				unsigned long SupportedMethods = pLensEngine->GetSupportedMethods();
				if(!(SupportedMethods & wsUVProtectionSupported)) {
					SetError(9 + NumEng);
					m_ErrorString = strError_MethodNotSupported;
					return 0.;
				}

				bool bResult = pLensEngine->CalcUVProtection(GetInterface(), &ResultUVProtection);

				if(!bResult){
					SetError(9 + NumEng);
					m_ErrorString = pLensEngine->GetLastErrorMessage();
					return 0.;
				}
			}
			catch(...)
			{	
				SetError(9 + NumEng);
				m_ErrorString = strError_CalcUVProtection;
				return 0.;
			}
		}
		else {
			SetError(9 + NumEng);
			m_ErrorString = strError_NoEngine;
			return 0.;
		}
		m_NeedToComputeUVProtection = false;
		return ResultUVProtection;
	}
	else
	{
		return m_UVProtection;
	}
}

ICoatings * CLens::GetCoatings() 
{
	if(lpdispCoatings)
		lpdispCoatings->AddRef();
	return lpdispCoatings;
}

bool CLens::SetCoatings(ICoatings * newValue) 
{
	SetError(0);
	if(lpdispCoatings){
		CObjEventSimpleImpl<ICoatings, ICoatingsEvents>::UnadviseConnection(/*lpdispCoatings,*/ 4);
	}

	lpdispCoatings = newValue;
	if(newValue){
		CObjEventSimpleImpl<ICoatings, ICoatingsEvents>::AdviseConnection(lpdispCoatings, 4);
	}

	InvalidateComputableParams();
	FireChange();
	SetModifiedFlag();
	return true;
}

double CLens::GetPhotochromicCarIndex() 
{
	return m_PhotochromicCarIndex;
}

bool CLens::SetPhotochromicCarIndex(double newValue) 
{
	if(!TestRangeSetError(newValue, 0., 1., 1, 1))
		return false;

	m_PhotochromicCarIndex = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

double CLens::GetPhotochromicHeatIndex() 
{
	return m_PhotochromicHeatIndex;
}

bool CLens::SetPhotochromicHeatIndex(double newValue) 
{
	if(!TestRangeSetError(newValue, 0., 1., 1, 1))
		return false;

	m_PhotochromicHeatIndex = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

IMultifocalSegmentCollection * CLens::GetMultifocal() 
{
	if(lpdispMultifocal)
		lpdispMultifocal->AddRef();
	return lpdispMultifocal;

}

bool CLens::SetMultifocal(IMultifocalSegmentCollection * newValue) 
{
	SetError(0);
	if(lpdispMultifocal){
		CObjEventSimpleImpl<IMultifocalSegmentCollection, IMultifocalSegmentCollectionEvents>::UnadviseConnection(/*lpdispCoatings,*/ 5);
	}

	lpdispMultifocal = newValue;
	if(newValue){
		CObjEventSimpleImpl<IMultifocalSegmentCollection, IMultifocalSegmentCollectionEvents>::AdviseConnection(lpdispMultifocal, 5);
	}

	FireChange();
	SetModifiedFlag();
	return true;
}

IProgressive * CLens::GetProgressive() 
{
	if(lpdispProgressive)
		lpdispProgressive->AddRef();
	return lpdispProgressive;
}

bool CLens::SetProgressive(IProgressive * newValue) 
{
	SetError(0);
	if(lpdispProgressive)
	{
		CObjEventSimpleImpl<IProgressive, IProgressiveEvents>::UnadviseConnection(/*lpdispCoatings,*/ 6);
	}

	bool bRelease = false;

	if(!newValue)
	{
		CProgressive *pProgressive = CProgressive::CreateObject();
		if(pProgressive) {
			newValue = pProgressive->GetInterface();
			bRelease = true;
		}
	}

	lpdispProgressive = newValue;

	if(newValue)
	{
		CObjEventSimpleImpl<IProgressive, IProgressiveEvents>::AdviseConnection(lpdispProgressive, 6);
		if(bRelease)
			newValue->Release();
	}

	FireChange();
	SetModifiedFlag();
	return true;
}

double CLens::GetPrism() 
{
	return m_Prism;
}

bool CLens::SetPrism(double newValue) 
{
	if(!TestRangeSetError(newValue, 0., 25., 1, 2))
		return false;
	if(m_Prism != newValue){
		m_Prism = newValue;
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}

long CLens::GetPrismAxis() 
{
	return m_PrismAxis;
}

bool CLens::SetPrismAxis(long nNewValue) 
{
	if(!TestRangeSetError(nNewValue, 0., 360., 1, 2))
		return false;
	if(m_PrismAxis != nNewValue){
		m_PrismAxis = nNewValue;
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}

bool CLens::GetEllipticBlank() 
{
	return m_EllipticBlank;
}

bool CLens::SetEllipticBlank(bool bNewValue) 
{
	SetError(0);
	if(m_EllipticBlank != bNewValue){
		m_EllipticBlank = bNewValue;
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}

double CLens::GetEllipticVertToHorizRatio() 
{
	return m_EllipticVertToHorizRatio;
}

bool CLens::SetEllipticVertToHorizRatio(double newValue) 
{
	if(!TestRangeSetError(newValue, 0., 150., 1, 2))
		return false;
	if(m_EllipticVertToHorizRatio != newValue){
		m_EllipticVertToHorizRatio = newValue;
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}

double CLens::GetOptToGeomCenterDisplacementX() 
{
	return m_OptToGeomCenterDisplacementX;
}

bool CLens::SetOptToGeomCenterDisplacementX(double newValue) 
{
	if(!TestRangeSetError(newValue, -30., 30., 1, 2))
		return false;
	if(m_OptToGeomCenterDisplacementX != newValue){
		m_OptToGeomCenterDisplacementX = newValue;
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}

double CLens::GetOptToGeomCenterDisplacementY() 
{
	return m_OptToGeomCenterDisplacementY;
}

bool CLens::SetOptToGeomCenterDisplacementY(double newValue) 
{
	if(!TestRangeSetError(newValue, -30., 30., 1, 2))
		return false;
	if(m_OptToGeomCenterDisplacementY != newValue){
		m_OptToGeomCenterDisplacementY = newValue;
		InvalidateComputableParams();
		FireChange();
		SetModifiedFlag();
	}
	return true;
}

void CLens::InvalidateComputableParams()
{
	m_NeedToComputeWeight = true;
	m_NeedToComputeDiam = true;
	m_NeedToComputeThicknesses = true;
	m_NeedToComputeUVProtection = true;
}

bool CLens::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	string DefDesign(wsClassic);

	if(pPX->PX_Double("Sph", m_Sph, 0.) == false)
		return false;
	if(pPX->PX_Double("Cyl", m_Cyl, 0.) == false)
		return false;
	if(pPX->PX_Double("Addit", m_Add, 0.) == false)
		return false;
	if(pPX->PX_Long("Axis", m_Axis, 0) == false)
		return false;
	if(pPX->PX_Long("KindOfMaterial", (long&)m_KindOfMaterial, 0) == false)
		return false;
	if(pPX->PX_Double("RefIndex", m_RefIndex, 1.501) == false)
		return false;
	if(pPX->PX_Double("Density", m_Density, 1.32) == false)
		return false;
	if(pPX->PX_String("Design", m_Design, DefDesign) == false)
		return false;
	if(pPX->PX_Long("Diameter", m_Diameter, 70) == false)
		return false;
	if(pPX->PX_Double("CenterX", m_CenterX, m_Diameter/2. + 5.) == false)
		return false;
	if(pPX->PX_Double("CenterY", m_CenterY, m_Diameter/2.) == false)
		return false;
	if(pPX->PX_Double("PermittedCenterThickness", m_PermittedCenterThickness, 1.5) == false)
		return false;
	if(pPX->PX_Double("PermittedEdgeThickness", m_PermittedEdgeThickness, .6) == false)
		return false;
	if(pPX->PX_Double("PermittedZeroThickness", m_PermittedZeroThickness, 2.2) == false)
		return false;
	if(pPX->PX_Long("MultifocalType", (long&)m_multifocalType, 0) == false)
		return false;
	if(pPX->PX_Double("SegmHeight", m_segmHeight, 23.) == false)
		return false;
	if(pPX->PX_Long("Side", (long&)m_Side, 0) == false)
		return false;
	if(pPX->PX_String("EDPCode", m_EDPCode, "") == false)
		return false;
	if(pPX->PX_Long("EngineUsageMask", m_engineUsageMask, 0) == false)
		return false;
	if(pPX->PX_Long("SuspendCalcMask", m_SuspendCalc, 0) == false)
		return false;

	if(pPX->PX_Double("Weight", m_Weight, 0.) == false)
		return false;
	if(pPX->PX_Double("CenterThickness", m_CenterThickness, 0.) == false)
		return false;
	if(pPX->PX_Double("MinEdgeThickness", m_MinEdgeThickness, 0.) == false)
		return false;
	if(pPX->PX_Double("MaxEdgeThickness", m_MaxEdgeThickness, 0.) == false)
		return false;
	if(pPX->PX_Double("UVProtection", m_UVProtection, 0.) == false)
		return false;
	if(pPX->PX_Double("MinDiameter", m_MinDiam, 0.) == false)
		return false;

	if(pPX->PX_Bool("NeedToComputeWeight", m_NeedToComputeWeight, true) == false)
		return false;
	if(pPX->PX_Bool("NeedToComputeDiam", m_NeedToComputeDiam, true) == false)
		return false;
	if(pPX->PX_Bool("NeedToComputeThicknesses", m_NeedToComputeThicknesses, true) == false)
		return false;
	if(pPX->PX_Bool("NeedToComputeUVProtection", m_NeedToComputeUVProtection, true) == false)
		return false;
	if(pPX->PX_Bool("IsPhotochromic", m_IsPhotochromic, false) == false)
		return false;

	if(pPX->PX_Double("Prism", m_Prism, 0.) == false)
		return false;
	if(pPX->PX_Long("PrismAxis", m_PrismAxis, 0) == false)
		return false;

	if(pPX->PX_Bool("EllipticBlank", m_EllipticBlank, false) == false)
		return false;
	if(pPX->PX_Double("EllipticVertToHorizRatio", m_EllipticVertToHorizRatio, 1.) == false)
		return false;
	if(pPX->PX_Double("OptToGeomCenterDisplacementX", m_OptToGeomCenterDisplacementX, 0.) == false)
		return false;
	if(pPX->PX_Double("OptToGeomCenterDisplacementY", m_OptToGeomCenterDisplacementY, 0.) == false)
		return false;

	CObjEventSimpleImpl<ITint, ITintEvents>::UnadviseConnection(/*lpdispTint,*/ 0);
	CTint *pDefTint = CTint::CreateObject();
	CObjPtr<ITint> pDefITint;
	if(pDefTint)
		pDefITint.AttachDispatch(pDefTint->GetInterface());
	if(PX_WSLObject<CTint, ITint>(pPX, "Tint", lpdispTint, pDefITint) == false)
		return false;
	CObjEventSimpleImpl<ITint, ITintEvents>::AdviseConnection(lpdispTint, 0);

	CObjEventSimpleImpl<IARCoating, IARCoatingEvents>::UnadviseConnection(/*lpdispARCoating,*/ 1);
	CARCoating *pDefARCoating = CARCoating::CreateObject();
	CObjPtr<IARCoating> pDefIARCoating;
	if(pDefARCoating)
		pDefIARCoating.AttachDispatch(pDefARCoating->GetInterface());
	if(PX_WSLObject<CARCoating, IARCoating>(pPX, "ARCoating", lpdispARCoating, pDefIARCoating) == false)
		return false;
	CObjEventSimpleImpl<IARCoating, IARCoatingEvents>::AdviseConnection(lpdispARCoating, 1);

	CObjEventSimpleImpl<IContour, IContourEvents>::UnadviseConnection(/*lpdispContour,*/ 2);
	if(PX_WSLObject<CContour, IContour>(pPX, "Contour", lpdispContour, pDefContour) == false)
		return false;
	CObjEventSimpleImpl<IContour, IContourEvents>::AdviseConnection(lpdispContour, 2);

	CObjEventSimpleImpl<ITint, ITintEvents>::UnadviseConnection(/*lpdispPhotochromicTint,*/ 3);
	if(PX_WSLObject<CTint, ITint>(pPX, "PhotochromicTint", lpdispPhotochromicTint, NULL) == false)
		return false;
	CObjEventSimpleImpl<ITint, ITintEvents>::AdviseConnection(lpdispPhotochromicTint, 3);

	CObjEventSimpleImpl<ICoatings, ICoatingsEvents>::UnadviseConnection(/*lpdispCoatings,*/ 4);
	if(PX_WSLObject<CCoatings, ICoatings>(pPX, "Coatings", lpdispCoatings, NULL) == false)
		return false;
	CObjEventSimpleImpl<ICoatings, ICoatingsEvents>::AdviseConnection(lpdispCoatings, 4);

	CObjEventSimpleImpl<IMultifocalSegmentCollection, IMultifocalSegmentCollectionEvents>::UnadviseConnection(/*lpdispMultifocal,*/ 5);
	if(PX_WSLObject<CMultifocalSegmentCollection, IMultifocalSegmentCollection>(pPX, "Multifocal", lpdispMultifocal, NULL) == false)
		return false;
	CObjEventSimpleImpl<IMultifocalSegmentCollection, IMultifocalSegmentCollectionEvents>::AdviseConnection(lpdispMultifocal, 5);

	if(pPX->PX_Double("PhotochromicCarIndex", m_PhotochromicCarIndex, 0.2) == false)
		return false;
	if(pPX->PX_Double("PhotochromicHeatIndex", m_PhotochromicHeatIndex, 0.8) == false)
		return false;

	CObjEventSimpleImpl<IProgressive, IProgressiveEvents>::UnadviseConnection(/*lpdispProgressive,*/ 6);
	CProgressive *pDefProgressive = CProgressive::CreateObject();
	CObjPtr<IProgressive> pDefIProgressive;
	if(pDefProgressive)
		pDefIProgressive.AttachDispatch(pDefProgressive->GetInterface());
	if(PX_WSLObject<CProgressive, IProgressive>(pPX, "Progressive", lpdispProgressive, pDefIProgressive) == false)
		return false;
	CObjEventSimpleImpl<IProgressive, IProgressiveEvents>::AdviseConnection(lpdispProgressive, 6);

	// Engines
	CWSLensEngine *pDefEngine1 = CWSLensEngine::CreateObject();
	CObjPtr<ILensEngine> pDefIEngine1;
	if(pDefEngine1)
		pDefIEngine1.AttachDispatch(pDefEngine1->GetInterface());
	if(PX_WSLObject<CWSLensEngine, ILensEngine>(pPX, "Engine1", pLensEngine1, pDefIEngine1) == false)
		return false;

	if(PX_WSLObject<CWSLensEngine, ILensEngine>(pPX, "Engine2", pLensEngine2, NULL) == false)
		return false;

	if(pPX->IsLoading())
		FireChange();

	return true;
}
