#ifndef __CONTOURS_H__
#define __CONTOURS_H__

struct RimContour{
	int N_points_contour ;
	float rcl ;
	double *ContourX ;
	double *ContourY ;
};

#endif