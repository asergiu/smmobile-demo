

#if !defined(__objecT_h__)
#define __objecT_h__

//#if _MSC_VER >= 1000
#pragma once
//#endif // _MSC_VER >= 1000

#include "Asph2.h"
#include "ftypes.h"
#include <assert.h>
#include "../Spectacles/Array.h"

//#define PI 		    3.14159265358979323846

 
typedef double FType;

typedef struct {
	int N_cut ;
	int N_points ;
	int CloseCirc ;
	int CloseLine ;
	int FrontBackFacet ;
	int ComplexObject ; 
} Mesh3D ;


struct Object3D_com
{
	PointF_t *vertices ;
	PointF_t *normals ;
	PointF_t  *verts_texture ;
	ColorFA_t *verts_color ;


	Fixed16_t *plist ; //����� �������� ������������ ���������������, � ������� - ����� ����� ��������������, ������� �����

	Fixed16_t *triangles; // ����� �������� ������������ �������������, �� ��� ������� ��� �������������� ����������

	PointF_t *vertices_p ;
	PointF_t *normals_p ;

	Fixed32_t N_vertices, 
			  N_polygons,
			  N_triangles;  

	int MaxVertices;
	int PolynomSize;

	int  CCW ;
	int colored ;
	int textured ;
	int cull ;
	int transformed ;

	int Mesh ;
	Mesh3D Meshaprs ;

	int TexturedBy ;
	float xt, yt, dxt, dyt ;

	int ConvertToTriangles()
	{
		if(	triangles)
			return N_triangles;

		int ip = 0;
		int ipl = 0;
		N_triangles = 0;
		int fl_direction;

		if( CCW ) 
			fl_direction = -1.f ;
		else 
			fl_direction = 1.f ;
		fl_direction = 1.f ;


        int i = 0;
        ipl = 0;
        ip = 0;
        for(i = 0; i < N_polygons; i++) {
            int np = plist[ip];

            for( int j = 0 ; j < np-2 ; j++ )
            {
                ipl += 3;
            }
            ip += (np+1);
        }

//        int TIMemSize = N_polygons*8;
        int TIMemSize = ipl;
        triangles = (Fixed16_t*)malloc(TIMemSize*sizeof(Fixed16_t));

        ipl = 0;
        ip = 0;
        for(i = 0 ; i < N_polygons; i++) {
			int np = plist[ip];

			for( int j = 0 ; j < np-2 ; j++ )
			{
				if(ipl + 2 >= TIMemSize)
				{
                    // TriangleIndex overflow
                    assert(0);
					continue;
				}
				if( fl_direction > 0.f )
				{
					triangles[ipl+0] = plist[ip+1];
					triangles[ipl+1] = plist[ip+2+j];
					triangles[ipl+2] = plist[ip+3+j];
				}
				if( fl_direction < 0.f )
				{
					triangles[ipl+0] = plist[ip+1];
					triangles[ipl+1] = plist[ip+3+j];
					triangles[ipl+2] = plist[ip+2+j];
				}
				ipl += 3;
				N_triangles ++;
			}
			ip += (np+1);
		}
		return N_triangles;
	}
	

	Object3D_com()
	{
		vertices = NULL;
		normals = NULL;
		verts_texture = NULL;
		verts_color = NULL;
		plist = NULL;
		vertices_p = NULL;
		normals_p = NULL;
		triangles = NULL;

		N_vertices = 0;
		N_polygons = 0;
		MaxVertices = 0;
		PolynomSize = 0;
	}



	void ResizeObject3D(int max_vert, int pol_size)
	{
		if(max_vert < MaxVertices)
			return;
		if(pol_size < PolynomSize)
			return;

		void * p;

		if(max_vert > MaxVertices)
		{
			p = vertices;
			vertices = (PointF_t *)calloc(max_vert, sizeof(PointF_t));
			if(vertices)
				memset(vertices, 0, max_vert * sizeof(PointF_t));
			if(vertices && p)
				memcpy(vertices, p, MaxVertices * sizeof(PointF_t));
			if(p)
				free(p);
		}

		if(max_vert > MaxVertices)
		{
			p = normals;
			normals  = (PointF_t *)calloc(max_vert, sizeof(PointF_t));
			if(normals)
				memset(normals, 0, max_vert * sizeof(PointF_t));
			if(normals && p)
				memcpy(normals, p, MaxVertices * sizeof(PointF_t));
			if(p)
				free(p);
		}

		if(max_vert > MaxVertices)
		{
			p = vertices_p;
			vertices_p = (PointF_t *)calloc(max_vert, sizeof(PointF_t));
			if(vertices_p)
				memset(vertices_p, 0, max_vert * sizeof(PointF_t));
			if(vertices_p && p)
				memcpy(vertices_p, p, MaxVertices * sizeof(PointF_t));
			if(p)
				free(p);
		}

		if(max_vert > MaxVertices)
		{
			p = normals_p;
			normals_p  = (PointF_t *)calloc(max_vert, sizeof(PointF_t));
			if(normals_p)
				memset(normals_p, 0, max_vert * sizeof(PointF_t));
			if(normals_p && p)
				memcpy(normals_p, p, MaxVertices * sizeof(PointF_t));
		}

		if(pol_size > PolynomSize)
		{
			p = plist;
			plist = (unsigned short *)calloc(pol_size, sizeof(Fixed16_t));
			if(plist)
				memset(plist, 0, pol_size * sizeof(Fixed16_t));
			if(plist && p)
				memcpy(plist, p, PolynomSize * sizeof(Fixed16_t));
			if(p)
				free(p);
		}

		if(verts_color)
		{
			if(max_vert > MaxVertices)
			{
				p = verts_color;
				verts_color = (ColorFA_t *)calloc(max_vert, sizeof(ColorFA_t));
				if(verts_color)
				{
					memset(verts_color, 0, max_vert * sizeof(ColorFA_t));
					memcpy(verts_color, p, MaxVertices * sizeof(ColorFA_t));
				}
				free(p);
			}
		}

		if(verts_texture)
		{
			if(max_vert > MaxVertices)
			{
				p = verts_texture;
				verts_texture = (PointF_t *)calloc(max_vert, sizeof(PointF_t));
				if(verts_texture)
				{
					memset(verts_texture, 0, max_vert * sizeof(PointF_t));
					memcpy(verts_texture, p, MaxVertices * sizeof(PointF_t));
				}
				free(p);
			}
		}

		MaxVertices = max_vert ;
		PolynomSize = pol_size;
	}
};

struct Object3D : public Object3D_com
{

	Object3D() { memset(this, 0, sizeof(*this)); }

	char	 *point_status ;

} ;



typedef struct {
	PointF_t point ;
	float ScaleX ;
	float ScaleY ;
	float RotAngle ;
}  Wire ;

typedef	struct {
    ColorF_t Oa;
	ColorFA_t Od;
	ColorF_t Os;
	Float_t	Se;
} SurfaceMaterial ;

typedef struct {
	R3dHandle_t TextureID ;
	int TexturingMethod ;
	int MaterialID ;
	int MaterialBackID ;
	int Reflection ;
}  Surface ;


typedef	struct {
	float x, y ;
}  Point2D ;

typedef struct {
	Object3D object3d ;	
	Surface  surface  ;
}  FullObject ;

typedef struct {
	Object3D_com object3d ;	
	Surface  surface  ;
}  FullObject_com ;

typedef struct {
	FullObject_com ForeSurf ;
	FullObject_com BackSurf ;
	FullObject_com SideSurf ;
	int *SideForePoint ;
	float x_center, y_center, z_center ;
	float Index, Sph, Cyl, Axis, Weight, Density ;
	float place_angle ;
	float Shift ;
	int Material, Form, Design ;
	int N_points_side ;
	lens_inf math_struct ;

	int Placed ;
} FullLense_com;



typedef struct {
	FullObject ForeSurf ;
	FullObject BackSurf ;
	FullObject SideSurf ;
	FullObject BevelSideSurf ;
	FullObject BevelSideSurfExtern ;
	int *SideForePoint ;
	float x_center, y_center, z_center ;
	float x_center_opt, y_center_opt ;

	float Index, Sph, Cyl, Axis, Weight, Density ;
	float Addition ;
	float place_angle ;
	float Shift ;
	int Material;//, Form, Design ;
	int N_points_side ;
	int BevelType3D ;
	int Photochromic ;
	lens_inf math_struct ;

	int Placed ;
	
	ThicknessStruct Thickness;

	float Lense_center_x, Lense_center_y ;
	void  *Segments[10] ;
	double Pars[10] ;
	FullObject SegmentIntSideSurf ;
	int *SegmentLenseSideLinks ;
	int *SegmentSidePointStatus ;


	FullObject SegmentShield ;
	FullObject SegmentBackwardSideSurf ;
	int ShieldPointer_1 ;
	int ShieldPointer_2 ;
	int ShieldPointer_3 ;
	

	int N_segments ;
	int SegmentIndex ;
	int SegmentType ;

	FullObject MinMarker ;
	FullObject MaxMarker ;

	float sign ;
} FullLense;



/*
typedef struct {
	int N_points_contour ;
	float rcl ;
	double *ContourX ;
	double *ContourY ;
} RimContour ;
*/
typedef struct {

float CirclMaxX, CirclYMaxX ;				
float CirclMinX, CirclYMinX ;				
float CirclMaxY, CirclXMaxY ;				
float CirclMinY, CirclXMinY ;				
float CirclHeight, CirclWidth, FrameWidth ;
int n_MaxX, n_MaxY, n_MinX, n_MinY ;

} FrameMetriks ;

typedef struct {
	FullObject Object ;
	char name[30] ;

	double Pars[10] ;
	int		Flags[10] ;

	int number ; 
	int RefractableFromFront ;
	int RefractableFromBack  ;
	int SeenThroughtGlass ;
	char RightTemple ;
	char LeftTemple ;
	char RightRim ;
	char LeftRim ;
	char RightRimPart ;
	char LeftRimPart	;
	char OtherPart ;
} FramePart ;					

typedef struct {
	FullObject_com Object ;
	char name[30] ;

	double Pars[10] ;
	double Flags[10] ;

	int number ; 
	int RefractableFromFront ;
	int RefractableFromBack  ;
	int SeenThroughtGlass ;
	char RightTemple ;
	char LeftTemple ;
	char RightRim ;
	char LeftRim ;
	char RightRimPart ;
	char LeftRimPart	;
	char OtherPart ;
} FramePart_com ;					


class Frame {
public:
	Frame(){};
	~Frame(){};

	char name[30] ;

	int frame_type ;    // 0 - adaptive, 1 - static
	int adaptive_type ;
	float  StaticScale ;

	FramePart Parts[40] ;
	int N_parts ;
	FrameMetriks Metriks ;

	PointF_t temple_base_right ;
	PointF_t temple_base_left ;
//	int temple_type ;
};
/*
class Frame {
public:
	Frame();
	~Frame();

	char name[30] ;

	int frame_type ;    // 0 - adaptive, 1 - static
	int adaptive_type ;
	float  StaticScale ;

	FramePart Parts[40] ;
	int N_parts ;
	FrameMetriks Metriks ;

	PointF_t temple_base_right ;
	PointF_t temple_base_left ;
	int temple_type ;
};
*/


class DFrame {
public:
	DFrame(){};
	~DFrame(){};

	char name[30] ;

	int frame_type ;    // 0 - adaptive, 1 - static
	int adaptive_type ;
	float  StaticScale ;

	FramePart Parts[40] ;
	int N_parts ;
	FrameMetriks Metriks ;

	PointF_t temple_base_right ;
	PointF_t temple_base_left ;
//	int temple_type ;
};




class Frame_com {
public:
	Frame_com(){};
	~Frame_com(){};

	char name[30] ;

	int frame_type ;    // 0 - adaptive, 1 - static
	int adaptive_type ;
	float  StaticScale ;

	FramePart_com Parts[40] ;
	int N_parts ;
	FrameMetriks Metriks ;

	PointF_t temple_base_right ;
	PointF_t temple_base_left ;
//	int temple_type ;
};

typedef struct {
	int Type;
	int SubType ;
	int Flags[10] ;
	float Pars[20] ;
} FrameVariablePart ;
/*
typedef struct {
	char Name[80] ;
	char TreePath[500] ;
	float Xrim[100], Yrim[100] ; // ���������� ������ �������. ������������ ������ ( � �������� �������� )
	int	N_points_rim ;
	float Rc ;  // ������������ ���������� ( � �������� �������� )
	float Rc_local ;  // ����������� ������������ ���������� ( � �� )
	float RimWidth ;
	float BridgeWidth ;
	float RimScale ;
	float SpanScale ;
	Point2D CutLine[20] ; // ���������� ����� ����� �������� ����������� ������� ������
	int N_cut ;
	FrameVariablePart Parts[15] ;
	int N_parts ;
} FrameTemplateOld;
*/
typedef CArray<float, float&> CFloatArray;

typedef struct
{
	char Name[80] ;
	char TreePath[500] ;
    CFloatArray Xrim, Yrim ; // ���������� ������ �������. ������������ ������ ( � �������� �������� )
//	float Xrim[100], Yrim[100] ; // ���������� ������ �������. ������������ ������ ( � �������� �������� )
	int	N_points_rim ;
	float Rc ;  // ������������ ���������� ( � �������� �������� )
	float Rc_local ;  // ����������� ������������ ���������� ( � �� )
	float RimWidth ;
	float BridgeWidth ;
	float RimScale ;
	float SpanScale ;
	Point2D CutLine[20] ; // ���������� ����� ����� �������� ����������� ������� ������
	int N_cut ;
	FrameVariablePart Parts[15] ;
	int N_parts ;
//	GUID	ContourGuid;

	BYTE Reserved[100];

} FrameTemplate;

typedef struct 
	   { float x ;
		 float y ;
		 float z ;
		 int xp ;
		 int yp ;
		 int part ;
		 int point ;
		} Mrk ;

//typedef struct
//		{ char *name ;
//		  HTREEITEM ItemHandle ;
//		  int StartTemplateNumber ;
//		} TreeObject ;
//_________________________________
//void PrepareTexturing( void ) ;
//
//void Init3dr( HWND hwnd ) ;
//void Destroy3dr( void ) ;
//void Display3dr( void ) ;
//int  Get3drStatus( void ) ;
//
//void InitFrame( void ) ;
////void DestroyFrame( void );
//void InitLensePair( void ) ;
//void DestroyLensePair( void ) ;
//
//void InitTempObjects( void ) ;
//void DestroyTempObjects( void ) ;
//
////void InitialiseObject3D( Object3D *object, int textured, int colored,
////						int max_vert ) ;
//void NullObject3D( Object3D *object ) ;
////void DestroyObject3D( Object3D *object ) ;
//void InitialiseLense3D( FullLense *lense ) ;
//void DestroyLense3D( FullLense *lense ) ;
//void GenerateSide( Object3D *object, int N_points_side,
//				   float *X, float *Y ) ;
////void GenerateObjectNormals( Object3D *object, float normal_sign ) ;
//void GenerateObjectNormalsFromTo( Object3D *object,  int begin, int end, float normal_sign ) ;
//void generate_normal_lense( void ) ;
//void DrawObject( FullObject *object ) ;
//void DrawLineObject( FullObject * ) ;
//void DrawFacetObject( FullObject *object ) ;
//void GenerateForeBack( Object3D *object, PointF_t *circ,
//				   int N_points,
//				   double Z_fun( double x, double y ) ) ;
//void AddVertices( Object3D *object, double Z_fun( double x, double y ) ) ;
//void computeEnvUV( TransformF_t mat, PointF_t *nrm, PointF_t *tuv, float sign ) ;
////void ReflectFromObject( Object3D object, float sign ) ;
//void action_texfunc( int func ) ;
//void prepare_texturing( void  ) ;
///*
//void GenerateWireGeometry( Object3D *object, PointF_t BasePlane,
//						 Wire *WireLine,
//						 int N_points, float MaxStep,
//						 Point2D *CutLine, int N_cut_points,
//						 int CloseCirc, int CloseLine, int FrontBackFacet, int ComplexObject, int Textured ) ;
//*/
////void MoveObject3D( Object3D object, float dx, float dy, float dz ) ;
//void GenerateWireFrame( Frame *frame, Point2D *XY, int N_points,
//					float x_c_right, float y_c_right,
//					float x_c_left, float y_c_left ) ;
//void GenerateHornFrame( Frame *frame, Point2D *XY, int N_points,
//					float x_c_right, float y_c_right,
//					float x_c_left, float y_c_left ) ;
////void RotateObject3D( Object3D object, float xa, float ya, float za, float angle, float  x_center, float y_center, float z_center ) ;
//int  GenerateNormalLense( FullLense *lense, Point2D *XY, int N_points_side,
//							float x_center, float y_center, float sign ) ;
//void SetSurfacePars( FullObject *object ) ;
//void RotateTemple( Frame &frame, int RightLeft, float angle ) ;
//void SetBaseLight( void ) ;
//void ComputeLightThroughLense( FullLense lense, int ind, int side ) ;
//void ComputeLenseColoring( FullObject surf, float y_center ) ;
//int GetCutPoints(FType CirRad,FType CirCenZ,FType StX,FType StY,FType StZ,
//				 FType Vec1X,FType Vec1Y,FType Vec1Z,
//				 FType *Point1X,FType *Point1Y,FType *Point1Z,
//				 FType *Point2X,FType *Point2Y,FType *Point2Z);
////void TransformObject3d( Object3D *object ) ;
////void ClearTransformObject3d( Object3D *object ) ;
//void SetTransformObject3d( Object3D *object ) ;
////void PrepareTransform( void ) ;
//void CalculateSideSurfThroughGlass( FullLense full_object, int side ) ;
//void RecalcObjectColors( Object3D object, int side ) ;
//void ComputeSmartFullReflection( FullObject full_object, float xc, float yc,
//							   float Npl, PlaneF_t BasePlane, int ind, int side )  ;
//void RestoreRefractedObject( void ) ;
//void RefractObject( FullObject *full_object ) ;
//void SetRefractionPars( FullLense Lense, float view_fore, float view_back ) ;
//float DetermineSurfaceVisibility( FullObject full_object, int N_points ) ;
//int DetermineSurfaceFullVisibility( FullObject full_object ) ;
////void GenerateObjectTransformedNormals( Object3D *object,  float normal_sign ) ;
////int  InitializeZMinMax( FullLense *LenseP,  float *Xb, float *Yb, int N_points_side ) ;
////void GetAffLensesPars( LENSE lense, float *xt, float *yt, short *cnt, short *dxField, short *dyField, int *y_up ) ;
//void SaveCurrentDrawingPars( int ind ) ;
//void RestoreCurrentDrawingPars( int ind ) ;
//void PrepareDrawingInWindow( void ) ;
//void ClearDrawBuffers( void ) ;
//void DrawFrameAndLense( void ) ;
//void Show3drWindow( void ) ;
////void DecreasePointsNumber( Point2D XYf[], int N_points_side, float _Xt[], float _Yt[] ) ;
//void UnplaceLenses( void ) ;

//_________________________________

#endif

















/* 
typedef double FType;

typedef struct {
	PointF_t *vertices ;
	PointF_t *normals ;
	PointF_t  *verts_texture ;
	ColorFA_t *verts_color ;
	Fixed16_t *plist ;
//	short     *plist ;

	PointF_t *vertices_p ;
	PointF_t *normals_p ;
	char	 *point_status ;


	Fixed32_t N_vertices, 
			  N_polygons ;  
	int  CCW ;
	int colored ;
	int textured ;
	int cull ;
	int transformed ;
} Object3D ;

typedef struct {
	PointF_t point ;
	float ScaleX ;
	float ScaleY ;
	float RotAngle ;
}  Wire ;

typedef	struct {
    ColorF_t Oa;
	ColorFA_t Od;
	ColorF_t Os;
	Float_t	Se;
} SurfaceMaterial ;

typedef struct {
	R3dHandle_t TextureID ;
	int TexturingMethod ;
	int MaterialID ;
	int MaterialBackID ;
	int Reflection ;
}  Surface ;


typedef	struct {
	float x, y ;
}  Point2D ;

typedef struct {
	Object3D object3d ;	
	Surface  surface  ;
}  FullObject ;

typedef struct {
	FullObject ForeSurf ;
	FullObject BackSurf ;
	FullObject SideSurf ;
	FullObject BevelSideSurf ;
	FullObject BevelSideSurfExtern ;
	int *SideForePoint ;
	float x_center, y_center, z_center ;
	float Index, Sph, Cyl, Axis, Weight, Density ;
	float Addition ;
	float place_angle ;
	float Shift ;
	int Material;//, Form, Design ;
	int N_points_side ;
	int BevelType3D ;
	int Photochromic ;
	lens_inf math_struct ;

	int Placed ;
	
	ThicknessStruct Thickness;

	float Lense_center_x, Lense_center_y ;
	void  *Segments[10] ;
	double Pars[10] ;
	FullObject SegmentIntSideSurf ;
	int *SegmentLenseSideLinks ;
	int *SegmentSidePointStatus ;


	FullObject SegmentShield ;
	FullObject SegmentBackwardSideSurf ;
	int ShieldPointer_1 ;
	int ShieldPointer_2 ;
	int ShieldPointer_3 ;
	

	int N_segments ;
	int SegmentIndex ;
	int SegmentType ;
} FullLense;


typedef struct {

float CirclMaxX, CirclYMaxX ;				
float CirclMinX, CirclYMinX ;				
float CirclMaxY, CirclXMaxY ;				
float CirclMinY, CirclXMinY ;				
float CirclHeight, CirclWidth, FrameWidth ;
int n_MaxX, n_MaxY, n_MinX, n_MinY ;

} FrameMetriks ;

typedef struct {
	FullObject Object ;
	char name[30] ;
	int number ; 
	int RefractableFromFront ;
	int RefractableFromBack  ;
	int SeenThroughtGlass ;
	int RightTemple ;
	int LeftTemple ;
} FramePart ;					


typedef struct {
	char name[30] ;

	int frame_type ;    // 0 - adaptive, 1 - static
	int adaptive_type ;
	float  StaticScale ;

	FramePart Parts[40] ;
	int N_parts ;
	FrameMetriks Metriks ;

	PointF_t temple_base_right ;
	PointF_t temple_base_left ;
	int temple_type ;
} Frame ;

typedef CArray<float, float&> CFloatArray;


typedef struct
{
	char Name[80] ;
	char TreePath[500] ;
	float Xrim, Yrim ; // ���������� ������ �������. ������������ ������ ( � �������� �������� )
//	float Xrim[100], Yrim[100] ; // ���������� ������ �������. ������������ ������ ( � �������� �������� )
	int	N_points_rim ;
	float Rc ;  // ������������ ���������� ( � �������� �������� )
	float Rc_local ;  // ����������� ������������ ���������� ( � �� )
	float RimWidth ;
	float BridgeWidth ;
	float RimScale ;
	float SpanScale ;
	Point2D CutLine[20] ; // ���������� ����� ����� �������� ����������� ������� ������
	int N_cut ;
	FrameVariablePart Parts[15] ;
	int N_parts ;
	GUID	ContourGuid;

	BYTE Reserved[100];

} FrameTemplate;


//_________________________________

void PrepareTexturing( void ) ;

void Init3dr( HWND hwnd ) ;	
void Destroy3dr( void ) ;
void Display3dr( void ) ;

void InitFrame( void ) ;
void DestroyFrame( void );
void InitLensePair( void ) ;
void DestroyLensePair( void ) ;

void InitTempObjects( void ) ;
void DestroyTempObjects( void ) ;

void InitialiseObject3D( Object3D *object, int textured, int colored,
						int max_vert ) ;
void NullObject3D( Object3D *object ) ;
void DestroyObject3D( Object3D *object ) ;
void InitialiseLense3D( FullLense *lense ) ;
void DestroyLense3D( FullLense *lense ) ;
void GenerateSide( Object3D *object, int N_points_side,
				   float *X, float *Y ) ;
void GenerateObjectNormals( Object3D *object, float normal_sign ) ;
void GenerateObjectNormalsFromTo( Object3D *object,  int begin, int end, float normal_sign ) ;
void generate_normal_lense( void ) ;
void DrawObject( FullObject *object ) ;
void DrawLineObject( FullObject * ) ;
void DrawFacetObject( FullObject *object ) ;
void GenerateForeBack( Object3D *object, PointF_t *circ,
				   int N_points,
				   double Z_fun( double x, double y ) ) ;
void AddVertices( Object3D *object, double Z_fun( double x, double y ) ) ;
void computeEnvUV( TransformF_t mat, PointF_t *nrm, PointF_t *tuv, float sign ) ;
void ReflectFromObject( Object3D object, float sign ) ;
void action_texfunc( int func ) ;
void prepare_texturing( void  ) ;
void GenerateWireGeometry( Object3D *object, PointF_t BasePlane,
						 Wire *WireLine, 
						 int N_points, float MaxStep,
						 Point2D *CutLine, int N_cut_points,
						 int CloseCirc, int CloseLine, int FrontBackFacet, int ComplexObject, int Textured ) ;
void MoveObject3D( Object3D object, float dx, float dy, float dz ) ;
void GenerateWireFrame( Frame *frame, Point2D *XY, int N_points, 
					float x_c_right, float y_c_right,
					float x_c_left, float y_c_left ) ;
void GenerateHornFrame( Frame *frame, Point2D *XY, int N_points, 
					float x_c_right, float y_c_right,
					float x_c_left, float y_c_left ) ;
void RotateObject3D( Object3D object, float xa, float ya, float za, float angle, float  x_center, float y_center, float z_center ) ;
void GenerateNormalLense( FullLense *lense, Point2D *XY, int N_points_side, 
							float x_center, float y_center, float sign ) ;
void SetSurfacePars( FullObject *object ) ;
void RotateTemple( Frame frame, int RightLeft, float angle ) ;
void SetBaseLight( void ) ;
void ComputeLightThroughLense( FullLense lense, int ind, int side ) ;
void ComputeLenseColoring( FullObject surf, float y_center ) ;
int GetCutPoints(FType CirRad,FType CirCenZ,FType StX,FType StY,FType StZ,
				 FType Vec1X,FType Vec1Y,FType Vec1Z,
				 FType *Point1X,FType *Point1Y,FType *Point1Z,
				 FType *Point2X,FType *Point2Y,FType *Point2Z);
void TransformObject3d( Object3D *object ) ;
void ClearTransformObject3d( Object3D *object ) ;
void SetTransformObject3d( Object3D *object ) ;
void PrepareTransform( void ) ;
void CalculateSideSurfThroughGlass( FullLense full_object, int side ) ;
void RecalcObjectColors( Object3D object, int side ) ;
void ComputeSmartFullReflection( FullObject full_object, float xc, float yc, 
							   float Npl, PlaneF_t BasePlane, int ind, int side )  ;
void RestoreRefractedObject( void ) ;
void RefractObject( FullObject *full_object ) ;
void SetRefractionPars( FullLense Lense, float view_fore, float view_back ) ;
float DetermineSurfaceVisibility( FullObject full_object, int N_points ) ;
int DetermineSurfaceFullVisibility( FullObject full_object ) ;
void GenerateObjectTransformedNormals( Object3D *object,  float normal_sign ) ;
void InitializeZMinMax( FullLense *LenseP,  float *Xb, float *Yb, int N_points_side ) ;
void GetAffLensesPars( LENSE lense, float *xt, float *yt, short *cnt, short *dxField, short *dyField, int *y_up ) ;
void SaveCurrentDrawingPars( int ind ) ;
void RestoreCurrentDrawingPars( int ind ) ;
void PrepareDrawingInWindow( void ) ;
void ClearDrawBuffers( void ) ;
void DrawFrameAndLense( void ) ;
void Show3drWindow( void ) ;
void DecreasePointsNumber( Point2D XYf[], int N_points_side, float _Xt[], float _Yt[] ) ;
void UnplaceLenses( void ) ;
*/
//_________________________________
