#ifndef __FTYPES_H_
#define __FTYPES_H_

typedef float Float_t;
typedef unsigned long Dword_t;
typedef	void* LPVoid_t;

typedef struct {
	Float_t x, y, z;
}   PointF_t;

typedef struct {
	Float_t r, g, b, a;
}   ColorFA_t;

typedef struct {
	Float_t r, g, b;
}   ColorF_t;

typedef struct {
	Float_t x, y, z, w;
}   PointFW_t;

typedef WORD Fixed16_t;
typedef LONG Fixed32_t;

typedef			 Dword_t	R3dHandle_t;

typedef Float_t TransformF_t[4][4];
typedef Float_t Transform23F_t[2][3];

typedef struct {
	Float_t a, b, c, d;
}   PlaneF_t;

typedef struct {
	PointF_t point, vector;
}   LineF_t;

typedef LPVoid_t G3dHandle_t;

#define G3dVectorLength(x,y,z) ((Float_t)sqrt((x)*(x)+(y)*(y)+(z)*(z)))

#endif