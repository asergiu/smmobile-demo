#pragma once
#include "Material.h"

typedef struct {
	int FramesPresence ;   // 0 - ��� ������, 1 - ����

	int AllLensesPresence ; 
	int LeftLensePresence ;
	int RightLensePresence ;

	int BackgroundScenePresence ;

	int LenseRefraction ;
	int LenseColoring ;
	int FrontCoating ;
	int BackCoating ;
	int ForeBlink ;
	int BackBlink ;
	int SmartFullReflectionZone ;
	int SideSurfReReflection ;
	int FramesRefraction ;

	int RefractionModel ;   //  0 - ������ ����� ; 1 - ���� �������
	int ReflectionModel ;
	
    int ShowLensThicknessMode;
} DisplayDetail ;

typedef struct {
	int FullScreenShape ;
	int QuaterNumber ;

	float Ry_bc, out_angle ;
	float _Xt[200], _Yt[200], _Zt[200] ;
	float  LeftLenseShift, RightLenseShift ;
	LENSE current_lense ;
	FullLense_com RightLense, LeftLense ;
	HWND hwndb ;
    DisplayDetail DisplayDet ;
	Frame_com CurrentFrame ;
	R3dHandle_t hRC ;
	G3dHandle_t hGC ;
	PointF_t angle, trans ;
	int first ;
	float x_cw_right, y_cw_right, x_cw_left, y_cw_left ;
	short current_cnt[12] ;
} FullImage ;
