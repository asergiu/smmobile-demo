#if !defined(__ASPH2_H__)
#define __ASPH2_H__

#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C++" {
	#include <math.h>
}
#endif

#ifndef EPS 
	#define EPS	(1e-8)
#endif

#define PI 		    3.14159265358979323846
#define COSMOLUX	0
#define COSMOLIT15	1
#define COSMOLIT16	2
#define PERFASTAR	3
#define LENTILUX	4
#define SPHERIC		5

#ifndef MAX
	#define MAX(a,b) ((a)<(b)?(b):(a))
#endif
#ifndef MIN
	#define MIN(a,b) ((a)>(b)?(b):(a))
#endif
#define near
#define GL 	0
#define PL 	1


struct  lens_inf {
double _sc_x,_sc_y,_dR,_d,_R,_r,_s_r,_r_main,_r_aux,_dpt,diop_main,
	   diop_aux,_x0,_y0,_angle,_ang_sh,
	   _OZ,A0,A1,A2,A3,A4,B0,B1,B2,B3,B4,B5;
int _old,_type,_r_org,_diam,_diam_org,_tor,_mat;
double z_coeff;
	   };

//extern double density[],_n_e[];
//extern int material[],_diam_table[];

const int _diam_table[]={62,80,66,66,70};

#define notOT_VER
#define notOT_VER1
int  base_index(int type,double s1);
int set_par_sph(int mat,float *x,float *y, int np,double s_r,double r,
				double R,double d,double angle,double x0,double y0);
typedef struct {
	double x, y, z_h, z_l;
	} Pnts;

typedef struct {
	double x, y, z;
	} Pnt;


int gen_pnts(float *x,float *y,int M,double _x0,double _y0,
			 double Px,double Py,double Vx, double Vy,Pnts *pnts,int N);
int gen_pnts1(float *x,float *y,int m,double _x0,double _y0,double step,
			  char *flags,float *coeff,Pnt **pnts,int *N);

double  a0(int type,double s);
double  a1(int type,double s);
double  a2(int type,double s);
double  a3(int type,double s);
double  a4(int type,double s);
double  b0(int type,double s);
double  b1(int type,double s);
double  b2(int type,double s);
double  b3(int type,double s);
double  b4(int type,double s);
double  b5(int type,double s);
double  DR(int type,double s,int d);
double  D_(int type,double s,int d);
double  R(int type,double s,int d);
double  OZ(int type,double s);

double  der_min(double r);
double  der_max(double r);

double edge(double x,double alpha,double y0,double y1);
void Epoints(double Cx,int reset,double *ly,double *hy);
int  tr_prepare(void);

int  exists(int type,double s,int d);

int  base_index(int type,double s1);


void rads(float *x,float *y,int np,float x0, float y0,
			float *fR,float *sr,float *sR) ;

#ifndef	__THICKNESSSTRUCT__
#define __THICKNESSSTRUCT__
struct ThicknessStruct{
	double	CenMin,
			EdgeMin,
			IntegralMin;
	};
#endif
#endif