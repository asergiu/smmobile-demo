#ifndef	__THICKNESSSTRUCT__
#define __THICKNESSSTRUCT__

struct ThicknessStruct{
	double	CenMin,		//default: 1.0, min=0.3, max=3.0
			EdgeMin,	//default: 0.8, min=0.3, max=3.0
			IntegralMin;//default: 1.5, min=0.3, max=3.0
	};

#endif
