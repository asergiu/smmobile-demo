#pragma once

#include "ObjectBase.h"
#include "UsefulThings.h"
#include "ClassMatrix.h"
#include <string>
using std::string;
#include "vector"
using std::vector;

void CalcDerMatrix(CDoubleMatrix &InMatrix, CDoubleMatrix &DerMatrixX, CDoubleMatrix &DerMatrixY, double h);
void CalcDerMatrixX(CDoubleMatrix &InMatrix, CDoubleMatrix &DerMatrixX, double h);
void CalcDerMatrixY(CDoubleMatrix &InMatrix, CDoubleMatrix &DerMatrixY, double h);
void CalcInterpNormalMatrix(CDoubleMatrix &DerMatrix, CDoubleMatrix &NormalMatrix, int size, BOOL fInvert);
void FillMatrixWithDBL_MAX(CDoubleMatrix &m);
double Interp2(CDoubleMatrix &m, double x, double y);
COLORREF Interp2(CColorMatrix32 &Pict,FType xx,FType yy);
double SaturConv(double Param, double Limiter);

int SolveEqSys2x2(double a1, double b1, double c1, double a2, double b2, double c2, double &t1, double &t2);

double LenVec3D(double x, double y, double z);
double SqrLenVec3D(double x, double y, double z);

class CPoint3D
{
public:
	CPoint3D(double Px, double Py, double Pz)
	{
		px = Px;
		py = Py;
		pz = Pz;
	};
	CPoint3D()
	{
		px = py = pz = 0.;
	};
	double px, py, pz;
	double DistanceTo(CPoint3D &OtherPoint);
	CPoint3D &operator=(const CPoint3D &point)
		{px = point.px; py = point.py; pz = point.pz; return *this;};
};

class CLine3D;

class CVec3D
{
public:
	double vx, vy, vz;
	CVec3D(CPoint3D &FirstPoint, CPoint3D &SecondPoint)
	{
		vx = SecondPoint.px - FirstPoint.px;
		vy = SecondPoint.py - FirstPoint.py;
		vz = SecondPoint.pz - FirstPoint.pz;
	};
	CVec3D(double Vx, double Vy, double Vz)
	{
		vx = Vx;
		vy = Vy;
		vz = Vz;
	};
	CVec3D()
	{
		vx = vy = vz = 0.;
	};
	bool IsOrtog(CVec3D &OtherVec);
	double CosOfAngle(CVec3D &OtherVec);
};

class CLine3D
{
public:
	CLine3D()
	{
		px = py = pz = vx = vy = vz = 0;
	}
	CLine3D(double Px, double Py, double Pz, double Vx, double Vy, double Vz)
	{
		SetData(Px, Py, Pz, Vx, Vy, Vz);
	}
	CLine3D(CPoint3D Pt, CVec3D Vec)
	{
		px = Pt.px; py = Pt.py; pz = Pt.pz; vx = Vec.vx; vy = Vec.vy; vz = Vec.vz;
	}
	double px, py, pz, vx, vy, vz;
	double FindNearestPoint(CLine3D &AnotherLine, CPoint3D &ResPoint);
	double FindDistanceToPoint(CPoint3D &Point, CPoint3D &ResPoint);
	void GetPoint(double Param, CPoint3D &Res);
	bool IsPointOnLine(CPoint3D &Point);
	void SetData(double Px, double Py, double Pz, double Vx, double Vy, double Vz)
	{
		px = Px; py = Py; pz = Pz; vx = Vx; vy = Vy; vz = Vz;
	}
	CLine3D& operator +=(CVec3D &vec);
	CLine3D& operator *=(double d);
	CLine3D& operator /=(double d);
};

class CSphere
{
public:
	CPoint3D Cen;
	double Rad;
	
	CSphere()
	{Rad = 0; Cen.px = Cen.py = Cen.pz = 0;};

	CSphere(CPoint3D &Center, double Radius)
	{Cen = Center; Rad = Radius;};

	CSphere(double CenterX, double CenterY, double CenterZ, double Radius)
	{Cen.px = CenterX; Cen.py = CenterY; Cen.pz = CenterZ; Rad = Radius;};

};

class CPlane
{
public:
	double A, B, C, D;

	CPlane()
	{A = B = C = D = 0.;};
	
    CPlane(CPoint3D point, CVec3D Norm)
	{
		A = Norm.vx; B = Norm.vy; C = Norm.vz;
		D = - (A * point.px + B * point.py + C * point.pz);
	};

	CPlane &operator=(CPlane plane)
	{
		A = plane.A; B = plane.B; C = plane.C; D = plane.D;
		return *this;
	}

	bool FindIntersectWithLine(CLine3D &Line, CPoint3D &Point)
	{
		double t1 = A * Line.vx + B * Line.vy + C * Line.vz;
		if(fabs(t1) < EPS)
			return false;
		double t = - (A * Line.px + B * Line.py + C * Line.pz + D) / t1;

		Point.px = Line.px + Line.vx * t;
		Point.py = Line.py + Line.vy * t;
		Point.pz = Line.pz + Line.vz * t;

		return true;
	}
};

typedef vector<CPoint3D> CPoint3DVector;
typedef vector<CLine3D> CLine3DVector;
typedef vector<FTPOINT> CFtPointVector;
typedef vector<int> CIntVector;
typedef vector<double> CDoubleVector;

class CVertBeam //��������� ����� �����, ������������ ��� Z
{
public:
	CFtPointVector Lines;
	void Clear();
	BOOL AddBeamPart(FTPOINT &CenterPoint, double AngleMin, double AngleMax, int AngleSteps, 
							double RadMin, double RadMax, int RadSteps);
};

class CBeam //��������� ����� �����, ���������� �������������� � �����
			//�������������� �������� ���� ������� ����� ��������� � �.�.
{
public:
	CIntVector Validity;
	CLine3DVector Lines;
	void Clear();
	bool CalcKnotSphere(CSphere &ResSph, double *pAvRad = NULL, double *pAvSqRad = NULL);
	int FindNearestLineToPoint(CPoint3D &Point);
};

class CLensSurfProducer
{
protected:
	string ErrorMsg;
public:
	virtual bool CalcValues(CFtPointVector &InArray, CDoubleVector &FrontSurf, CDoubleVector &RearSurf) = 0;
	string GetLastErrorString(){ return ErrorMsg; };
};

const double CalcPointDiopFailed = -100.;

class CRayTracer
{
	bool	DataReady;
	int		xOffsetGrid, yOffsetGrid;
	CDoubleMatrix	zFrontMatrix, zFrontNormalMatrixX, zFrontNormalMatrixY;
	CDoubleMatrix	zRearMatrix, zRearNormalMatrixX, zRearNormalMatrixY;
	double	LensIndex;
	double	zMinSurface;
	double LensRadMm;
	double cx, cy;
	string ErrorMsg;

public:
	CRayTracer()
	{
		cx = 0.; cy = 0.;
		DataReady = false;	
	};
	bool	PrepareByProducer(CLensSurfProducer *pLensProd, double LensDiam, double RefrIndex, 
								double LensGeomCenterX, double LensGeomCenterY,
								double EllipticVertToHorizRatio = 1., double *AverageZLens = NULL);
	bool	TraceVertRay(double x, double y, CLine3D &ResLine, double PrecisParam = 0.4,
						double SecSurfOffsetX = 0., double SecSurfOffsetY = 0.);
	bool	TraceRay(CLine3D &InLine, CLine3D &ResLine, double PrecisParam = 0.4,
						double SecSurfOffsetX = 0., double SecSurfOffsetY = 0.);

	void	CalcBeamTransmission(CVertBeam &InBeam, CBeam &OutBeam);
	bool	IsReady(){ return DataReady; };
	double	CalcPointDiop(double x, double y, double PrecisParam = 0.4);
	string	GetLastErrorString(){ return ErrorMsg; };
};

class CFuncOf2Args
{
public:
	virtual double CalcFunc(double x, double y, bool *pArgValid) = 0;
};

enum EOptimizationResult
{
	orFailureNoSolution = -1,
	orFailureOutOfRange = 0,
	orSuccess = 1
};

class CFuncOptimizer // ��������� ����� ��� ������� �� ���� ���������� (���������� ������� :) ),
					 // ���������� � 0
{
public:	
	static EOptimizationResult FindSolution(CFuncOf2Args *pFuncClass, double StartX, double StartY,
				double *pResX, double *pResY, double ErrorAllowed = 0.1, double StepVal = 1.);	
};

class CTestFunc : public CFuncOf2Args
{
public:
	double a, b, c, d, e, f, g, h, i, j;	
	CTestFunc()
		{c = f = i = 1.; a = b = d = e = g = h = 0.; j = 2.;};
	double CalcFunc(double x, double y, bool *pArgValid);
};

class CDirectLensProducer : public CLensSurfProducer
{
public:	
	bool CalcValues(CFtPointVector &InArray, CDoubleVector &FrontSurf, CDoubleVector &RearSurf);
};

class CLensDriverBaseProducer : public CLensSurfProducer
{
	CObjPtr<ILens> pLensDrv;
	bool fDataReady;
public:	
	bool CalcValues(CFtPointVector &InArray, CDoubleVector &FrontSurf, CDoubleVector &RearSurf);
	bool Prepare(ILens *pLensDriver);
	CLensDriverBaseProducer()
	{
		fDataReady = false;
	};
	bool IsReady()
	{
		return fDataReady;
	};
};


class CLensForPrismOptimization : public CFuncOf2Args
{
	CVec3D RefVect;
	CRayTracer RayTracer;
	bool fDataReady;
	double PrecParam;
	double LensRad;
public:
	CLensForPrismOptimization()
	{
		fDataReady = false; PrecParam = M_PI / 180.;
	};
	bool Prepare(CLensSurfProducer *pLensSurfSrc, double Prism, double PrismAxis,
										double LensDiam, double RefrIndex, 
										double LensCenterX, double LensCenterY);
																//���� � ��������!,
																//������ � �� �� �
	bool Solve(double RadPrecision, double *pResX, double *pResY);
				//���� � ��������!
	
	double CalcFunc(double x, double y, bool *pArgValid);
	BOOL IsReady()
	{return fDataReady;};
};
