#include "stdafx.h"
#include <math.h>
#include <float.h>
#include "RayTracer.h"
#include <assert.h>
//#include "UsefulThings.h"
//#include "iwslens.h"
//#include "DVConverter.h"
//#include "LensSurface.h"

const int GridStep = 1;
#define MOD_VECT(x, y, z)	(sqrt((x)*(x) + (y)*(y) + (z)*(z)))
#define MOD_VECT_SQ(x, y, z)	((x)*(x) + (y)*(y) + (z)*(z))

#ifdef _DEBUG
void PrintMatrixToFile(CDoubleMatrix &Matrix, char *FileName)
{
	FILE *Out = fopen(FileName,"w");
	int CurX, CurY, SizeX = Matrix.GetX(), SizeY = Matrix.GetY();
	double Temp;
	fprintf(Out,"SizeX=%d, SizeY=%d\n", SizeX, SizeY);
	for(CurY = -1; CurY < SizeY; CurY++){
		for(CurX = 0; CurX < SizeX; CurX++){
			if(CurY == -1)
				fprintf(Out, "%10d", CurX);
			else {
				Temp = Matrix[CurY][CurX];
				if(Temp != DBL_MAX)
					fprintf(Out, "%10.4f", Temp);
				else
					fprintf(Out, "%10s", "DBL_MAX");
			}
		}
		fprintf(Out,"\n");
	}
	fclose(Out);
}
#endif

bool CRayTracer::PrepareByProducer(CLensSurfProducer *pLensProd, double LensDiam, double RefrIndex, 
								   double LensGeomCenterX, double LensGeomCenterY,
								   double EllipticVertToHorizRatio,
								   double *AverageZLens)
{
	DataReady = false;
	double xMM, yMM, r; 
	int x, y;
	LensRadMm = LensDiam * 0.5;
	
	
	int i = 1, gx, gy;
	double GridX, GridY, GridZFront, GridZRear, h;
	bool fSucceeded = true;
	CDoubleMatrix	zFrontMatrixGrid;
	CDoubleMatrix	zRearMatrixGrid;
	CDoubleMatrix	zFrontDerMatrixX, zFrontDerMatrixY;
	CDoubleMatrix	zRearDerMatrixX, zRearDerMatrixY;

	CFtPointVector InArray;
	CDoubleVector FrontSurf, RearSurf;

	LensIndex = RefrIndex;

	cx = LensGeomCenterX, cy = LensGeomCenterY;
	int gdy, gdx = 0;
	for(yMM = -(int((LensRadMm * EllipticVertToHorizRatio) / GridStep))*GridStep, gdy = 0; yMM <= (int((LensRadMm * EllipticVertToHorizRatio) / GridStep))*GridStep; yMM += GridStep, gdy++){
		r = (int)sqrt((double)(LensRadMm*LensRadMm -
			(yMM / EllipticVertToHorizRatio) * (yMM / EllipticVertToHorizRatio)) + .5);
		for(xMM = -RoundDown((r-1)/GridStep)*GridStep; xMM <= RoundDown((r-1)/GridStep)*GridStep; xMM += GridStep){
			FTPOINT fptNew;
			fptNew.x = xMM + cx;
			fptNew.y = -yMM + cy;
			InArray.push_back(fptNew);
			if(yMM == 0)
				  gdx++;
		}
	}

	CFtPointVector::size_type PointsInGrid = InArray.size();
	BOOL bResult = pLensProd->CalcValues(InArray, FrontSurf, RearSurf);
	if(!bResult)
	{
		ErrorMsg = pLensProd->GetLastErrorString();
		return false;
	}

	assert((gdy % 2) || (gdx % 2));
	zFrontMatrixGrid.Create(gdy + 2, gdx + 2);
	zRearMatrixGrid.Create(gdy + 2, gdx + 2);
	xOffsetGrid = (gdx / 2) + 1; yOffsetGrid = (gdy / 2) + 1;

	assert(zFrontMatrixGrid.GetX() == zRearMatrixGrid.GetX() && zFrontMatrixGrid.GetY() == zRearMatrixGrid.GetY());
	for(y = 0; y < zFrontMatrixGrid.GetY(); y++)
		for(x = 0; x < zFrontMatrixGrid.GetX(); x++){
			zFrontMatrixGrid[y][x] = DBL_MAX;
			zRearMatrixGrid[y][x] = DBL_MAX;
		}

	zMinSurface = DBL_MAX;
	double zMaxSurface = -DBL_MAX;

	i = 0;
	for(yMM = -(int((LensRadMm * EllipticVertToHorizRatio) / GridStep))*GridStep; yMM <= (int((LensRadMm * EllipticVertToHorizRatio) / GridStep))*GridStep; yMM += GridStep){
		r = (int)sqrt((double)(LensRadMm*LensRadMm-(yMM / EllipticVertToHorizRatio) * (yMM / EllipticVertToHorizRatio)) + .5);
		for(xMM = -RoundDown((r-1)/GridStep)*GridStep; xMM <= RoundDown((r-1)/GridStep)*GridStep; xMM += GridStep){
//			Grid.GetItem(i++, &GridX, &GridY, &GridZRear, &GridZFront);
			GridX = InArray[i].x;
			GridY = InArray[i].y; 
			GridZRear = RearSurf[i];
			GridZFront = FrontSurf[i];
			i++;
			
			gy = Round((GridY - cy) / GridStep + yOffsetGrid);
			gx = Round((GridX - cx) / GridStep + xOffsetGrid);
			zFrontMatrixGrid[gy][gx] = GridZFront;
			zRearMatrixGrid[gy][gx] = GridZRear;
//			if(GridZFront < GridZRear)
//				fSucceeded = FALSE;
			if(GridZRear < zMinSurface)
				zMinSurface = GridZRear;
			if(GridZFront > zMaxSurface)
				zMaxSurface = GridZFront;
		}
	}

	if(!fSucceeded){
		return false;
	}
	
	if(AverageZLens)
		*AverageZLens = (zMaxSurface + zMinSurface) / 2.;

	h = GridStep;
	CalcDerMatrix(zFrontMatrixGrid, zFrontDerMatrixX, zFrontDerMatrixY, h);
	CalcDerMatrix(zRearMatrixGrid, zRearDerMatrixX, zRearDerMatrixY, h);

	CalcInterpNormalMatrix(zFrontDerMatrixX, zFrontNormalMatrixX, Round(LensRadMm), TRUE);
	CalcInterpNormalMatrix(zFrontDerMatrixY, zFrontNormalMatrixY, Round(LensRadMm), TRUE);

	CalcInterpNormalMatrix(zRearDerMatrixX, zRearNormalMatrixX, Round(LensRadMm), TRUE);
	CalcInterpNormalMatrix(zRearDerMatrixY, zRearNormalMatrixY, Round(LensRadMm), TRUE);

	CalcInterpNormalMatrix(zFrontMatrixGrid, zFrontMatrix, Round(LensRadMm), FALSE);
	CalcInterpNormalMatrix(zRearMatrixGrid, zRearMatrix, Round(LensRadMm), FALSE);

	DataReady = true;
	return true;
}
/*
BOOL CRayTracer::Prepare(ILensDriver &Lens, double *AverageZLens)
{
	DataReady = FALSE;
	double xMM, yMM, r; 
	int x, y;
	LensRadMm = Lens.GetDiameter() * 0.5;
	CRayTracer::cx = Lens.GetOptToGeomCenterDisplacementX();
	CRayTracer::cy = Lens.GetOptToGeomCenterDisplacementY();
	
	int i = 1, gx, gy;
	double GridX, GridY, GridZFront, GridZRear, h;
	BOOL fSucceeded = TRUE;
	CDoubleMatrix	zFrontMatrixGrid;
	CDoubleMatrix	zRearMatrixGrid;
	CDoubleMatrix	zFrontDerMatrixX, zFrontDerMatrixY;
	CDoubleMatrix	zRearDerMatrixX, zRearDerMatrixY;
	ILensEngineDriver LensEngine;
	LPDISPATCH lpdispEngine = NULL;
	IUnknown *pEngine = NULL;

	
	ILensSurfaceDriver Grid;
	if(!Grid.CreateDispatch(_T("WSLens.LensSurface"))){
//		ThrowException(1);
		ASSERT(0);
		return FALSE;
	}

	LensSurfPointArray PointArray;
	LensSurfPoint Point;
	Point.zFront = 0.;
	Point.zRear = 0.;

	LensIndex = Lens.GetRefIndex();
	Grid.Clear();
	double cx = Lens.GetCenterX(), cy = Lens.GetCenterY();
	int gdy, gdx = 0;
	for(yMM = -(int(LensRadMm/GridStep))*GridStep, gdy = 0; yMM <= (int(LensRadMm/GridStep))*GridStep; yMM += GridStep, gdy++){
		r = (int)sqrt((double)(LensRadMm*LensRadMm-yMM*yMM) + .5);
		for(xMM = -RoundDown((r-1)/GridStep)*GridStep; xMM <= RoundDown((r-1)/GridStep)*GridStep; xMM += GridStep){
//			Grid.Add(	xMM + cx,
//					   -yMM + cy,
//						0, 0);
			Point.X = xMM + cx;
			Point.Y =-yMM + cy;

			PointArray.Add(Point);
			
			if(yMM == 0)
				  gdx++;
		}
	}

	double (*DArr)[4] = NULL;
	int cElements = PointArray.GetSize();
	
	if((DArr = (double (*)[4])new double[4 * cElements]) == NULL)
		return FALSE;

	for(i = 0; i < cElements; i++)
	{
		Point = PointArray.GetAt(i);
		DArr[i][0] = Point.X;
		DArr[i][1] = Point.Y;
		DArr[i][2] = 0.;
		DArr[i][3] = 0.;
	}
	PointArray.RemoveAll();

	VARIANT Variant;

	if(DoubleArray2Variant(DArr, cElements, &Variant) != dvcSuccess)
	{
		delete []DArr;
		return FALSE;
	}

	delete []DArr;

	TRY {
		if(!Grid.AddArray(Variant))
			return FALSE;
		
		if(Lens.GetEngineUsageMask() & 2)
			pEngine = (IUnknown *)Lens.GetEngine2();
		else
			pEngine = (IUnknown *)Lens.GetEngine1();

		if(pEngine)
		{
			pEngine->QueryInterface(IID_ILensEngine, (LPVOID *)&lpdispEngine);
			pEngine->Release();
		}

		LensEngine.AttachDispatch(lpdispEngine);
		
		if(	!LensEngine.m_lpDispatch ||
			!(LensEngine.GetSupportedMethods() & 2) ||
			!LensEngine.CalcSurface(Lens.m_lpDispatch, Grid.m_lpDispatch)){
				return FALSE;
		}

		if(!Grid.GetArray(&Variant) ||
			Variant2DoubleArray(Variant, &DArr, &cElements) != dvcSuccess)
		{
			if(DArr)
				delete []DArr;
			return FALSE;
		}

		VariantClear(&Variant);

	}
	CATCH_ALL(e)
	{
		return FALSE;
	}
	END_CATCH_ALL


	ASSERT((gdy % 2) || (gdx % 2));
	zFrontMatrixGrid.Create(gdy + 2, gdx + 2);
	zRearMatrixGrid.Create(gdy + 2, gdx + 2);
	xOffsetGrid = (gdx / 2) + 1; yOffsetGrid = (gdy / 2) + 1;

	ASSERT(zFrontMatrixGrid.GetX() == zRearMatrixGrid.GetX() && zFrontMatrixGrid.GetY() == zRearMatrixGrid.GetY());
	for(y = 0; y < zFrontMatrixGrid.GetY(); y++)
		for(x = 0; x < zFrontMatrixGrid.GetX(); x++){
			zFrontMatrixGrid[y][x] = DBL_MAX;
			zRearMatrixGrid[y][x] = DBL_MAX;
		}

	zMinSurface = DBL_MAX;
	double zMaxSurface = -DBL_MAX;

	i = 0;
	for(yMM = -(int(LensRadMm/GridStep))*GridStep; yMM <= (int(LensRadMm/GridStep))*GridStep; yMM += GridStep){
		r = (int)sqrt((double)(LensRadMm*LensRadMm-yMM*yMM) + .5);
		for(xMM = -RoundDown((r-1)/GridStep)*GridStep; xMM <= RoundDown((r-1)/GridStep)*GridStep; xMM += GridStep){
//			Grid.GetItem(i++, &GridX, &GridY, &GridZRear, &GridZFront);
			GridX = DArr[i][0];
			GridY = DArr[i][1];
			GridZRear = DArr[i][2];
			GridZFront = DArr[i][3];
			i++;

			if(!(fabs(GridY - cy) > 2 || fabs(GridX - cx) > 2))
			{
				int t = 9;
			}
			
			gy = Round((GridY - cy) / GridStep + yOffsetGrid);
			gx = Round((GridX - cx) / GridStep + xOffsetGrid);
			zFrontMatrixGrid[gy][gx] = GridZFront;
			zRearMatrixGrid[gy][gx] = GridZRear;
			if(GridZFront < GridZRear)
				fSucceeded = FALSE;
			if(GridZRear < zMinSurface)
				zMinSurface = GridZRear;
			if(GridZFront > zMaxSurface)
				zMaxSurface = GridZFront;
		}
	}

	delete []DArr;

	if(!fSucceeded){
		return FALSE;
	}
	
	if(AverageZLens)
		*AverageZLens = (zMaxSurface + zMinSurface) / 2.;

	h = GridStep;
	CalcDerMatrix(zFrontMatrixGrid, zFrontDerMatrixX, zFrontDerMatrixY, h);
	CalcDerMatrix(zRearMatrixGrid, zRearDerMatrixX, zRearDerMatrixY, h);


	CalcInterpNormalMatrix(zFrontDerMatrixX, zFrontNormalMatrixX, Round(LensRadMm), TRUE);
	CalcInterpNormalMatrix(zFrontDerMatrixY, zFrontNormalMatrixY, Round(LensRadMm), TRUE);


	CalcInterpNormalMatrix(zRearDerMatrixX, zRearNormalMatrixX, Round(LensRadMm), TRUE);
	CalcInterpNormalMatrix(zRearDerMatrixY, zRearNormalMatrixY, Round(LensRadMm), TRUE);


	CalcInterpNormalMatrix(zFrontMatrixGrid, zFrontMatrix, Round(LensRadMm), FALSE);
	CalcInterpNormalMatrix(zRearMatrixGrid, zRearMatrix, Round(LensRadMm), FALSE);


	DataReady = TRUE;
	return TRUE;
}
*/
void CalcDerMatrix(CDoubleMatrix &InMatrix, CDoubleMatrix &DerMatrixX, CDoubleMatrix &DerMatrixY, double h)
{
	assert(h > 0.);

	int x, y, dx = InMatrix.GetX(), dy = InMatrix.GetY();

	if(	!DerMatrixX.Create(dy, dx) ||
		!DerMatrixY.Create(dy, dx))
		return;

	for(y = 0; y < dy; y++)
		for(x = 0; x < dx; x++){
			DerMatrixX[y][x] = DBL_MAX;
			DerMatrixY[y][x] = DBL_MAX;
		}

	CalcDerMatrixX(InMatrix, DerMatrixX, h);
	CalcDerMatrixY(InMatrix, DerMatrixY, h);
}

#pragma optimize("y", off)
void CalcDerMatrixX(CDoubleMatrix &InMatrix, CDoubleMatrix &DerMatrixX, double h)
{
	int x, y, dx = InMatrix.GetX(), dy = InMatrix.GetY(), mode;
	double elem0, elem1, elem2;
	
	mode = 0;
	for(y = 1; y < dy-1; y++)
		for(x = 1; x < dx-1; x++){
			if(mode == 0){
				elem0 = InMatrix[y][x];
				elem1 = InMatrix[y][x+1];
			}
			if(elem0 != DBL_MAX && elem1 != DBL_MAX){
				if(mode == 0){
					DerMatrixX[y][x] = (-elem0 + elem1) / h;
					mode = 1;
				}
				else {
					elem2 = InMatrix[y][x+1];
					if(elem2 != DBL_MAX){
						DerMatrixX[y][x] = (-elem0 + elem2) / (2 * h);
						elem0 = elem1;
						elem1 = elem2;
					}
					else {
						DerMatrixX[y][x] = (-elem0 + elem1) / h;
						mode = 0;
					}
				}
			}
		}
}

void CalcDerMatrixY(CDoubleMatrix &InMatrix, CDoubleMatrix &DerMatrixY, double h)
{
	int x, y, dx = InMatrix.GetX(), dy = InMatrix.GetY(), mode;
	double elem0, elem1, elem2;
	
	mode = 0;
	for(x = 1; x < dx-1; x++)
		for(y = 1; y < dy-1; y++){
			if(mode == 0){
				elem0 = InMatrix[y][x];
				elem1 = InMatrix[y+1][x];
			}
			if(elem0 != DBL_MAX && elem1 != DBL_MAX){
				if(mode == 0){
					DerMatrixY[y][x] = (-elem0 + elem1) / h;
					mode = 1;
				}
				else {
					elem2 = InMatrix[y+1][x];
					if(elem2 != DBL_MAX){
						DerMatrixY[y][x] = (-elem0 + elem2) / (2 * h);
						elem0 = elem1;
						elem1 = elem2;
					}
					else {
						DerMatrixY[y][x] = (-elem0 + elem1) / h;
						mode = 0;
					}
				}
			}
		}
}
#pragma optimize("", on)

void CalcInterpNormalMatrix(CDoubleMatrix &DerMatrix, CDoubleMatrix &NormalMatrix, int size, BOOL fInvert)
{
	if(!NormalMatrix.Create(2*size-1, 2*size-1, TRUE))
		return;

	int x, y, r, dx, dy;
	double gy, gx;
	int xOffsetGrid = ((dx = DerMatrix.GetX()) - 2) / 2 + 1, 
			yOffsetGrid = ((dy = DerMatrix.GetY()) - 2) / 2 + 1;

	CDoubleMatrix DerMatrixEx, DerMatrixExEx;
	if(	!DerMatrixEx.Create(dy, dx) ||
		!DerMatrixExEx.Create(dy, dx))
		return;
	
	FillMatrixWithDBL_MAX(DerMatrixEx);
	
	DerMatrixExEx = DerMatrix;

	int mode = 0;
	for(y = 0; y < dy; y++)
		for(x = 0; x < dx; x++)
			if(mode == 0){
				if(DerMatrix[y][x] != DBL_MAX){
					DerMatrixEx[y][x-1] = DerMatrix[y][x];
					mode = 1;
				}
			}
			else {
				if(DerMatrix[y][x] == DBL_MAX){
					DerMatrixEx[y][x] = DerMatrix[y][x-1];
					mode = 0;
				}
			}
	mode = 0;
	for(x = 0; x < dx; x++)
		for(y = 1; y < dy; y++)
			if(mode == 0){
				if(DerMatrix[y][x] != DBL_MAX){
					if(DerMatrixEx[y-1][x] == DBL_MAX)
						DerMatrixEx[y-1][x] = DerMatrix[y][x];
					else
						DerMatrixEx[y-1][x] = (DerMatrix[y][x] + DerMatrixEx[y-1][x]) / 2.;
					mode = 1;
				}
			}
			else {
				if(DerMatrix[y][x] == DBL_MAX){
					if(DerMatrixEx[y][x] == DBL_MAX)
						DerMatrixEx[y][x] = DerMatrix[y-1][x];
					else
						DerMatrixEx[y][x] = (DerMatrix[y-1][x] + DerMatrixEx[y][x]) / 2.;
					mode = 0;
				}
			}
	
	for(y = 0; y < dy; y++)
		for(x = 0; x < dx; x++)
			if(DerMatrixEx[y][x] != DBL_MAX){
				if(DerMatrixExEx[y][x] == DBL_MAX)
					DerMatrixExEx[y][x] = DerMatrixEx[y][x];
				else
					DerMatrixExEx[y][x] += DerMatrixEx[y][x];
			}


	BOOL g0, g1, g2, g3;
	double d0, d1, d2, d3;
	for(y = 0; y < dy; y++)
		for(x = 0; x < dx; x++){
			if(DerMatrix[y][x] == DBL_MAX){
				g0 = (y > 0 && (d0 = DerMatrixEx[y-1][x]) != DBL_MAX);
				g1 = (x < dx-1 && (d1 = DerMatrixEx[y][x+1]) != DBL_MAX);
				g2 = (y < dy-1 && (d2 = DerMatrixEx[y+1][x]) != DBL_MAX);
				g3 = (x > 0 && (d3 = DerMatrixEx[y][x-1]) != DBL_MAX);

				if(g0 && g1)
					DerMatrixExEx[y][x] = (d0 + d1) / 2.;
				else if(g1 && g2)
					DerMatrixExEx[y][x] = (d1 + d2) / 2.;
				else if(g2 && g3)
					DerMatrixExEx[y][x] = (d2 + d3) / 2.;
				else if(g3 && g0)
					DerMatrixExEx[y][x] = (d3 + d0) / 2.;
			}
		}

	int inv = (fInvert)?-1:1;
	for(y = -(size-1); y <= (size-1); y++){
		r = (int)sqrt((double)(size*size-y*y) + .5);
		for(x = -(r-1); x <= (r-1); x++){
			gy = y / (double)GridStep + yOffsetGrid;
			gx = x / (double)GridStep + xOffsetGrid;
			NormalMatrix[y+size-1][x+size-1] = inv * Interp2(DerMatrixExEx, gx, gy);
		}
	}
}

void FillMatrixWithDBL_MAX(CDoubleMatrix &m)
{
	for(int y = 0; y < m.GetY(); y++)
		for(int x = 0; x < m.GetX(); x++)
			m[y][x] = DBL_MAX;
}

#define Interp1(t1,t2,f1,f2,t) (f1+(t-t1)*(f2-f1)/(t2-t1))

#define SHIFT	10

COLORREF Interp2(CColorMatrix32 &Pict,FType xx,FType yy)
{
	int	x=(int)(xx*(1<<SHIFT)),y=(int)(yy*(1<<SHIFT));

	int x1=x>>SHIFT,x2=x1+1,y1=y>>SHIFT,y2=y1+1;
	int x1x=x1<<SHIFT,x2x=x2<<SHIFT,y1x=y1<<SHIFT,y2x=y2<<SHIFT;
	int ValueX1,ValueX2,f1,f2,f3,f4;
	int	R,G,B;
	RGBQUAD tr1,tr2,tr3,tr4;

	tr1=Pict[y1][x1];
	tr2=Pict[y1][x2];
	tr3=Pict[y2][x1];
	tr4=Pict[y2][x2];

	f1=tr1.rgbRed<<SHIFT;
	f2=tr2.rgbRed<<SHIFT;
	f3=tr3.rgbRed<<SHIFT;
	f4=tr4.rgbRed<<SHIFT;
	ValueX1=Interp1(x1x,x2x,f1,f2,x);
	ValueX2=Interp1(x1x,x2x,f3,f4,x);
	R=Interp1(y1x,y2x,ValueX1,ValueX2,y);
	f1=tr1.rgbGreen<<SHIFT;
	f2=tr2.rgbGreen<<SHIFT;
	f3=tr3.rgbGreen<<SHIFT;
	f4=tr4.rgbGreen<<SHIFT;
	ValueX1=Interp1(x1x,x2x,f1,f2,x);
	ValueX2=Interp1(x1x,x2x,f3,f4,x);
	G=Interp1(y1x,y2x,ValueX1,ValueX2,y);
	f1=tr1.rgbBlue<<SHIFT;
	f2=tr2.rgbBlue<<SHIFT;
	f3=tr3.rgbBlue<<SHIFT;
	f4=tr4.rgbBlue<<SHIFT;
	ValueX1=Interp1(x1x,x2x,f1,f2,x);
	ValueX2=Interp1(x1x,x2x,f3,f4,x);
	B=Interp1(y1x,y2x,ValueX1,ValueX2,y);
	return RGB(R>>SHIFT,G>>SHIFT,B>>SHIFT);//(ValueYX+ValueXY)/2;
}

double Interp2(CDoubleMatrix &m, double x, double y)
{
	int dx = m.GetX(), dy = m.GetY();
	
	int x1 = (int)x;
	if(x1 >= dx - 1)
		x1 = dx - 2;
	if(x1 < 0)
		x1 = 0;
	double dx1 = x - x1;

	int y1 = (int)y;
	if(y1 >= dy - 1)
		y1 = dy - 2;
	if(y1 < 0)
		y1 = 0;
	double dy1 = y - y1;

	return (
		(1. - dx1) * ( m[y1][x1] + dy1 * ( m[y1 + 1][x1] - m[y1][x1] ) ) +
		dx1 * ( m[y1][x1 + 1] + dy1 * ( m[y1 + 1][x1 + 1] - m[y1][x1 + 1] ) )
		);
}

double CRayTracer::CalcPointDiop(double x, double y, double PrecisParam)
{
	if(_hypot(x, y) > LensRadMm)
	{
		return CalcPointDiopFailed;
	}

	FTPOINT CenPt;
	CenPt.x = x;
	CenPt.y = y;
	CVertBeam InBeam;
	InBeam.AddBeamPart(CenPt, 0., 2. * M_PI, 2, 
							1.1, 1.1, 1);
	CBeam ResBeam;
	CSphere Sph;
	CalcBeamTransmission(InBeam, ResBeam);
	double AvRad, AvSqRad, Result;
	if(!ResBeam.CalcKnotSphere(Sph, &AvRad, &AvSqRad))
	{
		return CalcPointDiopFailed;
	}
	if(fabs(Sph.Cen.pz) > EPS)
	{
		Result = (-1000.)/Sph.Cen.pz;
		if(fabs(Result) > 20)
		{
//			int t = 1;
		}
		Result = SaturConv(Result, 20);
		return Result;
	}
	else
		return 0.;
}

double SaturConv(double Param, double Limiter)
{
	const double M_HALF_PI = M_PI * 0.5;
	double Res = Limiter * atan(Param * M_HALF_PI / Limiter) / M_HALF_PI;
	return Res;
}

bool CRayTracer::TraceVertRay(double x, double y, CLine3D &ResLine, double PrecisParam,
							  double SecSurfOffsetX, double SecSurfOffsetY)
{
	CLine3D InLine(x, y, 0., 0., 0., 1.);
	return TraceRay(InLine, ResLine, PrecisParam, SecSurfOffsetX, SecSurfOffsetY);
}

bool CRayTracer::TraceRay(CLine3D &InLine, CLine3D &ResLine, double PrecisParam,
							  double SecSurfOffsetX, double SecSurfOffsetY)
{
	if(!DataReady)
		return false;

	double x = InLine.px;
	double y = InLine.py;
	double Vx = InLine.vx, Vy = InLine.vy, Vz = InLine.vz;

	x -= cx;
	y -= cy;

	// _	_  _      _	  _	  _	  _		  _	 _
	// X = (V, N) / (|N|*|N|) N - V, and (V, N) = 1
	// _
	// V = (0, 0, 1)
	// _
	// N = (Nx, Ny, 1)

	double Nx, Ny, Xx, Xy, Xz;
	int dx, dy;
	double mx = (dx = (zFrontNormalMatrixX.GetX() / 2)) + x;
	double my = (dy = (zFrontNormalMatrixY.GetY() / 2)) + y;

	Nx = Interp2(zFrontNormalMatrixX, mx, my);
	Ny = Interp2(zFrontNormalMatrixY, mx, my);
	
	double VN = Vx * Nx + Vy * Ny + Vz * 1.;

	double ModNSq = MOD_VECT_SQ(Nx, Ny, 1), ModN = sqrt(ModNSq);

	Xx = VN * Nx / ModNSq - Vx;
	Xy = VN * Ny / ModNSq - Vy;
	Xz = VN * 1. / ModNSq - Vz;

	double ModXSq = MOD_VECT_SQ(Xx, Xy, Xz);

	//			 _	 _		 _	 _
	// sin a = | V x N | / (|V|*|N|)
	//

//	SinA = MOD_VECT(Vy - Vz * Ny, Vz * Nx - Vx, Vx * Ny - Vy * Nx) / (ModN * MOD_VECT(Vx, Vy, Vz));

	//
	// sin b = Sin a / index
	//

	double SinB = MOD_VECT(Vy - Vz * Ny, Vz * Nx - Vx, Vx * Ny - Vy * Nx) / (ModN * MOD_VECT(Vx, Vy, Vz) * LensIndex);

	//
	// tg b = sin b / sqrt(1 - sin b * sin b)
	//

//	double TgB = SinB / sqrt(1 - SinB * SinB);

	// _	_	  _			   _   _
	// R = -N + (|N| * tg b / |X|) X
	//

	double k, Rx, Ry, Rz;

	if(ModXSq < DBL_EPSILON)
		k = 0.;
	else
		k = SinB * sqrt(ModNSq / ((1 - SinB * SinB) * ModXSq));

	Rx = -Nx + k * Xx;
	Ry = -Ny + k * Xy;
	Rz = -1. + k * Xz;

//            P - point
//     _____|___________
//    /	  _	\           \					  _   _
//   |	  R	  \	  lens	 |	  - deflected ray P + Rt
//   |   ________\____	 |	 
//   |  /        |\	  \	 |
//   |/			 |  \ 	\|
//   _ _ _ _ _ _ _ _ _\_ _ _  - zMinSurface
//					 Po \
//						  \
//

	double P0x, P0y, P0z, P1x, P1y, P1z, P2x, P2y, P2z;
	P0x = x + SecSurfOffsetX;//PixtoMM(x);
	P0y = y + SecSurfOffsetY;//PixtoMM(y);
	
	P0z = Interp2(zFrontMatrix, mx, my);

//	����� ���� ����� (P0x, P0y, P0z) � ������������ ������ (Rx, Ry, Rz)
//
	double To, T1;

	To = (zMinSurface - P0z) / Rz, T1 = DBL_MAX;

	double A = Rx * Rx + Ry * Ry;
	double B = 2 * (P0x * Rx + P0y * Ry);
	double C = P0x * P0x + P0y * P0y - LensRadMm * LensRadMm;//PixtoMM(m_LensRadiusPix) * PixtoMM(m_LensRadiusPix);
	double D = B * B - 4 * A * C;
	if (D >= 0. && fabs(A) > DBL_EPSILON)
		T1 = (-B + sqrt(D)) / (2 * A);

	To = min(To, T1);

	P1x = P0x + Rx * To;
	P1y = P0y + Ry * To;
	P1z = P0z + Rz * To;

	double delta;
	BOOL s1;
#ifdef _DEBUG
	BOOL s0, s2;
#endif
	do {
		P2x = (P0x + P1x) / 2.;	
		P2y = (P0y + P1y) / 2.;	
		P2z = (P0z + P1z) / 2.;	

#ifdef _DEBUG
		s0 = (P0z - Interp2(zRearMatrix, P0x + dx, P0y + dy)) >= 0.;
#endif
		s1 = (P2z - Interp2(zRearMatrix, P2x + dx, P2y + dy)) >= 0.;
#ifdef _DEBUG
		s2 = (P1z - Interp2(zRearMatrix, P1x + dx, P1y + dy)) >= 0.;
#endif
		if(s1){
			delta = fabs(P2x - P0x) + fabs(P2y - P0y) + fabs(P2z - P0z);
			P0x = P2x;
			P0y = P2y;
			P0z = P2z;
		}
		else {
			delta = fabs(P2x - P1x) + fabs(P2y - P1y) + fabs(P2z - P1z);
			P1x = P2x;
			P1y = P2y;
			P1z = P2z;
		}
	} while(delta > PrecisParam);

	//	_    _
	//	V = -R
	//	_
	//  N = (Nx, Ny, 1)
	//

	Rx = -Rx;
	Ry = -Ry;
	Rz = -Rz;

	double Kx, Ky, Kz;
	Kx = P2x;
	Ky = P2y;
	Kz = P2z;
	
	Nx = Interp2(zRearNormalMatrixX, Kx + dx, Ky + dy);
	Ny = Interp2(zRearNormalMatrixY, Kx + dx, Ky + dy);

	ModNSq = MOD_VECT_SQ(Nx, Ny, 1);
	ModN = sqrt(ModNSq);

	VN = Rx * Nx + Ry * Ny + Rz * 1.;

	Xx = VN * Nx / ModNSq - Rx;
	Xy = VN * Ny / ModNSq - Ry;
	Xz = VN * 1. / ModNSq - Rz;

	ModXSq = MOD_VECT_SQ(Xx, Xy, Xz);

	SinB = MOD_VECT(Ry - Rz * Ny, Rz * Nx - Rx, Rx * Ny - Ry * Nx) * LensIndex / (ModN * MOD_VECT(Rx, Ry, Rz));

	if(ModXSq < DBL_EPSILON)
		k = 0.;
	else
		k = SinB * sqrt(ModNSq / ((1 - SinB * SinB) * ModXSq));

	Rx = -Nx + k * Xx;
	Ry = -Ny + k * Xy;
	Rz = -1. + k * Xz;

	//					   _   _
	// Final deflected ray K + Rt
	//

	ResLine.SetData(P2x - SecSurfOffsetX + cx, P2y - SecSurfOffsetY + cy, P2z, Rx, Ry, Rz);

	return (SinB <= 1.);
}

double CLine3D::FindDistanceToPoint(CPoint3D &Point, CPoint3D &ResPoint)
{
	double SqrVectLen = SqrLenVec3D(vx, vy, vz);
	if(SqrVectLen < EPS)
		return -1;
	double t0 = ( vx * (Point.px - px) + vy * (Point.py - py) + vz * (Point.pz - pz) ) / SqrVectLen;

	ResPoint.px = px + t0 * vx;
	ResPoint.py = py + t0 * vy;
	ResPoint.pz = pz + t0 * vz;

	return ResPoint.DistanceTo(Point);
}

double CLine3D::FindNearestPoint(CLine3D &AnotherLine, CPoint3D &ResPoint)
{
	double	a1 = vx, b1 = vy, c1 = vz, 
			u1 = px, v1 = py, w1 = pz, 
			a2 = AnotherLine.vx, b2 = AnotherLine.vy, c2 = AnotherLine.vz, 
			u2 = AnotherLine.px, v2 = AnotherLine.py, w2 = AnotherLine.pz, 

			s_a1 = - a1 * a1 - b1 * b1 - c1 * c1, s_a2 = - a1 * a2 - b1 * b2 - c1 * c2,
			s_b1 = a1 * a2 + b1 * b2 + c1 * c2, s_b2 = a2 * a2 + b2 * b2 + c2 * c2,
			s_c1 = a1 * (u2 - u1) + b1 * (v2 - v1) + c1 * (w2 - w1),
			s_c2 = a2 * (u2 - u1) + b2 * (v2 - v1) + c2 * (w2 - w1),
			
			t1, t2,
			
			ResDist;
	int SolNum = SolveEqSys2x2(s_a1, s_b1, s_c1, s_a2, s_b2, s_c2, t1, t2);
	CPoint3D pt1, pt2;
	if(SolNum == 2)
	{	//������� ����. �����, �.�. ������ ��������� ��� �����������
		GetPoint(t1, pt1);
		if(AnotherLine.IsPointOnLine(pt1))
		{	//��������� 
			ResDist = 0;
		}
		else
		{
			ResDist = -1;
		}
		ResPoint = pt1;
	}
	else if(SolNum == 1)
	{	//������� ����, �.�. ������ ������������ ��� ������������
		GetPoint(t1, pt1);
		AnotherLine.GetPoint(t2, pt2);
		ResDist = pt1.DistanceTo(pt2);
		if(ResDist < EPS)
		{	//������������ 

		}
		else
		{	//������������ 

		}
		ResPoint = pt1;
	}
	else
	{	// -- ����, ������� ���
		assert(0);
		ResDist = -1;
	}


	return ResDist;
}

bool CLine3D::IsPointOnLine(CPoint3D &Point)
{
	double	diffX = Point.px - px, 
			diffY = Point.py - py,
			diffZ = Point.pz - pz;
	double	ScProd = diffX * vx + diffY * vy + diffZ * vz,
			ModDiff = LenVec3D(diffX, diffY, diffZ),
			ModV = LenVec3D(vx, vy, vz);
	if(ModDiff < EPS)
		return true;
	if(ModV < EPS)
		return false;
	if(fabs(fabs(ScProd / (ModDiff * ModV)) - 1) < EPS)
		return true;
	else
		return false;
}

CLine3D& CLine3D::operator +=(CVec3D &vec)
{
	px += vec.vx;
	py += vec.vy;
	pz += vec.vz;

	return *this;
}

CLine3D& CLine3D::operator *=(double d)
{
	px *= d; py *= d; pz *= d;
	vx *= d; vy *= d; vz *= d;

	return *this;
}
			
CLine3D& CLine3D::operator /=(double d)
{
	if(fabs(d) > EPS)
	{
		px /= d; py /= d; pz /= d;
		vx /= d; vy /= d; vz /= d;
	}

	return *this;
}

double LenVec3D(double x, double y, double z)
{
	return sqrt(x * x + y * y + z * z);
}

double SqrLenVec3D(double x, double y, double z)
{
	return x * x + y * y + z * z;
}

void CLine3D::GetPoint(double Param, CPoint3D &Res)
{
	Res.px = px + vx * Param;
	Res.py = py + vy * Param;
	Res.pz = pz + vz * Param;
}

double CPoint3D::DistanceTo(CPoint3D &OtherPoint)
{
	return sqrt((px - OtherPoint.px) * (px - OtherPoint.px) + (py - OtherPoint.py) * (py - OtherPoint.py) +
		(pz - OtherPoint.pz) * (pz - OtherPoint.pz));
}

int SolveEqSys2x2(double a1, double b1, double c1, double a2, double b2, double c2, double &t1, double &t2)
{
	double det = a1 * b2 - a2 * b1, det2 = a2 * c1 - a1 * c2;
	if(fabs(det) < 1e-15)
	{
		if(fabs(det2) < 1e-15)
		{
			int SelA, SelB;
			double aSelected, bSelected;
			t1 = 0; t2 = 0;
			if(fabs(a1) > fabs(a2))
			{
				SelA = 1;		
				aSelected = a1;
			}
			else
			{
				SelA = 2;		
				aSelected = a2;
			}
			if(fabs(b1) > fabs(b2))
			{
				SelB = 1;		
				bSelected = b1;
			}
			else
			{
				SelB = 2;		
				bSelected = b2;
			}
			if(fabs(bSelected) > fabs(aSelected))
			{
				if(SelB == 1)
				{
					t2 = -c1 / b1;
				}
				else
				{
					t2 = -c2 / b2;
				}
			}
			else
			{
				if(SelA == 1)
				{
					t1 = -c1 / a1;
				}
				else
				{
					t1 = -c2 / a2;
				}
			}



			
			return 2;//����� �������, ���������� �����
		}
		else
			return 0;//������� ���
	}
	t2 = det2 / det;
	double det1 = b1 * c2 - b2 * c1;
	t1 = det1 / det;
	return 1;//���� �������
}

bool CVec3D::IsOrtog(CVec3D &OtherVec)
{
	return fabs(vx * OtherVec.vx + vy * OtherVec.vy + vz * OtherVec.vz) < 1e-6;
}

double CVec3D::CosOfAngle(CVec3D &OtherVec)
{
	double VecLen = LenVec3D(vx, vy, vz), OtherVecLen = LenVec3D(OtherVec.vx, OtherVec.vy, OtherVec.vz);
	return (vx * OtherVec.vx + vy * OtherVec.vy + vz * OtherVec.vz) / (VecLen * OtherVecLen);
}

void CRayTracer::CalcBeamTransmission(CVertBeam &InBeam, CBeam &OutBeam)
{
	CFtPointVector::size_type NumOfLines = InBeam.Lines.size();
	
	OutBeam.Lines.resize(NumOfLines);
	OutBeam.Validity.resize(NumOfLines);
	if(!DataReady)
		return;
	CFtPointVector::size_type i, nSize = InBeam.Lines.size();
	for(i = 0; i < nSize; i++)
	{
		CLine3D TempLine;
		if(TraceVertRay(InBeam.Lines[i].x, InBeam.Lines[i].y, TempLine))
		{
			OutBeam.Lines[i] = TempLine;
			OutBeam.Validity[i] = TRUE;
		}
		else
		{
			OutBeam.Validity[i] = FALSE;
		}
	}

}

void CBeam::Clear()
{
	Lines.clear();
}

int CBeam::FindNearestLineToPoint(CPoint3D &Point)
{
	int IndOfMin = -1;
	double CurDist, MinDist = 0;
	CLine3DVector::size_type CurInd, nSize = Lines.size();
	for(CurInd = 0; CurInd < nSize; CurInd++)
	{
		CPoint3D TempPoint;
		CurDist = Lines[CurInd].FindDistanceToPoint(Point, TempPoint);
		if(IndOfMin < 0 || CurDist < MinDist)
		{
			MinDist = CurDist;
			IndOfMin = CurInd;
		}
	}
	return IndOfMin;
}

bool CBeam::CalcKnotSphere(CSphere &ResSph, double *pAvRad, double *pAvSqRad)
{
	CPoint3DVector MutualInSectPoints;
	double AveragedX = 0., AveragedY = 0., AveragedZ = 0.;
	bool CheckValidity = Validity.size() == Lines.size();
	long i, j;
	for(i = 0; i < Lines.size(); i++)
	{
		if(CheckValidity && !Validity[i])
			continue;
		for(j = i + 1; j < Lines.size(); j++)
		{
			if(CheckValidity && !Validity[j])
				continue;
			CPoint3D CurInSectPoint;
			double CurDist = Lines[i].FindNearestPoint(Lines[j], CurInSectPoint);
			if(CurDist >= 0)
			{
				MutualInSectPoints.push_back(CurInSectPoint);
				AveragedX += CurInSectPoint.px;
				AveragedY += CurInSectPoint.py;
				AveragedZ += CurInSectPoint.pz;
			}
		}
	}
	CPoint3DVector::size_type NumOfInSectPoints = MutualInSectPoints.size();
	if(NumOfInSectPoints < 1)
		return false;
	AveragedX /= NumOfInSectPoints;
	AveragedY /= NumOfInSectPoints;
	AveragedZ /= NumOfInSectPoints;
	CPoint3D CenPoint(AveragedX, AveragedY, AveragedZ);
	double MaxRad = 0, RadSqSum = 0, RadSum = 0;

	for(i = NumOfInSectPoints - 1; i >= 0; i--)
	{
		double CurRad = CenPoint.DistanceTo(MutualInSectPoints[i]);
		if(CurRad > MaxRad)
		{
			MaxRad = CurRad;
		}
		RadSqSum += CurRad * CurRad;
		RadSum += CurRad;
	}
	ResSph.Rad = MaxRad;
	ResSph.Cen = CenPoint;
	RadSum /= NumOfInSectPoints;
	RadSqSum = sqrt(RadSqSum/NumOfInSectPoints);
	if(pAvRad)
		*pAvRad = RadSum;
	if(pAvSqRad)
		*pAvSqRad = RadSqSum;
	return true;
}

BOOL CVertBeam::AddBeamPart(FTPOINT &CenterPoint, double AngleMin, double AngleMax, int AngleSteps, 
							double RadMin, double RadMax, int RadSteps)
{
	if(RadSteps <= 0 || AngleSteps <= 0)
		return FALSE;
	double	CurRad = RadMin, RadStep = 0.,
			CurAngle = AngleMin, AngleStep = 0.;
	if(RadSteps > 1)
		RadStep = (RadMax - RadMin) / (RadSteps - 1);
	if(AngleSteps > 1)
		AngleStep = (AngleMax - AngleMin) / AngleSteps;
	for(int AngleInd = 0; AngleInd < AngleSteps; AngleInd++, CurAngle += AngleStep)
	{
		double BaseX = cos(CurAngle), BaseY = sin(CurAngle);
		for(int RadInd = 0; RadInd < RadSteps; RadInd++, CurRad += RadStep)
		{
			FTPOINT CurPt;
			CurPt.x = CenterPoint.x + CurRad * BaseX; 
			CurPt.y = CenterPoint.y + CurRad * BaseY; 
			Lines.push_back(CurPt);
		}
	}
	return TRUE;
}

EOptimizationResult CFuncOptimizer::FindSolution(CFuncOf2Args *pFunc,
								  double StartX, double StartY,
								  double *pResX, double *pResY,
								  double ErrorAllowed, double StepVal)
{
	double CurPointX = StartX, CurPointY = StartY, CurFuncVal,
			NeibXPointX, NeibXPointY, NeibXPVal, NeibXMVal, 
			NeibYPointX, NeibYPointY, NeibYPVal, NeibYMVal,
			HorizNeibOrder, VertNeibOrder,
			PartDerX, PartDerY, RecStepVal = 1. / StepVal,
			FullNeib1X, FullNeib1Y, FullNeib2X, FullNeib2Y, FullNeib3X, FullNeib3Y,
			FullNeib1Val, FullNeib2Val, FullNeib3Val;

	bool bSuccess, bSuccess2;
	int CurPointPos, Count = 0, MaxCount = 10;
	CurFuncVal = pFunc->CalcFunc(CurPointX, CurPointY, &bSuccess);
	if(!bSuccess)
		return orFailureOutOfRange;
	while(fabs(CurFuncVal) > ErrorAllowed)
	{
		if(Count > MaxCount)
			return orFailureNoSolution;
		NeibXPointX = CurPointX + StepVal;
		NeibXPointY = CurPointY;
		NeibXPVal = pFunc->CalcFunc(NeibXPointX, NeibXPointY, &bSuccess);
		NeibXPointX = CurPointX - StepVal;
		NeibXPointY = CurPointY;
		NeibXMVal = pFunc->CalcFunc(NeibXPointX, NeibXPointY, &bSuccess2);
		if(!bSuccess)
		{
			if(!bSuccess2)
				return orFailureOutOfRange;
			NeibXPVal = CurFuncVal;
			HorizNeibOrder = RecStepVal;
		}
		else
		{
			if(!bSuccess2)
			{
				NeibXMVal = CurFuncVal;
				HorizNeibOrder = RecStepVal;
			}
			else
				HorizNeibOrder = 0.5 * RecStepVal;
		}
		NeibYPointX = CurPointX;
		NeibYPointY = CurPointY + StepVal;
		NeibYPVal = pFunc->CalcFunc(NeibYPointX, NeibYPointY, &bSuccess);
		NeibYPointX = CurPointX;
		NeibYPointY = CurPointY - StepVal;
		NeibYMVal = pFunc->CalcFunc(NeibYPointX, NeibYPointY, &bSuccess2);
		if(!bSuccess)
		{
			if(!bSuccess2)
				return orFailureOutOfRange;
			NeibYPVal = CurFuncVal;
			VertNeibOrder = RecStepVal;
		}
		else
		{
			if(!bSuccess2)
			{
				NeibYMVal = CurFuncVal;
				VertNeibOrder = RecStepVal;
			}
			else
				VertNeibOrder = 0.5 * RecStepVal;
		}

		PartDerX = (NeibXPVal - NeibXMVal) * HorizNeibOrder;
		PartDerY = (NeibYPVal - NeibYMVal) * VertNeibOrder;
		double FullDerMod = _hypot(PartDerX, PartDerY) * RecStepVal;
		if(FullDerMod < EPS * EPS)
			return orFailureNoSolution;
		PartDerX /= FullDerMod;
		PartDerY /= FullDerMod;
		
		FullNeib3X = CurPointX + 2. * PartDerX;
		FullNeib3Y = CurPointY + 2. * PartDerY;
		FullNeib3Val = pFunc->CalcFunc(FullNeib3X, FullNeib3Y, &bSuccess);
		FullNeib2X = CurPointX + PartDerX;
		FullNeib2Y = CurPointY + PartDerY;
		FullNeib2Val = pFunc->CalcFunc(FullNeib2X, FullNeib2Y, &bSuccess2);
		if(!bSuccess || !bSuccess2)
		{
			FullNeib1X = CurPointX - PartDerX;
			FullNeib1Y = CurPointY - PartDerY;
			FullNeib1Val = pFunc->CalcFunc(FullNeib1X, FullNeib1Y, &bSuccess);
			if(!bSuccess2)
			{
				if(!bSuccess)
					return orFailureNoSolution;
				FullNeib2X = FullNeib1X;	
				FullNeib2Y = FullNeib1Y;
				FullNeib2Val = FullNeib1Val;
				FullNeib1X = CurPointX - 2. * PartDerX;
				FullNeib1Y = CurPointY - 2. * PartDerY;
				FullNeib1Val = pFunc->CalcFunc(FullNeib1X, FullNeib1Y, &bSuccess);
				if(!bSuccess)
					return orFailureNoSolution;
				//[1 2 3 - -]
				FullNeib3X = CurPointX;
				FullNeib3Y = CurPointY;
				FullNeib3Val = CurFuncVal;
				CurPointPos = 3;

			}
			else	//[? 2 3 4 -]
			{
				FullNeib3X = FullNeib2X;
				FullNeib3Y = FullNeib2Y;
				FullNeib3Val = FullNeib2Val;
				FullNeib2X = CurPointX;
				FullNeib2Y = CurPointY;
				FullNeib2Val = CurFuncVal;
				CurPointPos = 2;
			}
		}
		else	//[- - 3 4 5]
		{
			FullNeib1X = CurPointX;
			FullNeib1Y = CurPointY;
			FullNeib1Val = CurFuncVal;
			CurPointPos = 1;
		}
		
		double	SqPolyA = (FullNeib1Val + FullNeib3Val - 2. * FullNeib2Val) * 0.5,
				SqPolyB = (FullNeib3Val - FullNeib1Val) * 0.5,
				SqPolyC = FullNeib2Val,
				NewPointX, NewPointY, NewFuncVal;
		bSuccess = bSuccess2 = false;
		if(fabs(SqPolyA) < EPS)
		{
			if(fabs(SqPolyB) > EPS)
			{
				double t = - SqPolyB / SqPolyC;
				NewPointX = FullNeib2X + t * PartDerX;
				NewPointY = FullNeib2Y + t * PartDerY;
				NewFuncVal = pFunc->CalcFunc(NewPointX, NewPointY, &bSuccess);
			}
			else
				return orFailureNoSolution;
		}	
		else
		{
			double det = SqPolyB * SqPolyB - 4. * SqPolyA * SqPolyC;
			if(det >= 0.)
			{
				double	t1 = (-SqPolyB + sqrt(det)) / (2. * SqPolyA),
						t2 = (-SqPolyB - sqrt(det)) / (2. * SqPolyA),
						P1Val, P2Val;
				double	P1X = FullNeib2X + t1 * PartDerX, P1Y = FullNeib2Y + t1 * PartDerY, 
						P2X = FullNeib2X + t2 * PartDerX, P2Y = FullNeib2Y + t2 * PartDerY;
				P1Val = pFunc->CalcFunc(P1X, P1Y, &bSuccess);
				P2Val = pFunc->CalcFunc(P2X, P2Y, &bSuccess2);			
				if(!bSuccess)
				{
					if(!bSuccess2)
						det = -1.;
					else
					{
						NewPointX = P2X;
						NewPointY = P2Y;
						NewFuncVal = P2Val;
					}
				}
				else
				{
					if(bSuccess2)
					{
						if(fabs(P1Val) < fabs(P2Val))
						{
							NewPointX = P1X;
							NewPointY = P1Y;
							NewFuncVal = P1Val;
						}
						else
						{
							NewPointX = P2X;
							NewPointY = P2Y;
							NewFuncVal = P2Val;
						}
					}
					else
					{
						NewPointX = P1X;
						NewPointY = P1Y;
						NewFuncVal = P1Val;
					}
				}
			}
			if(det < 0.)
			{
				double	t1 = (-SqPolyB) / (2. * SqPolyA);
				double	P1X = FullNeib2X + t1 * PartDerX, P1Y = FullNeib2Y + t1 * PartDerY,
						P1Val = pFunc->CalcFunc(P1X, P1Y, &bSuccess);
				if(!bSuccess)
				{
					bSuccess2 = false;
				}
				else
				{
					NewPointX = P1X;
					NewPointY = P1Y;
					NewFuncVal = P1Val;
				}
			}
		}
		if(!bSuccess && !bSuccess2)
			return orFailureNoSolution;
//		double Delta = fabs(NewPointX - CurPointX) + fabs(NewPointY - CurPointY) + fabs(NewFuncVal - CurFuncVal);

		CurPointX = NewPointX;
		CurPointY = NewPointY;
		CurFuncVal = NewFuncVal;
		Count++;

	}
	*pResX = CurPointX;
	*pResY = CurPointY;
	return orSuccess;
}

double CTestFunc::CalcFunc(double x, double y, bool *pArgValid)
{
	if(fabs(x) + fabs(y) > 30.)
	{
		*pArgValid = false;
		return 0.;
	}
	*pArgValid = true;
	return c * _hypot(x - a, y - b) + f * ((x - d) * (x - d) + (y - e) * (y - e)) + j;
}



double CLensForPrismOptimization::CalcFunc(double x, double y, bool *pArgValid)
{
	if(!fDataReady || _hypot(x, y) > LensRad)
	{
		*pArgValid = false;
		return 0.;
	}
	CLine3D ResLine;
	if(!RayTracer.TraceVertRay(0, 0, ResLine, PrecParam, x, y))
	{
		*pArgValid = false;
		return 0.;
	}
	*pArgValid = true;
	CVec3D ResVec(ResLine.vx, ResLine.vy, ResLine.vz);
	return acos(RefVect.CosOfAngle(ResVec)); 
}

bool CLensForPrismOptimization::Prepare(CLensSurfProducer *pLensSurfSrc, double Prism, double PrismAxis,
										double LensDiam, double RefrIndex, 
										double LensCenterX, double LensCenterY)
{
	fDataReady = false;
	LensRad = LensDiam * 0.5;
	if(!RayTracer.PrepareByProducer(pLensSurfSrc,
										LensDiam, RefrIndex, 
										LensCenterX, LensCenterY))
		return false;
	double x = tan(Prism * 0.01) * 100., y = 0., z = -100.;
	y = x * sin(PrismAxis);
	x = x * cos(PrismAxis); 
	RefVect.vx = x;
	RefVect.vy = y;
	RefVect.vz = z;
	fDataReady = true;
	return true;
}

bool CLensDriverBaseProducer::CalcValues(CFtPointVector &InArray, CDoubleVector &FrontSurf, CDoubleVector &RearSurf)
{
	if(!fDataReady)
		return false;
//	CObjPtr<ILensEngine> LensEngine;
	CObjPtr<ILensEngine> pEngine;
	CObjPtr<ILensSurface> Grid;
	CFtPointVector::size_type i, PointsInGrid = InArray.size();
	vector<LensSurfPoint> DArr(PointsInGrid);

	vector<LensSurfPoint>::iterator it;
	for(i = 0, it = DArr.begin(); i < PointsInGrid; i++, ++it)
	{
		it->X = InArray[i].x;
		it->Y = InArray[i].y;
		it->zRear = 0.;
		it->zFront = 0.;
	}

    try {
        Grid.AttachDispatch(Create_ILensSurface());
		Grid->Clear();

		if(!Grid->AddArray(DArr))
			return false;

		if(pLensDrv->GetEngineUsageMask() & 2)
			pEngine.AttachDispatch(pLensDrv->GetEngine2());
		else
			pEngine.AttachDispatch(pLensDrv->GetEngine1());

		if(	!pEngine.m_p ||
			!(pEngine->GetSupportedMethods() & 2) ||
			!pEngine->CalcSurface(pLensDrv, Grid)){
				return false;
		}
	}
    catch(...)
	{
		return false;
	}

    vector<LensSurfPoint> * pDArr = Grid->GetArray();
	RearSurf.resize(PointsInGrid);
	FrontSurf.resize(PointsInGrid);
    
    for(i = 0, it = pDArr->begin(); it != pDArr->end(); ++it, i++)
	{
        RearSurf[i] = it->zRear;
        FrontSurf[i] = it->zFront;
	}

	return true;
}

bool CLensDriverBaseProducer::Prepare(ILens *pLensDriver)
{
	pLensDrv = pLensDriver;
	fDataReady = true;
	return true;
}

bool CLensForPrismOptimization::Solve(double RadPrecision, double *pResX, double *pResY)
{
	if(!fDataReady)
		return false;
	EOptimizationResult Res = CFuncOptimizer::FindSolution(this, 0., 0., pResX, pResY);
	return Res == orSuccess;
}
