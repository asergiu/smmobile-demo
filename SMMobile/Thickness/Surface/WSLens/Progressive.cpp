// Progressive.cpp : implementation file
//

#include "stdafx.h"
#include "Progressive.h"

/////////////////////////////////////////////////////////////////////////////
// CProgressive

CProgressive* CProgressive::CreateObject()
{
	CProgressive *pObject = new CProgressive;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

IProgressive* CProgressive::GetInterface()
{
	return static_cast<IProgressive*>(this);
}

CProgressive::CProgressive()
{
	m_farVisionCenterX = 0.;
	m_farVisionCenterY = 8.;
	m_farVisionMarkerDiam = 8.;
	m_nearVisionCenterX = -2.5;
	m_nearVisionCenterY = -15.;
	m_nearVisionMarkerDiam = 8.;
}

CProgressive::~CProgressive()
{
}

bool CProgressive::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	if(pPX->PX_Double("FarVisionCenterX", m_farVisionCenterX, 0.) == false)
		return false;
	if(pPX->PX_Double("FarVisionCenterY", m_farVisionCenterY, 8.) == false)
		return false;
	if(pPX->PX_Double("FarVisionMarkerDiam", m_farVisionMarkerDiam, 8.) == false)
		return false;
	if(pPX->PX_Double("NearVisionCenterX", m_nearVisionCenterX, -2.5) == false)
		return false;
	if(pPX->PX_Double("NearVisionCenterY", m_nearVisionCenterY, -15.) == false)
		return false;
	if(pPX->PX_Double("NearVisionMarkerDiam", m_nearVisionMarkerDiam, 8.) == false)
		return false;

	return true;
}

IProgressive * CProgressive::Clone() 
{
	CProgressive *pNewObject = CProgressive::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}

double CProgressive::GetFarVisionCenterX()
{
	return m_farVisionCenterX;
}
bool CProgressive::SetFarVisionCenterX(double newValue)
{
	if(!TestRange(newValue, -25., 25., 1))
		return false;
	m_farVisionCenterX = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

double CProgressive::GetFarVisionCenterY()
{
	return m_farVisionCenterY;
}
bool CProgressive::SetFarVisionCenterY(double newValue)
{
	if(!TestRange(newValue, -25., 25., 1))
		return false;
	m_farVisionCenterY = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

double CProgressive::GetFarVisionMarkerDiam()
{
	return m_farVisionMarkerDiam;
}
bool CProgressive::SetFarVisionMarkerDiam(double newValue)
{
	if(!TestRange(newValue, 0., 25., 1))
		return false;
	m_farVisionMarkerDiam = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

double CProgressive::GetNearVisionCenterX()
{
	return m_nearVisionCenterX;
}
bool CProgressive::SetNearVisionCenterX(double newValue)
{
	if(!TestRange(newValue, -25., 25., 1))
		return false;
	m_nearVisionCenterX = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

double CProgressive::GetNearVisionCenterY()
{
	return m_nearVisionCenterY;
}
bool CProgressive::SetNearVisionCenterY(double newValue)
{
	if(!TestRange(newValue, -25., 25., 1))
		return false;
	m_nearVisionCenterY = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

double CProgressive::GetNearVisionMarkerDiam()
{
	return m_nearVisionMarkerDiam;
}
bool CProgressive::SetNearVisionMarkerDiam(double newValue)
{
	if(!TestRange(newValue, 0., 25., 1))
		return false;
	m_nearVisionMarkerDiam = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}
