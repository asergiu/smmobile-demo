#pragma once

#include "IWSLLens.h"
#include "ObjectPointer.h"
#include <map>
using std::map;
#include <sstream>
#include <iosfwd>

#include "PropObjectExchange.h"

#define TestRange(n, min, max, e)	(( (n)<(min) || (n)>(max) )? false : true)
#define TestRangeMin0(n, max, e)	(( (n)>(max) )? false : true)
#define TestRangeSetError(n, min, max, e, error) \
	(( (n)<(min) || (n)>(max) )?(SetError(error), false):(SetError(0), true))

class CEventObjData
{
public:
	CEventObjData()
	{
		dwCookie = 0;
		pObject = NULL;
	}
    CEventObjData(unsigned long dwCookieValue, void* pObjectValue)
	{
		dwCookie = dwCookieValue;
		pObject = pObjectValue;
	}
	const CEventObjData& operator=(const CEventObjData& eod)
	{
		dwCookie = eod.dwCookie;
		pObject = eod.pObject;
		return *this;
	}
	unsigned long dwCookie;
    void* pObject;
};

template <class TObject, class TEvent>
class CObjEventSimpleImpl : public TEvent
{
public:
    typedef map<long, CEventObjData> EventObjMap;
	EventObjMap m_EOmap;

	CObjEventSimpleImpl()
	{
	}
	~CObjEventSimpleImpl()
	{
		if(m_EOmap.size() > 0) {
            EventObjMap::iterator it;
            for(it = m_EOmap.begin(); it != m_EOmap.end(); ++it){
                UnadviseConnection(it->first, false);
            }
		}
	}
	bool EventAdvise(TObject* pUnk)
	{
		return AdviseConnection(pUnk, 0);
	}
	bool EventUnadvise()
	{
		return UnadviseConnection(0);
	}
	bool AdviseConnection(TObject* pUnk, long nIndex)
	{
		bool bRes = false;
		EventObjMap::iterator it = m_EOmap.find(nIndex);
		if(it == m_EOmap.end()) {
			if(pUnk) {
				unsigned long dwCookie = 0;
                bRes = pUnk->Advise(this, &dwCookie);
				if(bRes) {
                    CEventObjData eod(dwCookie, pUnk);
					m_EOmap.insert(EventObjMap::value_type(nIndex, eod));
					pUnk->AddRef();
				}
			}
		}
		else
			bRes = true;
		return bRes;
	}
	bool UnadviseConnection(long nIndex, bool bErase = true)
	{
		bool bRes = false;
		EventObjMap::iterator it = m_EOmap.find(nIndex);
		if(it == m_EOmap.end()) {
			return true;
		}
        TObject* pObject = (TObject*)it->second.pObject;
		unsigned long dwCookie = it->second.dwCookie;
		if(pObject) {
            bRes = pObject->Unadvise(dwCookie);
			pObject->Release();
		}
        if(bErase)
            m_EOmap.erase(nIndex);
		return bRes;
	}
};

template <class TObject, class TEvent>
class CObjectEventBase : public TObject
{
public:
	long m_dwRef;
	bool m_bModified;

	CObjectEventBase()
	{
		m_dwRef = 0;
		m_bModified = false;
		m_index = 0;
		AddRef();
	}

	ULONG AddRef()
	{
		m_dwRef++;
		return m_dwRef;
	}
	ULONG Release()
	{
		if(m_dwRef > 0) {
			m_dwRef--;
			if(m_dwRef == 0) {
				FinalRelease();
				return 0;
			}
		}
		return m_dwRef;
	}
	bool WriteToSteam(ostream * pOutStream)
	{
		if(!pOutStream)
			return false;
		CPropObjectExchange PX;
		PX.pOutStream = pOutStream;
		PX.bLoad = false;
		return DoPropExchange(&PX);
	}
	bool ReadFromSteam(istream * pInStream)
	{
		if(!pInStream)
			return false;
		CPropObjectExchange PX;
		PX.pInStream = pInStream;
		PX.bLoad = true;
		return DoPropExchange(&PX);
	}
	bool ResetState()
	{
		CPropObjectExchange PX;
		PX.bLoad = true;
		PX.bReset= true;
		return DoPropExchange(&PX);
	}
	void SetModifiedFlag(bool bModified = true){ m_bModified = bModified; }
	bool IsModified(){ return m_bModified; }

protected:
	virtual void FinalRelease() = 0;
	virtual bool DoPropExchange(CPropObjectExchange* pPX) = 0;

public:
	typedef map<unsigned long, TEvent*> EventMap;
	EventMap m_map;
	unsigned long m_index;

	bool Advise(TEvent* pUnkSink, unsigned long * pdwCookie)
	{
		if(pUnkSink && pdwCookie) {
			m_index++;
			m_map.insert(std::make_pair(m_index, pUnkSink));
			*pdwCookie = m_index;
			return true;
		}
		return false;
	}
	bool Unadvise(unsigned long dwCookie)
	{
		m_map.erase(dwCookie);
		return true;
	}
};

template <class TObject>
class CObjectBase : public TObject
{
public:
	long m_dwRef;
	bool m_bModified;

	CObjectBase()
	{
		m_dwRef = 0;
		m_bModified = false;
		AddRef();
	}

	ULONG AddRef()
	{
		m_dwRef++;
		return m_dwRef;
	}
	ULONG Release()
	{
		if(m_dwRef > 0) {
			m_dwRef--;
			if(m_dwRef == 0) {
				FinalRelease();
				return 0;
			}
		}
		return m_dwRef;
	}
	bool WriteToSteam(ostream * pOutStream)
	{
		if(!pOutStream)
			return false;
		CPropObjectExchange PX;
		PX.pOutStream = pOutStream;
		PX.bLoad = false;
		return DoPropExchange(&PX);
	}
	bool ReadFromSteam(istream * pInStream)
	{
		if(!pInStream)
			return false;
		CPropObjectExchange PX;
		PX.pInStream = pInStream;
		PX.bLoad = true;
		return DoPropExchange(&PX);
	}
	bool ResetState()
	{
		CPropObjectExchange PX;
		PX.bLoad = true;
		PX.bReset= true;
		return DoPropExchange(&PX);
	}
	void SetModifiedFlag(bool bModified = true){ m_bModified = bModified; }
	bool IsModified(){ return m_bModified; }

protected:
	virtual void FinalRelease() = 0;
	virtual bool DoPropExchange(CPropObjectExchange* pPX) = 0;
};
