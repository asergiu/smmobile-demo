#pragma once

#include <list>

class CMassiv
{
protected:
	int Dx, Dy;
	unsigned char * Point;
	unsigned char ** m_Content;
	int iColor, Xn, Yn;
public:
	int GetX(void){return Dx;};
	int GetY(void){return Dy;};
	CMassiv(){Dx = Dy = 0; Point = NULL; m_Content = NULL;}
	CMassiv(int offsetX, int offsetY, unsigned char * pMassiv)
	{Dx = Dy = 0; Point = NULL; m_Content = NULL; CreateMassiv(offsetX, offsetY, pMassiv);}
	void CreateMassiv(int offsetX, int offsetY, unsigned char * pMassiv)
	{
	int i;
		Dx = offsetX; Dy = offsetY; Point = pMassiv;
		if(m_Content!=NULL){free(m_Content); m_Content=NULL;}
		m_Content = (unsigned char**)malloc(sizeof(unsigned char *) * Dy);
		if(m_Content == NULL){
			Dx = Dy = 0; Point = NULL; m_Content = NULL;
			return;
		}
		for(i = 0; i < Dy; i++) m_Content[i] = &Point[i*Dx];
	}
	unsigned char GetPoint(int ix, int iy){return(Point[iy*Dx+ix]);}
	void SetPoint(int ix, int iy, unsigned char ip){Point[iy*Dx+ix] = ip;}
	unsigned char * operator[](int nIndex){return ((unsigned char *)m_Content[nIndex]);}
	void Destroy(void){if(m_Content!=NULL){free(m_Content); m_Content=NULL;} Point = NULL;};
	~CMassiv(){Destroy();};
	unsigned char * GetPoint(void){return(Point);}
	void SetColor(int Color){iColor = Color;}
	void MoveTo(int x, int y){Xn = x; Yn = y;}
	void MoveTo(POINT point){Xn = point.x; Yn = point.y;}
	void LineTo(int x, int y){Line(Xn, Yn, x, y, iColor); MoveTo(x, y);}
	void LineTo(POINT point){Line(Xn, Yn, point.x, point.y, iColor); MoveTo(point);}
	void Line(CPoint &pt1, CPoint &pt2, int nom){Line(pt1.x, pt1.y, pt2.x, pt2.y, nom);}
	void Line(int x1, int y1, int x2, int y2, int nom){
		int dx,dy,ico=0,i,j,ii,jj;
		int xb = 0,
			xe = Dx - 1,
			yb = 0,
			ye = Dy - 1;

 		dx=abs((jj=x1)-x2);
		dy=abs((ii=y1)-y2);
		
		if(y1>=yb && y1<=ye && x1>=xb && x1<=xe)
			m_Content[y1][x1]=nom;
		if(y2>=yb && y2<=ye && x2>=xb && x2<=xe)
			m_Content[y2][x2]=nom;

		if(dy<=dx){
			for(j=1;j<=dx;j++){
				ico += dy;
				jj=jj+sign(x2-x1);
				if(ico<dx){
					if(ii>=yb && ii<=ye && jj>=xb && jj<=xe)
						m_Content[ii][jj]=nom;
					}
				else {
					ico -= dx;
					ii=ii+sign(y2-y1);
					if(ii>=yb && ii<=ye && jj>=xb && jj<=xe)
						m_Content[ii][jj]=nom;
					}
				}
			}
		else {
			for(i=1;i<=dy;i++){
				ico += dx;
				ii=ii+sign(y2-y1);
				if(ico<dy){
					if(ii>=yb && ii<=ye && jj>=xb && jj<=xe)
						m_Content[ii][jj]=nom;
					}
				else {
					ico -= dy;
					jj=jj+sign(x2-x1);
					if(ii>=yb && ii<=ye && jj>=xb && jj<=xe)
						m_Content[ii][jj]=nom;
					}
				}
			}
		}

	void FloodFill(CPoint &pt, int FillColor){FloodFill(pt.x, pt.y, FillColor);}
	void FloodFill(int XEyeCent,int YEyeCent, int FillColor){
		int xb = 0,
			xe = Dx - 1,
			yb = 0,
			ye = Dy - 1;

		if(!(YEyeCent>yb && YEyeCent<ye && XEyeCent>xb && XEyeCent<xe))
			return;

		std::list<POINT> List;
		int curX, curY, baseX = XEyeCent, baseY = YEyeCent;
		POINT pt;
		BOOL fEmpty;
		int Color = m_Content[YEyeCent][XEyeCent];

		do {
			curX = baseX; curY = baseY;
			// ������� ��������� � ����� ������ ����� ������,
			// ���� ��� �� ���������
			///			if(m_Content[curY-1][curX]==Color && curY > yb){
			if(m_Content[curY-1][curX]==Color){
				pt.x = curX; pt.y = curY-1;
				List.push_front(pt);
			}
			// ����� ��������� � ����� ������ ����� �����,
			// ���� ��� �� ���������
			///			if(m_Content[curY+1][curX]==Color && curY < ye){
			if(m_Content[curY+1][curX]==Color){
				pt.x = curX; pt.y = curY+1;
				List.push_front(pt);
			}
			// �������� ������� �����
			//			if(m_Content[curY][curX]==Color)
			m_Content[curY][curX] = FillColor;
			// ���� ������
			curX++;
			while(m_Content[curY][curX]==Color){
				///				if(curX > xe) break;
				m_Content[curY][curX] = FillColor;
				// �����
				if(m_Content[curY+1][curX]==Color && m_Content[curY+1][curX-1]!=Color){
					pt.x = curX; pt.y = curY+1;
					List.push_front(pt);
				}
				// ������
				if(m_Content[curY-1][curX]==Color && m_Content[curY-1][curX-1]!=Color){
					pt.x = curX; pt.y = curY-1;
					List.push_front(pt);
				}
				curX++;
			}
			// ���� �����
			curX = baseX-1; curY = baseY;
			while(m_Content[curY][curX]==Color){
				///				if(curX < xb) break;
				m_Content[curY][curX] = FillColor;	
				// �����
				if(m_Content[curY+1][curX]==Color && m_Content[curY+1][curX+1]!=Color){
					pt.x = curX; pt.y = curY+1;
					List.push_front(pt);
				}
				// ������
				if(m_Content[curY-1][curX]==Color && m_Content[curY-1][curX+1]!=Color){
					pt.x = curX; pt.y = curY-1;
					List.push_front(pt);
				}
				curX--;
			}
			if(!(fEmpty = List.empty())){
				pt = List.front();
				List.pop_front();
				baseX = pt.x; baseY = pt.y;
			}
		} while(!fEmpty);
	}
	int sign(int a){
		if(a>0)
			return 1;
		else if(a<0)
			return -1;
		else
			return 0;

		}
};
