#pragma once

#include "UsefulThings.h"
#include "FTPoint.h"

#define MAX_SPL_NODES 12	
#define MAX_POLY_NODES 30

//#ifndef EPS
//	#define EPS (1e-12)
//#endif
//
//#ifndef M_PI
//	#define M_PI 3.14159265358979323846
//#endif

#ifndef SPL_OK
#define SPL_OK 0
#define SPL_ILLEGAL_POINTS 1
#define SPL_NOT_ENOUGH_MEMORY 2
#define SPL_NO_SOLUTION 3
#define SPL_INVALID_NUMBER 4
#endif
typedef int HSPL;
//typedef double FType;
typedef struct
{
	double x,y;
	FType t,Phi;
}
AdvArrType;
/*
typedef struct {
	FType x,y;
	} FTPOINT;
typedef FTPOINT * PFTPOINT;
*/

//extern int SplineError;

class CCompSpline
{
protected:
	FType *X,*PolyCoeffs,*LastUsedCoeffs;
	int NumPoints;
	int LastUsedSegment;
public:
	BOOL SplExists;
	int SplineError;
public:
	CCompSpline(){SplExists = FALSE; X = PolyCoeffs = NULL; SplineError = SPL_OK;}
	BOOL CreateCompSpline(FTPOINT *Points,int Size,FType Period);
	void DestroyCompSpline(void);
	FType ComputeCompSpline(FType Argument);
	
	~CCompSpline(){DestroyCompSpline();}
};

typedef struct tagSFTPOINT
{
	double x,y;
	BOOL SelectedFlag;
	double h;
	int NextPoint;
	int PrevPoint; // ������� ������ �����������, ��� ����� ����� � �������
}
SFTPOINT,*PSFTPOINT;

class CSpline
{
protected:
	CCompSpline SplX,SplY;
	AdvArrType * Arr;
	PFTPOINT ArrX, ArrY;
	FType Dist;

public:
	BOOL SplExists;
    int nPolyNodes;
	POINT * PolyNodes;
	CFTPoint * FloatPolyNodes;
	BOOL MakeProportionalSegment;

public:
	CSpline();
	~CSpline();
	void CreateSpline(int nCoord, FTPOINT * CoordFloat);
	void CreateSpline(int nCoord, FTPOINT * CoordFloat, int nOut);
	
	void CreateSplineAdv(int nCoord, FTPOINT * CoordFloat, int nOut);	// ������� ������, ����������
																		// nOut ����� ����������
	void RecreateSpline(int nNewPointsNumber);
	void DuplicateSpline(int nCoordIn, FTPOINT * CoordFloatIn);
	void DestroySpline(void);
	BOOL DecreasePointsNumber(int nNewPointsNumber); // ���������� ����� �� _������������_ �������
													 // ������� ���������� �������
	int GetLeastSignificantPointIndex(int nCoordIn, FTPOINT *CoordFloatIn, int nEceptPointIndex = -1);
    void ProjectPointToSpline(FTPOINT *Point);
	BOOL PtInSpline(FTPOINT point, double kx = 1., double ky = 1.);

	FTPOINT MassCenter();
    void GetBoundRect(FTRECT *rect);

protected:
	BOOL DisposeSplinePoint();
	void CalculatePointH(PSFTPOINT PolyLine, int nPolyLine, int sPoint, int ePoint);
	
	int GetNextIndex(int n){ return (n + 1) % nPolyNodes; }
	int GetPrevIndex(int n){ return (nPolyNodes + n - 1) % nPolyNodes; }

	double SplineDistance(CSpline &Spline);
	double SplineDistance(int nCoordIn, double *t_param);

	FTPOINT GetOneMoreApproxPoint(int nCoordIn, FTPOINT *CoordFloatIn, double *distance = NULL, double *t_param = NULL);
	double GetOneMoreApproxPoint(int nCoordIn, double *t_param, double *distance = NULL);
	void AdjustPointsForMinDistance(int nCoordIn, double *t_param, double *distance = NULL);

	//friend void DrawSpline(CSpline &TargetSpline, int nCoord, double *t_param, int ind, BOOL bWait = TRUE);

	static int AdvArrComp(const void *Elem1,const void *Elem2);
	static FType Azimut(FType Y, FType X);
	static FType Azimut(SIZE sz);
	static FType Azimut(FTPOINT pt);
	static FType Hypot(SIZE sz);
	static FType Hypot(FTPOINT pt);
	static FType Hypot(FTPOINT pt1, FTPOINT pt2);
    static double CalcTDist(double t0, double t1, double T, double direction);
};
