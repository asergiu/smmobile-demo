// ContourPointBelong.cpp: implementation of the CContourPointBelong class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ContourPointBelong.h"
#include "CircleLineCrossPoints.h"
#include "CirclesCrossPoints.h"
#include "WSPicture.h"
#include "ShellPartner.h"

#include "Spline.h"
#include <math.h>
#include "ShellLens.h"
#include "Massiv.h"
#include "FTPoint.h"

#include "Lens.h"
#include "Resource/rcInstall.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CContourPointBelong::CContourPointBelong()
{
	picSize = CSize(0, 0);
	
	m_LeftPupilX = m_LeftPupilY = m_RightPupilX = m_RightPupilY = -1.;
	m_LeftPD = m_RightPD = -1.;

	m_LeftOptCenOffsetX = m_LeftOptCenOffsetY = m_RightOptCenOffsetX = m_RightOptCenOffsetY = 0.;

	m_LeftRadius = m_RightRadius = 0.;
	hOch = NULL;
	Mss = NULL;
	MayBeUsed = FALSE;

	ClearArchive();
}

CContourPointBelong::~CContourPointBelong()
{
	FreeData();
	MayBeUsed = FALSE;
}

void CContourPointBelong::FreeData()
{
	if(hOch != NULL){free(hOch); hOch = NULL;}
	if(Mss != NULL){free(Mss); Mss = NULL;}
}

void CContourPointBelong::LoadData(CContourPointBelong* Be)
{
	FreeData();

	picSize = Be->picSize;

	m_LeftPupilX = Be->m_LeftPupilX;
	m_LeftPupilY = Be->m_LeftPupilY;
	m_RightPupilX = Be->m_RightPupilX;
	m_RightPupilY = Be->m_RightPupilY;

	m_LeftPD = Be->m_LeftPD;
	m_RightPD = Be->m_RightPD;

	lpdispLeftContour = Be->lpdispLeftContour;
	lpdispRightContour = Be->lpdispRightContour;

	m_LeftOptCenOffsetX = Be->m_LeftOptCenOffsetX;
	m_LeftOptCenOffsetY = Be->m_LeftOptCenOffsetY;
	m_RightOptCenOffsetX = Be->m_RightOptCenOffsetX;
	m_RightOptCenOffsetY = Be->m_RightOptCenOffsetY;

	m_LeftRadius = Be->m_LeftRadius;
	m_RightRadius = Be->m_RightRadius;

	LenseZon = Be->LenseZon;

	hOch = NULL;
	if(Be->hOch)
	{
		int hOchSize = LenseZon.Width() * LenseZon.Height();
		if(hOchSize > 0)
		{
			hOch = (PUCHAR)malloc(hOchSize);
			memcpy(hOch, Be->hOch, hOchSize);
			Och.CreateMassiv(LenseZon.Width(), LenseZon.Height(), hOch);
		}
	}

	Mss = NULL;
	if(Be->Mss)
	{
		int MssSize = 4L * 4L * LenseZon.Height();
		if(MssSize > 0)
		{
			Mss = (int (*)[4])malloc(MssSize);
			memcpy(Mss, Be->Mss, MssSize);
		}
	}

	Xcn[wsLeft] = Be->Xcn[wsLeft];
	Ycn[wsLeft] = Be->Ycn[wsLeft];
	Xcn[wsRight] = Be->Xcn[wsRight];
	Ycn[wsRight] = Be->Ycn[wsRight];

	XcContour[wsLeft] = Be->XcContour[wsLeft];
	YcContour[wsLeft] = Be->YcContour[wsLeft];
	XcContour[wsRight] = Be->XcContour[wsRight];
	YcContour[wsRight] = Be->YcContour[wsRight];

	BorderPoints[wsLeft] = Be->BorderPoints[wsLeft];
	BorderPoints[wsRight] = Be->BorderPoints[wsRight];

	MayBeUsed = Be->MayBeUsed;
}

void CContourPointBelong::ClearArchive()
{
	picSize_Arc = CSize(0, 0);
	m_LeftPupilX_Arc = m_LeftPupilY_Arc = m_RightPupilX_Arc = m_RightPupilY_Arc = -1.;
	m_LeftPD_Arc = m_RightPD_Arc = -1.;
	m_LeftOptCenOffsetX_Arc = m_LeftOptCenOffsetY_Arc = m_RightOptCenOffsetX_Arc = m_RightOptCenOffsetY_Arc = 0.;
}

void CContourPointBelong::SaveToArchive()
{
/*
	if(lpdispLeftContour == NULL || lpdispRightContour == NULL) return;

	picSize_Arc = picSize;
	m_LeftPupilX_Arc = m_LeftPupilX;
	m_LeftPupilY_Arc = m_LeftPupilY;
	m_RightPupilX_Arc = m_RightPupilX;
	m_RightPupilY_Arc = m_RightPupilY;
	m_LeftPD_Arc = m_LeftPD;
	m_RightPD_Arc = m_RightPD;
	m_LeftOptCenOffsetX_Arc = m_LeftOptCenOffsetX;
	m_LeftOptCenOffsetY_Arc = m_LeftOptCenOffsetY;
	m_RightOptCenOffsetX_Arc = m_RightOptCenOffsetX;
	m_RightOptCenOffsetY_Arc = m_RightOptCenOffsetY;

	CContour * LeftContour = (CContour *)CCmdTargetPersist::FromIDispatch(lpdispLeftContour);
	CContour * RightContour = (CContour *)CCmdTargetPersist::FromIDispatch(lpdispRightContour);
	if(LeftContour == NULL || RightContour == NULL) return;
	GuidLeftContour_Arc = LeftContour->m_GUID;
	GuidRightContour_Arc = RightContour->m_GUID;
*/
}

BOOL CContourPointBelong::CompareDataArchive()
{
	return FALSE;
/*
	if(lpdispLeftContour == NULL || lpdispRightContour == NULL) return FALSE;

	if(picSize_Arc != picSize) return FALSE;
	if(m_LeftPupilX_Arc != m_LeftPupilX) return FALSE;
	if(m_LeftPupilY_Arc != m_LeftPupilY) return FALSE;
	if(m_RightPupilX_Arc != m_RightPupilX) return FALSE;
	if(m_RightPupilY_Arc != m_RightPupilY) return FALSE;
	if(m_LeftPD_Arc != m_LeftPD) return FALSE;
	if(m_RightPD_Arc != m_RightPD) return FALSE;
	if(m_LeftOptCenOffsetX_Arc != m_LeftOptCenOffsetX) return FALSE;
	if(m_LeftOptCenOffsetY_Arc != m_LeftOptCenOffsetY) return FALSE;
	if(m_RightOptCenOffsetX_Arc != m_RightOptCenOffsetX) return FALSE;
	if(m_RightOptCenOffsetY_Arc != m_RightOptCenOffsetY) return FALSE;

	CContour * LeftContour = (CContour *)CCmdTargetPersist::FromIDispatch(lpdispLeftContour);
	CContour * RightContour = (CContour *)CCmdTargetPersist::FromIDispatch(lpdispRightContour);
	if(LeftContour == NULL || RightContour == NULL) return FALSE;
	if(!IsEqualGUID(GuidLeftContour_Arc, LeftContour->m_GUID)) return FALSE;
	if(!IsEqualGUID(GuidRightContour_Arc, RightContour->m_GUID)) return FALSE;

	return TRUE;
*/
}

//	BOOL CalculateContourPointMap()
//	Returns TRUE if successfully
//			FALSE if error
BOOL CContourPointBelong::CalculateContourPointMap()
{
	if(MayBeUsed == TRUE && CompareDataArchive() == TRUE) return TRUE;

	// ��������� ������� ������ �� �����������
	if(m_LeftPupilX < 0. || m_LeftPupilY < 0. || m_RightPupilX < 0. || m_RightPupilY < 0.) return FALSE;
	if(m_LeftPD < 0. || m_RightPD < 0.) return FALSE;
	if(lpdispLeftContour == NULL || lpdispRightContour == NULL) return FALSE;
	if(picSize.cx <= 0 || picSize.cy <= 0) return FALSE;

	MayBeUsed = FALSE;
	
	// ���� ����������� ��������� ����������
	int i, j, l;
	CSpline Spline[2];
	CRect GlassZon[2];
	PFTPOINT ContourFloat;

	// �������� ������� ��� ����� � ������ ����
	IContour * ContourRight = lpdispRightContour.m_p;
	IContour * ContourLeft = lpdispLeftContour.m_p;

	// ���������� ����� ����� � ��������
	int CountRight = ContourRight->GetCount();
	int CountLeft = ContourLeft->GetCount();

	// ������ ������ ��� ������� �������
	ContourFloat = (PFTPOINT)malloc(CountRight * sizeof(FTPOINT));
	if(ContourFloat == NULL) return FALSE;
	for(i = 0; i < CountRight; i++){
		CObjPtr<IContourPoint> cpr;
		cpr.AttachDispatch(ContourRight->GetItem(i+1));
		ContourFloat[i].x = cpr->GetX();
		ContourFloat[i].y = cpr->GetY();
	}
	if(CountRight < 30) Spline[wsRight].CreateSpline(CountRight, ContourFloat);
	else				Spline[wsRight].DuplicateSpline(CountRight, ContourFloat);
	free(ContourFloat);
	if(Spline[wsRight].SplExists == FALSE) return FALSE;	// ����� ���� ������ �� ��������

	// ������ ������ ��� ������ �������
	ContourFloat = (PFTPOINT)malloc(CountLeft * sizeof(FTPOINT));
	if(ContourFloat == NULL) return FALSE;
	for(i = 0; i < CountLeft; i++){
		CObjPtr<IContourPoint> cpl;
		cpl.AttachDispatch(ContourLeft->GetItem(i+1));
		ContourFloat[i].x = cpl->GetX();
		ContourFloat[i].y = cpl->GetY();
	}
	if(CountLeft < 30)  Spline[wsLeft].CreateSpline(CountLeft, ContourFloat);
	else				Spline[wsLeft].DuplicateSpline(CountLeft, ContourFloat);
	free(ContourFloat);
	if(Spline[wsLeft].SplExists == FALSE) return FALSE;	// ����� ���� ������ �� ��������

	// ���������� ���� ������ � ������� ��������, � ����� ������ ���
	for(l = 0; l < 2; l++){
		XcContour[l] = YcContour[l] = 0.;
		GlassZon[l] = CRect(CPoint(0, 0), CSize(0, 0));
		GlassZon[l].left = GlassZon[l].top = 100000;
		for(i = 0; i < Spline[l].nPolyNodes; i++){
			CPoint Point = Spline[l].PolyNodes[i];
			CFTPoint FTPoint = Spline[l].FloatPolyNodes[i];
			XcContour[l] += FTPoint.x;	YcContour[l] += FTPoint.y;
			if(GlassZon[l].left   > Point.x) GlassZon[l].left   = Point.x;
			if(GlassZon[l].right  < Point.x) GlassZon[l].right  = Point.x;
			if(GlassZon[l].top    > Point.y) GlassZon[l].top    = Point.y;
			if(GlassZon[l].bottom < Point.y) GlassZon[l].bottom = Point.y;
		}
		GlassZon[l].left  -= 2;  GlassZon[l].top    -= 2;
		GlassZon[l].right += 2;  GlassZon[l].bottom += 2;
		if(GlassZon[l].left < 0) GlassZon[l].left = 0;
		if(GlassZon[l].top  < 0) GlassZon[l].top  = 0;
		if(GlassZon[l].right >= picSize.cx) GlassZon[l].right = picSize.cx - 1;
		if(GlassZon[l].bottom >= picSize.cy) GlassZon[l].bottom = picSize.cy - 1;

		XcContour[l] /= (double)Spline[l].nPolyNodes; YcContour[l] /= (double)Spline[l].nPolyNodes;
	}

	// ��������� ����� ��������, ����� ��� ������ ������ ���.
	for(l = 0; l < 2; l++){
		for(i = 0; i < Spline[l].nPolyNodes; i++){
			if(Spline[l].PolyNodes[i].x < GlassZon[l].left)
				Spline[l].PolyNodes[i].x = GlassZon[l].left;
			if(Spline[l].PolyNodes[i].x > GlassZon[l].right)
				Spline[l].PolyNodes[i].x = GlassZon[l].right;
			if(Spline[l].PolyNodes[i].y < GlassZon[l].top)
				Spline[l].PolyNodes[i].y = GlassZon[l].top;
			if(Spline[l].PolyNodes[i].y > GlassZon[l].bottom)
				Spline[l].PolyNodes[i].y = GlassZon[l].bottom;
		}
	}

	// ���������� ����������� ������� ����
	m_LeftRadius = m_RightRadius = 0.;
	CDimension Dim(picSize, m_LeftPD, m_RightPD, m_LeftPupilX, m_LeftPupilY, m_RightPupilX, m_RightPupilY);
	//	�������� ������ �������� �� �������� ��������
	Xcn[wsLeft] = XcContour[wsLeft] + m_LeftOptCenOffsetX;
	Xcn[wsRight] = XcContour[wsRight] + m_RightOptCenOffsetX;
	Ycn[wsLeft] = YcContour[wsLeft] + m_LeftOptCenOffsetY;
	Ycn[wsRight] = YcContour[wsRight] + m_RightOptCenOffsetY;
	for(l = 0; l < 2; l++){
		if(Spline[l].SplExists == FALSE) continue;
		for(i = 0; i < Spline[l].nPolyNodes; i++){
			double dx, dy, R;
			CPoint Point = Spline[l].PolyNodes[i];
			dx = (Xcn[l] - (double)Point.x) * Dim.GetMMinPixelByX();
			dy = (Ycn[l] - (double)Point.y) * Dim.GetMMinPixelByY();
			R = _hypot(dx, dy);

			switch(l){
			case 0:
				if(R > m_RightRadius) m_RightRadius = R;
				break;
			case 1:
				if(R > m_LeftRadius) m_LeftRadius = R;
				break;
			}
		}
	}

	// ���������� ���� ���� �� ��������
	LenseZon.left = min(GlassZon[wsRight].left, GlassZon[wsLeft].left);
	LenseZon.top  = min(GlassZon[wsRight].top,  GlassZon[wsLeft].top );
	// � �������� Right & Bottom ���������� 1, ����� ��������� ������������ Width & Height ���� ����
	LenseZon.right = max(GlassZon[wsRight].right, GlassZon[wsLeft].right) + 1;
	LenseZon.bottom = max(GlassZon[wsRight].bottom, GlassZon[wsLeft].bottom) + 1;

	// ���������� ���� ���������
	const int _conturPen[2] = {ColorRightContur, ColorLeftContur};
	const int _fonPen[2] = {ColorRightInternelFon, ColorLeftInternelFon};
	// ������� ������
	if(hOch != NULL){free(hOch); hOch = NULL;}
	if(Mss != NULL){free(Mss); Mss = NULL;}
	// ����������� ����� ������
	hOch = (PUCHAR)malloc(LenseZon.Width() * LenseZon.Height());
	Mss = (int (*)[4])malloc(4L * 4L * LenseZon.Height());
	// ���� ��������� ������� ��������� ��������, �� ������
	if(hOch == NULL || Mss == NULL){
		if(hOch != NULL){free(hOch); hOch = NULL;}
		if(Mss != NULL){free(Mss); Mss = NULL;}
		return FALSE;
	}
	else{
		memset(hOch, ColorExternelFon, LenseZon.Width() * LenseZon.Height());
		Och.CreateMassiv(LenseZon.Width(), LenseZon.Height(), hOch);

//		Och.SetColor(200);
//		Och.MoveTo(0, 0);
//		Och.LineTo(Och.GetX() - 1, 0);
//		Och.LineTo(Och.GetX() - 1, Och.GetY() - 1);
//		Och.LineTo(0, Och.GetY() - 1);
//		Och.LineTo(0, 0);

		for(l = 0; l < 2; l++){
			Och.SetColor(_conturPen[l]);
			CPoint Point = Spline[l].PolyNodes[0];
			Och.MoveTo(Point-CSize(LenseZon.left, LenseZon.top));
			for(i = 1; i < Spline[l].nPolyNodes+1; i++){
				Point = Spline[l].PolyNodes[i];
				Och.LineTo(Point-CSize(LenseZon.left, LenseZon.top));
			}
//Och.SaveToBMP("C:\\Och.bmp");
			Och.FloodFill((int)XcContour[l]-LenseZon.left, (int)YcContour[l]-LenseZon.top, _fonPen[l]);
		}

		//	������ ����� ����������� ������� � �������� ����
		BorderPoints[wsRight] = BorderPoints[wsLeft] = 0;
		for(j = 1; j < LenseZon.Height(); j++){
			for(i = 1; i < LenseZon.Width(); i++){
				if(Och[j][i] == _fonPen[wsLeft] && j < LenseZon.Height()-1 && i < LenseZon.Width()-1){
					if(	Och[j][i-1] == ColorLeftContur || Och[j][i+1] == ColorLeftContur ||
						Och[j+1][i] == ColorLeftContur || Och[j-1][i] == ColorLeftContur ||
						Och[j-1][i-1] == ColorLeftContur || Och[j-1][i+1] == ColorLeftContur ||
						Och[j+1][i+1] == ColorLeftContur || Och[j+1][i-1] == ColorLeftContur){
								Och[j][i] = ColorLeftBorder;
								BorderPoints[wsLeft]++;
					}
				}
				else if(Och[j][i] == _conturPen[wsLeft]) BorderPoints[wsLeft]++;
				else if(Och[j][i] == _fonPen[wsRight] && j < LenseZon.Height()-1 && i < LenseZon.Width()-1){
					if(	Och[j][i-1] == ColorRightContur || Och[j][i+1] == ColorRightContur ||
						Och[j+1][i] == ColorRightContur || Och[j-1][i] == ColorRightContur ||
						Och[j-1][i-1] == ColorRightContur || Och[j-1][i+1] == ColorRightContur ||
						Och[j+1][i+1] == ColorRightContur || Och[j+1][i-1] == ColorRightContur){
								Och[j][i] = ColorRightBorder;
								BorderPoints[wsRight]++;
					}
				}
				else if(Och[j][i] == _conturPen[wsRight]) BorderPoints[wsRight]++;
			}
		}
		
		memset(Mss, 0, 4L * 4L * LenseZon.Height());
		for(l = 0; l < 2; l++){
			for(j = 0; j < LenseZon.Height(); j++){
				int iLeft = l * 2;
				int iRight = iLeft + 1;
				for(i = 0; i < LenseZon.Width(); i++){
					if(Och[j][i] == _conturPen[l]){Mss[j][iLeft] = i + LenseZon.left; break;}
				}
				for(i = LenseZon.Width()-1; i >= 0; i--){
					if(Och[j][i] == _conturPen[l]){
						Mss[j][iRight] = i + LenseZon.left;
						if(Mss[j][iRight] == Mss[j][iLeft]){
							if(Mss[j][iLeft] != 0){
								if(j != 0 && Mss[j-1][iRight] != 0) Mss[j][iRight] = Mss[j-1][iRight];
							}
						}
						break;
					}
				}
			}
		}
	}

	MayBeUsed = TRUE;
	SaveToArchive();
	return TRUE;
}

// � ���� ��������� �������� ����������� �� �������� �� ����� ��������.
// �������������� ��� ������������ ��������� ��������, ������� �������
//  ������� "CalculateContourPointMap".
// ��� ������� ��� ���� ���-�� �� ��������� "SHELL"

// CRect GetLensRect()
// ���������� ���������� ��������������,
//  ������ �������� ��������� �����
CRect CContourPointBelong::GetLensRect()
{
	return LenseZon;
}

// int GetStartX(int Y, int iLens)
// ���������� ��������� ���������� � ������� �� ������ Y
// Y - � ����������� ��������
// iLens == wsRight - 0;
// iLens == wsLeft - 1;
int CContourPointBelong::GetStartX(int Y, int iLens)
{
	// iLens == 0 - right contour
	// iLens == 1 - left contour
	int jY = Y - LenseZon.top;

	return Mss[jY][iLens*2];
}

// int GetStartX(int Y, int iLens)
// ���������� ��������� ���������� � ������� �� ������ Y
// Y - � ����������� ��������
// iLens == wsRight - 0;
// iLens == wsLeft - 1;
int CContourPointBelong::GetEndX(int Y, int iLens)
{
	// iLens == 0 - right contour
	// iLens == 1 - left contour
	int iY = Y - LenseZon.top;

	return Mss[iY][iLens*2+1];
}

// int Belong(int X, int Y, int iLens)
// Returns value:
//  0 - ����� �� ����� �� ������ �������, �� �� �������
//  1 - ����� ����� ������ �������
//  2 - ����� ����� �� �������
//	3 - ����� ����������� ��������� ����� �������(����� ����������� � ������� ������� �����)
// iLens == wsRight - 0;
// iLens == wsLeft - 1;
int CContourPointBelong::Belong(int X, int Y, int iLens)
{
	// iLens == 0 - right contour
	// iLens == 1 - left contour
	const int _conturPen[2] = {ColorRightContur, ColorLeftContur};
	const int _borderPen[2] = {ColorRightBorder, ColorLeftBorder};
	const int _fonPen[2] = {ColorRightInternelFon, ColorLeftInternelFon};
	int iY = Y - LenseZon.top;
	int iX = X - LenseZon.left;

//	if(iY < 0 || iX < 0)
//		return 0;
//	if(iY >= Och.GetY() || iX >= Och.GetX())
//		return 0;

	if(Och[iY][iX] == _borderPen[iLens]) return 3;
	if(Och[iY][iX] == _conturPen[iLens]) return 2;
	if(Och[iY][iX] == _fonPen[iLens]) return 1;
	
	return 0;
}

// double GetCenterX(int iLens)
// ���������� ���������� � ����������� ������
// iLens == wsRight - 0;
// iLens == wsLeft - 1;
double CContourPointBelong::GetCenterX(int iLens)
{
	return Xcn[iLens];
}

// double GetCenterY(int iLens)
// ���������� ���������� Y ����������� ������
// iLens == wsRight - 0;
// iLens == wsLeft - 1;
double CContourPointBelong::GetCenterY(int iLens)
{
	return Ycn[iLens];
}

// double GetLensRadius(int iLens)
// ���������� ������ "prescription" �����
// iLens == wsRight - 0;
// iLens == wsLeft - 1;
double CContourPointBelong::GetLensRadius(int iLens)
{
	double Radius = 35.;

	switch(iLens){
	case wsRight:
		Radius = m_RightRadius; break;
	case wsLeft:
		Radius = m_LeftRadius; break;
	}

	if(Radius < 20.) Radius = 20.;

	return Radius;
}

// long GetBorderPointsNumber(int iLens)
// ���������� ��������� ����������� ����� ������ �������
// iLens == wsRight - 0;
// iLens == wsLeft - 1;
long CContourPointBelong::GetBorderPointsNumber(int iLens)
{
	return BorderPoints[iLens];
}

// double GetContourCenterX(int iLens)
// ���������� ���������� � ������ �������
// iLens == wsRight - 0;
// iLens == wsLeft - 1;
double CContourPointBelong::GetContourCenterX(int iLens)
{
	return XcContour[iLens];
}

// double GetContourCenterY(int iLens)
// ���������� ���������� Y ������ �������
// iLens == wsRight - 0;
// iLens == wsLeft - 1;
double CContourPointBelong::GetContourCenterY(int iLens)
{
	return YcContour[iLens];
}

//////////////////////////////////////////////////////////////////////
// Implementation of the CRefraction class.
//
//////////////////////////////////////////////////////////////////////

//void DefineShell2Par(double * buffer);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CColorMatrix32 CRefraction::MultiRefracMatrix;

CRefraction::CRefraction(CContourPointBelong * Be,
						CDimension * dimPwof,
						CDimension * dimPwf,
						CDimension * dimCPwf,
						CMassiv * mRwof,
						CMassiv * mGwof,
						CMassiv * mBwof,
						ILens * lpdispRightLens,
						ILens * lpdispLeftLens)
{
//	pShell2Par = new double[160];
	InitializeRefraction(Be,
							dimPwof, dimPwf, dimCPwf,
							mRwof, mGwof, mBwof,
							lpdispRightLens, lpdispLeftLens, 0., 0.);
	
	if(!MultiRefracMatrix.IsReady())
	{
		CWSPicture * pMultiRefracPic = CWSPicture::CreateObject();
		if(pMultiRefracPic) {
			CObjPtr<IWSPicture> MultiRefracPic;
			MultiRefracPic.AttachDispatch(pMultiRefracPic->GetInterface());
			MultiRefracPic->LoadBMPFromMemory(IDB_REFRAC);
			BitmapToMatrix32(MultiRefracPic->GetPicture(), MultiRefracMatrix);
		}
	}
}

CRefraction::CRefraction()
{
//	pShell2Par = new double[160];
	m_Red = m_Green = m_Blue = 1.;

	if(!MultiRefracMatrix.IsReady())
	{
		CWSPicture * pMultiRefracPic = CWSPicture::CreateObject();
		if(pMultiRefracPic) {
			CObjPtr<IWSPicture> MultiRefracPic;
			MultiRefracPic.AttachDispatch(pMultiRefracPic->GetInterface());
			MultiRefracPic->LoadBMPFromMemory(IDB_REFRAC);
			BitmapToMatrix32(MultiRefracPic->GetPicture(), MultiRefracMatrix);
		}
	}
}

CRefraction::~CRefraction()
{
//	if(pShell2Par)
//		delete []pShell2Par;
}

void CRefraction::InitializeRefraction(CContourPointBelong * Be,
										CDimension * dimPwof,
										CDimension * dimPwf,
										CDimension * dimCPwf,
										CMassiv * mRwof,
										CMassiv * mGwof,
										CMassiv * mBwof,
										ILens * lpdispRightLens,
										ILens * lpdispLeftLens,
										double AverZRightLens,
										double AverZLeftLens)
{
	CRefraction::Be = Be;
	CRefraction::dimPwof = dimPwof;
	CRefraction::dimPwf = dimPwf;
	CRefraction::dimCPwf = dimCPwf;
	CRefraction::mRwof = mRwof;
	CRefraction::mGwof = mGwof;
	CRefraction::mBwof = mBwof;

	Lens[wsRight] = lpdispRightLens;
	Lens[wsLeft] = lpdispLeftLens;

	AverZLens[wsRight] = AverZRightLens;
	AverZLens[wsLeft] = AverZLeftLens;

	Initialize();
}

void CRefraction::Initialize()
{
	// �������������� ����������
	Ao = dimPwf->GetCosinus() * dimPwof->GetCosinus() + dimPwf->GetSinus()   * dimPwof->GetSinus();
	Bo = dimPwf->GetSinus()   * dimPwof->GetCosinus() - dimPwf->GetCosinus() * dimPwof->GetSinus();
	PS = 0.18 * dimPwf->PD() * dimPwf->PD();

	// ���������� ��� ������� ����
	string GuidDesign = Lens[wsRight]->GetDesign();
	if(GuidDesign == wsLenticular) LnsCache.Design = 2;
	else if(GuidDesign == wsAspheric) LnsCache.Design = 1;
	else LnsCache.Design = 0;

	// ���������� ��������
	LnsCache.KindOfMat = Lens[wsRight]->GetKindOfMaterial();
	LnsCache.RefIndex = Lens[wsRight]->GetRefIndex();
	LnsCache.Density = Lens[wsRight]->GetDensity();

	// ��� ��������� ����������
	for(int i = 0; i < 2; i++)
	{
		LnsCache.Sph[i] = Lens[i]->GetSph();
		LnsCache.Cyl[i] = Lens[i]->GetCyl();
		LnsCache.Add[i] = Lens[i]->GetAddit();
		LnsCache.Axis[i] = Lens[i]->GetAxis();
		LnsCache.CenterX[i] = Lens[i]->GetCenterX();
		LnsCache.CenterY[i] = Lens[i]->GetCenterY();

		if(wsProgressive == Lens[i]->GetMultifocalType())
		{
			CObjPtr<IProgressive> ProgrObj;
			ProgrObj.AttachDispatch(Lens[i]->GetProgressive());
			if(ProgrObj.m_p)
			{
				LnsCache.OptToGeomCenterDisplacementX[i] = -ProgrObj->GetFarVisionCenterX();
				LnsCache.OptToGeomCenterDisplacementY[i] = -ProgrObj->GetFarVisionCenterY() + 0.5 * ProgrObj->GetFarVisionMarkerDiam();
			}
		}	
		else
		{
			LnsCache.OptToGeomCenterDisplacementX[i] = Lens[i]->GetOptToGeomCenterDisplacementX();
			LnsCache.OptToGeomCenterDisplacementY[i] = Lens[i]->GetOptToGeomCenterDisplacementY();
		}
	}
}
/*
void CRefraction::PrepareRefraction(int iLens)
{
	BOOL Res = RayTracer[iLens].Prepare(Lens[iLens]);
	if(!Res)
	{
		int r =5;
	}
*/
	/*
	if(LnsCache.Cyl[iLens] > 0.){
		CosinusAstigmat = cos(LnsCache.Axis[iLens] * M_PI / 180.);
		SinusAstigmat = sin(LnsCache.Axis[iLens] * M_PI / 180.);
	}

	if(LnsCache.Design > 0){
		int iForm, Quit;
		double	tm = (dimPwf->Height() * 4.) / (dimPwf->Width() * 3.),
				dxCenter = Be->GetCenterX(wsLeft)-Be->GetCenterX(wsRight),
				dyCenter = Be->GetCenterY(wsLeft)-Be->GetCenterY(wsRight),
				tmp = dimPwf->PD() / sqrt( dxCenter*dxCenter + dyCenter*dyCenter/(tm*tm) );
		//double Shell2Par[160];
		LensModel.DefineShell2Par(pShell2Par);

		iForm = LensModel.as_len_mark(LnsCache.Design, LnsCache.KindOfMat, LnsCache.RefIndex,
			LnsCache.Sph[iLens]);
		while(1){
			Quit = LensModel.set_par(iForm, LnsCache.Sph[iLens], LnsCache.Cyl[iLens], -LnsCache.Axis[iLens] * M_PI / 180.,
				(int)Be->GetLensRadius(iLens), tmp, tmp * tm, Be->GetCenterX(iLens), Be->GetCenterY(iLens));
			if(Quit == 0 && iForm == 0){iForm = 2;  continue;}
			break;
		}
	}
	*/
/*
}
*/

// Xoc, Yoc - ���������� ����� �����
double CRefraction::aSTIGMAT(double X, double Y, int iLens)
{
	double aDiopt = 0.;
	double Sph = LnsCache.Sph[iLens];
	double Cyl = LnsCache.Cyl[iLens];
	
	double dx, dy, xb, yb, fi, a, b;

	if(Sph+Cyl > Sph){a = Sph+Cyl; b = Sph;}
	else             {a = Sph; b = Sph+Cyl;}
	dx = (X - Be->GetCenterX(iLens)) * dimCPwf->GetMMinPixelByX();
	dy = (Y - Be->GetCenterY(iLens)) * dimCPwf->GetMMinPixelByY();
	xb = dx * dimCPwf->GetCosinus() + dy * dimCPwf->GetSinus();
	yb = dx * dimCPwf->GetSinus() - dy * dimCPwf->GetCosinus();
	dx = xb * CosinusAstigmat + yb * SinusAstigmat;
	dy = -xb * SinusAstigmat + yb * CosinusAstigmat;

	double r = _hypot(dx, dy);
	if(fabs(r) < 1.e-10) aDiopt = 0.5 * (a + b);
	else {
		fi = asin(fabs(dy/r));
		fi *= 2./M_PI;
		fi = (-2.*fi+3.)*fi*fi;
		aDiopt = a * (1.-fi) + b * fi;
	}

	return aDiopt;
}

BOOL CRefraction::CalculateRefraction2(int Xs, int Ys, int iLens, double FocalDiopt, FTRECT *BoundRect)
{
	CLine3D ResLine;
	
	double dx, dy, xb, yb; 
	double px, py, vx, vy;
	double pxb, pyb, vxb, vyb;

	double rr[4], gg[4], bb[4], PX, PY;

	// �������������� � ������� ���������, ��������� � �������� ��������
	dx = (Xs - Be->GetCenterX(iLens)) * dimCPwf->GetMMinPixelByX();
	dy = (Ys - Be->GetCenterY(iLens)) * dimCPwf->GetMMinPixelByY();
	xb =   ( dx * dimCPwf->GetCosinus() + dy * dimCPwf->GetSinus());
	yb = - (-dx * dimCPwf->GetSinus()   + dy * dimCPwf->GetCosinus());

	// ���������� ��� X ��� Engine ��� ������� ����
	if(iLens == 0) xb = -xb;

	double cx = LnsCache.CenterX[iLens] + LnsCache.OptToGeomCenterDisplacementX[iLens];
	double cy = LnsCache.CenterY[iLens] + LnsCache.OptToGeomCenterDisplacementY[iLens];

	xb += cx;
	yb += cy;

	if(RayTracer[iLens].IsReady())
	{
		BOOL bTraceResult;
		// ���������� ���
		if(FocalDiopt > EPS)
		{
			CLine3D Line(	xb, yb, 0,
							xb - LnsCache.CenterX[iLens],
							yb - LnsCache.CenterY[iLens],
							1000. / FocalDiopt);
			bTraceResult = RayTracer[iLens].TraceRay(Line, ResLine);
		}
		else
		{
			bTraceResult = RayTracer[iLens].TraceVertRay(xb, yb, ResLine);
		}

		if(!bTraceResult)
		{
			// ���� ���������
			// ������� �� ���� �������� � �������

			double l = BoundRect->left;
			double r = BoundRect->right;
			double t = BoundRect->top;
			double b = BoundRect->bottom;

			double dxm = MultiRefracMatrix.GetX();
			double dym = MultiRefracMatrix.GetY();
					
			pxb = (Xs - dimPwf->GetPupilX(iLens)) * dimPwf->GetMMinPixelByX();
			pyb = (Ys - dimPwf->GetPupilY(iLens)) * dimPwf->GetMMinPixelByY();

			px =  dimPwf->GetCosinus() * pxb + dimPwf->GetSinus()   * pyb;
			py = -dimPwf->GetSinus()   * pxb + dimPwf->GetCosinus() * pyb;
			
			px = ((px - l) / (r - l) * 0.8 + 0.1) * dxm;
			py = ((py - t) / (b - t) * 0.8 + 0.1) * dym;

			COLORREF Color = Interp2(MultiRefracMatrix, px, py);

			m_Red = GetRValue(Color) / 255.;
			m_Green = GetGValue(Color) / 255.;
			m_Blue = GetBValue(Color) / 255.;

			return TRUE;
		}
	}
	else
	{
		// RayTracer �� �����
		ResLine.SetData(xb, yb, 0., 0., 0., 1.);
	}

	ResLine.px -= cx;
	ResLine.py -= cy;
	
	px = ResLine.px; py = ResLine.py;
	vx = ResLine.vx; vy = ResLine.vy;

	// ���������� ��� ��� ��� X ��� ������� ����
	if(iLens == 0)
	{
		px = -px;
		vx = -vx;
	}

	// ��������������� � �������� ������� ���������
	// �����
	pxb =   ( px * dimCPwf->GetCosinus() + py * dimCPwf->GetSinus());
	pyb = - (-px * dimCPwf->GetSinus()   + py * dimCPwf->GetCosinus());
	pxb = pxb / dimCPwf->GetMMinPixelByX() + Be->GetCenterX(iLens);
	pyb = pyb / dimCPwf->GetMMinPixelByY() + Be->GetCenterY(iLens);
	// � ������
	vxb =   ( vx * dimCPwf->GetCosinus() + vy * dimCPwf->GetSinus());
	vyb = - (-vx * dimCPwf->GetSinus()   + vy * dimCPwf->GetCosinus());
	vxb = vxb / dimCPwf->GetMMinPixelByX();
	vyb = vyb / dimCPwf->GetMMinPixelByY();

	// �������� � �.�., ��������� � ��������� � �������
	pxb = (pxb - dimPwf->GetPupilX(iLens)) * dimPwf->GetMMinPixelByX();
	pyb = (pyb - dimPwf->GetPupilY(iLens)) * dimPwf->GetMMinPixelByY();
	vxb = vxb * dimPwf->GetMMinPixelByX();
	vyb = vyb * dimPwf->GetMMinPixelByY();

	// ������� �� �������� ����� (� �������/��� ������)
	px =  Ao * pxb + Bo * pyb;
	py = -Bo * pxb + Ao * pyb;
	vx =  Ao * vxb + Bo * vyb;
	vy = -Bo * vxb + Ao * vyb;

/*
	double t = (AverZLens[iLens] - ZPLANE - ResLine.pz) / ResLine.vz;
	
	// ��������������� � �������� ������� ���������
	PX = (px + vx * t) / dimPwof->GetMMinPixelByX() + dimPwof->GetPupilX(iLens);
	PY = (py + vy * t) / dimPwof->GetMMinPixelByY() + dimPwof->GetPupilY(iLens);
*/
	
	ResLine.px = px - 32. + iLens * 64.; ResLine.py = -py;
	ResLine.vx = vx; ResLine.vy = -vy;
	ResLine.pz -= AverZLens[iLens];

	CPoint3D p;
	
	if(!Face.FindIntersectionPoint(ResLine, p))
	{
		// !!!
		// ������� ���������� FALSE ������ ��� �������
		// !!!
		m_Red = m_Green = 0;
		m_Blue = 1.;
		return FALSE;
	}

	p.px = p.px + 32. - iLens * 64.;
	p.py = -p.py;

	PX = p.px / dimPwof->GetMMinPixelByX() + dimPwof->GetPupilX(iLens);
	PY = p.py / dimPwof->GetMMinPixelByY() + dimPwof->GetPupilY(iLens);

	if(dimPwof->PtInPicture(PX, PY)){
		int ix = (int)PX, iy = (int)PY;
		double _aa = PX - (double)ix;
		double _bb = PY - (double)iy;
		double aa_ = 1. - _aa;
		double bb_ = 1. - _bb;

		rr[0] = (double)(*mRwof)[iy][ix];
		gg[0] = (double)(*mGwof)[iy][ix];
		bb[0] = (double)(*mBwof)[iy][ix];
			rr[1] = (double)(*mRwof)[iy][ix+1];
			gg[1] = (double)(*mGwof)[iy][ix+1];
			bb[1] = (double)(*mBwof)[iy][ix+1];
		rr[2] = (double)(*mRwof)[iy+1][ix+1];
		gg[2] = (double)(*mGwof)[iy+1][ix+1];
		bb[2] = (double)(*mBwof)[iy+1][ix+1];
			rr[3] = (double)(*mRwof)[iy+1][ix];
			gg[3] = (double)(*mGwof)[iy+1][ix];
			bb[3] = (double)(*mBwof)[iy+1][ix];

		double Color[4];
		double ResColor;
		int iPoint;
		for(int iColor = 0; iColor < 3; iColor++){
			switch(iColor){
			case 0:			// -> Red
				for(iPoint = 0; iPoint < 4; iPoint++) Color[iPoint] = rr[iPoint];
				break;
			case 1:			// -> Green
				for(iPoint = 0; iPoint < 4; iPoint++) Color[iPoint] = gg[iPoint];
				break;
			case 2:			// -> Blue
				for(iPoint = 0; iPoint < 4; iPoint++) Color[iPoint] = bb[iPoint];
				break;
			}

			ResColor = Color[0] * aa_ * bb_ + Color[1] * _aa * bb_ +
									Color[2] * _aa * _bb + Color[3] * aa_ * _bb;

			switch(iColor){
			case 0:			// -> Red
				m_Red = ResColor / 255.;
				break;
			case 1:			// -> Green
				m_Green = ResColor / 255.;
				break;
			case 2:			// -> Blue
				m_Blue = ResColor / 255.;
				break;
			}
		}
	}
	else{m_Red = 0.75;  m_Green = 0.75;  m_Blue = 0.75;}

	return TRUE;
}

double CRefraction::CalculateBaseDioptries(int X, int Y, int iLens)
{
	double Diopt;
	
//return 3.;

	/*
	if(LnsCache.Design > 0) Diopt = LensModel.trace(X, Y, LnsCache.RefIndex);
	else if(LnsCache.Cyl[iLens] > 0.) Diopt = aSTIGMAT(X, Y, iLens);
	else Diopt = LnsCache.Sph[iLens];
	*/
	double dx, dy, xb, yb;
	dx = (X - Be->GetCenterX(iLens)) * dimCPwf->GetMMinPixelByX();
	dy = (Y - Be->GetCenterY(iLens)) * dimCPwf->GetMMinPixelByY();
	xb =   ( dx * dimCPwf->GetCosinus() + dy * dimCPwf->GetSinus());
	yb = - (-dx * dimCPwf->GetSinus()   + dy * dimCPwf->GetCosinus());

	if(iLens == 0) xb = -xb;

//	return (xb > 0. && yb > 0.) ? -100 : 0;

	Diopt = RayTracer[iLens].CalcPointDiop(xb, yb, 0.1);
	ASSERT(fabs(Diopt) < 20. || iLens >= 0);
	return (Diopt == -100.) ? 0 : Diopt;
}

void CRefraction::CalculateRefraction(int Xs, int Ys, int iLens, double Dioptries)
{
/*
	if(fabs(Dioptries) > 25)
	{
		m_Red = m_Green = m_Blue = 0;
		return;
	}

*/
	double dx, dy, Xosr, Yosr, qq, rr[4], gg[4], bb[4], Top, q;
	double X, Y;

	double Index = LnsCache.RefIndex;
	if(Index > 1.541) Index *= 1.335;

	dx = (Xs - dimPwf->GetPupilX(iLens)) * dimPwf->GetMMinPixelByX();
	dy = (Ys - dimPwf->GetPupilY(iLens)) * dimPwf->GetMMinPixelByY();

	// ���������� X � Y �� ���� ��� ������ ������������ ������
	double Xmm =  Ao * dx + Bo * dy;
	double Ymm = -Bo * dx + Ao * dy;
	Xosr = Xmm / dimPwof->GetMMinPixelByX();
	Yosr = Ymm / dimPwof->GetMMinPixelByY();

	// ���� �������� ����� ����, �� ���������� ���������� ���������� ����� �� ���� ��� ������
	if(fabs(Dioptries) < 1.e-2){
		X = Xosr + dimPwof->GetPupilX(iLens);
		Y = Yosr + dimPwof->GetPupilY(iLens);
		goto Cont ;
	}

	// ���� �������� ��������������
	if(Dioptries < 0.){
//		dx = (Xosr/ScaleX0) * (Xosr/ScaleX0) ;
//		dy = dx + Yosr * Yosr ;
		double dxSquared = Xmm * Xmm;
		double dySquared = Ymm * Ymm;
		double RSquared = dxSquared + dySquared;
		if(RSquared > PS) Top = 0.;
		else Top = 30. * (1. - RSquared/PS);
		if((iLens == wsLeft && Xosr > 0.) || (iLens == wsRight && Xosr < 0.)){
			if(dxSquared > PS) q = 1.;
			else q = dxSquared / PS;
			Top += q * RSquared / PS * 50.;
		}
		qq = 1. - Top * Dioptries / 1000. * 1.5 / Index;
		X = Xosr * qq + dimPwof->GetPupilX(iLens);
		Y = Yosr * qq + dimPwof->GetPupilY(iLens);
		goto Cont ;
	}

	// ���� �������� �������������
	if(Dioptries > 0.){
		qq = 1. - 30. * Dioptries / 1000. * 1.5 / Index;
		X = Xosr * qq + dimPwof->GetPupilX(iLens);
		Y = Yosr * qq + dimPwof->GetPupilY(iLens);
		goto Cont ;
	}

Cont:
	if(dimPwof->PtInPicture(X, Y)){
		int ix = (int)X, iy = (int)Y;
		double _aa = X - (double)ix;
		double _bb = Y - (double)iy;
		double aa_ = 1. - _aa;
		double bb_ = 1. - _bb;

		rr[0] = (double)(*mRwof)[iy][ix];
		gg[0] = (double)(*mGwof)[iy][ix];
		bb[0] = (double)(*mBwof)[iy][ix];
			rr[1] = (double)(*mRwof)[iy][ix+1];
			gg[1] = (double)(*mGwof)[iy][ix+1];
			bb[1] = (double)(*mBwof)[iy][ix+1];
		rr[2] = (double)(*mRwof)[iy+1][ix+1];
		gg[2] = (double)(*mGwof)[iy+1][ix+1];
		bb[2] = (double)(*mBwof)[iy+1][ix+1];
			rr[3] = (double)(*mRwof)[iy+1][ix];
			gg[3] = (double)(*mGwof)[iy+1][ix];
			bb[3] = (double)(*mBwof)[iy+1][ix];

		double Color[4];
		double ResColor;
		int iPoint;
		for(int iColor = 0; iColor < 3; iColor++){
			switch(iColor){
			case 0:			// -> Red
				for(iPoint = 0; iPoint < 4; iPoint++) Color[iPoint] = rr[iPoint];
				break;
			case 1:			// -> Green
				for(iPoint = 0; iPoint < 4; iPoint++) Color[iPoint] = gg[iPoint];
				break;
			case 2:			// -> Blue
				for(iPoint = 0; iPoint < 4; iPoint++) Color[iPoint] = bb[iPoint];
				break;
			}

			ResColor = Color[0] * aa_ * bb_ + Color[1] * _aa * bb_ +
									Color[2] * _aa * _bb + Color[3] * aa_ * _bb;

			switch(iColor){
			case 0:			// -> Red
				m_Red = ResColor / 255.;
				break;
			case 1:			// -> Green
				m_Green = ResColor / 255.;
				break;
			case 2:			// -> Blue
				m_Blue = ResColor / 255.;
				break;
			}
		}
	}
	else{m_Red = 0.75;  m_Green = 0.75;  m_Blue = 0.75;}
}

double CRefraction::GetR()
{
	return m_Red;
}

double CRefraction::GetG()
{
	return m_Green;
}

double CRefraction::GetB()
{
	return m_Blue;
}

