#pragma once
// Progressive.h : header file
//

#include "ObjectBase.h"

/////////////////////////////////////////////////////////////////////////////
// CProgressive

class CProgressive :
	public CObjectEventBase<IProgressive, IProgressiveEvents>
{
public:
    static CProgressive* CreateObject();
	IProgressive * GetInterface();

protected:
	CProgressive();           // protected constructor used by dynamic creation

// Attributes
	double m_farVisionCenterX;
	double m_farVisionCenterY;
	double m_farVisionMarkerDiam;
	double m_nearVisionCenterX;
	double m_nearVisionCenterY;
	double m_nearVisionMarkerDiam;

public:

// Operations
public:
	double GetFarVisionCenterX();
	bool SetFarVisionCenterX(double newValue);
	double GetFarVisionCenterY();
	bool SetFarVisionCenterY(double newValue);
	double GetFarVisionMarkerDiam();
	bool SetFarVisionMarkerDiam(double newValue);
	double GetNearVisionCenterX();
	bool SetNearVisionCenterX(double newValue);
	double GetNearVisionCenterY();
	bool SetNearVisionCenterY(double newValue);
	double GetNearVisionMarkerDiam();
	bool SetNearVisionMarkerDiam(double newValue);
	IProgressive * Clone();

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);

protected:
	virtual ~CProgressive();

	virtual void FinalRelease()
	{
		delete this;
	}

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->ProgressiveChanged();
			}
		}
	}
};
