#include "stdafx.h"
#include <float.h>
#include "ShellLens.h"
#include "ShellPartner.h"
#include "UsefulThings.h"

#include "Spline.h"
#include "Contour.h"

typedef // retuning code
enum ShellLens_1000_0001
{
	wsOK									=  0,	//  0 - No error
	wsPictureWithoutFrameNotPresent			=  1,	//  1 - Picture without frame not present
	wsPictureWithFrameNotPresent			=  2,	//  2 - Picture with frame not present
	wsErrorUsingPictureWithoutFrame			=  3,	//  3 - Picture without frame not present
	wsErrorUsingPictureWithFrame			=  4,	//  4 - Picture with frame not present
	wsErrorCalculateContourPointMap			=  5,	//  5 - Error calculate contour point map
	wsRightLensNotPresent					=  6,	//  6 - Right lens not present
	wsLeftLensNotPresent					=  7,	//  7 - Left lens not present
	wsErrorPrepareLensForm					=  8,	//  8 - Error prepare lens form
	wsPupilsFromPictureWithoutFrameNotSet	=  9,	//  9 - Pupils from picture without frame not set
	wsPupilsFromPictureWithFrameNotSet		= 10,	// 10 - Pupils from picture with frame not set
	wsPDsisNotSet							= 11,	// 11 - PDs is not set
	wsContoursIsNotSet						= 12,	// 12 - Contours is not set
	wsLensesIsNotSet						= 13,	// 13 - Lenses is not set
	wsPicturesIsNotSet						= 14,	// 14 - Pictures is not set
	wsOptCenOffsetsIsNotSet					= 15,	// 15 - Optical centers offsets is not set
	wsCannotCreateObject					= 20,	// 20 - Can't create object
	wsCannotStartThread						= 21,	// 21 - Can't start thread
	wsCannotConstructLens					= 22,	// 22 - Can't create lens
	wsResultPictureNotPresent				= 23,	// 23 - Result Picture not present
} InstallLensRetuningCode;

CShellLens::CShellLens()
{
	VariablePresent = 0;

	m_PWOFLeftPupilX = m_PWOFLeftPupilY = m_PWOFRightPupilX = m_PWOFRightPupilY = 0.;
	m_PWFLeftPupilX = m_PWFLeftPupilY = m_PWFRightPupilX = m_PWFRightPupilY = 0.;
	m_LeftPD = m_RightPD = 0.;
}

CShellLens::~CShellLens()
{
}

/////////////////////////////////////////////////////////////////////////////
// CShellLens message handlers

void CShellLens::SIGNAL_COMPLETION(long Percents)
{
}
void CShellLens::ThrowException(long Percents)
{
}

void CShellLens::OnStart(IWSPicture * lpdispResultPicture)
{
	long ResultCode = wsOK;
	
	// prepare data
	long Mode = ThreadParams.Mode;
	int StartPercent = ThreadParams.StartPercent;
	int EndPercent = ThreadParams.EndPercent;
	ThreadParams.IsBusy = TRUE;

	if((Mode & wsThreadedInstall) > 0)
	{
		SIGNAL_COMPLETION(0);
	}
	else
	{
	}


	PUCHAR Rwof = NULL, Gwof = NULL, Bwof = NULL;
	PUCHAR Rwf = NULL, Gwf = NULL, Bwf = NULL;
	BOOL QuitBe = FALSE, QuitPlaneWof, QuitPlaneWf;

	// �������� �������� ���������
	int WidthWof, HeightWof, WidthWf, HeightWf;
	CColorPlanes ColorPlanes;
	QuitPlaneWof = ColorPlanes.GetColorPlanes(lpdispPictureWithoutFrame, Rwof, Gwof, Bwof, WidthWof, HeightWof);
	QuitPlaneWf = ColorPlanes.GetColorPlanes(lpdispPictureWithFrame, Rwf, Gwf, Bwf, WidthWf, HeightWf);

	// ������ contour point map
	Be.picSize = CSize(WidthWf, HeightWf);
	Be.m_LeftPupilX = m_PWFLeftPupilX;
	Be.m_LeftPupilY = m_PWFLeftPupilY;
	Be.m_RightPupilX = m_PWFRightPupilX;
	Be.m_RightPupilY = m_PWFRightPupilY;
	Be.m_LeftPD = m_LeftPD;
	Be.m_RightPD = m_RightPD;
	Be.lpdispLeftContour = lpdispContour[wsLeft];
	Be.lpdispRightContour = lpdispContour[wsRight];
	Be.m_LeftOptCenOffsetX = m_LeftOptCenOffsetX;
	Be.m_LeftOptCenOffsetY = m_LeftOptCenOffsetY;
	Be.m_RightOptCenOffsetX = m_RightOptCenOffsetX;
	Be.m_RightOptCenOffsetY = m_RightOptCenOffsetY;
	QuitBe = Be.CalculateContourPointMap();	// ������������� ���������� -> "�� ��������� ��������".

	//TRACE1("CalculateContourPointMap -> %dL\n", (ULONG)GetTickCount() - m_Time);

	if(QuitPlaneWof != FALSE && QuitPlaneWf != FALSE && QuitBe != FALSE){
		//	�������������� ������, ���������� �� ���������
		dimPwof.Initialize(CSize(WidthWof, HeightWof), m_LeftPD, m_RightPD, m_PWOFLeftPupilX, m_PWOFLeftPupilY, m_PWOFRightPupilX, m_PWOFRightPupilY);	// ������������� ���������� -> "�� ��������� ��������".
		dimPwf.Initialize(CSize(WidthWf, HeightWf), m_LeftPD, m_RightPD, m_PWFLeftPupilX, m_PWFLeftPupilY, m_PWFRightPupilX, m_PWFRightPupilY);	// ������������� ���������� -> "�� ��������� ��������".
		dimCPwf.Initialize(&dimPwf, Be.GetCenterX(wsLeft), Be.GetCenterY(wsLeft), Be.GetCenterX(wsRight), Be.GetCenterY(wsRight));	// ������������� ���������� -> "�� ��������� ��������".
		dimCContourPwf.Initialize(&dimPwf, Be.GetContourCenterX(wsLeft), Be.GetContourCenterY(wsLeft), Be.GetContourCenterX(wsRight), Be.GetContourCenterY(wsRight));	// ������������� ���������� -> "�� ��������� ��������".

		//	�������������� ������, ���������� �� �������� ���������
		CMassiv mRwof, mGwof, mBwof, mRwf, mGwf, mBwf;
		mRwof.CreateMassiv(WidthWof, HeightWof, Rwof);
		mGwof.CreateMassiv(WidthWof, HeightWof, Gwof);
		mBwof.CreateMassiv(WidthWof, HeightWof, Bwof);
		mRwf.CreateMassiv(WidthWf, HeightWf, Rwf);
		mGwf.CreateMassiv(WidthWf, HeightWf, Gwf);
		mBwf.CreateMassiv(WidthWf, HeightWf, Bwf);
		//	�������������� �����, ���������� �� REFRACTION
		Refraction.InitializeRefraction(&Be, &dimPwof, &dimPwf, &dimCContourPwf,
			&mRwof, &mGwof, &mBwof, lpdispLens[wsRight], lpdispLens[wsLeft],
			AverZLens[wsRight], AverZLens[wsLeft]);

		CRect LensZon = Be.GetLensRect();
		//	����������, ��������������� ����� ��������� ����� ���������� 
		double EstimatedTime = 525. + 2. * ((double)LensZon.Height() * 20.374 + 300.) + 110.;
		long Percent, LastPercent;
	
		//TRACE1("End of initialization -> %dL\n", (ULONG)GetTickCount() - m_Time);
		//	� ���� ����� ���������, ��� ��������� 11% ������
		if((Mode & wsThreadedInstall) > 0){
			Percent = (long)(525. / EstimatedTime * 100.);
			LastPercent = (Percent / 10) * 10;
			SIGNAL_COMPLETION(StartPercent + (long)((double)(EndPercent - StartPercent) * ((double)Percent / 100.)));
//			((CWSImage *)WSImage)->FirePercentsCompleted(StartPercent + (long)((double)(EndPercent - StartPercent) * ((double)Percent / 100.)));
		}

		//	Do it: iLens = 0 - wsRight; iLens = 1 - wsLeft
		const int nLens = 2;
		int iLens;
		FTRECT BoundFrameRect[nLens];
		
		for(iLens = 0; iLens < nLens; iLens++){

			int Y, X;
			double Y1, X1, Y2, X2;

			BoundFrameRect[iLens].left = BoundFrameRect[iLens].top = 1000.;
			BoundFrameRect[iLens].right = BoundFrameRect[iLens].bottom = -1000.;
			
			for(Y = LensZon.top; Y < LensZon.bottom; Y++){
				
				if(Be.GetStartX(Y, iLens) == 0 && Be.GetEndX(Y, iLens) == 0) continue;

				for(X = Be.GetStartX(Y, iLens); X <= Be.GetEndX(Y, iLens); X++){
					if(Be.Belong(X, Y, iLens) == 0) continue;
				
					X1 = (X - dimPwf.GetPupilX(iLens)) * dimPwf.GetMMinPixelByX();
					Y1 = (Y - dimPwf.GetPupilY(iLens)) * dimPwf.GetMMinPixelByY();
					
					X2 =  dimPwf.GetCosinus() * X1 + dimPwf.GetSinus()   * Y1;
					Y2 = -dimPwf.GetSinus()   * X1 + dimPwf.GetCosinus() * Y1;

					if(X2 < BoundFrameRect[iLens].left)
						BoundFrameRect[iLens].left = X2;
					if(X2 > BoundFrameRect[iLens].right)
						BoundFrameRect[iLens].right = X2;
					if(Y2 < BoundFrameRect[iLens].top)
						BoundFrameRect[iLens].top = Y2;
					if(Y2 > BoundFrameRect[iLens].bottom)
						BoundFrameRect[iLens].bottom = Y2;
				}
			}
		}

		for(iLens = 0; iLens < nLens; iLens++){
			//	�� ������� ����� ��� ������� ����������� �������
			// ��� ��� ������������ � �������� ������

//			Refraction.PrepareRefraction(iLens);

			int Y, X;
			
			//TRACE1("Number of calculation lines -> %dL\n", LensZon.Height());

			for(Y = LensZon.top; Y < LensZon.bottom; Y++){
				
				if(Be.GetStartX(Y, iLens) == 0 && Be.GetEndX(Y, iLens) == 0) continue;

				for(X = Be.GetStartX(Y, iLens); X <= Be.GetEndX(Y, iLens); X++){
					if(Be.Belong(X, Y, iLens) == 0) continue;

					// ���� � ����� ���������� ���������: mRwf[Y][X], mGwf[Y][X], mBwf[Y][X]

#define NEWREFRAC

#ifdef NEWREFRAC
					Refraction.CalculateRefraction2(X, Y, iLens,
									0.,
									BoundFrameRect + iLens);
#else
					//	�������� �������� ������� �������� ����� � �������� �����
					double Diopt = Refraction.CalculateBaseDioptries(X, Y, iLens);
					//	���������� � �������� �������� �������� ����������� �������� � �������� �����
					///Diopt += Focal.SelectFocal(X, Y, iLens);
					//	���� �������� � �������� �����, ����������� ���������� ������ (refraction - �����������)
					Refraction.CalculateRefraction(X, Y, iLens, Diopt);
#endif

					double ColRes[3];

					ColRes[0] = Refraction.GetR() * 255.;
					ColRes[1] = Refraction.GetG() * 255.;
					ColRes[2] = Refraction.GetB() * 255.;

					//	� ���������� �������� �������� ��������� ����� ������� �� 255.
					//	����� ������� ��� ����������� ���������� �������� ��������� � ������� 0...255.
					double ColMax = max(ColRes[0], max(ColRes[1], ColRes[2]));
					if(ColMax <= 255.){
						mRwf[Y][X] = (UCHAR)ColRes[0];
						mGwf[Y][X] = (UCHAR)ColRes[1];
						mBwf[Y][X] = (UCHAR)ColRes[2];
					}
					else{
						double Kr[3], Kof = 255. / ColMax;
						for(int i = 0; i < 3; i++)
							Kr[i] = 1.-(ColMax-ColRes[i])/ColMax * Kof;

						mRwf[Y][X] = (UCHAR)(255. * Kr[0]);
						mGwf[Y][X] = (UCHAR)(255. * Kr[1]);
						mBwf[Y][X] = (UCHAR)(255. * Kr[2]);
					}
				}

				if((Mode & wsThreadedInstall) > 0){
					Percent = (long)((525. + iLens * (LensZon.Height() * 20.374 + 300.) + (Y - LensZon.top) * 20.374) / EstimatedTime * 100.);
					if(Percent >= LastPercent + 10){
						LastPercent = (Percent / 10) * 10;
						SIGNAL_COMPLETION(StartPercent + (long)((double)(EndPercent - StartPercent) * ((double)Percent / 100.)));
//						((CWSImage *)WSImage)->FirePercentsCompleted(StartPercent + (long)((double)(EndPercent - StartPercent) * ((double)Percent / 100.)));
					}
				}
				//TRACE1("End of Y -> %dL\n", (ULONG)GetTickCount() - m_Time);
			}
			
			if((Mode & wsThreadedInstall) > 0){
				Percent = (long)((525. + (iLens + 1) * (LensZon.Height() * 20.374 + 300.)) / EstimatedTime * 100.);
				if(Percent >= LastPercent + 10){
					LastPercent = (Percent / 10) * 10;
					SIGNAL_COMPLETION(StartPercent + (long)((double)(EndPercent - StartPercent) * ((double)Percent / 100.)));
//					((CWSImage *)WSImage)->FirePercentsCompleted(StartPercent + (long)((double)(EndPercent - StartPercent) * ((double)Percent / 100.)));
				}
			}
			//TRACE1("End calculate of one lense -> %dL\n", (ULONG)GetTickCount() - m_Time);
		}
		//	��������� �������� �����������
		ColorPlanes.CreatePictureFromColorPlanes(lpdispResultPicture,
			Rwf, Gwf, Bwf, WidthWf, HeightWf);

		//	����������� �������� ������
		mRwof.Destroy(); mGwof.Destroy(); mBwof.Destroy();
		mRwf.Destroy(); mGwf.Destroy(); mBwf.Destroy();
	}
	else if(QuitPlaneWof == FALSE) ResultCode = wsErrorUsingPictureWithoutFrame;
	else if(QuitPlaneWf == FALSE) ResultCode = wsErrorUsingPictureWithFrame;
	else if(QuitBe == FALSE)ResultCode = wsErrorCalculateContourPointMap;

	if(Rwof != NULL){free(Rwof); Rwof = NULL;}
	if(Gwof != NULL){free(Gwof); Gwof = NULL;}
	if(Bwof != NULL){free(Bwof); Bwof = NULL;}
	if(Rwf != NULL){free(Rwf); Rwf = NULL;}
	if(Gwf != NULL){free(Gwf); Gwf = NULL;}
	if(Bwf != NULL){free(Bwf); Bwf = NULL;}

	ThreadParams.IsBusy = FALSE;

	if((Mode & wsThreadedInstall) > 0)
	{
		SIGNAL_COMPLETION(100);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CShellLens helpers and initializers

void CShellLens::SetPupilsPWOF(double PWOFRightPupilX, double PWOFRightPupilY, double PWOFLeftPupilX, double PWOFLeftPupilY)
{
	m_PWOFRightPupilX = PWOFRightPupilX;
	m_PWOFRightPupilY = PWOFRightPupilY;
	m_PWOFLeftPupilX = PWOFLeftPupilX;
	m_PWOFLeftPupilY = PWOFLeftPupilY;

	VariablePresent |= wsPupilsPWOF;
}
void CShellLens::SetPupilsPWF(double PWFRightPupilX, double PWFRightPupilY, double PWFLeftPupilX, double PWFLeftPupilY)
{
	m_PWFRightPupilX = PWFRightPupilX;
	m_PWFRightPupilY = PWFRightPupilY;
	m_PWFLeftPupilX = PWFLeftPupilX;
	m_PWFLeftPupilY = PWFLeftPupilY;

	VariablePresent |=	wsPupilsPWF;
}
void CShellLens::SetPDs(double RightPD, double LeftPD)
{
	m_LeftPD = LeftPD;
	m_RightPD = RightPD;

	VariablePresent |= wsPDs;
}
void CShellLens::SetContours(IContour * lpdispRight, IContour * lpdispLeft)
{
	lpdispContour[wsLeft] = lpdispLeft;
	lpdispContour[wsRight] = lpdispRight;
	
	VariablePresent |= wsContours;
}
void CShellLens::SetLenses(ILens * lpdispRight, ILens * lpdispLeft)
{
	// SetLenses ������ ���������� ��������� �� ���� Set-��
	ASSERT(	(VariablePresent & wsContours) &&
			(VariablePresent & wsPupilsPWOF) &&
			(VariablePresent & wsPupilsPWF) &&
			(VariablePresent & wsPictures) &&
			(VariablePresent & wsOffsets) &&
			(VariablePresent & wsPDs));

	int OurLensDiam[2];

	lpdispLens[wsLeft] = lpdispLeft;
	lpdispLens[wsRight] = lpdispRight;

	try
	{
		ConvertCoords(lpdispPictureWithFrame->GetWidth(), lpdispPictureWithoutFrame->GetHeight(),
						lpdispLens[wsLeft].m_p, lpdispLens[wsRight].m_p,
						lpdispContour[wsLeft].m_p, lpdispContour[wsRight].m_p,
						m_LeftPD, m_RightPD,
						m_PWFLeftPupilX, m_PWFLeftPupilY,
						m_PWFRightPupilX, m_PWFRightPupilY,
						m_RightOptCenOffsetX, m_RightOptCenOffsetY,
						m_LeftOptCenOffsetX, m_LeftOptCenOffsetY);
	}
	catch(...)
	{
		goto Exit;		
	}

	OurLensDiam[wsLeft] = (int)ceil(lpdispLens[wsLeft]->GetMinDiam() + .5);
	OurLensDiam[wsRight] = (int)ceil(lpdispLens[wsRight]->GetMinDiam() + .5);

	if(OurLensDiam[wsLeft] > lpdispLens[wsLeft]->GetDiameter())
		lpdispLens[wsLeft]->SetDiameter(OurLensDiam[wsLeft]);

	if(OurLensDiam[wsRight] > lpdispLens[wsRight]->GetDiameter())
		lpdispLens[wsRight]->SetDiameter(OurLensDiam[wsRight]);

Exit:
	VariablePresent |= wsLenses;
}
void CShellLens::SetPictures(IWSPicture * lpdispPictureWOF, IWSPicture * lpdispPictureWF)
{
	lpdispPictureWithoutFrame = lpdispPictureWOF;
	lpdispPictureWithFrame = lpdispPictureWF;

	VariablePresent |= wsPictures;
}
void CShellLens::SetOptCentOffsets(double RightOptCenOffsetX, double RightOptCenOffsetY, double LeftOptCenOffsetX, double LeftOptCenOffsetY)
{
	m_LeftOptCenOffsetX = LeftOptCenOffsetX;
	m_LeftOptCenOffsetY = LeftOptCenOffsetY;
	m_RightOptCenOffsetX = RightOptCenOffsetX;
	m_RightOptCenOffsetY = RightOptCenOffsetY;

	VariablePresent |= wsOffsets;
}
CRect CShellLens::GetLensRect()
{
	return Be.GetLensRect();
}

long CShellLens::InstallLens(IWSPicture * lpdispResultPicture, long Mode, int StartPercent, int EndPercent)
{
	if((VariablePresent & wsPupilsPWOF) == 0) return wsPupilsFromPictureWithoutFrameNotSet;
	if((VariablePresent & wsPupilsPWF) == 0) return wsPupilsFromPictureWithFrameNotSet;
	if((VariablePresent & wsPDs) == 0) return wsPDsisNotSet;
	if((VariablePresent & wsContours) == 0) return wsContoursIsNotSet;
	if((VariablePresent & wsLenses) == 0) return wsLensesIsNotSet;
	if((VariablePresent & wsPictures) == 0) return wsPicturesIsNotSet;
	if((VariablePresent & wsOffsets) == 0) return wsOptCenOffsetsIsNotSet;

	if(lpdispResultPicture == NULL) return wsResultPictureNotPresent;
	if(lpdispPictureWithoutFrame.m_p == NULL) return wsPictureWithoutFrameNotPresent;
	if(lpdispPictureWithFrame.m_p == NULL) return wsPictureWithFrameNotPresent;
	if(lpdispLens[wsRight].m_p == NULL) return wsRightLensNotPresent;
	if(lpdispLens[wsLeft].m_p == NULL) return wsLeftLensNotPresent;

	string Design = lpdispLens[wsRight]->GetDesign();
	if(Design != wsClassic && Design != wsAspheric && Design != wsLenticular)
		return wsErrorPrepareLensForm;

	CLensDriverBaseProducer BaseProducer;
	double LensDiam, RefrIndex, LensGeomCenterX, LensGeomCenterY, EllipticVertToHorizRatio;

	for(int iLens = 0; iLens < 2; iLens++)
	{
		BaseProducer.Prepare(lpdispLens[iLens]);

		LensDiam = lpdispLens[iLens]->GetDiameter();
		RefrIndex = lpdispLens[iLens]->GetRefIndex();

		double m_OptToGeomCenterDisplacementX = lpdispLens[iLens]->GetOptToGeomCenterDisplacementX(),
				m_OptToGeomCenterDisplacementY = lpdispLens[iLens]->GetOptToGeomCenterDisplacementY();

		if(wsProgressive == lpdispLens[iLens]->GetMultifocalType())
		{
			CObjPtr<IProgressive> ProgrObj;
			ProgrObj.AttachDispatch(lpdispLens[iLens]->GetProgressive());
			if(ProgrObj.m_p)
			{
				m_OptToGeomCenterDisplacementX = -ProgrObj->GetFarVisionCenterX();
				m_OptToGeomCenterDisplacementY = -ProgrObj->GetFarVisionCenterY() + 0.5 * ProgrObj->GetFarVisionMarkerDiam();
			}
		}	

		LensGeomCenterX = lpdispLens[iLens]->GetCenterX() + m_OptToGeomCenterDisplacementX;
		LensGeomCenterY = lpdispLens[iLens]->GetCenterY() + m_OptToGeomCenterDisplacementY;

		if(!lpdispLens[iLens]->GetEllipticBlank())
			EllipticVertToHorizRatio = 1.;
		else
		{
			EllipticVertToHorizRatio = lpdispLens[iLens]->GetEllipticVertToHorizRatio();
			
			if(EllipticVertToHorizRatio > 10.)
			{
				double NewEllipseHorizMinusVert = 100. - EllipticVertToHorizRatio;
				EllipticVertToHorizRatio = (LensDiam - NewEllipseHorizMinusVert) / LensDiam;
			}
		}
/*

*/
		Refraction.RayTracer[iLens].PrepareByProducer(&BaseProducer,
														LensDiam,
														RefrIndex,
														LensGeomCenterX,
														LensGeomCenterY,
														EllipticVertToHorizRatio,
														AverZLens + iLens);
		if(!Refraction.RayTracer[iLens].IsReady())
		{
			ErrorMsg = Refraction.RayTracer[iLens].GetLastErrorString();
			return wsCannotConstructLens;
		}
	}

	ThreadParams.Mode = Mode;
	ThreadParams.StartPercent = StartPercent;
	ThreadParams.EndPercent = EndPercent;

	if((Mode & wsThreadedInstall) > 0)
	{
	}
	else
	{
		OnStart(lpdispResultPicture);
	}

	return wsOK;
}

void CShellLens::ConvertCoords(int PicWidth, int PicHeight,
				ILens * lpdispLeftLens, ILens * lpdispRightLens,
				IContour * lpdispLeftContour, IContour * lpdispRightContour,
				double LeftPD, double RightPD,
				double PWFLeftPupilX, double PWFLeftPupilY,
				double PWFRightPupilX, double PWFRightPupilY,
				double RightOptCenOffsetX, double RightOptCenOffsetY,
				double LeftOptCenOffsetX, double LeftOptCenOffsetY
				)
{
	int iLens, i;
	double xcContour[2], ycContour[2];
	
	if(lpdispLeftContour == NULL) return;
	if(lpdispRightContour == NULL) return;

	CObjPtr<IContour> OutContour[2];
	for(iLens = 0; iLens < 2; iLens++) {
		CContour * pOutContour = CContour::CreateObject();
		if(!pOutContour) {
			ThrowException(25);	// -> Can't create object
			return;
		}
		OutContour[iLens].AttachDispatch(pOutContour->GetInterface());
	}
	
	CDimension Dim, DimC;
	IContour * Contour[2];

	int Width = PicWidth;
	int Height = PicHeight;
	
	Contour[wsRight] = lpdispRightContour;
	Contour[wsLeft] = lpdispLeftContour;

	Dim.Initialize(CSize(Width, Height),
		LeftPD, RightPD,
		PWFLeftPupilX, PWFLeftPupilY,
		PWFRightPupilX, PWFRightPupilY);

	CSpline Spline[2];
	CFTPoint * ContourFloat;

	for(iLens = 0; iLens < 2; iLens++){
		int Count = Contour[iLens]->GetCount();
		ContourFloat = new CFTPoint[Count];
		if(ContourFloat == NULL){
			ThrowException(5);	// -> Insufficient memory
			return;
		}
		for(i = 0; i < Count; i++){
			CObjPtr<IContourPoint> cp;
			cp.AttachDispatch(Contour[iLens]->GetItem(i+1));
			ContourFloat[i].x = cp->GetX();
			ContourFloat[i].y = cp->GetY();
		}
		if(Count < 30)	Spline[iLens].CreateSpline(Count, ContourFloat);
		else			Spline[iLens].DuplicateSpline(Count, ContourFloat);
		delete [] ContourFloat;
		if(Spline[iLens].SplExists == FALSE){
			// ����� ���� ������ �� ��������
			ThrowException(5);	// -> Insufficient memory
			return;
		}

		xcContour[iLens] = ycContour[iLens] = 0.;
		for(i = 0; i < Spline[iLens].nPolyNodes; i++){
			CFTPoint FTPoint = Spline[iLens].FloatPolyNodes[i];
			xcContour[iLens] += FTPoint.x;
			ycContour[iLens] += FTPoint.y;
		}
		xcContour[iLens] /= (double)Spline[iLens].nPolyNodes;
		ycContour[iLens] /= (double)Spline[iLens].nPolyNodes;
	}

	double dx, dy, dc;
	dx = (xcContour[wsLeft] - xcContour[wsRight]) * Dim.GetMMinPixelByX();
	dy = (ycContour[wsLeft] - ycContour[wsRight]) * Dim.GetMMinPixelByY();
	dc = _hypot(dx, dy);

	DimC.Initialize(CSize(Width, Height), dc/2., dc/2., xcContour[wsLeft], ycContour[wsLeft], xcContour[wsRight], ycContour[wsRight]);

	double CenterX = dx / 2.;
	double CenterY = dy / 2.;
	double Ymin = 1000000.;
	double X, Y, dxP, dyP;
	
	for(iLens = 0; iLens < 2; iLens++){
		OutContour[iLens]->Clear();

		for(i = 0; i < Spline[iLens].nPolyNodes; i++){
			switch(iLens){
			case wsRight:
				dxP = (xcContour[iLens] - Spline[iLens].FloatPolyNodes[i].x) * Dim.GetMMinPixelByX() + CenterX;
				dyP = (ycContour[iLens] - Spline[iLens].FloatPolyNodes[i].y) * Dim.GetMMinPixelByY() + CenterY;
					X =  dxP * DimC.GetCosinus() + dyP * DimC.GetSinus();
					Y = -dxP * DimC.GetSinus()   + dyP * DimC.GetCosinus();
				break;
			case wsLeft:
				dxP = (Spline[iLens].FloatPolyNodes[i].x - xcContour[iLens]) * Dim.GetMMinPixelByX() + CenterX;
				dyP = (ycContour[iLens] - Spline[iLens].FloatPolyNodes[i].y) * Dim.GetMMinPixelByY() - CenterY;
					X =  dxP * DimC.GetCosinus() - dyP * DimC.GetSinus();
					Y =  dxP * DimC.GetSinus()   + dyP * DimC.GetCosinus();
				break;
			}

			OutContour[iLens]->Add(X, Y)->Release();
			if(Ymin > Y) Ymin = Y;
		}
	}

	for(iLens = 0; iLens < 2; iLens++){
		int Count = OutContour[iLens]->GetCount();
		for(i = 0; i < Count; i++){
			CObjPtr<IContourPoint> cp;
			cp.AttachDispatch(OutContour[iLens]->GetItem(i+1));
			double Y;
			Y = cp->GetY() - Ymin;
			cp->SetY(Y);
		}
	}

	for(iLens = 0; iLens < 2; iLens++){
		switch(iLens){
		case wsRight:
			dxP = (-RightOptCenOffsetX) * Dim.GetMMinPixelByX() + CenterX;
			dyP = (-RightOptCenOffsetY) * Dim.GetMMinPixelByY() + CenterY;
				xcContour[iLens] =  dxP * DimC.GetCosinus() + dyP * DimC.GetSinus();
				ycContour[iLens] = -dxP * DimC.GetSinus()   + dyP * DimC.GetCosinus();
			break;
		case wsLeft:
			dxP = LeftOptCenOffsetX * Dim.GetMMinPixelByX() + CenterX;
			dyP = (-LeftOptCenOffsetY) * Dim.GetMMinPixelByY() - CenterY;
				xcContour[iLens] =  dxP * DimC.GetCosinus() - dyP * DimC.GetSinus();
				ycContour[iLens] =  dxP * DimC.GetSinus()   + dyP * DimC.GetCosinus();
		break;
		}

		ycContour[iLens] -= Ymin;
	}

	ILens * Lens[2];
	Lens[wsRight] = lpdispRightLens;
	Lens[wsLeft] = lpdispLeftLens;
	for(iLens = 0; iLens < 2; iLens++){
		Lens[iLens]->SetContour(OutContour[iLens]);
//		Lens[iLens]->SetCenterX(dc / 2.);
//		Lens[iLens]->SetCenterY(-Ymin);
		Lens[iLens]->SetCenterX(xcContour[iLens]);
		Lens[iLens]->SetCenterY(ycContour[iLens]);
	}
}