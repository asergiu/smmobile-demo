#pragma once
// FTPoint.h: interface for the CFTPoint class.

#if !defined __FTPOINT__
#define __FTPOINT__
typedef struct tagFTPOINT {
	double x,y;
	} FTPOINT,*PFTPOINT;
#endif

class CFTPoint : public tagFTPOINT
{
public:

// Constructors
	CFTPoint(){};
	CFTPoint(FTPOINT initPt){ *(FTPOINT*)this = initPt; };
	CFTPoint(double initX, double initY){ x = initX; y = initY; };

// Operations
	void Offset(double xOffset, double yOffset){ x += xOffset; y += yOffset; };
	void Offset(FTPOINT point){ x += point.x; y += point.y; };
	BOOL operator==(FTPOINT point) const{ return (x == point.x && y == point.y); };
	BOOL operator!=(FTPOINT point) const{ return (x != point.x || y != point.y); };
	void operator+=(FTPOINT point){ x += point.x; y += point.y; };
	void operator-=(FTPOINT point){ x -= point.x; y -= point.y; };

// Operators returning CFTPoint values
	CFTPoint operator-() const{ return CFTPoint(-x, -y); };
	CFTPoint operator+(FTPOINT point) const{ return CFTPoint(x + point.x, y + point.y); };
	CFTPoint operator-(FTPOINT point) const{ return CFTPoint(x - point.x, y - point.y); };
};
