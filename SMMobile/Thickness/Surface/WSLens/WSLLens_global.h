#ifndef WSLLENS_GLOBAL_H
#define WSLLENS_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(WSLLENS_LIBRARY)
#  define WSLLENSSHARED_EXPORT Q_DECL_EXPORT
#else
#  define WSLLENSSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // WSLLENS_GLOBAL_H
