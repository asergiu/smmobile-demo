// CirclesCrossPoints.cpp: implementation of the CCirclesCrossPoints class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CirclesCrossPoints.h"
#include "UsefulThings.h"
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCirclesCrossPoints::CCirclesCrossPoints()
{
	nPoints = 0;
	X[0] = X[1] = Y[0] = Y[1] = 0.;
}

CCirclesCrossPoints::~CCirclesCrossPoints()
{

}

void CCirclesCrossPoints::Calculate(double Xa, double Ya, double Ra, double Xb, double Yb, double Rb)
{
	nPoints = 0;
	X[0] = X[1] = Y[0] = Y[1] = 0.;
	
	double Ax = 2. * (Xb - Xa);
	double Bx = Xa * Xa - Xb * Xb;
	double Ay = 2. * (Yb - Ya);
	double By = Ya * Ya - Yb * Yb;
	double B = Bx + By;
	double R = Ra * Ra - Rb * Rb;

	// ���������� ������� ������, ����� Ay ����� ����.
	if(fabs(Ay) < EPS){
		if(fabs(Ax) < EPS) return;	// ������ �� ����� ������������ �������.
		double Xp = (R - B) / Ax;
		double Xk = (Xp - Xa) * (Xp - Xa);
		double Yk = Ya * Ya + Xk - R * R;
		double D = 4. * (Ya * Ya - Yk);
		if(D < -EPS) return;		// ����� ����������� ���.
		else if(fabs(D) < EPS){		// ���������� ���� ����� �����������.
			nPoints = 1;
			X[0] = Xp;
			Y[0] = Ya;
			return;
		}
		else{					// ���������� ��� ����� �����������.
			nPoints = 2;
			X[0] = X[1] = Xp;
			Y[0] = (2. * Ya - sqrt(D)) / 2.;
			Y[1] = (2. * Ya + sqrt(D)) / 2.;
			return;
		}
	}
	
	// ���������� ������� ������, ����� Ay �� ����� ����.
	double K = (R - B) / Ay;
	double Ka = Ax / Ay;
	double a = 1. + Ka * Ka;
	double b = 2. * Xa + 2. * (K - Ya) * Ka;
	double c = Xa * Xa + (K - Ya)*(K - Ya) - Ra * Ra;

	double d = b * b - 4. * a * c;
	if(d < -EPS) return;		// ����� ����������� ���.
	else if(fabs(d) < EPS){		// ���������� ���� ����� �����������.
		nPoints = 1;
		X[0] = b / (2. * a);
		Y[0] = K - X[0] * Ka;
		return;
	}
	else{					// ���������� ��� ����� �����������.
		nPoints = 2;
		X[0] = (b - sqrt(d)) / (2. * a);
		X[1] = (b + sqrt(d)) / (2. * a);
		Y[0] = K - X[0] * Ka;
		Y[1] = K - X[1] * Ka;
		return;
	}
}
