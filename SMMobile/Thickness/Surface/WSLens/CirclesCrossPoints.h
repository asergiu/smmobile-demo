#pragma once
// CirclesCrossPoints.h: interface for the CCirclesCrossPoints class.
//

class CCirclesCrossPoints  
{
public:
	CCirclesCrossPoints();
	virtual ~CCirclesCrossPoints();

	void Calculate(double Xa, double Ya, double Ra, double Xb, double Yb, double Rb);

	int nPoints;
	double X[2], Y[2];
};
