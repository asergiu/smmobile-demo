// -----------------------------------------------------
// ‘ÛÌÍˆËË ‰Îˇ ‡·ÓÚ˚ Ò Ù‡ÈÎ‡ÏË ‚ ÙÓÏ‡ÚÂ DIB
// -----------------------------------------------------

#include "stdafx.h"
#include "WSPicture.h"
#ifdef _WINDOWS_
#include <io.h>
#endif
#include <fcntl.h>
//#include <stdlib.h>
//#include <stdio.h>
#include <unistd.h>

// -------------------------------
// ‘ÛÌÍˆËˇ DIBReadFile
// ◊ÚÂÌËÂ DIB-Ù‡ÈÎ‡
// -------------------------------

unsigned char * CWSPicture::DIBReadFile(LPCTSTR FileName, int *dwFileSize)
{
	unsigned char * pResultPicture = NULL;

	// ”Í‡Á‡ÚÂÎ¸ Ì‡ „ÎÓ·‡Î¸Ì˚È ·ÎÓÍ Ô‡ÏˇÚË
	LPDIB lpBuf = NULL;

	int hfDIBFile;
    if((hfDIBFile = open(FileName, O_RDONLY)) != -1){
		// ŒÔÂ‰ÂÎˇÂÏ ‡ÁÏÂ Ù‡ÈÎ‡. ƒÎˇ ˝ÚÓ„Ó
		// ÛÒÚ‡Ì‡‚ÎË‚‡ÂÏ ÚÂÍÛ˘Û˛ ÔÓÁËˆË˛ Ì‡
		// ÍÓÌÂˆ Ù‡ÈÎ‡
        *dwFileSize = lseek(hfDIBFile, 0l, SEEK_END);

		// ”ÒÚ‡Ì‡‚ÎË‚‡ÂÏ ÚÂÍÛ˘Û˛ ÔÓÁËˆË˛
		// Ì‡ Ì‡˜‡ÎÓ Ù‡ÈÎ‡
        lseek(hfDIBFile, 0l, SEEK_SET);

		// «‡Í‡Á˚‚‡ÂÏ Ô‡ÏˇÚ¸,
		// ‡ÁÏÂ ÍÓÚÓÓ„Ó ‡‚ÂÌ ‰ÎËÌÂ Ù‡ÈÎ‡
		lpBuf = new unsigned char[*dwFileSize];

		// ≈ÒÎË Ï‡ÎÓ Ò‚Ó·Ó‰ÌÓÈ Ô‡ÏˇÚË,
		// ‚ÓÁ‚‡˘‡ÂÏ ÔËÁÌ‡Í Ó¯Ë·ÍË
		if(lpBuf == NULL) return(NULL);

		// ◊ËÚ‡ÂÏ Ù‡ÈÎ ‚ ÔÓÎÛ˜ÂÌÌ˚È ·ÎÓÍ Ô‡ÏˇÚË
        read(hfDIBFile, lpBuf, *dwFileSize);

		// «‡Í˚‚‡ÂÏ Ù‡ÈÎ
        close(hfDIBFile);

		pResultPicture = lpBuf;
	}
	return pResultPicture;
}

// -------------------------------
// ‘ÛÌÍˆËˇ DIBType
// ŒÔÂÂ‰ÂÎÂÌËÂ Ë ÔÓ‚ÂÍ‡ ÙÓÏ‡Ú‡ DIB
// -------------------------------

int CWSPicture::DIBType(LPDIB lpDib)
{
	LPBITMAPFILEHEADER lpDIBFileHeader;
	LPBITMAPINFOHEADER lpih;
	LPBITMAPCOREHEADER lpch;

	int biSize;
	LPDIB hDIBPtr;
	int   nDIBType;

	// ‘ËÍÒËÛÂÏ Ô‡ÏˇÚ¸, ‚ ÍÓÚÓÓÈ Ì‡ıÓ‰ËÚÒˇ DIB
	hDIBPtr = (LPDIB)lpDib;
	if(hDIBPtr == NULL)
	return(-1);

	lpDIBFileHeader = (LPBITMAPFILEHEADER)hDIBPtr;

	// œÓ‚ÂˇÂÏ ÚËÔ Ù‡ÈÎ‡
    if(lpDIBFileHeader->bfType != 0x4d42) {
		return 0;
	}

	// œÓ‚ÂˇÂÏ ‡ÁÏÂ Á‡„ÓÎÓ‚Í‡
    biSize = (int)(hDIBPtr[sizeof(BITMAPFILEHEADER)]);

	if(biSize == sizeof(BITMAPINFOHEADER)) // 40 ·‡ÈÚ
	{
	// ›ÚÓ Á‡„ÓÎÓ‚ÓÍ DIB ‚ ÙÓÏ‡ÚÂ Windows
	lpih = (LPBITMAPINFOHEADER)(hDIBPtr + sizeof(BITMAPFILEHEADER));

	// œÓ‚ÂˇÂÏ ÓÒÌÓ‚Ì˚Â ÔÓÎˇ Á‡„ÓÎÓ‚Í‡ DIB
	if((lpih->biPlanes   == 1) &&
		((lpih->biBitCount == 1) ||
		(lpih->biBitCount == 4) ||
		(lpih->biBitCount == 8) ||
		(lpih->biBitCount == 16)||
		(lpih->biBitCount == 24)||
		(lpih->biBitCount == 32)) &&
		((lpih->biCompression  == BI_RGB) ||
			(lpih->biCompression == BI_RLE4 && lpih->biBitCount == 4) ||
			(lpih->biCompression == BI_RLE8 && lpih->biBitCount == 8)))
	{
		// ŒÔÂ‰ÂÎˇÂÏ ÏÂÚÓ‰ ÍÓÏÔÂÒÒËË Ù‡ÈÎ‡
		if(lpih->biCompression  == BI_RGB)
			nDIBType = WINRGB_DIB;
		else if(lpih->biCompression  == BI_RLE4)
			nDIBType = WINRLE4_DIB;
		else if(lpih->biCompression  == BI_RLE8)
			nDIBType = WINRLE8_DIB;
		else
			nDIBType = 0;
	}
	else
		nDIBType = 0;
	}

	else if(biSize == sizeof(BITMAPCOREHEADER)) // 12 ·‡ÈÚ
	{
		// ›ÚÓ Á‡„ÓÎÓ‚ÓÍ DIB ‚ ÙÓÏ‡ÚÂ Presentation Manager
		lpch = (LPBITMAPCOREHEADER)(hDIBPtr + sizeof(BITMAPFILEHEADER));

		// œÓ‚ÂˇÂÏ ÓÒÌÓ‚Ì˚Â ÔÓÎˇ Á‡„ÓÎÓ‚Í‡ DIB
		if((lpch->bcPlanes   == 1) &&
			(lpch->bcBitCount == 1 ||
			lpch->bcBitCount == 4 ||
			lpch->bcBitCount == 8 ||
			lpch->bcBitCount == 24))
		{
			nDIBType = PM_DIB;
		}
		else
			nDIBType = 0;
	}

	else
		nDIBType = 0;

	// ¬ÓÁ‚‡˘‡ÂÏ ÚËÔ Ù‡ÈÎ‡ ËÎË ÔËÁÌ‡Í Ó¯Ë·ÍË
	return nDIBType;
}

// -------------------------------
// ‘ÛÌÍˆËˇ DIBNumColors
// ŒÔÂ‰ÂÎÂÌËÂ ‡ÁÏÂ‡ Ô‡ÎËÚ˚
// -------------------------------

int CWSPicture::DIBNumColors(LPDIB lpDib)
{
	int dwColorUsed;
	LPBITMAPINFOHEADER lpih;

	lpih =
	(LPBITMAPINFOHEADER)(lpDib + sizeof(BITMAPFILEHEADER));

	//  ÓÎË˜ÂÒÚ‚Ó ˆ‚ÂÚÓ‚
	dwColorUsed = lpih->biClrUsed;

	// ≈ÒÎË ËÒÔÓÎ¸ÁÛÂÚÒˇ Ô‡ÎËÚ‡ ÛÏÂÌ¸¯ÂÌÌÓ„Ó ‡ÁÏÂ‡,
	// ‚ÓÁ‚‡˘‡ÂÏ ÌÛÊÌ˚È ‡ÁÏÂ
	if(dwColorUsed)
		return((int)dwColorUsed);

	// ≈ÒÎË ÍÓÎË˜ÂÒÚ‚Ó ËÒÔÓÎ¸ÁÓ‚‡ÌÌ˚ı ˆ‚ÂÚÓ‚ ÌÂ ÛÍ‡Á‡ÌÓ,
	// ‚˚˜ËÒÎˇÂÏ ÒÚ‡Ì‰‡ÚÌ˚È ‡ÁÏÂ Ô‡ÎËÚ˚ ËÒıÓ‰ˇ ËÁ
	// ÍÓÎË˜ÂÒÚ‚‡ ·ËÚ, ÓÔÂ‰ÂÎˇ˛˘Ëı ˆ‚ÂÚ ÔËÍÒÂÎ‡
	switch(lpih->biBitCount)
	{
	case 1:
		return 2;
	case 4:
		return 16;
	case 8:
		return 256;
	default:
		return 0;   // Ô‡ÎËÚ‡ ÌÂ ËÒÔÓÎ¸ÁÛÂÚÒˇ
	}
}
