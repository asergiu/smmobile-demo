#include "stdafx.h"
#include "ShellPartner.h"
#include "Massiv.h"

#define WIDTHBYTES(i)   ((unsigned)((i+31)&(~31))/8)  /* ULONG aligned ! */

BOOL CColorPlanes::GetColorPlanes(IWSPicture * lpdispPicture, PUCHAR &R, PUCHAR &G, PUCHAR &B, int& Width, int& Height)
{
	if(R != NULL || G != NULL || B != NULL || lpdispPicture == NULL) return FALSE;
	
	BOOL Result = FALSE;

	PUCHAR RedPlane = NULL, GreenPlane = NULL, BluePlane = NULL;
	int WidthPlane = 0, HeightPlane = 0;

	PUCHAR pPicture = (PUCHAR)lpdispPicture->GetPicture();
	if(pPicture != NULL){
		BITMAPINFOHEADER bmiHeader = *((LPBITMAPINFOHEADER)pPicture);
		
		WidthPlane = bmiHeader.biWidth;
		HeightPlane = bmiHeader.biHeight;

		RedPlane = (PUCHAR)malloc(WidthPlane * HeightPlane);
		GreenPlane = (PUCHAR)malloc(WidthPlane * HeightPlane);
		BluePlane = (PUCHAR)malloc(WidthPlane * HeightPlane);

		if(RedPlane != NULL && GreenPlane != NULL && BluePlane != NULL){
			int iOffset = 0, aOffset = 0;
			BOOL Format = TRUE;
			unsigned int * bmiColors;
			int LineLenBitmap;
			int i, j, k, l;

			if(bmiHeader.biCompression == BI_BITFIELDS && (bmiHeader.biBitCount == 16 || bmiHeader.biBitCount == 32))
				iOffset = bmiHeader.biSize + 3 * sizeof(DWORD);
			else if(bmiHeader.biCompression != BI_RGB)
				Format = FALSE;
			if(bmiHeader.biBitCount == 8 && iOffset == 0)
				iOffset = bmiHeader.biSize + 256 * sizeof(DWORD);
			if(bmiHeader.biClrUsed > 0)
				aOffset = bmiHeader.biClrUsed * sizeof(DWORD);

			bmiColors = (unsigned int *)(pPicture + bmiHeader.biSize);

			LineLenBitmap = (unsigned short)WIDTHBYTES(bmiHeader.biWidth * bmiHeader.biBitCount);

			if(iOffset == 0) iOffset = bmiHeader.biSize;
			
			if(Format){
				switch(bmiHeader.biBitCount){
				case  8:
					if(aOffset > 0)
						iOffset = bmiHeader.biSize + aOffset;
					for(j = 0; j < (int)bmiHeader.biHeight; j++){
						for(i = 0; i < (int)bmiHeader.biWidth; i++){
							int Lplane = WidthPlane * j + i;
							int Lbmp = iOffset + LineLenBitmap * ((int)bmiHeader.biHeight - 1 - j) + i * (bmiHeader.biBitCount/8);
							unsigned int Color = 0;
							memcpy(&Color, pPicture+Lbmp, (bmiHeader.biBitCount/8));
							RedPlane[Lplane]	= ((RGBQUAD *)bmiColors)[Color].rgbRed;
							GreenPlane[Lplane]	= ((RGBQUAD *)bmiColors)[Color].rgbGreen;
							BluePlane[Lplane]	= ((RGBQUAD *)bmiColors)[Color].rgbBlue;
						}
					}
					Result = TRUE;
					break;
				case 32:
				case 16:
					int rgbShift[3], rgbColor[3];
					if(iOffset - bmiHeader.biSize >= 12){
						unsigned int Maska;
						for(j = 0; j < 3; j++){
							Maska = (unsigned int)bmiColors[j];
							for(i = 0; i < (int)bmiHeader.biBitCount/8; i++){
								int d = Maska % 2;
								if( d > 0) break;
							}
							rgbShift[j]	= i;
							rgbColor[j] = bmiColors[j];
							Maska = ((unsigned int)rgbColor[j] >> (unsigned int)rgbShift[j]) + 1;
							for(i = 0; i < (int)bmiHeader.biBitCount/8; i++){
								int d = Maska % 2;
								if( d > 0) break;
							}
							rgbShift[j]	-= 8 - i;
						}
					}
					else{
						switch(bmiHeader.biBitCount){
							case 32:
								rgbColor[0] = 0xFF0000; rgbColor[1] = 0xFF00; rgbColor[2] = 0xFF;
								rgbShift[0] = 16;	    rgbShift[1] = 8;	  rgbShift[2] = 0;
								break;
							case 16:
	//							rgbColor[0] = 0xF800; rgbColor[1] = 0x7E0; rgbColor[2] = 0x1F;
	//							rgbShift[0] = 8;	  rgbShift[1] = 3;	   rgbShift[2] = -3;
								rgbColor[0] = 0x7C00; rgbColor[1] = 0x3E0; rgbColor[2] = 0x1F;
								rgbShift[0] = 7;	  rgbShift[1] = 2;	   rgbShift[2] = -3;
								break;
						}
					}
					iOffset += aOffset;
					for(j = 0; j < (int)bmiHeader.biHeight; j++){
						for(i = 0; i < (int)bmiHeader.biWidth; i++){
							int Lplane = WidthPlane * j + i;
							int Lbmp = iOffset + LineLenBitmap * ((int)bmiHeader.biHeight - 1 - j) + i * (bmiHeader.biBitCount/8);
							unsigned int Color = 0;
							memcpy(&Color, pPicture+Lbmp, (bmiHeader.biBitCount/8));
							for(l = 0; l < 3; l++){
								UCHAR Col;
								if(rgbShift[2-l] < 0)
									Col = (UCHAR)((Color & rgbColor[2-l]) << abs(rgbShift[2-l]));
								else
									Col = (UCHAR)((Color & rgbColor[2-l]) >> rgbShift[2-l]);
								switch(l){
								case 0: BluePlane[Lplane] = Col; break;
								case 1: GreenPlane[Lplane] = Col; break;
								case 2: RedPlane[Lplane] = Col; break;
								}
							}
						}
					}
					Result = TRUE;
					break;
				case 24:
					iOffset += aOffset;
					for(j = (int)bmiHeader.biHeight - 1, k = 0; j >= 0; j--){
						int Lbmp = iOffset + LineLenBitmap * j;
						for(i = 0; i < (int)bmiHeader.biWidth; i++){
							BluePlane[k] = pPicture[Lbmp++];
							GreenPlane[k] = pPicture[Lbmp++];
							RedPlane[k++] = pPicture[Lbmp++];
						}
					}
					Result = TRUE;
					break;
				}
			}
		}
	}

	if(Result == FALSE){
		if(RedPlane != NULL) free(RedPlane);
		if(GreenPlane != NULL) free(GreenPlane);
		if(BluePlane != NULL) free(BluePlane);
	}
	else{
		R = RedPlane; G = GreenPlane; B = BluePlane;
		Width = WidthPlane; Height = HeightPlane;
	}

	return Result;
}

BOOL CColorPlanes::CreatePictureFromColorPlanes(IWSPicture * lpdispResult,
									PUCHAR R, PUCHAR G, PUCHAR B, int Width, int Height)
{
	if(lpdispResult == NULL || R == NULL || G == NULL || B == NULL || Width <= 0 || Height <= 0) return FALSE;

	BITMAPINFOHEADER bmiHeader;
	memset(&bmiHeader, 0, sizeof(BITMAPINFOHEADER));
	bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmiHeader.biWidth = Width;
	bmiHeader.biHeight = Height;
	bmiHeader.biBitCount = 32;
	bmiHeader.biPlanes = 1;
	bmiHeader.biCompression = BI_RGB;

	int LineLen = (unsigned short)WIDTHBYTES(bmiHeader.biWidth * bmiHeader.biBitCount);
	PUCHAR pPicture = (PUCHAR)malloc(sizeof(BITMAPINFOHEADER) + LineLen * Height);
	if(pPicture != NULL){
		*((LPBITMAPINFOHEADER)pPicture) = bmiHeader;
		for(int l = 0, j = Height - 1; l < Height; l++, j--){
			int iTarget = j * LineLen + sizeof(BITMAPINFOHEADER);
			int iSource = l * Width;
			for(int i = 0; i < Width; i++){
				pPicture[iTarget++] = B[iSource];
				pPicture[iTarget++] = G[iSource];
				pPicture[iTarget++] = R[iSource++];
				pPicture[iTarget++] = 0;
			}
		}

		lpdispResult->SetPicture(pPicture);
		free(pPicture);
	}

	return TRUE;
}

RGBQUAD CColorPlanes::GetPixel(IWSPicture * lpdispPicture, int X, int Y)
{
	RGBQUAD Color = {0, 0, 0, 0};

	if(lpdispPicture && X >= 0 && Y >= 0){
		PUCHAR R = NULL, G = NULL, B = NULL;
		int Width, Height;
		
		if(GetColorPlanes(lpdispPicture, R, G, B, Width, Height)){
			if(X < Width && Y < Height){
				CMassiv Rplane, Gplane, Bplane;
				Rplane.CreateMassiv(Width, Height, R);
				Gplane.CreateMassiv(Width, Height, G);
				Bplane.CreateMassiv(Width, Height, B);

				Color.rgbRed = Rplane[Y][X];
				Color.rgbGreen = Gplane[Y][X];
				Color.rgbBlue = Bplane[Y][X];
			}
		}

		if(R)
			free(R);
		if(G)
			free(G);
		if(B)
			free(B);
	}

	return Color;
}
