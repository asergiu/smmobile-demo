// MultifocalSegmentCollection.cpp : implementation file
//

#include "stdafx.h"
#include "MultifocalSegmentCollection.h"
#include "MultifocalSegment.h"
#include "StringConvert.h"

/////////////////////////////////////////////////////////////////////////////
// CMultifocalSegmentCollection

CMultifocalSegmentCollection* CMultifocalSegmentCollection::CreateObject()
{
	CMultifocalSegmentCollection *pObject = new CMultifocalSegmentCollection;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

IMultifocalSegmentCollection * CMultifocalSegmentCollection::GetInterface()
{
	return static_cast<IMultifocalSegmentCollection*>(this);
}

CMultifocalSegmentCollection::CMultifocalSegmentCollection()
{
	IsFinalClear = false;
}

CMultifocalSegmentCollection::~CMultifocalSegmentCollection()
{
}

unsigned long CMultifocalSegmentCollection::GetCount() 
{
	return pt.size();
}

IMultifocalSegment * CMultifocalSegmentCollection::GetItem(unsigned long Index)
{
	VMultifocalSegment::size_type nCount = pt.size();
	if(!TestRange(Index, 1, nCount, 1))
		return NULL;
	if(pt[Index-1])
		pt[Index-1]->AddRef();
	
	return pt[Index-1];
}

bool CMultifocalSegmentCollection::SetItem(unsigned long Index, IMultifocalSegment * newValue) 
{
	VMultifocalSegment::size_type nCount = pt.size();
	if(!TestRange(Index, 1, nCount, 1))
		return false;
	
	if(pt[Index-1])
	{
		UnadviseConnection(Index-1);
	}
	
	pt[Index-1] = newValue;
	
	if(pt[Index-1])
	{
		AdviseConnection(pt[Index-1], Index-1);
	}

	FireChange();
	SetModifiedFlag();
	return true;
}

bool CMultifocalSegmentCollection::Clear() 
{
	VMultifocalSegment::size_type i, nCount = pt.size();
	for(i = 0; i < nCount; i++){
		UnadviseConnection(/*pt[i],*/ i);
	}

	pt.clear();

	if(!IsFinalClear){
		FireChange();
		SetModifiedFlag();
	}
	return true;
}

bool CMultifocalSegmentCollection::Add(IMultifocalSegment * newMultifocal) 
{
	if(newMultifocal) {
		VMultifocalSegment::size_type nCount = pt.size();
		if(nCount >= Max_MultifocalSegment_Count)
			return false;

		pt.push_back(newMultifocal);

		if(newMultifocal)
		{
			AdviseConnection(pt[nCount], nCount);
		}

		FireChange();
		SetModifiedFlag();
		return true;
	}
	return false;
}

IMultifocalSegmentCollection * CMultifocalSegmentCollection::Clone() 
{
	CMultifocalSegmentCollection *pNewObject = CMultifocalSegmentCollection::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}

bool CMultifocalSegmentCollection::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	string PropName;
	unsigned long i, nLoadCount;

	if(pPX->IsLoading()){
		Clear();
		if(pPX->PX_Long("Count", nLoadCount, 0) == false)
			return false;
		for(i = 0; i < nLoadCount; i++) {
			PropName = "MultifocalSegment" + toString(i);
			CMultifocalSegment * ptObject = CMultifocalSegment::CreateObject();
			if(ptObject) {
				CObjPtr<IMultifocalSegment> ptTemp;
				ptTemp.AttachDispatch(ptObject->GetInterface());
				if(PX_WSLObject<CMultifocalSegment, IMultifocalSegment>(pPX, PropName.c_str(), ptTemp, NULL) == false)
					return false;
				if(ptTemp)
					Add(ptTemp);
			}
			else
				return false;
		}
	}
	else {
		nLoadCount = GetCount();
		if(pPX->PX_Long("Count", nLoadCount) == false)
			return false;
		for(i = 0; i < nLoadCount; i++)
		{
			PropName = "MultifocalSegment" + toString(i);
			if(PX_WSLObject<CMultifocalSegment, IMultifocalSegment>(pPX, PropName.c_str(), pt[i], NULL) == false)
				return false;
		}
	}

	return true;
}

void CMultifocalSegmentCollection::OnChildChanged() 
{
	FireChange();
	SetModifiedFlag();
}
