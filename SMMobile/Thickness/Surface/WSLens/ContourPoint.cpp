// ContourPoint.cpp : implementation file
//

#include "stdafx.h"
#include "ContourPoint.h"

/////////////////////////////////////////////////////////////////////////////
// CContourPoint

CContourPoint* CContourPoint::CreateObject()
{
	CContourPoint *pObject = new CContourPoint;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

IContourPoint * CContourPoint::GetInterface()
{
	return static_cast<IContourPoint*>(this);
}

CContourPoint::CContourPoint()
{
	m_x = 0;
	m_y = 0;
}

CContourPoint::~CContourPoint()
{
}

bool CContourPoint::SetX(double Xvalue)
{
	m_x = Xvalue;
	FireChange();
	SetModifiedFlag();
	return true;
}

bool CContourPoint::SetY(double Yvalue)
{
	m_y = Yvalue;
	FireChange();
	SetModifiedFlag();
	return true;
}

double CContourPoint::GetX()
{
	return m_x;
}

double CContourPoint::GetY()
{
	return m_y;
}

bool CContourPoint::SetPoint(double Xvalue, double Yvalue)
{
	m_x = Xvalue;
	m_y = Yvalue;
	FireChange();
	SetModifiedFlag();
	return true;
}

bool CContourPoint::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	if(pPX->PX_Double("X", m_x, 0.) == false)
		return false;
	if(pPX->PX_Double("Y", m_y, 0.) == false)
		return false;

	return true;
}

IContourPoint * CContourPoint::Clone() 
{
	CContourPoint *pNewObject = CContourPoint::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}
