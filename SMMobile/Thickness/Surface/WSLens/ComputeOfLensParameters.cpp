#include "stdafx.h"
#include "math.h"
#include "Parameters/Asph2.h"

//struct lens_inf * lns;

bool DecreasePointsNumber(	int InSize, double InX[], double InY[],
							int OutSize, double OutX[], double OutY[])
{
	double CurT = 0, *TValues = (double *)calloc(InSize+1, sizeof(double));
	if(TValues == NULL) return FALSE;
	for(int CurNum = 0; CurNum < InSize; CurNum++){
		TValues[CurNum] = CurT;
		int NextNum = CurNum + 1;
		if(NextNum >= InSize){
			NextNum = 0;
		}
		CurT += _hypot(InX[NextNum]-InX[CurNum], InY[NextNum]-InY[CurNum]);
	}
	TValues[InSize]=CurT;

	int CurInd = 0, NextInd = 1;
	double StepT = CurT / OutSize;
	CurT = 1e-3;
  	for(int i = 0; i < OutSize; i++, CurT += StepT){
		while(CurT > TValues[NextInd]){
			NextInd++;
			CurInd++;
			if(CurInd >= InSize)
			{
				free(TValues);
				return FALSE;
			}
		}
		int CurIndAux = CurInd, NextIndAux = NextInd;
		if(CurIndAux >= InSize){
			CurIndAux -= InSize;
		}
		if(NextIndAux >= InSize){
			NextIndAux -= InSize;
		}
		OutX[i] = InX[CurIndAux] + (InX[NextIndAux]-InX[CurIndAux])*
						(CurT-TValues[CurInd]) / (TValues[NextInd]-TValues[CurInd]);
		OutY[i] = InY[CurIndAux] + (InY[NextIndAux]-InY[CurIndAux])*
						(CurT-TValues[CurInd]) / (TValues[NextInd]-TValues[CurInd]);
	}
	free(TValues);
	return TRUE;
}

