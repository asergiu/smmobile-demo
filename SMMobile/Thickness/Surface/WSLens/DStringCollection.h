#pragma once
// DStringCollection.h : header file
//

#include "ObjectBase.h"
#include <vector>
using std::vector;

const unsigned int Max_DString_Count = 100;

/////////////////////////////////////////////////////////////////////////////
// CDStringCollection

class CDStringCollection :
	public CObjectEventBase<IDStringCollection, IDStringCollectionEvents>,
	public CObjEventSimpleImpl<IDString, IDStringEvents>
{
public:
    static CDStringCollection* CreateObject();
	IDStringCollection * GetInterface();

protected:
	CDStringCollection();           // protected constructor used by dynamic creation

// Attributes
	typedef vector< CObjPtr<IDString> > VDString;
	VDString pt;

public:

// Operations
public:
	unsigned long GetCount();
	IDString * GetItem(unsigned long Index);
	bool SetItem(unsigned long Index, IDString * newValue);
	bool Clear();
	IDStringCollection * Clone();
	bool Add(IDString * newDString);

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);
	bool IsFinalClear;

// Implementation
protected:
	virtual ~CDStringCollection();

	virtual void FinalRelease()
	{
		IsFinalClear = true;
		Clear();
		delete this;
	}

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->DStringCollectionChanged();
			}
		}
	}

protected:
	void DStringChanged()
	{
		OnChildChanged();
	}
	void OnChildChanged();
};
