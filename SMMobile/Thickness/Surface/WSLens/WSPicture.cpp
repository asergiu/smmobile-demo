// WSPicture.cpp : implementation file
//

#include "stdafx.h"
#include "WSPicture.h"
#include <stdio.h>

/////////////////////////////////////////////////////////////////////////////
// CWSPicture

CWSPicture* CWSPicture::CreateObject()
{
	CWSPicture *pObject = new CWSPicture;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

IWSPicture * CWSPicture::GetInterface()
{
	return static_cast<IWSPicture*>(this);
}

CWSPicture::CWSPicture()
{
	m_Status = 0;
	m_Picture = NULL;
}

CWSPicture::~CWSPicture()
{
	if(m_Picture != NULL) {
		delete [] m_Picture;
		m_Picture = NULL;
	}
}

const unsigned char * CWSPicture::GetPicture()
{
	if(m_Status == 0) return NULL;
	return m_Picture;
}

bool CWSPicture::SetPicture(const unsigned char * nNewValue) 
{
    int Code = 0;
    if((Code = CheckPictureFormat(nNewValue)) > 0){
		return false;
	}

	if(m_Picture != NULL){delete [] m_Picture; m_Picture = NULL;}
	m_Status = 0;

	m_Picture = CreateNewBitmap24(nNewValue);
	if(m_Picture != NULL){
		m_Status = wsPicture;
	}

	FireChange();
	SetModifiedFlag();
	return true;
}

int CWSPicture::CheckPictureFormat(const unsigned char * Block)
{
	int Code = 0;

	if(Block == NULL) return Code;

	const unsigned char * data = Block;
	if(data == NULL){
		Code = 23;
		return Code;
	}

	BITMAPINFOHEADER bmiHeader = *(BITMAPINFOHEADER *)(data);

	if(bmiHeader.biCompression == BI_BITFIELDS && (bmiHeader.biBitCount == 16 || bmiHeader.biBitCount == 32))
		Code = 0;
	else if(bmiHeader.biCompression != BI_RGB){
		Code = 24;
		return Code;
	}

	switch(bmiHeader.biBitCount){
	default:
		Code = 24;
		break;
	case  8:
	case 32:
	case 16:
	case 24:
		Code = 0;
		break;
	}

	return Code;
}

unsigned char * CWSPicture::MakePicture()
{
	unsigned char * hResult = NULL;

	return hResult;
}

bool CWSPicture::MakePictureObject()
{
	if(m_Picture == NULL) return false;

	bool bRes = ConvertPictureToBitmap24(&m_Picture);

	return bRes;
}

unsigned char * CWSPicture::CopyFromDibToGSF (unsigned char * lp, int iOffset, int& ErrorCode)
{
struct GSF_TITLE gsf;
DWORD dwLen;
unsigned char * lpCopy;
unsigned short LineLenBitmap, LineLenSvf;
unsigned int * bmiColors;
BOOL fbmiColors = FALSE;
int aOffset = 0;

	ErrorCode = 1;
	if (lp == NULL) return NULL;
	BITMAPINFOHEADER bmiHeader = *(BITMAPINFOHEADER *)lp;

	ErrorCode = 2;
	if(bmiHeader.biCompression == BI_BITFIELDS && (bmiHeader.biBitCount == 16 || bmiHeader.biBitCount == 32)){
		fbmiColors = TRUE;
		iOffset = bmiHeader.biSize + 3 * sizeof(DWORD);
	}
	else if(bmiHeader.biCompression != BI_RGB)
	{
		ErrorCode = 3;
		return NULL;
	}

	if(bmiHeader.biBitCount == 8 && iOffset == 0){
		iOffset = bmiHeader.biSize + 256 * sizeof(DWORD);
	}
	if(bmiHeader.biClrUsed > 0){
		aOffset = bmiHeader.biClrUsed * sizeof(DWORD);
	}

	bmiColors = (unsigned int *)(lp + bmiHeader.biSize);

	LineLenBitmap = (unsigned short)WIDTHBYTES(bmiHeader.biWidth * bmiHeader.biBitCount);
	LineLenSvf = (unsigned short)WIDTHBYTES(bmiHeader.biWidth * 24);
	InitTitleGSF(&gsf, (short)bmiHeader.biWidth, (short)bmiHeader.biHeight);
	gsf.LineLen	= LineLenSvf;

	dwLen = (long)sizeof(struct GSF_TITLE) + (long)LineLenSvf * (long)bmiHeader.biHeight;
	lpCopy = new unsigned char[dwLen];

	if(iOffset == 0) iOffset = bmiHeader.biSize;
	
	if(lpCopy != NULL){
    	memcpy(lpCopy, &gsf, sizeof(struct GSF_TITLE));
		switch(bmiHeader.biBitCount){
			default:
				delete [] lpCopy;
				lpCopy = NULL;
				ErrorCode = 3;
				return NULL;
			case  8: {
					if(aOffset > 0)
						iOffset = bmiHeader.biSize + aOffset;
					for(int j = 0; j < bmiHeader.biHeight; j++){
						for(int i = 0; i < bmiHeader.biWidth; i++){
							int Lsvf = sizeof(struct GSF_TITLE) + (int)LineLenSvf * j + i * 3;
							int Lbmp = iOffset + (int)LineLenBitmap * j + i * (bmiHeader.biBitCount/8);
							unsigned int Color = 0;
							memcpy(&Color, lp+Lbmp, (bmiHeader.biBitCount/8));
							lpCopy[Lsvf+0] = ((RGBQUAD *)bmiColors)[Color].rgbBlue;
							lpCopy[Lsvf+1] = ((RGBQUAD *)bmiColors)[Color].rgbGreen;
							lpCopy[Lsvf+2] = ((RGBQUAD *)bmiColors)[Color].rgbRed;
						}
					}
				}
				ErrorCode = 0;
				break;
			case 32:
			case 16:
				{
					int j, rgbShift[3], rgbColor[3];
					if(iOffset - bmiHeader.biSize >= 12){
						unsigned int i, Maska;
						for(j = 0; j < 3; j++){
							Maska = (unsigned int)bmiColors[j];
							for(i = 0; i < (unsigned int)bmiHeader.biBitCount/8; i++){
								int d = Maska % 2;
								if( d > 0) break;
							}
							rgbShift[j]	= i;
							rgbColor[j] = bmiColors[j];
							Maska = ((unsigned int)rgbColor[j] >> (unsigned int)rgbShift[j]) + 1;
							for(i = 0; i < (unsigned int)bmiHeader.biBitCount/8; i++){
								int d = Maska % 2;
								if( d > 0) break;
							}
							rgbShift[j]	-= 8 - i;
						}
					}
					else{
						switch(bmiHeader.biBitCount){
							case 32:
								rgbColor[0] = 0xFF0000; rgbColor[1] = 0xFF00; rgbColor[2] = 0xFF;
								rgbShift[0] = 16;	    rgbShift[1] = 8;	  rgbShift[2] = 0;
								break;
							case 16:
								rgbColor[0] = 0x7C00; rgbColor[1] = 0x3E0; rgbColor[2] = 0x1F;
								rgbShift[0] = 7;	  rgbShift[1] = 2;	   rgbShift[2] = -3;
								break;
						}
					}
					iOffset += aOffset;
					for(j = 0; j < bmiHeader.biHeight; j++){
						for(int i = 0; i < bmiHeader.biWidth; i++){
							int Lsvf = sizeof(struct GSF_TITLE) + (int)LineLenSvf * j + i * 3;
							int Lbmp = iOffset + (int)LineLenBitmap * j + i * (bmiHeader.biBitCount/8);
							unsigned int Color = 0;
							memcpy(&Color, lp+Lbmp, (bmiHeader.biBitCount/8));
							for(int l = 0; l < 3; l++){
								if(rgbShift[2-l] < 0)
									lpCopy[Lsvf+l] = (unsigned char)((Color & rgbColor[2-l]) << abs(rgbShift[2-l]));
								else
									lpCopy[Lsvf+l] = (unsigned char)((Color & rgbColor[2-l]) >> rgbShift[2-l]);
							}
						}
					}
				}
				ErrorCode = 0;
				break;
			case 24:
				iOffset += aOffset;
				memcpy(lpCopy+sizeof(struct GSF_TITLE), lp+iOffset, (int)LineLenSvf	* (int)bmiHeader.biHeight);
				ErrorCode = 0;
				break;
		}
	}

	return lpCopy;
}

void CWSPicture::InitTitleGSF (struct GSF_TITLE * gsf, short dxField, short dyField)
{
	memset(gsf, 0, sizeof(struct GSF_TITLE));
	memcpy(gsf->Maska, GsfMaska, 5 );
	gsf->Number = 55;
	gsf->Width = dxField;
	gsf->Height = dyField;
	gsf->GroupBytes = 0;
	gsf->YUVmode = (char)YUVmode;
	gsf->LineLen = (unsigned short)WIDTHBYTES(gsf->Width * 24);
	gsf->System = -1;
	gsf->OffsetYUV = sizeof(struct GSF_TITLE);
}

bool CWSPicture::ConvertPictureToBitmap24(unsigned char ** ppPicture)
{
	bool bRes = false;
	if(ppPicture && *ppPicture) {
		unsigned char * pPicture;
		BITMAPINFOHEADER bmiHeader;

		pPicture = *ppPicture;
		if(pPicture != NULL){
			bmiHeader = *((LPBITMAPINFOHEADER)pPicture);
			if(bmiHeader.biBitCount == 24 && bmiHeader.biClrUsed == 0) return true;
		}
		else return false;

		unsigned char * pOutPicture = CreateNewBitmap24(pPicture);
		if(pOutPicture) {
			delete [] pPicture;
			*ppPicture = pOutPicture;
			pOutPicture = NULL;
		}
	}
	return bRes;
}

unsigned char * CWSPicture::CreateNewBitmap24(const unsigned char * pInPicture)
{
	if(pInPicture == NULL) return NULL;

	unsigned char * pResultPicture = NULL;
	const unsigned char * pPicture = NULL;
	BITMAPINFOHEADER bmiHeader;

	pPicture = pInPicture;
	if(pPicture != NULL){
		bmiHeader = *((LPBITMAPINFOHEADER)pPicture);
	}
	else return NULL;

	if(CheckPictureFormat(pPicture) != 0) return NULL;

	BOOL hPicKeep24 = FALSE;

	BITMAPINFOHEADER bmiHeaderTag;
	memset(&bmiHeaderTag, 0, sizeof(BITMAPINFOHEADER));
	bmiHeaderTag.biSize = sizeof(BITMAPINFOHEADER);
	bmiHeaderTag.biWidth = bmiHeader.biWidth;
	bmiHeaderTag.biHeight = bmiHeader.biHeight;
	bmiHeaderTag.biBitCount = 24;
	bmiHeaderTag.biPlanes = 1;
	bmiHeaderTag.biCompression = BI_RGB;

	long nSizeTag = sizeof(BITMAPINFOHEADER) + /*WIDTHBYTES(*/bmiHeaderTag.biWidth * bmiHeaderTag.biBitCount / 8/*)*/ * bmiHeaderTag.biHeight;
	unsigned char * pPic = new unsigned char[nSizeTag];
	if(pPic != NULL){
		*((LPBITMAPINFOHEADER)pPic) = bmiHeaderTag;
		if(pPicture != NULL){
			bmiHeader = *(BITMAPINFOHEADER *)pPicture;
			int iOffset = 0, aOffset = 0;
			BOOL Format = TRUE;
			unsigned int * bmiColors;
			int LineLenBitmap, LineLen24;
			unsigned int i, j;

			if(bmiHeader.biCompression == BI_BITFIELDS && (bmiHeader.biBitCount == 16 || bmiHeader.biBitCount == 32))
				iOffset = bmiHeader.biSize + 3 * sizeof(DWORD);
			else if(bmiHeader.biCompression != BI_RGB)
				Format = FALSE;
			if(bmiHeader.biBitCount == 8 && iOffset == 0)
				iOffset = bmiHeader.biSize + 256 * sizeof(DWORD);
			if(bmiHeader.biClrUsed > 0)
				aOffset = bmiHeader.biClrUsed * sizeof(DWORD);

			bmiColors = (unsigned int *)(pPicture + bmiHeader.biSize);

			LineLenBitmap = (unsigned short)WIDTHBYTES(bmiHeader.biWidth * bmiHeader.biBitCount);
			LineLen24 = (unsigned short)WIDTHBYTES(bmiHeaderTag.biWidth * bmiHeaderTag.biBitCount);

			if(iOffset == 0) iOffset = bmiHeader.biSize;

			if(Format){
				switch(bmiHeader.biBitCount){
				default:
					break;
				case  8:
					if(aOffset > 0)
						iOffset = bmiHeader.biSize + aOffset;
					for(j = 0; j < (unsigned int)bmiHeader.biHeight; j++){
						for(i = 0; i < (unsigned int)bmiHeader.biWidth; i++){
							int L24 = bmiHeaderTag.biSize + LineLen24 * j + i * 3;
							int Lbmp = iOffset + LineLenBitmap * j + i * (bmiHeader.biBitCount/8);
							unsigned int Color = 0;
							memcpy(&Color, pPicture+Lbmp, (bmiHeader.biBitCount/8));
							pPic[L24+0] = ((RGBQUAD *)bmiColors)[Color].rgbBlue;
							pPic[L24+1] = ((RGBQUAD *)bmiColors)[Color].rgbGreen;
							pPic[L24+2] = ((RGBQUAD *)bmiColors)[Color].rgbRed;
						}
					}
					hPicKeep24 = TRUE;
					break;
				case 32:
				case 16:
					int rgbShift[3], rgbColor[3];
					if(iOffset - bmiHeader.biSize >= 12){
						unsigned int Maska;
						for(j = 0; j < 3; j++){
							Maska = (unsigned int)bmiColors[j];
							for(i = 0; i < (unsigned int)bmiHeader.biBitCount/8; i++){
								int d = Maska % 2;
								if( d > 0) break;
							}
							rgbShift[j]	= i;
							rgbColor[j] = bmiColors[j];
							Maska = ((unsigned int)rgbColor[j] >> (unsigned int)rgbShift[j]) + 1;
							for(i = 0; i < (unsigned int)bmiHeader.biBitCount/8; i++){
								int d = Maska % 2;
								if( d > 0) break;
							}
							rgbShift[j]	-= 8 - i;
						}
					}
					else{
						switch(bmiHeader.biBitCount){
				case 32:
					rgbColor[0] = 0xFF0000; rgbColor[1] = 0xFF00; rgbColor[2] = 0xFF;
					rgbShift[0] = 16;	    rgbShift[1] = 8;	  rgbShift[2] = 0;
					break;
				case 16:
					rgbColor[0] = 0x7C00; rgbColor[1] = 0x3E0; rgbColor[2] = 0x1F;
					rgbShift[0] = 7;	  rgbShift[1] = 2;	   rgbShift[2] = -3;
					break;
						}
					}
					iOffset += aOffset;
					for(j = 0; j < (unsigned int)bmiHeader.biHeight; j++){
						for(i = 0; i < (unsigned int)bmiHeader.biWidth; i++){
							int L24 = bmiHeaderTag.biSize + LineLen24 * j + i * 3;
							int Lbmp = iOffset + LineLenBitmap * j + i * (bmiHeader.biBitCount/8);
							unsigned int Color = 0;
							memcpy(&Color, pPicture+Lbmp, (bmiHeader.biBitCount/8));
							for(int l = 0; l < 3; l++){
								if(rgbShift[2-l] < 0)
									pPic[L24+l] = (UCHAR)((Color & rgbColor[2-l]) << abs(rgbShift[2-l]));
								else
									pPic[L24+l] = (UCHAR)((Color & rgbColor[2-l]) >> rgbShift[2-l]);
							}
						}
					}
					hPicKeep24 = TRUE;
					break;
				case 24:
					iOffset += aOffset;
					memcpy(pPic+bmiHeaderTag.biSize, pPicture+iOffset, LineLen24*bmiHeader.biHeight);
					hPicKeep24 = TRUE;
					break;
				}
			}
		}
	}

	if(hPicKeep24 == TRUE) {
		pResultPicture = pPic;
		pPic = NULL;
	}
	else {
		if (pPic)
			delete [] pPic;
		pPic = NULL;
	}

	return pResultPicture;
}

unsigned char *	CWSPicture::GetPicturePart(unsigned char ** ppPicture, unsigned long x, unsigned long y,
										   unsigned long cx, unsigned long cy)
{
	if(ppPicture == NULL || *ppPicture == NULL) return NULL;

	if(ConvertPictureToBitmap24(ppPicture) == FALSE)
		return NULL;

	unsigned char *	pResultPicture = NULL;
	unsigned char *	pPic = *ppPicture;
	if(pPic != NULL){
		BITMAPINFOHEADER bmiHeader = *((LPBITMAPINFOHEADER)pPic);

		if(bmiHeader.biBitCount == 24){
			//	��������� ����� �� ��� ������������� ������ ��������
			if(long(x + cx) <= bmiHeader.biWidth && long(y + cy) <= bmiHeader.biHeight) {
				//	�����
				BITMAPINFOHEADER bmiHeaderTag;
				memset(&bmiHeaderTag, 0, sizeof(BITMAPINFOHEADER));
				bmiHeaderTag.biSize = sizeof(BITMAPINFOHEADER);
				bmiHeaderTag.biWidth = cx;
				bmiHeaderTag.biHeight = cy;
				bmiHeaderTag.biBitCount = 24;
				bmiHeaderTag.biPlanes = 1;
				bmiHeaderTag.biCompression = BI_RGB;

				long nSizeTag = sizeof(BITMAPINFOHEADER) + WIDTHBYTES(bmiHeaderTag.biWidth * bmiHeaderTag.biBitCount) * bmiHeaderTag.biHeight;
				unsigned char * pNew = new unsigned char[nSizeTag];
				if(pNew != NULL){
					*((LPBITMAPINFOHEADER)pNew) = bmiHeaderTag;
					for(unsigned long ySrc = y, yTag = 0; yTag < cy; ySrc++, yTag++){
						int offsetTag = sizeof(BITMAPINFOHEADER) + (bmiHeaderTag.biHeight - yTag - 1) * WIDTHBYTES(bmiHeaderTag.biWidth * bmiHeaderTag.biBitCount);
						int offsetSrc = sizeof(BITMAPINFOHEADER) + (bmiHeader.biHeight - ySrc - 1) * WIDTHBYTES(bmiHeader.biWidth * bmiHeader.biBitCount) + x * bmiHeader.biBitCount / 8;
						memcpy(pNew + offsetTag, pPic + offsetSrc, WIDTHBYTES(bmiHeaderTag.biWidth * bmiHeaderTag.biBitCount));
					}
				}
				pResultPicture = pNew;
				pNew = NULL;
			}
		}
	}

	return pResultPicture;
}

long CWSPicture::GetWidth() 
{
	if((m_Status & wsPicture) > 0 && m_Picture != NULL){
		unsigned char * pPicture = m_Picture;
		if(pPicture != NULL){
			BITMAPINFOHEADER bmiHeader = *((LPBITMAPINFOHEADER)pPicture);
			return bmiHeader.biWidth;
		}
	}
	return 0;
}

long CWSPicture::GetHeight() 
{
	if((m_Status & wsPicture) > 0 && m_Picture != NULL){
		unsigned char * pPicture = m_Picture;
		if(pPicture != NULL){
			BITMAPINFOHEADER bmiHeader = *((LPBITMAPINFOHEADER)pPicture);
			return bmiHeader.biHeight;
		}
	}
	return 0;
}

bool CWSPicture::PictureSize(long Width, long Height) 
{
	// TODO: Add your dispatch handler code here
	long m_Width = GetWidth();
	long m_Height = GetHeight();

	if(m_Width == Width && m_Height == Height)
		return true;

	if(m_Width < 2 && m_Height < 2)
		return false;

	if((m_Status & wsPicture) > 0){
		unsigned char *	pNewPicture = RescalePicture(Width, Height);
		if(pNewPicture != NULL){
			if(m_Picture != NULL){delete [] m_Picture; m_Picture = NULL;}
			m_Status = 0;

			m_Picture = pNewPicture;
			if(m_Picture != NULL) m_Status = wsPicture;

			FireChange();
			SetModifiedFlag();
			return true;
		}
	}
	return false;
}

unsigned char *	CWSPicture::RescalePicture(long cx, long cy)
{
	unsigned char *	pResultPicture = NULL;
	
	if(m_Picture == NULL) return NULL;

	if(ConvertPictureToBitmap24(&m_Picture) == FALSE)
		return NULL;

	unsigned char * pPic = m_Picture;
	if(pPic != NULL){
		BITMAPINFOHEADER bmiHeader = *((LPBITMAPINFOHEADER)pPic);

		if(bmiHeader.biBitCount == 24) {
			long nSize = sizeof(BITMAPINFOHEADER) + cy * WIDTHBYTES(cx * 24);
			unsigned char * p = new unsigned char[nSize];
			if(p != NULL){
				ScaleBuffer(pPic + bmiHeader.biSize, bmiHeader.biWidth, bmiHeader.biHeight, 24, 
								p + bmiHeader.biSize, cx, cy, 24);

				memset(&bmiHeader, 0, sizeof(BITMAPINFOHEADER));
				bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
				bmiHeader.biWidth = cx;
				bmiHeader.biHeight = cy;
				bmiHeader.biBitCount = 24;
				bmiHeader.biPlanes = 1;
				bmiHeader.biCompression = BI_RGB;

				*((LPBITMAPINFOHEADER)p) = bmiHeader;

				pResultPicture = p;
			}
		}
	}

	return pResultPicture;
}

// returns:
// 0 - No error
// 1 - File not found or error opening file
// 2 - Insufficient memory
// 3 - Wrong file format
// 4 - Wrong extension
// 5 - JPEG library initialization error
long CWSPicture::LoadFromFile(LPCTSTR FileName) 
{
	if(FileName == NULL)
		return 1;
	int Quit = 4;

	if(m_Picture != NULL){delete [] m_Picture; m_Picture = NULL;}
	m_Status = 0;

	// Get file extension
	string Ext, Name(FileName);
	string::size_type idx = Name.rfind('.');
	if(idx != string::npos){
		Ext = Name.substr(idx + 1);
		if(stricmp(Ext.c_str(), "BMP") == 0)
			Quit = LoadFromBMP(FileName);

		else if(stricmp(Ext.c_str(), "JPEG") == 0 || stricmp(Ext.c_str(), "JPG") == 0)
			Quit = LoadFromJPEG(FileName);
	}

	FireChange();
	SetModifiedFlag();

	return Quit;
}

long CWSPicture::LoadFromBMP(LPCTSTR FileName) 
{
	int	  nFileSize;
	unsigned char * gsfPicture = NULL;
	int Quit = 0;
	unsigned char * pLoadPicture = NULL;

	pLoadPicture = DIBReadFile(FileName, &nFileSize);
	if(pLoadPicture != NULL){
		if(DIBType(pLoadPicture) == WINRGB_DIB){
            LPBYTE data = pLoadPicture;
			if(data != NULL){
				int ErrorCode;

				gsfPicture = CopyFromDibToGSF(data+sizeof(BITMAPFILEHEADER),((BITMAPFILEHEADER *)data)->bfOffBits - sizeof(BITMAPFILEHEADER), ErrorCode);
				if(gsfPicture != NULL){
					struct GSF_TITLE gsf = *((struct GSF_TITLE *)gsfPicture);

					BITMAPINFOHEADER bmiHeader;
					memset(&bmiHeader, 0, sizeof(BITMAPINFOHEADER));
					bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
					bmiHeader.biWidth = gsf.Width;
					bmiHeader.biHeight = gsf.Height;
					bmiHeader.biBitCount = 24;
					bmiHeader.biPlanes = 1;
					bmiHeader.biCompression = BI_RGB;

					long nSize = sizeof(BITMAPINFOHEADER) + gsf.LineLen * gsf.Height;
					unsigned char * pPic = new unsigned char[nSize];
					if(pPic != NULL){
						*((LPBITMAPINFOHEADER)pPic) = bmiHeader;
						memcpy(pPic+sizeof(BITMAPINFOHEADER), gsfPicture+gsf.OffsetYUV, gsf.LineLen * gsf.Height);
						if(m_Picture != NULL){delete [] m_Picture; m_Picture = NULL;}
						m_Picture = pPic;
						if(m_Picture != NULL){
							m_Status = wsPicture;
						}
					}
					else Quit = 2;

					delete [] gsfPicture;
				}
				else Quit = ErrorCode;
			}
			else Quit = 2;
		}
		else Quit = 3;
	}
	else Quit = 1;

	if(pLoadPicture) {
		delete [] pLoadPicture;
		pLoadPicture = NULL;
	}

	return Quit;
}

long CWSPicture::LoadBMPFromMemory(const unsigned char * pFileData)
{
    unsigned char * gsfPicture = NULL;
    int Quit = 0;
    unsigned char * pLoadPicture = (unsigned char *)pFileData;
    
    if(pLoadPicture != NULL){
        if(DIBType(pLoadPicture) == WINRGB_DIB){
            LPBYTE data = pLoadPicture;
            if(data != NULL){
                int ErrorCode;
                
                gsfPicture = CopyFromDibToGSF(data+sizeof(BITMAPFILEHEADER),((BITMAPFILEHEADER *)data)->bfOffBits - sizeof(BITMAPFILEHEADER), ErrorCode);
                if(gsfPicture != NULL){
                    struct GSF_TITLE gsf = *((struct GSF_TITLE *)gsfPicture);
                    
                    BITMAPINFOHEADER bmiHeader;
                    memset(&bmiHeader, 0, sizeof(BITMAPINFOHEADER));
                    bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
                    bmiHeader.biWidth = gsf.Width;
                    bmiHeader.biHeight = gsf.Height;
                    bmiHeader.biBitCount = 24;
                    bmiHeader.biPlanes = 1;
                    bmiHeader.biCompression = BI_RGB;
                    
                    long nSize = sizeof(BITMAPINFOHEADER) + gsf.LineLen * gsf.Height;
                    unsigned char * pPic = new unsigned char[nSize];
                    if(pPic != NULL){
                        *((LPBITMAPINFOHEADER)pPic) = bmiHeader;
                        memcpy(pPic+sizeof(BITMAPINFOHEADER), gsfPicture+gsf.OffsetYUV, gsf.LineLen * gsf.Height);
                        if(m_Picture != NULL){delete [] m_Picture; m_Picture = NULL;}
                        m_Picture = pPic;
                        if(m_Picture != NULL){
                            m_Status = wsPicture;
                        }
                    }
                    else Quit = 2;
                    
                    delete [] gsfPicture;
                }
                else Quit = ErrorCode;
            }
            else Quit = 2;
        }
        else Quit = 3;
    }
    else Quit = 1;
    
    return Quit;
}

long CWSPicture::LoadFromJPEG(LPCTSTR FileName)
{
	int Quit = 0;

    return Quit;
}

// 0 - No error
// 1 - Error opening file
// 2 - Insufficient memory
// 3 - No picture to save
// 4 - Wrong extension
// 5 - JPEG library initialization error
long CWSPicture::SaveToFile(LPCTSTR FileName) 
{
	remove(FileName);

	if(m_Status == 0) return 3;
	if(m_Picture == NULL) return 3;

    if(ConvertPictureToBitmap24(&m_Picture) == false)
		return 2;

	int Quit = 4;

	// Get file extension
	string Ext, Name(FileName);
	string::size_type idx = Name.rfind('.');
	if(idx != string::npos){
		Ext = Name.substr(idx + 1);
		if(stricmp(Ext.c_str(), "BMP") == 0)
			Quit = SaveToBMP(FileName);

		else if(stricmp(Ext.c_str(), "JPEG") == 0 || stricmp(Ext.c_str(), "JPG") == 0)
			Quit = SaveToJPEG(FileName);
	}

	return Quit;
}

long CWSPicture::SaveToBMP(LPCTSTR FileName) 
{
	// TODO: Add your dispatch handler code here
	int Quit = 0;

	unsigned char * pPic = m_Picture;
	if(pPic != NULL){
		BITMAPINFOHEADER bmiHeader = *((LPBITMAPINFOHEADER)pPic);

		if(bmiHeader.biBitCount == 24){

			FILE * fileSave;
			fileSave = fopen(FileName, "wb");
			if(fileSave){
				int LineLen = WIDTHBYTES(bmiHeader.biWidth*bmiHeader.biBitCount);

				BITMAPFILEHEADER bmfHeader;
				memset(&bmfHeader, 0, sizeof(BITMAPFILEHEADER));
				bmfHeader.bfType = 0x4D42;
				bmfHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
				bmfHeader.bfSize = bmfHeader.bfOffBits + LineLen * (int)bmiHeader.biHeight;
				
				fwrite(&bmfHeader, 1, sizeof(BITMAPFILEHEADER), fileSave);
				fwrite(&bmiHeader, 1, sizeof(BITMAPINFOHEADER), fileSave);
				fwrite(pPic + bmiHeader.biSize, 1, LineLen * (int)bmiHeader.biHeight, fileSave);

				fclose(fileSave);
			}
			else Quit = 1;
		}
		else Quit = 2;
	}
	else Quit = 2;

	return Quit;
}

long CWSPicture::SaveToJPEG(LPCTSTR FileName)
{
	int Quit = 0;

    return Quit;
}

bool CWSPicture::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	long StoreMethod = 0;
	StoreMethod = m_Status;
	if(pPX->PX_Long("Status", StoreMethod, 0) == false)
		return false;

	unsigned long nSize = 0;

	switch(StoreMethod){
	case wsPicture:
		if(pPX->IsLoading()){
			if(pPX->PX_Long("BitmapSize", nSize, 0) == false)
				return false;
			if(nSize < sizeof(BITMAPINFOHEADER))
				return false;
			unsigned char * pPic = new unsigned char[nSize];
			if(pPic != NULL){
				if(pPX->PX_Memory("Bitmap", nSize, pPic) == false) {
					delete [] pPic;
					return false;
				}
			}
			else
				return false;

			if(m_Picture != NULL){delete [] m_Picture; m_Picture = NULL;}
			m_Picture = pPic;
			m_Status = StoreMethod;
		}
		else {
			if(m_Picture) {
				BITMAPINFOHEADER bmiHeader;
				memset(&bmiHeader, 0, sizeof(BITMAPINFOHEADER));
				bmiHeader = *((LPBITMAPINFOHEADER)m_Picture);
				nSize = sizeof(BITMAPINFOHEADER) + WIDTHBYTES(bmiHeader.biWidth * bmiHeader.biBitCount) * bmiHeader.biHeight;
				if(pPX->PX_Long("BitmapSize", nSize, 0) == false)
					return false;
				if(pPX->PX_Memory("Bitmap", nSize, m_Picture) == false)
					return false;
			}
			else
				return false;
		}
		break;
	}

	return true;
}

#ifndef _WINDOWS_
int get_byte_order (void)
{
    union {
        char c[sizeof(short)];
        short s;
    } order;

    order.s = 1;
    if ((1 == order.c[0])) {
        return 0;
    } else {
        return 1;
    }
}

/*XImage* create_image_from_bmp_buffer (Display *dis, int screen, u_char *buf, int width, int height, int LineLen)
{
    int depth;
    XImage *img = NULL;
    Visual *vis;
    double rRatio;
    double gRatio;
    double bRatio;
    int outIndex = 0;
    int i, l, k;
    int numBufBytes = (3 * (width * height));

    depth = DefaultDepth (dis, screen);
    vis = DefaultVisual (dis, screen);

    rRatio = vis->red_mask / 255.0;
    gRatio = vis->green_mask / 255.0;
    bRatio = vis->blue_mask / 255.0;

    if (depth >= 24) {
        size_t numNewBufBytes = (4 * (width * height));
        DWORD *newBuf = (DWORD *)malloc (numNewBufBytes);

        for (l = height - 1; l >= 0 ; l--) {
            i = l * LineLen;
            for (k = 0; k < width; k++) {
                unsigned int r, g, b;
                r = (buf[i] * rRatio);
                ++i;
                g = (buf[i] * gRatio);
                ++i;
                b = (buf[i] * bRatio);
                ++i;

                r &= vis->red_mask;
                g &= vis->green_mask;
                b &= vis->blue_mask;

                newBuf[outIndex] = r | g | b;
                ++outIndex;
            }
        }

        img = XCreateImage (dis,
            CopyFromParent, depth,
            ZPixmap, 0,
            (char *) newBuf,
            width, height,
            32, 0
        );

    } else if (depth >= 15) {
        size_t numNewBufBytes = (2 * (width * height));
        WORD *newBuf = (WORD *)malloc (numNewBufBytes);

        for (l = height - 1; l >= 0 ; l--) {
            i = l * LineLen;
            for (k = 0; k < width; k++) {
                unsigned int r, g, b;

                r = (buf[i] * rRatio);
                ++i;
                g = (buf[i] * gRatio);
                ++i;
                b = (buf[i] * bRatio);
                ++i;

                r &= vis->red_mask;
                g &= vis->green_mask;
                b &= vis->blue_mask;

                newBuf[outIndex] = r | g | b;
                ++outIndex;
            }
        }

        img = XCreateImage (dis,
            CopyFromParent, depth,
            ZPixmap, 0,
            (char *) newBuf,
            width, height,
            16, 0
        );
    } else {
        fprintf (stderr, "This program does not support displays with a depth less than 15.");
        return NULL;
    }

    XInitImage (img);
    if ((LSBFirst == get_byte_order ())) {
        img->byte_order = LSBFirst;
    } else {
        img->byte_order = MSBFirst;
    }

    img->bitmap_bit_order = MSBFirst;

    return img;
}*/

/*bool CWSPicture::Render(Display* dpy, Window win, long x, long y, long cx, long cy, long xSrc, long ySrc, long cxSrc, long cySrc)
{
    bool bRes = false;
    if(m_Picture) {
        if(ConvertPictureToBitmap24(&m_Picture)) {
            unsigned char * pPic = m_Picture;
            BITMAPINFOHEADER bmiHeader = *((LPBITMAPINFOHEADER)pPic);
            if(bmiHeader.biBitCount == 24){
                int LineLen = WIDTHBYTES(bmiHeader.biWidth*bmiHeader.biBitCount);
                if(dpy) {
                    int screen = DefaultScreen (dpy);
                    XImage * Image = create_image_from_bmp_buffer (dpy, screen, pPic + bmiHeader.biSize,
                                                               bmiHeader.biWidth, bmiHeader.biHeight, LineLen);
                    if(Image) {
                        GC gc = XCreateGC(dpy, win, 0, NULL);
                        XPutImage (dpy, win, gc, Image, 0, 0, 0, 0, bmiHeader.biWidth, bmiHeader.biHeight);
                        bRes = true;
                    }
                }
            }
        }
    }
    return bRes;
}*/
#endif

IWSPicture * CWSPicture::Clone()
{
	CWSPicture *pNewObject = CWSPicture::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}

// Mode:
//	0 - ��������� �� 90 ��������
//	1 - ��������� �� 180 ��������
//	2 - ��������� �� 270 ��������
bool CWSPicture::RotatePicture(unsigned char ** ppPicture, long Mode)
{

	if(ppPicture == NULL || *ppPicture == NULL) return FALSE;

	if(ConvertPictureToBitmap24(ppPicture) == FALSE)
		return FALSE;

	unsigned char * pNew = NULL;
	bool bResult = false;

	unsigned char * pPic = *ppPicture;
	if(pPic != NULL)
	{
		BITMAPINFOHEADER bmiHeaderTag;
		BITMAPINFOHEADER bmiHeader = *((LPBITMAPINFOHEADER)pPic);
		long LineLenBitmap = (long)WIDTHBYTES(bmiHeader.biWidth * bmiHeader.biBitCount);
		long nSize = 0;

		if(bmiHeader.biBitCount == 24)
		{
			switch(Mode){
			case 2:
				memset(&bmiHeaderTag, 0, sizeof(BITMAPINFOHEADER));
				bmiHeaderTag.biSize = sizeof(BITMAPINFOHEADER);
				bmiHeaderTag.biWidth = bmiHeader.biHeight;
				bmiHeaderTag.biHeight = bmiHeader.biWidth;
				bmiHeaderTag.biBitCount = 24;
				bmiHeaderTag.biPlanes = 1;
				bmiHeaderTag.biCompression = BI_RGB;

				nSize = sizeof(BITMAPINFOHEADER) + WIDTHBYTES(bmiHeaderTag.biWidth * bmiHeaderTag.biBitCount) * bmiHeaderTag.biHeight;
				pNew = new unsigned char[nSize];
				if(pNew != NULL)
				{
					*((LPBITMAPINFOHEADER)pNew) = bmiHeaderTag;
					long LineLenBitmapTag = (long)WIDTHBYTES(bmiHeaderTag.biWidth * bmiHeaderTag.biBitCount);
					// ������������ �� 270 ��������
					for(int iY = 0; iY < bmiHeaderTag.biHeight; iY++)
					{
						unsigned char * lpTagget = pNew + iY * LineLenBitmapTag + sizeof(BITMAPINFOHEADER);
						for(int iX = 0; iX < bmiHeaderTag.biWidth; iX++)
						{
			  				unsigned char * lpSource = pPic + (bmiHeaderTag.biWidth - iX - 1) * LineLenBitmap + iY * 3 + sizeof(BITMAPINFOHEADER);
							lpTagget[0] = lpSource[0];
							lpTagget[1] = lpSource[1];
							lpTagget[2] = lpSource[2];
							lpTagget += 3;
						}
					}
				}
				break;
			case 1:
				memset(&bmiHeaderTag, 0, sizeof(BITMAPINFOHEADER));
				bmiHeaderTag.biSize = sizeof(BITMAPINFOHEADER);
				bmiHeaderTag.biWidth = bmiHeader.biWidth;
				bmiHeaderTag.biHeight = bmiHeader.biHeight;
				bmiHeaderTag.biBitCount = 24;
				bmiHeaderTag.biPlanes = 1;
				bmiHeaderTag.biCompression = BI_RGB;

				nSize = sizeof(BITMAPINFOHEADER) + WIDTHBYTES(bmiHeaderTag.biWidth * bmiHeaderTag.biBitCount) * bmiHeaderTag.biHeight;
				pNew = new unsigned char[nSize];
				if(pNew != NULL)
				{
					*((LPBITMAPINFOHEADER)pNew) = bmiHeaderTag;
					long LineLenBitmapTag = (long)WIDTHBYTES(bmiHeaderTag.biWidth * bmiHeaderTag.biBitCount);
					// ������������ �� 180 ��������
					for(int iY = 0; iY < bmiHeaderTag.biHeight; iY++)
					{
						unsigned char * lpTagget = pNew + iY * LineLenBitmapTag + sizeof(BITMAPINFOHEADER);
						for(int iX = 0; iX < bmiHeaderTag.biWidth; iX++)
						{
			  				unsigned char * lpSource = pPic + (bmiHeaderTag.biWidth - iX - 1) * 3 + (bmiHeaderTag.biHeight - iY - 1) * LineLenBitmap + sizeof(BITMAPINFOHEADER);
							lpTagget[0] = lpSource[0];
							lpTagget[1] = lpSource[1];
							lpTagget[2] = lpSource[2];
							lpTagget += 3;
						}
					}
				}
				break;
			case 0:
				memset(&bmiHeaderTag, 0, sizeof(BITMAPINFOHEADER));
				bmiHeaderTag.biSize = sizeof(BITMAPINFOHEADER);
				bmiHeaderTag.biWidth = bmiHeader.biHeight;
				bmiHeaderTag.biHeight = bmiHeader.biWidth;
				bmiHeaderTag.biBitCount = 24;
				bmiHeaderTag.biPlanes = 1;
				bmiHeaderTag.biCompression = BI_RGB;

				nSize = sizeof(BITMAPINFOHEADER) + WIDTHBYTES(bmiHeaderTag.biWidth * bmiHeaderTag.biBitCount) * bmiHeaderTag.biHeight;
				pNew = new unsigned char[nSize];
				if(pNew != NULL)
				{
					*((LPBITMAPINFOHEADER)pNew) = bmiHeaderTag;
					long LineLenBitmapTag = (long)WIDTHBYTES(bmiHeaderTag.biWidth * bmiHeaderTag.biBitCount);
					// ������������ �� 90 ��������
					for(int iY = 0; iY < bmiHeaderTag.biHeight; iY++)
					{
						unsigned char * lpTagget = pNew + iY * LineLenBitmapTag + sizeof(BITMAPINFOHEADER);
						for(int iX = 0; iX < bmiHeaderTag.biWidth; iX++)
						{
			  				unsigned char * lpSource = pPic + iX * LineLenBitmap + (bmiHeaderTag.biHeight - iY - 1) * 3 + sizeof(BITMAPINFOHEADER);
							lpTagget[0] = lpSource[0];
							lpTagget[1] = lpSource[1];
							lpTagget[2] = lpSource[2];
							lpTagget += 3;
						}
					}
				}
				break;
			}
		}

		if(pNew) {
			if(pPic != NULL) {
				delete [] pPic;
				pPic = NULL;
			}
			*ppPicture = pNew;
			bResult = true;
			pNew = NULL;
		}
	}

	if(pNew) {
		delete [] pNew;
		pNew = NULL;
	}

	return bResult;
}

void CWSPicture::ScaleBuffer(unsigned char * InBuffer, int InW, int InH, int InBPP, unsigned char * OutBuffer, int OutW, int OutH, int OutBPP)
{

}
