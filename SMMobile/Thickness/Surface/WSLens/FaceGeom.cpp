#include "stdafx.h"
#include <math.h>
#include "FaceGeom.h"

#define _cyl_x_axis		75.			// x-������� �������� ������
#define _cyl_z_axis		105.		// z-������� �������� ������
#define _face_d			15.			// ���������� �� ����� �������� ����� �������� ��
									// ��������� ����
#define _brow_d			10.			// ���������� �� ��������� �� ����� ���� 
									// �� ������ ��������� ������
#define _brow_alpha		15.			// ������ ��������� ������ �� �����������
#define _lens_z_height	13.			// ������ ����� ��� �����

#define _cone_d			5.			// ������ ������ ������ �� ��������� �� ����� ����
#define _cone_alpha		30.			// ���� ��������� ������ � ��������� XY
#define _cone_h			70.			// ������ ������
#define _cone_axes_r	1.7			// ��������� ������� ������� ������ � ��-�� YZ � ��� � ��-�� XY
#define _cone_z_depth	5.			// �������� ������ ������ ������ �� ��������� ����

#define _z_backblane	-40.		// z-���������� ��������� ���������
#define _k_backplane	0.01		// �������� ��������� ���������

// ������� ����� ��������� ����
CPoint3D CFaceGeom::ptUpFacePlane1
(
	_cyl_x_axis * sqrt(_face_d * (2 * _cyl_z_axis - _face_d)) / _cyl_z_axis,
	_brow_d,
	_cyl_z_axis - _face_d
);

CPoint3D CFaceGeom::ptUpFacePlane2
(
	- _cyl_x_axis * sqrt(_face_d * (2 * _cyl_z_axis - _face_d)) / _cyl_z_axis,
	_brow_d,
	_cyl_z_axis - _face_d
);

// ��������� ������
CPlane CFaceGeom::BrowPlane
(
	ptUpFacePlane1,
	CVec3D(0, sin(_brow_alpha), cos(_brow_alpha))
);

// ��������� ����
CPlane CFaceGeom::FacePlane
(
	ptUpFacePlane1,
	CVec3D(0, 0, 1)
);

BOOL CFaceGeom::FindIntersectionPoint(CLine3D &Line, CPoint3D &Point)
{
	// !!!
	// ������� ���������� FALSE ������ ��� �������
	// !!!
	
	// ��� �������� � ����������� z, ������������� � �������� �����
	// ������� �������������
	Line.pz += (_cyl_z_axis - _face_d + _lens_z_height);

	CPoint3D p;

	// ����� ����������� ������ � ���������� ����
	if(FacePlane.FindIntersectWithLine(Line, p) &&
		p.py <= _brow_d && p.px < ptUpFacePlane1.px && p.px > ptUpFacePlane2.px)
	{
		// ����� ����
		//				    	  2	                              2				  2
		//			    2	     X + (Z - (_cyl_z_axis - _face_d))	/ _cone_axes_r
		// (Y - _cone_d)  = --------------------------------------------------------
		//	                    	               2
		//					                     tg _cone_alpha
		
		Line.pz -= _cyl_z_axis - _face_d - _cone_z_depth;
		Line.py -= _cone_d;

		double K = tan(_cone_alpha * M_PI / 180.); K = K * K;
		
		double A = Line.vx * Line.vx + Line.vz * Line.vz / (_cone_axes_r * _cone_axes_r) - Line.vy * Line.vy * K;
		
		double B = Line.px * Line.vx + Line.pz * Line.vz / (_cone_axes_r * _cone_axes_r) - Line.py * Line.vy * K;

		double C = Line.px * Line.px + Line.pz * Line.pz / (_cone_axes_r * _cone_axes_r) - Line.py * Line.py * K;

		double D = B * B - A * C;

		if(D > 0. && A > EPS)
		{
			double t1, t2;
			t1 = (- B - sqrt(D)) / A;
			t2 = (- B + sqrt(D)) / A;

			CPoint3D p1(Line.px + Line.vx * t1,
						Line.py + Line.vy * t1 + _cone_d,
						Line.pz + Line.vz * t1 + _cyl_z_axis - _face_d - _cone_z_depth);
			CPoint3D p2(Line.px + Line.vx * t2,
						Line.py + Line.vy * t2 + _cone_d,
						Line.pz + Line.vz * t2 + _cyl_z_axis - _face_d - _cone_z_depth);
			
			if(p1.pz > p2.pz)
				Point = p1;
			else
				Point = p2;

			if(Point.py > (_cone_d - _cone_h) && Point.py < _cone_d && Point.pz > (_cyl_z_axis - _face_d))
				return TRUE;
		}

		Point = p;
		return TRUE;
	}
	
	// ��������� ������
	if(BrowPlane.FindIntersectWithLine(Line, p) &&
		p.py > _brow_d && p.pz > (_cyl_z_axis - _face_d) && p.pz < _cyl_z_axis &&
		_hypot(p.px / _cyl_x_axis, p.pz / _cyl_z_axis) < 1.)
	{
		Point = p;
		return TRUE;
	}

	// ����� ����������� ������ � ���������
	double A = Line.vx / _cyl_x_axis * Line.vx / _cyl_x_axis +
				Line.vz / _cyl_z_axis * Line.vz / _cyl_z_axis;

	double B = Line.px / _cyl_x_axis * Line.vx / _cyl_x_axis +
				Line.pz / _cyl_z_axis * Line.vz / _cyl_z_axis;
	
	double C = Line.px / _cyl_x_axis * Line.px / _cyl_x_axis +
				Line.pz / _cyl_z_axis * Line.pz / _cyl_z_axis - 1.;

	double D = B * B - A * C;

	if(D <= 0. || A < EPS)
	{
		// ������� �� ������������
		// ������ ����������� � ��������� ����������
		if(_k_backplane > 0.)
		{
			Line.pz -= _z_backblane + 1. / _k_backplane;

			double A = Line.vx * Line.vx + Line.vz * Line.vz;
			double B = Line.px * Line.vx + Line.pz * Line.vz;
			double C = Line.px * Line.px + Line.pz * Line.pz - 1. / (_k_backplane * _k_backplane);

			double D = B * B - A * C;

			if(D > 0. && A > EPS)
			{
				double t1, t2;
				t1 = (- B - sqrt(D)) / A;
				t2 = (- B + sqrt(D)) / A;

				CPoint3D p1(Line.px + Line.vx * t1,
							Line.py + Line.vy * t1,
							Line.pz + Line.vz * t1 + _z_backblane + 1. / _k_backplane);
				CPoint3D p2(Line.px + Line.vx * t2,
							Line.py + Line.vy * t2,
							Line.pz + Line.vz * t2 + _z_backblane + 1. / _k_backplane);
				
				if(p1.pz < p2.pz)
					Point = p1;
				else
					Point = p2;

				return TRUE;
			}
		}

		double t = (_z_backblane - Line.pz) / Line.vz;
		Point.px = Line.px + Line.vx * t;
		Point.py = Line.py + Line.vy * t;
		Point.pz = Line.pz + Line.vz * t;
		
		return TRUE;
	}

	double t1, t2;
	t1 = (- B - sqrt(D)) / A;
	t2 = (- B + sqrt(D)) / A;

	CPoint3D p1(Line.px + Line.vx * t1, Line.py + Line.vy * t1, Line.pz + Line.vz * t1);
	CPoint3D p2(Line.px + Line.vx * t2, Line.py + Line.vy * t2, Line.pz + Line.vz * t2);

	if(p1.pz > p2.pz)
		Point = p1;
	else
		Point = p2;

	return TRUE;
}
