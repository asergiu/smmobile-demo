#include "stdafx.h"
#include "PropObjectExchange.h"
#include "ObjectBase.h"

CPropObjectExchange::CPropObjectExchange(void)
{
	bLoad = false;
	bReset = false;
	pOutStream = NULL;
	pInStream = NULL;
}

CPropObjectExchange::~CPropObjectExchange(void)
{
}

bool CPropObjectExchange::IsLoading()
{
	return bLoad;
}

bool CPropObjectExchange::ExchangeData(unsigned long nSize, void* pvData)
{
	if(!pvData)
		return false;
	if(nSize == 0)
		return false;

	try {
		if(IsLoading()) {
			if(!pInStream)
				return false;
			pInStream->read((char*)pvData, nSize);
		}
		else {
			if(!pOutStream)
				return false;
			pOutStream->write((char*)pvData, nSize);
		}
	}
	catch(...) {
		return false;
	}
	return true;
}

bool CPropObjectExchange::PX_Memory(LPCTSTR /*pszPropName*/, unsigned long nSize, void* pvProp)
{
	if(bReset)
		return true;
	return ExchangeData(nSize, pvProp);
}
bool CPropObjectExchange::ExchangeVersion(long& lVersion)
{
	if(bReset)
		return true;
	return ExchangeData(sizeof(long), &lVersion);
}
bool CPropObjectExchange::PX_Long(LPCTSTR /*pszPropName*/, long& lValue, long lDefault)
{
	if(bLoad)
		lValue = lDefault;
	if(bReset)
		return true;
	return ExchangeData(sizeof(long), &lValue);
}
bool CPropObjectExchange::PX_Long(LPCTSTR /*pszPropName*/, unsigned long& lValue, long lDefault)
{
	if(bLoad)
		lValue = lDefault;
	if(bReset)
		return true;
	return ExchangeData(sizeof(long), &lValue);
}
bool CPropObjectExchange::PX_Double(LPCTSTR /*pszPropName*/, double& doubleValue, double doubleDefault)
{
	if(bLoad)
		doubleValue = doubleDefault;
	if(bReset)
		return true;
	return ExchangeData(sizeof(double), &doubleValue);
}
bool CPropObjectExchange::PX_Float(LPCTSTR /*pszPropName*/, float& floatValue, float floatDefault)
{
	if(bLoad)
		floatValue = floatDefault;
	if(bReset)
		return true;
	return ExchangeData(sizeof(float), &floatValue);
}
bool CPropObjectExchange::PX_String(LPCTSTR /*pszPropName*/, string& strValue, const string& strDefault)
{
	if(bLoad)
		strValue = strDefault;
	if(bReset)
		return true;
	bool bRes = false;
	long nstrSize;
	try {
		if(IsLoading()) {
			if((bRes = ExchangeData(sizeof(long), &nstrSize)) == false)
				return false;
			strValue.clear();
			if(nstrSize > 0) {
				strValue.resize(nstrSize);
				bRes = ExchangeData(nstrSize, (void*)strValue.c_str());
			}
		}
		else {
			nstrSize = strValue.size();
			if((bRes = ExchangeData(sizeof(long), &nstrSize)) == false)
				return false;
			if(nstrSize > 0) {
				bRes = ExchangeData(nstrSize, (void*)strValue.c_str());
			}
		}
	}
	catch(...) {
		return false;
	}
	return bRes;
}
bool CPropObjectExchange::PX_Bool(LPCTSTR pszPropName, bool& bValue, bool bDefault)
{
	bool bRes = false;
	long nValue = (bValue == true)? 1 : 0;
	long nDefault = (bDefault == true)? 1 : 0;
	if((bRes = PX_Long(pszPropName, nValue, nDefault)) == false)
		return false;
	bValue = (nValue)? true : false;
	return bRes;
}
bool CPropObjectExchange::PX_Font(LPCTSTR /*pszPropName*/, LOGFONT& fValue, LOGFONT * pfDefault)
{
	if(bLoad) {
		if(pfDefault)
			fValue = *pfDefault;
	}
	if(bReset)
		return true;
	return ExchangeData(sizeof(LOGFONT), &fValue);
}
