// ARCoating.cpp : implementation file
//

#include "stdafx.h"
#include "ARCoating.h"

/////////////////////////////////////////////////////////////////////////////
// CARCoating

CARCoating* CARCoating::CreateObject()
{
	CARCoating *pObject = new CARCoating;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

IARCoating * CARCoating::GetInterface()
{
	return static_cast<IARCoating*>(this);
}

CARCoating::CARCoating()
{
	m_r = m_g = m_b = 255;

	m_Name = "";
	m_EDPCode = "";
	m_Info = "";
}

CARCoating::~CARCoating()
{
}

unsigned long CARCoating::GetB()
{
	return m_b;
}
unsigned long CARCoating::GetG()
{
	return m_g;
}
unsigned long CARCoating::GetR()
{
	return m_r;
}
bool CARCoating::SetB(unsigned long newVal)
{
	if(!TestRangeMin0(newVal, 255, 1))
		return false;
	m_b = newVal;
	FireChange();
	SetModifiedFlag();
	return true;
}
bool CARCoating::SetG(unsigned long newVal)
{
	if(!TestRangeMin0(newVal, 255, 1))
		return false;
	m_g = newVal;
	FireChange();
	SetModifiedFlag();
	return true;
}
bool CARCoating::SetR(unsigned long newVal)
{
	if(!TestRangeMin0(newVal, 255, 1))
		return false;
	m_r = newVal;
	FireChange();
	SetModifiedFlag();
	return true;
}

string CARCoating::GetName() 
{
	return m_Name;
}

bool CARCoating::SetName(LPCTSTR lpszNewValue) 
{
	m_Name = lpszNewValue;
	SetModifiedFlag();
	FireChange();
	return true;
}

string CARCoating::GetEDPCode() 
{
	return m_EDPCode;
}

bool CARCoating::SetEDPCode(LPCTSTR lpszNewValue) 
{
	m_EDPCode = lpszNewValue;
	SetModifiedFlag();
	FireChange();
	return true;
}

string CARCoating::GetInfo() 
{
	return m_Info;
}

bool CARCoating::SetInfo(LPCTSTR lpszNewValue) 
{
	m_Info = lpszNewValue;
	SetModifiedFlag();
	FireChange();
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// Persistence

bool CARCoating::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	if(pPX->PX_Long("R", m_r, 255) == false)
		return false;
	if(pPX->PX_Long("G", m_g, 255) == false)
		return false;
	if(pPX->PX_Long("B", m_b, 255) == false)
		return false;
	if(pPX->PX_String("Name", m_Name, "") == false)
		return false;
	if(pPX->PX_String("EDPCode", m_EDPCode, "") == false)
		return false;
	if(pPX->PX_String("Info", m_Info, "") == false)
		return false;

	return true;
}

IARCoating * CARCoating::Clone() 
{
	CARCoating *pNewObject = CARCoating::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}
