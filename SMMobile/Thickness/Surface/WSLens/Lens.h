#pragma once
// Lens.h : header file
//

#include "ObjectBase.h"
#include <string>
using std::string;

enum InternalAutomationID
{
	idaTintPoint = 0,
	idaTint,
	idaContourPoint,
	idaContour,
	idaARCoating,
	idaLens,
	idaLensSurface,
	idaDString,
	idaDStringCollection,
	idaMeasureData,
	idaContactLens,
	idaWSImage,
	idaCaptureDrivers,
	idaWSPicture,
	idaPictureCollection,
	idaCoating,
	idaCoatings
};

#define SET_FLAG(var, mask)	((var) |= (mask))
#define CLEAR_FLAG(var, mask)	((var) &= ~(mask))
#define IS_FLAG_SET(var, mask) ((var) & (mask))
#define IS_FLAG_CLEARED(var, mask) ((~var) & (mask))//(!IS_FLAG_SET(var, mask))

int ConvertToWSDesign(LPCTSTR lpstrDesign);

class CSpecEllipseBlank
{
	double SizeA, SizeB, Diff, SizeASqRec, SizeBSqRec;
public:
	CSpecEllipseBlank(double diff = 0.) {Diff = diff; SizeA = Diff + 1.; UpdateInternalData();};	
	double FindSpecEllipseSize(double x, double y);
	bool TrySpecEllipseSize(double x, double y);
	void UpdateInternalData();
};

/////////////////////////////////////////////////////////////////////////////
// CLens

class CLens :
	public CObjectEventBase<ILens, ILensEvents>,
	public CObjEventSimpleImpl<IContour, IContourEvents>,
	public CObjEventSimpleImpl<ITint, ITintEvents>,
	public CObjEventSimpleImpl<IARCoating, IARCoatingEvents>,
	public CObjEventSimpleImpl<ICoatings, ICoatingsEvents>,
	public CObjEventSimpleImpl<IMultifocalSegmentCollection, IMultifocalSegmentCollectionEvents>,
	public CObjEventSimpleImpl<IProgressive, IProgressiveEvents>
{
public:
    static CLens* CreateObject();
	ILens * GetInterface();

protected:
	CLens();           // protected constructor used by dynamic creation

// Attributes
	SideType m_Side;
	double m_Sph;
	double m_Cyl;
	double m_Add;
	long m_Axis;
	KindOfMaterial m_KindOfMaterial;
	double m_RefIndex;
	double m_Density;
	string m_Design;
	double m_CenterX;
	double m_CenterY;

	bool m_IsPhotochromic;
	double m_PhotochromicCarIndex;
	double m_PhotochromicHeatIndex;

	CObjPtr<ITint> lpdispTint;
	CObjPtr<ITint> lpdispPhotochromicTint;
	CObjPtr<IContour> lpdispContour;
	CObjPtr<IARCoating> lpdispARCoating;
	CObjPtr<ICoatings> lpdispCoatings;
	CObjPtr<IMultifocalSegmentCollection> lpdispMultifocal;

	CObjPtr<IContour> pDefContour;

	CObjPtr<ILensEngine> pLensEngine1;
	CObjPtr<ILensEngine> pLensEngine2;

	long m_Diameter;
	double m_PermittedCenterThickness;
	double m_PermittedEdgeThickness;
	double m_PermittedZeroThickness;

	double m_Weight;
	double m_CenterThickness;
	double m_MinEdgeThickness;
	double m_MaxEdgeThickness;
	double m_UVProtection;
	double m_MinDiam;

	long m_ErrorState;
	string m_ErrorString;

	CObjPtr<IProgressive> lpdispProgressive;
	double	m_Prism;
	long	m_PrismAxis;

	bool	m_EllipticBlank;
	double	m_EllipticVertToHorizRatio,
		m_OptToGeomCenterDisplacementX,
		m_OptToGeomCenterDisplacementY;

	MultifocalType m_multifocalType;
	double m_segmHeight;
	string m_EDPCode;
	long m_engineUsageMask;
	long m_SuspendCalc;

public:

// Operations
public:
	SideType GetSide();
	bool SetSide(SideType newValue);
	double GetSph();
	bool SetSph(double newValue);
	double GetCyl();
	bool SetCyl(double newValue);
	double GetAddit();
	bool SetAddit(double newValue);
	long GetAxis();
	bool SetAxis(long newValue);
	KindOfMaterial GetKindOfMaterial();
	bool SetKindOfMaterial(KindOfMaterial newValue);
	double GetRefIndex();
	bool SetRefIndex(double newValue);
	double GetDensity();
	bool SetDensity(double newValue);
	string GetDesign();
	bool SetDesign(LPCTSTR newValue);
	double GetCenterX();
	bool SetCenterX(double newValue);
	double GetCenterY();
	bool SetCenterY(double newValue);
	IContour * GetContour();
	bool SetContour(IContour *);
	ITint * GetTint();
	bool SetTint(ITint * newValue);
	IARCoating * GetARCoating();
	bool SetARCoating(IARCoating * newValue);
	long GetDiameter();
	bool SetDiameter(long newValue);
	double GetPermittedCenterThickness();
	bool SetPermittedCenterThickness(double newValue);
	double GetPermittedEdgeThickness();
	bool SetPermittedEdgeThickness(double newValue);
	double GetPermittedZeroThickness();
	bool SetPermittedZeroThickness(double newValue);
	long GetErrorState();
	MultifocalType GetMultifocalType();
	bool SetMultifocalType(MultifocalType newValue);
	double GetSegmHeight();
	bool SetSegmHeight(double newValue);
	string GetEDPCode();
	bool SetEDPCode(LPCTSTR newValue);
	ILensEngine * GetEngine1();
	bool SetEngine1(ILensEngine * newValue);
	ILensEngine * GetEngine2();
	bool SetEngine2(ILensEngine * newValue);
	long GetEngineUsageMask();
	bool SetEngineUsageMask(long newValue);
	long GetSuspendCalc();
	bool SetSuspendCalc(long newValue);
	double GetWeight();
	bool SetWeight(double newValue);
	double GetCenterThickness();
	bool SetCenterThickness(double newValue);
	double GetMinEdgeThickness();
	bool SetMinEdgeThickness(double newValue);
	double GetMaxEdgeThickness();
	bool SetMaxEdgeThickness(double newValue);
	double GetMinDiam();
	bool SetMinDiam(double newValue);
	bool GetIsPhotochromic();
	bool SetIsPhotochromic(bool newValue);
	ITint * GetPhotochromicTint();
	bool SetPhotochromicTint(ITint * newValue);
	double GetUVProtection();
	bool SetUVProtection(double newValue);
	ICoatings * GetCoatings();
	bool SetCoatings(ICoatings * newValue);
	double GetPhotochromicCarIndex();
	bool SetPhotochromicCarIndex(double newValue);
	double GetPhotochromicHeatIndex();
	bool SetPhotochromicHeatIndex(double newValue);
	IMultifocalSegmentCollection * GetMultifocal();
	bool SetMultifocal(IMultifocalSegmentCollection * newValue);
	IProgressive * GetProgressive();
	bool SetProgressive(IProgressive * newValue);
	double GetPrism();
	bool SetPrism(double newValue);
	long GetPrismAxis();
	bool SetPrismAxis(long newValue);
	bool GetEllipticBlank();
	bool SetEllipticBlank(bool newValue);
	double GetEllipticVertToHorizRatio();
	bool SetEllipticVertToHorizRatio(double newValue);
	double GetOptToGeomCenterDisplacementX();
	bool SetOptToGeomCenterDisplacementX(double newValue);
	double GetOptToGeomCenterDisplacementY();
	bool SetOptToGeomCenterDisplacementY(double newValue);
	ILens * Clone();

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);

// Implementation
protected:
	virtual ~CLens();

	virtual void FinalRelease()
	{
		Clear();
		delete this;
	}
	void Clear();

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->LensChanged();
			}
		}
	}

	void SetError(int nError);
	
	bool CalcThickness(	double *ResultCenterThickness, double *ResultMinEdgeThickness,
						double *ResultMaxEdgeThickness);
	bool CalcDiam(double *ResultDiam);
	double CalcUVProtection();

	bool m_NeedToComputeWeight, m_NeedToComputeDiam, m_NeedToComputeThicknesses,
		m_NeedToComputeUVProtection;

	void InvalidateComputableParams();

protected:
	void OnChildChanged();

	void ContourChanged()
	{
		OnChildChanged();
	}
	void TintChanged()
	{
		OnChildChanged();
	}
	void ARCoatingChanged()
	{
		OnChildChanged();
	}
	void CoatingsChanged()
	{
		OnChildChanged();
	}
	void MultifocalSegmentCollectionChanged()
	{
		OnChildChanged();
	}
	void ProgressiveChanged()
	{
		OnChildChanged();
	}
};
