#pragma once

#include "WSPicture.h"

class CColorPlanes
{
public:
	enum HandleUsageTypes{
		wsCreateNewHandle = 0,
		wsUseCurrentHandle = 1
	};
	BOOL GetColorPlanes(IWSPicture * lpdispPicture, PUCHAR &R, PUCHAR &G, PUCHAR &B, int& Width, int& Height);
	BOOL CreatePictureFromColorPlanes(IWSPicture * lpdispResult,
		PUCHAR R, PUCHAR G, PUCHAR B, int Width, int Height);
	RGBQUAD GetPixel(IWSPicture * lpdispPicture, int X, int Y);
};
