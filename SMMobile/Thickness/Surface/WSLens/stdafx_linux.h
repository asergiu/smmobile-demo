#pragma once

// C RunTime Header Files
#include <stdlib.h>
#include <alloca.h>
//#include <malloc.h>
#include <memory.h>
#include <strings.h>
#include <math.h>
#include <assert.h>

/*#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glext.h>*/
#include <OpenGLES/ES1/gl.h>
#include <OpenGLES/ES1/glext.h>

#define _alloca alloca

// TODO: reference additional headers your program requires here
typedef const char *LPCSTR;
typedef LPCSTR LPCTSTR;
typedef int LONG;
typedef unsigned long ULONG;
typedef unsigned int UINT;
typedef unsigned char BYTE;
typedef unsigned char *LPBYTE;
typedef char CHAR;
typedef unsigned char UCHAR;
typedef UCHAR *PUCHAR;
typedef unsigned short WORD;
typedef unsigned int DWORD;
typedef float FLOAT;
typedef unsigned short TCHAR;

#define MAKELONG(a, b)      ((LONG)(((WORD)(a)) | ((DWORD)((WORD)(b))) << 16))
#define LOWORD(l)           ((WORD)(l))
#define HIWORD(l)           ((WORD)(((DWORD)(l) >> 16) & 0xFFFF))

#define LF_FACESIZE         32
typedef struct tagLOGFONTA
{
    LONG      lfHeight;
    LONG      lfWidth;
    LONG      lfEscapement;
    LONG      lfOrientation;
    LONG      lfWeight;
    BYTE      lfItalic;
    BYTE      lfUnderline;
    BYTE      lfStrikeOut;
    BYTE      lfCharSet;
    BYTE      lfOutPrecision;
    BYTE      lfClipPrecision;
    BYTE      lfQuality;
    BYTE      lfPitchAndFamily;
    CHAR      lfFaceName[LF_FACESIZE];
} LOGFONT;

#define _EXPORTDLL_

typedef long HWND;
//typedef bool BOOL;
#define BOOL bool
#define TRUE true
#define FALSE false
#define CONST const

typedef long HRESULT;
#define S_OK 1;
#define S_FALSE 0;

typedef struct tagSIZE
{
    LONG cx;
    LONG cy;
} SIZE, *LPSIZE;
class CSize : public tagSIZE
{
public:
    
    // Constructors
    CSize(){};
    CSize(SIZE initPt){ *(SIZE*)this = initPt; };
    CSize(LONG initCX, LONG initCY){ cx = initCX; cy = initCY; };
    
    // Operations
    BOOL operator==(SIZE size) const{ return (cx == size.cx && cy == size.cy); };
    BOOL operator!=(SIZE size) const{ return (cx != size.cx || cy != size.cy); };
};

typedef struct tagPOINT
{
    LONG x;
    LONG y;
} POINT, *LPPOINT;
class CPoint : public tagPOINT
{
public:
    
    // Constructors
    CPoint(){};
    CPoint(POINT initPt){ *(POINT*)this = initPt; };
    CPoint(LONG initX, LONG initY){ x = initX; y = initY; };
    
    // Operations
    void Offset(LONG xOffset, LONG yOffset){ x += xOffset; y += yOffset; };
    void Offset(POINT point){ x += point.x; y += point.y; };
    BOOL operator==(POINT point) const{ return (x == point.x && y == point.y); };
    BOOL operator!=(POINT point) const{ return (x != point.x || y != point.y); };
    void operator+=(POINT point){ x += point.x; y += point.y; };
    void operator-=(POINT point){ x -= point.x; y -= point.y; };
    
    // Operators returning CPoint values
    CPoint operator-() const{ return CPoint(-x, -y); };
    CPoint operator+(POINT point) const{ return CPoint(x + point.x, y + point.y); };
    CPoint operator-(POINT point) const{ return CPoint(x - point.x, y - point.y); };
    CPoint operator+(SIZE size){ return CPoint(x + size.cx, y + size.cy); };
    CPoint operator-(SIZE size){ return CPoint(x - size.cx, y - size.cy); };
};

typedef struct tagRECT
{
    LONG left;
    LONG top;
    LONG right;
    LONG bottom;
} RECT, *LPRECT;
typedef const RECT *LPCRECT;
class CRect : public tagRECT
{
public:
    
    // Constructors
    CRect(){};
    CRect(int l, int t, int r, int b){ left = l; top = t; right = r; bottom = b; };
    CRect(const RECT& srcRect){ *(RECT*)this = srcRect; };
    CRect(POINT point, SIZE size){ right = (left = point.x) + size.cx; bottom = (top = point.y) + size.cy; };
    
    LONG Width(){ return right - left; };
    LONG Height(){ return bottom - top; };
};

typedef struct tagRGBQUAD
{
    BYTE    rgbBlue;
    BYTE    rgbGreen;
    BYTE    rgbRed;
    BYTE    rgbReserved;
} RGBQUAD;

typedef unsigned long COLORREF;
#define RGB(r,g,b)          ((COLORREF)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16)))

inline int stricmp(const char * a, const char * b)
{
    return strcasecmp(a, b);
}

inline double _hypot(double a, double b)
{
    return hypot(a, b);
}

inline double _copysign(double a, double b)
{
    return copysign(a, b);
}

/* constants for the biCompression field */
#define BI_RGB        0L
#define BI_RLE8       1L
#define BI_RLE4       2L
#define BI_BITFIELDS  3L
#define BI_JPEG       4L
#define BI_PNG        5L

#pragma pack(push, 1)
/* structures for defining DIBs */
typedef struct tagBITMAPCOREHEADER {
        DWORD   bcSize;                 /* used to get to color table */
        WORD    bcWidth;
        WORD    bcHeight;
        WORD    bcPlanes;
        WORD    bcBitCount;
} BITMAPCOREHEADER, *LPBITMAPCOREHEADER;

typedef struct tagBITMAPFILEHEADER {
        WORD    bfType;
        DWORD   bfSize;
        WORD    bfReserved1;
        WORD    bfReserved2;
        DWORD   bfOffBits;
} BITMAPFILEHEADER, *LPBITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER{
        DWORD      biSize;
        LONG       biWidth;
        LONG       biHeight;
        WORD       biPlanes;
        WORD       biBitCount;
        DWORD      biCompression;
        DWORD      biSizeImage;
        LONG       biXPelsPerMeter;
        LONG       biYPelsPerMeter;
        DWORD      biClrUsed;
        DWORD      biClrImportant;
} BITMAPINFOHEADER, *LPBITMAPINFOHEADER;
#pragma pack(pop)

/* Font Weights */
#define FW_DONTCARE         0
#define FW_THIN             100
#define FW_EXTRALIGHT       200
#define FW_LIGHT            300
#define FW_NORMAL           400
#define FW_MEDIUM           500
#define FW_SEMIBOLD         600
#define FW_BOLD             700
#define FW_EXTRABOLD        800
#define FW_HEAVY            900

#define FW_ULTRALIGHT       FW_EXTRALIGHT
#define FW_REGULAR          FW_NORMAL
#define FW_DEMIBOLD         FW_SEMIBOLD
#define FW_ULTRABOLD        FW_EXTRABOLD
#define FW_BLACK            FW_HEAVY

//#ifndef max
//#define max(a,b)            (((a) > (b)) ? (a) : (b))
//#endif

//#ifndef min
//#define min(a,b)            (((a) < (b)) ? (a) : (b))
//#endif

template<class TObject>
TObject max(TObject a, TObject b)
{
    return (((a) > (b)) ? (a) : (b));
}

template<class TObject>
TObject min(TObject a, TObject b)
{
    return (((a) < (b)) ? (a) : (b));
}

inline bool EqualRect(const RECT * rA, const RECT * rB)
{
    if(rA && rB) {
        if(rA->left == rB->left && rA->right == rB->right && rA->top == rB->top && rA->bottom == rB->bottom)
            return true;
    }
    return false;
}

#define GetRValue(rgb)      ((BYTE)(rgb))
#define GetGValue(rgb)      ((BYTE)(((WORD)(rgb)) >> 8))
#define GetBValue(rgb)      ((BYTE)((rgb)>>16))

#define __cdecl

template<class TObject>
void ASSERT(TObject a)
{
    assert(a);
}
