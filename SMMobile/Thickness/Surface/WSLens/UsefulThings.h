#ifndef USEFUL_TYPES_AND_FUNCS_HEADER_INCLUDED

#include "ClassMatrix.h"
#define USEFUL_TYPES_AND_FUNCS_HEADER_INCLUDED

#ifndef SomeTypesDefined
#define SomeTypesDefined

typedef int		YType;	   //yt
typedef YType* 	YTLine;	   //yl
typedef YTLine* YTMatrix;  //ym
typedef double 	FType;	   //ft
typedef FType* 	FTLine;	   //fl
typedef FTLine* FTMatrix;  //fm

#endif

#ifndef CommonInfoHeaderIncluded
#define CommonInfoHeaderIncluded

#if !defined __FTPOINT__
#define __FTPOINT__
typedef struct tagFTPOINT {
	FType x,y;
	} FTPOINT,*PFTPOINT;
#endif

struct FTRECT {
	double left,top,right,bottom;
	};

FType Azimut(FType y,FType x);

#endif

#ifndef EPS

#define EPS 		(1e-9)
#define M_PI        3.14159265358979323846
#define GREY_COLOR(a)	RGB(a,a,a)



int RoundUp(FType Arg);
int RoundDown(FType Arg);
int Round(FType Arg);
#endif

FType Hypot(int a,int b);
FType Azimuth(FType y,FType x);
//CRect &CreateSquareAroundPoint(CPoint Point,int Len);
RECT &Inflate(POINT& Point,int Len);
RECT &Inflate(RECT& Rect,int Len);
void TransCoord(RECT &SrcRect,RECT &DstRect,POINT &SrcPoint,POINT &DstPoint);
void TransCoord(RECT &SrcRect,RECT &DstRect,FTPOINT &SrcPoint,FTPOINT &DstPoint);
double RectS(const RECT &Rect);
void CopyPartMatr(CByteMatrix &SourceMatrix,RECT &SourceRect,
				  CByteMatrix &DestMatrix,POINT &DestBasePoint);
void CopyPartMatr(CIntMatrix &SourceMatrix,RECT &SourceRect,
				  CIntMatrix &DestMatrix,POINT &DestBasePoint);
BOOL InSeRect(RECT &Src,RECT &Dst);

#endif

#ifdef _WINDOWS_
void DrawColorMatrixInDC(CDC *dc,CColorMatrix32 &Matrix,int PosX,int PosY);
#endif
