// CDString.cpp : implementation file
//

#include "stdafx.h"
#include "DString.h"
#include "PrepareFont.h"

/////////////////////////////////////////////////////////////////////////////
// CDString

CDString* CDString::CreateObject()
{
	CDString *pObject = new CDString;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

IDString * CDString::GetInterface()
{
	return static_cast<IDString*>(this);
}

CDString::CDString()
{
	m_origin = wsLeftLower;
	m_left = 0;
	m_top = 0;
	m_width = 0;
	m_height = 0;
	m_format = wsDStrTop | wsDStrLeft;
	PrepareFont(&m_Font, 10);
	m_foreColor = RGB(255, 255, 255);
	m_backColor = RGB(0, 0, 0);
	m_bkMode = wsTransparent;
	m_outline = true;
	m_outlineColor = RGB(0, 0, 0);
    m_FontL = "*-fixed-medium-r-normal-*-12-*";
}

CDString::~CDString()
{
}

string CDString::GetText()
{
	return m_Text;
}
bool CDString::SetText(LPCTSTR newValue)
{
	m_Text.clear();
	if(newValue) {
		m_Text = newValue;
	}
	FireChange();
	SetModifiedFlag();
	return true;
}

DStringOrigins CDString::GetOrigin()
{
	return m_origin;
}
bool CDString::SetOrigin(DStringOrigins newValue)
{
	m_origin = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

unsigned long CDString::GetLeft()
{
	return m_left;
}
bool CDString::SetLeft(unsigned long newValue)
{
	m_left = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

unsigned long CDString::GetTop()
{
	return m_top;
}
bool CDString::SetTop(unsigned long newValue)
{
	m_top = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

unsigned long CDString::GetWidth()
{
	return m_width;
}
bool CDString::SetWidth(unsigned long newValue)
{
	m_width = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

unsigned long CDString::GetHeight()
{
	return m_height;
}
bool CDString::SetHeight(unsigned long newValue)
{
	m_height = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

long CDString::GetFormat()
{
	return m_format;
}
bool CDString::SetFormat(long newValue)
{
	m_format = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

#ifndef _WINDOWS_
string CDString::GetFont()
{
    return m_FontL;
}
bool CDString::SetFont(const char * newValue)
{
    m_FontL.clear();
    if(newValue) {
        m_FontL = newValue;
    }
    FireChange();
    SetModifiedFlag();
    return true;
}
#else
LOGFONT CDString::GetFont()
{
	return m_Font;
}
bool CDString::SetFont(LOGFONT newValue)
{
	m_Font = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}
#endif

unsigned long CDString::GetForeColor()
{
	return m_foreColor;
}
bool CDString::SetForeColor(unsigned long newValue)
{
	m_foreColor = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

unsigned long CDString::GetBackColor()
{
	return m_backColor;
}
bool CDString::SetBackColor(unsigned long newValue)
{
	m_backColor = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

DStringBkModes CDString::GetBkMode()
{
	return m_bkMode;
}
bool CDString::SetBkMode(DStringBkModes newValue)
{
	m_bkMode = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

bool CDString::GetOutline()
{
	return m_outline;
}
bool CDString::SetOutline(bool newValue)
{
	m_outline = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

unsigned long CDString::GetOutlineColor()
{
	return m_outlineColor;
}
bool CDString::SetOutlineColor(unsigned long newValue)
{
	m_outlineColor = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

bool CDString::CalcRect(unsigned long * ExactWidth, unsigned long * ExactHeight) 
{
	bool bRes = false;
#ifdef _WINDOWS_
	CClientDC dc(NULL);
	CDC MemDC;

	MemDC.CreateCompatibleDC(&dc);

	CFont f, *oldf;
	f.CreateFontIndirect(&m_Font);

	oldf = MemDC.SelectObject(&f);
	CString str(m_Text.c_str());
	CSize sz = MemDC.GetTextExtent(str);
	MemDC.SelectObject(oldf);
	
	int d = (m_outline)?2:0;

	*ExactWidth = sz.cx + d;
	*ExactHeight = sz.cy + d;
	bRes = true;
#else
    /**ExactWidth = 20;
    *ExactHeight = 10;
    bRes = true;
    try {
        Display *dpy = XOpenDisplay(0);
        if(dpy) {
            XFontStruct * FontInfo = XLoadQueryFont(dpy, m_FontL.c_str());
            if(FontInfo) {
                int direction_return;
                int font_ascent_return, font_descent_return;
                XCharStruct overall_return;

                XTextExtents(FontInfo, m_Text.c_str(), m_Text.size(), &direction_return,
                             &font_ascent_return, &font_descent_return, &overall_return);

                int d = (m_outline)?2:0;

                *ExactWidth = overall_return.width + d;
                *ExactHeight = font_ascent_return + font_descent_return + d;
                bRes = true;

                XFreeFont(dpy, FontInfo);
            }
            XCloseDisplay(dpy);
        }
    }
    catch(...) {
    }*/

#endif
	return bRes;
}

bool CDString::SizeToContent() 
{
	bool bRes = CalcRect(&m_width, &m_height);
	if(bRes) {
		FireChange();
		SetModifiedFlag();
	}
	return bRes;
}

IDString * CDString::Clone() 
{
	CDString *pNewObject = CDString::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}

bool CDString::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	if(pPX->PX_String("Text", m_Text, "") == false)
		return false;
	if(pPX->PX_Long("Origin", (long&)m_origin, 1) == false)
		return false;
	if(pPX->PX_Long("Left", m_left, 0) == false)
		return false;
	if(pPX->PX_Long("Top", m_top, 0) == false)
		return false;
	if(pPX->PX_Long("Width", m_width, 0) == false)
		return false;
	if(pPX->PX_Long("Height", m_height, 0) == false)
		return false;
	if(pPX->PX_Long("Format", m_format, 0) == false)
		return false;
	if(pPX->PX_Long("ForeColor", m_foreColor, 0x00FFFFFF) == false)
		return false;
	if(pPX->PX_Long("BackColor", m_backColor, 0x00000000) == false)
		return false;
	if(pPX->PX_Long("BkMode", (long&)m_bkMode, 0) == false)
		return false;
	if(pPX->PX_Bool("Outline", m_outline, true) == false)
		return false;
	if(pPX->PX_Long("OutlineColor", m_outlineColor, 0x00000000) == false)
		return false;
	if(pPX->PX_Font("Font", m_Font) == false)
		return false;
    if(pPX->PX_String("FontL", m_FontL, "") == false)
        return false;

	return true;
}
