#pragma once
// CircleLineCrossPoints.h: interface for the CCircleLineCrossPoints class.
//

class CCircleLineCrossPoints  
{
public:
	CCircleLineCrossPoints();
	virtual ~CCircleLineCrossPoints();

	void Calculate(double Xa, double Ya, double Ra, double K, double B);
	void Calculate(double Xa, double Ya, double Ra, double X1, double Y1, double X2, double Y2);

	int nPoints;
	double X[2], Y[2];

protected:
	void Clear();
};
