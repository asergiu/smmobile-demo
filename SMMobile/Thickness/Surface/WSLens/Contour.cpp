// Contour.cpp : implementation file
//

#include "stdafx.h"
#include "Contour.h"
#include "ContourPoint.h"
#include "Spline.h"
#include "StringConvert.h"
#include "math.h"

/////////////////////////////////////////////////////////////////////////////
// CContour

CContour* CContour::CreateObject()
{
	CContour *pObject = new CContour;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

IContour * CContour::GetInterface()
{
	return static_cast<IContour*>(this);
}

CContour::CContour()
{
	IsFinalClear = false;
}

CContour::~CContour()
{
}

unsigned long CContour::GetCount() 
{
	return pt.size();
}

double CContour::GetCenterX() 
{
	// TODO: Add your property handler here
	double X = 0.;
	PFTPOINT ContourFloat;
	CSpline Spline;
	VContourPoint::size_type i, nCount = pt.size();

	if(nCount > 0) {
		ContourFloat = new FTPOINT[nCount];
		if(ContourFloat != NULL){
			for(i = 0; i < nCount; i++)
			{
				CObjPtr<IContourPoint> pPt = pt[i];
				if(pPt)
				{				
					ContourFloat[i].x = pPt->GetX();
					ContourFloat[i].y = pPt->GetY();
				}
			}

			if(nCount < 30)	Spline.CreateSpline(nCount, ContourFloat);
			else			Spline.DuplicateSpline(nCount, ContourFloat);

			delete [] ContourFloat;

			if(Spline.SplExists == TRUE){
				for(int ik = 0; ik < Spline.nPolyNodes; ik++){
					X += Spline.FloatPolyNodes[ik].x;
				}

				X /= (double)Spline.nPolyNodes;
			}
		}
	}

	return X;
}

double CContour::GetCenterY() 
{
	// TODO: Add your property handler here
	double Y = 0.;
	PFTPOINT ContourFloat;
	CSpline Spline;
	VContourPoint::size_type i, nCount = pt.size();

	if(nCount > 0){
		ContourFloat = new FTPOINT[nCount];
		if(ContourFloat != NULL){
			for(i = 0; i < nCount; i++){
				CObjPtr<IContourPoint> pPt = pt[i];
				if(pPt)
				{				
					ContourFloat[i].x = pPt->GetX();
					ContourFloat[i].y = pPt->GetY();
				}
			}

			if(nCount < 30)	Spline.CreateSpline(nCount, ContourFloat);
			else			Spline.DuplicateSpline(nCount, ContourFloat);

			delete [] ContourFloat;

			if(Spline.SplExists == TRUE){
				for(int ik = 0; ik < Spline.nPolyNodes; ik++){
					Y += Spline.FloatPolyNodes[ik].y;
				}

				Y /= (double)Spline.nPolyNodes;
			}
		}
	}

	return Y;
}

IContourPoint * CContour::Add(double Xvalue, double Yvalue) 
{
	VContourPoint::size_type nCount = pt.size();
	if(!TestRange(nCount+1, 1, MaxCountContourPoint, 2)) return NULL;

	CContourPoint * pNewObj = NULL;
	IContourPoint * pNewPt = NULL;

	pNewObj = CContourPoint::CreateObject();
	if(pNewObj) {
		pNewPt = pNewObj->GetInterface();
		if(pNewPt) {
			pNewPt->SetPoint(Xvalue, Yvalue);
			pt.push_back(pNewPt);

			AdviseConnection(pt[nCount], nCount);

			FireChange();
			SetModifiedFlag();
		}
		else
			pNewObj->Release();
	}
	return pNewPt;
}

IContourPoint * CContour::GetItem(unsigned long Index) 
{
	VContourPoint::size_type nCount = pt.size();
	if(!TestRange(Index, 1, nCount, 1))
		return NULL;
	if(pt[Index-1])
		pt[Index-1]->AddRef();

	return pt[Index-1];
}

bool CContour::SetItem(unsigned long Index, IContourPoint * newValue) 
{
	VContourPoint::size_type nCount = pt.size();
	if(!TestRange(Index, 1, nCount, 1))
		return false;

	if(pt[Index-1])
	{
		UnadviseConnection(/*pt[Index-1],*/ Index-1);
	}
	
	pt[Index-1] = newValue;
	
	if(pt[Index-1])
	{
		AdviseConnection(pt[Index-1], Index-1);
	}

	FireChange();
	SetModifiedFlag();
	return true;
}

bool CContour::Clear() 
{
	VContourPoint::size_type i, nCount = pt.size();
	for(i = 0; i < nCount; i++){
		UnadviseConnection(/*pt[i],*/ i);
	}

	pt.clear();

	if(!IsFinalClear){
		FireChange();
		SetModifiedFlag();
	}
	return true;
}

bool CContour::GetPoint(unsigned long index, double *x, double *y)
{
	VContourPoint::size_type nCount = pt.size();
	if(!TestRangeMin0(index, nCount-1, 1))
		return false;

	bool bRes = false;
	CObjPtr<IContourPoint> pPt = pt[index];
	if(pPt)
	{
		*x = pPt->GetX();
		*y = pPt->GetY();
		bRes = true;
	}
	return bRes;
}

void CContour::OnChildChanged() 
{
	FireChange();
	SetModifiedFlag();
}

bool CContour::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	string PropName;
	unsigned long i, nLoadCount;

	if(pPX->IsLoading())
	{
		Clear();
		if(pPX->PX_Long("Count", nLoadCount, 0) == false)
			return false;
		for(i = 0; i < nLoadCount; i++) {
			PropName = "ContourPoint" + toString(i);
//			PropName = "ContourPoint";
			CContourPoint * ptObject = CContourPoint::CreateObject();
			if(ptObject) {
				CObjPtr<IContourPoint> ptTemp;
				ptTemp.AttachDispatch(ptObject->GetInterface());
				if(PX_WSLObject<CContourPoint, IContourPoint>(pPX, PropName.c_str(), ptTemp, NULL) == false)
					return false;
				if(ptTemp)
					Add(ptTemp->GetX(), ptTemp->GetY())->Release();
			}
			else
				return false;
		}
	}
	else 
	{
		nLoadCount = GetCount();
		if(pPX->PX_Long("Count", nLoadCount) == false)
			return false;
		for(i = 0; i < nLoadCount; i++)
		{
			PropName = "ContourPoint" + toString(i);
//			PropName = "ContourPoint";
			if(PX_WSLObject<CContourPoint, IContourPoint>(pPX, PropName.c_str(), pt[i], NULL) == false)
				return false;
		}
	}

	return true;
}

IContour * CContour::Clone() 
{
	CContour *pNewObject = CContour::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}

bool CContour::Intersection(double x, double y, double angle, vector<FTPOINT>& result)
{
	if (angle < 0. || angle > 360.)
		return false;
	result.clear();
	
	double K = tan(angle * M_PI / 180.);
	double B = y - K * x;
	int i = 0;
	for (i = 0; i < GetCount(); i++) {
		double x1 = 0., x2 = 0., y1 = 0., y2 = 0.;
		if (!GetPoint(i, &x1, &y1))
			return false;
		if (i + 1 == GetCount()) {
			if (!GetPoint(0, &x2, &y2))
				return false;
			
		}
		else {
			if (!GetPoint(i + 1, &x2, &y2))
				return false;
			
		}
		double KC, BC, xi, yi;
		if (x2 - x1 == 0.) {
			if (angle == 90. || angle == 270.)
				continue;
			xi = x1;
			yi = K * xi + B;
		}
		else {
			KC = (y2 - y1) / (x2 - x1);
			BC = y1 - x1 * KC;
			if (angle == 90. || angle == 270.) {
				xi = x;
				yi = KC * xi + BC;
			}
			else {
				xi = (BC - B) / (K - KC);
				yi = K * xi + B;
			}
		}
		bool bAddPoint = false;
		bool byCheck = false;
		if (x2 > x1) {
			if (xi >= x1 && xi < x2) {
				byCheck = true;
			}
		}
		else if (x2 == x1) {
			if (xi == x1) {
				byCheck = true;
			}
		}
		else {
			if (xi > x2 && xi <= x1) {
				byCheck = true;
			}
		}
		if (byCheck) {
			if (y2 > y1) {
				if (yi >= y1 && yi < y2) {
					bAddPoint = true;
				}
			}
			else if (y2 == y1) {
				if (yi == y1) {
					bAddPoint = true;
				}
			}
			else {
				if (yi > y2 && yi <= y1) {
					bAddPoint = true;
				}
			}
		}
		if (bAddPoint) {
			FTPOINT xy;
			xy.x = xi;
			xy.y = yi;
			result.push_back(xy);
		}
	}
	return true;
}
