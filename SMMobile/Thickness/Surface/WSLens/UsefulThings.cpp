#include "stdafx.h"
#include <math.h>

#include "UsefulThings.h"

double Azimuth(double y,double x){
	double Temp=atan2(y,x);
	if(Temp>=0)
		return Temp;
	else
		return Temp+2.0*M_PI;
	}

int RoundUp(double Arg){
	return (int)floor(Arg+1.-EPS);
	}

int RoundDown(double Arg){
	return (int)floor(Arg);
	}

int Round(double Arg){
	return (int)floor(Arg+0.5);
	}

double Hypot(int a,int b){
	return sqrt((double)(a*a+b*b));
	}

RECT &CreateSquareAroundPoint(POINT& Point,int Len){
    static RECT Result = { Point.x-Len,Point.y-Len,Point.x+Len,Point.y+Len };
	return Result;
	}

RECT &Inflate(POINT& Point,int Len){
	static RECT Result = { Point.x-Len,Point.y-Len,Point.x+Len,Point.y+Len };
	return Result;
	}

RECT &Inflate(RECT& Rect,int Len){
	static RECT Result = { Rect.left-Len,Rect.top-Len,Rect.right+Len,Rect.bottom+Len };
	return Result;
	}

void TransCoord(RECT &SrcRect,RECT &DstRect,POINT &SrcPoint,POINT &DstPoint){
	DstPoint.x=Round((SrcPoint.x)*(DstRect.right-DstRect.left)/
							(double)(SrcRect.right-SrcRect.left));
	DstPoint.y=Round((SrcPoint.y)*(DstRect.bottom-DstRect.top)/
							(double)(SrcRect.bottom-SrcRect.top));
	}

void TransCoord(RECT &SrcRect,RECT &DstRect,FTPOINT &SrcPoint,FTPOINT &DstPoint){

	DstPoint.x=(SrcPoint.x-SrcRect.left)*(double)(DstRect.right-DstRect.left)/
							(double)(SrcRect.right-SrcRect.left)+DstRect.left;
	DstPoint.y=(SrcPoint.y-SrcRect.top)*(double)(DstRect.bottom-DstRect.top)/
							(double)(SrcRect.bottom-SrcRect.top)+DstRect.top;
/*
	picWnd->GetWindowRect(&RcScreen);
	CurFrameInfo.GetFrameBounds(&RcBitmap);
	x=Round(x*(RcScreen.right-RcScreen.left+1.)/(RcBitmap.right+1.));
	y=Round(y*(RcScreen.bottom-RcScreen.top+1.)/(RcBitmap.bottom+1.));

	static BOOL FirVersion=TRUE;
	static int Offset=1;
	if(FirVersion){
		DstPoint.x=(SrcPoint.x-SrcRect.left)*(double)(DstRect.right-DstRect.left)/
								(double)(SrcRect.right-SrcRect.left)+DstRect.left;
		DstPoint.y=(SrcPoint.y-SrcRect.top)*(double)(DstRect.bottom-DstRect.top)/
								(double)(SrcRect.bottom-SrcRect.top)+DstRect.top;
		}
	else{
		DstPoint.x=(SrcPoint.x-SrcRect.left)*(double)(DstRect.right-DstRect.left-2)/
								(double)(SrcRect.right-SrcRect.left)+DstRect.left+Offset;
		DstPoint.y=(SrcPoint.y-SrcRect.top)*(double)(DstRect.bottom-DstRect.top-2)/
								(double)(SrcRect.bottom-SrcRect.top)+DstRect.top+Offset;
		}
*/
/*
	DstPoint.x=Round((SrcPoint.x-SrcRect.left)*(DstRect.right-DstRect.left)/
							(double)(SrcRect.right-SrcRect.left));
	DstPoint.y=Round((SrcPoint.y-SrcRect.top)*(DstRect.bottom-DstRect.top)/
							(double)(SrcRect.bottom-SrcRect.top));
*/
/*
void ConvertPicToWndFloat (FTPOINT * WndPoint, double xPicPoint, double yPicPoint, CSize picSize)
{
extern CSize wndSize;

	WndPoint->x = ((xPicPoint * (double)(wndSize.cx-2)) / (double)picSize.cx + 1. );
	WndPoint->y = ((yPicPoint * (double)(wndSize.cy-2)) / (double)picSize.cy + 1. );
}

*/
	}

double RectS(const RECT &Rect){
	return (double)(Rect.right-Rect.left+1)*(double)(Rect.bottom-Rect.top+1);
	}

void CopyPartMatr(CByteMatrix &SourceMatrix,RECT &SourceRect,
				  CByteMatrix &DestMatrix,POINT &DestBasePoint){
	int i,SizeX=(SourceRect.right-SourceRect.left)+1,
		SizeY=(SourceRect.bottom-SourceRect.top)+1,CurYDest,CurYSource;
	for(i=SizeY,CurYDest=DestBasePoint.y,CurYSource=SourceRect.top;i;i--,CurYDest++,CurYSource++){
		memcpy(DestMatrix[CurYDest]+DestBasePoint.x,
			   SourceMatrix[CurYSource]+SourceRect.left,SizeX);		  

		}				  
	}

void CopyPartMatr(CIntMatrix &SourceMatrix,RECT &SourceRect,
				  CIntMatrix &DestMatrix,POINT &DestBasePoint){
	int i,SizeX=((SourceRect.right-SourceRect.left)+1)*sizeof(int),
		SizeY=(SourceRect.bottom-SourceRect.top)+1,CurYDest,CurYSource;
	for(i=SizeY,CurYDest=DestBasePoint.y,CurYSource=SourceRect.top;i;i--,CurYDest++,CurYSource++){
		memcpy(DestMatrix[CurYDest]+DestBasePoint.x,
			   SourceMatrix[CurYSource]+SourceRect.left,SizeX);		  

		}				  
	}

BOOL InSeRect(RECT &Src,RECT &Dst){
	if(Src.left>Dst.left){
		Dst.left=Src.left;
		}
	if(Src.right<Dst.right){
		Dst.right=Src.right;
		}
	if(Dst.right<Dst.left){
		return FALSE;
		}

	if(Src.top>Dst.top){
		Dst.top=Src.top;
		}
	if(Src.bottom<Dst.bottom){
		Dst.bottom=Src.bottom;
		}
	if(Dst.bottom<Dst.top){
		return FALSE;
		}
	return TRUE;
	}
