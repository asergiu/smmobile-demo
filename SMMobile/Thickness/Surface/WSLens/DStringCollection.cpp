// DStringCollection.cpp : implementation file
//

#include "stdafx.h"
#include "DStringCollection.h"
#include "DString.h"
#include "StringConvert.h"

/////////////////////////////////////////////////////////////////////////////
// CDStringCollection

CDStringCollection* CDStringCollection::CreateObject()
{
	CDStringCollection *pObject = new CDStringCollection;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

IDStringCollection * CDStringCollection::GetInterface()
{
	return static_cast<IDStringCollection*>(this);
}

CDStringCollection::CDStringCollection()
{
	IsFinalClear = false;
}

CDStringCollection::~CDStringCollection()
{
}

unsigned long CDStringCollection::GetCount()
{
	return pt.size();
}

IDString * CDStringCollection::GetItem(unsigned long Index)
{
	VDString::size_type nCount = pt.size();
	if(!TestRange(Index, 1, nCount, 1))
		return NULL;
	if(pt[Index-1])
		pt[Index-1]->AddRef();
	
	return pt[Index-1];
}

bool CDStringCollection::SetItem(unsigned long Index, IDString * newValue)
{
	VDString::size_type nCount = pt.size();
	if(!TestRange(Index, 1, nCount, 1))
		return false;
	
	if(pt[Index-1])
	{
		UnadviseConnection(/*pt[Index-1],*/ Index-1);
	}
	
	pt[Index-1] = newValue;
	
	if(pt[Index-1])
	{
		AdviseConnection(pt[Index-1], Index-1);
	}

	FireChange();
	SetModifiedFlag();
	return true;
}

bool CDStringCollection::Clear() 
{
	VDString::size_type i, nCount = pt.size();
	for(i = 0; i < nCount; i++){
		UnadviseConnection(/*pt[i],*/ i);
	}

	pt.clear();

	if(!IsFinalClear){
		FireChange();
		SetModifiedFlag();
	}
	return true;
}

bool CDStringCollection::Add(IDString * newDString)
{
	if(newDString) {
		vector< CObjPtr<IDString> >::size_type nCount = pt.size(); 
		if(nCount >= Max_DString_Count)
			return false;

		pt.push_back(newDString);

		if(newDString)
		{
			AdviseConnection(pt[nCount], nCount);
		}

		FireChange();
		SetModifiedFlag();
		return true;
	}
	return false;
}

void CDStringCollection::OnChildChanged() 
{
	FireChange();
	SetModifiedFlag();
}

bool CDStringCollection::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	string PropName;
	unsigned long i, nLoadCount;

	if(pPX->IsLoading()){
		Clear();
		if(pPX->PX_Long("Count", nLoadCount, 0) == false)
			return false;
		for(i = 0; i < nLoadCount; i++) {
			PropName = "DString" + toString(i);
			CDString * ptObject = CDString::CreateObject();
			if(ptObject) {
				CObjPtr<IDString> ptTemp;
				ptTemp.AttachDispatch(ptObject->GetInterface());
				if(PX_WSLObject<CDString, IDString>(pPX, PropName.c_str(), ptTemp, NULL) == false)
					return false;
				if(ptTemp)
					Add(ptTemp);
			}
			else
				return false;
		}
	}
	else {
		nLoadCount = GetCount();
		if(pPX->PX_Long("Count", nLoadCount) == false)
			return false;
		for(i = 0; i < nLoadCount; i++) {
			PropName = "DString" + toString(i);
			if(PX_WSLObject<CDString, IDString>(pPX, PropName.c_str(), pt[i], NULL) == false)
				return false;
		}
	}

	return true;
}

IDStringCollection * CDStringCollection::Clone() 
{
	CDStringCollection *pNewObject = CDStringCollection::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}
