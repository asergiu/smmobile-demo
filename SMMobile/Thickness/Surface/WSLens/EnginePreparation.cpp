#include "stdafx.h"
#include "WSLensEngine.h"
#include "Parameters/Asph2.h"
#include <float.h>
#include "LensSurface.h"


bool DecreasePointsNumber(	int InSize, double InX[], double InY[],
							int OutSize, double OutX[], double OutY[]);
int comp_x(const void *a,const void *b);
#define N	80//70 FTPOINT

bool CWSLensEngine::PrepareLensData(ILens * LensObj)
{
	SetLastErrorCode(0);

	double * xcContour = NULL, * ycContour = NULL;
	double * Xb = NULL, * Yb = NULL;
	static int N_points_side = 32;

	double PrevOffsX = 0., PrevOffsY = 0., CurOffsX = 0., CurOffsY = 0.,
			PrevCenterThickness, PosError = 0.1, InitD;
//    double ThicknessError = 0.1;
	bool TimeToExit = true, FirstTime = true;
	int Attempts = 0, MaxAttempts = 20;

	CObjPtr<IContour> ContourObj;
	ContourObj.AttachDispatch(LensObj->GetContour());
	int m_ContourCount = ContourObj->GetCount();

	LensModel.fRayTracerPrepared = false;
	m_LeftSide = LensObj->GetSide();
	m_Sph = LensObj->GetSph();
	m_Cyl = LensObj->GetCyl();
	m_Add = LensObj->GetAddit();
	m_Axis = LensObj->GetAxis();
	if(m_LeftSide)
		m_Axis = 180 - m_Axis;

	m_RefIndex = LensObj->GetRefIndex();
	m_Density = LensObj->GetDensity();
	m_Design = ConvertToWSDesign(LensObj->GetDesign().c_str());
	if(m_Design == -1){
		SetLastErrorCode(18);
		goto Error;
	}
	m_KindOfMaterial = LensObj->GetKindOfMaterial();
	m_CenterX = LensObj->GetCenterX(); 
	m_CenterY = LensObj->GetCenterY(); 
	m_OptToGeomCenterDisplacementX = LensObj->GetOptToGeomCenterDisplacementX();
	m_OptToGeomCenterDisplacementY = LensObj->GetOptToGeomCenterDisplacementY();

	if(wsProgressive == LensObj->GetMultifocalType())
	{

		CObjPtr<IProgressive> ProgrObj;
		ProgrObj.AttachDispatch(LensObj->GetProgressive());
		if(ProgrObj.m_p)
		{
			m_OptToGeomCenterDisplacementX = -ProgrObj->GetFarVisionCenterX();
			m_OptToGeomCenterDisplacementY = -ProgrObj->GetFarVisionCenterY() + 0.5 * ProgrObj->GetFarVisionMarkerDiam();
		}
	}	
	m_BlankCenterX = m_CenterX + m_OptToGeomCenterDisplacementX;
	m_BlankCenterY = m_CenterY + m_OptToGeomCenterDisplacementY;

	m_Prism = LensObj->GetPrism();
	m_PrismAxis = LensObj->GetPrismAxis();
	//  Further there are 6 lines which give the chance to set a prism axis on �Tabo - Schema�,
	// accepted in Germany.
	// left-lens: 0� is temporal, 90� top, 180� nasal, 270� down.
	// right-lens: 0� is nasal, 90� top, 180� temporal, 270� down.
	//  Changes are made Ed - 5 November 2003.
	if(!m_LeftSide)
	{
		m_PrismAxis = 180 - m_PrismAxis;
		if(m_PrismAxis < 0)
			m_PrismAxis += 360;
	}

	m_Diameter = LensObj->GetDiameter();
	m_EllipticVertToHorizRatio = LensObj->GetEllipticVertToHorizRatio();
	
	if(m_EllipticVertToHorizRatio > 10.)
	{
		double NewEllipseHorizMinusVert = 100. - m_EllipticVertToHorizRatio;
		m_EllipticVertToHorizRatio = (m_Diameter - NewEllipseHorizMinusVert) / m_Diameter;
	}

	m_EllipticBlank = LensObj->GetEllipticBlank();
	if(!m_EllipticBlank)
	{
		m_EllipticVertToHorizRatio = 1.;	
	}
	
	m_PermittedCenterThickness = LensObj->GetPermittedCenterThickness();
	m_PermittedEdgeThickness = LensObj->GetPermittedEdgeThickness();
	m_PermittedZeroThickness = LensObj->GetPermittedZeroThickness();


	if(m_ContourCount < 3 || m_ContourCount > 1000 ){
		SetLastErrorCode(12);
		goto Error;
	}
	if(m_Sph < -20. || m_Sph > 20. ||
			m_Cyl < -10. || m_Cyl > 10. ||
			m_Axis < 0 || m_Axis > 180 ||
			m_Add < 0. || m_Add > 4. ||
			m_Prism < 0. || m_Prism > 25. ||
			m_PrismAxis < 0. || m_PrismAxis > 360. ||
			m_EllipticVertToHorizRatio < 0. || m_EllipticVertToHorizRatio > 150.){
		SetLastErrorCode(19);
		goto Error;
	}
	if(m_RefIndex < 1. || m_RefIndex > 2.){
		SetLastErrorCode(19);
		goto Error;
	}
	if(m_Density < 0.2 || m_Density > 10.){
		SetLastErrorCode(19);
		goto Error;
	}
	m_PrismAxis *= M_PI/180.;
	double Rmax, RMaxAroundOpticalCenter;
//	static lens_inf Lns_f;

	xcContour = (double *)malloc(m_ContourCount * sizeof(double));
	ycContour = (double *)malloc(m_ContourCount * sizeof(double));
	if(xcContour == NULL || ycContour == NULL){
		SetLastErrorCode(20);
		goto Error;
	}

	int i;
//	double x, y;
	
//	TempVal = ContourObj.GetItem(1);
//	TempVal->Release();
	for(i = 0; i < m_ContourCount; i++){
//		pContour->GetPoint(i, &x, &y);
		CObjPtr<IContourPoint> CurContPointObj;
		CurContPointObj.AttachDispatch(ContourObj->GetItem(i + 1));
//		((CContour *)CCmdTargetPersist::FromIDispatch(lpdispContour))->GetPoint(i, &x, &y);
		xcContour[i] = CurContPointObj->GetX() - m_CenterX;
		ycContour[i] = CurContPointObj->GetY() - m_CenterY;
	}
//	goto Error;
	Xb = (double *)calloc(N_points_side, sizeof(double));
	Yb = (double *)calloc(N_points_side, sizeof(double));
	if(Xb == NULL || Yb == NULL){
		SetLastErrorCode(20);
		goto Error;
	}

	if(!DecreasePointsNumber(m_ContourCount, xcContour, ycContour, N_points_side, Xb, Yb)){
		SetLastErrorCode(20);
		goto Error;
	}

	for(i = 0; i < N_points_side; i++){
		Xb[i] /= 10.;
		Yb[i] /= 10.;
	}

	int iForm, Quit;
	if(m_Design == 0) iForm = 0;
	else{
		double Shell2Par[160];
		LensModel.DefineShell2Par(Shell2Par);

		iForm = LensModel.as_len_mark(m_Design, m_KindOfMaterial, m_RefIndex, m_Sph);
		while ( 1 ) {
			Quit = LensModel.set_par(iForm, m_Sph, m_Cyl,
						-m_Axis*M_PI/180., 40, 1, 1, 1, 1);
			if(Quit == 0 && iForm == 0){iForm = 2;  continue ;}
			break ;
		}
	}

	LensModel.lns = &Lns_f;
	LensModel.RearSurfOffsetX = 0.;
	LensModel.RearSurfOffsetY = 0.;

	Rmax = MaxContR(Xb, Yb, N_points_side, m_OptToGeomCenterDisplacementX * 0.1, m_OptToGeomCenterDisplacementY * 0.1,
					m_EllipticVertToHorizRatio)*10.;
	RMaxAroundOpticalCenter = MaxContR(Xb, Yb, N_points_side, 0., 0., 1.) * 10.;// + _hypot(m_OptToGeomCenterDisplacementX, m_OptToGeomCenterDisplacementY);
	if(RMaxAroundOpticalCenter > 100){
		SetLastErrorCode(19);
		goto Error;
	}
//goto Error;
	m_MinDiam = Rmax * 2.;
	if(m_Diameter >= m_MinDiam)
		Rmax = m_Diameter / 2.;
	else
	{
		SetLastErrorCode(12);
		goto Error;
	}
	ThicknessStruct Thickness;
	Thickness.CenMin		= m_PermittedCenterThickness;
	Thickness.EdgeMin		= m_PermittedEdgeThickness;
	Thickness.IntegralMin	= m_PermittedZeroThickness;


	if(m_Design) 
		Quit = LensModel.set_par(m_RefIndex, m_KindOfMaterial, Xb, Yb, N_points_side,
			  	iForm, m_Sph, m_Cyl,
				m_Axis/57.3, (int)RMaxAroundOpticalCenter, 1.f, 1.f, 0.f, 0.f, &Thickness);
	else 
		Quit = LensModel.set_par(m_RefIndex, m_KindOfMaterial, Xb, Yb, N_points_side,
			  	SPHERIC, m_Sph, m_Cyl,
				m_Axis/57.3, (int)RMaxAroundOpticalCenter, 1.f, 1.f, 0.f, 0.f, &Thickness);
	if(Quit == 0){
		SetLastErrorCode(19);
		goto Error;
	}

	for(i = 0; i < N_points_side; i++){
		Xb[i] *= 10.; 
		Yb[i] *= 10.; 
	}

//	goto Error;
	if(m_Prism > 0)
	{
		double x = m_Prism, y = 0., z = -100.;
//		double x = tan(m_Prism * 0.01) * 100., y = 0., z = -100.;
		y = x * sin(m_PrismAxis);
		x = x * cos(m_PrismAxis); 
		LensModel.ReferVect.vx = x;
		LensModel.ReferVect.vy = y;
		LensModel.ReferVect.vz = z;
		LensModel.PrecParam = M_PI / 1800.;// ����������� � 0.1 �������!!!
	}
	InitD = LensModel.lns->_d;
	do{
		LensModel.lns->_d = InitD;
		if(!asph_weight_m(Xb, Yb, N_points_side, 0.f, 0.f, 
						   &m_MinEdgeThickness, &m_MaxEdgeThickness,
						   &m_CenterThickness, m_Density, &m_Weight, Rmax))//,
//						   m_OptToGeomCenterDisplacementX, m_OptToGeomCenterDisplacementY,
//						   m_EllipticVertToHorizRatio))//RMaxAroundOpticalCenter
		{
			SetLastErrorCode(11);
			goto Error;
		}

		if(m_Prism > 0)
		{
			LensModel.fRayTracerPrepared = LensModel.RayTracer.PrepareByProducer(&LensModel, Rmax * 2., m_RefIndex,
						m_OptToGeomCenterDisplacementX, m_OptToGeomCenterDisplacementY, m_EllipticVertToHorizRatio);
			double rX, rY;
			CurOffsX = 0.;
			CurOffsY = 0.;
			EOptimizationResult Res = CFuncOptimizer::FindSolution(&LensModel, CurOffsX, CurOffsY, &rX, &rY, M_PI / 360.);
			if(Res != orSuccess)
			{
				SetLastErrorCode(19);
				goto Error;
			}
			CurOffsX = rX;
			CurOffsY = rY;
			
			if(!FirstTime)
			{
				if(_hypot(CurOffsX, CurOffsY) < PosError)
					TimeToExit = TRUE;
				if(Attempts++ == MaxAttempts)
				{
					SetLastErrorCode(19);
					goto Error;
				}
			}
			else
			{
				FirstTime = FALSE;
				TimeToExit = _hypot(CurOffsX, CurOffsY) < PosError;
			}
			LensModel.RearSurfOffsetX += CurOffsX;
			LensModel.RearSurfOffsetY += CurOffsY;
			PrevOffsX = CurOffsX;
			PrevOffsY = CurOffsY;
			PrevCenterThickness = m_CenterThickness;
		}
	}while(!TimeToExit);
/*
	SetError(0);

Error:
*/
Error:
	if(xcContour != NULL) free(xcContour);
	if(ycContour != NULL) free(ycContour);
	if(Xb != NULL) free(Xb);
	if(Yb != NULL) free(Yb);
	if(GetLastErrorCode())
//		ThrowExceptionWithKnownErrorCode();
		return false;
	else
		return true;
}
/*
double CWSLensEngine::ThicknessesByExternalSurface()
{
	
}
*/

typedef struct {
	int orj_num;
	double x;
	double y;
	} _point_xy;


bool CWSLensEngine::ThicknessAndDiamByExternalSurface(ILens * LensObj,
			ILensEngine * LensEngine)
{

	CObjPtr<ILensSurface> Grid;
	double *xcContour = NULL, *ycContour = NULL, *Xb = NULL, *Yb = NULL;
	const int N_points_side = 100;//32
	CObjPtr<IContour> ContourObj;
	ContourObj.AttachDispatch(LensObj->GetContour());
	int i, m_ContourCount = ContourObj->GetCount();

    try {
        if(m_ContourCount < 3 || m_ContourCount > 1000 ){
            throw 12;
        }

        xcContour = (double *)malloc(m_ContourCount * sizeof(double));
        ycContour = (double *)malloc(m_ContourCount * sizeof(double));
        if(xcContour == NULL || ycContour == NULL){
            throw 9;
        }
        for(i = 0; i < m_ContourCount; i++){
    //		pContour->GetPoint(i, &x, &y);
            CObjPtr<IContourPoint> CurContPointObj;
            CurContPointObj.AttachDispatch(ContourObj->GetItem(i + 1));
    //		((CContour *)CCmdTargetPersist::FromIDispatch(lpdispContour))->GetPoint(i, &x, &y);
            xcContour[i] = CurContPointObj->GetX();// - m_CenterX
            ycContour[i] = CurContPointObj->GetY();// - m_CenterY
        }
        Xb = (double *)calloc(N_points_side, sizeof(double));
        Yb = (double *)calloc(N_points_side, sizeof(double));
        if(Xb == NULL || Yb == NULL){
            throw 9;
        }

        if(!DecreasePointsNumber(m_ContourCount, xcContour, ycContour, N_points_side, Xb, Yb)){
            throw 9;
        }
        m_MinDiam = MaxContR(Xb, Yb, N_points_side, m_BlankCenterX, m_BlankCenterY, m_EllipticVertToHorizRatio) * 2.;

        CLensSurface * pLensSurfaceObj = CLensSurface::CreateObject();
        if(!pLensSurfaceObj){
            throw 8;
        }
        Grid.AttachDispatch(pLensSurfaceObj->GetInterface());

        Grid->Add(m_CenterX, m_CenterY, 0., 0.);
        for(i = 0; i < N_points_side; i++)
        {
            Grid->Add(Xb[i], Yb[i], 0, 0);
        }

        bool ResCS = LensEngine->CalcSurface(LensObj, Grid);
        if(!ResCS)
        {
            throw 11;
        }

        double ResX, ResY, ResZf, ResZr, Diff;
        Grid->GetItem(1, &ResX, &ResY, &ResZr, &ResZf);
        if(ResZf < ResZr)
        {
            throw 0;
        }
        m_CenterThickness = ResZf - ResZr;
        for(i = 0;  i < N_points_side; i++)
        {
            Grid->GetItem(2 + i, &ResX, &ResY, &ResZr, &ResZf);
            if(ResZf < ResZr)
            {
                throw 21;
            }
            Diff = ResZf - ResZr;
            if(i == 0)
            {
                m_MinEdgeThickness = m_MaxEdgeThickness = Diff;
            }
            else
            {
                if(Diff < m_MinEdgeThickness)
                    m_MinEdgeThickness = Diff;

                else
                {
                    if(Diff > m_MaxEdgeThickness)
                        m_MaxEdgeThickness = Diff;

                }
            }
        }
    }
    catch(int nError)
    {
        SetLastErrorCode(nError);
    }

	if(xcContour)
	{
		free(xcContour);
		xcContour = NULL;
	}
	if(ycContour)
	{
		free(ycContour);
		ycContour = NULL;
	}
	
	if(Xb)
	{
		free(Xb);
		Xb = NULL;
	}
	if(Yb)
	{
		free(Yb);
		Yb = NULL;
	}

	
	if(GetLastErrorCode())
		return false;
	else
		return true;

}

bool CWSLensEngine::WeightByExternalSurface(ILens * LensObj,
					ILensEngine * LensEngine)
{
	SetLastErrorCode(0);
	int i,j,k,l;
	double v=0,x_min,x_max,y_min,y_max,
		d_x,d_y,a,b,c,d,e,S=0;////,g
//    double f=1000;
	int j0,j1,uc_seg=-1,dc_seg=-1;////,nearcen;
	double c_x,c_y;
    double _x0 = LensObj->GetCenterX(), _y0 = LensObj->GetCenterY();
	CObjPtr<ILensSurface> Grid;

//	#define M1 50
	int	*up_cont, *down_cont;				 
	_point_xy *points;

	CObjPtr<IContour> ContourObj;
	ContourObj.AttachDispatch(LensObj->GetContour());
	int m_ContourCount = ContourObj->GetCount();

    try {
        if(m_ContourCount < 3 || m_ContourCount > 1000 ){
            throw 12;
        }
        up_cont = (int*)calloc(m_ContourCount + 1, sizeof(long));
        if(!up_cont)
        {
            throw 9;
        }
        down_cont = (int*)calloc(m_ContourCount + 1, sizeof(long));
        if(!down_cont)
        {
            throw 9;
        }
        points = (_point_xy*)calloc(m_ContourCount, sizeof(_point_xy));
        if(!points)
        {
            throw 9;
        }

        for(i=0;i<m_ContourCount;i++)
        {
            CObjPtr<IContourPoint> CurContPointObj;
            CurContPointObj.AttachDispatch(ContourObj->GetItem(i + 1));

            c=points[i].x=CurContPointObj->GetX();//-_x0;
            d=points[i].y=CurContPointObj->GetY();//-_y0;
            if(i==0){
                x_min=x_max=c;
                y_min=y_max=d;
                }
            else{
                if(c<x_min){
                    x_min=c;
                    }
                else if(c>x_max){
                    x_max=c;
                    }

                if(d<y_min){
                    y_min=d;
                    }
                else if(d>y_max){
                    y_max=d;
                    }
                }
            }
        qsort(points,m_ContourCount,sizeof(_point_xy),comp_x);
        b=(points[m_ContourCount-1].y-points[0].y)/(x_max-x_min);
        a=points[0].y-b*points[0].x;
        for(l=0;fabs(points[l+1].x-points[0].x)<EPS;l++)
            ;

        for(i=k=j=0;i<m_ContourCount;i++){
            if(points[i].y<=a+b*points[i].x+EPS && (i==0||points[i].x-points[i-1].x>EPS))
                down_cont[j++]=i;
            if(i>=l&&points[i].y>=a+b*points[i].x-EPS)
                up_cont[k++]=i;
            }

        d_x=x_max-x_min;
        k=N;//ceil(d_x/DELTA-EPS);
        d_x/=k;
        d_y=y_max-y_min;
        l=N;//ceil(d_y/DELTA-EPS);
        d_y/=l;

        CLensSurface * pLensSurfaceObj = CLensSurface::CreateObject();
        if(!pLensSurfaceObj){
            throw 8;
        }
        Grid.AttachDispatch(pLensSurfaceObj->GetInterface());

        for(i=0,c_x=x_min;i<=k;i++,c_x+=d_x){
            if(c_x+EPS>=points[down_cont[dc_seg+1]].x){
                do{
                    dc_seg++;
                }while(down_cont[dc_seg+1]!=0&&c_x+EPS>=points[down_cont[dc_seg+1]].x);
                if(i==k)
                    b=0;
                else
                    b=(points[down_cont[dc_seg+1]].y-points[down_cont[dc_seg]].y)/
                        (points[down_cont[dc_seg+1]].x-points[down_cont[dc_seg]].x);
                a=points[down_cont[dc_seg]].y-b*points[down_cont[dc_seg]].x;
                }
            if(c_x+EPS>=points[up_cont[uc_seg+1]].x){
                do{
                    uc_seg++;
                }while(up_cont[uc_seg+1]!=0&&c_x+EPS>=points[up_cont[uc_seg+1]].x);
                if(i==k)
                    d=0;
                else
                    d=(points[up_cont[uc_seg+1]].y-points[up_cont[uc_seg]].y)/
                        (points[up_cont[uc_seg+1]].x-points[up_cont[uc_seg]].x);
                c=points[up_cont[uc_seg]].y-d*points[up_cont[uc_seg]].x;
                }
            j0=(int)ceil((a+b*c_x-y_min)/d_y+EPS);//// -----
            j1=(int)floor((c+d*c_x-y_min)/d_y+EPS);
            if(j1>=j0){
                for(j=j0,c_y=y_min+j0*d_y;j<=j1;j++,c_y+=d_y){

                    Grid->Add(c_x,c_y, 0., 0.);

                    S+=1;
                    }
                }
            }

        bool ResCS = LensEngine->CalcSurface(LensObj, Grid);
        if(!ResCS)
        {
            throw 11;
        }

        for(i = Grid->GetCount(); i >= 1; i--)
        {
            double tempX, tempY, tempZf, tempZr;
            Grid->GetItem(i, &tempX, &tempY, &tempZr, &tempZf);
            if(tempZf < tempZr)
            {
                throw 21;
            }
            e = tempZf - tempZr;
            v+=e;
        }
        double ddTemp;
        ddTemp = v*d_x*d_y;
        m_Weight = v*d_x*d_y*(LensObj->GetDensity())/1000.;
    }
    catch(int nError)
    {
        SetLastErrorCode(nError);
    }

	if(up_cont)
	{
		free(up_cont);
		up_cont = NULL;
	}
	if(down_cont)
	{
		free(down_cont);
		down_cont = NULL;
	}
	
	if(points)
	{
		free(points);
		points = NULL;
	}


	if(GetLastErrorCode())
		return false;
	else
		return true;

}


bool CWSLensEngine::asph_weight_m(double *x,double *y,int n_p,double _x0,double _y0,
				   double *th_min, double *th_max, double *th_cen,double density,
				   double *ComputedWeight, double BlankR)//,
{
	bool ErrorOccured = false;
	int i,j,k,l;
	double v=0,x_min,x_max,y_min,y_max,
		d_x,d_y,a,b,c,d,e,f=1000,S=0, BlankS = 0., d_x_Blank, d_y_Blank,
		BlankMinEdgeThickness = 1000., BlankMinCenThickness = 1000.;
	int j0,j1,uc_seg=-1,dc_seg=-1,nearcen;
	double c_x,c_y, LensBlankEdgeMin, LensBlankIntegral = 0., Temp;
////	BOOL BlankThinNearCen;g,

	*ComputedWeight = 0;
	*th_cen=LensModel.z_max(0,0)-LensModel.z__min(0,0);//lns->_d*lns->z_coeff;//
	*th_min=1000;
	*th_max=-1000;
	for(i = 0; i < N; i++)
	{
		double TestX = m_OptToGeomCenterDisplacementX + BlankR * cos(2 * M_PI * double(i) / double(N)),
			   TestY = m_OptToGeomCenterDisplacementY + 
						m_EllipticVertToHorizRatio * BlankR * sin(2 * M_PI * double(i) / double(N)),

			   CurThick = LensModel.z_max(TestX, TestY)-LensModel.z__min(TestX, TestY);
		if(i == 0 || LensBlankEdgeMin > CurThick)
		{
			LensBlankEdgeMin = CurThick;
		}
	}

	
	int	*up_cont, *down_cont;				 
	_point_xy *points;

	int m_ContourCount = n_p;//((CContour *)CCmdTargetPersist::FromIDispatch(lpdispContour))->GetCount();

	if(m_ContourCount < 3 || m_ContourCount > 1000 ){
		ErrorOccured = true;
		goto Error;
	}
	up_cont = (int*)calloc(m_ContourCount + 1, sizeof(long));
	if(!up_cont)
	{
		ErrorOccured = true;
		goto Error;
	}
	down_cont = (int*)calloc(m_ContourCount + 1, sizeof(long));
	if(!down_cont)
	{
		ErrorOccured = true;
		goto Error;
	}
	points = (_point_xy*)calloc(m_ContourCount, sizeof(_point_xy));
	if(!points)
	{
		ErrorOccured = true;
		goto Error;
	}
	
/*
	for(i=0;i<m_ContourCount+1;i++){
		up_cont[i]=down_cont[i]=0;
		}
*/
	for(i=0;i<n_p;i++){
		c=points[i].x=x[i]-_x0;
		d=points[i].y=y[i]-_y0;
		e=LensModel.z_max(c,d)-LensModel.z__min(c,d);
		if(e<*th_min)
			*th_min=e;
		if(e>*th_max)
			*th_max=e;
		if(i==0){
			x_min=x_max=c;
			y_min=y_max=d;
			}
		else{
			if(c<x_min){
				x_min=c;
				}
			else if(c>x_max){
				x_max=c;
				}

			if(d<y_min){
				y_min=d;
				}
			else if(d>y_max){
				y_max=d;
				}
			}
		}
	qsort(points,n_p,sizeof(_point_xy),comp_x);
	b=(points[n_p-1].y-points[0].y)/(x_max-x_min);
	a=points[0].y-b*points[0].x;
	for(l=0;fabs(points[l+1].x-points[0].x)<EPS;l++)
		;

	for(i=k=j=0;i<n_p;i++){
		if(points[i].y<=a+b*points[i].x+EPS && (i==0||points[i].x-points[i-1].x>EPS))
			down_cont[j++]=i;
		if(i>=l&&points[i].y>=a+b*points[i].x-EPS)
			up_cont[k++]=i;
		}

	d_x=x_max-x_min;
	k=N;//ceil(d_x/DELTA-EPS);
	d_x/=k;
	d_y=y_max-y_min;
	l=N;//ceil(d_y/DELTA-EPS);
	d_y/=l;

	for(i=0,c_x=x_min;i<=k;i++,c_x+=d_x){
		if(c_x+EPS>=points[down_cont[dc_seg+1]].x){
			do{
				dc_seg++;
			}while(down_cont[dc_seg+1]!=0&&c_x+EPS>=points[down_cont[dc_seg+1]].x);
			if(i==k)
				b=0;
			else
				b=(points[down_cont[dc_seg+1]].y-points[down_cont[dc_seg]].y)/
					(points[down_cont[dc_seg+1]].x-points[down_cont[dc_seg]].x);
			a=points[down_cont[dc_seg]].y-b*points[down_cont[dc_seg]].x;
			}
		if(c_x+EPS>=points[up_cont[uc_seg+1]].x){
			do{
				uc_seg++;
			}while(up_cont[uc_seg+1]!=0&&c_x+EPS>=points[up_cont[uc_seg+1]].x);
			if(i==k)
				d=0;
			else
				d=(points[up_cont[uc_seg+1]].y-points[up_cont[uc_seg]].y)/
					(points[up_cont[uc_seg+1]].x-points[up_cont[uc_seg]].x);
			c=points[up_cont[uc_seg]].y-d*points[up_cont[uc_seg]].x;
			}
		j0=int(ceil((a+b*c_x-y_min)/d_y-EPS));
		j1=int(floor((c+d*c_x-y_min)/d_y+EPS));
		if(j1>=j0){
			for(j=j0,c_y=y_min+j0*d_y;j<=j1;j++,c_y+=d_y){
				e=LensModel.z_max(c_x,c_y)-LensModel.z__min(c_x,c_y);
				v+=e;
				S+=1;
				if(e<f){
					f=e;
					if(c_x*c_x+c_y*c_y<10){
						nearcen=1;
						}
					else{
						nearcen=0;
						}
					}
				}
			}
		}

	d_x_Blank = 2. * BlankR;
	k=N;//ceil(d_x/DELTA-EPS);
	d_x_Blank /=k;
	d_y_Blank = 2. * BlankR * m_EllipticVertToHorizRatio;
	l=N;//ceil(d_y/DELTA-EPS);
	d_y_Blank /=l;
//	double c_x_2, c_y_2, d_x_Blank_2, d_y_Blank_2, _c_d_x, 2_c_d_y;
	//, c_x_2 = c_x * c_x, 2_c_d_x = 2. * c_x * d_x_Blank

	for(i=0,c_x= - BlankR;
			i<=k;i++, c_x+=d_x_Blank ){
		Temp = sqrt(fabs(BlankR * BlankR - c_x * c_x)) * m_EllipticVertToHorizRatio;
		j0=int(ceil((-Temp + BlankR * m_EllipticVertToHorizRatio)/d_y_Blank +EPS));
		j1=int(floor((Temp + BlankR * m_EllipticVertToHorizRatio)/d_y_Blank +EPS));
		if(j1>=j0){
			for(j=j0,c_y= -BlankR * m_EllipticVertToHorizRatio + j0*d_y_Blank ;j<=j1;j++,c_y+=d_y_Blank ){
				double cx = m_OptToGeomCenterDisplacementX + c_x,
						cy = m_OptToGeomCenterDisplacementY + c_y;
				e=LensModel.z_max(cx,cy)-LensModel.z__min(cx,cy);
				LensBlankIntegral += e;
				BlankS += 1.;
				if(cx*cx+cy*cy<10.)
				{
					if(e < BlankMinCenThickness)
					{
						BlankMinCenThickness = e;
					}
				}
				else
				{
					if(e < BlankMinEdgeThickness)
					{
						BlankMinEdgeThickness = e;
					}
				}
				

			
				}
			}
		}
		
	LensBlankIntegral /= BlankS;
	double FirstCorrection, DiffCen, DiffEdge, SecondCorrection, FullCorrection;
	if(BlankMinEdgeThickness < LensBlankEdgeMin)
	{
		LensBlankEdgeMin = BlankMinEdgeThickness;
	}
	DiffCen = LensModel.lns->Thickness.CenMin - BlankMinCenThickness ;
	DiffEdge = LensModel.lns->Thickness.EdgeMin - LensBlankEdgeMin;//BlankMinEdgeThickness;
	if(DiffCen > DiffEdge)
	{
		FirstCorrection = DiffCen;
	}
	else
	{
		FirstCorrection = DiffEdge;
	}
/*
	if(*th_cen < LensBlankEdgeMin)
	{
		nearcen = 1;
		f = *th_cen;
	}
	else
	{
		nearcen = 0;
		f = LensBlankEdgeMin;
	}
	b=lns->_d;
*/
	LensModel.lns->_d += FirstCorrection/(LensModel.lns->z_coeff);

	LensBlankIntegral += FirstCorrection;
	SecondCorrection = LensModel.lns->Thickness.IntegralMin - LensBlankIntegral;

	if(SecondCorrection > 0.)
	{
		LensModel.lns->_d += SecondCorrection/(LensModel.lns->z_coeff);
	}
	else
	{
		SecondCorrection = 0.;
	}
	FullCorrection = FirstCorrection + SecondCorrection;
	*th_min+=FullCorrection;
	*th_cen+=FullCorrection;
	*th_max+=FullCorrection;
	*ComputedWeight = (v + FullCorrection * S) * d_x * d_y * density / 1000.;//[_type]
Error:
	if(up_cont)
	{
		free(up_cont);
		up_cont = NULL;
	}
	if(down_cont)
	{
		free(down_cont);
		down_cont = NULL;
	}
	
	if(points)
	{
		free(points);
		points = NULL;
	}


	if(ErrorOccured)
		return false;
	else
		return true;


}

double CWSLensEngine::MaxContR(double *x,double *y,int np,double x0, double y0, double EllipticVertToHorizRatio)
{
	int i;
	double r,r_max=0;
	for(i=0;i<np;i++){
		double	OffsX = x[i]-x0,
				OffsY = (y[i]-y0)/EllipticVertToHorizRatio;
		r=_hypot(OffsX, OffsY);
		if(r>r_max)
			r_max=r;
		}
	return r_max;
}
