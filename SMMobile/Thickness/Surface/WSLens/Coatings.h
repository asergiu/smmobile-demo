#pragma once
// Coatings.h : header file
//

#include "ObjectBase.h"
#include "Coating.h"
#include <string>
using std::string;

/////////////////////////////////////////////////////////////////////////////
// CCoatings

class CEventImpl_UVProtection : public CObjEventSimpleImpl<ICoating, ICoatingEvents>
{
	void CoatingChanged()
	{
		CoatingChanged_UVProtection();
	}
	virtual void CoatingChanged_UVProtection() = 0;
};

class CEventImpl_ScratchResistance : public CObjEventSimpleImpl<ICoating, ICoatingEvents>
{
	void CoatingChanged()
	{
		CoatingChanged_ScratchResistance();
	}
	virtual void CoatingChanged_ScratchResistance() = 0;
};

class CEventImpl_WaterRepellent : public CObjEventSimpleImpl<ICoating, ICoatingEvents>
{
	void CoatingChanged()
	{
		CoatingChanged_WaterRepellent();
	}
	virtual void CoatingChanged_WaterRepellent() = 0;
};

class CEventImpl_OtherCoating : public CObjEventSimpleImpl<ICoating, ICoatingEvents>
{
	void CoatingChanged()
	{
		CoatingChanged_OtherCoating();
	}
	virtual void CoatingChanged_OtherCoating() = 0;
};

class CCoatings :
	public CObjectEventBase<ICoatings, ICoatingsEvents>,
	public CEventImpl_UVProtection,
	public CEventImpl_ScratchResistance,
	public CEventImpl_WaterRepellent,
	public CEventImpl_OtherCoating
{
public:
    static CCoatings* CreateObject();
	ICoatings * GetInterface();

protected:
	CCoatings();           // protected constructor used by dynamic creation

// Attributes
	string m_Name, m_EDPCode, m_Info;
	CObjPtr<ICoating> lpdispUVProtection, lpdispScratchResistance,
		lpdispWaterRepellent, lpdispOtherCoating;


public:
	void CoatingChanged_UVProtection()
	{
		OnChildChanged();
	}
	void CoatingChanged_ScratchResistance()
	{
		OnChildChanged();
	}
	void CoatingChanged_WaterRepellent()
	{
		OnChildChanged();
	}
	void CoatingChanged_OtherCoating()
	{
		OnChildChanged();
	}

// Operations
public:

	string GetName();
	bool SetName(LPCTSTR);
	string GetEDPCode();
	bool SetEDPCode(LPCTSTR);
	string GetInfo();
	bool SetInfo(LPCTSTR);
	ICoating * GetUVProtection();
	bool SetUVProtection(ICoating *);
	ICoating * GetScratchResistance();
	bool SetScratchResistance(ICoating *);
	ICoating * GetWaterRepellent();
	bool SetWaterRepellent(ICoating *);
	ICoating * GetOtherCoating();
	bool SetOtherCoating(ICoating *);
	ICoatings * Clone();

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);

protected:

	virtual ~CCoatings();

	virtual void FinalRelease()
	{
		delete this;
	}

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->CoatingsChanged();
			}
		}
	}

protected:
	void OnChildChanged();
};
