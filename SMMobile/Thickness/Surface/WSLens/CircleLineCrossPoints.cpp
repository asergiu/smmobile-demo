// CircleLineCrossPoints.cpp: implementation of the CCircleLineCrossPoints class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CircleLineCrossPoints.h"
#include <math.h>
#include "UsefulThings.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCircleLineCrossPoints::CCircleLineCrossPoints()
{
	nPoints = 0;
	X[0] = X[1] = Y[0] = Y[1] = 0.;
}

CCircleLineCrossPoints::~CCircleLineCrossPoints()
{

}

void CCircleLineCrossPoints::Clear()
{
	nPoints = 0;
	X[0] = X[1] = Y[0] = Y[1] = 0.;
}

void CCircleLineCrossPoints::Calculate(double Xa, double Ya, double Ra, double K, double B)
{
	Clear();

	double a = 1. + K * K;
	double b = 2. * (Xa + Ya * K - K * B);
	double c = Xa * Xa + B * B - 2. * Ya * B + Ya * Ya - Ra * Ra;

	double d = b * b - 4. * a * c;

	if(d < -EPS) return;		// ����� ����������� ���.
	else if(fabs(d) < EPS){		// ���������� ���� ����� �����������.
		nPoints = 1;
		X[0] = b / (2. * a);
		Y[0] = K * X[0] + B;
		return;
	}
	else{					// ���������� ��� ����� �����������.
		nPoints = 2;
		X[0] = (b - sqrt(d)) / (2. * a);
		X[1] = (b + sqrt(d)) / (2. * a);
		Y[0] = K * X[0] + B;
		Y[1] = K * X[1] + B;
		return;
	}
}

void CCircleLineCrossPoints::Calculate(double Xa, double Ya, double Ra, double X1, double Y1, double X2, double Y2)
{
	Clear();

	if(X1 == X2) return;

	double K = (Y1 - Y2) / (X1 - X2);
	double B = Y1 - K * X1;

	Calculate(Xa, Ya, Ra, K, B);
}
