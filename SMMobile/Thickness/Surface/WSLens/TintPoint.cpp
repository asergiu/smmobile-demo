// TintPoint.cpp : implementation file
//

#include "stdafx.h"
#include "TintPoint.h"

/////////////////////////////////////////////////////////////////////////////
// CTintPoint

CTintPoint* CTintPoint::CreateObject()
{
	CTintPoint *pObject = new CTintPoint;
	if(pObject)
		pObject->ResetState();
	return pObject;
}

ITintPoint * CTintPoint::GetInterface()
{
	return static_cast<ITintPoint*>(this);
}

CTintPoint::CTintPoint()
{
	m_r = m_g = m_b = 255;
}

CTintPoint::~CTintPoint()
{
}

unsigned long CTintPoint::GetR()
{
	return m_r;
}
bool CTintPoint::SetR(unsigned long newValue)
{
	if(!TestRangeMin0(newValue, 255, 1))
		return false;
	m_r = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

unsigned long CTintPoint::GetG()
{
	return m_g;
}
bool CTintPoint::SetG(unsigned long newValue)
{
	if(!TestRangeMin0(newValue, 255, 1))
		return false;
	m_g = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

unsigned long CTintPoint::GetB()
{
	return m_b;
}
bool CTintPoint::SetB(unsigned long newValue)
{
	if(!TestRangeMin0(newValue, 255, 1))
		return false;
	m_b = newValue;
	FireChange();
	SetModifiedFlag();
	return true;
}

bool CTintPoint::SetPoint(unsigned long R, unsigned long G, unsigned long B)
{
	if(!TestRangeMin0(R, 255, 1))
		return false;
	if(!TestRangeMin0(G, 255, 1))
		return false;
	if(!TestRangeMin0(B, 255, 1))
		return false;
	m_r = R; m_g = G; m_b = B;
	FireChange();
	SetModifiedFlag();
	return true;
}

bool CTintPoint::DoPropExchange(CPropObjectExchange* pPX)
{
	if(!pPX)
		return false;

	long lVersion = _wVersion;
	if(pPX->ExchangeVersion(lVersion) == false)
		return false;

	if(pPX->PX_Long("R", m_r, 255) == false)
		return false;
	if(pPX->PX_Long("G", m_g, 255) == false)
		return false;
	if(pPX->PX_Long("B", m_b, 255) == false)
		return false;

	return true;
}

ITintPoint * CTintPoint::Clone() 
{
	CTintPoint *pNewObject = CTintPoint::CreateObject();
	if(pNewObject) {
		std::stringbuf buf;
		std::ostream out(&buf);
		std::istream in(out.rdbuf());

		if(WriteToSteam(&out))
			if(pNewObject->ReadFromSteam(&in))
				return pNewObject->GetInterface();
		pNewObject->Release();
	}
	return NULL;
}
