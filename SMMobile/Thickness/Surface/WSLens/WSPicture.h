#pragma once

#include "ClassMatrix.h"
#include "ObjectBase.h"
#include <string>
using std::string;

const char GsfMaska[] = { "GS5.0" };
const char YUVmode = 111;
#define WIDTHBYTES(i)   ((unsigned)((i+31)&(~31))/8)  /* ULONG aligned ! */

struct GSF_TITLE {
	char  Maska[5] ;				// --> "GS5.0"
	short Number ;					// --> (int)55 ;
	short Width ;					// --> Image width in pixel
	short Height ;					// --> Image height in pixel
	short GroupBytes ;				// -->
	char  YUVmode ;					// --> YUV-mode: 0=YYVUYY, 2=YYVU, 111=RGB ;
	short LineLen ;					// --> YUV-LineLen: (if YUVmode=0 and Width=640 then LineLen=960)
	short System ;					// --> TV-System (NTSC=1, PAL=2, SECAM=3, eiaaa iacaaeneo=-1)
	char  AFF ;						// --> 0=FF, 1=AFF
	long  OffsetYUV ;				// --> Offset YUV
	long  OffsetCenter ;			// --> Offset eye Centers
	long  OffsetLense ;				// --> Offset struct LENSE
	long  OffsetRGB1 ;				// -->
	long  OffsetRGB2 ;				// -->
	long  OffsetHoch ;				// -->
	long  OffsetUpDown[2] ;			// -->
	long  OffsetPhotochrom ;		// -->
	long  OffsetOffsetPhotochrom ;	// -->
	char  Reserv[1] ;				// --> Reserv bytes
} ;

typedef
enum PictureStatus
{
	wsPicture		= 1,
	wsPictureObject	= 2
} PictureStatus;

const int WINRGB_DIB = 1;
const int WINRLE4_DIB = 2;
const int WINRLE8_DIB = 3;
const int PM_DIB = 10;

typedef unsigned char *LPDIB;

#define RECTWIDTH(lpRect)  ((lpRect)->right - (lpRect)->left)
#define RECTHEIGHT(lpRect) ((lpRect)->bottom - (lpRect)->top)

#define LUM(r, g, b)	(0.3*(r) + 0.6 * (g) + 0.1 * (b))
const int kOpacity = 55; // %
const BYTE Text_Color_R = 0x10; //0x10;
const BYTE Text_Color_G = 0x4a; //0x4a;
const BYTE Text_Color_B = 0xb0; //0x94;
const BYTE Text_Color_Y = (BYTE)(LUM(Text_Color_R, Text_Color_G, Text_Color_B) + 0.5);

/////////////////////////////////////////////////////////////////////////////
// CWSPicture

class CWSPicture :
	public CObjectEventBase<IWSPicture, IWSPictureEvents>
{
public:
    static CWSPicture* CreateObject();
	IWSPicture * GetInterface();

protected:
	CWSPicture();           // protected constructor used by dynamic creation

// Attributes
public:
	long GetWidth();
	long GetHeight();

	const unsigned char * GetPicture();
	bool SetPicture(const unsigned char * pNewValue);
	bool PictureSize(long Width, long Height);
	long LoadFromFile(LPCTSTR FileName);
	long SaveToFile(LPCTSTR FileName);
    long LoadBMPFromMemory(const unsigned char * pFileData);
#ifndef _WINDOWS_
    //bool Render(Display* display, Window win, long x, long y, long cx, long cy, long xSrc, long ySrc, long cxSrc, long cySrc);
#endif
    IWSPicture * Clone();

protected:
	static const long _wVersion = 1;
	bool DoPropExchange(CPropObjectExchange* pPX);

protected:
	virtual ~CWSPicture();

	virtual void FinalRelease()
	{
		delete this;
	}

	void FireChange()
	{
		if(m_map.size() > 0) {
			EventMap::iterator it;
			for(it = m_map.begin(); it != m_map.end(); ++it){
				it->second->PictureChanged();
			}
		}
	}

// Implementation
protected:
	long m_Status;	// 0 - Picture not present, 1 - Picture is present.
	unsigned char * m_Picture;

public:
	static bool ConvertPictureToBitmap24(unsigned char ** ppPicture);
	static bool RotatePicture(unsigned char ** ppPicture, long Mode);

protected:
	unsigned char *	GetPicturePart(unsigned char ** ppPicture, unsigned long x, unsigned long y, unsigned long cx, unsigned long cy);
	static int	CheckPictureFormat(const unsigned char * Block);
	void	ScaleBuffer(unsigned char * InBuffer, int InW, int InH, int InBPP, unsigned char * OutBuffer, int OutW, int OutH, int OutBPP);
	unsigned char *	RescalePicture(long Width, long Height);
	static unsigned char * CreateNewBitmap24(const unsigned char * Block);
	void	InitTitleGSF (struct GSF_TITLE * gsf, short dxField, short dyField);
	unsigned char * CopyFromDibToGSF(unsigned char * lp, int iOffset, int& ErrorCode);
	bool	MakePictureObject();
	unsigned char * MakePicture();
	void	PrepDemoString(LPRECT pRC, CColorMatrix32 &mtrxdest);
	void	DrawDemoText(LPBYTE lpBits, LPBITMAPINFOHEADER bi);
	void	Add_NotForCommercialUse_Label();

	unsigned char * DIBReadFile(LPCTSTR FileName, int *dwFileSize);
	int		DIBType(LPDIB lpDib);
	int		DIBNumColors(LPDIB lpDib);

	long	LoadFromBMP(LPCTSTR FileName);
	long	LoadFromJPEG(LPCTSTR FileName);
	long	SaveToBMP(LPCTSTR FileName);
	long	SaveToJPEG(LPCTSTR FileName);
};
