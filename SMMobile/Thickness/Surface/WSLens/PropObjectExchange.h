#pragma once

#include "iosfwd"
#include "istream"
#include "ostream"
#include "string"
using std::ostream;
using std::istream;
using std::string;

#include "IWSLLens.h"
#include "ObjectPointer.h"

#include "stdafx_linux.h"

class CPropObjectExchange
{
public:
	bool bLoad;
	bool bReset;
	ostream * pOutStream;
	istream * pInStream;

	CPropObjectExchange(void);
	~CPropObjectExchange(void);
	bool IsLoading();
	bool ExchangeData(unsigned long nSize, void* pvProp);

	bool PX_Memory(LPCTSTR pszPropName, unsigned long nSize, void* pvProp);
	bool ExchangeVersion(long& lVersion);
	bool PX_Long(LPCTSTR pszPropName, long& lValue, long lDefault = 0);
	bool PX_Long(LPCTSTR pszPropName, unsigned long& lValue, long lDefault = 0);
	bool PX_Double(LPCTSTR pszPropName, double& doubleValue, double doubleDefault = 0.);
	bool PX_Float(LPCTSTR pszPropName, float& floatValue, float floatDefault = 0.);
	bool PX_String(LPCTSTR pszPropName, string& strValue, const string& strDefault);
	bool PX_Bool(LPCTSTR pszPropName, bool& bValue, bool bDefault);
	bool PX_Font(LPCTSTR pszPropName, LOGFONT& fValue, LOGFONT * pfDefault = NULL);
};

template<class TObject, class IObject>
bool PX_WSLObject(CPropObjectExchange* pPX, LPCTSTR /*pszPropName*/,
				  CObjPtr<IObject>& pUnk, IObject* pUnkDefault)
{
	if(!pPX)
		return false;
	if(pPX->bReset) {
		pUnk.ReleaseDispatch();
		if(pUnkDefault)
			pUnk.AttachDispatch(pUnkDefault->Clone());
		return true;
	}
	bool bRes = false;
	long nobjStatus = 0;
	try {
		if(pPX->IsLoading()) {
			if((bRes = pPX->ExchangeData(sizeof(long), &nobjStatus)) == false)
				return false;
			if(nobjStatus) {
				if(!pPX->pInStream)
					return false;
				if(!pUnk) {
					TObject * pObject = TObject::CreateObject();
					if(pObject)
						pUnk.AttachDispatch(pObject->GetInterface());
				}
				if(!pUnk)
					return false;
				bRes = pUnk->ReadFromSteam(pPX->pInStream);
			}
			else {
				pUnk.ReleaseDispatch();
			}
		}
		else {
			nobjStatus = (pUnk == NULL)? 0 : 1;
			if((bRes = pPX->ExchangeData(sizeof(long), &nobjStatus)) == false)
				return false;
			if(nobjStatus) {
				if(!pPX->pOutStream)
					return false;
				bRes = pUnk->WriteToSteam(pPX->pOutStream);
			}
		}
	}
	catch(...) {
		return false;
	}
	return bRes;
}
