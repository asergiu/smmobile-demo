#include "stdafx.h"
#include "UsefulThings.h"

typedef struct {
	int orj_num;
	double x;
	double y;
} _point_xy;

int comp_x(const void *a,const void *b)
{
    double diff=(((_point_xy *)a)->x)-(((_point_xy *)b)->x);
    if(diff>EPS)
        return 1;
    else if(diff<(-EPS))
        return -1;
    else if((diff=(((_point_xy *)a)->y)-(((_point_xy *)b)->y))>EPS)
        return 1;
    else if(diff<(-EPS))
        return -1;
    else return 0;
}
