//
//  ThicknessCurveData.h
//  thickness
//
//  Created by VS on 30.04.12.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SurfaceData.h"

@interface ThicknessCurveData : NSObject
{
    NSDictionary * dataDict;
    CGPoint point0,point5,point10,point15,point20,point25,point30,point35,point40,point45,point50,point55,point60,point65;
}

@property (nonatomic,readonly) CGPoint point0;
@property (nonatomic,readonly) CGPoint point5;
@property (nonatomic,readonly) CGPoint point10;
@property (nonatomic,readonly) CGPoint point15;
@property (nonatomic,readonly) CGPoint point20;
@property (nonatomic,readonly) CGPoint point25;
@property (nonatomic,readonly) CGPoint point30;
@property (nonatomic,readonly) CGPoint point35;
@property (nonatomic,readonly) CGPoint point40;
@property (nonatomic,readonly) CGPoint point45;
@property (nonatomic,readonly) CGPoint point50;
@property (nonatomic,readonly) CGPoint point55;
@property (nonatomic,readonly) CGPoint point60;
@property (nonatomic,readonly) CGPoint point65;

- (id)initWithDictonary:(NSDictionary*)array;
- (id)initWithSurfaceData:(struct SurfaceData)data;
- (struct SurfaceData)getSurfaceData;
- (void) zeroSurface;
- (void) updateData;
- (CGPoint) pointForKey: (NSString*)key;
@end
