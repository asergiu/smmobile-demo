//
//  DataSource.h
//  thickness
//
//  Created by VS on 30.04.12.
//
#import <UIKit/UIKit.h>
#import "SurfaceData.h"

typedef NSString ThicknessLensDesignType;
enum// ThicknessLensDesignType
{
    ThicknessLensDesignSpherical15,
    ThicknessLensDesignSpherical16,
    ThicknessLensDesignAspheric15,
    ThicknessLensDesignAspheric16,
    ThicknessLensDesignAspheric167,
    ThicknessLensDesignAspheric174,
    ThicknessLensDesignAspheric,
    ThicknessLensDesignSpherical
};

typedef NSString ThicknessLensDiameter;
enum// ThicknessLensDiameter
{
    ThicknessLensDiameter60,
    ThicknessLensDiameter65,
    ThicknessLensDiameter70,
    ThicknessLensDiameter75
};

typedef NSString ThicknessDataType;
enum// ThicknessDataType
{
    ThicknessDataTypeR1,
    ThicknessDataTypeR2,
    ThicknessDataTypeET
};


#import <Foundation/Foundation.h>
#import "ThicknessCurveData.h"

#define MainDataSource [DataSource realData]

#define ThicknessLensDesignSpherical15 @"CR39"
#define ThicknessLensDesignSpherical153 @"1.53(Trivex)"
#define ThicknessLensDesignSpherical156 @"1.56"
#define ThicknessLensDesignSpherical159 @"1.59(Poly)"
#define ThicknessLensDesignSpherical16 @"1.6"
#define ThicknessLensDesignSpherical167 @"1.67"
#define ThicknessLensDesignSpherical174 @"1.74"

#define ThicknessLensDesignAspheric15 @"1.5as"
#define ThicknessLensDesignAspheric153 @"1.53as(Trivex)"
#define ThicknessLensDesignAspheric156 @"1.56as"
#define ThicknessLensDesignAspheric159 @"1.59as(Poly)"
#define ThicknessLensDesignAspheric16 @"1.6as"
#define ThicknessLensDesignAspheric167 @"1.67as"
#define ThicknessLensDesignAspheric174 @"1.74as"

#define ThicknessLensDesignDAspheric15 @"1.5das"
#define ThicknessLensDesignDAspheric153 @"1.53das(Trivex)"
#define ThicknessLensDesignDAspheric156 @"1.56das"
#define ThicknessLensDesignDAspheric159 @"1.59das(Poly)"
#define ThicknessLensDesignDAspheric16 @"1.6das"
#define ThicknessLensDesignDAspheric167 @"1.67das"
#define ThicknessLensDesignDAspheric174 @"1.74das"

#define ThicknessLensDiameter60 @"60"
#define ThicknessLensDiameter65 @"65"
#define ThicknessLensDiameter70 @"70"
#define ThicknessLensDiameter75 @"75"

#define ThicknessDataTypeR1 @"R1"
#define ThicknessDataTypeR2 @"R2"
#define ThicknessDataTypeET @"ET"

#define kThicknessElementPower @"Power"
#define kThicknessElementCenter @"Center"

@interface DataSource : NSObject
{
    NSDictionary * data;
    NSDictionary * videoData;
    NSMutableDictionary * dataForDraw;
    struct SurfaceData leftLensSurface[3];
    struct SurfaceData rightLensSurface[3];
    struct SurfaceData left2DSurface[4];
    struct SurfaceData right2DSurface[4];
    double leftLensWeight;
    double rightLensWeight;
}

@property (nonatomic,readonly) NSDictionary * data;
@property (nonatomic,readonly) NSMutableDictionary * dataForDraw;

+ (DataSource *)realData;

- (NSDictionary*) lensByType:(ThicknessLensDesignType*)type;

- (ThicknessCurveData*) dataForLensWithTypeOfData:(ThicknessDataType*)dataType lensType:(ThicknessLensDesignType*)lensType power:(float)power diam:(ThicknessLensDiameter*)lensDiam;
- (ThicknessCurveData*) dataForLensWithTypeOfData:(ThicknessDataType*)dataType lensType:(ThicknessLensDesignType*)lensType power:(float)power;

- (NSMutableDictionary*) dataForLensWithType:(ThicknessLensDesignType*)lensType power:(float)power;
- (NSString*) nameOfVideoForLensType:(ThicknessLensDesignType*)type andPower:(float)power andCyl:(float)cyl;

- (void) refreshDataWithThicknessCurveDataLR1:(ThicknessCurveData*)dataLeftR1 dataLR2:(ThicknessCurveData*)dataLeftR2 dataLR3:(ThicknessCurveData*)dataLeftR3 dataRR1:(ThicknessCurveData*)dataRightR1 dataRR2:(ThicknessCurveData*)dataRightR2 dataRR3:(ThicknessCurveData*)dataRightR3;

- (void) updateDataForDrawWithPower:(float)power andCyl:(float)cyl andLeftLensType:(ThicknessLensDesignType*)leftLensDesign andRightLensType:(ThicknessLensDesignType*)rightLensDesign andPrecalibratedLeft:(bool)leftPrecalibrated andPrecalibratedRight:(bool)rightPrecalibrated andPrecalibratedDiam:(int)diam;

- (NSDictionary*) separateArray:(NSArray*)array byPower: (float)power;


- (NSDictionary*)setDictonaryWithThicknessCurveData:(ThicknessCurveData*) curveData forLeftSide:(BOOL)leftSide andType:(int)type;

- (struct SurfaceData)getSurfaceDataForDraw:(int)nCurve forLeftSide:(BOOL)leftSide;
- (struct SurfaceData)getSurfaceDataForDraw2D:(int)nCurve forLeftSide:(BOOL)leftSide;
- (float) getThicknessDataForDraw:(BOOL)firstSection forLeftSide:(BOOL)leftSide forCenterPoint:(BOOL)centerPoint;
- (float) getThicknessDataForDraw2D:(BOOL)firstSection forLeftSide:(BOOL)leftSide forCenterPoint:(BOOL)centerPoint;
- (double) getLensWeightForDraw2D:(BOOL)leftSide;
- (long) getAsphericStatus:(NSString *)design;

- (UIImage *) updatePhotoForDrawWithPower:(double)power andCyl:(double)cyl andAxis:(long)axis
                          andLeftLensType:(ThicknessLensDesignType*)leftLensDesign
                         andRightLensType:(ThicknessLensDesignType*)rightLensDesign;

@end
