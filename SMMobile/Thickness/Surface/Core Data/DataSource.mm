//
//  DataSource.m
//  thickness
//
//  Created by VS on 30.04.12.
//

#import "DataSource.h"
#import "ThicknessCurveData.h"
#import "MatrixEngine.h"
#import "math.h"
#include "ShellLens.h"

@implementation DataSource

@synthesize data;
@synthesize dataForDraw;

static DataSource * realData = NULL;
+(DataSource *)realData {
    if (!realData || realData == NULL) {
		realData = [[DataSource alloc] init];
	}
	return realData;
}

- (id)init
{
    self = [super init];
    if (self) {
        data = [[NSDictionary alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Data" ofType:@"plist"]];
        dataForDraw = [[NSMutableDictionary alloc] init];
        videoData = [[NSDictionary alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Video" ofType:@"plist"]];  
        
    }
    
    return self;
}

-(NSString*) nameOfVideoForLensType:(ThicknessLensDesignType*)type andPower:(float)power andCyl:(float)cyl
{
    float f1 = (power>=0) ? [MainDataSource dataForLensWithTypeOfData:ThicknessDataTypeET lensType:type power:power].point0.y : [MainDataSource dataForLensWithTypeOfData:ThicknessDataTypeET lensType:type power:power].point65.y;
    float f2 = ((power+cyl)>=0) ? [MainDataSource dataForLensWithTypeOfData:ThicknessDataTypeET lensType:type power:(power+cyl)].point0.y : [MainDataSource dataForLensWithTypeOfData:ThicknessDataTypeET lensType:type power:(power+cyl)].point65.y;
    
    return (f1 == 0 || f2 == 0) ? nil : @"video";
}


- (void) dealloc
{
}

- (struct SurfaceData)getSurfaceDataForDraw:(int)nCurve forLeftSide:(BOOL)leftSide
{
    struct SurfaceData surface;
    if (leftSide) {
        if (nCurve >= 0 && nCurve < 3) {
            surface = leftLensSurface[nCurve];
        }
    }
    else {
        if (nCurve >= 0 && nCurve < 3) {
            surface = rightLensSurface[nCurve];
        }
    }
    return surface;
}
- (struct SurfaceData)getSurfaceDataForDraw2D:(int)nCurve forLeftSide:(BOOL)leftSide
{
    struct SurfaceData surface;
    if (leftSide) {
        if (nCurve >= 0 && nCurve < 4) {
            surface = left2DSurface[nCurve];
        }
    }
    else {
        if (nCurve >= 0 && nCurve < 4) {
            surface = right2DSurface[nCurve];
        }
    }
    return surface;
}
- (float) getThicknessDataForDraw:(BOOL)firstSection forLeftSide:(BOOL)leftSide forCenterPoint:(BOOL)centerPoint
{
    float thickness = 0.;
    if (leftSide) {
        if (centerPoint) {
            if (firstSection)
                thickness = leftLensSurface[1].z[0] - leftLensSurface[0].z[0];
            else
                thickness = leftLensSurface[2].z[0] - leftLensSurface[0].z[0];
        }
        else {
            if (firstSection)
                thickness = leftLensSurface[1].z[13] - leftLensSurface[0].z[13];
            else
                thickness = leftLensSurface[2].z[13] - leftLensSurface[0].z[13];
        }
    }
    else {
        if (centerPoint) {
            if (firstSection)
                thickness = rightLensSurface[1].z[0] - rightLensSurface[0].z[0];
            else
                thickness = rightLensSurface[2].z[0] - rightLensSurface[0].z[0];
        }
        else {
            if (firstSection)
                thickness = rightLensSurface[1].z[13] - rightLensSurface[0].z[13];
            else
                thickness = rightLensSurface[2].z[13] - rightLensSurface[0].z[13];
        }
    }
    return thickness;
}
- (float) getThicknessDataForDraw2D:(BOOL)firstSection forLeftSide:(BOOL)leftSide forCenterPoint:(BOOL)centerPoint
{
    float thickness = 0.;
    const int nRightSize = 1 + right2DSurface[0].diam / 5 + ((right2DSurface[0].diam % 5)? 1 : 0);
    const int nLeftSize = 1 + left2DSurface[0].diam / 5 + ((left2DSurface[0].diam % 5)? 1 : 0);
    if (leftSide) {
        if (centerPoint) {
            if (firstSection)
                thickness = left2DSurface[1].z[0] - left2DSurface[0].z[0];
            else
                thickness = left2DSurface[2].z[0] - left2DSurface[0].z[0];
        }
        else {
            if (firstSection)
                thickness = left2DSurface[1].z[nLeftSize - 1] - left2DSurface[0].z[nLeftSize - 1];
            else
                thickness = left2DSurface[2].z[nLeftSize - 1] - left2DSurface[0].z[nLeftSize - 1];
        }
    }
    else {
        if (centerPoint) {
            if (firstSection)
                thickness = right2DSurface[1].z[0] - right2DSurface[0].z[0];
            else
                thickness = right2DSurface[2].z[0] - right2DSurface[0].z[0];
        }
        else {
            if (firstSection)
                thickness = right2DSurface[1].z[nRightSize - 1] - right2DSurface[0].z[nRightSize - 1];
            else
                thickness = right2DSurface[2].z[nRightSize - 1] - right2DSurface[0].z[nRightSize - 1];
        }
    }
    return thickness;
}

- (double) getLensWeightForDraw2D:(BOOL)leftSide
{
    double weight = 0.;
    if (leftSide) {
        weight = leftLensWeight;
    }
    else {
        weight = rightLensWeight;
    }
    return weight;
}

- (void) refreshDataWithThicknessCurveDataLR1:(ThicknessCurveData*)dataLeftR1 dataLR2:(ThicknessCurveData*)dataLeftR2 dataLR3:(ThicknessCurveData*)dataLeftR3 dataRR1:(ThicknessCurveData*)dataRightR1 dataRR2:(ThicknessCurveData*)dataRightR2 dataRR3:(ThicknessCurveData*)dataRightR3
{
    [dataForDraw removeAllObjects];
    [dataForDraw addEntriesFromDictionary:[self setDictonaryWithThicknessCurveData:dataLeftR1 forLeftSide:YES andType:1]];
    [dataForDraw addEntriesFromDictionary:[self setDictonaryWithThicknessCurveData:dataLeftR2 forLeftSide:YES andType:2]];
    [dataForDraw addEntriesFromDictionary:[self setDictonaryWithThicknessCurveData:dataLeftR3 forLeftSide:YES andType:3]];
    [dataForDraw addEntriesFromDictionary:[self setDictonaryWithThicknessCurveData:dataRightR1 forLeftSide:NO andType:1]];
    [dataForDraw addEntriesFromDictionary:[self setDictonaryWithThicknessCurveData:dataRightR2 forLeftSide:NO andType:2]];
    [dataForDraw addEntriesFromDictionary:[self setDictonaryWithThicknessCurveData:dataRightR3 forLeftSide:NO andType:3]];
}

-(NSDictionary*)setDictonaryWithThicknessCurveData:(ThicknessCurveData*) curveData forLeftSide:(BOOL)leftSide andType:(int)type
{
    NSString * key;
    if (leftSide) key = [NSString stringWithFormat:@"L_R%i",type];
    else key = [NSString stringWithFormat:@"R_R%i",type];
    
    
    NSMutableDictionary * dataDict = [NSMutableDictionary dictionary];
    [dataDict setObject:[NSNumber numberWithFloat:curveData.point0.y]  forKey:[@"point0_" stringByAppendingString:key]];
    [dataDict setObject:[NSNumber numberWithFloat:curveData.point5.y] forKey:[@"point5_" stringByAppendingString:key]];
    [dataDict setObject:[NSNumber numberWithFloat:curveData.point10.y] forKey:[@"point10_" stringByAppendingString:key]];
    [dataDict setObject:[NSNumber numberWithFloat:curveData.point15.y] forKey:[@"point15_" stringByAppendingString:key]];
    [dataDict setObject:[NSNumber numberWithFloat:curveData.point20.y] forKey:[@"point20_" stringByAppendingString:key]];
    [dataDict setObject:[NSNumber numberWithFloat:curveData.point25.y] forKey:[@"point25_" stringByAppendingString:key]];
    [dataDict setObject:[NSNumber numberWithFloat:curveData.point30.y] forKey:[@"point30_" stringByAppendingString:key]];
    [dataDict setObject:[NSNumber numberWithFloat:curveData.point35.y] forKey:[@"point35_" stringByAppendingString:key]];
    [dataDict setObject:[NSNumber numberWithFloat:curveData.point40.y] forKey:[@"point40_" stringByAppendingString:key]];
    [dataDict setObject:[NSNumber numberWithFloat:curveData.point45.y] forKey:[@"point45_" stringByAppendingString:key]];
    [dataDict setObject:[NSNumber numberWithFloat:curveData.point50.y] forKey:[@"point50_" stringByAppendingString:key]];
    [dataDict setObject:[NSNumber numberWithFloat:curveData.point55.y] forKey:[@"point55_" stringByAppendingString:key]];
    [dataDict setObject:[NSNumber numberWithFloat:curveData.point60.y] forKey:[@"point60_" stringByAppendingString:key]];
    [dataDict setObject:[NSNumber numberWithFloat:curveData.point65.y] forKey:[@"point65_" stringByAppendingString:key]];
    
    return dataDict;
}

- (NSDictionary*) dataForPower:(float)power
{
    NSMutableDictionary * powerDict = [NSMutableDictionary dictionary];
    
    
    
    return powerDict;
}

- (NSDictionary*) separateArray:(NSArray*)array byPower: (float)power
{
        
    for (NSDictionary* element in array) 
    {
        if ([[element objectForKey:kThicknessElementPower] floatValue] == power) 
            return element;
    }
    
    return nil;
}

-(NSDictionary*) lensByType:(ThicknessLensDesignType*)type
{
        return [data objectForKey:type];
}

- (ThicknessCurveData*) dataForLensWithTypeOfData:(ThicknessDataType*)dataType lensType:(ThicknessLensDesignType*)lensType power:(float)power diam:(ThicknessLensDiameter*)lensDiam
{
    return [[[self lensByType:lensType] objectForKey:dataType] objectForKey:lensDiam];
}

- (ThicknessCurveData*) dataForLensWithTypeOfData:(ThicknessDataType*)dataType lensType:(ThicknessLensDesignType*)lensType power:(float)power
{
    ThicknessCurveData* thicknessCurveData = [[ThicknessCurveData alloc] initWithDictonary:[self separateArray: [[[self lensByType:lensType] objectForKey:dataType] objectForKey:ThicknessLensDiameter65] byPower:power]];
    
    return thicknessCurveData;
}

- (NSMutableDictionary*) dataForLensWithType:(ThicknessLensDesignType*)lensType power:(float)power
{
    NSMutableDictionary * dataDict = [NSMutableDictionary dictionaryWithCapacity:3];
    [dataDict setObject:[self dataForLensWithTypeOfData:ThicknessDataTypeR1 lensType:lensType power:power] forKey:ThicknessDataTypeR1];
    [dataDict setObject:[self dataForLensWithTypeOfData:ThicknessDataTypeR2 lensType:lensType power:power] forKey:ThicknessDataTypeR2];
    [dataDict setObject:[self dataForLensWithTypeOfData:ThicknessDataTypeET lensType:lensType power:power] forKey:ThicknessDataTypeET];
    return dataDict;
    
}

- (long) getAsphericStatus:(NSString *)design
{
    // Spherical
    if( ![ design compare : ThicknessLensDesignSpherical15 ] )
        return 0;
    if( ![ design compare : ThicknessLensDesignSpherical153 ] )
        return 0;
    if( ![ design compare : ThicknessLensDesignSpherical156 ] )
        return 0;
    if( ![ design compare : ThicknessLensDesignSpherical159 ] )
        return 0;
    if( ![ design compare : ThicknessLensDesignSpherical16 ] )
        return 0;
    if( ![ design compare : ThicknessLensDesignSpherical167 ] )
        return 0;
    if( ![ design compare : ThicknessLensDesignSpherical174 ] )
        return 0;
    // Aspheric
    if( ![ design compare : ThicknessLensDesignAspheric15 ] )
        return 1;
    if( ![ design compare : ThicknessLensDesignAspheric153 ] )
        return 1;
    if( ![ design compare : ThicknessLensDesignAspheric156 ] )
        return 1;
    if( ![ design compare : ThicknessLensDesignAspheric159 ] )
        return 1;
    if( ![ design compare : ThicknessLensDesignAspheric16 ] )
        return 1;
    if( ![ design compare : ThicknessLensDesignAspheric167 ] )
        return 1;
    if( ![ design compare : ThicknessLensDesignAspheric174 ] )
        return 1;
    // Double Aspheric
    if( ![ design compare : ThicknessLensDesignDAspheric15 ] )
        return 2;
    if( ![ design compare : ThicknessLensDesignDAspheric153 ] )
        return 2;
    if( ![ design compare : ThicknessLensDesignDAspheric156 ] )
        return 2;
    if( ![ design compare : ThicknessLensDesignDAspheric159 ] )
        return 2;
    if( ![ design compare : ThicknessLensDesignDAspheric16 ] )
        return 2;
    if( ![ design compare : ThicknessLensDesignDAspheric167 ] )
        return 2;
    if( ![ design compare : ThicknessLensDesignDAspheric174 ] )
        return 2;
    
    return false;
}
- (double) getMaterialIndex:(NSString *)design
{
    // Spherical
    if( ![ design compare : ThicknessLensDesignSpherical15 ] )
        return 1.5;
    if( ![ design compare : ThicknessLensDesignSpherical153 ] )
        return 1.53;
    if( ![ design compare : ThicknessLensDesignSpherical156 ] )
        return 1.56;
    if( ![ design compare : ThicknessLensDesignSpherical159 ] )
        return 1.59;
    if( ![ design compare : ThicknessLensDesignSpherical16 ] )
        return 1.6;
    if( ![ design compare : ThicknessLensDesignSpherical167 ] )
        return 1.67;
    if( ![ design compare : ThicknessLensDesignSpherical174 ] )
        return 1.74;
    // Aspheric
    if( ![ design compare : ThicknessLensDesignAspheric15 ] )
        return 1.5;
    if( ![ design compare : ThicknessLensDesignAspheric153 ] )
        return 1.53;
    if( ![ design compare : ThicknessLensDesignAspheric156 ] )
        return 1.56;
    if( ![ design compare : ThicknessLensDesignAspheric159 ] )
        return 1.59;
    if( ![ design compare : ThicknessLensDesignAspheric16 ] )
        return 1.6;
    if( ![ design compare : ThicknessLensDesignAspheric167 ] )
        return 1.67;
    if( ![ design compare : ThicknessLensDesignAspheric174 ] )
        return 1.74;
    // Double Aspheric
    if( ![ design compare : ThicknessLensDesignDAspheric15 ] )
        return 1.5;
    if( ![ design compare : ThicknessLensDesignDAspheric153 ] )
        return 1.53;
    if( ![ design compare : ThicknessLensDesignDAspheric156 ] )
        return 1.56;
    if( ![ design compare : ThicknessLensDesignDAspheric159 ] )
        return 1.59;
    if( ![ design compare : ThicknessLensDesignDAspheric16 ] )
        return 1.6;
    if( ![ design compare : ThicknessLensDesignDAspheric167 ] )
        return 1.67;
    if( ![ design compare : ThicknessLensDesignDAspheric174 ] )
        return 1.74;
    
    return 1.5;
}
- (double) getMaterialDensity:(NSString *)design
{
    // Spherical
    if( ![ design compare : ThicknessLensDesignSpherical15 ] )
        return 1.32;
    if( ![ design compare : ThicknessLensDesignSpherical153 ] )
        return 1.11;
    if( ![ design compare : ThicknessLensDesignSpherical156 ] )
        return 1.21;
    if( ![ design compare : ThicknessLensDesignSpherical159 ] )
        return 1.21;
    if( ![ design compare : ThicknessLensDesignSpherical16 ] )
        return 1.34;
    if( ![ design compare : ThicknessLensDesignSpherical167 ] )
        return 1.36;
    if( ![ design compare : ThicknessLensDesignSpherical174 ] )
        return 1.47;
    // Aspheric
    if( ![ design compare : ThicknessLensDesignAspheric15 ] )
        return 1.32;
    if( ![ design compare : ThicknessLensDesignAspheric153 ] )
        return 1.11;
    if( ![ design compare : ThicknessLensDesignAspheric156 ] )
        return 1.21;
    if( ![ design compare : ThicknessLensDesignAspheric159 ] )
        return 1.21;
    if( ![ design compare : ThicknessLensDesignAspheric16 ] )
        return 1.34;
    if( ![ design compare : ThicknessLensDesignAspheric167 ] )
        return 1.36;
    if( ![ design compare : ThicknessLensDesignAspheric174 ] )
        return 1.47;
    // Double Aspheric
    if( ![ design compare : ThicknessLensDesignDAspheric15 ] )
        return 1.32;
    if( ![ design compare : ThicknessLensDesignDAspheric153 ] )
        return 1.11;
    if( ![ design compare : ThicknessLensDesignDAspheric156 ] )
        return 1.21;
    if( ![ design compare : ThicknessLensDesignDAspheric159 ] )
        return 1.21;
    if( ![ design compare : ThicknessLensDesignDAspheric16 ] )
        return 1.34;
    if( ![ design compare : ThicknessLensDesignDAspheric167 ] )
        return 1.36;
    if( ![ design compare : ThicknessLensDesignDAspheric174 ] )
        return 1.47;
    
    return 1.5;
}

- (double) getMinCenterThickness:(NSString *)design
{
    // Spherical
    if( ![ design compare : ThicknessLensDesignSpherical15 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignSpherical153 ] )
        return 1.15;
    if( ![ design compare : ThicknessLensDesignSpherical156 ] )
        return 1.3;
    if( ![ design compare : ThicknessLensDesignSpherical159 ] )
        return 1.3;
    if( ![ design compare : ThicknessLensDesignSpherical16 ] )
        return 1.3;
    if( ![ design compare : ThicknessLensDesignSpherical167 ] )
        return 1.3;
    if( ![ design compare : ThicknessLensDesignSpherical174 ] )
        return 1.1;
    // Aspheric
    if( ![ design compare : ThicknessLensDesignAspheric15 ] )
        return 1.3;
    if( ![ design compare : ThicknessLensDesignAspheric153 ] )
        return 1.15;
    if( ![ design compare : ThicknessLensDesignAspheric156 ] )
        return 1.3;
    if( ![ design compare : ThicknessLensDesignAspheric159 ] )
        return 1.3;
    if( ![ design compare : ThicknessLensDesignAspheric16 ] )
        return 1.3;
    if( ![ design compare : ThicknessLensDesignAspheric167 ] )
        return 1.1;
    if( ![ design compare : ThicknessLensDesignAspheric174 ] )
        return 1.1;
    // Double Aspheric
    if( ![ design compare : ThicknessLensDesignDAspheric15 ] )
        return 1.3;
    if( ![ design compare : ThicknessLensDesignDAspheric153 ] )
        return 1.15;
    if( ![ design compare : ThicknessLensDesignDAspheric156 ] )
        return 1.3;
    if( ![ design compare : ThicknessLensDesignDAspheric159 ] )
        return 1.3;
    if( ![ design compare : ThicknessLensDesignDAspheric16 ] )
        return 1.3;
    if( ![ design compare : ThicknessLensDesignDAspheric167 ] )
        return 1.1;
    if( ![ design compare : ThicknessLensDesignDAspheric174 ] )
        return 1.1;
    
    return 1.3;
}
- (double) getMinEdgeThickness:(NSString *)design
{
    // Spherical
    if( ![ design compare : ThicknessLensDesignSpherical15 ] )
        return 1.0;
    if( ![ design compare : ThicknessLensDesignSpherical153 ] )
        return 1.0;
    if( ![ design compare : ThicknessLensDesignSpherical156 ] )
        return 1.0;
    if( ![ design compare : ThicknessLensDesignSpherical159 ] )
        return 1.0;
    if( ![ design compare : ThicknessLensDesignSpherical16 ] )
        return 0.8;
    if( ![ design compare : ThicknessLensDesignSpherical167 ] )
        return 0.8;
    if( ![ design compare : ThicknessLensDesignSpherical174 ] )
        return 0.8;
    // Aspheric
    if( ![ design compare : ThicknessLensDesignAspheric15 ] )
        return 1.0;
    if( ![ design compare : ThicknessLensDesignAspheric153 ] )
        return 0.8;
    if( ![ design compare : ThicknessLensDesignAspheric156 ] )
        return 0.8;
    if( ![ design compare : ThicknessLensDesignAspheric159 ] )
        return 0.8;
    if( ![ design compare : ThicknessLensDesignAspheric16 ] )
        return 0.8;
    if( ![ design compare : ThicknessLensDesignAspheric167 ] )
        return 0.8;
    if( ![ design compare : ThicknessLensDesignAspheric174 ] )
        return 0.8;
    // Double Aspheric
    if( ![ design compare : ThicknessLensDesignDAspheric15 ] )
        return 1.0;
    if( ![ design compare : ThicknessLensDesignDAspheric153 ] )
        return 0.8;
    if( ![ design compare : ThicknessLensDesignDAspheric156 ] )
        return 0.8;
    if( ![ design compare : ThicknessLensDesignDAspheric159 ] )
        return 0.8;
    if( ![ design compare : ThicknessLensDesignDAspheric16 ] )
        return 0.8;
    if( ![ design compare : ThicknessLensDesignDAspheric167 ] )
        return 0.8;
    if( ![ design compare : ThicknessLensDesignDAspheric174 ] )
        return 0.8;
    
    return 0.8;
}
- (double) getZeroThickness:(NSString *)design
{
    // Spherical
    if( ![ design compare : ThicknessLensDesignSpherical15 ] )
        return 2.2;
    if( ![ design compare : ThicknessLensDesignSpherical153 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignSpherical156 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignSpherical159 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignSpherical16 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignSpherical167 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignSpherical174 ] )
        return 2.0;
    // Aspheric
    if( ![ design compare : ThicknessLensDesignAspheric15 ] )
        return 2.2;
    if( ![ design compare : ThicknessLensDesignAspheric153 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignAspheric156 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignAspheric159 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignAspheric16 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignAspheric167 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignAspheric174 ] )
        return 2.0;
    // Double Aspheric
    if( ![ design compare : ThicknessLensDesignDAspheric15 ] )
        return 2.2;
    if( ![ design compare : ThicknessLensDesignDAspheric153 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignDAspheric156 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignDAspheric159 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignDAspheric16 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignDAspheric167 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignDAspheric174 ] )
        return 2.0;
    
    return 2.0;
}
- (double) getPowerCorrection:(NSString *)design
{
    // Spherical
    if( ![ design compare : ThicknessLensDesignSpherical15 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignSpherical153 ] )
        return 1.75;
    if( ![ design compare : ThicknessLensDesignSpherical156 ] )
        return 1.75;
    if( ![ design compare : ThicknessLensDesignSpherical159 ] )
        return 1.5;
    if( ![ design compare : ThicknessLensDesignSpherical16 ] )
        return 1.0;
    if( ![ design compare : ThicknessLensDesignSpherical167 ] )
        return 0.5;
    if( ![ design compare : ThicknessLensDesignSpherical174 ] )
        return 0.0;
    // Aspheric
    if( ![ design compare : ThicknessLensDesignAspheric15 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignAspheric153 ] )
        return 1.75;
    if( ![ design compare : ThicknessLensDesignAspheric156 ] )
        return 1.75;
    if( ![ design compare : ThicknessLensDesignAspheric159 ] )
        return 1.5;
    if( ![ design compare : ThicknessLensDesignAspheric16 ] )
        return 1.0;
    if( ![ design compare : ThicknessLensDesignAspheric167 ] )
        return 0.5;
    if( ![ design compare : ThicknessLensDesignAspheric174 ] )
        return 0.0;
    // Double Aspheric
    if( ![ design compare : ThicknessLensDesignDAspheric15 ] )
        return 2.0;
    if( ![ design compare : ThicknessLensDesignDAspheric153 ] )
        return 1.75;
    if( ![ design compare : ThicknessLensDesignDAspheric156 ] )
        return 1.75;
    if( ![ design compare : ThicknessLensDesignDAspheric159 ] )
        return 1.5;
    if( ![ design compare : ThicknessLensDesignDAspheric16 ] )
        return 1.0;
    if( ![ design compare : ThicknessLensDesignDAspheric167 ] )
        return 0.5;
    if( ![ design compare : ThicknessLensDesignDAspheric174 ] )
        return 0.0;
    
    return 0.;
}

- (void) updateDataForDrawWithPower:(float)power andCyl:(float)cyl andLeftLensType:(ThicknessLensDesignType*)leftLensDesign andRightLensType:(ThicknessLensDesignType*)rightLensDesign andPrecalibratedLeft:(bool)leftPrecalibrated andPrecalibratedRight:(bool)rightPrecalibrated andPrecalibratedDiam:(int)diam
{
    ThicknessCurveData * thicknessCurveDataLeftR1 = nil;
    ThicknessCurveData * thicknessCurveDataRightR1 = nil;
    ThicknessCurveData * thicknessCurveDataLeftR2 = nil;
    ThicknessCurveData * thicknessCurveDataRightR2 = nil;
    ThicknessCurveData * thicknessCurveDataLeftR4 = nil;
    ThicknessCurveData * thicknessCurveDataRightR4 = nil;
    ThicknessCurveData * thicknessCurveDataLeftET = nil;
    ThicknessCurveData * thicknessCurveDataRightET = nil;
    ThicknessCurveData * thicknessCurveDataLeftRB = nil;
    ThicknessCurveData * thicknessCurveDataRightRB = nil;
    bool bSecondSurface = true;
    bool bBaseSurface2 = true;
    int ix = 0;
    double dx = 2.5;
    if (cyl == 0.) {
		bSecondSurface = false;
        thicknessCurveDataLeftR1 = [self dataForLensWithTypeOfData:ThicknessDataTypeR1 lensType:leftLensDesign power:power];
        thicknessCurveDataRightR1 = [self dataForLensWithTypeOfData:ThicknessDataTypeR1 lensType:rightLensDesign power:power];
        thicknessCurveDataLeftR2 = [self dataForLensWithTypeOfData:ThicknessDataTypeR2 lensType:leftLensDesign power:power];
        thicknessCurveDataRightR2 = [self dataForLensWithTypeOfData:ThicknessDataTypeR2 lensType:rightLensDesign power:power];
    }
    else if (cyl < 0.) {
        // cyl - minus
        if (power <= 0.) {
            // power - minus, cyl - minus
            thicknessCurveDataLeftR1 = [self dataForLensWithTypeOfData:ThicknessDataTypeR1 lensType:leftLensDesign power:power+cyl];
            thicknessCurveDataRightR1 = [self dataForLensWithTypeOfData:ThicknessDataTypeR1 lensType:rightLensDesign power:power+cyl];
            // back surface, axis = 90
            thicknessCurveDataLeftR4 = [self dataForLensWithTypeOfData:ThicknessDataTypeR2 lensType:leftLensDesign power:power+cyl];
            thicknessCurveDataRightR4 = [self dataForLensWithTypeOfData:ThicknessDataTypeR2 lensType:rightLensDesign power:power+cyl];
            // back surface, axis = 0
            thicknessCurveDataLeftET = [self dataForLensWithTypeOfData:ThicknessDataTypeET lensType:leftLensDesign power:power];
            thicknessCurveDataRightET = [self dataForLensWithTypeOfData:ThicknessDataTypeET lensType:rightLensDesign power:power];
            bBaseSurface2 = false;
        }
        else {
            // power - plus, cyl - minus
            thicknessCurveDataLeftR1 = [self dataForLensWithTypeOfData:ThicknessDataTypeR1 lensType:leftLensDesign power:power];
            thicknessCurveDataRightR1 = [self dataForLensWithTypeOfData:ThicknessDataTypeR1 lensType:rightLensDesign power:power];
            // back surface, axis = 0
            thicknessCurveDataLeftR2 = [self dataForLensWithTypeOfData:ThicknessDataTypeR2 lensType:leftLensDesign power:power];
            thicknessCurveDataRightR2 = [self dataForLensWithTypeOfData:ThicknessDataTypeR2 lensType:rightLensDesign power:power];
            // back surface, axis = 90
            thicknessCurveDataLeftET = [self dataForLensWithTypeOfData:ThicknessDataTypeET lensType:leftLensDesign power:power+cyl];
            thicknessCurveDataRightET = [self dataForLensWithTypeOfData:ThicknessDataTypeET lensType:rightLensDesign power:power+cyl];
            bBaseSurface2 = true;
        }
    }
    else {
        // cyl - plus
        if (power <= 0.) {
            // power - minus, cyl - plus
            if (fabs(power) > fabs(cyl)) {
                thicknessCurveDataLeftR1 = [self dataForLensWithTypeOfData:ThicknessDataTypeR1 lensType:leftLensDesign power:power];
                thicknessCurveDataRightR1 = [self dataForLensWithTypeOfData:ThicknessDataTypeR1 lensType:rightLensDesign power:power];
                // back surface, axis = 90
                thicknessCurveDataLeftR4 = [self dataForLensWithTypeOfData:ThicknessDataTypeR2 lensType:leftLensDesign power:power];
                thicknessCurveDataRightR4 = [self dataForLensWithTypeOfData:ThicknessDataTypeR2 lensType:rightLensDesign power:power];
                // back surface, axis = 0
                thicknessCurveDataLeftET = [self dataForLensWithTypeOfData:ThicknessDataTypeET lensType:leftLensDesign power:power+cyl];
                thicknessCurveDataRightET = [self dataForLensWithTypeOfData:ThicknessDataTypeET lensType:rightLensDesign power:power+cyl];
	            bBaseSurface2 = false;
            }
            else {
                thicknessCurveDataLeftR1 = [self dataForLensWithTypeOfData:ThicknessDataTypeR1 lensType:leftLensDesign power:power+cyl];
                thicknessCurveDataRightR1 = [self dataForLensWithTypeOfData:ThicknessDataTypeR1 lensType:rightLensDesign power:power+cyl];
                // back surface, axis = 0
                thicknessCurveDataLeftR2 = [self dataForLensWithTypeOfData:ThicknessDataTypeR2 lensType:leftLensDesign power:power+cyl];
                thicknessCurveDataRightR2 = [self dataForLensWithTypeOfData:ThicknessDataTypeR2 lensType:rightLensDesign power:power+cyl];
                // back surface, axis = 90
                thicknessCurveDataLeftET = [self dataForLensWithTypeOfData:ThicknessDataTypeET lensType:leftLensDesign power:power];
                thicknessCurveDataRightET = [self dataForLensWithTypeOfData:ThicknessDataTypeET lensType:rightLensDesign power:power];
                bBaseSurface2 = true;
            }
        }
        else {
            // power - plus, cyl - plus
            thicknessCurveDataLeftR1 = [self dataForLensWithTypeOfData:ThicknessDataTypeR1 lensType:leftLensDesign power:power+cyl];
            thicknessCurveDataRightR1 = [self dataForLensWithTypeOfData:ThicknessDataTypeR1 lensType:rightLensDesign power:power+cyl];
            // back surface, axis = 0
            thicknessCurveDataLeftR2 = [self dataForLensWithTypeOfData:ThicknessDataTypeR2 lensType:leftLensDesign power:power+cyl];
            thicknessCurveDataRightR2 = [self dataForLensWithTypeOfData:ThicknessDataTypeR2 lensType:rightLensDesign power:power+cyl];
            // back surface, axis = 90
            thicknessCurveDataLeftET = [self dataForLensWithTypeOfData:ThicknessDataTypeET lensType:leftLensDesign power:power];
            thicknessCurveDataRightET = [self dataForLensWithTypeOfData:ThicknessDataTypeET lensType:rightLensDesign power:power];
            bBaseSurface2 = true;
        }
    }
    // Second surface
    if (bSecondSurface) {
        // Check surface
        if ((thicknessCurveDataLeftR1.point0.y == 0. && thicknessCurveDataLeftR1.point65.y == 0.) || (thicknessCurveDataLeftET.point0.y == 0. && thicknessCurveDataLeftET.point65.y == 0.)) {
            [thicknessCurveDataLeftR1 zeroSurface];
            [thicknessCurveDataLeftET zeroSurface];
            if (bBaseSurface2)
                [thicknessCurveDataLeftR2 zeroSurface];
            else
                [thicknessCurveDataLeftR4 zeroSurface];
        }
        if ((thicknessCurveDataRightR1.point0.y == 0. && thicknessCurveDataRightR1.point65.y == 0.) || (thicknessCurveDataRightET.point0.y == 0. && thicknessCurveDataRightET.point65.y == 0.)) {
            [thicknessCurveDataRightR1 zeroSurface];
            [thicknessCurveDataRightET zeroSurface];
            if (bBaseSurface2)
                [thicknessCurveDataRightR2 zeroSurface];
            else
                [thicknessCurveDataRightR4 zeroSurface];
        }
        // make surface left
        struct SurfaceData R1 = [thicknessCurveDataLeftR1 getSurfaceData];
        struct SurfaceData ET = [thicknessCurveDataLeftET getSurfaceData];
        struct SurfaceData R2;
        float etCenter;
        if (bBaseSurface2)
			etCenter = thicknessCurveDataLeftR2.point0.y - thicknessCurveDataLeftR1.point0.y;
        else
			etCenter = thicknessCurveDataLeftR4.point0.y - thicknessCurveDataLeftR1.point0.y;
        R2.power = 0.;
        R2.bAspheric = 0;
        for(ix = 0; ix < 14; ix++) {
            R2.x[ix] = (ix == 0)? 0. : R2.x[ix-1] + dx;
            R2.z[ix] = R1.z[ix] + ET.z[ix] + (etCenter - ET.z[0]);
        }
        if (bBaseSurface2)
			thicknessCurveDataLeftR4 = [[ThicknessCurveData alloc] initWithSurfaceData:R2];
        else
			thicknessCurveDataLeftR2 = [[ThicknessCurveData alloc] initWithSurfaceData:R2];
        // make surface right
        R1 = [thicknessCurveDataRightR1 getSurfaceData];
        ET = [thicknessCurveDataRightET getSurfaceData];
        if (bBaseSurface2)
			etCenter = thicknessCurveDataRightR2.point0.y - thicknessCurveDataRightR1.point0.y;
        else 
			etCenter = thicknessCurveDataRightR4.point0.y - thicknessCurveDataRightR1.point0.y;
        R2.power = 0.;
        R2.bAspheric = 0;
        for(ix = 0; ix < 14; ix++) {
            R2.x[ix] = (ix == 0)? 0. : R2.x[ix-1] + dx;
            R2.z[ix] = R1.z[ix] + ET.z[ix] + (etCenter - ET.z[0]);
        }
        if (bBaseSurface2)
			thicknessCurveDataRightR4 = [[ThicknessCurveData alloc] initWithSurfaceData:R2];
        else
			thicknessCurveDataRightR2 = [[ThicknessCurveData alloc] initWithSurfaceData:R2];
        // Build back curve left
        double Rlens = 65. / 2., y;
        struct SurfaceData leftRB;
        double zmin = thicknessCurveDataLeftR2.point65.y;
        double zmax = thicknessCurveDataLeftR4.point65.y;
        double k1 = zmin;
        double k2 = zmax - k1;
        for(ix = 0; ix < 14; ix++) {
            leftRB.x[ix] = (ix == 0)? 0. : leftRB.x[ix-1] + dx;
            y = sqrt(Rlens * Rlens - leftRB.x[ix] * leftRB.x[ix]);
            double alfa = atan2(y, leftRB.x[ix]);
            leftRB.z[ix] = k1 + k2 * sin(alfa) * sin(alfa);
        }
        thicknessCurveDataLeftRB = [[ThicknessCurveData alloc] initWithSurfaceData:leftRB];
        // Build back curve right
        struct SurfaceData rightRB;
        zmin = thicknessCurveDataRightR2.point65.y;
        zmax = thicknessCurveDataRightR4.point65.y;
        k1 = zmin;
        k2 = zmax - k1;
        for(ix = 0; ix < 14; ix++) {
            rightRB.x[ix] = (ix == 0)? 0. : rightRB.x[ix-1] + dx;
            y = sqrt(Rlens * Rlens - rightRB.x[ix] * rightRB.x[ix]);
            double alfa = atan2(y, rightRB.x[ix]);
            rightRB.z[ix] = k1 + k2 * sin(alfa) * sin(alfa);
        }
        thicknessCurveDataRightRB = [[ThicknessCurveData alloc] initWithSurfaceData:rightRB];
    }
    else {
        // Build back curve
        struct SurfaceData leftRB, rightRB;
        for(ix = 0; ix < 14; ix++) {
            leftRB.x[ix] = (ix == 0)? 0. : leftRB.x[ix-1] + dx;
            leftRB.z[ix] = thicknessCurveDataLeftR2.point65.y;
            rightRB.x[ix] = (ix == 0)? 0. : rightRB.x[ix-1] + dx;
            rightRB.z[ix] = thicknessCurveDataRightR2.point65.y;
        }
        thicknessCurveDataLeftRB = [[ThicknessCurveData alloc] initWithSurfaceData:leftRB];
        thicknessCurveDataRightRB = [[ThicknessCurveData alloc] initWithSurfaceData:rightRB];
    }
    // Update 3D draw data
    leftLensSurface[0] = [thicknessCurveDataLeftR1 getSurfaceData];
    leftLensSurface[1] = [thicknessCurveDataLeftR2 getSurfaceData];
    rightLensSurface[0] = [thicknessCurveDataRightR1 getSurfaceData];
    rightLensSurface[1] = [thicknessCurveDataRightR2 getSurfaceData];
    if (bSecondSurface) {
        leftLensSurface[2] = [thicknessCurveDataLeftR4 getSurfaceData];
        rightLensSurface[2] = [thicknessCurveDataRightR4 getSurfaceData];
    }
    leftLensSurface[0].bAspheric = leftLensSurface[1].bAspheric = leftLensSurface[2].bAspheric = [self getAsphericStatus:leftLensDesign];
    leftLensSurface[0].power = leftLensSurface[1].power = leftLensSurface[2].power = power;
    leftLensSurface[0].cyl = leftLensSurface[1].cyl = leftLensSurface[2].cyl = cyl;
    rightLensSurface[0].bAspheric = rightLensSurface[1].bAspheric = rightLensSurface[2].bAspheric = [self getAsphericStatus:rightLensDesign];
    rightLensSurface[0].power = rightLensSurface[1].power = rightLensSurface[2].power = power;
    rightLensSurface[0].cyl = rightLensSurface[1].cyl = rightLensSurface[2].cyl = cyl;
    // Update 2D draw data
    [self refreshDataWithThicknessCurveDataLR1:thicknessCurveDataLeftR1 dataLR2:thicknessCurveDataLeftR2 dataLR3:thicknessCurveDataLeftRB dataRR1:thicknessCurveDataRightR1 dataRR2:thicknessCurveDataRightR2 dataRR3:thicknessCurveDataRightRB];

    left2DSurface[0] = leftLensSurface[0];
    left2DSurface[1] = leftLensSurface[1];
    left2DSurface[2] = leftLensSurface[2];
    left2DSurface[3] = [thicknessCurveDataLeftRB getSurfaceData];
    left2DSurface[3].bAspheric = [self getAsphericStatus:leftLensDesign];
    left2DSurface[3].power = power;
    left2DSurface[3].cyl = cyl;
    right2DSurface[0] = rightLensSurface[0];
    right2DSurface[1] = rightLensSurface[1];
    right2DSurface[2] = rightLensSurface[2];
    right2DSurface[3] = [thicknessCurveDataRightRB getSurfaceData];
    right2DSurface[3].bAspheric = [self getAsphericStatus:rightLensDesign];
    right2DSurface[3].power = power;
    right2DSurface[3].cyl = cyl;

    CMatrixEngine engine;
    double regularDiam = (1 + diam / 10) * 10;
    double lensDiam;
    // left lens
    lensDiam = (leftPrecalibrated)? diam : regularDiam;
    if (lensDiam != 65.) {
        if(engine.CalibrateSurfaceData(&leftLensSurface[0], &leftLensSurface[1], &leftLensSurface[2], lensDiam))
        {
            left2DSurface[0] = engine.GetSurfaceData(sdForward);
            left2DSurface[1] = engine.GetSurfaceData(sdBack);
            left2DSurface[2] = engine.GetSurfaceData(sdAstigmaticBack);
            left2DSurface[3] = engine.GetSurfaceData(sdLineBack);
        }
    }
    // right lens
    lensDiam = (rightPrecalibrated)? diam : regularDiam;
    if (lensDiam != 65.) {
        if(engine.CalibrateSurfaceData(&rightLensSurface[0], &rightLensSurface[1], &rightLensSurface[2], lensDiam))
        {
            right2DSurface[0] = engine.GetSurfaceData(sdForward);
            right2DSurface[1] = engine.GetSurfaceData(sdBack);
            right2DSurface[2] = engine.GetSurfaceData(sdAstigmaticBack);
            right2DSurface[3] = engine.GetSurfaceData(sdLineBack);
        }
    }

    // Calculate lenses weight
    leftLensWeight = [self CulculateLensWeightForDesign:leftLensDesign R1:left2DSurface[0] R2:left2DSurface[1] R3:left2DSurface[2]];
    rightLensWeight = [self CulculateLensWeightForDesign:rightLensDesign R1:right2DSurface[0] R2:right2DSurface[1] R3:right2DSurface[2]];
}

- (double) CulculateLensWeightForDesign:(ThicknessLensDesignType*)lensDesign R1:(SurfaceData)R1 R2:(SurfaceData)R2 R3:(SurfaceData)R3
{
    double weight = 0.;
    
    double power = R1.power;
    double cyl = R1.cyl;
    long axis = 0;
    bool bAspheric = (R1.bAspheric)? true : false;
    double index = [self getMaterialIndex:lensDesign];
    double density = [self getMaterialDensity:lensDesign];
    double CenterThickness = 2.0;
    double EdgeThickness = 1.0;
    double ZeroThickness = 2.2;
    
    // contour
    ILens * Lens = Create_ILens();
    if (Lens) {
        Lens->SetSph( power );
        Lens->SetCyl( cyl );
        Lens->SetAxis( axis );
        Lens->SetRefIndex( index );
        Lens->SetDensity( density );
        if( bAspheric )
            Lens->SetDesign( wsAspheric.c_str() );
        else
            Lens->SetDesign( wsClassic.c_str() );
        Lens->SetPermittedCenterThickness(CenterThickness);
        Lens->SetPermittedEdgeThickness(EdgeThickness);
        Lens->SetPermittedZeroThickness(ZeroThickness);
        
        // contour
        IContour * Contour = Create_IContour();
        if (Contour) {
            const long LensDiameter = R1.diam;
            double Radius = LensDiameter / 2.;
            double xCenter = R1.diam / 2.;;
            double yCenter = R1.diam / 2.;
            int iPoint;
            const int Count = 50;
            for(iPoint = 0; iPoint < Count; iPoint++) {
                double x = 0., y = 0.;
                double alfa = iPoint * (360. / Count) * (M_PI / 180.);
                x = xCenter + Radius * cos(alfa);
                y = yCenter + Radius * sin(alfa);
                Contour->Add(x, y)->Release();
            }
            
            if(Contour->GetCount() > 3) {
                double minDiam = 0.;
                Lens->SetCenterX(xCenter);
                Lens->SetCenterY(yCenter);
                Lens->SetDiameter(LensDiameter);
                Lens->SetContour(Contour);
                minDiam = Lens->GetMinDiam();
                if(minDiam > LensDiameter)
                    Lens->SetDiameter(ceil(minDiam));
            }
            
            Contour->Release();
            Contour = NULL;
        }
        
        // Engine
        CMatrixEngine engine;
        engine.SetSurfaceData(&R1, &R2, &R3);
        // Set engine
        Lens->SetEngine2(&engine);
        // Set engine usage mask
        Lens->SetEngineUsageMask(wsSurfaceEngine2);
        
        // Calculate Weight
        weight = Lens->GetWeight();
        
        Lens->Release();
        Lens = NULL;
    }
    return weight;
}

- (void) initWSPicture:(IWSPicture *)pwsPicture fromUIImag:(UIImage *)srcImage
{
    if(!pwsPicture)
        return;
    
    NSInteger dxPicture = srcImage.size.width;
    NSInteger dyPicture = srcImage.size.height;
    
    int bytePerPixel = 4;
    long pic_SrcSize = dxPicture * dyPicture * bytePerPixel;
    long pic_TagSize = sizeof(BITMAPINFOHEADER) + dxPicture * dyPicture * 3;
    unsigned char * pSrcPicture = new unsigned char[pic_SrcSize];
    unsigned char * pTagPicture = new unsigned char[pic_TagSize];
    if(pSrcPicture && pTagPicture) {
        // Getting CGImage from UIImage
        CGImageRef imageRef1 = srcImage.CGImage;
        
        CGColorSpaceRef colorSpace1 = CGColorSpaceCreateDeviceRGB();
        CGContextRef contextRef1 = CGBitmapContextCreate(
                                                         pSrcPicture, dxPicture, dyPicture,
                                                         8, dxPicture * bytePerPixel,
                                                         colorSpace1, kCGImageAlphaPremultipliedLast|kCGBitmapByteOrderDefault
                                                         );
        // Drawing CGImage to CGContext
        CGContextDrawImage(
                           contextRef1,
                           CGRectMake(0, 0, dxPicture, dyPicture),
                           imageRef1
                           );
        
        memset(pTagPicture, 0, pic_TagSize);
        
        BITMAPINFOHEADER * bmih = (BITMAPINFOHEADER *)(pTagPicture);
        bmih->biSize = sizeof(BITMAPINFOHEADER);
        bmih->biWidth = dxPicture;
        bmih->biHeight = dyPicture;
        bmih->biPlanes = 1;
        bmih->biBitCount = 24;
        bmih->biCompression = 0;
        bmih->biSizeImage = dxPicture * dyPicture * 3;
        bmih->biXPelsPerMeter = 2834;
        bmih->biYPelsPerMeter = 2834;
        bmih->biClrUsed = 0;
        bmih->biClrImportant = 0;
        
        int iy, ix;
        unsigned char * pSrc, * pTag;
        for(iy = 0; iy < dyPicture; iy++) {
            pSrc = pSrcPicture + (dyPicture - iy - 1) * bytePerPixel * dxPicture;
            pTag = pTagPicture + sizeof(BITMAPINFOHEADER) + iy * 3 * dxPicture;
            for(ix = 0; ix < dxPicture; ix++) {
                int dxSrc = ix * bytePerPixel;
                int dxTag = ix * 3;
                pTag[dxTag+0] = pSrc[dxSrc+2];
                pTag[dxTag+1] = pSrc[dxSrc+1];
                pTag[dxTag+2] = pSrc[dxSrc+0];
            }
        }
        
        pwsPicture->SetPicture(pTagPicture);
        
        CGContextRelease(contextRef1);
        CGColorSpaceRelease(colorSpace1);
    };
    if(pSrcPicture)
        delete [] pSrcPicture;
    if(pTagPicture)
        delete [] pTagPicture;
}
- (UIImage *) getUIImageFrom:(IWSPicture *)pwsPicute
{
    UIImage * Photo;
    const unsigned char * psrcPicture = pwsPicute->GetPicture();
    if(psrcPicture) {
        BITMAPINFOHEADER * psrcbmih = (BITMAPINFOHEADER *)(psrcPicture);
        int dxPicture = psrcbmih->biWidth;
        int dyPicture = psrcbmih->biHeight;
        int bytePerPixel = psrcbmih->biBitCount / 8;
        
        unsigned char * pResPic;
        int res_size = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + dxPicture * dyPicture * bytePerPixel;
        pResPic = new unsigned char[res_size];
        if(pResPic) {
            memset(pResPic, 0, res_size);
            
            BITMAPFILEHEADER * bmfh = (BITMAPFILEHEADER *)pResPic;
            bmfh->bfType = 0x4D42;
            bmfh->bfSize = res_size;
            bmfh->bfReserved1 = 0;
            bmfh->bfReserved2 = 0;
            bmfh->bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
            
            BITMAPINFOHEADER * bmih = (BITMAPINFOHEADER *)(pResPic + sizeof(BITMAPFILEHEADER));
            bmih->biSize = sizeof(BITMAPINFOHEADER);
            bmih->biWidth = dxPicture;
            bmih->biHeight = dyPicture;
            bmih->biPlanes = 1;
            bmih->biBitCount = bytePerPixel * 8;
            bmih->biCompression = 0;
            bmih->biSizeImage = dxPicture * dyPicture * bytePerPixel;
            bmih->biXPelsPerMeter = 2834;
            bmih->biYPelsPerMeter = 2834;
            bmih->biClrUsed = 0;
            bmih->biClrImportant = 0;
            
            memcpy(pResPic + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER), psrcPicture + sizeof(BITMAPINFOHEADER), dxPicture * dyPicture * bytePerPixel);
            
            NSData* dataBMP = [NSData dataWithBytes:(const void *)pResPic length:res_size];
            //            Photo = [[UIImage alloc] initWithData:dataBMP];
            Photo = [UIImage imageWithData:dataBMP];
            
            delete [] pResPic;
        }
    }
    return Photo;
}
- (UIImage *) updatePhotoForDrawWithPower:(double)power andCyl:(double)cyl andAxis:(long)axis
                          andLeftLensType:(ThicknessLensDesignType*)leftLensDesign
                         andRightLensType:(ThicknessLensDesignType*)rightLensDesign
{
    UIImage * Photo = [UIImage imageNamed:@"PhotoWithoutFrame.jpg"];
    UIImage * PhotoFrame = [UIImage imageNamed:@"PhotoFrame.png"];
    
    CShellLens ShellLenses;
    
    double m_PWOFRightPupilX, m_PWOFRightPupilY, m_PWOFLeftPupilX, m_PWOFLeftPupilY;
    double m_PWFRightPupilX, m_PWFRightPupilY, m_PWFLeftPupilX, m_PWFLeftPupilY;
    double m_RightPD, m_LeftPD;
    
    m_PWOFRightPupilX = 371.;
    m_PWOFRightPupilY = 321.;
    m_PWOFLeftPupilX = 638.;
    m_PWOFLeftPupilY = 316.;
    
    m_PWFRightPupilX = 371.;
    m_PWFRightPupilY = 321.;
    m_PWFLeftPupilX = 638.;
    m_PWFLeftPupilY = 316.;
    
    m_RightPD = 32.;
    m_LeftPD = 32.;
    
    IContour * lpdispRightContour, * lpdispLeftContour;
    lpdispRightContour = Create_IContour();
    lpdispLeftContour = Create_IContour();
    
    FTPOINT rightContour[] = {
        450, 384,
        421, 410,
        394, 419,
        346, 421,
        302, 410,
        280, 393,
        264, 354,
        261, 319,
        267, 288,
        311, 268,
        394, 267,
        458, 285,
        469, 309,
        464, 348
    };
    FTPOINT leftContour[] = {
        732, 390,
        709, 409,
        662, 419,
        614, 416,
        587, 407,
        560, 381,
        545, 346,
        539, 307,
        551, 283,
        612, 265,
        697, 264,
        741, 281,
        749, 317,
        747, 353
    };
    for(int i = 0; i < (sizeof(rightContour)/sizeof(rightContour[0])); i++) {
        lpdispRightContour->Add(rightContour[i].x, rightContour[i].y)->Release();
    }
    for(int i = 0; i < (sizeof(leftContour)/sizeof(leftContour[0])); i++) {
        lpdispLeftContour->Add(leftContour[i].x, leftContour[i].y)->Release();
    }
    
    IWSPicture * lpdispPictureWithoutFrame, * lpdispPictureWithFrame;
    lpdispPictureWithoutFrame = Create_IWSPicture();
    lpdispPictureWithFrame = Create_IWSPicture();
    
    [self initWSPicture:lpdispPictureWithoutFrame fromUIImag:Photo];
    [self initWSPicture:lpdispPictureWithFrame fromUIImag:Photo];
    
    double m_RightOptCenOffsetX, m_RightOptCenOffsetY, m_LeftOptCenOffsetX, m_LeftOptCenOffsetY;
    m_RightOptCenOffsetX = 0;
    m_RightOptCenOffsetY = 0;
    m_LeftOptCenOffsetX = 0;
    m_LeftOptCenOffsetY = 0;
    
    ILens * lpdispRightLens, * lpdispLeftLens;
    lpdispRightLens = Create_ILens();
    lpdispLeftLens = Create_ILens();
    
    long nAsphericLeft = [self getAsphericStatus:leftLensDesign];
    long nAsphericRight = [self getAsphericStatus:rightLensDesign];
    double indexLeft = [self getMaterialIndex:leftLensDesign];
    double indexRight = [self getMaterialIndex:rightLensDesign];
    double densityLeft = [self getMaterialDensity:leftLensDesign];
    double densityRight = [self getMaterialDensity:rightLensDesign];
    double CenterThicknessLeft = [self getMinCenterThickness:leftLensDesign];
    double CenterThicknessRight = [self getMinCenterThickness:rightLensDesign];
    double EdgeThicknessLeft = [self getMinEdgeThickness:leftLensDesign];
    double EdgeThicknessRight = [self getMinEdgeThickness:rightLensDesign];
    double ZeroThicknessLeft = [self getZeroThickness:leftLensDesign];
    double ZeroThicknessRight = [self getZeroThickness:rightLensDesign];
    
    double corPower = power * 6. / 8.;
    corPower = 0.25 * Round(corPower / 0.25);
    double rightPower = (power == 0.)? 0. : ((power > 0.)? corPower + [self getPowerCorrection:rightLensDesign] : corPower - [self getPowerCorrection:rightLensDesign]);
    double leftPower = (power == 0.)? 0. : ((power > 0.)? corPower + [self getPowerCorrection:leftLensDesign] : corPower - [self getPowerCorrection:leftLensDesign]);
    // MyopSee - POWER correction for demonstration
    if(nAsphericRight == 2) {
        rightPower = 0.25 * Round((rightPower / 1.4) / 0.25);
    }
    if(nAsphericLeft == 2) {
        leftPower = 0.25 * Round((leftPower / 1.4) / 0.25);
    }
    
    lpdispRightLens->SetSph( rightPower);
    lpdispLeftLens->SetSph( leftPower);
    lpdispRightLens->SetCyl( cyl);
    lpdispLeftLens->SetCyl( cyl);
    lpdispRightLens->SetAxis( axis);
    lpdispLeftLens->SetAxis( axis);
    if(nAsphericRight > 0)
        lpdispRightLens->SetDesign( wsAspheric.c_str() );
    if(nAsphericLeft > 0)
        lpdispLeftLens->SetDesign( wsAspheric.c_str() );
    lpdispRightLens->SetRefIndex(indexRight);
    lpdispLeftLens->SetRefIndex(indexLeft);
    lpdispRightLens->SetDensity(densityRight);
    lpdispLeftLens->SetDensity(densityLeft);
    lpdispRightLens->SetPermittedCenterThickness(CenterThicknessRight);
    lpdispRightLens->SetPermittedEdgeThickness(EdgeThicknessRight);
    lpdispRightLens->SetPermittedZeroThickness(ZeroThicknessRight);
    lpdispLeftLens->SetPermittedCenterThickness(CenterThicknessLeft);
    lpdispLeftLens->SetPermittedEdgeThickness(EdgeThicknessLeft);
    lpdispLeftLens->SetPermittedZeroThickness(ZeroThicknessLeft);

    m_PWOFRightPupilY -= 2.5 * power / 8.;
    m_PWOFLeftPupilY  -= 3.5 * power / 8.;
    m_PWOFRightPupilX += 2.5 * power / 8.;
    m_PWOFLeftPupilX  -= 3.5 * power / 8.;
    
    ShellLenses.SetPupilsPWOF(m_PWOFRightPupilX, m_PWOFRightPupilY,
                              m_PWOFLeftPupilX, m_PWOFLeftPupilY);
    ShellLenses.SetPupilsPWF(m_PWFRightPupilX, m_PWFRightPupilY,
                             m_PWFLeftPupilX, m_PWFLeftPupilY);
    ShellLenses.SetPDs(m_RightPD, m_LeftPD);
    ShellLenses.SetContours(lpdispRightContour, lpdispLeftContour);
    ShellLenses.SetPictures(lpdispPictureWithoutFrame, lpdispPictureWithFrame);
    ShellLenses.SetOptCentOffsets(m_RightOptCenOffsetX, m_RightOptCenOffsetY,
                                  m_LeftOptCenOffsetX, m_LeftOptCenOffsetY);
    ShellLenses.SetLenses(lpdispRightLens, lpdispLeftLens);
    
    IWSPicture * lpdispResPicture;
    lpdispResPicture = Create_IWSPicture();
    
    long nRes = ShellLenses.InstallLens(lpdispResPicture, 0, 0, 100);
    UIImage * finalImage;
    if(nRes) {
        finalImage = Photo;
    }
    else {
        UIImage * resImage = [self getUIImageFrom:lpdispResPicture];
        
        CGSize size = CGSizeMake(resImage.size.width, resImage.size.height);
        UIGraphicsBeginImageContext(size);
        
        [resImage drawInRect:CGRectMake(0, 0, resImage.size.width, resImage.size.height)];
        [PhotoFrame drawInRect:CGRectMake(0, 0, PhotoFrame.size.width, PhotoFrame.size.height)];
        
        finalImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    lpdispRightContour->Release();
    lpdispLeftContour->Release();
    lpdispPictureWithoutFrame->Release();
    lpdispPictureWithFrame->Release();
    lpdispRightLens->Release();
    lpdispLeftLens->Release();
    lpdispResPicture->Release();
    
    return finalImage;
}

@end
