//
//  ThicknessCurveData.m
//  thickness
//
//  Created by VS on 30.04.12.
//

#import "ThicknessCurveData.h"
#import "DataSource.h"

@implementation ThicknessCurveData
@synthesize point0,point5,point10,point15,point20,point25,point30,point35,point40,point45,point50,point55,point60,point65;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (id)initWithDictonary:(NSDictionary*)dictonary
{
    dataDict = dictonary;
    [self updateData];
    
    return [self init];
}
- (id)initWithSurfaceData:(struct SurfaceData)data
{
    dataDict = nil;
    point0 = CGPointMake(data.x[0] * 2., data.z[0]);
    point5 = CGPointMake(data.x[1] * 2., data.z[1]);
    point10 = CGPointMake(data.x[2] * 2., data.z[2]);
    point15 = CGPointMake(data.x[3] * 2., data.z[3]);
    point20 = CGPointMake(data.x[4] * 2., data.z[4]);
    point25 = CGPointMake(data.x[5] * 2., data.z[5]);
    point30 = CGPointMake(data.x[6] * 2., data.z[6]);
    point35 = CGPointMake(data.x[7] * 2., data.z[7]);
    point40 = CGPointMake(data.x[8] * 2., data.z[8]);
    point45 = CGPointMake(data.x[9] * 2., data.z[9]);
    point50 = CGPointMake(data.x[10] * 2., data.z[10]);
    point55 = CGPointMake(data.x[11] * 2., data.z[11]);
    point60 = CGPointMake(data.x[12] * 2., data.z[12]);
    point65 = CGPointMake(data.x[13] * 2., data.z[13]);
    return [self init];
}
- (struct SurfaceData)getSurfaceData
{
    struct SurfaceData data;
    int ix = 0;
    double dx = 2.5;
    // forward surface - left
    data.power = 0.;
    data.cyl = 0.;
    data.axis = 0;
    data.bAspheric = 0;
    data.diam = 65;
    data.z[0] = point0.y;
    data.z[1] = point5.y;
    data.z[2] = point10.y;
    data.z[3] = point15.y;
    data.z[4] = point20.y;
    data.z[5] = point25.y;
    data.z[6] = point30.y;
    data.z[7] = point35.y;
    data.z[8] = point40.y;
    data.z[9] = point45.y;
    data.z[10] = point50.y;
    data.z[11] = point55.y;
    data.z[12] = point60.y;
    data.z[13] = point65.y;
    data.z[14] = 0.;
    data.z[15] = 0.;
    data.z[16] = 0.;
    for(ix = 0; ix < 17; ix++) {
        data.x[ix] = (ix == 0)? 0. : data.x[ix-1] + dx;
    }
    return data;
}

- (void) zeroSurface;
{
    point0.y = 0.;
    point5.y = 0.;
    point10.y = 0.;
    point15.y = 0.;
    point20.y = 0.;
    point25.y = 0.;
    point30.y = 0.;
    point35.y = 0.;
    point40.y = 0.;
    point45.y = 0.;
    point50.y = 0.;
    point55.y = 0.;
    point60.y = 0.;
    point65.y = 0.;
}

//-(void)updateWith 

- (void) updateData
{
    point0 = [self pointForKey:kThicknessElementCenter];
    point5 = [self pointForKey:@"5"];
    point10 = [self pointForKey:@"10"];
    point15 = [self pointForKey:@"15"];
    point20 = [self pointForKey:@"20"];
    point25 = [self pointForKey:@"25"];
    point30 = [self pointForKey:@"30"];
    point35 = [self pointForKey:@"35"];
    point40 = [self pointForKey:@"40"];
    point45 = [self pointForKey:@"45"];
    point50 = [self pointForKey:@"50"];
    point55 = [self pointForKey:@"55"];
    point60 = [self pointForKey:@"60"];
    point65 = [self pointForKey:@"65"];
}

- (CGPoint) pointForKey: (NSString*)key
{
    CGPoint point = CGPointMake([key floatValue], [[dataDict objectForKey:key] floatValue]);
   
    return point;
}

@end
