#include "stdafx.h"
#include "MatrixEngine.h"
#include "math.h"

CMatrixEngine::CMatrixEngine()
{
    bDataPresent = false;
    maxDiam = 70;
}
void CMatrixEngine::SetMaxDiam(long diam)
{
    if(diam >= 70 && diam <= 80) {
        maxDiam = diam;
    }
}
bool CMatrixEngine::SetSurfaceData(SurfaceData * pdR1, SurfaceData * pdR2, SurfaceData * pdR3)
{
    bool bRes = false;
    if(pdR1 && pdR2) {
        dR1 = *pdR1;
        dR2 = *pdR2;
        dR3 = *pdR3;
		bool bC1 = CalcCubicFunction(&dR1, A1, B1, D1);
		bool bC2 = CalcCubicFunction(&dR2, A2, B2, D2);
		bool bC3 = CalcCubicFunction(&dR3, A3, B3, D3);
		bool bC4 = CalcSquareFunction(&dR1, R1);
		bool bC5 = CalcSquareFunction(&dR2, R2);
		bool bC6 = CalcSquareFunction(&dR3, R3);
		bRes = bC1 && bC2 && bC3 && bC4 && bC5 && bC6;
    }
    bDataPresent = bRes;
    return bRes;
}
bool CMatrixEngine::CalibrateSurfaceData(SurfaceData * pdR1, SurfaceData * pdR2, SurfaceData * pdR3, long diam)
{
    int i;
    bool bRes = false;
    if (diam > maxDiam) {
        diam = maxDiam;
    }
    bRes = SetSurfaceData(pdR1, pdR2, pdR3);
    const int nSize = 1 + diam / 5 + ((diam % 5)? 1 : 0);
    if (bRes) {
        // forward surface
        dR1.diam = diam;
        if (diam % 5 > 0 || diam > pdR1->diam) {
            double RadiusVector = dR1.x[nSize - 1] = diam / 2.;
            if (dR1.bAspheric) {
                dR1.z[nSize - 1] = (A1 * RadiusVector * RadiusVector * RadiusVector + B1 * RadiusVector * RadiusVector + D1);
            }
            else {
                dR1.z[nSize - 1] = (R1 - sqrt(R1 * R1 - RadiusVector * RadiusVector) + dR1.z[0]);
            }
        }
        // Back surface
        dR2.diam = diam;
        if (diam % 5 > 0 || diam > pdR2->diam) {
            double RadiusVector = dR2.x[nSize - 1] = diam / 2.;
            if (dR2.bAspheric) {
                dR2.z[nSize - 1] = (A2 * RadiusVector * RadiusVector * RadiusVector + B2 * RadiusVector * RadiusVector + D2);
            }
            else {
                dR2.z[nSize - 1] = (R2 - sqrt(R2 * R2 - RadiusVector * RadiusVector) + dR2.z[0]);
            }
        }
        if (dR2.power > 0. && diam != pdR2->diam) {
            const int nSrcSize = 1 + pdR2->diam / 5 + ((pdR2->diam % 5)? 1 : 0);
            double dzSrc = pdR2->z[nSrcSize - 1] - pdR1->z[nSrcSize - 1];
            double dz = dR2.z[nSize - 1] - dR1.z[nSize - 1];
            for (i = 0; i < nSize; i++) {
                dR2.z[i] = dR2.z[i] - dz + dzSrc;
            }
        }
        if (dR1.cyl != 0.) { // Astigmatism
            // Back astigmatic curve
            dR3.diam = diam;
            if (diam % 5 > 0 || diam > pdR3->diam) {
                double RadiusVector = dR3.x[nSize - 1] = diam / 2.;
                if (dR3.bAspheric) {
                    dR3.z[nSize - 1] = (A3 * RadiusVector * RadiusVector * RadiusVector + B3 * RadiusVector * RadiusVector + D3);
                }
                else {
                    dR3.z[nSize - 1] = (R3 - sqrt(R3 * R3 - RadiusVector * RadiusVector) + dR3.z[0]);
                }
            }
            if (diam != pdR3->diam) {
                // Back surface in center
                double dz2 = dR2.z[0] - dR1.z[0];
                double dz3 = dR3.z[0] - dR1.z[0];
                for (i = 0; i < nSize; i++) {
                    dR3.z[i] = dR3.z[i] - dz3 + dz2;
                }
            }
            // Back astigmatic 2D line B
            dRB = dR1;
            double Rlens = diam / 2., y;
            double zmin = dR2.z[nSize - 1];
            double zmax = dR3.z[nSize - 1];
            double k1 = zmin;
            double k2 = zmax - k1;
            for(i = 0; i < nSize; i++) {
                y = sqrt(Rlens * Rlens - dRB.x[i] * dRB.x[i]);
                double alfa = atan2(y, dRB.x[i]);
                dRB.z[i] = k1 + k2 * sin(alfa) * sin(alfa);
            }
            // Back astigmatic 2D line A
            dRA = dR1;
            k1 = zmax;
            k2 = zmin - k1;
            for(i = 0; i < nSize; i++) {
                y = sqrt(Rlens * Rlens - dRA.x[i] * dRA.x[i]);
                double alfa = atan2(y, dRA.x[i]);
                dRA.z[i] = k1 + k2 * sin(alfa) * sin(alfa);
            }
        }
        else {
            dR3 = dR2;
            // Back 2D line
            dRB = dR1;
            for(i = 0; i < nSize; i++) {
                dRB.z[i] = dR2.z[nSize - 1];
            }
            dRA = dRB;
        }
        bool bC1 = CalcCubicFunction(&dR1, A1, B1, D1);
        bool bC2 = CalcCubicFunction(&dR2, A2, B2, D2);
        bool bC3 = CalcCubicFunction(&dR3, A3, B3, D3);
        bool bC4 = CalcSquareFunction(&dR1, R1);
        bool bC5 = CalcSquareFunction(&dR2, R2);
        bool bC6 = CalcSquareFunction(&dR3, R3);
        bRes = bC1 && bC2 && bC3 && bC4 && bC5 && bC6;
        bDataPresent = bRes;
    }
    return bRes;
}
SurfaceData CMatrixEngine::GetSurfaceData(SurfaceDataType sdType)
{
    SurfaceData surface;
    switch (sdType) {
        case sdForward:
            surface = dR1;
            break;
        case sdBack:
            surface = dR2;
            break;
        case sdAstigmaticBack:
            surface = dR3;
            break;
        case sdLineBack:
            surface = dRB;
            break;
        case sdLineBackA:
            surface = dRA;
            break;
        default:
            break;
    }
    return surface;
}
bool CMatrixEngine::CalcCubicFunction(SurfaceData * data, double& A, double& B, double& D)
{
    bool bRes = false;
    if(data) {
        int i;
        const int nSize = 1 + data->diam / 5 + ((data->diam % 5)? 1 : 0);
        double S3 = 0., S2 = 0., Sy = 0.;
        for(i = 1; i < nSize - 1; i++) {
            double s = data->x[i] * data->x[i];
            S2 += s;
            S3 += s * data->x[i];
            Sy += data->z[i];
        }
        double Yne = data->z[0] - data->z[nSize-1];
        double X3ne = data->x[0] * data->x[0] * data->x[0] - data->x[nSize-1] * data->x[nSize-1] * data->x[nSize-1];
        double X2ne = data->x[0] * data->x[0] - data->x[nSize-1] * data->x[nSize-1];
        double X3n = data->x[0] * data->x[0] * data->x[0];
        double X2n = data->x[0] * data->x[0];
        double Yn = data->z[0];
        
        double K1 = Sy - (S2 * Yne)/X2ne - (nSize - 2) * Yn + ((nSize - 2) * X2n * Yne)/X2ne;
        double K2 = S3 - (S2 * X3ne)/X2ne - (nSize - 2) * X3n + ((nSize - 2) * X2n * X3ne)/X2ne;
        
        A = K1 / K2;
        B = (Yne - A * X3ne)/X2ne;
        D = Yn - A * X3n - X2n * (Yne - A * X3ne)/X2ne;
        
        bRes = true;
    }
    return bRes;
}
bool CMatrixEngine::CalcSquareFunction(SurfaceData * data, double& R)
{
    bool bRes = false;
    if(data) {
        const int nSize = 1 + data->diam / 5 + ((data->diam % 5)? 1 : 0);
        R = (data->x[nSize-1] * data->x[nSize-1] + (data->z[nSize-1] - data->z[0]) * (data->z[nSize-1] - data->z[0])) / (2. * (data->z[nSize-1] - data->z[0]));
        bRes = true;
    }
    return bRes;
}
void CMatrixEngine::CalcSurfaceNormanls(double xDer, double yDer, double zDer, double& xN, double& yN, double& zN)
{
    xN = xDer / sqrt(xDer * xDer + yDer * yDer + zDer * zDer);
    yN = yDer / sqrt(xDer * xDer + yDer * yDer + zDer * zDer);
    zN = zDer / sqrt(xDer * xDer + yDer * yDer + zDer * zDer);
    
}

// ILensEngine interface
bool CMatrixEngine::CalcSurface(ILens * LensObject, ILensSurface * LensSurface)
{
	bool bRes = false;
	if(bDataPresent) {
		try
		{
			double xCenter = LensObject->GetCenterX();
			double yCenter = LensObject->GetCenterY();
			if(LensSurface) {
				vector<LensSurfPoint> * DArr = NULL;
				vector<LensSurfPoint>::size_type i, cElements = 0;

				DArr = LensSurface->GetArray();
				if(DArr)
					cElements = DArr->size();
				vector<LensSurfPoint>::iterator it;
				for(i = 0, it = DArr->begin(); i < cElements; i++, ++it)
				{
					double X = 0.;
					double Y = 0.;
					double Zf = 0.;
					double Zr = 0.;
                    double Xnf = 0., Xnr = 0.;
                    double Ynf = 0., Ynr = 0.;
                    double Znf = 0., Znr = 0.;
                    double der = 0., alfa = 0.;
                    double lXder = 0., lYder = 0., lZder = 1.;

					X = it->X - xCenter; Y = it->Y - yCenter;
					double RadiusVector = _hypot(X, Y);
                    if(dR1.bAspheric == 2 || (dR1.bAspheric == 1 && (dR1.power + dR1.cyl) > 0.)) {
                        Zf = -(A1 * RadiusVector * RadiusVector * RadiusVector + B1 * RadiusVector * RadiusVector + D1);
                        if (RadiusVector == 0.) {
                            Znf = 1.;
                            Xnf = 0.;
                            Ynf = 0.;
                        }
                        else {
                            der = -(A1 * 3. * RadiusVector * RadiusVector + B1 * 2. * RadiusVector);
                            alfa = atan(der);
                            Znf = cos(alfa);
                            Xnf = -(sin(alfa) * X / RadiusVector);
                            Ynf = -(sin(alfa) * Y / RadiusVector);
                        }
                    }
                    else {
                        Zf = -(R1 - sqrt(R1 * R1 - RadiusVector * RadiusVector) + dR1.z[0]);
                        if (RadiusVector == 0.) {
                            Znf = 1.;
                            Xnf = 0.;
                            Ynf = 0.;
                        }
                        else {
                            alfa = atan2(-RadiusVector, sqrt(R1 * R1 - RadiusVector * RadiusVector));
                            Znf = cos(alfa);
                            Xnf = -(sin(alfa) * X / RadiusVector);
                            Ynf = -(sin(alfa) * Y / RadiusVector);
                        }
                    }
                    if(dR2.bAspheric == 2 || (dR2.bAspheric == 1 && dR1.power < 0.) || dR1.cyl != 0.) {
                        // для астигматики всегда применяем кубическую функцию для задней поверхности
                        Zr = -(A2 * RadiusVector * RadiusVector * RadiusVector + B2 * RadiusVector * RadiusVector + D2);
                        if (RadiusVector == 0.) {
                            Znr = -1.;
                            Xnr = 0.;
                            Ynr = 0.;
                        }
                        else {
                            der = -(A2 * 3. * RadiusVector * RadiusVector + B2 * 2. * RadiusVector);
                            alfa = atan(der);
                            Znr = -cos(alfa);
                            Xnr = sin(alfa) * X / RadiusVector;
                            Ynr = sin(alfa) * Y / RadiusVector;
                        }
                    }
                    else {
                        Zr = -(R2 - sqrt(R2 * R2 - RadiusVector * RadiusVector) + dR2.z[0]);
                        if (RadiusVector == 0.) {
                            Znr = -1.;
                            Xnr = 0.;
                            Ynr = 0.;
                        }
                        else {
                            alfa = atan2(-RadiusVector, sqrt(R2 * R2 - RadiusVector * RadiusVector));
                            Znr = -cos(alfa);
                            Xnr = sin(alfa) * X / RadiusVector;
                            Ynr = sin(alfa) * Y / RadiusVector;
                        }
                    }
                    if (dR1.cyl != 0.) { // Astigmatism
                        // для астигматики всегда применяем кубическую функцию для задней поверхности
                        double Zr2 = 0.;
                        Zr2 = -(A3 * RadiusVector * RadiusVector * RadiusVector + B3 * RadiusVector * RadiusVector + D3);

                        // Culc back surface point
                        double k1 = Zr;
                        double k2 = Zr2 - Zr;
                        double alfa = atan2(Y, X) - ((180 - dR1.axis) * M_PI / 180);
                        Zr = k1 + k2 * sin(alfa) * sin(alfa);
                        
                        double lXderRot, lYderRot;
                        if (RadiusVector == 0.) {
                            lZder = -1.;
                            lXder = 0.;
                            lYder = 0.;

                            lXderRot = 0.;
                            lYderRot = 0.;
                        }
                        else {
                            double A4 = A2 + (A3 - A2) * sin(alfa) * sin(alfa);
                            double B4 = B2 + (B3 - B2) * sin(alfa) * sin(alfa);
                            
                            lXder = -(A4 * 3. * RadiusVector * RadiusVector + B4 * 2. * RadiusVector);
                            double alfaY = alfa + M_PI_2;
                            lYder = -(2. * k2 * sin(alfaY) * cos(alfaY)) / RadiusVector;
                            lZder = -(1.);
                            
                            lXderRot = lXder * (X / RadiusVector) - lYder * (Y / RadiusVector);
                            lYderRot = lXder * (Y / RadiusVector) + lYder * (X / RadiusVector);
                        }
                        CalcSurfaceNormanls(lXderRot, lYderRot, lZder, Xnr, Ynr, Znr);
                    }
					it->zRear = Zr;
					it->zFront = Zf;
                    it->xnRear = Xnr;
                    it->ynRear = Ynr;
                    it->znRear = Znr;
                    it->xnFront = Xnf;
                    it->ynFront = Ynf;
                    it->znFront = Znf;
				}
				bRes = true;
			}
		}
		catch(...)
		{
		}
	}

	return bRes;
}

bool CMatrixEngine::CalcWeight(ILens * LensObject, double* ResultWeight)
{
	*ResultWeight = 1.;
	return true;
}

bool CMatrixEngine::CalcThickness(ILens * LensObject, double* ResultCenterThickness, double* ResultMinEdgeThickness, double* ResultMaxEdgeThickness)
{
	*ResultCenterThickness = 1.5;
	*ResultMinEdgeThickness = 2.;
	*ResultMaxEdgeThickness = 5.;
	return true;
}

bool CMatrixEngine::CalcUVProtection(ILens * LensObject, double* ResultUVProtection)
{
	*ResultUVProtection = 80.;
	return true;
}

unsigned long CMatrixEngine::GetSupportedMethods()
{
	return wsWeightSupported | wsSurfaceSupported | wsThicknessSupported | wsUVProtectionSupported;
}

void CMatrixEngine::GetDepth(ILens* LensObject, double x, double y, double& rear, double& front)
{
	CObjPtr<ILensSurface> Grid;
	vector<LensSurfPoint> DArr(1), * pDArr;
		DArr[0].X = x;
		DArr[0].Y = y;
		DArr[0].zRear = 0.;
		DArr[0].zFront = 0.;
    try {
        ILensSurface * pGrid = Create_ILensSurface();
        if (pGrid) {
            Grid.AttachDispatch(pGrid);
        }
		Grid->Clear();
        
		if(!Grid->AddArray(DArr))
			return;
        
		if(!CalcSurface(LensObject, Grid)){
            return;
		}
        DArr.clear();
        pDArr = Grid->GetArray();
        if (pDArr->size() == 1) {
            rear = (*pDArr)[0].zRear;
            front = (*pDArr)[0].zFront;
        }
	}
    catch(...)
	{
		return;
	}
}

string CMatrixEngine::GetLastErrorMessage()
{
	return "";
}

ILensEngine * CMatrixEngine::Clone()
{
	return this;
}

// IObjectBase interface
unsigned long CMatrixEngine::AddRef()
{
	return 1;
}
unsigned long CMatrixEngine::Release()
{
	return 1;
}
bool CMatrixEngine::WriteToSteam(ostream * pOutStream)
{
	return true;
}
bool CMatrixEngine::ReadFromSteam(istream * pInStream)
{
	return true;
}

