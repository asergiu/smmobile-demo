//
//  UIWindow+VisibleController.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 9/22/15.
//  Copyright © 2015 ACEP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (VisibleController)

- (UIViewController *)visibleViewController;

@end
