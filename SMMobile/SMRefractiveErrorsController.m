//
//  SMRefractiveErrorsController.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 10/16/14.
//

#import "SMRefractiveErrorsController.h"
#import "UIViewController+ThumbnailsAnimation.h"
#import "SMAppManager.h"
#import "SMGELeftView.h"
#import "SMGERightView.h"
#import "SMInfoViewController.h"

typedef NS_ENUM (NSInteger, SMDeffectType) {
    SMDeffectTypeMyopia,
    SMDeffectTypeHyperopia,
    SMDeffectTypePresbyopia,
    SMDeffectTypeAstigmatism
};

@interface SMRefractiveErrorsController () <SMInfoViewControllerDelegate>
{
    SMInfoViewController *infoContoller;
    UIView *backgroundView;
    
    IBOutlet UISegmentedControl *deffectsControl;
    
    IBOutlet SMGELeftView * leftView;
    IBOutlet SMGERightView * rightView;
    
    IBOutlet UILabel *symptomTitle;
    IBOutlet UILabel *symptomSubTitle;
    IBOutlet UITextView *symptomDesc;
    
    IBOutlet UIButton * playButton;
    IBOutlet UIButton * stopButton;
    
    SMDeffectType currenDeffect;
}

- (IBAction)homePressed:(id)sender;
- (IBAction)switchDeffect:(UISegmentedControl *)sender;
- (IBAction)toggleAnimation:(UIButton *)sender;
- (IBAction)resetAnimation:(UIButton *)sender;
- (IBAction)showInfo:(id)sender;

@end

@implementation SMRefractiveErrorsController

extern int globalLensShape;

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    currenDeffect = SMDeffectTypeMyopia;
    
    [deffectsControl setTitle:NSLocalizedString(@"Myopia", @"") forSegmentAtIndex:0];
    [deffectsControl setTitle:NSLocalizedString(@"Hyperopia", @"") forSegmentAtIndex:1];
    [deffectsControl setTitle:NSLocalizedString(@"Presbyopia", @"") forSegmentAtIndex:2];
    [deffectsControl setTitle:NSLocalizedString(@"Astigmatism", @"") forSegmentAtIndex:3];
    
    symptomTitle.text = [self mainTitleForDeffect:YES];
    symptomSubTitle.text = [self mainTitleForDeffect:NO];
    symptomDesc.text = [self descriptionForDeffect];
    
    [ leftView play ];
    [ rightView play ];
    
    [ leftView setShape : globalLensShape ];
    
    [ leftView setState : currenDeffect ];
    [ rightView setState : currenDeffect ];
    
    playButton.selected = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self moduleEntered];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [ leftView stopWithDeallocCam:NO ];
    [ rightView stopWithDeallocCam:NO ];
    
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSString *)mainTitleForDeffect:(BOOL)isMainTitle
{
    switch (currenDeffect) {
        case SMDeffectTypeMyopia:
            if (isMainTitle)
                return NSLocalizedString(@"Myopia: shortsight", nil);
            else
                return NSLocalizedString(@"MYOPIA_SYMPTOM", nil);
            break;
        case SMDeffectTypeHyperopia:
            if (isMainTitle)
                return NSLocalizedString(@"Hyperopia: longsight", nil);
            else
                return NSLocalizedString(@"HYPEROPIA_SYMPTOM", nil);
            break;
        case SMDeffectTypePresbyopia:
            if (isMainTitle)
                return NSLocalizedString(@"Presbyopia:", nil);
            else
                return NSLocalizedString(@"PRESBYOPIA_SYMPTOM", nil);
            break;
        case SMDeffectTypeAstigmatism:
            if (isMainTitle)
                return NSLocalizedString(@"Astigmatism:", nil);
            else
                return NSLocalizedString(@"ASTIGMATISM_SYMPTOM", nil);
            break;
    }
}

- (NSString *)descriptionForDeffect
{
    switch (currenDeffect) {
        case SMDeffectTypeMyopia:
            return NSLocalizedString(@"MYOPIA_DESCRIPTION", nil);
            break;
        case SMDeffectTypeHyperopia:
            return NSLocalizedString(@"HYPEROPIA_DESCRIPTION", nil);
            break;
        case SMDeffectTypePresbyopia:
            return NSLocalizedString(@"PRESBYOPIA_DESCRIPTION", nil);
            break;
        case SMDeffectTypeAstigmatism:
            return NSLocalizedString(@"ASTIGMATISM_DESCRIPTION", nil);
            break;
    }
}

#pragma mark Actions

- (void)homePressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)switchDeffect:(UISegmentedControl *)sender
{
    currenDeffect = sender.selectedSegmentIndex;
    
    symptomTitle.text = [self mainTitleForDeffect:YES];
    symptomSubTitle.text = [self mainTitleForDeffect:NO];
    symptomDesc.text = [self descriptionForDeffect];
    
    [ leftView setState : currenDeffect ];
    [ rightView setState : currenDeffect ];
    
    playButton.selected = YES;
}

- (IBAction)toggleAnimation:(UIButton*)sender
{
    playButton.selected = !playButton.selected;
    
    if( leftView.isFinished )
        [ leftView move : -9999 ];
    if( rightView.isFinished )
        [ rightView move : -9999 ];
    
    [ leftView setAnimate : playButton.selected ];
    [ rightView setAnimate : playButton.selected ];
}

- (IBAction)resetAnimation:(UIButton*)sender
{
    playButton.selected = NO;
    
    [ leftView setAnimate : NO ];
    [ rightView setAnimate : NO ];
    
    [ leftView move : 9999 ];
    [ rightView move : 9999 ];
}

#pragma mark - Back View For Popovers

- (void)placeBackgoundView
{
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024.f, 1024.f)];
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0;
    [self.view addSubview:backgroundView];
    
    [UIView animateWithDuration:0.4f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        backgroundView.alpha = 0.4f;
    } completion:nil];
}

- (void)removeBackgoundView
{
    [UIView animateWithDuration:0.4f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        backgroundView.alpha = 0;
    } completion:^(BOOL finished){
        [backgroundView removeFromSuperview];
        backgroundView = nil;
    }];
}

#pragma mark Actions

- (void)showInfo:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [self placeBackgoundView];
    
    if (!infoContoller)
    {
        infoContoller = [storyboard instantiateViewControllerWithIdentifier:@"infoViewController"];
        
        infoContoller.delegate = self;
        
        infoContoller.view.frame = CGRectMake((self.view.bounds.size.width - 540.f)/2,
                                              (self.view.bounds.size.height - 650.f)/2,
                                              540.f,
                                              650.f);
        
        [infoContoller setInfoTextForType:(currenDeffect + 2)];
        
        [self.view addSubview:infoContoller.view];
        
        [infoContoller.view showInfoViewInPoint:CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2)];
    }
}

- (void)dismissInfo
{
    [infoContoller.view hideInfoViewWithAnimation];
    infoContoller = nil;
    
    [self removeBackgoundView];
}

#pragma mark Autorotate

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{    
    playButton.selected = NO;
    
    [ leftView setAnimate : NO ];
    [ rightView setAnimate : NO ];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

#pragma mark Google Analytics

- (void)moduleEntered
{
    [APP_MANAGER sendAnalyticsEnteredModuleWithName:@"SMRefractiveErrors"];
}

@end
