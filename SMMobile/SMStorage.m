#import "SMStorage.h"

@implementation SMStorage

@synthesize moviePlayerHelper;
@synthesize dublicateMoviePlayerHelper;

+ (SMStorage *)sharedInstance
{
    static dispatch_once_t pred = 0;
    
    __strong static SMStorage *_sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[SMStorage alloc] init];
        _sharedObject.moviePlayerHelper = [SMMoviePLayerHelper new];
        _sharedObject.dublicateMoviePlayerHelper = [SMMoviePLayerHelper new];
    });
    
    return _sharedObject;
}

@end
