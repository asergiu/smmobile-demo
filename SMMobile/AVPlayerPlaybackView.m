//
//  AVPlayerPlaybackView.m
//
//  Created by VS on 02.05.12.
//

#import "AVPlayerPlaybackView.h"
#import <AVFoundation/AVFoundation.h>

@implementation AVPlayerPlaybackView

+ (Class)layerClass
{
    return [AVPlayerLayer class];
}

- (AVPlayer *)player
{
    return [(AVPlayerLayer *)[self layer] player];
}

- (void)setPlayer:(AVQueuePlayer *)player
{
        
    [(AVPlayerLayer *)[self layer] setPlayer:player];
}

- (void)clearLayer
{
    [(AVPlayerLayer *)[self layer] removeFromSuperlayer];
}

- (void)setVideoFillMode:(NSString *)fillMode
{
    AVPlayerLayer *playerLayer = (AVPlayerLayer *)[self layer];
    playerLayer.videoGravity = fillMode;
}

@end