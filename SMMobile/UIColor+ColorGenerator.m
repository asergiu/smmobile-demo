//
//  UIColor+ColorGenerator.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 8/5/14.
//

#import "UIColor+ColorGenerator.h"

@implementation UIColor (ColorGenerator)

+ (UIColor *)R:(NSUInteger)red
             G:(NSUInteger)green
             B:(NSUInteger)blue
{
    return [UIColor colorWithRed:(float)(red/255.f)
                           green:(float)(green/255.f)
                            blue:(float)(blue/255.f)
                           alpha:1.f];
}

+ (UIColor *)R:(NSUInteger)red
             G:(NSUInteger)green
             B:(NSUInteger)blue
             A:(NSUInteger)alpha
{
    return [UIColor colorWithRed:(float)(red/255.f)
                           green:(float)(green/255.f)
                            blue:(float)(blue/255.f)
                           alpha:(float)(alpha/255.f)];
}

@end
