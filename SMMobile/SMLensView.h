//
//  SMLensView.h
//  ACEPSmartMirror
//
//  Created by Dgut on 02.05.12.
//

#import "GLKView_SmartMirror.h"

@interface SMLensView : GLKView_SmartMirror

@property ( nonatomic, weak ) IBOutlet UIView * designView;

-( void )addBackground : ( NSString * )name;
-( void )setBackground : ( int )index;
-( void )setPower : ( float )power;
-( void )setCyl : ( float )cyl;
-( void )setAxis : ( float )axis;

-( void )setDesign : ( int )design;
-( void )setIndex : ( float )index;
-( void )setShape : ( NSInteger )aShape;

-( void )renderDesignView;

@end
