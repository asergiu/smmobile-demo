//
//  Thickness3DView.m
//  Thickness
//
//  Created by Dgut on 05.08.14.
//

#import "SMThickness3DView.h"

#import "SMCommon.h"
#import "SMModel.h"
#import "SMShader.h"
#import "SMBuffer.h"
#import "SMContour.h"
#import "SMOldEngine.h"
#import "SMGlasses.h"
#import "SMGlassesDynamic.h"
#import "SMLens.h"
#import "SMMannequin.h"

#import "PDDeviceInfo.h"
//#import "PDMDataManager.h"

SMModel * const thickness3DGetModel( const char * name )
{
    return [ SMThickness3DView.currentView getModel : name ];
}

SMShader * const thickness3DGetShader( const char * name )
{
    return [ SMThickness3DView.currentView getShader : name ];
}

GLKTextureInfo * const thickness3DGetTexture( const char * name )
{
    return [ SMThickness3DView.currentView getTexture : name ];
}

GLKTextureInfo * const thickness3DGetCubemap( const char * name )
{
    return [ SMThickness3DView.currentView getCubemap : name ];
}

SMBuffer * const thickness3DGetBuffer( void )
{
    return [ SMThickness3DView.currentView getBuffer ];
}

SMBuffer * const thickness3DGetInternalBuffer( void )
{
    return [ SMThickness3DView.currentView getInternalBuffer ];
}

SMBuffer * const thickness3DGetBackgroundBuffer( void )
{
    return [ SMThickness3DView.currentView getBackgroundBuffer ];
}

//static const float WOMAN_SCALE = 0.029 / 3.2231;
static const float WOMAN_SCALE = 0.029 / 3.22310;
static const float FRAME_SCALE = WOMAN_SCALE;//0.0095f;
static const float IPAD3_SCREEN_WIDTH = 196.f;
static const float FRAME_FULLSCREEN_SCALE = 13.75f;

using namespace std;

@interface SMThickness3DView()
{
    SMGlassesBase * womanGlasses;
    SMGlassesDynamic * womanGlassesDynamic;
    SMGlassesBase * manGlasses;
    SMGlassesDynamic * manGlassesDynamic;
    
    SMGlassesBase * currentGlasses;
    
    SMMannequin * womanMannequin;
    SMMannequin * manMannequin;
    
    SMMannequin * currentMannequin;
    
    GLKTextureInfo * background;
    
    float yaw;
    float pitch;
    
    CGPoint dragPoint;
    
    float power;
    float cyl;
    float axis;
    
    BOOL split;
    
    int person;
    BOOL dynamicGlasses;
    
    //SMContour contourLeft;
    
    SMOldEngine engine;
    
    std::map< std::string, SMModel * > models;
    std::map< std::string, SMShader * > shaders;
    std::map< std::string, GLKTextureInfo * > textures;
    
    SMBuffer * buffer;
    SMBuffer * internalBuffer;
    SMBuffer * backgroundBuffer;
    
    //float frameWidth;
    float pupillaryDistance;
    bool showPerson;
    bool showFrame;
    
    BOOL otherDragging;
    
    float test0;
    float test1;
    
    BOOL iPad2;
    
    bool readyToDraw;
}

@end

@implementation SMThickness3DView

-( void )loadThings
{
    if( readyToDraw )
        return;
    
    SMThickness3DView.currentView = self;
    
    [ EAGLContext setCurrentContext : self.context ];
    
    buffer = new SMBuffer( 1024 + 512, 1024 + 512 );
    internalBuffer = new SMBuffer( 1024 + 512, 1024 + 512 );
    backgroundBuffer = new SMBuffer( 1024 + 512, 1024 + 512 );
    
    // preload
    thickness3DGetShader( "ThicknessLens" );
    thickness3DGetShader( "ThicknessOneLight" );
    thickness3DGetShader( "ThicknessOneLightClip" );
    thickness3DGetShader( "ThicknessOneTexture" );
    thickness3DGetShader( "ThicknessQuick" );
    
    if( !iPad2 )
    {
        SMModel * model = thickness3DGetModel( "Female_body_05_10_16.3ds" );
        model->SetTextureForMesh( "girl_bikin", thickness3DGetTexture( "bikini_col.jpg" ) );
        model->SetTextureForMesh( "girl_head_", thickness3DGetTexture( "body_color.jpg" ) );
        model->SetTextureForMesh( "girl_lashe", thickness3DGetTexture( "eyelashes.jpg" ) );
        model->SetTextureForMesh( "girl_eye_c", thickness3DGetTexture( "body_color.jpg" ) );
        model->SetTextureForMesh( "girl_eyes_", thickness3DGetTexture( "eyes.jpg" ) );
        model->SetTextureForMesh( "girl_hairt", thickness3DGetTexture( "tail_texture.jpg" ) );
        model->SetTextureForMesh( "girl_hairr", thickness3DGetTexture( "rubber.jpg" ) );
        model->SetTextureForMesh( "girl_teeth", thickness3DGetTexture( "teeth.jpg" ) );
        
        womanMannequin = new SMMannequin( model );
        womanMannequin->SetScale( FRAME_SCALE );
    }
    
    if( !iPad2 )
    {
        SMModel * model = thickness3DGetModel( "Mark_avatar_01.3ds" );
        model->SetTextureForMesh( "Mark_head", thickness3DGetTexture( "Mark_face_01.jpg" ) );
        model->SetTextureForMesh( "m_bottons", thickness3DGetTexture( "botton.jpg" ) );
        model->SetTextureForMesh( "m_lashes", thickness3DGetTexture( "eyelashes.jpg" ) );
        model->SetTextureForMesh( "m_eyes_s", thickness3DGetTexture( "Mark_face_01.jpg" ) );
        model->SetTextureForMesh( "m_eyes", thickness3DGetTexture( "Mark_eye_Diffuse_256x.jpg" ) );
        model->SetTextureForMesh( "m_jacket", thickness3DGetTexture( "Jacket_Shirt.jpg" ) );
        //model->SetTextureForMesh( "unnamed_o6", thickness3DGetTexture( "Jacket_Shirt.jpg" ) );
        
        manMannequin = new SMMannequin( model );
        manMannequin->SetScale( FRAME_SCALE );
    }
    
    {
        SMModel * frame = thickness3DGetModel( "girl_frame_01.3ds" );
        frame->SetTextureForMesh( "girl_frame", thickness3DGetTexture( "glasses_frame.jpg" ) );
        frame->SetTextureForMesh( "girl_wire_", thickness3DGetTexture( "glasses_frame.jpg" ) );
        frame->SetTextureForMesh( "girl_pads_", thickness3DGetTexture( "glasses_pads.png" ) );
        
        SMModel * ears = thickness3DGetModel( "girl_ears_s_01.3ds" );
        ears->SetTextureForMesh( "girl_ears_", thickness3DGetTexture( "glasses_frame.jpg" ) );
        
        SMGlasses * glasses = new SMGlasses( "contour_left.txt", frame, ears );
        glasses->SetScale( FRAME_SCALE );
        glasses->SetShift( GLKVector3Make( 0, 0.085f, 10.645f + 0.08f ) );
        
        glasses->GetContour()->SetStep( 20. );
        
        womanGlasses = glasses;
    }
    
    {
        GLKTextureInfo * texture = thickness3DGetTexture( "Rod_girl_frames_01.png" );
        
        SMModel * bridge = thickness3DGetModel( "girl_rod_bridge.3ds" );
        SMModel * frame_left = thickness3DGetModel( "girl_rod_frame_left.3ds" );
        SMModel * ear_left = thickness3DGetModel( "girl_rod_ear_left.3ds" );
        
        bridge->SetTexture( texture );
        frame_left->SetTexture( texture );
        ear_left->SetTexture( texture );
        
        SMGlassesDynamic * glasses = new SMGlassesDynamic( "contour_girl_rod.txt", 1.72f, bridge, frame_left, ear_left );
        glasses->SetScale( FRAME_SCALE );
        glasses->SetShift( GLKVector3Make( 0, 0.085f, 10.645f + 0.08f ) );
        glasses->SetAxis( GLKVector3Make( 6.69314, 36.69057, -9.37072 ) );
        glasses->SetEar( GLKVector3Make( 7.61164, 36.63073, -1.78251 ) );
        
        glasses->GetContour()->SetStep( 20. );
        
        womanGlassesDynamic = glasses;
    }
    
    {
        SMModel * frame = thickness3DGetModel( "mark_frame_01.3ds" );
        frame->SetTextureForMesh( "mark_frame", thickness3DGetTexture( "glasses_frame.jpg" ) );
        frame->SetTextureForMesh( "mark_wire_", thickness3DGetTexture( "glasses_frame.jpg" ) );
        frame->SetTextureForMesh( "mark_pads_", thickness3DGetTexture( "glasses_pads.png" ) );
        
        SMModel * ears = thickness3DGetModel( "mark_ears_s_01.3ds" );
        ears->SetTextureForMesh( "mark_ears_", thickness3DGetTexture( "glasses_frame.jpg" ) );
        
        //showFrame = true;
        
        SMGlasses * glasses = new SMGlasses( "contour.txt", frame, ears );
        glasses->SetScale( FRAME_SCALE );
        glasses->SetShift( GLKVector3Make( 0, 0.05f, 11.3f ) );
        
        glasses->GetContour()->SetStep( 20. );
        
        manGlasses = glasses;
    }
    
    {
        GLKTextureInfo * texture = thickness3DGetTexture( "Rod_mark_glasses_01.png" );
        
        SMModel * bridge = thickness3DGetModel( "mark_rod_bridge.3ds" );
        SMModel * frame_left = thickness3DGetModel( "mark_rod_frame_left.3ds" );
        SMModel * ear_left = thickness3DGetModel( "mark_rod_ear_left.3ds" );
        
        bridge->SetTexture( texture );
        frame_left->SetTexture( texture );
        ear_left->SetTexture( texture );
        
        SMGlassesDynamic * glasses = new SMGlassesDynamic( "contour_mark_rod.txt", 1.85f, bridge, frame_left, ear_left );
        glasses->SetScale( FRAME_SCALE );
        glasses->SetShift( GLKVector3Make( 0, /*-0.295f*/0, 11.3f ) );
        //glasses->SetDBL( 5.f );
        glasses->SetAxis( GLKVector3Make( 6.17449, 36.49044, -10.15453 ) );
        glasses->SetEar( GLKVector3Make( 7.77899, 36.43216, -0.87229 ) );
        
        glasses->GetContour()->SetStep( 20. );
        
        manGlassesDynamic = glasses;
    }
    
    background = thickness3DGetTexture( "sm2-main-menu_bg.png" );
    
    /*if( !currentGlasses )
     currentGlasses = womanGlasses;
     if( !currentMannequin )
     currentMannequin = womanMannequin;*/
    
    thickness3DGetShader( "ThicknessOneLight" )->Uniform1( "ambientFactor", 0.5f );
    thickness3DGetShader( "ThicknessOneLight" )->Uniform1( "specularFactor", 0.f );
    thickness3DGetShader( "ThicknessOneLight" )->Uniform1( "specularPower", 1.f );
    
    thickness3DGetShader( "ThicknessOneLightClip" )->Uniform1( "ambientFactor", 0.5f );
    thickness3DGetShader( "ThicknessOneLightClip" )->Uniform1( "specularFactor", 0.f );
    thickness3DGetShader( "ThicknessOneLightClip" )->Uniform1( "specularPower", 1.f );
    
    engine.SetPower( power );
    engine.SetCyl( cyl );
    engine.SetAxis( axis );
    
    readyToDraw = true;
    
    [ self setPerson : person ];
    
    SMThickness3DView.currentView = 0;
}

-( void )clearThings
{
    if( !readyToDraw )
        return;
    
    [ EAGLContext setCurrentContext : self.context ];
    
    SMThickness3DView.currentView = self;
    
    gCommon.Clear();
    
    for( map< string, SMModel * >::iterator i = models.begin(); i != models.end(); i++ )
        delete i->second;
    models.clear();
    
    for( map< string, SMShader * >::iterator i = shaders.begin(); i != shaders.end(); i++ )
        delete i->second;
    shaders.clear();
    
    for( map< string, GLKTextureInfo * >::iterator i = textures.begin(); i != textures.end(); i++ )
    {
        GLuint t = i->second.name;
        glDeleteTextures( 1, &t );
    }
    textures.clear();
    
    GLuint name = background.name;
    glDeleteTextures(1, &name);
    
    delete buffer;
    buffer = 0;
    
    delete internalBuffer;
    internalBuffer = 0;
    
    delete backgroundBuffer;
    backgroundBuffer = 0;
    
    delete womanGlasses;
    delete womanGlassesDynamic;
    delete womanMannequin;
    
    delete manGlasses;
    delete manGlassesDynamic;
    delete manMannequin;
    
    SMThickness3DView.currentView = 0;
    
    readyToDraw = false;
}

-( id )initWithCoder : ( NSCoder * )aDecoder
{
    self = [ super initWithCoder : aDecoder ];
    if( self )
    {
        iPad2 = [ PDDeviceInfo new ].deviceDetails.model == PDDeviceModeliPad2;
        
        SMThickness3DView.currentView = self;
        
        gCommon.Init();
        
        /*engine.SetGlasses( currentGlasses );
         engine.SetPower( -3 );
         engine.SetCyl( 0 );
         engine.SetAxis( 0 );*/
        engine.SetLeftDesign( ThicknessLensDesignSpherical15 );
        engine.SetRightDesign( ThicknessLensDesignSpherical15 );
        
        //engine.Refresh();
        
        SMThickness3DView.currentView = 0;
        
        pupillaryDistance = 64.f;
        showPerson = true;
        showFrame = true;
        
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    }
    return self;
}

-( void )dealloc
{
    [ self clearThings ];
}

-( void )setCamera : ( CGRect )rect
{
    float aspect = rect.size.height / rect.size.width;
    
    //gCommon.SetMatrix( SM_PROJECTION, GLKMatrix4MakePerspective( 30.f * M_PI / 180.f, self.frame.size.width / self.frame.size.height, 0.1f, 100.f ) );
    //gCommon.SetMatrix( SM_PROJECTION, GLKMatrix4MakeOrtho( -1.f, 1.f, -aspect, aspect, 0.1f, 100.f ) );
    gCommon.SetMatrix( SM_PROJECTION, GLKMatrix4MakeOrtho( -1.f, 1.f, -aspect, aspect, -100.f, 100.f ) );
    
    float magic = 55.f / 140.f;
    
    float scale = FRAME_FULLSCREEN_SCALE * ( pupillaryDistance / magic ) / IPAD3_SCREEN_WIDTH;
    
    gCommon.SetMatrix( SM_VIEW, GLKMatrix4Identity );
    gCommon.Scale( SM_VIEW, scale );
    gCommon.Translate( SM_VIEW, 0.f, 0.f, -0.6f );
    gCommon.Rotate( SM_VIEW, pitch, 1, 0, 0 );
    gCommon.Rotate( SM_VIEW, yaw, 0, 1, 0 );
    gCommon.Translate( SM_VIEW, 0.f, 0.f, -0.1f );
}

-( void )drawRect : ( CGRect )rect
{
    if( !readyToDraw )
        return;
    
    SMThickness3DView.currentView = self;
    
    gCommon.Tick();
    
    if( split )
    {
        [ self drawViewport : CGRectMake( 0, self.frame.size.height * self.contentScaleFactor / 2.f, self.frame.size.width * self.contentScaleFactor, self.frame.size.height * self.contentScaleFactor / 2.f ) leftOptions : NO ];
        [ self drawViewport : CGRectMake( 0, 0, self.frame.size.width * self.contentScaleFactor, self.frame.size.height * self.contentScaleFactor / 2.f ) leftOptions : YES ];
    }
    else
        [ self drawViewport : CGRectMake( 0, 0, self.frame.size.width * self.contentScaleFactor, self.frame.size.height * self.contentScaleFactor ) leftOptions : NO ];
    
    SMThickness3DView.currentView = 0;
}

-( void )drawViewport : ( CGRect )rect leftOptions : ( BOOL )leftOptions
{
    thickness3DGetShader( "ThicknessLens" )->Uniform1( "aspect", float( self.frame.size.width / self.frame.size.height ) );
    
    thickness3DGetInternalBuffer()->Bind();
    glClearColor( 1.f, 1.f, 1.f, 0.f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    if( power + cyl < 0.f )
    {
        [ self setCamera : rect ];
        [ self drawFrame : NO ears : NO lens : YES left : NO refraction : YES split : split leftOptions : leftOptions side : 0.f sideOffset : 0.f ];
        [ self drawFrame : NO ears : NO lens : YES left : YES refraction : YES split : split leftOptions : leftOptions side : 0.f sideOffset : 0.f ];
    }
    thickness3DGetInternalBuffer()->Unbind();
    
    thickness3DGetBackgroundBuffer()->Bind();
    glClearColor( 1.f, 1.f, 1.f, 0.f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glBindTexture( background.target, background.name );
    [ self drawScreen : background.width / background.height * ( split ? 0.5f : 1.f ) ];
    
    thickness3DGetBackgroundBuffer()->Unbind();
    
    // clear all the shit
    glClear( GL_DEPTH_BUFFER_BIT );
    thickness3DGetBuffer()->Bind();
    glClearColor( 0.f, 0.f, 0.f, 0.f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    //glBindTexture( background.target, background.name );
    //[ self drawScreen : background.width / background.height ];
    
    BOOL leftFirst = yaw > 0.f;
    
    [ self setCamera : rect ];
    [ self drawPerson : leftFirst ? 1.f : -1.f offset : leftFirst ? 0.005f : -0.005f ];
    {
        float off = fabs( yaw ) * 0.01f;
        
        gCommon.PushMatrix( SM_MODEL );
        gCommon.Translate( SM_MODEL, leftFirst ? 0.01f : -0.01f, 0.f, off );
        
        //NSLog( @"%f", off );
        
        if( showFrame )
            [ self drawFrame : NO ears : YES lens : NO left : leftFirst refraction : NO split : split leftOptions : leftOptions side : leftFirst ? 1.f : -1.f sideOffset : 0.f ];
        
        gCommon.PopMatrix( SM_MODEL );
    }
    
    thickness3DGetBuffer()->Unbind();
    glViewport( rect.origin.x, rect.origin.y, rect.size.width, rect.size.height );
    
    glBindTexture( background.target, background.name );
    [ self drawScreen : background.width / background.height * ( split ? 0.5f : 1.f ) ];
    
    [ self setCamera : rect ];
    [ self drawPerson : 0.f offset : 0.f ];
    
    [ self drawFrame : NO ears : NO lens : YES left : leftFirst refraction : NO split : split leftOptions : leftOptions side : 0.f sideOffset : 0.f ];
    
    thickness3DGetBuffer()->Bind();
    
    [ self setCamera : rect ];
    [ self drawPerson : leftFirst ? -1.f : 1.f offset : leftFirst ? 0.005f : -0.005f ];
    //[ self drawFrame : NO ears : leftFirst lens : NO left : leftFirst refraction : NO split : split leftOptions : leftOptions side : leftFirst ? -1.f : 1.f sideOffset : leftFirst ? 0.005f : -0.005f ];
    if( showFrame )
        [ self drawFrame : NO ears : YES lens : NO left : leftFirst refraction : NO split : split leftOptions : leftOptions side : leftFirst ? -1.f : 1.f sideOffset : 0.f ];
    
    thickness3DGetBuffer()->Unbind();
    glViewport( rect.origin.x, rect.origin.y, rect.size.width, rect.size.height );
    
    [ self setCamera : rect ];
    [ self drawFrame : showFrame ears : showFrame lens : YES left : !leftFirst refraction : NO split : split leftOptions : leftOptions side : 0.f sideOffset : 0.f ];
}

-( void )drawScreen : ( float )aspect
{
    //glDisable( GL_DEPTH_TEST );
    glDisable( GL_CULL_FACE );
    glDepthMask( GL_FALSE );
    
    float w = self.frame.size.width;
    float h = self.frame.size.height;
    
    aspect /= w / h;
    
    gCommon.SetMatrix( SM_VIEW, GLKMatrix4Identity );
    gCommon.SetMatrix( SM_PROJECTION, GLKMatrix4MakeOrtho( -1.f, 1.f, -1.f, 1.f, -4.f, 4.f ) );
    
    thickness3DGetShader( "ThicknessOneTexture" )->ApplyTransform();
    {
        CGRect rect;
        
        if( aspect < 1.f )
            rect.size = CGSizeMake( 2.f, 2.f / aspect );
        else
            rect.size = CGSizeMake( 2.f * aspect, 2.f );
        
        rect.origin = CGPointMake( -rect.size.width / 2.f, -rect.size.height / 2.f );
        [ self drawRectangle : rect ];
    }
    
    //glEnable( GL_DEPTH_TEST );
    glEnable( GL_CULL_FACE );
    glDepthMask( GL_TRUE );
}

-( void )drawPerson : ( float )side offset : ( float )offset
{
    if( iPad2 )
        return;
    
    if( !showPerson )
        return;
    
    gCommon.PushMatrix( SM_MODEL );
    gCommon.Translate( SM_MODEL, 0.f, -0.3f - 0.015f, 0.f );
    
    currentMannequin->Draw( side, offset );
    
    gCommon.PopMatrix( SM_MODEL );
}

-( void )drawFrame : ( BOOL )frame ears : ( BOOL )ears lens : ( BOOL )lens left : ( BOOL )left refraction : ( BOOL )refraction split : ( BOOL )split leftOptions : ( BOOL )leftOptions side : ( float )side sideOffset : ( float )sideOffset
{
    gCommon.PushMatrix( SM_MODEL );
    gCommon.Translate( SM_MODEL, 0.f, -0.3f - 0.015f, 0.f );
    
    currentGlasses->Draw( frame, ears, lens, left, refraction, 0.05f, 0.5f, split, leftOptions, side, sideOffset );
    
    gCommon.PopMatrix( SM_MODEL );
}

-( void )setPower : ( float )power cyl : ( float )cyl axis : ( long )axis
{
    [ EAGLContext setCurrentContext : self.context ];
    
    self->power = power;
    self->cyl = cyl;
    self->axis = axis;
    
    if( readyToDraw )
    {
        engine.SetPower( power );
        engine.SetCyl( cyl );
        engine.SetAxis( axis );
        engine.Refresh( dynamicGlasses );
    }
}

-( void )setLeftDesign : ( NSString * )design
{
    engine.SetLeftDesign( design );
    
    if( !readyToDraw )
        return;
    
    [ EAGLContext setCurrentContext : self.context ];
    engine.Refresh( dynamicGlasses );
}

-( void )setRightDesign : ( NSString * )design
{
    engine.SetRightDesign( design );
    
    if( !readyToDraw )
        return;
    
    [ EAGLContext setCurrentContext : self.context ];
    engine.Refresh( dynamicGlasses );
}

/*-( void )setFrameWidth : ( float )frameWidth
 {
 self->frameWidth = frameWidth;
 }*/

-( void )setTestValue0 : ( float )test
{
    [ EAGLContext setCurrentContext : self.context ];
    
    test0 = test;
    
    currentGlasses->GetContour()->SetStep( test );
    engine.SetContour( currentGlasses->GetContour(), currentGlasses->GetScale(), FALSE );
    engine.Refresh( FALSE );
    [ self setNeedsDisplay ];
}

-( void )setTestValue1 : ( float )test
{
    test1 = test;
    [ self setNeedsDisplay ];
}

-( void )setPupillaryDistance : ( float )pupillaryDistance
{
    self->pupillaryDistance = pupillaryDistance;
}

-( void )showPerson : ( bool )showPerson
{
    self->showPerson = showPerson;
}

-( void )showFrame : ( bool )showFrame
{
    self->showFrame = showFrame;
}

-( void )setPerson : ( int )index
{
    person = index;
    
    if( !readyToDraw )
        return;
    
    SMThickness3DView.currentView = self;
    
    [ EAGLContext setCurrentContext : self.context ];
    
    switch( index )
    {
        case 0:
            currentMannequin = womanMannequin;
            currentGlasses = dynamicGlasses ? womanGlassesDynamic : womanGlasses;
            break;
            
        case 1:
            currentMannequin = manMannequin;
            currentGlasses = dynamicGlasses ? manGlassesDynamic : manGlasses;
            break;
    }
    
    engine.SetGlasses( currentGlasses, dynamicGlasses );
    engine.Refresh( dynamicGlasses );
}

-( void )setDynamicGlasses : ( BOOL )dynamic
{
    dynamicGlasses = dynamic;
    
    if( !readyToDraw )
        return;
    
    SMThickness3DView.currentView = self;
    
    [ EAGLContext setCurrentContext : self.context ];
    
    switch( person )
    {
        case 0:
            currentGlasses = dynamicGlasses ? womanGlassesDynamic : womanGlasses;
            break;
            
        case 1:
            currentGlasses = dynamicGlasses ? manGlassesDynamic : manGlasses;
            break;
    }
    
    engine.SetGlasses( currentGlasses, dynamicGlasses );
    engine.Refresh( dynamicGlasses );
}

-( void )setDBL : ( float )dbl
{
    womanGlassesDynamic->SetDBL( dbl );
    manGlassesDynamic->SetDBL( dbl );
}

-( void )setA : ( float )a
{
    womanGlassesDynamic->SetA( a );
    manGlassesDynamic->SetA( a );
}

-( void )setB : ( float )b
{
    womanGlassesDynamic->SetB( b );
    manGlassesDynamic->SetB( b );
}

-( void )reset
{
    womanGlassesDynamic->Reset();
    manGlassesDynamic->Reset();
}

-( void )setSplitMode : ( BOOL )split
{
    self->split = split;
}

-( BOOL )splitMode
{
    return split;
}

-( SMModel * const )getModel : ( const char * )name
{
    if( models[ name ] )
        return models[ name ];
    
    return models[ name ] = gCommon.GetModel( name );
}

-( SMShader * const )getShader : ( const char * )name
{
    if( shaders[ name ] )
        return shaders[ name ];
    
    return shaders[ name ] = gCommon.GetShader( name );
}

-( GLKTextureInfo * const )getTexture : ( const char * )name
{
    if( textures[ name ] )
        return textures[ name ];
    
    return textures[ name ] = gCommon.GetTexture( name );
}

-( GLKTextureInfo * const )getCubemap : ( const char * )name
{
    if( textures[ name ] )
        return textures[ name ];
    
    return textures[ name ] = gCommon.GetCubemap( name );
}

-( SMBuffer * const )getBuffer
{
    return buffer;
}

-( SMBuffer * const )getInternalBuffer
{
    return internalBuffer;
}

-( SMBuffer * const )getBackgroundBuffer
{
    return backgroundBuffer;
}

-( void )setYaw : ( float )yaw
{
    self->yaw = yaw;
    
    if( !self.hidden )
        [ self setNeedsDisplay ];
}

-( void )setOtherDragging : ( BOOL )dragging
{
    otherDragging = dragging;
}

#if 0
-( void )layoutSubviews
{
    [ super layoutSubviews ];
    
    //gCommon.Resize( GLKVector2Make( self.frame.size.width, self.frame.size.height ) );
    /*if( !self.tag )
     [ self display ];*/
    //[ self setNeedsDisplay ];
}
#endif

-( void )touchesBegan : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    if( otherDragging )
        return;
    
    for( UITouch * touch in touches )
    {
        CGPoint point = [ touch locationInView : self ];
        //point.y = self.bounds.size.height - point.y;
        
        dragPoint = point;
    }
}

-( void )touchesMoved : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    if( otherDragging )
        return;
    
    [ self.otherView setOtherDragging : YES ];
    
    for( UITouch * touch in touches )
    {
        CGPoint point = [ touch locationInView : self ];
        //point.y = self.bounds.size.height - point.y;
        
        yaw += ( point.x - dragPoint.x ) * 0.005;
        //pitch += ( point.y - dragPoint.y ) * 0.005;
        
        if( yaw > M_PI_2 )
            yaw = M_PI_2;
        if( yaw < -M_PI_2 )
            yaw = -M_PI_2;
        
        [ self.otherView setYaw : yaw ];
        
        dragPoint = point;
    }
    
    [ self setNeedsDisplay ];
}

-( void )touchesEnded : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    if( otherDragging )
        return;
    
    [ self.otherView setOtherDragging : NO ];
}

-( void )touchesCancelled : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    [ self touchesEnded : touches withEvent : event ];
}

-( void )drawRectangle : ( CGRect )rect
{
    glBindBuffer( GL_ARRAY_BUFFER, 0 );
    
    static float verts[ 8 ];
    static float tverts[ 8 ] = { 0.f, 0.f, 1.f, 0.f, 1.f, 1.f, 0.f, 1.f };
    
    //glDisableVertexAttribArray( GLKVertexAttribNormal );
    //glDisableVertexAttribArray( GLKVertexAttribTexCoord1 );
    
    glVertexAttribPointer( GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, 0, verts );
    glVertexAttribPointer( GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, 0, tverts );
    
    verts[ 0 ] = rect.origin.x;
    verts[ 1 ] = rect.origin.y;
    
    verts[ 2 ] = rect.origin.x + rect.size.width;
    verts[ 3 ] = rect.origin.y;
    
    verts[ 4 ] = rect.origin.x + rect.size.width;
    verts[ 5 ] = rect.origin.y + rect.size.height;
    
    verts[ 6 ] = rect.origin.x;
    verts[ 7 ] = rect.origin.y + rect.size.height;
    
    glDrawArrays( GL_TRIANGLE_FAN, 0, 4 );
    
    //glEnableVertexAttribArray( GLKVertexAttribNormal );
    //glEnableVertexAttribArray( GLKVertexAttribTexCoord1 );
}

static SMThickness3DView * currentView = 0;

+( void )setCurrentView : ( SMThickness3DView * )currentView
{
    ::currentView = currentView;
}

+( SMThickness3DView * )currentView
{
    return currentView;
}

@end
