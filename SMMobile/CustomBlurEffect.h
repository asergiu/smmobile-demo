//
//  CustomBlurEffect.h
//  SMMobile
//
//  Created by Sergej Bogatenko on 2/22/18.
//  Copyright © 2018 Intelex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomBlurEffect : UIBlurEffect

+ (instancetype)effectWithStyle:(UIBlurEffectStyle)style;

@end
