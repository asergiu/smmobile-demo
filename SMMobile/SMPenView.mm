#import "SMPenView.h"

#include <vector>

using namespace std;

/**
 * Unlike the other implementation here, which uses the default "uniform"
 * treatment of t, this computation is used to calculate the same values but
 * introduces the ability to "parameterize" the t values used in the
 * calculation. This is based on Figure 3 from
 * http://www.cemyuksel.com/research/catmullrom_param/catmullrom.pdf
 *
 * @param p An array of double values of length 4, where interpolation
 * occurs from p1 to p2.
 * @param time An array of time measures of length 4, corresponding to each
 * p value.
 * @param t the actual interpolation ratio from 0 to 1 representing the
 * position between p1 and p2 to interpolate the value.
 * @return
 */
const double interpolate( double * p, double * time, double t )
{
    double L01 = p[0] * (time[1] - t) / (time[1] - time[0]) + p[1] * (t - time[0]) / (time[1] - time[0]);
    double L12 = p[1] * (time[2] - t) / (time[2] - time[1]) + p[2] * (t - time[1]) / (time[2] - time[1]);
    double L23 = p[2] * (time[3] - t) / (time[3] - time[2]) + p[3] * (t - time[2]) / (time[3] - time[2]);
    double L012 = L01 * (time[2] - t) / (time[2] - time[0]) + L12 * (t - time[0]) / (time[2] - time[0]);
    double L123 = L12 * (time[3] - t) / (time[3] - time[1]) + L23 * (t - time[1]) / (time[3] - time[1]);
    double C12 = L012 * (time[2] - t) / (time[2] - time[1]) + L123 * (t - time[1]) / (time[2] - time[1]);
    return C12;
}

/**
 * Given a list of control points, this will create a list of pointsPerSegment
 * points spaced uniformly along the resulting Catmull-Rom curve.
 *
 * @param points The list of control points, leading and ending with a
 * coordinate that is only used for controling the spline and is not visualized.
 * @param index The index of control point p0, where p0, p1, p2, and p3 are
 * used in order to create a curve between p1 and p2.
 * @param pointsPerSegment The total number of uniformly spaced interpolated
 * points to calculate for each segment. The larger this number, the
 * smoother the resulting curve.
 * @param curveType Clarifies whether the curve should use uniform, chordal
 * or centripetal curve types. Uniform can produce loops, chordal can
 * produce large distortions from the original lines, and centripetal is an
 * optimal balance without spaces.
 * @return the list of coordinates that define the CatmullRom curve
 * between the points defined by index+1 and index+2.
 */
const vector< GLKVector2 > interpolate( const vector< GLKVector2 > & points, int index, int pointsPerSegment/*, CatmullRomType curveType*/)
{
    vector< GLKVector2 > result;
    double x[ 4 ];
    double y[ 4 ];
    double time[ 4 ];
    for( int i = 0; i < 4; i++ )
    {
        x[ i ] = points[ index + i ].x;
        y[ i ] = points[ index + i ].y;
        time[ i ] = i;
    }
    
    double tstart = 1;
    double tend = 2;
    //if (!curveType.equals(CatmullRomType.Uniform)) {
    {
        double total = 0;
        for (int i = 1; i < 4; i++) {
            double dx = x[i] - x[i - 1];
            double dy = y[i] - y[i - 1];
            //if (curveType.equals(CatmullRomType.Centripetal)) {
            total += pow(dx * dx + dy * dy, .25);
            /*} else {
             total += Math.pow(dx * dx + dy * dy, .5);
             }*/
            time[i] = total;
        }
        tstart = time[1];
        tend = time[2];
    }
    
    int segments = pointsPerSegment - 1;
    result.push_back( points[ index + 1 ] );
    
    for( int i = 1; i < segments; i++ )
    {
        double xi = interpolate(x, time, tstart + (i * (tend - tstart)) / segments);
        double yi = interpolate(y, time, tstart + (i * (tend - tstart)) / segments);

        result.push_back( GLKVector2Make( xi, yi ) );
    }
    result.push_back( points[ index + 2 ] );
    return result;
}

const vector< GLKVector2 > interpolate( const vector< GLKVector2 > & coordinates, int pointsPerSegment/*, CatmullRomType curveType*/ )
{
    vector< GLKVector2 > vertices = coordinates;

    if( pointsPerSegment < 2 )
        return vertices;
    
    if( vertices.size() < 3 )
        return vertices;
    
    // Test whether the shape is open or closed by checking to see if
    // the first point intersects with the last point.  M and Z are ignored.
    {
        // The shape is open, so use control points that simply extend
        // the first and last segments
        
        // Get the change in x and y between the first and second coordinates.
        double dx = vertices[ 1 ].x - vertices[ 0 ].x;
        double dy = vertices[ 1 ].y - vertices[ 0 ].y;
        
        // Then using the change, extrapolate backwards to find a control point.
        double x1 = vertices[ 0 ].x - dx;
        double y1 = vertices[ 0 ].y - dy;
        
        // Actaully create the start point from the extrapolated values.
        GLKVector2 start = GLKVector2Make( x1, y1 );
        
        // Repeat for the end control point.
        int n = vertices.size() - 1;
        dx = vertices[ n ].x - vertices[ n - 1 ].x;
        dy = vertices[ n ].y - vertices[ n - 1 ].y;
        double xn = vertices[ n ].x + dx;
        double yn = vertices[ n ].y + dy;
        GLKVector2 end = GLKVector2Make( xn, yn );
        
        // insert the start control point at the start of the vertices list.
        vertices.insert( vertices.begin(), start );
        
        // append the end control ponit to the end of the vertices list.
        //vertices.push_back( end );
    }
    
    // Dimension a result list of coordinates.
    vector< GLKVector2 > result;
    // When looping, remember that each cycle requires 4 points, starting
    // with i and ending with i+3.  So we don't loop through all the points.
    for (int i = 0; i < vertices.size() - 3; i++) {
        
        // Actually calculate the Catmull-Rom curve for one segment.
        vector< GLKVector2 > points = interpolate( vertices, i, pointsPerSegment );
        // Since the middle points are added twice, once for each bordering
        // segment, we only add the 0 index result point for the first
        // segment.  Otherwise we will have duplicate points.
        if( result.size() > 0 )
            points.erase( points.begin() );
        
        result.insert( result.end(), points.begin(), points.end() );
    }
    
    return result;
}

using namespace std;

@interface SMPenView()
{
    GLuint renderTexture;
    GLuint framebuffer;
    
    GLKTextureInfo * penTexture;
    
    GLuint penProgram;
    GLuint defaultProgram;
    
    GLKVector3 colors[ 4 ];
    
    //CGPoint dragPoint;
    
    vector< GLKVector2 > points;
}

@end

@implementation SMPenView

@synthesize penActivated;
@synthesize penColor;
@synthesize penWidth;
@synthesize delegatePen;

-( id )initWithCoder : ( NSCoder * )decoder
{
    self = [ super initWithCoder : decoder ];
    if( self )
    {
        //self.premultiplicationParameter = NO;
        
        renderTexture = [ self genTexture ];
        framebuffer = [ self genFramebufferWithTexture : renderTexture width : 2048 height : 2048 ];
        
        penTexture = [ self getTexture : @"pen.png" ];
    
        penProgram = [ self getProgram : @"Pen" : @"Pen" ];
        defaultProgram = [ self getProgram : @"Default" : @"Default" ];
        
        GLKMatrix4 modelViewProjectionMatrix = GLKMatrix4MakeOrtho( 0, self.frame.size.width, 0, self.frame.size.height, -99999., 99999. );
        
        glUseProgram( penProgram );
        glUniformMatrix4fv( glGetUniformLocation( penProgram, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
        
        glUseProgram( defaultProgram );
        glUniformMatrix4fv( glGetUniformLocation( defaultProgram, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
        
        colors[ 0 ] = GLKVector3Make( 233 / 255.f, 92 / 255.f, 92 / 255.f );
        colors[ 1 ] = GLKVector3Make( 233 / 255.f, 156 / 255.f, 67 / 255.f );
        colors[ 2 ] = GLKVector3Make( 95 / 255.f, 201 / 255.f, 105 / 255.f );
        colors[ 3 ] = GLKVector3Make( 78 / 255.f, 108 / 255.f, 202 / 255.f );
        colors[ 4 ] = GLKVector3Make( 0.f, 0.f, 0.f );
        
        self.penWidth = 4.f;
        
        self.layer.opaque = NO;
        self.backgroundColor = [ UIColor clearColor ];
        
        glEnable( GL_BLEND );
        //glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
        //glBlendFunc(GL_ONE_MINUS_DST_ALPHA,GL_DST_ALPHA);
        //glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
        glBlendFuncSeparate( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
        
        [ self clear ];
    }
    return self;
}

-( void )drawRect : ( CGRect )rect
{
    [ self bindDrawable ];
    
    glClearColor( 0, 0, 0, 0 );
    glClear( GL_COLOR_BUFFER_BIT );
    
    glUseProgram( defaultProgram );
    glBindTexture( GL_TEXTURE_2D, renderTexture );
    [ self drawRectangle : rect ];
}

-( void )drawDot : ( GLKVector2 )point
{
    [ EAGLContext setCurrentContext : self.context ];
    
    glBindFramebuffer( GL_FRAMEBUFFER, framebuffer );
    glViewport( 0, 0, 2048, 2048 );
    
    glBindTexture( penTexture.target, penTexture.name );
    
    glUseProgram( penProgram );
    glUniform3fv( glGetUniformLocation( penProgram, "color" ), 1, colors[ penColor ].v );
    
    CGRect rect = CGRectMake( point.x - penWidth / 2.f, point.y - penWidth / 2.f, penWidth, penWidth );
    [ self drawRectangle : rect ];
}

-( void )drawLineFrom : ( GLKVector2 )a to : ( GLKVector2 )b
{
    float delta = penWidth / 4.f;
    float distance = GLKVector2Distance( a, b );
    int count = floor( distance / delta ) + 1;
    
    for( int i = 0; i <= count; i++ )
    {
        GLKVector2 point = GLKVector2Lerp( a, b, ( float )i / count );
        [ self drawDot : point ];
    }
    
    /*int penColorOld = penColor;
    penColor = 4;
    [ self drawDot : a ];
    [ self drawDot : b ];
    penColor = penColorOld;*/
}

-( void )touchesBegan : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    if (delegatePen && [delegatePen respondsToSelector:@selector(touchBeganView)])
        [delegatePen touchBeganView];
    
    points.clear();
    
    for( UITouch * touch in touches )
    {
        CGPoint point = [ touch locationInView : self ];
        point.y = self.bounds.size.height - point.y;
        
        points.push_back( GLKVector2Make( point.x, point.y ) );
        
        [ self drawDot : GLKVector2Make( point.x, point.y ) ];
    }
    
    //[ self display ];
    [ self setNeedsDisplay ];
}

-( void )touchesMoved : ( NSSet * )touches withEvent : ( UIEvent * )event
{    
    for( UITouch * touch in touches )
    {
        CGPoint point = [ touch locationInView : self ];
        point.y = self.bounds.size.height - point.y;
        
        points.push_back( GLKVector2Make( point.x, point.y ) );
        
        if( points.size() < 4 )
            continue;
        
        static const int POINTS_PER_SEGMENT = 8;
        
        vector< GLKVector2 > result = interpolate( points, POINTS_PER_SEGMENT );
        
        if( points.size() == 4 )
            for( int i = 0; i < result.size() - 1; i++ )
                [ self drawLineFrom : result[ i ] to : result[ i + 1 ] ];
        else
            for( int i = result.size() - POINTS_PER_SEGMENT; i < result.size() - 1; i++ )
                [ self drawLineFrom : result[ i ] to : result[ i + 1 ] ];
    }
    
    [ self display ];
    //[ self setNeedsDisplay ];
}

-( void )touchesEnded : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    if (delegatePen && [delegatePen respondsToSelector:@selector(touchesEndedView)])
        [delegatePen touchesEndedView];
}

-( void )touchesCancelled : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    [ self touchesEnded : touches withEvent : event ];
}

-( void )setPenActivated : ( BOOL )penActivated
{
    self->penActivated = penActivated;
    self.userInteractionEnabled = penActivated;
    
    if( !penActivated )
        [ self clear ];
    
    [ self display ];
    //[ self setNeedsDisplay ];
}

-( void )clear
{
    [ EAGLContext setCurrentContext : self.context ];
    
    glBindFramebuffer( GL_FRAMEBUFFER, framebuffer );
    
    glClearColor( 0, 0, 0, 0 );
    glClear( GL_COLOR_BUFFER_BIT );
}

@end
