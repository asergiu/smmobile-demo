//
//  SMVideoSource.m
//  activescreen
//
//  Created by Dgut on 09.10.13.
//

#import "SMVideoSource.h"
#import <AVFoundation/AVFoundation.h>
#import "NSObject+EncryptVideoPAL.h"
#import "SMImageLoader.h"

@interface SMVideoSource()
{
    AVURLAsset * asset;
    AVAssetTrack * track;
    AVAssetReaderTrackOutput * output;
    AVAssetReader * reader;
    NSURL * url;
}

@end

@implementation SMVideoSource

- (id)initWithFile:(NSString *)file decrypt:(BOOL)decrypt
{
    self = [ super init ];
    if( self )
    {
        if (decrypt)
            url = [self encryptFile:file fileExtantion:@"mp4"];
        else
        {
            NSString * path = [ [ NSBundle mainBundle ] pathForResource : file ofType : nil ];
            url = [ NSURL fileURLWithPath : path ];
        }
        
        asset = [ [ AVURLAsset alloc ] initWithURL : url options : nil ];
        
        NSArray * tracks = [ asset tracksWithMediaType : AVMediaTypeVideo ];
        track = [ tracks objectAtIndex : 0 ];
        
        NSLog( @"tracks: %lu", (unsigned long)tracks.count );
        NSLog( @"track fps: %f", track.nominalFrameRate );
        
        CMTime duration = track.timeRange.duration;
        
        NSLog( @"track duration: %qi/%i", duration.value, duration.timescale );
        
        [ self restart ];
    }
    return self;
}

- (void)dealloc
{
    NSError *error = nil;
    
    [[NSFileManager defaultManager] removeItemAtURL:url error:&error];
}


-( void )restart
{
    NSString * key = ( NSString * )kCVPixelBufferPixelFormatTypeKey;
    NSNumber * value = [ NSNumber numberWithUnsignedInt : kCVPixelFormatType_32BGRA ];
    NSDictionary * settings = [ [ NSDictionary alloc ] initWithObjectsAndKeys : value, key, nil ];
    
    output = [ [ AVAssetReaderTrackOutput alloc ] initWithTrack : track outputSettings : settings ];
    
    reader = [ [ AVAssetReader alloc ] initWithAsset : asset error : nil ];
    [ reader addOutput : output ];
    [ reader startReading ];
}

-( BOOL )readNext
{
    return [ self readNextWithBlock : NULL ];
}

-( BOOL )readNextWithBlock : ( void ( ^ )( void * ptr, int width, int height ) )block
{
    BOOL reset = NO;
    
    CMSampleBufferRef sampleBuffer;
    while( !( sampleBuffer = [ output copyNextSampleBuffer ] ) )
    {
        [ self restart ];
        reset = YES;
    }
    
    CVImageBufferRef pixelBuffer = CMSampleBufferGetImageBuffer( sampleBuffer );
    CVPixelBufferLockBaseAddress( pixelBuffer, 0 );
    
    int bufferHeight = CVPixelBufferGetHeight( pixelBuffer );
	int bufferWidth = CVPixelBufferGetBytesPerRow( pixelBuffer ) / 4;
    
    //glBindTexture(GL_TEXTURE_2D, mTextureHandle);
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, bufferWidth, bufferHeight, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, CVPixelBufferGetBaseAddress( pixelBuffer ) );
    
    if( block )
        block( CVPixelBufferGetBaseAddress( pixelBuffer ), bufferWidth, bufferHeight );
    
    CVPixelBufferUnlockBaseAddress( pixelBuffer, 0 );
    CFRelease( sampleBuffer );
    
    return reset;
}

-( float )frameRate
{
    return track.nominalFrameRate;
}

-( CGSize )size
{
    return track.naturalSize;
}

@end
