//
//  CustomBlurEffect.m
//  SMMobile
//
//  Created by Sergej Bogatenko on 2/22/18.
//  Copyright © 2018 Intelex. All rights reserved.
//

#import "CustomBlurEffect.h"
#import <objc/runtime.h>

@interface UIBlurEffect (Protected)

@property (nonatomic, readonly) id effectSettings;

@end

@implementation CustomBlurEffect


+ (instancetype)effectWithStyle:(UIBlurEffectStyle)style
{
    id result = [super effectWithStyle:style];
    object_setClass(result, self);
    
    return result;
}

- (id)effectSettings
{
    id settings = [super effectSettings];
    [settings setValue:@5 forKey:@"blurRadius"];
    
    return settings;
}

- (id)copyWithZone:(NSZone *)zone
{
    id result = [super copyWithZone:zone];
    object_setClass(result, [self class]);
    
    return result;
}

@end
