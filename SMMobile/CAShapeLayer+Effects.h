//
//  CAShapeLayer+Effects.h
//  SMMobile
//
//  Created by Sergej Bogatenko on 12/19/18.
//  Copyright © 2018 ACEP. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CAShapeLayer (Effects)

- (CAShapeLayer *)getShapeLayerWithFrame:(CGRect)frame
                               holeFrame:(CGRect)holeFrame;

@end

