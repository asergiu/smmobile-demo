#import <UIKit/UIKit.h>

@interface SMDistanceView : UIView
{
    int newDistance;
    int newDistanceCount;
}

@property( nonatomic ) int distance;
@property ( strong, nonatomic ) UIImage * selectedButton;
@property ( strong, nonatomic ) UIImage * notSelectedButton;

@end
