//
//  SMExpirationDateChecker.m
//  SMMobile
//
//  Created by Sergej Bogatenko on 3/3/18.
//  Copyright © 2018 ACEP. All rights reserved.
//

#import "SMProvisionChecker.h"

#define isNeedCheckExpirationDate YES

static NSString * const CERTIFICATE_EXPIRATION_DATE = @"06-02-2021"; // "Certificate Expiration Date" format: dd-MM-yyyy

static uint const sevenDays = 604800; // 7 days in unixtime
static uint const oneDay = 86400;     // 1 day in unixtime

@interface SMProvisionChecker ()
{
    uint PROFILE_EXPIRATION_DATE;
    uint DELAY_DATE;
    
    BOOL isNeedDelayAlert;
    BOOL isNeedCheckNewProfileExpirationDate;
}

@end

@implementation SMProvisionChecker

- (id)init
{
    if (self = [super init])
    {
        isNeedCheckNewProfileExpirationDate = NO;
        isNeedDelayAlert = NO;
        
        DELAY_DATE = 0;
        
        PROFILE_EXPIRATION_DATE = (uint) [[self getProfileExpiryDate] timeIntervalSince1970];
    }
    
    return self;
}

- (BOOL)isSoonExpirationDate
{
    if (isNeedCheckExpirationDate)
    {
        if (isNeedCheckNewProfileExpirationDate)
            [self checkNewProfileExpirationDate];
        
        if ([self checkExpirationDate:PROFILE_EXPIRATION_DATE] ||
            [self checkExpirationDate:[self getCertificateExpirationDate]])
            return [self checkIsNeedShowTodayExpirationDateAlert];
        else
        {
            if ([self dontShowAgainExpirationDateAlert])
                [self setupIsNeedShowAgainExpirationDateAlert:YES];
            
            return NO;
        }
    }
    else
        return NO;
}

- (BOOL)checkExpirationDate:(uint)expirationDate
{
    if ((expirationDate - [self getCurrentDate]) < sevenDays)
        return YES;
    
    return NO;
}

- (BOOL)checkIsNeedShowTodayExpirationDateAlert
{
    [self delayIsExpiry];
    
    if (![self dontShowAgainExpirationDateAlert])
        return !isNeedDelayAlert;
    else
        return NO;
}

- (void)setupIsNeedShowAgainExpirationDateAlert:(BOOL)isNeedShowAgain
{
    [[NSUserDefaults standardUserDefaults] setBool:!isNeedShowAgain forKey:@"dontShowAgain_expirationDateAlert"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)dontShowAgainExpirationDateAlert
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"dontShowAgain_expirationDateAlert"];
}

- (void)setupIsNeedCheckNewProfileExpirationDate
{
    isNeedCheckNewProfileExpirationDate = YES;
}

- (void)checkNewProfileExpirationDate
{
    uint new_PROFILE_EXPIRATION_DATE = (uint) [[self getProfileExpiryDate] timeIntervalSince1970];
    
    if (PROFILE_EXPIRATION_DATE != new_PROFILE_EXPIRATION_DATE)
    {
        PROFILE_EXPIRATION_DATE = new_PROFILE_EXPIRATION_DATE;
        
        isNeedCheckNewProfileExpirationDate = NO;
    }
}

- (void)setupIsNeedDelayExpirationDateAlert
{
    if (![self dontShowAgainExpirationDateAlert])
    {
        DELAY_DATE = [self getCurrentDate] + oneDay;
        
        isNeedDelayAlert = YES;
    }
}

- (void)delayIsExpiry
{
    if ([self getCurrentDate] >= DELAY_DATE)
        isNeedDelayAlert = NO;
}

- (uint)getCurrentDate
{
    return (uint) [[NSDate new] timeIntervalSince1970];
}

#pragma mark - Profile/Certificate Expiry Date

- (uint)getCertificateExpirationDate
{
    NSDate *certificateExpirationDate = [[self getCertificateDateFormatter] dateFromString:CERTIFICATE_EXPIRATION_DATE];
    uint expirationDate = (uint) [certificateExpirationDate timeIntervalSince1970];
    
    return expirationDate;
}

- (NSDate *)getProfileExpiryDate
{
    NSString *bundleDirectory = [[NSBundle mainBundle] bundlePath];
    NSString *filePath = [NSString stringWithFormat:@"%@/embedded.mobileprovision", bundleDirectory];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    NSData *dataToFind = [@"<key>ExpirationDate</key>\n\t<date>" dataUsingEncoding:NSUTF8StringEncoding];
    NSRange range = [data rangeOfData:dataToFind options:0 range:NSMakeRange(0, [data length])];
    NSData *expirationTimeData = nil;
    NSString *expirationTimeRawStr = nil;
    
    if (range.location != NSNotFound)
    {
        expirationTimeData = [data subdataWithRange:NSMakeRange(range.location + range.length,
                                                                [@"yyyy-MM-ddThh:mm:ssZ" length])];
        expirationTimeRawStr = [[NSString alloc] initWithData:expirationTimeData encoding:NSUTF8StringEncoding];

        expirationTimeRawStr = [expirationTimeRawStr stringByReplacingOccurrencesOfString:@"T" withString:@" "];
        expirationTimeRawStr = [expirationTimeRawStr stringByReplacingOccurrencesOfString:@"Z" withString:@""];

        return [[self getProfileDateFormatter] dateFromString:expirationTimeRawStr];
    }
    else
    {
        return nil;
    }
}

- (NSDateFormatter *)getProfileDateFormatter
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    return dateFormatter;
}

- (NSDateFormatter *)getCertificateDateFormatter
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    return dateFormatter;
}

@end
