//
//  SMAppDelegate.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 8/5/14.
//

#import <UIKit/UIKit.h>

#import <CoreMotion/CoreMotion.h>

@class ViewController;

@interface SMAppDelegate : UIResponder <UIApplicationDelegate>
{
    CMMotionManager *motionManager;
}

@property (strong, nonatomic) UIWindow *window;

@property (readonly) CMMotionManager *motionManager;

@property (strong, nonatomic) ViewController *viewController;

@end
