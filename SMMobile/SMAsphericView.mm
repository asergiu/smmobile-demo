//
//  SMCoatingView.m
//  Optovision
//
//  Created by Oleg Bogatenko on 30.05.13.
//
//

#import "SMAsphericView.h"

static const float LENS_SHIFT = 20.f;
static const float LENS_SCALE = 0.85f;

#define FBO_WIDTH 320
#define FBO_HEIGHT 240

@interface SMAsphericView()
{
    GLKTextureInfo * lenses[ 2 ];
    GLKTextureInfo * masks[ 2 ];
    
    GLKTextureInfo * arrow;
    
    GLKTextureInfo * backgrounds[ 2 ];
    
    int backgroundIndex;
    int lensIndex;
    
    NSInteger shape;
    
    GLuint progText;
    GLuint progUniversal;
    GLuint progDefault;
    GLuint progDivider;
    
    GLuint gaussianBlurHorizontal;
    GLuint gaussianBlurVertical;
    
    GLuint positionRenderTexture0;
    GLuint positionFramebuffer0;
    
    GLuint positionRenderTexture1;
    GLuint positionFramebuffer1;
    
    GLKVector2 lensPosition;
    
    CGPoint dragPoint;
    
    GLuint interfaceSphericTexture;
    GLuint interfaceAsphericTexture;
    
    float divider;
    GLfloat divSpeed;
    GLfloat divLimLeft, divLimRight;
    
    BOOL dragArrow;
    
    float b0;
    float F3;
    float F3_cyl;
    float soapPower;
    float axis;
    
    float leftIndex;
    float rightIndex;
    
    int leftDesign;
    int rightDesign;
    
    bool readyToDraw;
}

@end

@implementation SMAsphericView

-( void )loadThings
{
    if( readyToDraw )
        return;
    
    [ EAGLContext setCurrentContext : self.context ];
    
    positionRenderTexture0 = [ self genTexture ];
    positionFramebuffer0 = [ self genFramebufferWithTexture : positionRenderTexture0 width : FBO_WIDTH height : FBO_HEIGHT ];
    
    positionRenderTexture1 = [ self genTexture ];
    positionFramebuffer1 = [ self genFramebufferWithTexture : positionRenderTexture1 width : FBO_WIDTH height : FBO_HEIGHT ];
    
    interfaceSphericTexture = [ self genTexture ];
    interfaceAsphericTexture = [ self genTexture ];
    
    lenses[ 0 ] = [ self getTexture : @"big_lens.png" ];
    masks[ 0 ] = [ self getTexture : @"big_mask_no_ar.png" ];
    lenses[ 1 ] = [ self getTexture : @"ellipse_big_lens.png" ];
    masks[ 1 ] = [ self getTexture : @"ellipse_big_mask_no_ar.png" ];
    
    backgrounds[ 0 ] = [ self getTexture : @"asph_dots" ];
    backgrounds[ 1 ] = [ self getTexture : @"asph_columns" ];
    
    arrow = [ self getTexture : @"divider_arrows@2x.png" ];
    
    progText = [ self getProgram : @"Coating" : @"CoatingText" ];
    progUniversal = [ self getProgram : @"Aspheric" : @"Aspheric" ];
    progDefault = [ self getProgram : @"Default" : @"Default" ];
    progDivider = [ self getProgram : @"CoatingAll" : @"CoatingDivider" ];
    
    gaussianBlurHorizontal = [ self getProgram : @"GaussianBlur" : @"GaussianBlurHorizontal" ];
    gaussianBlurVertical = [ self getProgram : @"GaussianBlur" : @"GaussianBlurVertical" ];
    
    GLKMatrix4 modelViewProjectionMatrix = GLKMatrix4MakeOrtho( 0, self.bounds.size.width, 0, self.bounds.size.height, -99999., 99999. );
    GLKVector2 screenSize = GLKVector2Make( self.frame.size.width, self.frame.size.height );
    GLKVector2 backgroundSize = GLKVector2Make( backgrounds[ 0 ].width, backgrounds[ 0 ].height );
    GLKVector2 lensSize = GLKVector2Make( lenses[ 0 ].width / 2.f * LENS_SCALE, lenses[ 0 ].height / 2.f * LENS_SCALE );
    
    divLimLeft = -lensSize.x * 0.5f;
    divLimRight = lensSize.x * 0.5f;
    
    glUseProgram( progText );
    glUniformMatrix4fv( glGetUniformLocation( progText, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    
    glUseProgram( progUniversal );
    glUniformMatrix4fv( glGetUniformLocation( progUniversal, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    glUniform1i( glGetUniformLocation( progUniversal, "image" ), 0 );
    glUniform1i( glGetUniformLocation( progUniversal, "blur" ), 1 );
    glUniform1i( glGetUniformLocation( progUniversal, "lens" ), 2 );
    glUniform1i( glGetUniformLocation( progUniversal, "mask" ), 3 );
    glUniform2fv( glGetUniformLocation( progUniversal, "screenSize" ), 1, screenSize.v );
    glUniform2fv( glGetUniformLocation( progUniversal, "backgroundSize" ), 1, backgroundSize.v );
    glUniform2fv( glGetUniformLocation( progUniversal, "lensSize" ), 1, lensSize.v );
    
    glUseProgram( progDefault );
    glUniformMatrix4fv( glGetUniformLocation( progDefault, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    
    glUseProgram( progDivider );
    glUniformMatrix4fv( glGetUniformLocation( progDivider, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    glUniform1i( glGetUniformLocation( progDivider, "mask" ), 0 );
    glUniform2fv( glGetUniformLocation( progDivider, "screenSize" ), 1, screenSize.v );
    glUniform2fv( glGetUniformLocation( progDivider, "backgroundSize" ), 1, backgroundSize.v );
    glUniform2fv( glGetUniformLocation( progDivider, "lensSize" ), 1, lensSize.v );
    //glUniform2fv( glGetUniformLocation( progDivider, "coatingSize" ), 1, coatingSize.v );
    
    readyToDraw = true;
}

-( void )clearThings
{
    if( !readyToDraw )
        return;
    
    [ EAGLContext setCurrentContext : self.context ];
    
    GLuint t;
    
    t = lenses[ 0 ].name;       glDeleteTextures( 1, &t );
    t = lenses[ 1 ].name;       glDeleteTextures( 1, &t );
    t = masks[ 0 ].name;        glDeleteTextures( 1, &t );
    t = masks[ 1 ].name;        glDeleteTextures( 1, &t );
    
    t = arrow.name;             glDeleteTextures( 1, &t );
    
    t = backgrounds[ 0 ].name;  glDeleteTextures( 1, &t );
    t = backgrounds[ 1 ].name;  glDeleteTextures( 1, &t );
    
    glDeleteProgram( progText );
    glDeleteProgram( progUniversal );
    glDeleteProgram( progDefault );
    glDeleteProgram( progDivider );
    
    glDeleteProgram( gaussianBlurHorizontal );
    glDeleteProgram( gaussianBlurVertical );
    
    glDeleteTextures( 1, &positionRenderTexture0 );
    glDeleteFramebuffers( 1, &positionFramebuffer0 );
    glDeleteRenderbuffers( 1, &positionFramebuffer0 );
    
    glDeleteTextures( 1, &positionRenderTexture1 );
    glDeleteFramebuffers( 1, &positionFramebuffer1 );
    glDeleteRenderbuffers( 1, &positionFramebuffer1 );
    
    glDeleteTextures( 1, &interfaceSphericTexture );
    glDeleteTextures( 1, &interfaceAsphericTexture );
    
    /*for( GLuint i = 1; i <= 256; i++ )
     glDeleteTextures( 1, &i );
     for( GLuint i = 1; i <= 256; i++ )
     glDeleteProgram( i );
     for( GLuint i = 1; i <= 2; i++ )
     glDeleteFramebuffers( 1, &i );
     for( GLuint i = 1; i <= 2; i++ )
     glDeleteRenderbuffers( 1, &i );
     for( GLuint i = 1; i <= 256; i++ )
     glDeleteBuffers( 1, &i );*/
    
    readyToDraw = false;
}

-( id )initWithCoder : ( NSCoder * )aDecoder
{
    self = [ super initWithCoder : aDecoder ];
    if( self )
    {
        leftIndex = 1.5f;
        rightIndex = 1.5f;
        
        glEnable( GL_BLEND );
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    }
    
    return self;
}

-( void )renderViews
{
    [ self renderInterfaceView : self.sphericView toTexture : interfaceSphericTexture ];
    [ self renderInterfaceView : self.asphericView toTexture : interfaceAsphericTexture ];
}

-( void )drawRect : ( CGRect )rect
{
    if( !readyToDraw )
        return;
    
    if( !dragArrow )
    {
        divSpeed *= 0.95f;
        
        if( abs( divSpeed ) < 0.0001 )
            divSpeed = 0.f;
        else
            divider += divSpeed;
    }
    
    if( divider < divLimLeft )
        divider = divLimLeft;
    if( divider > divLimRight )
        divider = divLimRight;
    
    [ self drawUniversal ];
    
    [ self drawLabels ];
    [ self drawDivider ];
    [ self drawArrow ];
}

-( void )drawUniversal
{
    GLKMatrix4 modelViewProjectionMatrix = GLKMatrix4MakeOrtho( 0, self.bounds.size.width, 0, self.bounds.size.height, -99999., 99999. );
    
    glBindFramebuffer( GL_FRAMEBUFFER, positionFramebuffer0 );
    glViewport( 0, 0, FBO_WIDTH, FBO_HEIGHT );
    glActiveTexture( GL_TEXTURE0 );
    if( self.videoMode )
        glBindTexture( GL_TEXTURE_2D, self.videoFrameTexture );
    else
        glBindTexture( backgrounds[ backgroundIndex ].target, backgrounds[ backgroundIndex ].name );
    
    glUseProgram( gaussianBlurHorizontal );
    glUniform1i( glGetUniformLocation( gaussianBlurHorizontal, "background" ), 0 );
    glUniformMatrix4fv( glGetUniformLocation( gaussianBlurHorizontal, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    glUniform1f( glGetUniformLocation( gaussianBlurHorizontal, "dx" ), 1.f / FBO_WIDTH );
    //glUniform1f( glGetUniformLocation( gaussianBlurHorizontal, "dy" ), 1.f / FBO_HEIGHT0 );
    [ self drawRectangle : self.frame ];
    
    glBindFramebuffer( GL_FRAMEBUFFER, positionFramebuffer1 );
    glViewport( 0, 0, FBO_WIDTH, FBO_HEIGHT );
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, positionRenderTexture0 );
    
    glUseProgram( gaussianBlurVertical );
    glUniform1i( glGetUniformLocation( gaussianBlurVertical, "background" ), 0 );
    glUniformMatrix4fv( glGetUniformLocation( gaussianBlurVertical, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    //glUniform1f( glGetUniformLocation( gaussianBlurVertical, "dx" ), 1.f / FBO_WIDTH );
    glUniform1f( glGetUniformLocation( gaussianBlurVertical, "dy" ), 1.f / FBO_HEIGHT );
    [ self drawRectangle : self.frame ];
    
    [ self bindDrawable ];
    
    glUseProgram( progUniversal );
    
    glActiveTexture( GL_TEXTURE0 );
    if( self.videoMode )
        glBindTexture( GL_TEXTURE_2D, self.videoFrameTexture );
    else
        glBindTexture( backgrounds[ backgroundIndex ].target, backgrounds[ backgroundIndex ].name );
    glActiveTexture( GL_TEXTURE1 );
    glBindTexture( GL_TEXTURE_2D, positionRenderTexture1 );
    glActiveTexture( GL_TEXTURE2 );
    glBindTexture( lenses[ lensIndex ].target, lenses[ lensIndex ].name );
    glActiveTexture( GL_TEXTURE3 );
    glBindTexture( masks[ lensIndex ].target, masks[ lensIndex ].name );
    
    glUniformMatrix4fv( glGetUniformLocation( progUniversal, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    
    GLKVector2 lensPosition = GLKVector2Make( self->lensPosition.x, self->lensPosition.y + LENS_SHIFT );
    
    GLKVector2 ground = GLKVector2Make( 0, -1 );
    
    if( self.videoMode )
    {
        glUniform1f( glGetUniformLocation( progUniversal, "backgroundSwapX" ), [ [ UIApplication sharedApplication ] statusBarOrientation] == UIInterfaceOrientationLandscapeLeft ? 1.f : 0.f );
        glUniform1f( glGetUniformLocation( progUniversal, "backgroundSwapY" ), [ [ UIApplication sharedApplication ] statusBarOrientation] == UIInterfaceOrientationLandscapeLeft ? 0.f : 1.f );
    }
    else
    {
        glUniform1f( glGetUniformLocation( progUniversal, "backgroundSwapX" ), 0.f );
        glUniform1f( glGetUniformLocation( progUniversal, "backgroundSwapY" ), 0.f );
    }
    
    float leftPowerFactor;
    
    if( leftDesign == 2 )
        leftPowerFactor = 0.3f;
    else if( leftDesign == 1 )
        leftPowerFactor = 0.5f;
    else
        leftPowerFactor = 1.f;
    
    GLKVector2 axis = GLKVector2Make( cosf( self->axis * M_PI / 180.f ), sinf( self->axis * M_PI / 180.f ) );
    
    glUniform2fv( glGetUniformLocation( progUniversal, "ground" ), 1, ground.v );
    glUniform2fv( glGetUniformLocation( progUniversal, "lensPosition" ), 1, lensPosition.v );
    glUniform1f( glGetUniformLocation( progUniversal, "b0" ), b0 );
    glUniform1f( glGetUniformLocation( progUniversal, "F3" ), F3 * ( 2.5f - leftIndex ) * leftPowerFactor );
    glUniform1f( glGetUniformLocation( progUniversal, "F3_cyl" ), F3_cyl * ( 2.5f - leftIndex ) * leftPowerFactor );
    glUniform1f( glGetUniformLocation( progUniversal, "soapPower" ), soapPower );
    glUniform2fv( glGetUniformLocation( progUniversal, "axis" ), 1, axis.v );
    
    CGRect rect;
    rect.origin = CGPointZero;
    rect.size = CGSizeMake( self.frame.size.width / 2.f + lensPosition.x + divider, self.frame.size.height );
    
    [ self drawRectangle : rect ];
    
    float rightPowerFactor;
    
    if( rightDesign == 2 )
        rightPowerFactor = 0.3f;
    else if( rightDesign == 1 )
        rightPowerFactor = 0.5f;
    else
        rightPowerFactor = 1.f;
    
    glUniform1f( glGetUniformLocation( progUniversal, "b0" ), b0 );
    glUniform1f( glGetUniformLocation( progUniversal, "F3" ), F3 * ( 2.5f - rightIndex ) * rightPowerFactor );
    glUniform1f( glGetUniformLocation( progUniversal, "F3_cyl" ), F3_cyl * ( 2.5f - rightIndex ) * rightPowerFactor );
    glUniform1f( glGetUniformLocation( progUniversal, "soapPower" ), soapPower );
    
    rect.origin.x = rect.size.width;
    rect.size.width = self.frame.size.width - rect.size.width;
    
    [ self drawRectangle : rect ];
}

-( void )drawLabels
{
    glUseProgram( progText );
    glUniform1f( glGetUniformLocation( progText, "div" ), self.frame.size.width / 2.f + lensPosition.x + divider );
    
    glActiveTexture( GL_TEXTURE0 );
    
    CGRect frame;
    
    glBindTexture( GL_TEXTURE_2D, interfaceSphericTexture );
    frame = self.sphericView.frame;
    frame.origin.x += lensPosition.x;
    frame.origin.y = self.frame.size.height - frame.origin.y - frame.size.height + lensPosition.y;
    glUniform1f( glGetUniformLocation( progText, "sign_" ), -1 );
    [ self drawRectangle : frame ];
    
    glBindTexture( GL_TEXTURE_2D, interfaceAsphericTexture );
    frame = self.asphericView.frame;
    frame.origin.x += lensPosition.x;
    frame.origin.y = self.frame.size.height - frame.origin.y - frame.size.height + lensPosition.y;
    glUniform1f( glGetUniformLocation( progText, "sign_" ), 1 );
    [ self drawRectangle : frame ];
}

-( void )drawDivider
{
    glUseProgram( progDivider );
    
    GLKVector2 lensPos = lensPosition;
    lensPos.y += LENS_SHIFT;
    glUniform2fv( glGetUniformLocation( progDivider, "lensPosition" ), 1, lensPos.v );
    
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( masks[ lensIndex ].target, masks[ lensIndex ].name );
    
    CGRect rect;
    rect.size = CGSizeMake( 3.f, self.frame.size.height );
    rect.origin = CGPointMake( self.frame.size.width / 2.f +  lensPosition.x + divider - 1.5f, 0.f );
    
    [ self drawRectangle : rect ];
}

-( void )drawArrow
{
    glUseProgram( progDefault );
    
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( arrow.target, arrow.name );
    
    CGRect rect;
    rect.size = CGSizeMake( arrow.width / 2.f, arrow.height / 2.f );
    rect.origin = CGPointMake( self.frame.size.width / 2.f +  lensPosition.x + divider - arrow.width / 4.f, self.frame.size.height / 2.f + lensPosition.y - 150.f );
    
    [ self drawRectangle : rect ];
}

-( void )setBackground : ( int )index
{
    backgroundIndex = index;
}

-( void )setShape : ( int )shape
{
    self->shape = shape;
    lensIndex = shape;
}

-( void )setPower : ( float )power
{
    b0 = 1.f;
    F3 = 0.0000003f * -power;
    
    /*if( aspheric == 1 )
     F3 *= 0.5f;
     else if( aspheric == 2 )
     F3 *= 0.3f;*/
    
    soapPower = power / 4.f;
}

-( void )setCyl : ( float )cyl
{
    F3_cyl = 0.0000003f * -cyl;
}

-( void )setAxis : ( float )axis
{
    self->axis = axis;
}

-( void )setLeftDesign : ( int )leftDesign
{
    self->leftDesign = leftDesign;
}

-( void )setRightDesign : ( int )rightDesign
{
    self->rightDesign = rightDesign;
}

-( void )setLeftIndex : ( float )index
{
    leftIndex = index;
}

-( void )setRightIndex : ( float )index
{
    rightIndex = index;
}

-( void )touchesBegan : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    for( UITouch * touch in touches )
    {
        CGPoint point = [ touch locationInView : self ];
        point.y = self.bounds.size.height - point.y;
        
        if( abs( self.frame.size.width / 2.f + lensPosition.x + divider - point.x ) < 150 )
            dragArrow = YES;
        
        dragPoint = point;
    }
}

-( void )touchesMoved : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    for( UITouch * touch in touches )
    {
        CGPoint point = [ touch locationInView : self ];
        point.y = self.bounds.size.height - point.y;
        
        if( dragArrow )
        {
            float divPrev = divider;
            divider += point.x - dragPoint.x;
            divSpeed = divider - divPrev;
        }
        else
        {
            lensPosition.x += point.x - dragPoint.x;
            lensPosition.y += point.y - dragPoint.y;
        }
        
        dragPoint = point;
    }
}

-( void )touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    lensPosition.x = 0.f;
    lensPosition.y = 0.f;
    
    dragArrow = NO;
}

-( void )touchesCancelled : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    [ self touchesEnded : touches withEvent : event ];
}

@end
