//
//  SMColorTrackingCamera.m
//  ACEPSmartMirror
//
//  Created by Dgut on 02.05.12.
//

#import "SMColorTrackingCamera.h"
#import "SMLogSaver.h"

#define SYSTEM_VERSION_GREATER_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

@interface SMColorTrackingCamera()
{
    NSString * preset;
    
    UIImage * photo;
}

@end

@implementation SMColorTrackingCamera

-( id )init
{
    return [ self initWithPreset : AVCaptureSessionPreset640x480 camera : AVCaptureDevicePositionBack ];
}

-( id )initWithPreset : ( NSString * )preset camera : ( AVCaptureDevicePosition )devicePosition
{
	if( !( self = [ super init ] ) )
		return nil;
    
    self->preset = preset;
	
	NSArray * devices = [ AVCaptureDevice devicesWithMediaType : AVMediaTypeVideo ];
	
    for( device in devices )
		if( device.position == devicePosition )
            break;
    
	captureSession = [ [ AVCaptureSession alloc ] init ];
	
	NSError * error = nil;
	videoInput = [ [ AVCaptureDeviceInput alloc ] initWithDevice : device error : &error ];
	if( [ captureSession canAddInput : videoInput ] )
		[ captureSession addInput : videoInput ];
	
	[ self videoPreviewLayer ];
    
    dispatch_queue_t queue = dispatch_queue_create( "cameraQueue", NULL );
	
	videoOutput = [ [ AVCaptureVideoDataOutput alloc ] init ];
	[ videoOutput setAlwaysDiscardsLateVideoFrames : YES ];
    [ videoOutput setVideoSettings : @{ ( id )kCVPixelBufferPixelFormatTypeKey: @( kCVPixelFormatType_32BGRA ) } ];
    [ videoOutput setSampleBufferDelegate : self queue : queue ];
    
	if( [ captureSession canAddOutput : videoOutput ] )
		[ captureSession addOutput : videoOutput ];
    
    imageOutput = [ [ AVCaptureStillImageOutput alloc ] init ];
    [ imageOutput setOutputSettings : @{ ( id )kCVPixelBufferPixelFormatTypeKey: @( kCVPixelFormatType_32BGRA ) } ];
    
    if( [ captureSession canAddOutput : imageOutput ] )
        [ captureSession addOutput : imageOutput ];

    //[ self startCamera ];
	
	return self;
}

-( void )setAutofocus
{
    NSError * error = nil;
    if( [ device lockForConfiguration : &error ] )
    {
        if( [ device isFocusPointOfInterestSupported ] && [ device isFocusModeSupported : AVCaptureFocusModeContinuousAutoFocus ] )
        {
            [ device setFocusMode : AVCaptureFocusModeContinuousAutoFocus ];
            [ device setFocusPointOfInterest : CGPointMake( 0.5f, 0.5f ) ];
        }
        if( [ device isExposurePointOfInterestSupported ] && [ device isExposureModeSupported : AVCaptureExposureModeContinuousAutoExposure ] )
        {
            
            [ device setExposureMode : AVCaptureExposureModeContinuousAutoExposure ];
            [ device setExposurePointOfInterest : CGPointMake( 0.5f, 0.5f )];
        }
        [ device unlockForConfiguration ];
    }
}

-( void )setFocus : ( float )value
{
    if ([device isFocusModeSupported:AVCaptureFocusModeLocked] && SYSTEM_VERSION_GREATER_THAN(@"8.0"))
    {
        NSError * error = nil;
        if( [ device lockForConfiguration : &error ] )
        {
            [ device setFocusMode : AVCaptureFocusModeLocked ];
            [ device setFocusModeLockedWithLensPosition : value completionHandler : nil ];
            [ device unlockForConfiguration ];
        }
    }
}

-( void )takePhoto
{
    [ imageOutput captureStillImageAsynchronouslyFromConnection : imageOutput.connections[ 0 ] completionHandler : ^( CMSampleBufferRef imageDataSampleBuffer, NSError * error )
    {
        CVImageBufferRef pixelBuffer = CMSampleBufferGetImageBuffer( imageDataSampleBuffer );
        
        CVPixelBufferLockBaseAddress( pixelBuffer, 0 );
        
        uint8_t * base = ( uint8_t * )CVPixelBufferGetBaseAddressOfPlane( pixelBuffer, 0 );
        size_t bytesPerRow = CVPixelBufferGetBytesPerRow( pixelBuffer );
        size_t width = CVPixelBufferGetWidth( pixelBuffer );
        size_t height = CVPixelBufferGetHeight( pixelBuffer );
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        
        CGContextRef context = CGBitmapContextCreate( base, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst );
        CGImageRef cgImage = CGBitmapContextCreateImage( context );
        CGContextRelease( context );
        
        CGColorSpaceRelease( colorSpace );
        CVPixelBufferUnlockBaseAddress( pixelBuffer, 0 );
        
        photo = [ UIImage imageWithCGImage : cgImage ];
        
        CGImageRelease( cgImage );
        
        [LOG saveLogData:@{ @"action" : @"SMColorTrackingCamera - takePhoto" }];
        
        [ self.delegate processPhoto : pixelBuffer ];
    } ];
}

-( UIImage * )photo
{
    return photo;
}

-( AVCaptureDevice * )device
{
    return device;
}

-( void )startCamera
{
    if( ![ captureSession isRunning ] )
    {
        [ captureSession setSessionPreset : preset ];
        [ captureSession startRunning ];
    }
}

-( void )stopCamera
{
    if( [ captureSession isRunning ] )
		[ captureSession stopRunning ];
}

- (BOOL)isActiveSession
{
    return [captureSession isRunning];
}

- (void)dealloc
{
	[self stopCamera];
}

#pragma mark -
#pragma mark AVCaptureVideoDataOutputSampleBufferDelegate

-( void )captureOutput : ( AVCaptureOutput * )captureOutput didOutputSampleBuffer : ( CMSampleBufferRef )sampleBuffer fromConnection : ( AVCaptureConnection * )connection
{
	CVImageBufferRef pixelBuffer = CMSampleBufferGetImageBuffer( sampleBuffer );
	[ self.delegate processCameraFrame : pixelBuffer ];
}

#pragma mark -
#pragma mark Accessors

@synthesize delegate;
@synthesize videoPreviewLayer;

-( AVCaptureVideoPreviewLayer * )videoPreviewLayer;
{
	if( videoPreviewLayer == nil )
	{
		videoPreviewLayer = [ [ AVCaptureVideoPreviewLayer alloc ] initWithSession : captureSession ];
        
        if( [ videoPreviewLayer.connection isVideoOrientationSupported ] )
            [ videoPreviewLayer.connection setVideoOrientation : AVCaptureVideoOrientationPortrait ];
        
        [ videoPreviewLayer setVideoGravity : AVLayerVideoGravityResizeAspectFill ];
	}
	
	return videoPreviewLayer;
}

#pragma mark - Flash

- (void)showFlash:(BOOL)show
{
    if ([device hasTorch])
    {
        [device lockForConfiguration:nil];
        
        if (show)
            [device setTorchModeOnWithLevel:[self getFlashPower] error:nil];
        else
            [device setTorchMode:AVCaptureTorchModeOff];
        
        [device unlockForConfiguration];
    }
}

- (float)getFlashPower
{
    BOOL isHigh = [[NSUserDefaults standardUserDefaults] boolForKey:@"flash_power_is_high"];
    
    return isHigh ? 0.8f : 0.5f;
}

@end
