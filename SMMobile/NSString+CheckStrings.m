//
//  NSstring+CheckStrings.m
//
//
//  Created by Sergej Bogatenko on 9/08/18.
//  
//

#import "NSString+CheckStrings.h"

@implementation NSString (CheckStrings)

- (BOOL)stringIsNotNil:(NSString *)string
{
    return [string stringByReplacingOccurrencesOfString:@" " withString:@""].length;
}

- (BOOL)validEmail:(NSString *)emailString
{
    if ([emailString length] == 0)
        return NO;
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern
                                                                      options:NSRegularExpressionCaseInsensitive
                                                                        error:nil];
    
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString
                                                     options:0
                                                       range:NSMakeRange(0, [emailString length])];
    
    if (regExMatches == 0)
        return NO;
    else
        return YES;
}

- (float)getTextWidth:(float)fontSize
{
    CGSize textSize = [self sizeWithAttributes:@{ NSFontAttributeName : [UIFont fontWithName:@"TrebuchetMS" size:fontSize] }];
    
    return textSize.width;
}

- (NSString *)removeNotAlphaNumericSymbols
{
    NSCharacterSet *charactersToRemove = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    NSString *strippedReplacement = [[self componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    
    return strippedReplacement;
}

- (NSString *)cutDeviceIDForFirebaseAnalytics
{
    if (self.length > 36)
        return [self substringToIndex:36];
    
    return self;
}

@end
