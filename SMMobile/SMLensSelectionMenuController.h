//
//  SMLensSelectionMenuController.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 8/7/14.
//

#import <UIKit/UIKit.h>

@interface SMLensSelectionMenuController : UIViewController
{
    IBOutlet UIImageView *mainBackground;
    IBOutlet UILabel *moduleNameLabel;
}

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *moduleButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *shapeButtons;

- (IBAction)showInfo:(id)sender;
- (IBAction)back:(id)sender;

@end
