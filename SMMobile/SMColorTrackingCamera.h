//
//  SMColorTrackingCamera.h
//  ACEPSmartMirror
//
//  Created by Dgut on 02.05.12.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>

@protocol SMColorTrackingCameraDelegate

@optional
-( void )processCameraFrame : ( CVImageBufferRef )cameraFrame;
-( void )processPhoto : ( CVImageBufferRef )imageData;

@end

@interface SMColorTrackingCamera : NSObject< AVCaptureVideoDataOutputSampleBufferDelegate >
{
	AVCaptureVideoPreviewLayer * videoPreviewLayer;
	AVCaptureSession * captureSession;
	AVCaptureDeviceInput * videoInput;
	AVCaptureVideoDataOutput * videoOutput;
    AVCaptureStillImageOutput * imageOutput;
    AVCaptureDevice * device;
}

@property ( nonatomic, assign ) id< SMColorTrackingCameraDelegate > delegate;
@property ( readonly ) AVCaptureVideoPreviewLayer * videoPreviewLayer;

-( id )initWithPreset : ( NSString * )preset camera : ( AVCaptureDevicePosition )devicePosition;

- (BOOL)isActiveSession;

-( void )startCamera;
-( void )stopCamera;

-( void )setAutofocus;
-( void )setFocus : ( float )value;

-( void )takePhoto;
-( UIImage * )photo;

-( AVCaptureDevice * )device;

- (void)showFlash:(BOOL)show;

@end
