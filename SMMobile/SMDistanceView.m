#import "SMDistanceView.h"

@implementation SMDistanceView

@synthesize distance;
@synthesize selectedButton;
@synthesize notSelectedButton;

-( void )baseInit
{
    distance = 0;
    newDistance = 0;
    newDistanceCount = 0;
    self.selectedButton = [ UIImage imageWithContentsOfFile : [ [ NSBundle mainBundle ] pathForResource : @"dist_selected_btn.png" ofType : NULL ] ];
    self.notSelectedButton = [ UIImage imageWithContentsOfFile : [ [ NSBundle mainBundle ] pathForResource : @"dist_btn.png" ofType : NULL ] ];
    self.opaque = NO;
    self.backgroundColor = [ UIColor clearColor ];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [ self baseInit ];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [ self baseInit ];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef contex = UIGraphicsGetCurrentContext();
    
    UIFont * font = [ UIFont fontWithName : @"Helvetica-Bold" size : 18 ];
    
    NSString * names[] = { NSLocalizedString(@"NEAR", @""), NSLocalizedString(@"MID", @""), NSLocalizedString(@"FAR", @"")};
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    
    paragraphStyle.lineBreakMode = NSLineBreakByClipping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{ NSFontAttributeName: font,
                                  NSParagraphStyleAttributeName: paragraphStyle };
    
    // Drawing code
    for( int i = 0; i < 3; i++ )
    {
        CGPoint point = CGPointMake( self.bounds.size.width / 2.f, self.bounds.size.height - ( i * 2 + 1 ) * ( self.bounds.size.height / 6.f ) );
        
        if( distance == i )
        {
            [ selectedButton drawInRect : CGRectMake( point.x - selectedButton.size.width / 2.f - 20, point.y - selectedButton.size.height / 2.f, selectedButton.size.width + 40, selectedButton.size.height ) ];
            
            CGContextSetFillColorWithColor( contex, [ UIColor colorWithRed : 1.f green : 1.f blue : 1.f alpha : 1.f ].CGColor );
            
            [names[i] drawInRect:CGRectMake( 0, self.bounds.size.height / 3.f * ( 2 - i ) + ( self.bounds.size.height / 3.f - font.pointSize ) / 2.f, self.bounds.size.width, font.pointSize ) withAttributes:attributes];
        }
        else
        {
            [ notSelectedButton drawInRect : CGRectMake( point.x - notSelectedButton.size.width / 2.f - 20, point.y - notSelectedButton.size.height / 2.f, notSelectedButton.size.width+40, notSelectedButton.size.height ) ];
            
            CGContextSetFillColorWithColor( contex, [ UIColor colorWithRed : 1.f green : 1.f blue : 1.f alpha : 0.75f ].CGColor );
            
            [names[i] drawInRect:CGRectMake( 0, self.bounds.size.height / 3.f * ( 2 - i ) + ( self.bounds.size.height / 3.f - font.pointSize ) / 2.f, self.bounds.size.width, font.pointSize ) withAttributes:attributes];
        }
    }
}

-( void )setDistance:(int)theDistance
{
    if( newDistance == theDistance )
        newDistanceCount++;
    else
    {
        newDistance = theDistance;
        newDistanceCount = 0;
    }
    
    if( newDistanceCount == 5 )
    {
        distance = newDistance;
        [ self setNeedsDisplay ];
    }
}

@end
