#pragma once

#include "LensMapData.h"

class CLensMap
{
public:
	CLensMap(void);
	~CLensMap(void);
	bool IsLensMapTypeChangeable(LMLensType lensType);
	void SetLensMapType(LMLensType lensType, double addDioptry);
	LMLensType GetLensMapType();
	double GetAdditionalDioptry();
	bool Calculate();
	unsigned char * GetLensMap();
	unsigned char * GetBlurMap();
	int GetLensWidth();
	int GetLensHeight();

protected:
	void CalCorridorY(int iy_s, int iy_e);
	void ClearBuffers();
	void ClearLMData();
	bool Calc_LensBlurMaps();
	bool Calc_LensBlurMaps_Threads();
	bool Calc_LensMap_Zoom();
	bool Calc_LensMap_Zoom2();
	bool Calc_LensMap_Blur();
	bool Calc_LensMap_DashedLine(bool bShowSeparateLines = false, bool bCenterZone = false);
	void DrawDash(double xs, double ys, double xe, double ye, int nWeight = 2, double Alpha = 1.);
	void DrawDashPoint1(double x, double y, double alfa);
	void DrawDashPoint2(double x, double y, double alfa);
	bool Calc_BlurMap();
	bool Calc_BlurMap2();
    bool Calc_BlurMap3();
	int  Round(double Arg);

	LMLensType lm_lensType;
	double lm_addDioptry, lm_k;
	LMLensMapData * lm;
	int lm_width, lm_height;
	int lm_upContour, lm_downContour, lm_leftContour, lm_rightContour;
	int lm_yUp, lm_yDown, lm_yLeft, lm_yRight, lm_yFarMid, lm_yMidNear;
	int lm_leftUp, lm_leftDown, lm_rightUp, lm_rightDown;
	int lm_dxZone, lm_dxZoneAverage;
	double lm_kEffectG, lm_kEffectB, lm_dxZoneCor;
	double lm_rLens_left, lm_rLens_right, lm_drLens;
	int lm_nBlur;
	int lm_left_dyBlur, lm_right_dyBlur;

	unsigned char * lm_LensMap;
	unsigned char * lm_BlurMap;

	bool lm_DataPresent;

	bool bRes_LensMap_Zoom, bRes_LensMap_Blur, bRes_LensMap_DashedLine, bRes_BlurMap;
	static void *thread_LensMap_Zoom(void *d)   { ((CLensMap *)d)->bRes_LensMap_Zoom = ((CLensMap *)d)->Calc_LensMap_Zoom(); return 0; }
	static void *thread_LensMap_Blur(void *d)   { ((CLensMap *)d)->bRes_LensMap_Blur = ((CLensMap *)d)->Calc_LensMap_Blur(); return 0; }
	static void *thread_LensMap_DashedLine(void *d)   { ((CLensMap *)d)->bRes_LensMap_DashedLine = ((CLensMap *)d)->Calc_LensMap_DashedLine(); return 0; }
	static void *thread_BlurMap(void *d)   { ((CLensMap *)d)->bRes_BlurMap = ((CLensMap *)d)->Calc_BlurMap(); return 0; }
};
