//#include "StdAfx.h"
#include "LensMap.h"
#include "math.h"
//#include "..\POSIX\pthread.h"
#include "pthread.h"
#include "string.h"
#include "stdlib.h"
// Lens data
#include "LensMap_Standard.h"
#include "LensMap2_Standard.h"
#include "LensMap_Premium.h"
#include "LensMap2_Premium.h"
#include "LensMap_Elite.h"
#include "LensMap2_Elite.h"
#include "LensMap_TailorMade.h"
#include "LensMap2_TailorMade.h"
#include "LensMap_Bifocal.h"
#include "LensMap2_Bifocal.h"
#include "LensMap_Reading.h"
#include "LensMap2_Reading.h"
#include "LensMap_Computer.h"
#include "LensMap2_Computer.h"

#define min(a,b) (((a) < (b)) ? (a) : (b))

CLensMap::CLensMap(void)
{
	lm_lensType = LMLT_NONE;
	lm_addDioptry = 0.5;
	lm_k = 0.;
	lm_DataPresent = false;
	lm = NULL;
	lm_width = 0;
	lm_height = 0;
	lm_yUp = lm_yDown = lm_yLeft = lm_yRight = lm_yFarMid = lm_yMidNear = 0;
	lm_leftUp = lm_leftDown = lm_rightUp = lm_rightDown = 0;
	lm_dxZone = lm_dxZoneAverage = 0;
	lm_LensMap = NULL;
	lm_BlurMap = NULL;
	lm_kEffectG = 1.;
	lm_kEffectB = 1.;
	lm_dxZoneCor = 92.;
	lm_rLens_left = lm_rLens_right = lm_drLens = 0.;
	lm_nBlur = 1;
	lm_left_dyBlur = lm_right_dyBlur = 10;
}

CLensMap::~CLensMap(void)
{
	lm_lensType = LMLT_NONE;
	lm_DataPresent = false;
	ClearBuffers();
	ClearLMData();
}

void CLensMap::ClearBuffers()
{
	if(lm_LensMap) {
		delete [] lm_LensMap;
		lm_LensMap = NULL;
	}
	if(lm_BlurMap) {
		delete [] lm_BlurMap;
		lm_BlurMap = NULL;
	}
}
void CLensMap::ClearLMData()
{
	if(lm) {
		delete [] lm;
		lm = NULL;
	}
}

bool CLensMap::IsLensMapTypeChangeable(LMLensType lensType)
{
	bool bRes = true;
	if(lensType == LMLT_BIFOCAL || lensType == LMLT2_BIFOCAL || lensType == LMLT_READING || lensType == LMLT2_READING || lensType == LMLT_COMPUTER || lensType == LMLT2_COMPUTER) {
		bRes = false;
	}
	return bRes;
}
void CLensMap::SetLensMapType(LMLensType lensType, double addDioptry)
{
	// Standard
	CLensMap_Standard lens_Standard;
	CLensMap2_Standard lens2_Standard;
	// Premium
	CLensMap_Premium lens_Premium;
	CLensMap2_Premium lens2_Premium;
	// Elite
	CLensMap_Elite lens_Elite;
	CLensMap2_Elite lens2_Elite;
	// Tailor Made
	CLensMap_TailorMade lens_TailorMade;
	CLensMap2_TailorMade lens2_TailorMade;
	// Bifocal
	CLensMap_Bifocal lens_Bifocal;
	CLensMap2_Bifocal lens2_Bifocal;
	// Single Vision Reading
	CLensMap_Reading lens_Reading;
	CLensMap2_Reading lens2_Reading;
	// Computer Lenses
	CLensMap_Computer lens_Computer;
	CLensMap2_Computer lens2_Computer;

	ClearBuffers();
	ClearLMData();
	lm_DataPresent = false;
	lm = NULL;
	lm_width = 0;
	lm_height = 0;
	lm_yUp = lm_yDown = lm_yLeft = lm_yRight = lm_yFarMid = lm_yMidNear = 0;
	lm_leftUp = lm_leftDown = lm_rightUp = lm_rightDown = 0;
	lm_dxZone = lm_dxZoneAverage = 0;
	lm_kEffectG = 1.;
	lm_kEffectB = 1.;
	lm_dxZoneCor = 96.;
	lm_rLens_left = lm_rLens_right = lm_drLens = 0.;

	lm_lensType = LMLT_NONE;
	switch(lensType) {
        case LMLT_NONE:
            break;
		case LMLT_STANDARD:
			lm = new LMLensMapData[lens_Standard.lm_height];
			memcpy(lm, lens_Standard.lm, sizeof(lens_Standard.lm));
			lm_width = lens_Standard.lm_width;
			lm_height = lens_Standard.lm_height;
			lm_lensType = lensType;
			lm_yUp = lens_Standard.lm_yUp;
			lm_yDown = lens_Standard.lm_yDown;
			lm_yLeft = lens_Standard.lm_yLeft;
			lm_yRight = lens_Standard.lm_yRight;
			lm_yFarMid = lens_Standard.lm_yFarMid;
			lm_yMidNear = lens_Standard.lm_yMidNear;
			lm_nBlur = lens_Standard.lm_nBlur;
			lm_left_dyBlur = lm_right_dyBlur = 10;
			lm_kEffectG = lens_Standard.lm_kEffectG;
			lm_kEffectB = lens_Standard.lm_kEffectB;
			break;
		case LMLT2_STANDARD:
			lm = new LMLensMapData[lens2_Standard.lm_height];
			memcpy(lm, lens2_Standard.lm, sizeof(lens2_Standard.lm));
			lm_width = lens2_Standard.lm_width;
			lm_height = lens2_Standard.lm_height;
			lm_lensType = lensType;
			lm_yUp = lens2_Standard.lm_yUp;
			lm_yDown = lens2_Standard.lm_yDown;
			lm_yLeft = lens2_Standard.lm_yLeft;
			lm_yRight = lens2_Standard.lm_yRight;
			lm_yFarMid = lens2_Standard.lm_yFarMid;
			lm_yMidNear = lens2_Standard.lm_yMidNear;
			lm_nBlur = lens2_Standard.lm_nBlur;
			lm_left_dyBlur = lm_right_dyBlur = 10;
			lm_kEffectG = lens2_Standard.lm_kEffectG;
			lm_kEffectB = lens2_Standard.lm_kEffectB;
			break;
		case LMLT_PREMIUM:
			lm = new LMLensMapData[lens_Premium.lm_height];
			memcpy(lm, lens_Premium.lm, sizeof(lens_Premium.lm));
			lm_width = lens_Premium.lm_width;
			lm_height = lens_Premium.lm_height;
			lm_lensType = lensType;
			lm_yUp = lens_Premium.lm_yUp;
			lm_yDown = lens_Premium.lm_yDown;
			lm_yLeft = lens_Premium.lm_yLeft;
			lm_yRight = lens_Premium.lm_yRight;
			lm_yFarMid = lens_Premium.lm_yFarMid;
			lm_yMidNear = lens_Premium.lm_yMidNear;
			lm_nBlur = lens_Premium.lm_nBlur;
			lm_left_dyBlur = lm_right_dyBlur = 10;
			lm_kEffectG = lens_Premium.lm_kEffectG;
			lm_kEffectB = lens_Premium.lm_kEffectB;
			break;
		case LMLT2_PREMIUM:
			lm = new LMLensMapData[lens2_Premium.lm_height];
			memcpy(lm, lens2_Premium.lm, sizeof(lens2_Premium.lm));
			lm_width = lens2_Premium.lm_width;
			lm_height = lens2_Premium.lm_height;
			lm_lensType = lensType;
			lm_yUp = lens2_Premium.lm_yUp;
			lm_yDown = lens2_Premium.lm_yDown;
			lm_yLeft = lens2_Premium.lm_yLeft;
			lm_yRight = lens2_Premium.lm_yRight;
			lm_yFarMid = lens2_Premium.lm_yFarMid;
			lm_yMidNear = lens2_Premium.lm_yMidNear;
			lm_nBlur = lens2_Premium.lm_nBlur;
			lm_left_dyBlur = lm_right_dyBlur = 10;
			lm_kEffectG = lens2_Premium.lm_kEffectG;
			lm_kEffectB = lens2_Premium.lm_kEffectB;
			break;
		case LMLT_ELITE:
			lm = new LMLensMapData[lens_Elite.lm_height];
			memcpy(lm, lens_Elite.lm, sizeof(lens_Elite.lm));
			lm_width = lens_Elite.lm_width;
			lm_height = lens_Elite.lm_height;
			lm_lensType = lensType;
			lm_yUp = lens_Elite.lm_yUp;
			lm_yDown = lens_Elite.lm_yDown;
			lm_yLeft = lens_Elite.lm_yLeft;
			lm_yRight = lens_Elite.lm_yRight;
			lm_yFarMid = lens_Elite.lm_yFarMid;
			lm_yMidNear = lens_Elite.lm_yMidNear;
			lm_nBlur = lens_Elite.lm_nBlur;
			lm_left_dyBlur = lm_right_dyBlur = 10;
			lm_kEffectG = lens_Elite.lm_kEffectG;
			lm_kEffectB = lens_Elite.lm_kEffectB;
			break;
		case LMLT2_ELITE:
			lm = new LMLensMapData[lens2_Elite.lm_height];
			memcpy(lm, lens2_Elite.lm, sizeof(lens2_Elite.lm));
			lm_width = lens2_Elite.lm_width;
			lm_height = lens2_Elite.lm_height;
			lm_lensType = lensType;
			lm_yUp = lens2_Elite.lm_yUp;
			lm_yDown = lens2_Elite.lm_yDown;
			lm_yLeft = lens2_Elite.lm_yLeft;
			lm_yRight = lens2_Elite.lm_yRight;
			lm_yFarMid = lens2_Elite.lm_yFarMid;
			lm_yMidNear = lens2_Elite.lm_yMidNear;
			lm_nBlur = lens2_Elite.lm_nBlur;
			lm_left_dyBlur = lm_right_dyBlur = 10;
			lm_kEffectG = lens2_Elite.lm_kEffectG;
			lm_kEffectB = lens2_Elite.lm_kEffectB;
			break;
		case LMLT_TAILORMADE:
			lm = new LMLensMapData[lens_TailorMade.lm_height];
			memcpy(lm, lens_TailorMade.lm, sizeof(lens_TailorMade.lm));
			lm_width = lens_TailorMade.lm_width;
			lm_height = lens_TailorMade.lm_height;
			lm_lensType = lensType;
			lm_yUp = lens_TailorMade.lm_yUp;
			lm_yDown = lens_TailorMade.lm_yDown;
			lm_yLeft = lens_TailorMade.lm_yLeft;
			lm_yRight = lens_TailorMade.lm_yRight;
			lm_yFarMid = lens_TailorMade.lm_yFarMid;
			lm_yMidNear = lens_TailorMade.lm_yMidNear;
			lm_nBlur = lens_TailorMade.lm_nBlur;
			lm_left_dyBlur = lm_right_dyBlur = 10;
			lm_kEffectG = lens_TailorMade.lm_kEffectG;
			lm_kEffectB = lens_TailorMade.lm_kEffectB;
			break;
		case LMLT2_TAILORMADE:
			lm = new LMLensMapData[lens2_TailorMade.lm_height];
			memcpy(lm, lens2_TailorMade.lm, sizeof(lens2_TailorMade.lm));
			lm_width = lens2_TailorMade.lm_width;
			lm_height = lens2_TailorMade.lm_height;
			lm_lensType = lensType;
			lm_yUp = lens2_TailorMade.lm_yUp;
			lm_yDown = lens2_TailorMade.lm_yDown;
			lm_yLeft = lens2_TailorMade.lm_yLeft;
			lm_yRight = lens2_TailorMade.lm_yRight;
			lm_yFarMid = lens2_TailorMade.lm_yFarMid;
			lm_yMidNear = lens2_TailorMade.lm_yMidNear;
			lm_nBlur = lens2_TailorMade.lm_nBlur;
			lm_left_dyBlur = lm_right_dyBlur = 10;
			lm_kEffectG = lens2_TailorMade.lm_kEffectG;
			lm_kEffectB = lens2_TailorMade.lm_kEffectB;
			break;
		case LMLT_BIFOCAL:
			lm = new LMLensMapData[lens_Bifocal.lm_height];
			memcpy(lm, lens_Bifocal.lm, sizeof(lens_Bifocal.lm));
			lm_width = lens_Bifocal.lm_width;
			lm_height = lens_Bifocal.lm_height;
			lm_lensType = lensType;
			lm_yUp = lens_Bifocal.lm_yUp;
			lm_yDown = lens_Bifocal.lm_yDown;
			lm_yLeft = lens_Bifocal.lm_yLeft;
			lm_yRight = lens_Bifocal.lm_yRight;
			lm_yFarMid = lens_Bifocal.lm_yFarMid;
			lm_yMidNear = lens_Bifocal.lm_yMidNear;
			lm_nBlur = lens_Bifocal.lm_nBlur;
			lm_left_dyBlur = lm_right_dyBlur = 10;
			lm_kEffectG = lens_Bifocal.lm_kEffectG;
			lm_kEffectB = lens_Bifocal.lm_kEffectB;
			break;
		case LMLT2_BIFOCAL:
			lm = new LMLensMapData[lens2_Bifocal.lm_height];
			memcpy(lm, lens2_Bifocal.lm, sizeof(lens2_Bifocal.lm));
			lm_width = lens2_Bifocal.lm_width;
			lm_height = lens2_Bifocal.lm_height;
			lm_lensType = lensType;
			lm_yUp = lens2_Bifocal.lm_yUp;
			lm_yDown = lens2_Bifocal.lm_yDown;
			lm_yLeft = lens2_Bifocal.lm_yLeft;
			lm_yRight = lens2_Bifocal.lm_yRight;
			lm_yFarMid = lens2_Bifocal.lm_yFarMid;
			lm_yMidNear = lens2_Bifocal.lm_yMidNear;
			lm_nBlur = lens2_Bifocal.lm_nBlur;
			lm_left_dyBlur = lm_right_dyBlur = 10;
			lm_kEffectG = lens2_Bifocal.lm_kEffectG;
			lm_kEffectB = lens2_Bifocal.lm_kEffectB;
			break;
		case LMLT_READING:
			lm = new LMLensMapData[lens_Reading.lm_height];
			memcpy(lm, lens_Reading.lm, sizeof(lens_Reading.lm));
			lm_width = lens_Reading.lm_width;
			lm_height = lens_Reading.lm_height;
			lm_lensType = lensType;
			lm_yUp = lens_Reading.lm_yUp;
			lm_yDown = lens_Reading.lm_yDown;
			lm_yLeft = lens_Reading.lm_yLeft;
			lm_yRight = lens_Reading.lm_yRight;
			lm_yFarMid = lens_Reading.lm_yFarMid;
			lm_yMidNear = lens_Reading.lm_yMidNear;
			lm_nBlur = lens_Reading.lm_nBlur;
			lm_left_dyBlur = lm_right_dyBlur = 10;
			lm_kEffectG = lens_Reading.lm_kEffectG;
			lm_kEffectB = lens_Reading.lm_kEffectB;
			break;
		case LMLT2_READING:
			lm = new LMLensMapData[lens2_Reading.lm_height];
			memcpy(lm, lens2_Reading.lm, sizeof(lens2_Reading.lm));
			lm_width = lens2_Reading.lm_width;
			lm_height = lens2_Reading.lm_height;
			lm_lensType = lensType;
			lm_yUp = lens2_Reading.lm_yUp;
			lm_yDown = lens2_Reading.lm_yDown;
			lm_yLeft = lens2_Reading.lm_yLeft;
			lm_yRight = lens2_Reading.lm_yRight;
			lm_yFarMid = lens2_Reading.lm_yFarMid;
			lm_yMidNear = lens2_Reading.lm_yMidNear;
			lm_nBlur = lens2_Reading.lm_nBlur;
			lm_left_dyBlur = lm_right_dyBlur = 10;
			lm_kEffectG = lens2_Reading.lm_kEffectG;
			lm_kEffectB = lens2_Reading.lm_kEffectB;
			break;
		case LMLT_COMPUTER:
			lm = new LMLensMapData[lens_Computer.lm_height];
			memcpy(lm, lens_Computer.lm, sizeof(lens_Computer.lm));
			lm_width = lens_Computer.lm_width;
			lm_height = lens_Computer.lm_height;
			lm_lensType = lensType;
			lm_yUp = lens_Computer.lm_yUp;
			lm_yDown = lens_Computer.lm_yDown;
			lm_yLeft = lens_Computer.lm_yLeft;
			lm_yRight = lens_Computer.lm_yRight;
			lm_yFarMid = lens_Computer.lm_yFarMid;
			lm_yMidNear = lens_Computer.lm_yMidNear;
			lm_nBlur = lens_Computer.lm_nBlur;
			lm_left_dyBlur = lm_right_dyBlur = 10;
			lm_kEffectG = lens_Computer.lm_kEffectG;
			lm_kEffectB = lens_Computer.lm_kEffectB;
			break;
		case LMLT2_COMPUTER:
			lm = new LMLensMapData[lens2_Computer.lm_height];
			memcpy(lm, lens2_Computer.lm, sizeof(lens2_Computer.lm));
			lm_width = lens2_Computer.lm_width;
			lm_height = lens2_Computer.lm_height;
			lm_lensType = lensType;
			lm_yUp = lens2_Computer.lm_yUp;
			lm_yDown = lens2_Computer.lm_yDown;
			lm_yLeft = lens2_Computer.lm_yLeft;
			lm_yRight = lens2_Computer.lm_yRight;
			lm_yFarMid = lens2_Computer.lm_yFarMid;
			lm_yMidNear = lens2_Computer.lm_yMidNear;
			lm_nBlur = lens2_Computer.lm_nBlur;
			lm_left_dyBlur = lm_right_dyBlur = 10;
			lm_kEffectG = lens2_Computer.lm_kEffectG;
			lm_kEffectB = lens2_Computer.lm_kEffectB;
			break;
	}
	// Set additional diopter
	lm_addDioptry = 0.5;
	lm_k = 0.;
	if(addDioptry >= 0.5 && addDioptry <= 4.0) {
		lm_addDioptry = addDioptry;
		lm_k = (lm_addDioptry - 0.5) / (4.0 - 0.5);
	}
	int iy = 0;
	if(lm_lensType != LMLT_NONE) {
		// Calculate diopters corridor
		CalCorridorY(0, lm_height - 1);
        // Calculate Up and Down points
        for(iy = 0; iy < lm_height; iy++) {
            if(lm[iy].ix_focalcorridor_left == lm[iy].ix_contour_left) {
                if(lm_leftDown == 0)
                    continue;
                break;
            }
            if(lm_leftUp == 0) {
                lm_leftUp = iy;
                lm_leftDown = iy;
                continue;
            }
            lm_leftDown = iy;
        }
        for(iy = 0; iy < lm_height; iy++) {
            if(lm[iy].ix_focalcorridor_right == lm[iy].ix_contour_right) {
                if(lm_rightDown == 0)
                    continue;
                break;
            }
            if(lm_rightUp == 0) {
                lm_rightUp = iy;
                lm_rightDown = iy;
                continue;
            }
            lm_rightDown = iy;
        }
        // Calculate minimum width of work zone
        // Calculate average width for near and middle zones
        int dxZoneAverage = 0, iZoneAverage = 0;
        int iy_waist = 0;
        for(iy = 0; iy < lm_height; iy++) {
            if(lm[iy].ix_focalcorridor_left == lm[iy].ix_contour_left)
                continue;
            if(lm[iy].ix_focalcorridor_right == lm[iy].ix_contour_right)
                continue;
            int dx = lm[iy].ix_focalcorridor_right - lm[iy].ix_focalcorridor_left + 1;
            if(lm_dxZone == 0) {
                lm_dxZone = dx;
                iy_waist = iy;
            }
            else if(dx < lm_dxZone) {
                lm_dxZone = dx;
                iy_waist = iy;
            }
            
            if(lm[iy].i_zone == 2 || lm[iy].i_zone == 3) {
                // Mid and Near zones
                dxZoneAverage += dx;
                iZoneAverage++;
            }
        }
        if (iZoneAverage > 0)
            lm_dxZoneAverage = dxZoneAverage / iZoneAverage;
        // Coefficients
        if (lm_dxZone > lm_dxZoneCor) {
//          lm_kEffectG = 1. - 0.3 * (lm_dxZone - lm_dxZoneCor) / lm_dxZoneCor;
//          lm_kEffectB = lm_kEffectB_cor * (1. - 0.15 * (lm_dxZone - lm_dxZoneCor) / lm_dxZoneCor);
        }
        // width zones
        lm_drLens = (lm[iy_waist].ix_focalcorridor_right - lm[iy_waist].ix_focalcorridor_left) / 2.;
        lm_rLens_left = (lm[iy_waist].ix_focalcorridor_left + lm_drLens) - lm[lm_yLeft].ix_contour_left;
        lm_rLens_right = lm[lm_yRight].ix_contour_right - (lm[iy_waist].ix_focalcorridor_left + lm_drLens);
    }
}
void CLensMap::CalCorridorY(int iy_s, int iy_e)
{
	int dxCorridor = 25;
	int iy = 0;
	if(lm_addDioptry == 2. || lm_lensType == LMLT_BIFOCAL || lm_lensType == LMLT2_BIFOCAL || lm_lensType == LMLT_READING || lm_lensType == LMLT2_READING || lm_lensType == LMLT_COMPUTER || lm_lensType == LMLT2_COMPUTER) {
		for(iy = iy_s; iy <= iy_e; iy++) {
			lm[iy].ix_focalcorridor_left = lm[iy].ix_20_focalcorridor_left;
			lm[iy].ix_focalcorridor_right = lm[iy].ix_20_focalcorridor_right;
		}
		return;
	}
	if(lm_addDioptry >= 0.0 && lm_addDioptry <= 4.0) {
		lm_k = (lm_addDioptry - 2.0) / 2.0;
	}
	// Calculate diopters corridor
	if(lm_lensType != LMLT_NONE) {
		double dx_left, dx_right;
		int iy_dx_left, iy_dx_right;
		int ix_left_s, ix_left_e, ix_right_s, ix_right_e;
		int iy_mid = lm_height / 2;
		// middle to up
		dx_left = dx_right = 0.;
		for(iy = iy_mid - 1; iy >= iy_s; iy--) {
			int dxCorridor20 = lm[iy].ix_20_focalcorridor_right - lm[iy].ix_20_focalcorridor_left;
			dxCorridor = (10 + 70 * dxCorridor20 / 720) / 2;
			if(lm_addDioptry > 2.) {
				ix_left_s = lm[iy].ix_20_focalcorridor_left;
				ix_left_e = lm[iy].ix_20_focalcorridor_left + dxCorridor;
			}
			else {
				ix_left_s = lm[iy].ix_20_focalcorridor_left;
				ix_left_e = lm[iy].ix_20_focalcorridor_left - dxCorridor;
			}
			if(ix_left_s == 0)
				ix_left_e = 0;
			if(lm[iy].ix_contour_left == ix_left_s && lm[iy].ix_contour_left == ix_left_e)
				lm[iy].ix_focalcorridor_left = lm[iy].ix_contour_left;
			else if(lm[iy].ix_contour_left == ix_left_s) {
				lm[iy].ix_focalcorridor_left = lm[iy + 1].ix_focalcorridor_left + dx_left;
				if(lm[iy].ix_focalcorridor_left < lm[iy].ix_contour_left)
					lm[iy].ix_focalcorridor_left = lm[iy].ix_contour_left;
			}
			else {
				lm[iy].ix_focalcorridor_left = ix_left_s + Round(fabs(ix_left_e - ix_left_s) * lm_k);
				dx_left = lm[iy].ix_focalcorridor_left - lm[iy + 1].ix_focalcorridor_left;
				if(lm[iy].ix_focalcorridor_left < lm[iy].ix_contour_left)
					lm[iy].ix_focalcorridor_left = lm[iy].ix_contour_left;
			}

			if(lm_addDioptry > 2.) {
				ix_right_s = lm[iy].ix_20_focalcorridor_right;
				ix_right_e = lm[iy].ix_20_focalcorridor_right - dxCorridor;
			}
			else {
				ix_right_s = lm[iy].ix_20_focalcorridor_right;
				ix_right_e = lm[iy].ix_20_focalcorridor_right + dxCorridor;
			}
			if(ix_right_s == 0)
				ix_right_e = 0;
			if(lm[iy].ix_contour_right == ix_right_s && lm[iy].ix_contour_right == ix_right_e)
				lm[iy].ix_focalcorridor_right = lm[iy].ix_contour_right;
			else if(lm[iy].ix_contour_right == ix_right_s) {
				lm[iy].ix_focalcorridor_right = lm[iy + 1].ix_focalcorridor_right + dx_right;
				if(lm[iy].ix_focalcorridor_right > lm[iy].ix_contour_right)
					lm[iy].ix_focalcorridor_right = lm[iy].ix_contour_right;
			}
			else {
				lm[iy].ix_focalcorridor_right = ix_right_s - Round(fabs(ix_right_e - ix_right_s) * lm_k);
				dx_right = lm[iy].ix_focalcorridor_right - lm[iy + 1].ix_focalcorridor_right;
				if(lm[iy].ix_focalcorridor_right > lm[iy].ix_contour_right)
					lm[iy].ix_focalcorridor_right = lm[iy].ix_contour_right;
			}
		}
		// middle to down
		dx_left = dx_right = 0;
		for(iy = iy_mid; iy <= iy_e; iy++) {
			int dxCorridor20 = lm[iy].ix_20_focalcorridor_right - lm[iy].ix_20_focalcorridor_left;
			dxCorridor = (10 + 70 * dxCorridor20 / 720) / 2;
			if(lm_addDioptry > 2.) {
				ix_left_s = lm[iy].ix_20_focalcorridor_left;
				ix_left_e = lm[iy].ix_20_focalcorridor_left + dxCorridor;
			}
			else {
				ix_left_s = lm[iy].ix_20_focalcorridor_left;
				ix_left_e = lm[iy].ix_20_focalcorridor_left - dxCorridor;
			}
			if(ix_left_s == 0)
				ix_left_e = 0;
			if(lm[iy].ix_contour_left == ix_left_s && lm[iy].ix_contour_left == ix_left_e)
				lm[iy].ix_focalcorridor_left = lm[iy].ix_contour_left;
			else if(lm[iy].ix_contour_left == ix_left_s) {
				lm[iy].ix_focalcorridor_left = lm[iy_dx_left].ix_focalcorridor_left + Round((iy - iy_dx_left) * dx_left);
				if(lm[iy].ix_focalcorridor_left < lm[iy].ix_contour_left)
					lm[iy].ix_focalcorridor_left = lm[iy].ix_contour_left;
			}
			else {
				lm[iy].ix_focalcorridor_left = ix_left_s + Round(fabs(ix_left_e - ix_left_s) * lm_k);
				int hy = 1;
				if((lm[iy].ix_focalcorridor_left - lm[iy - hy].ix_focalcorridor_left) == 0) {
					hy = 10;
				}
				iy_dx_left = iy;
				dx_left = double(lm[iy].ix_focalcorridor_left - lm[iy - hy].ix_focalcorridor_left) / hy;
				if(hy > 1)
					dx_left *= 0.5;
				if(lm[iy].ix_focalcorridor_left < lm[iy].ix_contour_left)
					lm[iy].ix_focalcorridor_left = lm[iy].ix_contour_left;
			}

			if(lm_addDioptry > 2.) {
				ix_right_s = lm[iy].ix_20_focalcorridor_right;
				ix_right_e = lm[iy].ix_20_focalcorridor_right - dxCorridor;
			}
			else {
				ix_right_s = lm[iy].ix_20_focalcorridor_right;
				ix_right_e = lm[iy].ix_20_focalcorridor_right + dxCorridor;
			}
			if(ix_right_s == 0)
				ix_right_e = 0;
			if(lm[iy].ix_contour_right == ix_right_s && lm[iy].ix_contour_right == ix_right_e)
				lm[iy].ix_focalcorridor_right = lm[iy].ix_contour_right;
			else if(lm[iy].ix_contour_right == ix_right_s) {
				lm[iy].ix_focalcorridor_right = lm[iy_dx_right].ix_focalcorridor_right + Round((iy - iy_dx_right) * dx_right);
				if(lm[iy].ix_focalcorridor_right > lm[iy].ix_contour_right)
					lm[iy].ix_focalcorridor_right = lm[iy].ix_contour_right;
			}
			else {
				lm[iy].ix_focalcorridor_right = ix_right_s - Round(fabs(ix_right_e - ix_right_s) * lm_k);
				int hy = 1;
				if((lm[iy].ix_focalcorridor_right - lm[iy - hy].ix_focalcorridor_right) == 0) {
					hy = 10;
				}
				iy_dx_right = iy;
				dx_right = double(lm[iy].ix_focalcorridor_right - lm[iy - hy].ix_focalcorridor_right) / hy;
				if(hy > 1)
					dx_right *= 0.5;
				if(lm[iy].ix_focalcorridor_right > lm[iy].ix_contour_right)
					lm[iy].ix_focalcorridor_right = lm[iy].ix_contour_right;
			}
		}
	}
	int aLeft[488], aRight[488];
	for(iy = 0; iy < 488; iy++) {
		aLeft[iy] = lm[iy].ix_focalcorridor_left;
		aRight[iy] = lm[iy].ix_focalcorridor_right;
	}
}
LMLensType CLensMap::GetLensMapType()
{
	return lm_lensType;
}
double CLensMap::GetAdditionalDioptry()
{
	return lm_addDioptry;
}

bool CLensMap::Calculate()
{
	bool bRes = false;
	lm_DataPresent = false;
	ClearBuffers();

	if(lm_width > 0 && lm_height > 0) {
		lm_LensMap = new unsigned char[lm_height * lm_width * 4];
		memset(lm_LensMap, 0, lm_height * lm_width * 4);

		lm_BlurMap = new unsigned char[lm_height * lm_width * 4];
		memset(lm_BlurMap, 0, lm_height * lm_width * 4);

		if((bRes = Calc_LensBlurMaps())) {
			lm_DataPresent = true;
		}
	}

	return bRes;
}
bool CLensMap::Calc_LensBlurMaps()
{
	bool bRes = false;
	bool bZoom = true;
	bool bBlur = true;
	bool bDashedLine = true;
	bool bRes_BlurMap = true;
	if(lm_lensType == LMLT_BIFOCAL || lm_lensType == LMLT2_BIFOCAL) {
		bZoom = Calc_LensMap_Zoom2();
		bDashedLine = Calc_LensMap_DashedLine(false, true);
		bRes_BlurMap = Calc_BlurMap2();
	}
	else if(lm_lensType == LMLT_READING || lm_lensType == LMLT2_READING) {
		bZoom = Calc_LensMap_Zoom2();
		bRes_BlurMap = Calc_BlurMap2();
	}
    else if(lm_lensType == LMLT_COMPUTER || lm_lensType == LMLT2_COMPUTER) {
        bZoom = Calc_LensMap_Zoom();
        bBlur = Calc_LensMap_Blur();
        bRes_BlurMap = Calc_BlurMap3();
    }
	else {
		bZoom = Calc_LensMap_Zoom();
		bBlur = Calc_LensMap_Blur();
		bDashedLine = Calc_LensMap_DashedLine();
		bRes_BlurMap = Calc_BlurMap();
	}
	
	bool bRes_LensMap = bZoom && bBlur && bDashedLine;
	if(bRes_LensMap && bRes_BlurMap) {
		bRes = true;
	}
	return bRes;
}
bool CLensMap::Calc_LensBlurMaps_Threads()
{
	bool bRes = false;
	bRes_LensMap_Zoom = bRes_LensMap_Blur = bRes_LensMap_DashedLine = bRes_BlurMap = false;

	pthread_t tid_LensMap_Zoom, tid_LensMap_Blur, tid_LensMap_DashedLine, tid_BlurMap;
	int thr_LensMap_Zoom = pthread_create(&tid_LensMap_Zoom, NULL, CLensMap::thread_LensMap_Zoom, (void*)this); 
	int thr_LensMap_Blur = pthread_create(&tid_LensMap_Blur, NULL, CLensMap::thread_LensMap_Blur, (void*)this); 
	int thr_LensMap_DashedLine = pthread_create(&tid_LensMap_DashedLine, NULL, CLensMap::thread_LensMap_DashedLine, (void*)this);
	int thr_BlurMap = pthread_create(&tid_BlurMap, NULL, CLensMap::thread_BlurMap, (void*)this);

	if(!thr_LensMap_Zoom && !thr_LensMap_Blur && !thr_LensMap_DashedLine && !thr_BlurMap) {
		int join_LensMap_Zoom = pthread_join(tid_LensMap_Zoom, NULL); 
		int join_LensMap_Blur = pthread_join(tid_LensMap_Blur, NULL); 
		int join_LensMap_DashedLine = pthread_join(tid_LensMap_DashedLine, NULL);
		int join_BlurMap = pthread_join(tid_BlurMap, NULL);
		if(!join_LensMap_Zoom && !join_LensMap_Blur && !join_LensMap_DashedLine && !join_BlurMap) {
			bRes = bRes_LensMap_Zoom && bRes_LensMap_Blur && bRes_LensMap_DashedLine && bRes_BlurMap;
		}
	}

	return bRes;
}
bool CLensMap::Calc_LensMap_Zoom()
{
	bool bRes = false;
	if(lm_lensType == LMLT_NONE || lm == NULL)
		return false;
	if(lm_LensMap == NULL)
		return false;

	_RGBQUAD_ * pTag = NULL;
	int ix = 0, iy = 0, dx = 0, iry = 0;
	// Focal zones
	const _RGBQUAD_ colorFar = {0x00, 0x00, 0x00, 0xFF};
	_RGBQUAD_ colorMid = {0x00, 0x00, 0x00, 0xFF};
	_RGBQUAD_ colorNear = {0x00, ( unsigned char )( lm_kEffectG * 255 ), 0x00, 0xFF};
	for(iy = 0; iy < lm_height; iy++) {
		dx = lm[iy].ix_focalcorridor_right - lm[iy].ix_focalcorridor_left + 1;
		iry = lm_height - iy - 1;
		pTag = (_RGBQUAD_ *)(lm_LensMap + 4 * (iry * lm_width + lm[iy].ix_focalcorridor_left));
		switch(lm[iy].i_zone) {
			case 1:	// Far zone
//				lm[iy].i_focal = colorFar.rgbGreen;
				for(ix = 0; ix < dx; ix++) {
					pTag[ix] = colorFar;
				}
				break;
			case 2: // Mid zone
				colorMid.rgbGreen = Round(lm_kEffectG * 255 * (200 - lm[iy].i_focal) / 100);
//				lm[iy].i_focal = colorMid.rgbGreen;
				for(ix = 0; ix < dx; ix++) {
					pTag[ix] = colorMid;
				}
				break;
			case 3: // Near zone
//				lm[iy].i_focal = colorNear.rgbGreen;
				for(ix = 0; ix < dx; ix++) {
					pTag[ix] = colorNear;
				}
				break;
		}
	}
	// Lateral zones - zoom
	_RGBQUAD_ colorLateral = {0x00, 0x00, 0x00, 0xFF};
	// left zone
	if(lm_leftUp > 0 && lm_leftDown > 0) {
		for(iy = lm_leftUp; iy <= lm_leftDown; iy++) {
			// Target point color
			double tagRed = 190. - 90. * double(iy - lm_leftUp) / double(lm_leftDown - lm_leftUp);
			iry = lm_height - iy - 1;
			pTag = (_RGBQUAD_ *)(lm_LensMap + 4 * (iry * lm_width));
			double srcRed = lm[iy].i_focal;
//			dx = lm[iy].ix_focalcorridor_left - lm[iy].ix_contour_left + 1;
			for(ix = lm[iy].ix_contour_left; ix < lm[iy].ix_focalcorridor_left; ix++) {
				double Red = srcRed + (tagRed - srcRed) * (lm[iy].ix_focalcorridor_left - ix) / (lm_rLens_left - lm_drLens);
				double diopt = lm_kEffectG * 255. * (200 - Red) / 100;
				colorLateral.rgbGreen = Round(diopt);
				pTag[ix] = colorLateral;
			}
		}
	}
	// right zone
	if(lm_rightUp > 0 && lm_rightDown > 0) {
		for(iy = lm_rightUp; iy <= lm_rightDown; iy++) {
			// Target point color
			double tagRed = 190. - 90. * double(iy - lm_rightUp) / double(lm_rightDown - lm_rightUp);
			iry = lm_height - iy - 1;
			pTag = (_RGBQUAD_ *)(lm_LensMap + 4 * (iry * lm_width));
			double srcRed = lm[iy].i_focal;
//			dx = lm[iy].ix_focalcorridor_left - lm[iy].ix_contour_left + 1;
			for(ix = lm[iy].ix_focalcorridor_right + 1; ix <= lm[iy].ix_contour_right; ix++) {
				double Red = srcRed + (tagRed - srcRed) * (ix - lm[iy].ix_focalcorridor_right + 1) / (lm_rLens_right - lm_drLens);
				double diopt = lm_kEffectG * 255. * (200 - Red) / 100;
				colorLateral.rgbGreen = Round(diopt);
				pTag[ix] = colorLateral;
			}
		}
	}

	bRes = true;

	return bRes;
}
bool CLensMap::Calc_LensMap_Zoom2()
{
	bool bRes = false;
	if(lm_lensType == LMLT_NONE || lm == NULL)
		return false;
	if(lm_LensMap == NULL)
		return false;

	if(lm_lensType != LMLT_BIFOCAL && lm_lensType != LMLT2_BIFOCAL && lm_lensType != LMLT_READING && lm_lensType != LMLT2_READING)
		return false;

	_RGBQUAD_ * pTag = NULL;
	int ix = 0, iy = 0, dx = 0, iry = 0;
	// Focal zones
	const _RGBQUAD_ colorFar = {0x00, 0x00, 0x00, 0xFF};
	const _RGBQUAD_ colorNear = {0x00, 0xFF, 0x00, 0xFF};
	if(lm_lensType == LMLT_BIFOCAL || lm_lensType == LMLT2_BIFOCAL) {
		// Far zone
		for(iy = 0; iy < lm_height; iy++) {
			if(lm[iy].ix_contour_right == 0 || lm[iy].ix_contour_left == 0)
				continue;
			dx = lm[iy].ix_contour_right - lm[iy].ix_contour_left + 1;
			iry = lm_height - iy - 1;
			pTag = (_RGBQUAD_ *)(lm_LensMap + 4 * (iry * lm_width + lm[iy].ix_contour_left));
			for(ix = 0; ix < dx; ix++) {
				pTag[ix] = colorFar;
			}
		}
	}
	// Near zone
	for(iy = 0; iy < lm_height; iy++) {
		dx = lm[iy].ix_focalcorridor_right - lm[iy].ix_focalcorridor_left + 1;
		iry = lm_height - iy - 1;
		pTag = (_RGBQUAD_ *)(lm_LensMap + 4 * (iry * lm_width + lm[iy].ix_focalcorridor_left));
		switch(lm[iy].i_zone) {
			case 3: // Near zone
				//				lm[iy].i_focal = colorNear.rgbGreen;
				for(ix = 0; ix < dx; ix++) {
					pTag[ix] = colorNear;
				}
				break;
		}
	}

	bRes = true;

	return bRes;
}
bool CLensMap::Calc_LensMap_Blur()
{
	bool bRes = false;
	if(lm_lensType == LMLT_NONE || lm == NULL)
		return false;
	if(lm_LensMap == NULL)
		return false;

	_RGBQUAD_ * pTag = NULL;
	int ix = 0, iy = 0, iry = 0;

	// Lateral zones - blur
	int yBlue1 = 220, yBlue2 = 220;
	double downY = 0.;
	// left zone
	if(lm_leftUp > 0 && lm_leftDown > 0) {
		for(iy = lm_leftUp; iy <= lm_leftDown; iy++) {
			iry = lm_height - iy - 1;
			pTag = (_RGBQUAD_ *)(lm_LensMap + 4 * (iry * lm_width));
			for(ix = lm[iy].ix_contour_left; ix < lm[iy].ix_focalcorridor_left; ix++) {
				yBlue1 = 50 + lm_kEffectB * 170 * ((lm[iy].ix_focalcorridor_left - 1) - ix) / (lm_rLens_left - lm_drLens);

				if(lm_nBlur == 1) {
					downY = (ix - (lm[lm_leftUp].ix_focalcorridor_left - 1)) *((lm_left_dyBlur) / (lm[lm_leftUp + lm_left_dyBlur].ix_focalcorridor_left - lm[lm_leftUp].ix_focalcorridor_left)) + lm_leftUp;
					yBlue2 = (50 + 170 * (iy - downY) / (lm_leftDown - lm_leftUp)) * 1.4;
					pTag[ix].rgbBlue = min(220, min(yBlue1, yBlue2));
				}
				else
					pTag[ix].rgbBlue = min(yBlue1, 220);
			}
		}
	}
	// right zone
	if(lm_rightUp > 0 && lm_rightDown > 0) {
		for(iy = lm_rightUp; iy <= lm_rightDown; iy++) {
			iry = lm_height - iy - 1;
			pTag = (_RGBQUAD_ *)(lm_LensMap + 4 * (iry * lm_width));
			for(ix = lm[iy].ix_focalcorridor_right + 1; ix <= lm[iy].ix_contour_right; ix++) {
				yBlue1 = 50 + lm_kEffectB * 170 * (ix - (lm[iy].ix_focalcorridor_right + 1)) / (lm_rLens_right - lm_drLens);

				if(lm_nBlur == 1) {
					downY = (ix - (lm[lm_rightUp].ix_focalcorridor_right + 1)) *((lm_right_dyBlur) / (lm[lm_rightUp + lm_right_dyBlur].ix_focalcorridor_right - lm[lm_rightUp].ix_focalcorridor_right)) + lm_rightUp;
					yBlue2 = (50 + 170 * (iy - downY) / (lm_rightDown - lm_rightUp)) * 1.4;
					pTag[ix].rgbBlue = min(220, min(yBlue1, yBlue2));
				}
				else
					pTag[ix].rgbBlue = min(yBlue1, 220);
			}
		}
	}

	bRes = true;

	return bRes;
}
bool CLensMap::Calc_LensMap_DashedLine(bool bShowSeparateLines, bool bCenterZone)
{
	bool bRes = false;
	if(lm_lensType == LMLT_NONE || lm == NULL)
		return false;
	if(lm_LensMap == NULL)
		return false;

	double dashLen1 = 6.;
	double dashLen2 = 7.;
	int iy = 0, dx = 0;
	double dLen = 0., Len = 0., segLen = 0.;
	int ixs, ixe;
	bool bDash = false;
	double dashLen = dashLen2;
	double xs, xe, ys, ye;
	double xsegs, ysegs;
	// left zone
	if(lm_leftUp > 0 && lm_leftDown > 0) {
		iy = 0, dx = 0;
		dLen = 0., Len = 0., segLen = 0.;
		dashLen = dashLen2;
		segLen = dashLen / 2.;
		bDash = false;
		if(bCenterZone)
			ixs = (lm[lm_leftUp].ix_focalcorridor_right + lm[lm_leftUp].ix_focalcorridor_left) / 2.;
		else
			ixs = lm[lm_leftUp].ix_contour_left;
		xsegs = ixs;
		ysegs = lm_leftUp;
		for(iy = lm_leftUp; iy <= lm_leftDown; iy++) {
			ixe = lm[iy].ix_focalcorridor_left;
			dx = ixe - ixs;
			dLen = sqrt(dx * dx + 1);
			xs = ixs;
			ys = iy;
			xe = ixe;
			ye = iy + 1;

			double aa;
			while(1) {
				if(segLen + dLen > dashLen) {
					double x, y;
					y = ye - (ye - ys) * (((segLen + dLen) - dashLen) / dLen);
					x = xs + (xe - xs) * ((y - ys) / (ye - ys));
					aa = sqrt((x - xsegs) * (x - xsegs) + (y - ysegs) * (y - ysegs));
					if(bDash) {
						DrawDash(xsegs, ysegs, x, y);
					}
					dLen = (segLen + dLen) - dashLen;
					xsegs = xs = x;
					ysegs = ys = y;
					bDash = !bDash;
					if(bDash)
						dashLen = dashLen1;
					else
						dashLen = dashLen2;
					segLen = 0.;
				}
				else {
					segLen += dLen;
					aa = sqrt((xe - xsegs) * (xe - xsegs) + (ye - ysegs) * (ye - ysegs));
					break;
				}
			}
			ixs = ixe;
		}
	}
	// right zone
	if(lm_rightUp > 0 && lm_rightDown > 0) {
		iy = 0, dx = 0;
		dLen = 0., Len = 0., segLen = 0.;
		dashLen = dashLen2;
		segLen = dashLen / 2.;
		bDash = false;
		if(bCenterZone)
			ixs = (lm[lm_leftUp].ix_focalcorridor_right + lm[lm_leftUp].ix_focalcorridor_left) / 2.;
		else
			ixs = lm[lm_rightUp].ix_contour_right + 1;
		xsegs = ixs;
		ysegs = lm_rightUp;
		for(iy = lm_rightUp; iy <= lm_rightDown; iy++) {
			ixe = lm[iy].ix_focalcorridor_right + 1;
			dx = ixe - ixs;
			dLen = sqrt(dx * dx + 1);
			xs = ixs;
			ys = iy;
			xe = ixe;
			ye = iy + 1;

			while(1) {
				if(segLen + dLen > dashLen) {
					double x, y;
					y = ye - (ye - ys) * (((segLen + dLen) - dashLen) / dLen);
					x = xs + (xe - xs) * ((y - ys) / (ye - ys));
					if(bDash) {
						DrawDash(xsegs, ysegs, x, y);
					}
					dLen = (segLen + dLen) - dashLen;
					xsegs = xs = x;
					ysegs = ys = y;
					bDash = !bDash;
					if(bDash)
						dashLen = dashLen1;
					else
						dashLen = dashLen2;
					segLen = 0.;
				}
				else {
					segLen += dLen;
					break;
				}
			}
			ixs = ixe;
		}
	}
	if(bShowSeparateLines) {
		// far - mid separating line
		if(lm_yFarMid > lm_yUp) {
			dx = 0;
			dLen = 0., Len = 0., segLen = 0.;
			dashLen = dashLen2;
			segLen = dashLen / 2.;
			bDash = false;
			ixs = lm[lm_yFarMid].ix_focalcorridor_left;
			xsegs = ixs;
			ysegs = lm_yFarMid;

			ixe = lm[lm_yFarMid].ix_focalcorridor_right + 1;
			dx = ixe - ixs;
			dLen = sqrt(dx * dx);
			xs = ixs;
			ys = lm_yFarMid;
			xe = ixe;
			ye = lm_yFarMid;

			int n = Round((dLen - dashLen2) / (dashLen1 + dashLen2));
			dashLen2 = (dLen - dashLen1 * n) / (n + 1);

			while(1) {
				if(segLen + dLen > dashLen) {
					double x, y;
					y = ys;
					x = xs + dashLen;
					if(bDash) {
						DrawDash(xsegs, ysegs, x, y, 1, 0.85);
					}
					dLen = (segLen + dLen) - dashLen;
					xsegs = xs = x;
					ysegs = ys = y;
					bDash = !bDash;
					if(bDash)
						dashLen = dashLen1;
					else
						dashLen = dashLen2;
					segLen = 0.;
				}
				else {
					segLen += dLen;
					break;
				}
			}
			ixs = ixe;
		}
		// mid - near separating line
		{
			dx = 0;
			dLen = 0., Len = 0., segLen = 0.;
			dashLen = dashLen2;
			segLen = dashLen / 2.;
			bDash = false;
			ixs = lm[lm_yMidNear].ix_focalcorridor_left;
			xsegs = ixs;
			ysegs = lm_yMidNear;

			ixe = lm[lm_yMidNear].ix_focalcorridor_right + 1;
			dx = ixe - ixs;
			dLen = sqrt(dx * dx);
			xs = ixs;
			ys = lm_yMidNear;
			xe = ixe;
			ye = lm_yMidNear;

			int n = Round((dLen - dashLen2) / (dashLen1 + dashLen2));
			dashLen2 = (dLen - dashLen1 * n) / (n + 1);

			while(1) {
				if(segLen + dLen > dashLen) {
					double x, y;
					y = ys;
					x = xs + dashLen;
					if(bDash) {
						DrawDash(xsegs, ysegs, x, y, 1, 0.85);
					}
					dLen = (segLen + dLen) - dashLen;
					xsegs = xs = x;
					ysegs = ys = y;
					bDash = !bDash;
					if(bDash)
						dashLen = dashLen1;
					else
						dashLen = dashLen2;
					segLen = 0.;
				}
				else {
					segLen += dLen;
					break;
				}
			}
			ixs = ixe;
		}
	}

	bRes = true;

	return bRes;
}
void CLensMap::DrawDash(double xs, double ys, double xe, double ye, int nWeight, double Alpha)
{
	if(lm_LensMap) {
		double x, y;
		x = xs;
		y = ys;
		double dx = xe - xs;
		double dy = ye - ys;
		double ka;
		if(abs(dx) > abs(dy)) {
			if(xe < xs) {
				double xb = xs;
				double yb = ys;
				xs = xe;
				ys = ye;
				xe = xb;
				ye = yb;
				x = xs;
				y = ys;
			}
			ka = Alpha * (((int)x + 1.) - x);
			while(1) {
				switch(nWeight){
					case 1:
						DrawDashPoint1(x, y, ka);
						break;
					case 2:
						DrawDashPoint2(x, y, ka);
						break;
				}
				if(x >= xe)
					break;
				if(x - (int)x > 0) {
					x = (int)x + 1;
				}
				else
					x += 1;
				ka = Alpha;
				if(x > xe) {
					switch(nWeight){
					case 1:
						x = xe + 1;
						break;
					case 2:
						x = xe;
						break;
					}
					ka = Alpha * (x - ((int)x));
				}
				y = ys + (ye - ys) * ((x - xs) / (xe - xs));
			}
		}
		else {
			ka = Alpha * (((int)y + 1.) - y);
			while(1) {
				switch(nWeight){
					case 1:
						DrawDashPoint1(x, y, ka);
						break;
					case 2:
						DrawDashPoint2(x, y, ka);
						break;
				}
				if(y >= ye)
					break;
				if(y - (int)y > 0) {
					y = (int)y + 1;
				}
				else
					y += 1;
				ka = Alpha;
				if(y > ye) {
					switch(nWeight){
					case 1:
						y = ye + 1;
						break;
					case 2:
						y = ye;
						break;
					}
					ka = Alpha * (y - ((int)y));
				}
				x = xs + (xe - xs) * ((y - ys) / (ye - ys));
			}
		}
	}
}
void CLensMap::DrawDashPoint1(double x, double y, double alfa)
{
	if(lm_LensMap) {
		_RGBQUAD_ * pTag = NULL;
		int ix[2], iy[2];
		double a[2][2];
		double ka = alfa;

		if(x == (int)x)
			ix[0] = (int)x - 1;
		else 
			ix[0] = (int)x;
		if(y == (int)y)
			iy[0] = (int)y - 1;
		else
			iy[0] = (int)y;

		ix[0] = (int)x;
		iy[0] = (int)y;

		a[0][0] = ka;

		int i, j, iry = 0;
		for(i = 0; i < 1; i++) {
			for(j = 0; j < 1; j++) {
				iry = lm_height - iy[i] - 1;
				pTag = (_RGBQUAD_ *)(lm_LensMap + 4 * (iry * lm_width));
				int nRed = pTag[ix[j]].rgbRed + a[i][j] * 255;
				if(nRed > 255)
					nRed = 255;
				pTag[ix[j]].rgbRed = nRed;
			}
		}
	}
}
/*
void CLensMap::DrawDashPoint1(double x, double y, double alfa)
{
	if(lm_LensMap) {
		_RGBQUAD_ * pTag = NULL;
		int ix[2], iy[2];
		double a[2][2];
		double ka = alfa;

		ix[0] = (int)x;
		ix[1] = (int)x + 1;
		iy[0] = (int)y;
		iy[1] = (int)y + 1;

		a[0][0] = ka * ((1. - (y - iy[0])) * (1. - (x - ix[0])));
		a[0][1] = ka * ((1. - (y - iy[0])) * (1. - (ix[1] - x)));
		a[1][0] = ka * ((1. - (iy[1] - y)) * (1. - (x - ix[0])));
		a[1][1] = ka * ((1. - (iy[1] - y)) * (1. - (ix[1] - x)));

		int i, j, iry = 0;
		for(i = 0; i < 2; i++) {
			for(j = 0; j < 2; j++) {
				iry = lm_height - iy[i] - 1;
				pTag = (_RGBQUAD_ *)(lm_LensMap + 4 * (iry * lm_width));
				int nRed = pTag[ix[j]].rgbRed + a[i][j] * 255;
				if(nRed > 255)
					nRed = 255;
				pTag[ix[j]].rgbRed = nRed;
			}
		}
	}
}
*/
void CLensMap::DrawDashPoint2(double x, double y, double alfa)
{
	if(lm_LensMap) {
		_RGBQUAD_ * pTag = NULL;
		double kx[3], ky[3];
		int ix[3], iy[3];
		double a[3][3];
		double ka = alfa / 2.;

		ix[0] = (int)x - 1;
		ix[1] = (int)x;
		ix[2] = (int)x + 1;
		iy[0] = (int)y - 1;
		iy[1] = (int)y;
		iy[2] = (int)y + 1;

		ky[0] = y - 1.;
		ky[1] = y;
		ky[2] = y + 1.;
		kx[0] = x - 1.;
		kx[1] = x;
		kx[2] = x + 1.;

		a[0][0] = ka * ((1. - (ky[0] - (int)ky[0])) * (1. - (kx[0] - (int)kx[0])));
		a[0][1] = ka * (1. - (ky[0] - (int)ky[0]));
		a[0][2] = ka * ((1. - (ky[0] - (int)ky[0])) * (kx[2] - (int)kx[2]));
		a[1][0] = ka * (1. - (kx[0] - (int)kx[0]));
		a[1][1] = ka * (1.);
		a[1][2] = ka * ((kx[2] - (int)kx[2]));
		a[2][0] = ka * ((ky[2] - (int)ky[2]) * (1. - (kx[0] - (int)kx[0])));
		a[2][1] = ka * ((ky[2] - (int)ky[2]));
		a[2][2] = ka * ((ky[2] - (int)ky[2]) * (kx[2] - (int)kx[2]));

		int i, j, iry = 0;
		for(i = 0; i < 3; i++) {
			for(j = 0; j < 3; j++) {
				iry = lm_height - iy[i] - 1;
				pTag = (_RGBQUAD_ *)(lm_LensMap + 4 * (iry * lm_width));
				int nRed = pTag[ix[j]].rgbRed + a[i][j] * 255;
				if(nRed > 255)
					nRed = 255;
				pTag[ix[j]].rgbRed = nRed;
			}
		}
	}
}
bool CLensMap::Calc_BlurMap()
{
	bool bRes = false;
	if(lm_lensType == LMLT_NONE || lm == NULL)
		return false;
	if(lm_BlurMap == NULL)
		return false;

	const _RGBQUAD_ colorFar = {0x28, 0x34, 0x8E, 0xFF};
	const _RGBQUAD_ colorMid = {0x1D, 0x0D, 0x50, 0xFF};
	const _RGBQUAD_ colorNear = {0x1D, 0x22, 0x1E, 0xFF};

	const int dySmoothFarMid = 91;
	int dySmothMidNear = 74;
	const double kSmoothFarMid = 0.82;
	double kSmothMidNear = 0.42;
	int yZone[4];
	if(lm_yFarMid > lm_yUp) {
		yZone[0] = lm_yFarMid - dySmoothFarMid * kSmoothFarMid;
		yZone[1] = lm_yFarMid + dySmoothFarMid * (1. - kSmoothFarMid);
	}
	else {
		yZone[0] = yZone[1] = lm_yUp;
		dySmothMidNear = 119;
		kSmothMidNear = 0.58;
	}
	yZone[2] = lm_yMidNear - dySmothMidNear * kSmothMidNear;
	yZone[3] = lm_yMidNear + dySmothMidNear * (1. - kSmothMidNear);

	_RGBQUAD_ * pTag = NULL;
	int ix = 0, iy = 0, dx = 0, iry = 0;
	if(lm_yFarMid > lm_yUp) {
		// Far zone
		for(iy = lm_yUp; iy < yZone[0]; iy++) {
			dx = lm[iy].ix_focalcorridor_right - lm[iy].ix_focalcorridor_left + 1;
			iry = lm_height - iy - 1;
			pTag = (_RGBQUAD_ *)(lm_BlurMap + 4 * (iry * lm_width + lm[iy].ix_focalcorridor_left));
			for(ix = 0; ix < dx; ix++) {
				pTag[ix] = colorFar;
			}
		}
		// Far to Mid zone
		for(iy = yZone[0]; iy < yZone[1]; iy++) {
			dx = lm[iy].ix_focalcorridor_right - lm[iy].ix_focalcorridor_left + 1;
			iry = lm_height - iy - 1;
			pTag = (_RGBQUAD_ *)(lm_BlurMap + 4 * (iry * lm_width + lm[iy].ix_focalcorridor_left));
			_RGBQUAD_ pt;
			double ky;
			ky = double(iy - yZone[0]) / double(yZone[1] - yZone[0]);
			pt.rgbRed = colorFar.rgbRed - (colorFar.rgbRed - colorMid.rgbRed) * ky;
			pt.rgbGreen = colorFar.rgbGreen - (colorFar.rgbGreen - colorMid.rgbGreen) * ky;
			pt.rgbBlue = colorFar.rgbBlue - (colorFar.rgbBlue - colorMid.rgbBlue) * ky;
			pt.rgbAlpha = 0xFF;
			for(ix = 0; ix < dx; ix++) {
				pTag[ix] = pt;
			}
		}
	}
	// Mid zone
	for(iy = yZone[1]; iy < yZone[2]; iy++) {
		dx = lm[iy].ix_focalcorridor_right - lm[iy].ix_focalcorridor_left + 1;
		iry = lm_height - iy - 1;
		pTag = (_RGBQUAD_ *)(lm_BlurMap + 4 * (iry * lm_width + lm[iy].ix_focalcorridor_left));
		for(ix = 0; ix < dx; ix++) {
			pTag[ix] = colorMid;
		}
	}
	// Mid to Near zone
	for(iy = yZone[2]; iy < yZone[3]; iy++) {
		dx = lm[iy].ix_focalcorridor_right - lm[iy].ix_focalcorridor_left + 1;
		iry = lm_height - iy - 1;
		pTag = (_RGBQUAD_ *)(lm_BlurMap + 4 * (iry * lm_width + lm[iy].ix_focalcorridor_left));
		_RGBQUAD_ pt;
		double ky;
		ky = double(iy - yZone[2]) / double(yZone[3] - yZone[2]);
		pt.rgbRed = colorMid.rgbRed - (colorMid.rgbRed - colorNear.rgbRed) * ky;
		pt.rgbGreen = colorMid.rgbGreen - (colorMid.rgbGreen - colorNear.rgbGreen) * ky;
		pt.rgbBlue = colorMid.rgbBlue - (colorMid.rgbBlue - colorNear.rgbBlue) * ky;
		pt.rgbAlpha = 0xFF;
		for(ix = 0; ix < dx; ix++) {
			pTag[ix] = pt;
		}
	}
	// Near zone
	for(iy = yZone[3]; iy <= lm_yDown; iy++) {
		dx = lm[iy].ix_focalcorridor_right - lm[iy].ix_focalcorridor_left + 1;
		iry = lm_height - iy - 1;
		pTag = (_RGBQUAD_ *)(lm_BlurMap + 4 * (iry * lm_width + lm[iy].ix_focalcorridor_left));
		for(ix = 0; ix < dx; ix++) {
			pTag[ix] = colorNear;
		}
	}

	bRes = true;

	return bRes;
}
bool CLensMap::Calc_BlurMap2()
{
	bool bRes = false;
	if(lm_lensType == LMLT_NONE || lm == NULL)
		return false;
	if(lm_BlurMap == NULL)
		return false;

	if(lm_lensType != LMLT_BIFOCAL && lm_lensType != LMLT2_BIFOCAL && lm_lensType != LMLT_READING && lm_lensType != LMLT2_READING)
		return false;

	const _RGBQUAD_ colorFar = {0x28, 0x34, 0x8E, 0xFF};
	const _RGBQUAD_ colorNear = {0x28, 0x22, 0x1E, 0xFF};
	const _RGBQUAD_ colorNear2 = {0x1D, 0x22, 0x1E, 0xFF};

	_RGBQUAD_ * pTag = NULL;
	int ix = 0, iy = 0, dx = 0, iry = 0;

	if(lm_lensType == LMLT_BIFOCAL || lm_lensType == LMLT2_BIFOCAL) {
		// Far zone
		/*
		for(iy = 0; iy < lm_height; iy++) {
			dx = lm[iy].ix_contour_right - lm[iy].ix_contour_left + 1;
			iry = lm_height - iy - 1;
			pTag = (_RGBQUAD_ *)(lm_BlurMap + 4 * (iry * lm_width + lm[iy].ix_contour_left));
			for(ix = 0; ix < dx; ix++) {
				pTag[ix] = colorFar;
			}
		}
		*/
		for(iy = 0; iy < lm_height; iy++) {
			dx = lm_width;
			iry = lm_height - iy - 1;
			pTag = (_RGBQUAD_ *)(lm_BlurMap + 4 * (iry * lm_width));
			for(ix = 0; ix < dx; ix++) {
				pTag[ix] = colorFar;
			}
		}
	}
	// Near zone
	for(iy = 0; iy < lm_height; iy++) {
		dx = lm[iy].ix_focalcorridor_right - lm[iy].ix_focalcorridor_left + 1;
		iry = lm_height - iy - 1;
		pTag = (_RGBQUAD_ *)(lm_BlurMap + 4 * (iry * lm_width + lm[iy].ix_focalcorridor_left));
		switch(lm[iy].i_zone) {
			case 3: // Near zone
				//				lm[iy].i_focal = colorNear.rgbGreen;
				for(ix = 0; ix < dx; ix++) {
					switch (lm_lensType) {
						case LMLT_BIFOCAL:
						case LMLT2_BIFOCAL:
							pTag[ix] = colorNear;
							break;
						case LMLT_READING:
						case LMLT2_READING:
						default:
							pTag[ix] = colorNear2;
							break;
					}
				}
				break;
		}
	}

	bRes = true;

	return bRes;
}
bool CLensMap::Calc_BlurMap3()
{
    bool bRes = false;
    if(lm_lensType == LMLT_NONE || lm == NULL)
        return false;
    if(lm_BlurMap == NULL)
        return false;
    
    const _RGBQUAD_ colorLens = {0x1D, 0x5E, 0x2F, 0xFF};
    
    _RGBQUAD_ * pTag = NULL;
    int ix = 0, iy = 0, dx = 0, iry = 0;
    
    if(lm_lensType == LMLT_COMPUTER || lm_lensType == LMLT2_COMPUTER) {
        for(iy = 0; iy < lm_height; iy++) {
            dx = lm_width;
            iry = lm_height - iy - 1;
            pTag = (_RGBQUAD_ *)(lm_BlurMap + 4 * (iry * lm_width));
            for(ix = 0; ix < dx; ix++) {
                pTag[ix] = colorLens;
            }
        }
        
        bRes = true;
    }
    
    return bRes;
}

int CLensMap::Round(double Arg)
{
	return (int)floor(Arg + 0.5);
}

unsigned char * CLensMap::GetLensMap()
{
	if(lm_DataPresent)
		return lm_LensMap;
	return NULL;
}
unsigned char * CLensMap::GetBlurMap()
{
	if(lm_DataPresent)
		return lm_BlurMap;
	return NULL;
}
int CLensMap::GetLensWidth()
{
	if(lm_DataPresent)
		return lm_width;
	return 0;
}
int CLensMap::GetLensHeight()
{
	if(lm_DataPresent)
		return lm_height;
	return 0;
}
