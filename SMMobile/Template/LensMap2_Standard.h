#pragma once

#include "LensMapData.h"

class CLensMap2_Standard
{
public:
	// Const data
	const static LMLensMapData lm[];
	const static int lm_width, lm_height;
	const static int lm_yUp, lm_yDown, lm_yLeft, lm_yRight, lm_yFarMid, lm_yMidNear;
	const static int lm_nBlur;
	const static double lm_kEffectG;
	const static double lm_kEffectB;
};

// Lens map sizes
const int CLensMap2_Standard::lm_width = 737;
const int CLensMap2_Standard::lm_height = 488;
const int CLensMap2_Standard::lm_yUp = 30;
const int CLensMap2_Standard::lm_yDown = 451;
const int CLensMap2_Standard::lm_yLeft = 113;
const int CLensMap2_Standard::lm_yRight = 130;
const int CLensMap2_Standard::lm_yFarMid = 206;
const int CLensMap2_Standard::lm_yMidNear = 319;
const int CLensMap2_Standard::lm_nBlur = 0;
const double CLensMap2_Standard::lm_kEffectG = 0.7;
const double CLensMap2_Standard::lm_kEffectB = 0.6;
const LMLensMapData CLensMap2_Standard::lm[] = 
{
   0,    0,    0, 0, 0,    0,    0,    0,    0.00,
   1,    0,    0, 0, 0,    0,    0,    0,    0.00,
   2,    0,    0, 0, 0,    0,    0,    0,    0.00,
   3,    0,    0, 0, 0,    0,    0,    0,    0.00,
   4,    0,    0, 0, 0,    0,    0,    0,    0.00,
   5,    0,    0, 0, 0,    0,    0,    0,    0.00,
   6,    0,    0, 0, 0,    0,    0,    0,    0.00,
   7,    0,    0, 0, 0,    0,    0,    0,    0.00,
   8,    0,    0, 0, 0,    0,    0,    0,    0.00,
   9,    0,    0, 0, 0,    0,    0,    0,    0.00,
  10,    0,    0, 0, 0,    0,    0,    0,    0.00,
  11,    0,    0, 0, 0,    0,    0,    0,    0.00,
  12,    0,    0, 0, 0,    0,    0,    0,    0.00,
  13,    0,    0, 0, 0,    0,    0,    0,    0.00,
  14,    0,    0, 0, 0,    0,    0,    0,    0.00,
  15,    0,    0, 0, 0,    0,    0,    0,    0.00,
  16,    0,    0, 0, 0,    0,    0,    0,    0.00,
  17,    0,    0, 0, 0,    0,    0,    0,    0.00,
  18,    0,    0, 0, 0,    0,    0,    0,    0.00,
  19,    0,    0, 0, 0,    0,    0,    0,    0.00,
  20,    0,    0, 0, 0,    0,    0,    0,    0.00,
  21,    0,    0, 0, 0,    0,    0,    0,    0.00,
  22,    0,    0, 0, 0,    0,    0,    0,    0.00,
  23,    0,    0, 0, 0,    0,    0,    0,    0.00,
  24,    0,    0, 0, 0,    0,    0,    0,    0.00,
  25,    0,    0, 0, 0,    0,    0,    0,    0.00,
  26,    0,    0, 0, 0,    0,    0,    0,    0.00,
  27,    0,    0, 0, 0,    0,    0,    0,    0.00,
  28,    0,    0, 0, 0,    0,    0,    0,    0.00,
  29,    0,    0, 0, 0,    0,    0,    0,    0.00,
  30,  334,  440, 0, 0,  334,  440,    1,  200.00,
  31,  302,  474, 0, 0,  302,  474,    1,  200.00,
  32,  269,  510, 0, 0,  269,  510,    1,  200.00,
  33,  253,  530, 0, 0,  253,  530,    1,  200.00,
  34,  232,  554, 0, 0,  232,  554,    1,  200.00,
  35,  220,  569, 0, 0,  220,  569,    1,  200.00,
  36,  203,  590, 0, 0,  203,  590,    1,  200.00,
  37,  193,  602, 0, 0,  193,  602,    1,  200.00,
  38,  179,  620, 0, 0,  179,  620,    1,  200.00,
  39,  170,  631, 0, 0,  170,  631,    1,  200.00,
  40,  158,  646, 0, 0,  158,  646,    1,  200.00,
  41,  151,  655, 0, 0,  151,  655,    1,  200.00,
  42,  140,  668, 0, 0,  140,  668,    1,  200.00,
  43,  133,  673, 0, 0,  133,  673,    1,  200.00,
  44,  124,  679, 0, 0,  124,  679,    1,  200.00,
  45,  117,  682, 0, 0,  117,  682,    1,  200.00,
  46,  108,  685, 0, 0,  108,  685,    1,  200.00,
  47,  103,  687, 0, 0,  103,  687,    1,  200.00,
  48,   94,  690, 0, 0,   94,  690,    1,  200.00,
  49,   89,  692, 0, 0,   89,  692,    1,  200.00,
  50,   82,  694, 0, 0,   82,  694,    1,  200.00,
  51,   77,  695, 0, 0,   77,  695,    1,  200.00,
  52,   69,  697, 0, 0,   69,  697,    1,  200.00,
  53,   65,  698, 0, 0,   65,  698,    1,  200.00,
  54,   61,  699, 0, 0,   61,  699,    1,  200.00,
  55,   57,  701, 0, 0,   57,  701,    1,  200.00,
  56,   54,  702, 0, 0,   54,  702,    1,  200.00,
  57,   51,  703, 0, 0,   51,  703,    1,  200.00,
  58,   49,  704, 0, 0,   49,  704,    1,  200.00,
  59,   46,  705, 0, 0,   46,  705,    1,  200.00,
  60,   44,  706, 0, 0,   44,  706,    1,  200.00,
  61,   42,  707, 0, 0,   42,  707,    1,  200.00,
  62,   40,  708, 0, 0,   40,  708,    1,  200.00,
  63,   38,  709, 0, 0,   38,  709,    1,  200.00,
  64,   37,  709, 0, 0,   37,  709,    1,  200.00,
  65,   35,  710, 0, 0,   35,  710,    1,  200.00,
  66,   34,  711, 0, 0,   34,  711,    1,  200.00,
  67,   32,  712, 0, 0,   32,  712,    1,  200.00,
  68,   31,  712, 0, 0,   31,  712,    1,  200.00,
  69,   29,  713, 0, 0,   29,  713,    1,  200.00,
  70,   29,  713, 0, 0,   29,  713,    1,  200.00,
  71,   27,  714, 0, 0,   27,  714,    1,  200.00,
  72,   26,  715, 0, 0,   26,  715,    1,  200.00,
  73,   25,  715, 0, 0,   25,  715,    1,  200.00,
  74,   24,  715, 0, 0,   24,  715,    1,  200.00,
  75,   23,  716, 0, 0,   23,  716,    1,  200.00,
  76,   23,  716, 0, 0,   23,  716,    1,  200.00,
  77,   22,  717, 0, 0,   22,  717,    1,  200.00,
  78,   21,  717, 0, 0,   21,  717,    1,  200.00,
  79,   20,  717, 0, 0,   20,  717,    1,  200.00,
  80,   19,  717, 0, 0,   19,  717,    1,  200.00,
  81,   19,  718, 0, 0,   19,  716,    1,  200.00,
  82,   18,  718, 0, 0,   18,  714,    1,  200.00,
  83,   17,  718, 0, 0,   18,  712,    1,  200.00,
  84,   16,  719, 0, 0,   20,  710,    1,  200.00,
  85,   16,  719, 0, 0,   22,  708,    1,  200.00,
  86,   15,  719, 0, 0,   24,  706,    1,  200.00,
  87,   15,  719, 0, 0,   26,  704,    1,  200.00,
  88,   14,  720, 0, 0,   28,  702,    1,  200.00,
  89,   14,  720, 0, 0,   30,  700,    1,  200.00,
  90,   13,  720, 0, 0,   32,  698,    1,  200.00,
  91,   12,  720, 0, 0,   35,  695,    1,  200.00,
  92,   12,  720, 0, 0,   37,  693,    1,  200.00,
  93,   12,  720, 0, 0,   39,  691,    1,  200.00,
  94,   11,  720, 0, 0,   42,  688,    1,  200.00,
  95,   11,  720, 0, 0,   44,  686,    1,  200.00,
  96,   10,  720, 0, 0,   46,  684,    1,  200.00,
  97,   10,  721, 0, 0,   49,  681,    1,  200.00,
  98,   10,  721, 0, 0,   51,  679,    1,  200.00,
  99,    9,  721, 0, 0,   54,  676,    1,  200.00,
 100,    9,  721, 0, 0,   56,  674,    1,  200.00,
 101,    9,  721, 0, 0,   58,  672,    1,  200.00,
 102,    8,  721, 0, 0,   60,  670,    1,  200.00,
 103,    8,  721, 0, 0,   61,  669,    1,  200.00,
 104,    8,  722, 0, 0,   63,  667,    1,  200.00,
 105,    8,  722, 0, 0,   66,  664,    1,  200.00,
 106,    8,  722, 0, 0,   68,  662,    1,  200.00,
 107,    7,  722, 0, 0,   70,  660,    1,  200.00,
 108,    7,  722, 0, 0,   72,  658,    1,  200.00,
 109,    7,  722, 0, 0,   74,  656,    1,  200.00,
 110,    7,  722, 0, 0,   76,  654,    1,  200.00,
 111,    7,  722, 0, 0,   78,  652,    1,  200.00,
 112,    7,  722, 0, 0,   80,  650,    1,  200.00,
 113,    6,  722, 0, 0,   82,  648,    1,  200.00,
 114,    6,  722, 0, 0,   84,  646,    1,  200.00,
 115,    6,  722, 0, 0,   87,  643,    1,  200.00,
 116,    6,  722, 0, 0,   88,  642,    1,  200.00,
 117,    6,  723, 0, 0,   90,  640,    1,  200.00,
 118,    6,  723, 0, 0,   91,  639,    1,  200.00,
 119,    6,  723, 0, 0,   94,  636,    1,  200.00,
 120,    6,  723, 0, 0,   96,  634,    1,  200.00,
 121,    6,  723, 0, 0,   98,  632,    1,  200.00,
 122,    6,  723, 0, 0,  100,  630,    1,  200.00,
 123,    6,  723, 0, 0,  102,  628,    1,  200.00,
 124,    6,  723, 0, 0,  104,  626,    1,  200.00,
 125,    6,  723, 0, 0,  106,  624,    1,  200.00,
 126,    6,  723, 0, 0,  108,  622,    1,  200.00,
 127,    6,  723, 0, 0,  111,  619,    1,  200.00,
 128,    6,  723, 0, 0,  112,  618,    1,  200.00,
 129,    6,  723, 0, 0,  114,  616,    1,  200.00,
 130,    6,  724, 0, 0,  116,  614,    1,  200.00,
 131,    6,  724, 0, 0,  118,  612,    1,  200.00,
 132,    6,  724, 0, 0,  121,  609,    1,  200.00,
 133,    6,  724, 0, 0,  122,  608,    1,  200.00,
 134,    7,  724, 0, 0,  125,  605,    1,  200.00,
 135,    7,  724, 0, 0,  126,  604,    1,  200.00,
 136,    7,  724, 0, 0,  128,  602,    1,  200.00,
 137,    7,  724, 0, 0,  129,  601,    1,  200.00,
 138,    7,  724, 0, 0,  132,  598,    1,  200.00,
 139,    7,  724, 0, 0,  133,  597,    1,  200.00,
 140,    7,  724, 0, 0,  136,  594,    1,  200.00,
 141,    7,  724, 0, 0,  137,  593,    1,  200.00,
 142,    8,  724, 0, 0,  139,  591,    1,  200.00,
 143,    8,  724, 0, 0,  140,  590,    1,  200.00,
 144,    8,  724, 0, 0,  142,  588,    1,  200.00,
 145,    8,  724, 0, 0,  144,  586,    1,  200.00,
 146,    8,  724, 0, 0,  146,  584,    1,  200.00,
 147,    8,  724, 0, 0,  148,  582,    1,  200.00,
 148,    8,  724, 0, 0,  150,  580,    1,  200.00,
 149,    8,  724, 0, 0,  151,  579,    1,  200.00,
 150,    8,  724, 0, 0,  152,  578,    1,  200.00,
 151,    8,  724, 0, 0,  154,  576,    1,  200.00,
 152,    9,  724, 0, 0,  156,  574,    1,  200.00,
 153,    9,  724, 0, 0,  158,  572,    1,  200.00,
 154,    9,  724, 0, 0,  160,  570,    1,  200.00,
 155,    9,  724, 0, 0,  161,  569,    1,  200.00,
 156,    9,  724, 0, 0,  163,  567,    1,  200.00,
 157,    9,  724, 0, 0,  164,  566,    1,  200.00,
 158,   10,  724, 0, 0,  167,  563,    1,  200.00,
 159,   10,  724, 0, 0,  168,  562,    1,  200.00,
 160,   10,  724, 0, 0,  169,  561,    1,  200.00,
 161,   10,  724, 0, 0,  171,  559,    1,  200.00,
 162,   10,  724, 0, 0,  173,  557,    1,  200.00,
 163,   10,  724, 0, 0,  174,  556,    1,  200.00,
 164,   10,  724, 0, 0,  176,  554,    1,  200.00,
 165,   10,  724, 0, 0,  178,  552,    1,  200.00,
 166,   10,  724, 0, 0,  179,  551,    1,  200.00,
 167,   11,  724, 0, 0,  181,  549,    1,  200.00,
 168,   11,  724, 0, 0,  182,  548,    1,  200.00,
 169,   11,  724, 0, 0,  184,  546,    1,  200.00,
 170,   11,  724, 0, 0,  185,  545,    1,  200.00,
 171,   11,  724, 0, 0,  187,  543,    1,  200.00,
 172,   12,  724, 0, 0,  188,  542,    1,  200.00,
 173,   12,  724, 0, 0,  190,  540,    1,  200.00,
 174,   12,  724, 0, 0,  191,  539,    1,  200.00,
 175,   12,  724, 0, 0,  193,  537,    1,  200.00,
 176,   12,  724, 0, 0,  195,  535,    1,  200.00,
 177,   12,  724, 0, 0,  197,  533,    1,  200.00,
 178,   12,  724, 0, 0,  198,  532,    1,  200.00,
 179,   12,  724, 0, 0,  199,  531,    1,  200.00,
 180,   13,  724, 0, 0,  201,  529,    1,  200.00,
 181,   13,  724, 0, 0,  202,  528,    1,  200.00,
 182,   13,  724, 0, 0,  204,  526,    1,  200.00,
 183,   13,  724, 0, 0,  205,  525,    1,  200.00,
 184,   14,  724, 0, 0,  206,  524,    1,  200.00,
 185,   14,  724, 0, 0,  207,  523,    1,  200.00,
 186,   14,  724, 0, 0,  209,  521,    1,  200.00,
 187,   14,  724, 0, 0,  210,  520,    1,  200.00,
 188,   14,  724, 0, 0,  212,  518,    1,  200.00,
 189,   15,  724, 0, 0,  213,  517,    1,  200.00,
 190,   15,  724, 0, 0,  214,  516,    1,  200.00,
 191,   15,  724, 0, 0,  215,  515,    1,  200.00,
 192,   15,  724, 0, 0,  216,  514,    1,  200.00,
 193,   15,  724, 0, 0,  218,  512,    1,  200.00,
 194,   15,  724, 0, 0,  220,  510,    1,  200.00,
 195,   15,  724, 0, 0,  221,  509,    1,  200.00,
 196,   16,  724, 0, 0,  222,  508,    1,  200.00,
 197,   16,  724, 0, 0,  223,  507,    1,  200.00,
 198,   16,  724, 0, 0,  224,  506,    1,  200.00,
 199,   16,  724, 0, 0,  226,  504,    1,  200.00,
 200,   16,  724, 0, 0,  227,  503,    1,  200.00,
 201,   17,  724, 0, 0,  228,  502,    1,  200.00,
 202,   17,  724, 0, 0,  229,  501,    1,  200.00,
 203,   17,  723, 0, 0,  230,  500,    1,  200.00,
 204,   17,  723, 0, 0,  232,  498,    1,  200.00,
 205,   17,  723, 0, 0,  233,  497,    1,  200.00,
 206,   17,  723, 0, 0,  234,  496,    2,  199.00,
 207,   18,  723, 0, 0,  235,  495,    2,  198.13,
 208,   18,  723, 0, 0,  236,  494,    2,  197.25,
 209,   18,  723, 0, 0,  238,  492,    2,  196.38,
 210,   18,  723, 0, 0,  238,  492,    2,  195.50,
 211,   19,  723, 0, 0,  239,  491,    2,  194.63,
 212,   19,  723, 0, 0,  240,  490,    2,  193.75,
 213,   19,  723, 0, 0,  241,  489,    2,  192.88,
 214,   19,  723, 0, 0,  243,  487,    2,  192.00,
 215,   19,  723, 0, 0,  244,  486,    2,  191.13,
 216,   19,  722, 0, 0,  244,  486,    2,  190.25,
 217,   20,  722, 0, 0,  245,  485,    2,  189.38,
 218,   20,  722, 0, 0,  246,  484,    2,  188.50,
 219,   20,  722, 0, 0,  248,  482,    2,  187.63,
 220,   20,  722, 0, 0,  249,  481,    2,  186.75,
 221,   21,  722, 0, 0,  250,  480,    2,  185.88,
 222,   21,  722, 0, 0,  250,  480,    2,  185.00,
 223,   21,  722, 0, 0,  251,  479,    2,  184.13,
 224,   21,  722, 0, 0,  252,  478,    2,  183.25,
 225,   21,  722, 0, 0,  253,  477,    2,  182.38,
 226,   22,  722, 0, 0,  254,  476,    2,  181.50,
 227,   22,  722, 0, 0,  255,  475,    2,  180.63,
 228,   22,  722, 0, 0,  255,  475,    2,  179.75,
 229,   22,  722, 0, 0,  256,  474,    2,  178.88,
 230,   23,  722, 0, 0,  257,  473,    2,  178.00,
 231,   23,  722, 0, 0,  258,  472,    2,  177.13,
 232,   23,  721, 0, 0,  259,  471,    2,  176.25,
 233,   23,  721, 0, 0,  260,  470,    2,  175.38,
 234,   23,  721, 0, 0,  261,  469,    2,  174.50,
 235,   23,  721, 0, 0,  262,  468,    2,  173.63,
 236,   24,  721, 0, 0,  262,  468,    2,  172.75,
 237,   24,  721, 0, 0,  263,  467,    2,  171.88,
 238,   24,  721, 0, 0,  263,  467,    2,  171.00,
 239,   25,  721, 0, 0,  264,  466,    2,  170.13,
 240,   25,  721, 0, 0,  265,  465,    2,  169.25,
 241,   25,  720, 0, 0,  265,  465,    2,  168.38,
 242,   25,  720, 0, 0,  266,  464,    2,  167.50,
 243,   25,  720, 0, 0,  266,  464,    2,  166.63,
 244,   26,  720, 0, 0,  267,  463,    2,  165.75,
 245,   26,  720, 0, 0,  267,  463,    2,  164.88,
 246,   26,  720, 0, 0,  268,  462,    2,  164.00,
 247,   27,  720, 0, 0,  269,  461,    2,  163.13,
 248,   27,  720, 0, 0,  269,  461,    2,  162.25,
 249,   27,  720, 0, 0,  270,  460,    2,  161.38,
 250,   27,  720, 0, 0,  270,  460,    2,  160.50,
 251,   27,  720, 0, 0,  271,  459,    2,  159.63,
 252,   28,  720, 0, 0,  271,  459,    2,  158.75,
 253,   28,  719, 0, 0,  272,  458,    2,  157.88,
 254,   28,  719, 0, 0,  272,  458,    2,  157.00,
 255,   28,  719, 0, 0,  273,  457,    2,  156.13,
 256,   29,  719, 0, 0,  273,  457,    2,  155.25,
 257,   29,  719, 0, 0,  273,  457,    2,  154.38,
 258,   29,  719, 0, 0,  274,  456,    2,  153.50,
 259,   29,  719, 0, 0,  274,  456,    2,  152.63,
 260,   30,  718, 0, 0,  274,  456,    2,  151.75,
 261,   30,  718, 0, 0,  274,  456,    2,  150.88,
 262,   30,  718, 0, 0,  275,  455,    2,  150.00,
 263,   31,  718, 0, 0,  275,  455,    2,  149.13,
 264,   31,  718, 0, 0,  275,  455,    2,  148.25,
 265,   31,  718, 0, 0,  276,  454,    2,  147.38,
 266,   31,  718, 0, 0,  276,  454,    2,  146.50,
 267,   31,  717, 0, 0,  276,  454,    2,  145.63,
 268,   32,  717, 0, 0,  277,  453,    2,  144.75,
 269,   32,  717, 0, 0,  277,  453,    2,  143.88,
 270,   33,  717, 0, 0,  277,  453,    2,  143.00,
 271,   33,  717, 0, 0,  277,  453,    2,  142.13,
 272,   33,  717, 0, 0,  278,  452,    2,  141.25,
 273,   33,  717, 0, 0,  278,  452,    2,  140.38,
 274,   33,  717, 0, 0,  278,  452,    2,  139.50,
 275,   34,  717, 0, 0,  278,  452,    2,  138.63,
 276,   34,  717, 0, 0,  279,  451,    2,  137.75,
 277,   34,  716, 0, 0,  279,  451,    2,  136.88,
 278,   35,  716, 0, 0,  279,  451,    2,  136.00,
 279,   35,  716, 0, 0,  280,  450,    2,  135.13,
 280,   36,  716, 0, 0,  280,  450,    2,  134.25,
 281,   36,  716, 0, 0,  280,  450,    2,  133.38,
 282,   36,  715, 0, 0,  281,  449,    2,  132.50,
 283,   36,  715, 0, 0,  281,  449,    2,  131.63,
 284,   37,  715, 0, 0,  281,  449,    2,  130.75,
 285,   37,  715, 0, 0,  281,  449,    2,  129.88,
 286,   37,  715, 0, 0,  281,  449,    2,  129.00,
 287,   38,  715, 0, 0,  282,  448,    2,  128.13,
 288,   38,  715, 0, 0,  282,  448,    2,  127.25,
 289,   38,  715, 0, 0,  282,  448,    2,  126.38,
 290,   38,  715, 0, 0,  282,  448,    2,  125.50,
 291,   39,  714, 0, 0,  282,  448,    2,  124.63,
 292,   39,  714, 0, 0,  282,  448,    2,  123.75,
 293,   40,  714, 0, 0,  283,  447,    2,  122.88,
 294,   40,  714, 0, 0,  283,  447,    2,  122.00,
 295,   40,  714, 0, 0,  283,  447,    2,  121.13,
 296,   40,  713, 0, 0,  283,  447,    2,  120.25,
 297,   41,  713, 0, 0,  284,  446,    2,  119.38,
 298,   41,  713, 0, 0,  284,  446,    2,  118.50,
 299,   41,  713, 0, 0,  284,  446,    2,  117.63,
 300,   42,  713, 0, 0,  284,  446,    2,  116.75,
 301,   42,  713, 0, 0,  284,  446,    2,  115.88,
 302,   42,  713, 0, 0,  284,  446,    2,  115.00,
 303,   43,  712, 0, 0,  285,  445,    2,  114.13,
 304,   43,  712, 0, 0,  285,  445,    2,  113.25,
 305,   43,  712, 0, 0,  285,  445,    2,  112.38,
 306,   44,  712, 0, 0,  285,  445,    2,  111.50,
 307,   44,  712, 0, 0,  286,  444,    2,  110.63,
 308,   44,  711, 0, 0,  286,  444,    2,  109.75,
 309,   45,  711, 0, 0,  286,  444,    2,  108.88,
 310,   45,  711, 0, 0,  286,  444,    2,  108.00,
 311,   46,  711, 0, 0,  286,  444,    2,  107.13,
 312,   46,  711, 0, 0,  286,  444,    2,  106.25,
 313,   46,  711, 0, 0,  287,  443,    2,  105.38,
 314,   47,  710, 0, 0,  287,  443,    2,  104.50,
 315,   47,  710, 0, 0,  287,  443,    2,  103.63,
 316,   48,  710, 0, 0,  287,  443,    2,  102.75,
 317,   48,  710, 0, 0,  287,  443,    2,  101.88,
 318,   48,  710, 0, 0,  288,  442,    2,  101.00,
 319,   48,  709, 0, 0,  288,  442,    3,  100.00,
 320,   49,  709, 0, 0,  288,  442,    3,  100.00,
 321,   49,  709, 0, 0,  288,  442,    3,  100.00,
 322,   50,  709, 0, 0,  288,  442,    3,  100.00,
 323,   50,  709, 0, 0,  288,  442,    3,  100.00,
 324,   50,  709, 0, 0,  288,  442,    3,  100.00,
 325,   51,  708, 0, 0,  288,  442,    3,  100.00,
 326,   51,  708, 0, 0,  289,  441,    3,  100.00,
 327,   52,  708, 0, 0,  289,  441,    3,  100.00,
 328,   52,  708, 0, 0,  289,  441,    3,  100.00,
 329,   52,  707, 0, 0,  289,  441,    3,  100.00,
 330,   53,  707, 0, 0,  289,  441,    3,  100.00,
 331,   53,  707, 0, 0,  289,  441,    3,  100.00,
 332,   54,  707, 0, 0,  289,  441,    3,  100.00,
 333,   54,  706, 0, 0,  290,  440,    3,  100.00,
 334,   54,  706, 0, 0,  290,  440,    3,  100.00,
 335,   55,  706, 0, 0,  290,  440,    3,  100.00,
 336,   55,  705, 0, 0,  290,  440,    3,  100.00,
 337,   56,  705, 0, 0,  290,  440,    3,  100.00,
 338,   56,  705, 0, 0,  290,  440,    3,  100.00,
 339,   57,  704, 0, 0,  291,  439,    3,  100.00,
 340,   57,  704, 0, 0,  291,  439,    3,  100.00,
 341,   58,  704, 0, 0,  291,  439,    3,  100.00,
 342,   58,  703, 0, 0,  291,  439,    3,  100.00,
 343,   58,  703, 0, 0,  291,  439,    3,  100.00,
 344,   59,  703, 0, 0,  291,  439,    3,  100.00,
 345,   59,  702, 0, 0,  291,  439,    3,  100.00,
 346,   60,  702, 0, 0,  291,  439,    3,  100.00,
 347,   60,  701, 0, 0,  292,  438,    3,  100.00,
 348,   61,  701, 0, 0,  292,  438,    3,  100.00,
 349,   61,  701, 0, 0,  292,  438,    3,  100.00,
 350,   62,  700, 0, 0,  292,  438,    3,  100.00,
 351,   62,  700, 0, 0,  292,  438,    3,  100.00,
 352,   63,  699, 0, 0,  292,  438,    3,  100.00,
 353,   63,  699, 0, 0,  292,  438,    3,  100.00,
 354,   64,  699, 0, 0,  292,  438,    3,  100.00,
 355,   64,  698, 0, 0,  292,  438,    3,  100.00,
 356,   65,  697, 0, 0,  292,  438,    3,  100.00,
 357,   65,  697, 0, 0,  292,  438,    3,  100.00,
 358,   66,  696, 0, 0,  292,  438,    3,  100.00,
 359,   66,  696, 0, 0,  292,  438,    3,  100.00,
 360,   67,  695, 0, 0,  292,  438,    3,  100.00,
 361,   67,  695, 0, 0,  292,  438,    3,  100.00,
 362,   68,  694, 0, 0,  292,  438,    3,  100.00,
 363,   68,  694, 0, 0,  292,  438,    3,  100.00,
 364,   69,  693, 0, 0,  292,  438,    3,  100.00,
 365,   69,  692, 0, 0,  292,  438,    3,  100.00,
 366,   69,  692, 0, 0,  293,  437,    3,  100.00,
 367,   70,  691, 0, 0,  293,  437,    3,  100.00,
 368,   71,  690, 0, 0,  293,  437,    3,  100.00,
 369,   71,  690, 0, 0,  293,  437,    3,  100.00,
 370,   72,  689, 0, 0,  293,  437,    3,  100.00,
 371,   73,  688, 0, 0,  293,  437,    3,  100.00,
 372,   73,  688, 0, 0,  293,  437,    3,  100.00,
 373,   74,  687, 0, 0,  293,  437,    3,  100.00,
 374,   74,  686, 0, 0,  293,  437,    3,  100.00,
 375,   75,  685, 0, 0,  293,  437,    3,  100.00,
 376,   75,  685, 0, 0,  293,  437,    3,  100.00,
 377,   76,  684, 0, 0,  293,  437,    3,  100.00,
 378,   77,  683, 0, 0,  293,  437,    3,  100.00,
 379,   77,  682, 0, 0,  293,  437,    3,  100.00,
 380,   78,  681, 0, 0,  293,  437,    3,  100.00,
 381,   78,  680, 0, 0,  293,  437,    3,  100.00,
 382,   79,  680, 0, 0,  293,  437,    3,  100.00,
 383,   80,  678, 0, 0,  293,  437,    3,  100.00,
 384,   80,  678, 0, 0,  293,  437,    3,  100.00,
 385,   81,  676, 0, 0,  293,  437,    3,  100.00,
 386,   82,  675, 0, 0,  293,  437,    3,  100.00,
 387,   82,  675, 0, 0,  293,  437,    3,  100.00,
 388,   83,  673, 0, 0,  293,  437,    3,  100.00,
 389,   84,  672, 0, 0,  293,  437,    3,  100.00,
 390,   84,  671, 0, 0,  293,  437,    3,  100.00,
 391,   85,  670, 0, 0,  293,  437,    3,  100.00,
 392,   86,  668, 0, 0,  293,  437,    3,  100.00,
 393,   86,  667, 0, 0,  293,  437,    3,  100.00,
 394,   87,  665, 0, 0,  293,  437,    3,  100.00,
 395,   88,  664, 0, 0,  293,  437,    3,  100.00,
 396,   89,  663, 0, 0,  292,  438,    3,  100.00,
 397,   89,  661, 0, 0,  292,  438,    3,  100.00,
 398,   90,  659, 0, 0,  292,  438,    3,  100.00,
 399,   91,  657, 0, 0,  292,  438,    3,  100.00,
 400,   92,  655, 0, 0,  292,  438,    3,  100.00,
 401,   92,  653, 0, 0,  292,  438,    3,  100.00,
 402,   93,  650, 0, 0,  292,  438,    3,  100.00,
 403,   94,  649, 0, 0,  292,  438,    3,  100.00,
 404,   94,  646, 0, 0,  292,  438,    3,  100.00,
 405,   95,  644, 0, 0,  292,  438,    3,  100.00,
 406,   96,  641, 0, 0,  292,  438,    3,  100.00,
 407,   97,  639, 0, 0,  292,  438,    3,  100.00,
 408,   99,  636, 0, 0,  292,  438,    3,  100.00,
 409,  100,  634, 0, 0,  292,  438,    3,  100.00,
 410,  101,  632, 0, 0,  292,  438,    3,  100.00,
 411,  103,  628, 0, 0,  292,  438,    3,  100.00,
 412,  104,  626, 0, 0,  292,  438,    3,  100.00,
 413,  105,  623, 0, 0,  292,  438,    3,  100.00,
 414,  107,  620, 0, 0,  291,  439,    3,  100.00,
 415,  109,  617, 0, 0,  291,  439,    3,  100.00,
 416,  110,  614, 0, 0,  291,  439,    3,  100.00,
 417,  112,  610, 0, 0,  291,  439,    3,  100.00,
 418,  113,  607, 0, 0,  291,  439,    3,  100.00,
 419,  116,  603, 0, 0,  291,  439,    3,  100.00,
 420,  118,  600, 0, 0,  290,  440,    3,  100.00,
 421,  120,  596, 0, 0,  290,  440,    3,  100.00,
 422,  122,  593, 0, 0,  290,  440,    3,  100.00,
 423,  124,  588, 0, 0,  290,  440,    3,  100.00,
 424,  126,  585, 0, 0,  290,  440,    3,  100.00,
 425,  129,  580, 0, 0,  290,  440,    3,  100.00,
 426,  131,  576, 0, 0,  290,  440,    3,  100.00,
 427,  134,  571, 0, 0,  290,  440,    3,  100.00,
 428,  137,  567, 0, 0,  290,  440,    3,  100.00,
 429,  141,  562, 0, 0,  290,  440,    3,  100.00,
 430,  143,  558, 0, 0,  290,  440,    3,  100.00,
 431,  145,  554, 0, 0,  290,  440,    3,  100.00,
 432,  149,  547, 0, 0,  289,  441,    3,  100.00,
 433,  152,  543, 0, 0,  289,  441,    3,  100.00,
 434,  157,  537, 0, 0,  289,  441,    3,  100.00,
 435,  160,  532, 0, 0,  289,  441,    3,  100.00,
 436,  165,  525, 0, 0,  289,  441,    3,  100.00,
 437,  168,  520, 0, 0,  289,  441,    3,  100.00,
 438,  174,  512, 0, 0,  289,  441,    3,  100.00,
 439,  178,  507, 0, 0,  288,  442,    3,  100.00,
 440,  184,  499, 0, 0,  288,  442,    3,  100.00,
 441,  189,  493, 0, 0,  288,  442,    3,  100.00,
 442,  196,  484, 0, 0,  288,  442,    3,  100.00,
 443,  201,  477, 0, 0,  288,  442,    3,  100.00,
 444,  209,  467, 0, 0,  287,  443,    3,  100.00,
 445,  216,  459, 0, 0,  287,  443,    3,  100.00,
 446,  226,  448, 0, 0,  287,  443,    3,  100.00,
 447,  234,  439, 0, 0,  287,  439,    3,  100.00,
 448,  248,  425, 0, 0,  287,  425,    3,  100.00,
 449,  258,  414, 0, 0,  286,  414,    3,  100.00,
 450,  281,  390, 0, 0,  286,  390,    3,  100.00,
 451,  306,  361, 0, 0,  306,  361,    3,  100.00,
 452,    0,    0, 0, 0,    0,    0,    0,    0.00,
 453,    0,    0, 0, 0,    0,    0,    0,    0.00,
 454,    0,    0, 0, 0,    0,    0,    0,    0.00,
 455,    0,    0, 0, 0,    0,    0,    0,    0.00,
 456,    0,    0, 0, 0,    0,    0,    0,    0.00,
 457,    0,    0, 0, 0,    0,    0,    0,    0.00,
 458,    0,    0, 0, 0,    0,    0,    0,    0.00,
 459,    0,    0, 0, 0,    0,    0,    0,    0.00,
 460,    0,    0, 0, 0,    0,    0,    0,    0.00,
 461,    0,    0, 0, 0,    0,    0,    0,    0.00,
 462,    0,    0, 0, 0,    0,    0,    0,    0.00,
 463,    0,    0, 0, 0,    0,    0,    0,    0.00,
 464,    0,    0, 0, 0,    0,    0,    0,    0.00,
 465,    0,    0, 0, 0,    0,    0,    0,    0.00,
 466,    0,    0, 0, 0,    0,    0,    0,    0.00,
 467,    0,    0, 0, 0,    0,    0,    0,    0.00,
 468,    0,    0, 0, 0,    0,    0,    0,    0.00,
 469,    0,    0, 0, 0,    0,    0,    0,    0.00,
 470,    0,    0, 0, 0,    0,    0,    0,    0.00,
 471,    0,    0, 0, 0,    0,    0,    0,    0.00,
 472,    0,    0, 0, 0,    0,    0,    0,    0.00,
 473,    0,    0, 0, 0,    0,    0,    0,    0.00,
 474,    0,    0, 0, 0,    0,    0,    0,    0.00,
 475,    0,    0, 0, 0,    0,    0,    0,    0.00,
 476,    0,    0, 0, 0,    0,    0,    0,    0.00,
 477,    0,    0, 0, 0,    0,    0,    0,    0.00,
 478,    0,    0, 0, 0,    0,    0,    0,    0.00,
 479,    0,    0, 0, 0,    0,    0,    0,    0.00,
 480,    0,    0, 0, 0,    0,    0,    0,    0.00,
 481,    0,    0, 0, 0,    0,    0,    0,    0.00,
 482,    0,    0, 0, 0,    0,    0,    0,    0.00,
 483,    0,    0, 0, 0,    0,    0,    0,    0.00,
 484,    0,    0, 0, 0,    0,    0,    0,    0.00,
 485,    0,    0, 0, 0,    0,    0,    0,    0.00,
 486,    0,    0, 0, 0,    0,    0,    0,    0.00,
 487,    0,    0, 0, 0,    0,    0,    0,    0.00,
};
