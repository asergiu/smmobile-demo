#pragma once

struct LMLensMapData
{
	int iy, ix_contour_left, ix_contour_right, ix_focalcorridor_left, ix_focalcorridor_right, ix_20_focalcorridor_left, ix_20_focalcorridor_right, i_zone;
	double i_focal;
};
struct LMFocalCorridor
{
	int ix_left, ix_right;
};

enum LMLensType
{
	LMLT_NONE,
	LMLT_STANDARD,
	LMLT2_STANDARD,
	LMLT_PREMIUM,
	LMLT2_PREMIUM,
	LMLT_ELITE,
	LMLT2_ELITE,
	LMLT_TAILORMADE,
	LMLT2_TAILORMADE,
	LMLT_BIFOCAL,
	LMLT2_BIFOCAL,
	LMLT_READING,
	LMLT2_READING,
	LMLT_COMPUTER,
	LMLT2_COMPUTER,
};

#define WIDTHBYTES(i) ((unsigned)((i+31)&(~31))/8) /* ULONG aligned ! */

typedef struct tag_RGBQUAD_ {
	unsigned char    rgbBlue;
	unsigned char    rgbGreen;
	unsigned char    rgbRed;
	unsigned char    rgbAlpha;
} _RGBQUAD_;
