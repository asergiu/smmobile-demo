//
//  CAShapeLayer+Effects.m
//  SMMobile
//
//  Created by Sergej Bogatenko on 12/19/18.
//  Copyright © 2018 ACEP. All rights reserved.
//

#import "CAShapeLayer+Effects.h"

@implementation CAShapeLayer (Effects)

- (CAShapeLayer *)getShapeLayerWithFrame:(CGRect)frame
                               holeFrame:(CGRect)holeFrame
{
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:frame
                                                    cornerRadius:0];
    
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithRoundedRect:holeFrame
                                                          cornerRadius:holeFrame.size.height / 2];
    
    [path appendPath:circlePath];
    [path setUsesEvenOddFillRule:YES];
    
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = path.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [UIColor whiteColor].CGColor;
    fillLayer.opacity = 1.;
    
    return fillLayer;
}

@end
