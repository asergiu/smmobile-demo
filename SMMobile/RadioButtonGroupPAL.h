//
//  RadioButtonGroup.h
//  thickness
//
//  Created by Vladimir Malashenkov on 05.05.12.
//

#import <UIKit/UIKit.h>

@class RadioButtonGroupPAL;

@protocol RadioButtonListPalDelegate <NSObject>

@optional
- (void)didSelectRadiobutton:(RadioButtonGroupPAL *)radioButtonGroup ItemAtIndex:(int)index;
- (void)clearSelection; 
@end

@interface RadioButtonGroupPAL : UIView
{
    NSMutableArray *allLabels;
    NSMutableArray *buttonList;
}

@property (nonatomic, retain) NSString *groupTitle;
@property (nonatomic, assign) int selectedItem;
@property (nonatomic, weak) id <RadioButtonListPalDelegate> delegate;


- (NSArray *)setLabelsForRadioButtons: (NSString *)labels,...;
- (void)setSelectedItemIndex:(NSInteger)index;
- (void)disableItemAtIndex:(NSInteger)index;
- (void)enableItemAtIndex:(NSInteger)index;
- (void)enableAllItems;
- (void)checkboxButton:(UIButton *)button;
- (void)setStatusForAllItemsInRadioGroupEnabled:(BOOL)enabled;

@end
