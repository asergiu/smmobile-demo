#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface SMMoviePLayerHelper : NSObject

@property (nonatomic, strong) MPMoviePlayerViewController *mpMovieController;
@property (nonatomic, strong) MPMoviePlayerController *moviePlayer;

- (instancetype)init;
- (void)setUpMovieControllerWith: (NSDictionary *)params;
- (void)setParamsForPLayer:(NSDictionary *)params;
- (void)playVideo;
- (void)endSession;

@end
