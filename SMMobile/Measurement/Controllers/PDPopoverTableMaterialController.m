//
//  PDPopoverTableMaterialController.m
//  Optovision
//
//  Created by Oleg Bogatenko on 18.06.13.
//
//

#import "PDPopoverTableMaterialController.h"
#import "PDMResultsController.h"

#define sysMaterial   [[NSUserDefaults standardUserDefaults] stringForKey:@"frame_type"]
#define usrMaterial   [[NSUserDefaults standardUserDefaults] stringForKey:@"user_frame_type"]

@implementation PDPopoverTableMaterialController

@synthesize popoverMaterials;
@synthesize selectedMaterial;
@synthesize parentController;

- (id)initWithParent: (UIViewController *)parent andStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.popoverMaterials = @[NSLocalizedString(@"Metal", @""), NSLocalizedString(@"Acetate", @""), NSLocalizedString(@"Nylor", @"")];
        self.preferredContentSize = CGSizeMake(215, self.popoverMaterials.count * 44 - 1);
        self.parentController = (PDMResultsController *)parent;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.scrollEnabled = NO;
    
    selectedMaterial = usrMaterial ? [usrMaterial intValue] : [sysMaterial intValue];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.popoverMaterials.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = selectedMaterial == indexPath.row ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    }
    cell.textLabel.text = [self.popoverMaterials objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"user_frame_type"];
    [userDefaults removeObjectForKey:@"user_bevel"];
    
    [userDefaults setObject:[NSString stringWithFormat:@"%d", indexPath.row] forKey:@"user_frame_type"];
    [userDefaults synchronize];
    
    [parentController setMaterialLabel:true];
    [self.parentController.popoverController dismissPopoverAnimated:YES];
    self.parentController.popoverController = nil;
    
}

@end
