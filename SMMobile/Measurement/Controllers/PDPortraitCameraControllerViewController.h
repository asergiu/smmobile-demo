//
//  UIPortraitCameraControllerViewController.h
//  PD-Measurement
//
//  Created by Pavel Stoma on 6/8/12.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMotion/CoreMotion.h>
#import <ImageIO/ImageIO.h>

#import "AVCamCaptureManager.h"
#import "AVCamRecorder.h"
#import "PDMCameraController.h"
#import "PDMTouchView.h"

@interface PDPortraitCameraControllerViewController : PDMCameraController <AVCamCaptureManagerDelegate> {
    IBOutlet UILabel *m_pleaseLookLabel;
    BOOL transitionFromSessionManager;
}

@property (nonatomic,retain) AVCamCaptureManager *captureManager;
@property (nonatomic, strong) PDMCameraController *parentController;
@property (strong, nonatomic) CMMotionManager *motionManager;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;

@property (nonatomic, assign) BOOL needToDismiss;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *snapButtons;

@property (weak, nonatomic) IBOutlet PDMTouchView *touchView;

@property (weak, nonatomic) IBOutlet UIView *roundView;
@property (strong, nonatomic) IBOutlet UIView *cameraPreview;
@property (strong, nonatomic) IBOutlet UIView *delayView;

@property (weak, nonatomic) IBOutlet UILabel *helperLabel;
@property (weak, nonatomic) IBOutlet UILabel *touchLabel;
@property (strong, nonatomic) IBOutlet UILabel *DALabel;
@property (strong, nonatomic) IBOutlet UILabel *delayLabel;
@property (strong, nonatomic) IBOutlet UILabel *warningLabel;

@property (weak, nonatomic) IBOutlet UIImageView *testImageView;
@property (weak, nonatomic) IBOutlet UIImageView *testCropImage;
@property (strong, nonatomic) IBOutlet UIImageView *leftArrowImageView;
@property (strong, nonatomic) IBOutlet UIImageView *rightArrowImageView;

@property (weak, nonatomic) IBOutlet UINavigationItem *navItem;

@property (weak, nonatomic) IBOutlet UITextField *DATextField;
@property (weak, nonatomic) IBOutlet UITextField *DAzTextField;

@property (weak, nonatomic) IBOutlet UIButton *btnHelp_Click;

@property (nonatomic, assign) bool bNearDeviceAnglePresent;
@property (nonatomic, assign) double NearPhotoDeviceAngle;

- (void)configureStaticImageView;
- (void)trackingDeviceAngle:(double)angle;

- (IBAction)snapPressed:(id)sender;
- (IBAction)changePdMode:(UISegmentedControl *)sender;
- (IBAction)changeBrightness:(id)sender;
- (IBAction)btnHelp_Click:(id)sender;
- (IBAction)homeButtonPressedLandscape:(id)sender;

@end
