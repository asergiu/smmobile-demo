//
//  PDPopoverTableBevelController.m
//  Optovision
//
//  Created by Oleg Bogatenko on 18.06.13.
//
//

#import "PDPopoverTableBevelController.h"
#import "PDMResultsController.h"

#define sysMaterial   [[NSUserDefaults standardUserDefaults] stringForKey:@"frame_type"]
#define usrMaterial   [[NSUserDefaults standardUserDefaults] stringForKey:@"user_frame_type"]
#define usrBevel      [[NSUserDefaults standardUserDefaults] stringForKey:@"user_bevel"]
#define metalBevel    [[NSUserDefaults standardUserDefaults] stringForKey:@"m_bvl"]
#define acetateBevel  [[NSUserDefaults standardUserDefaults] stringForKey:@"a_bvl"]
#define nylorBevel    [[NSUserDefaults standardUserDefaults] stringForKey:@"n_bvl"]

@implementation PDPopoverTableBevelController

@synthesize bevelArray;
@synthesize selectedBevel;
@synthesize selectedMaterial;
@synthesize parentController;

- (id)initWithParent: (UIViewController *)parent withArray:(NSArray *)array andStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.bevelArray = [NSArray arrayWithArray:array];
        self.preferredContentSize = CGSizeMake(215, self.bevelArray.count * 44 - 1);
        self.contentSizeForViewInPopover = CGSizeMake(215, self.bevelArray.count * 44 - 1);
        self.parentController = (PDMResultsController *)parent;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.scrollEnabled = NO;
    selectedMaterial = usrMaterial ? [usrMaterial intValue] : [sysMaterial intValue];
    
    float bevelValue = 0.0;
    
    switch (selectedMaterial) {
        case 0:
            bevelValue = [metalBevel floatValue];
            break;
        case 1:
            bevelValue = [acetateBevel floatValue];
            break;
        case 2:
            bevelValue = [nylorBevel floatValue];
            break;
        default:
            break;
    }
    
    selectedBevel = usrBevel ? [usrBevel floatValue] : bevelValue;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.bevelArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = selectedBevel == [[self.bevelArray objectAtIndex:indexPath.row] floatValue] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%.1f", [[self.bevelArray objectAtIndex:indexPath.row] floatValue]];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"user_bevel"];
    
    [userDefaults setObject:[NSString stringWithFormat:@"%.1f", [[self.bevelArray objectAtIndex:indexPath.row] floatValue]] forKey:@"user_bevel"];
    [userDefaults synchronize];
    
    [parentController setBevelLabel:true];
    [self.parentController.popoverController dismissPopoverAnimated:YES];
    self.parentController.popoverController = nil;
    
}

@end
