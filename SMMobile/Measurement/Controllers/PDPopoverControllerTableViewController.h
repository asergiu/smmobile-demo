//
//  PDPopoverControllerTableViewController.h
//  PD-Measurement
//
//  Created by Pavel Stoma on 6/26/12.
//

#import <UIKit/UIKit.h>

@interface PDMProgressiveType : NSObject

@property (nonatomic, strong) NSArray *popoverValues;
@property (nonatomic, assign) int shiftValue;

@end

@interface PDPopoverControllerTableViewController : UITableViewController

- (id)initWithParent: (UIViewController *)parent withProgressiveTypeData:(PDMProgressiveType *)pr_type;

@end
