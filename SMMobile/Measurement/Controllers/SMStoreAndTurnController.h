//
//  SMStoreAndTurnController.h
//  SMMobile
//
//  Created by Work Inteleks on 5/24/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SMStoreAndTurnControllerDelegate <NSObject>

@optional
- (void)closeTableAlert;
- (void)selectCustomer;
@end

@interface SMStoreAndTurnController : UIViewController

@property (nonatomic, assign) id <SMStoreAndTurnControllerDelegate> delegate;

@end
