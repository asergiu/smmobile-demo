//
//  PDSessionFullScreenViewController.m
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 12.05.13.
//
//

#import "PDSessionFullScreenViewController.h"
#import "SessionManager.h"
#import "PDSessionEditViewController.h"

@interface PDSessionFullScreenViewController ()
{
    IBOutlet UIButton *editButton;
    IBOutlet UIButton *loadButton;
    IBOutlet UIButton *deleteButton;
}

- (IBAction)backPressed:(id)sender;
- (IBAction)didSelectedLoadButton:(id)sender;
- (IBAction)didSelectedDeleteButton:(id)sender;
- (IBAction)didSelectedEditButton:(id)sender;

@end

@implementation PDSessionFullScreenViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (id)initWithSession:(Session *)session
{
    self = [super initWithNibName:@"PDSessionFullScreenViewController" bundle:[NSBundle mainBundle]];
    if (self)
    {
        _session = session;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setPhoto];
    
    [editButton setTitle:NSLocalizedString(@"Edit", nil) forState:UIControlStateNormal];
    [editButton setTitle:NSLocalizedString(@"Edit", nil) forState:UIControlStateHighlighted];
    [editButton setTitle:NSLocalizedString(@"Edit", nil) forState:UIControlStateSelected];
    
    [loadButton setTitle:NSLocalizedString(@"Load", nil) forState:UIControlStateNormal];
    [loadButton setTitle:NSLocalizedString(@"Load", nil) forState:UIControlStateHighlighted];
    [loadButton setTitle:NSLocalizedString(@"Load", nil) forState:UIControlStateSelected];
    
    [deleteButton setTitle:NSLocalizedString(@"Delete", nil) forState:UIControlStateNormal];
    [deleteButton setTitle:NSLocalizedString(@"Delete", nil) forState:UIControlStateHighlighted];
    [deleteButton setTitle:NSLocalizedString(@"Delete", nil) forState:UIControlStateSelected];
}

- (void)setPhoto
{
    NSString *photoKey = _session.screenShot.photoID;
    UIImage *sessionPhoto = [SESSION_MANAGER photoForPhotoId:photoKey source:SourceTypeLocal];
    
    self.image.image = sessionPhoto;
}

- (void)backPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didSelectedLoadButton:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(didSelectedLoadButton)])
    {
        [_delegate didSelectedLoadButton];
    }
}

- (void)didSelectedDeleteButton:(id)sender
{    
    if (![SESSION_MANAGER localTargetFromSettings])
    {
        [PD_API_MANAGER requestExistsStoreCode:[SESSION_MANAGER currentStoreCode]
                                withCompletion:^(BOOL success, NSString *message, NSDictionary *response){
                                    
                                    if (success)
                                    {
                                        [self deleteThenExit];
                                    }
                                    else
                                    {
                                        [SESSION_MANAGER showIncorrectStoreCodeError];
                                    }
                                }];
    }
    else
    {
        [self deleteThenExit];
    }
}

- (void)didSelectedDeleteALLButton:(id)sender
{

}

- (void)didSelectedEditButton:(id)sender
{
    PDSessionEditViewController * editViewController = [[PDSessionEditViewController alloc] initWithSession:[self currentSelectedFilledSession]];
        
    self.currentEditViewController = editViewController;
    
    [editViewController.view setFrame: self.view.bounds];
    
    [UIView transitionWithView:self.view duration:0.5f
                       options:UIViewAnimationOptionTransitionFlipFromLeft //change to whatever animation you like
                    animations:^ { [self.view addSubview:editViewController.view]; }
                    completion:nil];
}

- (void)deleteThenExit
{
    if (_delegate && [_delegate respondsToSelector:@selector(didSelectedDeleteButton)])
    {
        [_delegate didSelectedDeleteButton];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}

- (Session *)currentSelectedFilledSession
{
    [_session fillAllData];
    
    return _session;
}

#pragma mark - Autorotate Methods

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

@end
