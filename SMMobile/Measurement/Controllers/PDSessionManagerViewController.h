//
//  PDSessionManagerViewController.h
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 11.03.13.
//
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"
#import "SessionThumbsView.h"
#import "PDMCameraController.h"
#import "PDSessionSelectPopoverViewController.h"

typedef enum _PDFilterMode {
    PDFilterModeDate  = 0,
    PDFilterModeName = 1
} PDFilterMode;

@protocol PDSessionManagerViewControllerDelegate <NSObject>

@optional
- (void)loadSessionDidSelected:(Session *)session;
- (void)managerWillDismiss;
@end

@interface PDSessionManagerViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SessionThumbsViewDataSource, SessionThumbsViewDelegate, PDSessionSelectPopoverViewControllerDelegate>
{
    NSDictionary *dataDict;
    
    NSArray *sectionArray;
    
    IBOutlet UIView *sessionBottomView;
    IBOutlet UIView *progressBottomView;
    
    IBOutlet UITableView *table;
    
    IBOutlet UILabel *sizeLabel;
    IBOutlet UILabel *progressLabel;
    
    IBOutlet SessionThumbsView *thumbsView;
    
    IBOutlet UIProgressView *syncProgressView;
    
    IBOutlet UIButton *updateButton;
    IBOutlet UIButton *cancelButton;
    
    IBOutlet UISegmentedControl *filtersControl;
    
    int selectedIndex;
    int selectedSection;
    int selectedElementIndex;
}

@property (nonatomic, assign) BOOL farMode;
@property (nonatomic, strong) IBOutlet UIButton *goBack;
@property (nonatomic, assign) id <PDSessionManagerViewControllerDelegate> delegate;
@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, assign) UIViewController *parentView;
@property (nonatomic, strong) UIViewController *currentEditViewController;
@property (nonatomic, assign) PDFilterMode currentFilterMode;

- (IBAction)buttonPressed:(UIButton *)sender;
- (IBAction)exitSessionManager:(id)sender;
- (IBAction)btnHelp_Click:(id)sender;

@end
