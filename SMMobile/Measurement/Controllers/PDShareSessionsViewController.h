//
//  PDShareSessionsViewController.h
//  SMMobile
//
//  Created by Sergej Bogatenko on 1/15/20.
//  Copyright © 2020 ACEP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDShareSessionsViewController : UIViewController

- (void)setAppName:(NSString *)name;

@end

