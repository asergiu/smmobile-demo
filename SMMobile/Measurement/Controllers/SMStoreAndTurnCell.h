//
//  SMStoreAndTurnCell.h
//  SMMobile
//
//  Created by Work Inteleks on 5/24/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMStoreAndTurnCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
