//
//  PDWrapController.h
//  Visioner_4.0
//
//  Created by Dgut on 12.12.13.
//
//

#import <UIKit/UIKit.h>

#import "PDWrapView.h"

#import "PDMResultsController.h"

@interface PDWrapController : UIViewController

@property ( nonatomic, strong ) IBOutlet PDWrapView * wrapView;
@property ( nonatomic, strong ) IBOutlet UILabel * counterLabel;

@property ( nonatomic, strong ) IBOutlet UILabel *infoLabel;

@property ( nonatomic, strong ) IBOutletCollection( UIButton ) NSArray * shotButtons;

-( IBAction )makeShot : ( id )sender;

@end
