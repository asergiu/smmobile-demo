
#import <UIKit/UIKit.h>
#import <MediaPlayer/MPMoviePlayerController.h>

typedef NS_ENUM(NSInteger, PDHelpType) {
    PDHelpTypeFarCamera,
    PDHelpTypeFarMarkers,
    PDHelpTypeFarLines,
    PDHelpTypeFarResults,
    PDHelpTypeNearCamera,
    PDHelpTypeNearCameraNew,
    PDHelpTypeNearMarkers,
    PDHelpTypeNearMarkersNew,
    PDHelpTypeNearLines,
    PDHelpTypeNearLinesNew,
    PDHelpTypeNearResults,
    PDHelpTypeNearResultsNew,
    PDHelpTypeSessionsManager,
    PDHelpTypeWrapAngleBefore,
    PDHelpTypeWrapAngleAfter,
    PDHelpTypeMainScreen,
    PDHelpTypeNearResultsExpert,
    PDHelpTypeFarResultsExpert,
    PDHelpTypeNearResultsNewExpert
};

@interface PDHelpViewController : UIViewController

+ (UIViewController *)initWithPage:(PDHelpType)helpType;

- (void)setHelpForType:(PDHelpType)type;

@end
