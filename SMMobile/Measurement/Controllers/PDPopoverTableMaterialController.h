//
//  PDPopoverTableMaterialController.h
//  Optovision
//
//  Created by Oleg Bogatenko on 18.06.13.
//
//

#import <UIKit/UIKit.h>

@class PDMResultsController;

@interface PDPopoverTableMaterialController : UITableViewController

@property (nonatomic, strong) NSArray *popoverMaterials;
@property int selectedMaterial;

@property (strong, nonatomic) PDMResultsController *parentController;

- (id)initWithParent: (UIViewController *)parent andStyle:(UITableViewStyle)style;

@end
