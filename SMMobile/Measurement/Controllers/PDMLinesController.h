
#import <UIKit/UIKit.h>

@class PDMLinesView;

struct FrameData
{
    float rightEyeX;
    float rightEyeY;
    
    float rightTopEyeX;
    float rightTopEyeY;
    
    float leftEyeX;
    float leftEyeY;
    
    float leftTopEyeX;
    float leftTopEyeY;
};

@interface PDMLinesController : UIViewController <UIScrollViewDelegate>
{
    IBOutlet UILabel *m_lensLabel;
    
    FrameData currentFrame;
}

@property ( strong, nonatomic ) IBOutlet PDMLinesView *linesView;

@property (retain, nonatomic) IBOutlet UIButton *resultButton;
@property (strong, nonatomic) UIImage *snapImage;
@property (strong, nonatomic) NSNumber *secondPhotoMode;
@property (strong, nonatomic) NSNumber *farPdMode;
@property (assign, nonatomic) NSNumber *vdMode;

@end
