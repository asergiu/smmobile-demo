#import "PDMResultsController.h"
#import "UIImage+Extensions.h"
#import "PDMDataManager.h"
#import "UIImage+Crop.h"
#import "PDMEyeArea.h"
#import "PDMCameraController.h"
#import "UIView+Point.h"
#import "UIView+Effects.h"
#import "NSString+CWAddition.h"
#import "PDPopoverControllerTableViewController.h"
#import "UINavigationController+Block.h"
#import "PDMCameraController.h"
#import "NetworkManager.h"
#import "SMNetworkManager.h"
#import "PDWrapController.h"
#import "SessionManager.h"
#import "PDHelpViewController.h"
#import "PDDeviceInfo.h"
#import "PDDataSharing.h"
#import "PDUDPSelectPopover.h"
#import "SSNetworkInfo.h"
#import "PDMResults.h"
#import "PDConfigManager.h"
#import "PDPopoverTableMaterialController.h"
#import "PDPopoverTableBevelController.h"
#import "NSDictionary+KeysRotation.h"

#import "SMAppManager.h"
#import "SMStoreClient.h"

#import "SVProgressHUD.h"

#import "SMProvisionAlertViewController.h"

#include <math.h>
#include <arpa/inet.h>
#include <CFNetwork/CFNetwork.h>

#define noHIDE_WRAP_INFORMATION

#define kFTPsendFileName @"ipad-nearvision.xml"

#define kFTPPathKey @"kFTPPathKey"
#define kFTPNameKey @"kFTPNameKey"

#define kUDPUpdatePeriodInSeconds 2

#define Device_MMinOnePixel (197.0 / 1024.0)

#define CONTROL_SIZE 55.0
#define ADDITIONAL_CONTROL_SIZE 30.0
#define ARROW_OFFSET_LENGTH 5.0
#define MAX_ZOOM_SCALE 1.3
#define NEAR_PIXELS_IN_MM (70.0/10.0)
#define NEAR_TOP_PART_DIST_TO_POINT_MM 11.0
#define NEAR_PD_DIST_FROM_POINT_TO_LINE_MM 4.0
#define NEAR_PD_TOP_PART_HEIGHT_MM (117.0 * 10.0 / 70.0)
#define NEAR_PD_BOTTOM_PART_HEIGHT_MM (115.0 * 10.0 / 70.0)
#define NEAR_PD_WIDTH_MM (366.0 * 10.0 / 70.0)
#define NEAR_PD_BOTTOM_DIST_TO_POINT (58.0 * 10 / 70)

#define CROSS_IMAGEVIEW_TAG_RIGHT 23
#define CROSS_IMAGEVIEW_TAG_LEFT 24

#define kNewSessionAlertTag 10
#define PatientAlertTag 12

#define rotation [[NSUserDefaults standardUserDefaults] boolForKey:@"use_rotation"]
#define kHardFrame [[NSUserDefaults standardUserDefaults] boolForKey:@"hard_frame"]
#define SEMWRAP 1. // Standard Error of Measurement

#define usrMaterial   [[NSUserDefaults standardUserDefaults] stringForKey:@"user_frame_type"]
#define sysMaterial   [[NSUserDefaults standardUserDefaults] stringForKey:@"frame_type"]
#define usrBevel      [[NSUserDefaults standardUserDefaults] stringForKey:@"user_bevel"]
#define metalBevel    [[NSUserDefaults standardUserDefaults] stringForKey:@"m_bvl"]
#define acetateBevel  [[NSUserDefaults standardUserDefaults] stringForKey:@"a_bvl"]
#define nylorBevel    [[NSUserDefaults standardUserDefaults] stringForKey:@"n_bvl"]

#define DIAM_PD_WIDTH 448.0
#define DIAM_PD_HEIGHT 448.0

enum {
    kSendBufferSize = 32768
};

typedef enum PDDesignType {
    PDDesignTypeProgressive8 = 8,
    PDDesignTypeProgressive9 = 9,
    PDDesignTypeProgressive10 = 10,
    PDDesignTypeProgressive11 = 11,
    PDDesignTypeProgressive12 = 12,
    PDDesignTypeProgressive13 = 13,
    PDDesignTypeProgressive14 = 14,
    PDDesignTypeProgressive15 = 15,
    PDDesignTypeProgressive16 = 16,
    PDDesignTypeProgressive17 = 17,
    PDDesignTypeProgressive18 = 18
} PDDesignType;

@interface PDMResultsController () <PDUDPSelectPopoverDelegate, SMProvisionAlertViewControllerDelegate>
{
    IBOutlet UIButton *homeBtn;
    IBOutlet UIButton *backBtn;
    IBOutlet UIButton *sessionBtn;
    IBOutlet UIButton *seleckLensBtn;
    IBOutlet UIButton *exportBtn;
    
    IBOutlet UIView *view1;
    IBOutlet UIView *view2;
    IBOutlet UIView *view3;
    IBOutlet UIView *view4;
    IBOutlet UIView *schemeBG;
    IBOutlet UIView *rotationBG;
    
    IBOutlet UILabel *headRotationLabel;
    IBOutlet UIButton *headRotationCorrection;
    IBOutlet UIImageView *headRotationImage;
    
    IBOutlet UILabel *schemeLabel;
    IBOutlet UIButton *schemeBtn;
    IBOutlet UIView *staticView;
    IBOutlet UIView *staticDesignScheme;
    IBOutlet UIView *staticCorridScheme;
    
    IBOutlet UILabel *PDR;
    IBOutlet UILabel *PDL;
    IBOutlet UILabel *FHR;
    IBOutlet UILabel *FHL;
    IBOutlet UILabel *DBL;
    
    BOOL showStaticScheme;
    BOOL showNearPDDesign;
    BOOL showCircDiameters;
    
    IBOutlet UILabel *bifocalLabel;
    IBOutlet UITextField *bifocalRField;
    IBOutlet UITextField *bifocalLField;
    
    IBOutlet UILabel *insertLabel;
    IBOutlet UITextField *insertRField;
    IBOutlet UITextField *insertLField;
    
    IBOutlet UILabel *totalPDLabel;
    IBOutlet UITextField *totalPDField;
    
    // Diameter
    IBOutlet UIButton *diametrBtn;
    IBOutlet UILabel *diametrRLabel;
    IBOutlet UILabel *diametrLLabel;
    IBOutlet UITextField *diametrRField;
    IBOutlet UITextField *diametrLField;
    CGFloat firstDiameter;
    CGPoint leftCenterPoint;
    CGPoint rightCenterPoint;
    int editDiameterState;
    IBOutlet UIImageView *leftDiameterImageView;
    IBOutlet UIImageView *rightDiameterImageView;
    IBOutlet UIView *lensDiameterView;
    
    IBOutlet UIButton *typeBtn;
    IBOutlet UILabel *showTypeLabel;
    
    IBOutlet UIButton *bevelBtn;
    IBOutlet UILabel *showBevelLabel;

    IBOutlet UIView *crossesView;
    
    IBOutlet UILabel *pdNearLabel;
    
    IBOutlet UILabel *hardFrameLabel;
    IBOutlet UISwitch *hardFrameSwitch;
    
    UIPanGestureRecognizer *leftEyeHeightControlPanRecognizer;
    UIPanGestureRecognizer *leftEyeHeightBottomControlPanRecognizer;
    UIPanGestureRecognizer *rightEyeHeightTopControlPanRecognizer;
    UIPanGestureRecognizer *rightEyeHeightBottomControlPanRecognizer;
    UIPanGestureRecognizer *leftPdRightWidthControlPanRecognizer;
    UIPanGestureRecognizer *rightPdLeftWidthControlPanRecognizer;
    UIPanGestureRecognizer *leftBridgeControlPanRecognizer;
    UIPanGestureRecognizer *rightBridgeControlPanRecognizer;
    UIPanGestureRecognizer *lensDiameterControlPanRecognizer;
    
    SMProvisionAlertViewController *provisionController;
    
    UIView *backgroundView;
}

@property (strong, nonatomic) UILabel *bridgeLabel;

@property (strong, nonatomic) PDMEyeArea *eyeArea;

@property (assign, nonatomic) BOOL editModeEnabled;
@property (assign, nonatomic) BOOL initialInitPositionsAndFrames;

@property (strong, nonatomic) NSMutableDictionary *results;

@property (strong, nonatomic) NSString *plistPath;

@property (assign, nonatomic) CGPoint firstPoint;
@property (assign, nonatomic) CGFloat firstWidth;
@property (assign, nonatomic) CGFloat firstHeight;
@property (assign, nonatomic) CGFloat previousScale;
@property (assign, nonatomic) CGFloat currentZoomScale;
@property (assign, nonatomic) CGFloat scrollOffsetX;
@property (assign, nonatomic) CGFloat scrollOffsetY;

@property (strong, nonatomic) PDPopoverControllerTableViewController *popoverContentController;

@property (strong, nonatomic) PDPopoverTableMaterialController *popoverMaterialController;
@property (strong, nonatomic) PDPopoverTableBevelController *popoverBevelController;

@property (strong, nonatomic) PDMProgressiveType *popoverProgressiveType;
@property (strong, nonatomic) PDDeviceInfo * deviceInfo;

- (IBAction)toggleScheme:(UIButton *)sender;

- (IBAction)changeHardFrameOption:(UISwitch *)sender;

- (double)PhotoDistance;
- (double)convertToCM:(double)pixel;

- (void)refresh;
- (void)SaveEditResult;

- (void)configureResultControllerWillAppear;
- (void)configureResultControllerDidLoad;

- (void)addGestures;
- (void)configureControlViewFrames;
- (void)configureLeftEyeDesign:(CGPoint)eyePoint;
- (void)configureRightEyeDesign:(CGPoint)eyePoint;

- (void)showLeftEyeDiameter: (CGPoint)eyePoint withDiameter: (double)diam forProgressive: (bool)bProgressiveStatus;
- (void)showRightEyeDiameter: (CGPoint)eyePoint withDiameter: (double)diam forProgressive: (bool)bProgressiveStatus;
- (void)lensDiameterControlPanGesture: (UIPanGestureRecognizer *)gesture;

- (void)leftEyeHeightTopControlPanGesture:(UIPanGestureRecognizer *)gesture;
- (void)leftEyeHeightBottomControlPanGesture:(UIPanGestureRecognizer *)gesture;
- (void)rightEyeHeightTopControlPanGesture:(UIPanGestureRecognizer *)gesture;
- (void)rightEyeHeightBottomControlPanGesture:(UIPanGestureRecognizer *)gesture;
- (void)leftPdRightWidthControlPanGesture:(UIPanGestureRecognizer *)gesture;
- (void)rightPdLeftWidthControlPanGesture:(UIPanGestureRecognizer *)gesture;
- (void)leftBridgeControlPanGesture:(UIPanGestureRecognizer *)gesture;
- (void)rightBridgeControlPanGesture:(UIPanGestureRecognizer *)gesture;
- (void)saveResultsToResources:(BOOL)bSendToFt;

@end

@implementation PDMResultsController
{
    uint8_t     _buffer[kSendBufferSize];
    BOOL progressiveScheme;
    IBOutlet UIImageView *diameterStaticView;
}

@synthesize popoverMaterialController;
@synthesize popoverBevelController;
@synthesize frameTypes;
@synthesize metalOrAcetateBevels;
@synthesize nylorBevels;
@synthesize mainLogoView;
@synthesize secondPhotoMode;
@synthesize theNewIPad;
@synthesize wrapTextField;
@synthesize vdWrapLabel;
@synthesize VDTextField;
@synthesize popoverController;
@synthesize popoverContentController;
@synthesize popoverProgressiveType;
@synthesize plistPath;
@synthesize rectView;
@synthesize rightEyeView;
@synthesize leftEyeView;
@synthesize snapImage;
@synthesize imageView;
@synthesize measurement;
@synthesize rightPD;
@synthesize leftPD;
@synthesize rightHeight;
@synthesize leftHeight;
@synthesize bridge;
@synthesize headPosture;
@synthesize distanceToIPadTextField;
@synthesize testElementsToHide;
@synthesize switcherImageView;
@synthesize bridgeLabel;
@synthesize eyeArea;
@synthesize singleVisionNow;
@synthesize zoomScrollView;
@synthesize scrollContentView;
@synthesize editButton;
@synthesize infoEditLabel;
@synthesize leftEyeHeightTopControlView;
@synthesize leftEyeHeightBottomControlView;
@synthesize rightEyeHeightTopControlView;
@synthesize rightEyeHeightBottomControlView;
@synthesize leftPdWidthControlView;
@synthesize rightPdWidthControlView;
@synthesize leftBridgeControlView;
@synthesize rightBridgeControlView;
@synthesize markerViews;
@synthesize editModeEnabled;
@synthesize initialInitPositionsAndFrames;
@synthesize results;
@synthesize firstPoint;
@synthesize firstWidth;
@synthesize firstHeight;
@synthesize farPdMode;
@synthesize pdModeLabel;
@synthesize controllersView;
@synthesize distanceToIPadLabel;
@synthesize pdLabel;
@synthesize nearPdDesignViews;
@synthesize nearPdTopRightImageView;
@synthesize nearPdBottomRightImageView;
@synthesize nearPdTopLeftImageView;
@synthesize nearPdBottomLeftImageView;
@synthesize nearPdShowDesignButton;
@synthesize frameWidthTextField;
@synthesize frameHeightTextField;
@synthesize pdModeTitleLabel;
@synthesize pdModeSegmentedControl;
@synthesize previousScale;
@synthesize currentZoomScale;
@synthesize scrollOffsetX;
@synthesize scrollOffsetY;
@synthesize defaultProgressiveDesign;
@synthesize VDLabel;
@synthesize farVisionBlockLabel;
@synthesize nearVisionBlockLabel;
@synthesize frameBlockLabel;
@synthesize personalizedBlockLabel;
@synthesize rightNearPD;
@synthesize leftNearPD;
@synthesize farVision_R;
@synthesize farVision_L;
@synthesize nearVision_R;
@synthesize nearVision_L;
@synthesize nearVision_Upfit;
@synthesize UpfitNearPD;
@synthesize networkStream = _networkStream;
@synthesize fileStream    = _fileStream;
@synthesize bufferOffset  = _bufferOffset;
@synthesize bufferLimit   = _bufferLimit;
@synthesize deviceInfo;
@synthesize DALabel;
@synthesize DATextField;
@synthesize DAzTextField;
@synthesize udp_ip;
@synthesize udp_port;

CPDMResults Results;
extern bool bPDStandard;
extern BOOL needDetectFlashlightType;

// координаты предыдущего окна картинки (см. storyboard)
// xn = 17; yn = 20; width = 991; height = 615;
//static double dxOffset = -10;
//static double dyOffset = -18;
static double dxOffset = 0;
static double dyOffset = 0;
static int nearPdOffsetX = 0;
static int nearPdOffsetY = 0;

struct ResultScreenData {
    CGFloat dxSnapImage;
    CGFloat dySnapImage;
    CGFloat ScaleBigWnd;
    CGFloat ScaleSnapImage;

    CGFloat xnResWin;
    CGFloat ynResWin;
    CGFloat dxResWin;
    CGFloat dyResWin;
    
    CGFloat Scale;
    CGFloat dxPicture;
    CGFloat dyPicture;
    CGFloat cropX;
    CGFloat cropY;
    CGFloat picScale;
    CGFloat rectScale;
    
    CGFloat dxResLRect;
    CGFloat dyResLRect;
    CGFloat dxResRRect;
    CGFloat dyResRRect;
} rsd;

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)selectNearPdDesignType:(id)sender
{
    if (!popoverController)
    {
		popoverContentController = [[PDPopoverControllerTableViewController alloc] initWithParent:self
                                                                          withProgressiveTypeData:popoverProgressiveType];
        
		popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverContentController];
		
        popoverController.delegate = self;
		
        [popoverController presentPopoverFromRect:CGRectMake(750, 65, 100, 1)
                                           inView:self.view
                         permittedArrowDirections:(UIPopoverArrowDirectionUp|UIPopoverArrowDirectionDown)
                                         animated:YES];
	}
    else
    {
		[popoverController dismissPopoverAnimated:YES];
        
        popoverContentController = nil;
		popoverController = nil;
	}
}

- (IBAction)saveOrExit:(id)sender
{
    CPDMDataResults farData = Results.GetPhotoResults(0);
    CPDMDataResults nearData = Results.GetPhotoResults(2);
    
    if (![PD_MANAGER isLoadedSession])
    {
        if ((farData.bDataResults || nearData.bDataResults) && (!SESSION_MANAGER.isSavedSession))
        {
            [self saveSessionAlert];
        }
        else
        {
            if ([self differentDictionaryData] && !SESSION_MANAGER.isSavedSession)
            {
                [self saveSessionAlert];
            }
            else
            {
                [self homeButtonPressed:nil];
            }
        }
    }
    else
    {
        if ([self differentDictionaryData]) {
            
            [self saveSessionAlert];
            
        } else  {
            
            [self homeButtonPressed:nil];
        }
    }
}

- (void)saveSessionAlert
{
    __weak __typeof(self)weakSelf = self;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Session manager", @"")
                                                                   message:NSLocalizedString(@"Save changes in session?", @"")
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *saveButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Save",@"")
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           SESSION_MANAGER.userNotSaveSession = YES;
                                                           [weakSelf storeData];
                                                       }];
    
    UIAlertAction *dontSaveButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Don't save",@"")
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               [weakSelf homeButtonPressed:nil];
                                                           }];
    
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
    [alert addAction:saveButton];
    [alert addAction:dontSaveButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)configureLeftEyeDesign:(CGPoint)eyePoint
{
    int iPhoto = farPdMode.boolValue ? 0 : 2;
    
    double onePixelInMM =  Results.convertToCM(iPhoto, 1.0) / rsd.rectScale;
    
    if (iPhoto == 0) {
        
        CPDMPhotoData PhData =  Results.GetPhotoData(iPhoto);
        if (PhData.nPhotoMode != nNoData) {
            if(Results.CalcPhotoResults(iPhoto)) {
                CPDMDataResults PhResults = Results.GetPhotoResults(iPhoto);
                onePixelInMM = (PhResults.LeftHeight / (PhData.yLeftRect - PhData.yLeftEye)) / rsd.rectScale;
            }
        }
    }

    CGRect topFrame = nearPdTopLeftImageView.frame;
    CGRect bottomFrame = nearPdBottomLeftImageView.frame;
    topFrame.size = CGSizeMake(NEAR_PD_WIDTH_MM / onePixelInMM, NEAR_PD_TOP_PART_HEIGHT_MM / onePixelInMM);
    bottomFrame.size = CGSizeMake(NEAR_PD_WIDTH_MM / onePixelInMM, NEAR_PD_BOTTOM_PART_HEIGHT_MM / onePixelInMM);
    nearPdTopLeftImageView.frame = topFrame;
    nearPdBottomLeftImageView.frame = bottomFrame;
    
    topFrame.origin.x = eyePoint.x - 0.5 * (topFrame.size.width) + 1.0;
    topFrame.origin.y = eyePoint.y - topFrame.size.height * NEAR_TOP_PART_DIST_TO_POINT_MM / NEAR_PD_TOP_PART_HEIGHT_MM + 1.0;
    
    bottomFrame.origin.x = topFrame.origin.x;
    double onePixelForImage = topFrame.size.height / NEAR_PD_TOP_PART_HEIGHT_MM;
    bottomFrame.origin.y = topFrame.origin.y + (NEAR_TOP_PART_DIST_TO_POINT_MM + NEAR_PD_DIST_FROM_POINT_TO_LINE_MM - NEAR_PD_BOTTOM_DIST_TO_POINT - 4.0 + defaultProgressiveDesign) * onePixelForImage;
    
    nearPdTopLeftImageView.frame = topFrame;
    nearPdBottomLeftImageView.frame = bottomFrame;
}

- (void)configureRightEyeDesign:(CGPoint)eyePoint
{
    int iPhoto = farPdMode.boolValue ? 0 : 2;
    double onePixelInMM =  Results.convertToCM(iPhoto, 1.0) / rsd.rectScale;
    
    if (iPhoto == 0) {
        CPDMPhotoData PhData =  Results.GetPhotoData(iPhoto);
        if (PhData.nPhotoMode != nNoData) {
            if(Results.CalcPhotoResults(iPhoto)) {
                CPDMDataResults PhResults = Results.GetPhotoResults(iPhoto);
                onePixelInMM = (PhResults.RightHeight / (PhData.yRightRect - PhData.yRightEye)) / rsd.rectScale;
            }
        }
    }
    
    CGRect topFrame = nearPdTopRightImageView.frame;
    CGRect bottomFrame = nearPdBottomRightImageView.frame;
    topFrame.size = CGSizeMake(NEAR_PD_WIDTH_MM / onePixelInMM, NEAR_PD_TOP_PART_HEIGHT_MM / onePixelInMM);
    bottomFrame.size = CGSizeMake(NEAR_PD_WIDTH_MM / onePixelInMM, NEAR_PD_BOTTOM_PART_HEIGHT_MM / onePixelInMM);
    
    topFrame.origin.x = eyePoint.x - 0.5 * (topFrame.size.width) + 1.0;
    topFrame.origin.y = eyePoint.y - topFrame.size.height * NEAR_TOP_PART_DIST_TO_POINT_MM / NEAR_PD_TOP_PART_HEIGHT_MM + 1.0;
    
    bottomFrame.origin.x = topFrame.origin.x;
    double onePixelForImage = topFrame.size.height / NEAR_PD_TOP_PART_HEIGHT_MM;
    bottomFrame.origin.y = topFrame.origin.y + (NEAR_TOP_PART_DIST_TO_POINT_MM + NEAR_PD_DIST_FROM_POINT_TO_LINE_MM - NEAR_PD_BOTTOM_DIST_TO_POINT - 4.0 + defaultProgressiveDesign) * onePixelForImage;
    
    nearPdTopRightImageView.frame = topFrame;
    nearPdBottomRightImageView.frame = bottomFrame;
}

- (void)configureNearPdDesignFrames
{
    if (editModeEnabled) {
        [self editResults:nil];
    }
    CGPoint rightPoint = CGPointMake([eyeArea rightEye].x,
                                     [eyeArea rightEye].y);
    CGPoint leftPoint = CGPointMake([eyeArea leftEye].x,
                                    [eyeArea leftEye].y);
    [self configureRightEyeDesign:rightPoint];
    [self configureLeftEyeDesign:leftPoint];
    
    [self configureStaticCorridors];
    [self updateLensInfoData];
}

- (IBAction)newSessionPressed:(id)sender
{
    PDMCameraController *viewController = [self.navigationController.viewControllers objectAtIndex:0];
    viewController.returnedFromResults = YES;
    viewController.switchToFar = YES;
    
    UIAlertView *alertView;
    
    if (bPDStandard)
    {
        alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sessions", @"")
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:nil
                                     otherButtonTitles:NSLocalizedString(@"Far PD", @""),
                                                        NSLocalizedString(@"Near PD", @""),
                                                        NSLocalizedString(@"Progressive (Far + Near)",@""),
                                                        NSLocalizedString(@"Save Session",@""),
                                                        NSLocalizedString(@"Session manager",@""),
                                                        NSLocalizedString(@"Cancel", @""), nil];
    }
    else {
        alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sessions", @"")
                                               message:nil
                                              delegate:self
                                     cancelButtonTitle:nil
                                     otherButtonTitles:NSLocalizedString(@"Far PD", @""),
                                                        NSLocalizedString(@"Far PD + Vertex and wrap", @""),
                                                        NSLocalizedString(@"Near PD", @""),
                                                        NSLocalizedString(@"Save Session",@""),
                                                        NSLocalizedString(@"Save Session",@""),
                                                        NSLocalizedString(@"Cancel", @""), nil];
    }
    
    alertView.tag = kNewSessionAlertTag;

    [alertView show];
}

- (IBAction)showWrapController:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    PDWrapController *wrapController = [storyboard instantiateViewControllerWithIdentifier:@"PDWrapController"];
    
    wrapController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:wrapController animated:YES completion:nil];
}

- (IBAction)vdWrapPressed:(id)sender
{
    if (farPdMode.boolValue)
    {
        PDMCameraController *cameraController = [self.navigationController.viewControllers objectAtIndex:0];
        cameraController.returnedFromResults = YES;
        cameraController.switchToFar = YES;
        cameraController.secondPhotoMode = [NSNumber numberWithBool:YES];
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (IBAction)btnHelp_Click:(id)sender
{
    PDHelpType page;
    
    CPDMDataResults resDataFar = Results.GetPhotoResults(0);
    CPDMDataResults resDataNear = Results.GetPhotoResults(2);
    
    if ([PD_MANAGER inExpertMode])
    {
        if (resDataFar.bDataResults && resDataNear.bDataResults)
            page = PDHelpTypeNearResultsExpert;
        else if (resDataFar.bDataResults)
            page = PDHelpTypeFarResultsExpert;
        else
            page = PDHelpTypeNearResultsNewExpert;
    }
    else
    {
        if (resDataFar.bDataResults && resDataNear.bDataResults)
            page = PDHelpTypeNearResults;
        else if (resDataFar.bDataResults)
            page = PDHelpTypeFarResults;
        else
            page = PDHelpTypeNearResultsNew;
    }
    
    
    UIViewController *controller = [PDHelpViewController initWithPage:page];
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (IBAction)goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeButtonPressed:(id)sender
{
    needDetectFlashlightType = YES;
    
    [PD_MANAGER setTargetScreen:PDTargetScreenResults];

    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

- (void)configureResultControllerDidLoad
{
    theNewIPad = YES;
    
    PDDeviceDetails details = [deviceInfo deviceDetails];
    
    if (details.family == PDDeviceFamilyiPad) {
        switch (details.model) {
            case PDDeviceModeliPad:
            case PDDeviceModeliPad2:
                theNewIPad = NO;
                break;
            default:
                theNewIPad = YES;
                break;
        }
    }
	
    defaultProgressiveDesign = PDDesignTypeProgressive16;
    previousScale = 1.0;
    currentZoomScale = 1.0;
    scrollOffsetX = 0.0;
    scrollOffsetY = 0.0;
    
    results = [NSMutableDictionary dictionary];
    singleVisionNow = YES;
	// Do any additional setup after loading the view.
    measurement = [PDMDataManager sharedManager].storage;
    if (!ENABLE_TEST_RESULT_SIZES) {
        for (UIView *testView in testElementsToHide) {
            [testView setHidden:YES];
        }
    }
    if (!self.farPdMode.boolValue) {
        [self.distanceToIPadLabel setHidden:NO];
        [self.distanceToIPadTextField setHidden:NO];
    }
    
    [self refresh];
    // Центрируем и зуммируем картинку
    rsd.dxSnapImage = snapImage.size.width;
    rsd.dySnapImage = snapImage.size.height;
    // Скейл фактор предыдущего окна (width / height)
    rsd.ScaleBigWnd = 991.0 / 615.0;
    // т.к. картинка в режиме Test имеет другой скейл фактор вычисляем его
    // в режиме видео съемки скейл факторы должны совпадать или быть близки
    rsd.ScaleSnapImage = rsd.dxSnapImage / rsd.dySnapImage;
    
    // координаты и размеры результирующего окна
    rsd.xnResWin = imageView.frame.origin.x;
    rsd.ynResWin = imageView.frame.origin.y;
    rsd.dxResWin = imageView.frame.size.width;
    rsd.dyResWin = imageView.frame.size.height;
    
    if( !rsd.dxResWin )
        rsd.dxResWin = 995;
    if( !rsd.dyResWin )
        rsd.dyResWin = 382;
    
    rsd.Scale = rsd.dxSnapImage / 991.0;
    // берем положения маркеров в координатах окна
    CGFloat xLeftEye = [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_EYE_X : NEAR_LEFT_EYE_X] floatValue] - dxOffset;
    CGFloat yLeftEye = [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_EYE_Y : NEAR_LEFT_EYE_Y] floatValue] - dyOffset;
    CGFloat xRightEye = [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_EYE_X : NEAR_RIGHT_EYE_X] floatValue] - dxOffset;
    CGFloat yRightEye = [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_EYE_Y : NEAR_RIGHT_EYE_Y] floatValue] - dyOffset;
    // берем положения маркеров в координатах окна
    CGFloat xLeftSupport = [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_SUPPORT_X : NEAR_LEFT_SUPPORT_X] floatValue] - dxOffset;
    CGFloat yLeftSupport = [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_SUPPORT_Y : NEAR_LEFT_SUPPORT_Y] floatValue] - dyOffset;
    CGFloat xRightSupport = [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_SUPPORT_X : NEAR_RIGHT_SUPPORT_X] floatValue] - dxOffset;
    CGFloat yRightSupport = [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_SUPPORT_Y : NEAR_RIGHT_SUPPORT_Y] floatValue] - dyOffset;
    double lenSupport = sqrt((xLeftSupport - xRightSupport)*(xLeftSupport - xRightSupport) + (yLeftSupport - yRightSupport)*(yLeftSupport - yRightSupport));

    // задаем Ширину для cropImage. Масштабируем в размер Суппорта.
    rsd.dxPicture = floor(rsd.dxSnapImage * (lenSupport / (110. / Device_MMinOnePixel)) + 0.5);
    // определяем высоту cropImage. Необходимо учесть, если картинка отображалась с искажениями пропорций
    rsd.dyPicture = floor(rsd.dxPicture * (rsd.dyResWin / rsd.dxResWin) * (rsd.ScaleBigWnd / rsd.ScaleSnapImage) + 0.5);
    // начальная X координата cropImage. Учитываем желание отцентрировать по зрачкам
    if(rsd.dxPicture > rsd.dxSnapImage) {
        rsd.cropX = (rsd.dxSnapImage - rsd.dxPicture) / 2.0;
    }
    else {
        rsd.cropX = ((xLeftEye + xRightEye) * rsd.Scale - rsd.dxPicture) / 2.0;
        if(rsd.cropX < 0.0)
            rsd.cropX = 0.0;
        if(rsd.cropX + rsd.dxPicture > rsd.dxSnapImage) {
            rsd.cropX = rsd.dxSnapImage - rsd.dxPicture;
        }
    }
    // начальная Y координата cropImage. Учитываем желание отцентрировать по зрачкам
    if(rsd.dyPicture > rsd.dySnapImage) {
        rsd.cropY = (rsd.dySnapImage - rsd.dyPicture) / 2.0;
    }
    else {
        rsd.cropY = ((yLeftEye + yRightEye) * rsd.Scale - rsd.dyPicture) / 2.0;
        if(rsd.cropY < 0.0)
            rsd.cropY = 0.0;
        if(rsd.cropY + rsd.dyPicture > rsd.dySnapImage) {
            rsd.cropY = rsd.dySnapImage - rsd.dyPicture;
        }
    }
    UIImage *cropImage = [snapImage imageAtRect:CGRectMake(rsd.cropX, rsd.cropY, rsd.dxPicture, rsd.dyPicture)];

    // определяем положения зрачков в координатах окна на cropImage с учетом сдвига
    rsd.picScale = rsd.dxPicture / rsd.dxResWin;
    
    if( !rsd.picScale )
        rsd.picScale = 1;
    
    CGFloat xResLEye = (xLeftEye * rsd.Scale - rsd.cropX) / rsd.picScale + rsd.xnResWin;
    CGFloat yResLEye = (yLeftEye * rsd.Scale * (rsd.ScaleBigWnd / rsd.ScaleSnapImage) - rsd.cropY) * (rsd.ScaleSnapImage / rsd.ScaleBigWnd) / rsd.picScale + rsd.ynResWin;
    CGFloat xResREye = (xRightEye * rsd.Scale - rsd.cropX) / rsd.picScale + rsd.xnResWin;
    CGFloat yResREye = (yRightEye * rsd.Scale * (rsd.ScaleBigWnd / rsd.ScaleSnapImage) - rsd.cropY) * (rsd.ScaleSnapImage / rsd.ScaleBigWnd) / rsd.picScale + rsd.ynResWin;
    
    // берем положения прямоугольников в координатах окна
    CGFloat xLeftRect = [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_RECT_X : NEAR_LEFT_RECT_X] floatValue] - dxOffset; 
    CGFloat yLeftRect = [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_RECT_Y : NEAR_LEFT_RECT_Y] floatValue] - dyOffset;
    CGFloat dxLeftRect = [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_RECT_WIDTH : NEAR_LEFT_RECT_WIDTH] floatValue];
//    CGFloat dyLeftRect = [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_RECT_HEIGHT : NEAR_LEFT_RECT_HEIGHT] floatValue];
    CGFloat xRightRect = [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_RECT_X : NEAR_RIGHT_RECT_X] floatValue] - dxOffset;
    CGFloat yRightRect = [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_RECT_Y : NEAR_RIGHT_RECT_Y] floatValue] - dyOffset;
    CGFloat dxRightRect = [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_RECT_WIDTH : NEAR_RIGHT_RECT_WIDTH] floatValue];
//    CGFloat dyRightRect = [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_RECT_HEIGHT : NEAR_RIGHT_RECT_HEIGHT] floatValue];
    
    // определяем положения прямоугольников в координатах окна на cropImage с учетом сдвига
    rsd.rectScale = rsd.dxSnapImage / rsd.dxPicture;
    
    if( rsd.rectScale == INFINITY )
        rsd.rectScale = 1.3268757;
    
    CGFloat xResLRect = (xLeftRect * rsd.Scale - rsd.cropX) / rsd.picScale + rsd.xnResWin;
    CGFloat yResLRect = (yLeftRect * rsd.Scale * (rsd.ScaleBigWnd / rsd.ScaleSnapImage) - rsd.cropY) * (rsd.ScaleSnapImage / rsd.ScaleBigWnd) / rsd.picScale + rsd.ynResWin;
    rsd.dxResLRect = dxLeftRect * rsd.rectScale;
    rsd.dyResLRect = 0;// rsd.dxResLRect * dyLeftRect / dxLeftRect;
    CGFloat xResRRect = (xRightRect * rsd.Scale - rsd.cropX) / rsd.picScale + rsd.xnResWin;
    CGFloat yResRRect = (yRightRect * rsd.Scale * (rsd.ScaleBigWnd / rsd.ScaleSnapImage) - rsd.cropY) * (rsd.ScaleSnapImage / rsd.ScaleBigWnd) / rsd.picScale + rsd.ynResWin;
    rsd.dxResRRect = dxRightRect * rsd.rectScale;
    rsd.dyResRRect = 0;// rsd.dxResRRect * dyRightRect / dxRightRect;
    
    [customerFace setImage:cropImage];
    
    // картинку присваиваем как есть, без обрезания
    [imageView setImage:cropImage];
    
    eyeArea = [[PDMEyeArea alloc] initWithFrame:CGRectMake(0, 0, 1024, 400)];
    [eyeArea setLeftEye:CGPointMake(xResLEye, yResLEye)];
    [eyeArea setLeftRect:CGPointMake(xResLRect, yResLRect + rsd.dyResLRect)];
    [eyeArea setRightEye:CGPointMake(xResREye, yResREye)];    
    [eyeArea setRightRect:CGPointMake(xResRRect + rsd.dxResRRect, yResRRect + rsd.dyResRRect)];
    
    int iPhoto = farPdMode.boolValue ? 0 : 2;
    // Visual Controller data
    if(Results.CalcPhotoResults(iPhoto)) {
        CPDMDataResults DataPhoto;
        DataPhoto = Results.GetPhotoResults(iPhoto);
        
        [eyeArea setLeftPDString:[NSString stringWithFormat:@"%.1f", DataPhoto.LeftPD]];
        [eyeArea setLeftHeightString:[NSString stringWithFormat:@"%.1f", DataPhoto.LeftHeight + [self getBevelValue]]];
        [eyeArea setRightPDString:[NSString stringWithFormat:@"%.1f", DataPhoto.RightPD]];
        [eyeArea setRightHeightString:[NSString stringWithFormat:@"%.1f", DataPhoto.RightHeight + [self getBevelValue]]];
        [eyeArea setBridgeString:[NSString stringWithFormat:@"%.1f", DataPhoto.Bridge]];
    }
    
    [eyeArea setTag:7];
    [eyeArea refresh];
    [eyeArea setUserInteractionEnabled:NO];
    [controllersView addSubview:eyeArea];
    //    initialInitPositionsAndFrames = NO;
    //    [self refresh];
    [self addGestures];
}

- (void)configureResultControllerWillAppear
{
    editModeEnabled = NO;
    [zoomScrollView setZoomScale:1.0];
    //    self.navigationItem.title = [NSString stringWithFormat:@"%@ PD - Results", farPdMode.boolValue ? @"Far" : @"Near"];
    pdModeTitleLabel.text = farPdMode.boolValue ? NSLocalizedString(@"Far PD - Results", @"") : NSLocalizedString(@"Near PD - Results", @"");
    [self.pdModeSegmentedControl setSelectedSegmentIndex:farPdMode.boolValue ? 1 : 0];
    [self configureControlViewFrames];
    for (UIView *view in markerViews) {
        view.backgroundColor = [UIColor clearColor];
    }
    [controllersView setUserInteractionEnabled:NO];
    
    [self refresh];
    [self saveResultsToResources:NO];
}

- (void)setHeadRotateImage
{
    int iPhoto = farPdMode.boolValue ? 0 : 2;
    CPDMDataResults data = Results.GetPhotoResults(iPhoto);
    double rotate = -((data.alfaPan * 180) / M_PI);
    
    if (rotate <= 1. && rotate >= -1.)
    {
        [headRotationImage setImage:[UIImage imageNamed:@"HeadRotationCorrect"]];
    }
    else if (rotate < -1. && rotate >= -5.)
    {
        [headRotationImage setImage:[UIImage imageNamed:@"HeadRotationIncorrectLeft1"]];
    }
    else if (rotate < -5. && rotate >= -10.)
    {
        [headRotationImage setImage:[UIImage imageNamed:@"HeadRotationIncorrectLeft2"]];
    }
    else if (rotate < -10.)
    {
        [headRotationImage setImage:[UIImage imageNamed:@"HeadRotationIncorrectLeft3"]];
    }
    else if (rotate > 1. && rotate <= 5.)
    {
        [headRotationImage setImage:[UIImage imageNamed:@"HeadRotationIncorrectRight1"]];
    }
    else if (rotate > 5. && rotate <= 10.)
    {
        [headRotationImage setImage:[UIImage imageNamed:@"HeadRotationIncorrectRight2"]];
    }
    else if (rotate > 10.)
    {
        [headRotationImage setImage:[UIImage imageNamed:@"HeadRotationIncorrectRight3"]];
    }
}

- (void)tryToFillSaveDictionary
{
    if (!SESSION_MANAGER.saveResultDictionary)
    {
        SESSION_MANAGER.saveResultDictionary = [NSMutableDictionary dictionary];
        
        [SESSION_MANAGER.saveResultDictionary setObject:[rightPD text] forKey:@"rightPD"];
        [SESSION_MANAGER.saveResultDictionary setObject:[leftPD text] forKey:@"leftPD"];
        [SESSION_MANAGER.saveResultDictionary setObject:[rightHeight text] forKey:@"rightHeight"];
        [SESSION_MANAGER.saveResultDictionary setObject:[leftHeight text] forKey:@"leftHeight"];
        [SESSION_MANAGER.saveResultDictionary setObject:[frameWidthTextField text] forKey:@"frameWidthTextField"];
        [SESSION_MANAGER.saveResultDictionary setObject:[frameHeightTextField text] forKey:@"frameHeightTextField"];
        [SESSION_MANAGER.saveResultDictionary setObject:[bridge text] forKey:@"bridge"];
        [SESSION_MANAGER.saveResultDictionary setObject:[headPosture text] forKey:@"headPosture"];
        [SESSION_MANAGER.saveResultDictionary setObject:[VDTextField text] forKey:@"VDTextField"];
        [SESSION_MANAGER.saveResultDictionary setObject:[wrapTextField text] forKey:@"wrapTextField"];
        [SESSION_MANAGER.saveResultDictionary setObject:[rightNearPD text] forKey:@"rightNearPD"];
        [SESSION_MANAGER.saveResultDictionary setObject:[leftNearPD text] forKey:@"leftNearPD"];
        [SESSION_MANAGER.saveResultDictionary setObject:[distanceToIPadTextField text] forKey:@"distanceToIPadTextField"];
    }
}

- (BOOL)differentDictionaryData
{
    NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
    
    [tempDictionary setObject:[rightPD text] forKey:@"rightPD"];
    [tempDictionary setObject:[leftPD text] forKey:@"leftPD"];
    [tempDictionary setObject:[rightHeight text] forKey:@"rightHeight"];
    [tempDictionary setObject:[leftHeight text] forKey:@"leftHeight"];
    [tempDictionary setObject:[frameWidthTextField text] forKey:@"frameWidthTextField"];
    [tempDictionary setObject:[frameHeightTextField text] forKey:@"frameHeightTextField"];
    [tempDictionary setObject:[bridge text] forKey:@"bridge"];
    [tempDictionary setObject:[headPosture text] forKey:@"headPosture"];
    [tempDictionary setObject:[VDTextField text] forKey:@"VDTextField"];
    [tempDictionary setObject:[wrapTextField text] forKey:@"wrapTextField"];
    [tempDictionary setObject:[rightNearPD text] forKey:@"rightNearPD"];
    [tempDictionary setObject:[leftNearPD text] forKey:@"leftNearPD"];
    [tempDictionary setObject:[distanceToIPadTextField text] forKey:@"distanceToIPadTextField"];
    
    for (NSString *key in [SESSION_MANAGER.saveResultDictionary allKeys]) {
        
        if ([[SESSION_MANAGER.saveResultDictionary objectForKey:key] floatValue] != [[tempDictionary objectForKey:key] floatValue]) {
            
            tempDictionary = nil;
            
            return YES;
        }
    }
    
    tempDictionary = nil;
    
    return NO;
}

#pragma mark - Life Cycle
#pragma mark - viewDidLoad

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    progressiveScheme = NO;
    showStaticScheme = NO;
    showNearPDDesign = NO;
    showCircDiameters = NO;

    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"use_rotation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setBool:([PD_MANAGER inExpertMode] ? NO : YES) forKey:@"hard_frame"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [editButton setTitle:NSLocalizedString(@"Edit", @"") forState:UIControlStateNormal];
    schemeLabel.text = NSLocalizedString(@"Scheme", @"");
    m_labelLensType.text = (Results.GetProgressiveDiameterStatus())? NSLocalizedString(@"Progressive", @"") : NSLocalizedString(@"Single Vision", @"");

    popoverProgressiveType = [[PDMProgressiveType alloc] init];
    
    [self configureResultControllerDidLoad];
    
    Results.SetWrapAngle(false, 5. * M_PI / 180.);
    editDiameterState = 0;
    
#ifdef HIDE_WRAP_INFORMATION
    [vdWrapLabel setHidden:YES];
    [wrapTextField setHidden:YES];
#endif
    
    if (SHOW_DEVICE_ANGLE == 0) {
        [DALabel setHidden:YES];
        [DATextField setHidden:YES];
        [DAzTextField setHidden:YES];
    }
    
    //localization
    [pdModeSegmentedControl setTitle:NSLocalizedString(@"Near PD", @"") forSegmentAtIndex:0];
    [pdModeSegmentedControl setTitle:NSLocalizedString(@"Far PD", @"") forSegmentAtIndex:1];
    infoEditLabel.text = NSLocalizedString(@"Edit sizes as needed, tap OK when finished", @"");
    [self updateLensInfoData];
    
    [headRotationCorrection setTitle:NSLocalizedString(@"On", @"") forState:UIControlStateNormal];
    
    [zoomScrollView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [zoomScrollView.layer setBorderWidth:1.f];
    
    [self initMaterialArray];
    [self setMaterialLabel:false];
    [self setBevelLabel:false];
    
    metalOrAcetateBevels = @[@0.4, @0.5, @0.6, @0.7, @0.8, @0.9, @1.0, @1.1, @1.2, @1.3, @1.4];
    nylorBevels = @[@0.0, @0.1, @0.2, @0.3, @0.4, @0.5, @0.6, @0.7, @0.8, @0.9, @1.0, @1.1];
    
    if (PDM_DATA.currentSessionType == PDMSessionProgressive)
    {
        [self reloadNearData];
        [self reloadFarData];
    }

    bool bProgressiveStatus = Results.GetProgressiveDiameterStatus();
    if (showNearPDDesign == NO && bProgressiveStatus) {
        Results.SetProgressiveDiameterStatus(!bProgressiveStatus);
        [self showNearPdDesign:nearPdShowDesignButton];
    }
}

- (void)configureForSelectMode
{
    if ([PD_MANAGER inExpertMode])
    {
        view1.frame = CGRectMake(36, 548, 209, 182);
        view2.frame = CGRectMake(253, 548, 274, 182);
        view3.frame = CGRectMake(535, 548, 181, 182);
        view4.frame = CGRectMake(724, 548, 303, 182);
        
        view1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"PDResultsBounds1g"]];
        view2.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"PDResultsBounds2g"]];
        view3.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"PDResultsBounds3g"]];
        view4.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"PDResultsBounds4g"]];
        
        schemeBG.frame = CGRectMake(724, 735, 254, 45);
        rotationBG.frame = CGRectMake(419, 735, 297, 45);
        schemeBG.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"schemeBG"]];
        rotationBG.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"rotationBG"]];
        
        schemeBtn.frame = CGRectMake(114, 0, schemeBtn.frame.size.width, schemeBtn.frame.size.height);
        schemeLabel.frame = CGRectMake(-4, 10, schemeLabel.frame.size.width, schemeLabel.frame.size.height);
        _btnQuestion.frame = CGRectMake(997, 744, _btnQuestion.frame.size.width, _btnQuestion.frame.size.height);
        
        pdLabel.frame = CGRectMake(37+9, 13, pdLabel.frame.size.width, pdLabel.frame.size.height);
        m_labelHeight1.frame = CGRectMake(99, 13, m_labelHeight1.frame.size.width, m_labelHeight1.frame.size.height);
        rightPD.frame = CGRectMake(38+9, 48, rightPD.frame.size.width, rightPD.frame.size.height);
        leftPD.frame = CGRectMake(38+9, 91, leftPD.frame.size.width, leftPD.frame.size.height);
        farVision_R.frame = CGRectMake(1, 52, farVision_R.frame.size.width, farVision_R.frame.size.height);
        farVision_L.frame = CGRectMake(1, 97, farVision_L.frame.size.width, farVision_L.frame.size.height);
        rightHeight.frame = CGRectMake(121, 48, rightHeight.frame.size.width, rightHeight.frame.size.height);
        leftHeight.frame = CGRectMake(121, 91, leftHeight.frame.size.width, leftHeight.frame.size.height);
        totalPDLabel.frame = CGRectMake(10, 144, totalPDLabel.frame.size.width, totalPDLabel.frame.size.height);
        totalPDField.frame = CGRectMake(121, 140, totalPDField.frame.size.width, totalPDField.frame.size.height);
        m_labelFrameWidth.frame = CGRectMake(3, 8, m_labelFrameWidth.frame.size.width, m_labelFrameWidth.frame.size.height);
        frameWidthTextField.frame = CGRectMake(63, 9, frameWidthTextField.frame.size.width, frameWidthTextField.frame.size.height);
        typeBtn.frame = CGRectMake(137, 9, typeBtn.frame.size.width, typeBtn.frame.size.height);
        showTypeLabel.frame = CGRectMake(185, 13, showTypeLabel.frame.size.width, showTypeLabel.frame.size.height);
        m_labelFrameHeight.frame = CGRectMake(3, 52, m_labelFrameHeight.frame.size.width, m_labelFrameHeight.frame.size.height);
        frameHeightTextField.frame = CGRectMake(63, 53, frameHeightTextField.frame.size.width, frameHeightTextField.frame.size.height);
        bevelBtn.frame = CGRectMake(137, 53, bevelBtn.frame.size.width, bevelBtn.frame.size.height);
        showBevelLabel.frame = CGRectMake(199, 57, showBevelLabel.frame.size.width, showBevelLabel.frame.size.height);
        m_labelBridge.frame = CGRectMake(3, 94, m_labelBridge.frame.size.width, m_labelBridge.frame.size.height);
        bridge.frame = CGRectMake(63, 98, bridge.frame.size.width, bridge.frame.size.height);
        diametrBtn.frame = CGRectMake(63, 142, diametrBtn.frame.size.width, diametrBtn.frame.size.height);
        diametrRLabel.frame = CGRectMake(145, 123, diametrRLabel.frame.size.width, diametrRLabel.frame.size.height);
        diametrLLabel.frame = CGRectMake(207, 123, diametrLLabel.frame.size.width, diametrLLabel.frame.size.height);
        diametrRField.frame = CGRectMake(138, 144, diametrRField.frame.size.width, diametrRField.frame.size.height);
        diametrLField.frame = CGRectMake(200, 144, diametrLField.frame.size.width, diametrLField.frame.size.height);
        
        m_labelPanto.frame = CGRectMake(20, 5, m_labelPanto.frame.size.width, m_labelPanto.frame.size.height);
        headPosture.frame = CGRectMake(94, 9, headPosture.frame.size.width, headPosture.frame.size.height);
        VDLabel.frame = CGRectMake(20, 50, VDLabel.frame.size.width, VDLabel.frame.size.height);
        VDTextField.frame = CGRectMake(94, 53, VDTextField.frame.size.width, VDTextField.frame.size.height);
        _btnWrap.frame = CGRectMake(20, 98, _btnWrap.frame.size.width, _btnWrap.frame.size.height);
        wrapTextField.frame = CGRectMake(94, 98, wrapTextField.frame.size.width, wrapTextField.frame.size.height);
        hardFrameLabel.frame = CGRectMake(20.f, 139.f, hardFrameLabel.frame.size.width, hardFrameLabel.frame.size.height);
        hardFrameSwitch.frame = CGRectMake(103.f, 141.f, hardFrameSwitch.frame.size.width, hardFrameSwitch.frame.size.height);
        
        pdNearLabel.frame = CGRectMake(48, 16, pdNearLabel.frame.size.width, pdNearLabel.frame.size.height);
        insertLabel.frame = CGRectMake(122, 16, insertLabel.frame.size.width, insertLabel.frame.size.height);
        bifocalLabel.frame = CGRectMake(203, 12, bifocalLabel.frame.size.width, bifocalLabel.frame.size.height);
        nearVision_R.frame = CGRectMake(8, 51, nearVision_R.frame.size.width, nearVision_R.frame.size.height);
        rightNearPD.frame = CGRectMake(49, 47, rightNearPD.frame.size.width, rightNearPD.frame.size.height);
        insertRField.frame = CGRectMake(123, 47, insertRField.frame.size.width, insertRField.frame.size.height);
        bifocalRField.frame = CGRectMake(212, 47, bifocalRField.frame.size.width, bifocalRField.frame.size.height);
        nearVision_L.frame = CGRectMake(8, 94, nearVision_L.frame.size.width, nearVision_L.frame.size.height);
        leftNearPD.frame = CGRectMake(49, 90, leftNearPD.frame.size.width, leftNearPD.frame.size.height);
        insertLField.frame = CGRectMake(123, 90, insertLField.frame.size.width, insertLField.frame.size.height);
        nearVision_Upfit.frame = CGRectMake(15, 143, nearVision_Upfit.frame.size.width, nearVision_Upfit.frame.size.height);
        UpfitNearPD.frame = CGRectMake(123, 139, UpfitNearPD.frame.size.width, UpfitNearPD.frame.size.height);
        distanceToIPadTextField.frame = CGRectMake(212, 139, distanceToIPadTextField.frame.size.width, distanceToIPadTextField.frame.size.height);
        distanceToIPadLabel.frame = CGRectMake(207, 100, distanceToIPadLabel.frame.size.width, distanceToIPadLabel.frame.size.height);
        farVisionBlockLabel.frame = CGRectMake(22, 519, farVisionBlockLabel.frame.size.width, farVisionBlockLabel.frame.size.height);
        frameBlockLabel.frame = CGRectMake(320, 519, frameBlockLabel.frame.size.width, frameBlockLabel.frame.size.height);
        personalizedBlockLabel.frame = CGRectMake(538, 519, personalizedBlockLabel.frame.size.width, personalizedBlockLabel.frame.size.height);
        nearVisionBlockLabel.frame = CGRectMake(781, 519, nearVisionBlockLabel.frame.size.width, nearVisionBlockLabel.frame.size.height);
        
        rotationBG.hidden = NO;
        
        bifocalLabel.hidden = NO;
        bifocalRField.hidden = NO;
        bifocalLField.hidden = NO;
        
        insertLabel.hidden = NO;
        insertRField.hidden = NO;
        insertLField.hidden = NO;
        
        totalPDLabel.hidden = NO;
        totalPDField.hidden = NO;
        
        diametrBtn.hidden = NO;
        diametrRLabel.hidden = NO;
        diametrLLabel.hidden = NO;
        diametrRField.hidden = NO;
        diametrLField.hidden = NO;
        
        typeBtn.hidden = NO;
        showTypeLabel.hidden = NO;
        
        bevelBtn.hidden = NO;
        showBevelLabel.hidden = NO;
        
        hardFrameLabel.hidden  = NO;
        hardFrameSwitch.hidden = NO;
    }
    else
    {
        view1.frame = CGRectMake(57, 565, 230, 156);
        view2.frame = CGRectMake(329, 565, 157, 156);
        view3.frame = CGRectMake(528, 565, 202, 156);
        view4.frame = CGRectMake(772, 565, 237, 156);
        
        view1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"PDResultsBounds1gSimple"]];
        view2.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"PDResultsBounds2gSimple"]];
        view3.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"PDResultsBounds3gSimple"]];
        view4.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"PDResultsBounds4gSimple"]];
        
        schemeBG.frame = CGRectMake(772, 735, 180, 45);
        rotationBG.frame = CGRectMake(419, 735, 297, 45);
        schemeBG.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"schemeBGSimple"]];
        rotationBG.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"rotationBG"]];
        
        schemeBtn.frame = CGRectMake(81, 0, schemeBtn.frame.size.width, schemeBtn.frame.size.height);
        schemeLabel.frame = CGRectMake(-25, 10, schemeLabel.frame.size.width, schemeLabel.frame.size.height);
        _btnQuestion.frame = CGRectMake(984, 744, _btnQuestion.frame.size.width, _btnQuestion.frame.size.height);
        
        pdLabel.frame = CGRectMake(48+9, 17, pdLabel.frame.size.width, pdLabel.frame.size.height);
        m_labelHeight1.frame = CGRectMake(108+9, 17, m_labelHeight1.frame.size.width, m_labelHeight1.frame.size.height);
        farVision_R.frame = CGRectMake(12, 54, farVision_R.frame.size.width, farVision_R.frame.size.height);
        rightPD.frame = CGRectMake(49+9, 52, rightPD.frame.size.width, rightPD.frame.size.height);
        rightHeight.frame = CGRectMake(130+9, 52, rightHeight.frame.size.width, rightHeight.frame.size.height);
        farVision_L.frame = CGRectMake(12, 99, farVision_L.frame.size.width, farVision_L.frame.size.height);
        leftPD.frame = CGRectMake(49+9, 93, leftPD.frame.size.width, leftPD.frame.size.height);
        leftHeight.frame = CGRectMake(130+9, 93, leftHeight.frame.size.width, leftHeight.frame.size.height);
        farVisionBlockLabel.frame = CGRectMake(50, 532, farVisionBlockLabel.frame.size.width, farVisionBlockLabel.frame.size.height);
        frameBlockLabel.frame = CGRectMake(331, 532, frameBlockLabel.frame.size.width, frameBlockLabel.frame.size.height);
        m_labelFrameWidth.frame = CGRectMake(15, 17, m_labelFrameWidth.frame.size.width, m_labelFrameWidth.frame.size.height);
        frameWidthTextField.frame = CGRectMake(75, 18, frameWidthTextField.frame.size.width, frameWidthTextField.frame.size.height);
        m_labelFrameHeight.frame = CGRectMake(15, 60, m_labelFrameHeight.frame.size.width, m_labelFrameHeight.frame.size.height);
        frameHeightTextField.frame = CGRectMake(75, 61, frameHeightTextField.frame.size.width, frameHeightTextField.frame.size.height);
        m_labelBridge.frame = CGRectMake(15, 100, m_labelBridge.frame.size.width, m_labelBridge.frame.size.height);
        bridge.frame = CGRectMake(75, 104, bridge.frame.size.width, bridge.frame.size.height);
        personalizedBlockLabel.frame = CGRectMake(539, 532, personalizedBlockLabel.frame.size.width, personalizedBlockLabel.frame.size.height);
        m_labelPanto.frame = CGRectMake(26, 20, m_labelPanto.frame.size.width, m_labelPanto.frame.size.height);
        headPosture.frame = CGRectMake(112, 20, headPosture.frame.size.width, headPosture.frame.size.height);
        VDLabel.frame = CGRectMake(26, 63, VDLabel.frame.size.width, VDLabel.frame.size.height);
        VDTextField.frame = CGRectMake(112, 63, VDTextField.frame.size.width, VDTextField.frame.size.height);
        _btnWrap.frame = CGRectMake(26, 106, _btnWrap.frame.size.width, _btnWrap.frame.size.height);
        wrapTextField.frame = CGRectMake(112, 106, wrapTextField.frame.size.width, wrapTextField.frame.size.height);
        nearVisionBlockLabel.frame = CGRectMake(796, 532, nearVisionBlockLabel.frame.size.width, nearVisionBlockLabel.frame.size.height);
        pdNearLabel.frame = CGRectMake(0, 34, pdNearLabel.frame.size.width, pdNearLabel.frame.size.height);
        nearVision_R.frame = CGRectMake(82, 8, nearVision_R.frame.size.width, nearVision_R.frame.size.height);
        nearVision_L.frame = CGRectMake(164, 8, nearVision_L.frame.size.width, nearVision_L.frame.size.height);
        rightNearPD.frame = CGRectMake(71, 30, rightNearPD.frame.size.width, rightNearPD.frame.size.height);
        leftNearPD.frame = CGRectMake(152, 30, leftNearPD.frame.size.width, leftNearPD.frame.size.height);
        distanceToIPadTextField.frame = CGRectMake(152, 73, distanceToIPadTextField.frame.size.width, distanceToIPadTextField.frame.size.height);
        distanceToIPadLabel.frame = CGRectMake(66, 70, distanceToIPadLabel.frame.size.width, distanceToIPadLabel.frame.size.height);
        nearVision_Upfit.frame = CGRectMake(44, 120, nearVision_Upfit.frame.size.width, nearVision_Upfit.frame.size.height);
        UpfitNearPD.frame = CGRectMake(152, 116, UpfitNearPD.frame.size.width, UpfitNearPD.frame.size.height);
        
        rotationBG.hidden = YES;
        
        bifocalLabel.hidden = YES;
        bifocalRField.hidden = YES;
        bifocalLField.hidden = YES;
        
        insertLabel.hidden = YES;
        insertRField.hidden = YES;
        insertLField.hidden = YES;
        
        totalPDLabel.hidden = YES;
        totalPDField.hidden = YES;
        
        diametrBtn.hidden = YES;
        diametrRLabel.hidden = YES;
        diametrLLabel.hidden = YES;
        diametrRField.hidden = YES;
        diametrLField.hidden = YES;
        
        typeBtn.hidden = YES;
        showTypeLabel.hidden = YES;
        
        bevelBtn.hidden = YES;
        showBevelLabel.hidden = YES;
        
        hardFrameLabel.hidden  = YES;
        hardFrameSwitch.hidden = YES;
    }
}

#pragma mark - viewWillAppear

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    m_log = [[NSUserDefaults standardUserDefaults] stringForKey:@"ftp_login"];
    m_pas = [[NSUserDefaults standardUserDefaults] stringForKey:@"ftp_password"];

    [self configureResultControllerWillAppear];
    
    // For NearPD photo shows FarPD results if presents
    if((!farPdMode.boolValue && Results.CalcPhotoResults(0)) || farPdMode.boolValue)
    {
        [self reloadFarData];
    }
    
    if ([PD_MANAGER isLoadedSession])
        [self reloadAllData];
    
    [self configureForSelectMode];
    
    [self setControllerOrientation];
}

- (void)setControllerOrientation
{
    [[UIDevice currentDevice] setValue:@([[UIApplication sharedApplication] statusBarOrientation])
                                forKey:@"orientation"];
    
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
}

#pragma mark - viewDidAppear

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //start udp requests
    [self startRequestTimer];
    
    [self tryToFillSaveDictionary];
    
    if ([APP_MANAGER isSoonExpirationDate])
        [self showProvisionAlert];
}

#pragma mark - viewWillDisappear

- (void)viewWillDisappear:(BOOL)animated
{
    [self stopRequestTimer];
    
    [zoomScrollView setZoomScale:1.f];
    
    [super viewWillDisappear:animated];
}

#pragma mark - Dealloc

- (void)dealloc
{
    [self removeGestures];
    
    [self setRectView:nil];
    [self setSnapImage:nil];
    [self setImageView:nil];
    [self setRightPD:nil];
    [self setLeftPD:nil];
    [self setRightHeight:nil];
    [self setLeftHeight:nil];
    [self setBridge:nil];
    [self setHeadPosture:nil];
    [self setRightEyeView:nil];
    [self setLeftEyeView:nil];
    [self setDistanceToIPadTextField:nil];
    [self setTestElementsToHide:nil];
    [self setSwitcherImageView:nil];
    [self setZoomScrollView:nil];
    [self setScrollContentView:nil];
    [self setEditButton:nil];
    [self setInfoEditLabel:nil];
    [self setLeftEyeHeightTopControlView:nil];
    [self setLeftEyeHeightBottomControlView:nil];
    [self setRightEyeHeightTopControlView:nil];
    [self setRightEyeHeightBottomControlView:nil];
    [self setLeftPdWidthControlView:nil];
    [self setRightPdWidthControlView:nil];
    [self setLeftBridgeControlView:nil];
    [self setRightBridgeControlView:nil];
    [self setMarkerViews:nil];
    [self setPdModeLabel:nil];
    [self setControllersView:nil];
    [self setDistanceToIPadLabel:nil];
    [self setPdLabel:nil];
    [self setNearPdDesignViews:nil];
    [self setNearPdTopRightImageView:nil];
    [self setNearPdBottomRightImageView:nil];
    [self setNearPdTopLeftImageView:nil];
    [self setNearPdBottomLeftImageView:nil];
    [self setNearPdShowDesignButton:nil];
    [self setPdModeTitleLabel:nil];
    [self setPdModeSegmentedControl:nil];
    [self setWrapTextField:nil];
    [self setVDTextField:nil];
    [self setFrameWidthTextField:nil];
    [self setFrameHeightTextField:nil];
}

#pragma mark -

- (void)SaveEditResult
{
    // Используем положения элементов для вычисления значений в экранных координатах
    CGFloat xResLEye = [eyeArea leftEye].x;
    CGFloat yResLEye = [eyeArea leftEye].y;
    CGFloat xResREye = [eyeArea rightEye].x;
    CGFloat yResREye = [eyeArea rightEye].y;
//    NSLog(@"RX = %lf RY = %lf",xResREye ,yResREye);
    
    // вычисляем положения зрачков в координатах окна
    CGFloat xLeftEye = ((xResLEye - rsd.xnResWin) * rsd.picScale + rsd.cropX) / rsd.Scale;
    CGFloat yLeftEye = ((yResLEye - rsd.ynResWin) * rsd.picScale / (rsd.ScaleSnapImage / rsd.ScaleBigWnd) + rsd.cropY) / (rsd.Scale * (rsd.ScaleBigWnd / rsd.ScaleSnapImage));
    CGFloat xRightEye = ((xResREye - rsd.xnResWin) * rsd.picScale + rsd.cropX) / rsd.Scale;
    CGFloat yRightEye = ((yResREye - rsd.ynResWin) * rsd.picScale / (rsd.ScaleSnapImage / rsd.ScaleBigWnd) + rsd.cropY) / (rsd.Scale * (rsd.ScaleBigWnd / rsd.ScaleSnapImage));

    // учитываем сдвижку
    xLeftEye += dxOffset;
    yLeftEye += dyOffset;
    xRightEye += dxOffset;
    yRightEye += dyOffset;
    
    // сохраняем положения прямоугольников в координатах окна
    [measurement setValue:[NSNumber numberWithFloat:xLeftEye] forKey:farPdMode.boolValue ? FAR_LEFT_EYE_X : NEAR_LEFT_EYE_X];
    [measurement setValue:[NSNumber numberWithFloat:yLeftEye] forKey:farPdMode.boolValue ? FAR_LEFT_EYE_Y : NEAR_LEFT_EYE_Y];
    
    [measurement setValue:[NSNumber numberWithFloat:xRightEye] forKey:farPdMode.boolValue ? FAR_RIGHT_EYE_X : NEAR_RIGHT_EYE_X];
    [measurement setValue:[NSNumber numberWithFloat:yRightEye] forKey:farPdMode.boolValue ? FAR_RIGHT_EYE_Y : NEAR_RIGHT_EYE_Y];
    
    // Используем вычисленные ранее переменные - dxResLRect, dyResLRect, dxResRRect, dyResRRect
    CGFloat xResLRect = [eyeArea leftRect].x;
    CGFloat yResLRect = [eyeArea leftRect].y - rsd.dyResLRect;
    CGFloat xResRRect = [eyeArea rightRect].x - rsd.dxResRRect;
    CGFloat yResRRect = [eyeArea rightRect].y - rsd.dyResRRect;
    
    // вычисляем положения прямоугольников в координатах окна
    CGFloat xLeftRect = ((xResLRect - rsd.xnResWin) * rsd.picScale + rsd.cropX) / rsd.Scale;
    CGFloat yLeftRect = ((yResLRect - rsd.ynResWin) * rsd.picScale / (rsd.ScaleSnapImage / rsd.ScaleBigWnd) + rsd.cropY) / (rsd.Scale * (rsd.ScaleBigWnd / rsd.ScaleSnapImage));
    CGFloat xRightRect = ((xResRRect - rsd.xnResWin) * rsd.picScale + rsd.cropX) / rsd.Scale;
    CGFloat yRightRect = ((yResRRect - rsd.ynResWin) * rsd.picScale / (rsd.ScaleSnapImage / rsd.ScaleBigWnd) + rsd.cropY) / (rsd.Scale * (rsd.ScaleBigWnd / rsd.ScaleSnapImage));

    // учитываем сдвижку
    xLeftRect += dxOffset;
    yLeftRect += dyOffset;
    xRightRect += dxOffset;
    yRightRect += dyOffset;
    
    // cохраняем положение Wrap маркеров по вертикали
    int iPhoto = farPdMode.boolValue ? 0 : 2;
    double ysrcLeftRect, ysrcRightRect, xLeftWrap, yLeftWrap, xRightWrap, yRightWrap;
    double dyLeftWrap, dyRightWrap;
    ysrcLeftRect = [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_RECT_Y : NEAR_LEFT_RECT_Y] floatValue];
    ysrcRightRect = [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_RECT_Y : NEAR_RIGHT_RECT_Y] floatValue];
    frameMarkers.GetMarkersPosition(iPhoto, true, 0, xRightWrap, yRightWrap, false);
    frameMarkers.GetMarkersPosition(iPhoto, false, 2, xLeftWrap, yLeftWrap, false);
    dyLeftWrap = yLeftRect - ysrcLeftRect;
    dyRightWrap = yRightRect - ysrcRightRect;
    frameMarkers.SetMarkersPosition(iPhoto, true, 0, 0., yRightWrap - dyRightWrap, false);
    frameMarkers.SetMarkersPosition(iPhoto, false, 2, 0., yLeftWrap - dyLeftWrap, false);
    
    // сохраняем положения прямоугольников в координатах окна
    [measurement setValue:[NSNumber numberWithFloat:xLeftRect] forKey:farPdMode.boolValue ? FAR_LEFT_RECT_X : NEAR_LEFT_RECT_X];
    [measurement setValue:[NSNumber numberWithFloat:yLeftRect] forKey:farPdMode.boolValue ? FAR_LEFT_RECT_Y : NEAR_LEFT_RECT_Y];
    [measurement setValue:[NSNumber numberWithFloat:xRightRect] forKey:farPdMode.boolValue ? FAR_RIGHT_RECT_X : NEAR_RIGHT_RECT_X];
    [measurement setValue:[NSNumber numberWithFloat:yRightRect] forKey:farPdMode.boolValue ? FAR_RIGHT_RECT_Y : NEAR_RIGHT_RECT_Y];
}

- (void)updateView:(UIScrollView *)scrollView
{
    currentZoomScale = scrollView.zoomScale;
    eyeArea.currentZoomScale = currentZoomScale;
    scrollOffsetX = scrollView.contentOffset.x;
    scrollOffsetY = scrollView.contentOffset.y;
    eyeArea.scrollOffsetX = scrollOffsetX;
    eyeArea.scrollOffsetY = scrollOffsetY;
    [self refresh];
    [self configureControlViewFrames];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self updateView:scrollView];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updateView:scrollView];
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return scrollContentView;
}

#pragma mark - Hard Frame Option

- (void)changeHardFrameOption:(UISwitch *)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:sender.on forKey:@"hard_frame"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [self refresh];
}

#pragma mark - Use Head Rotation Methods

- (IBAction)switchRotation:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:(rotation ? NO : YES) forKey:@"use_rotation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (rotation)
    {
        [headRotationCorrection setTitle:NSLocalizedString(@"On", @"") forState:UIControlStateNormal];
        [headRotationCorrection setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [headRotationCorrection setBackgroundImage:[UIImage imageNamed:@"PDWrapBtn"] forState:UIControlStateNormal];
    }
    else
    {
        [headRotationCorrection setTitle:NSLocalizedString(@"Off", @"") forState:UIControlStateNormal];
        [headRotationCorrection setTitleColor:[UIColor colorWithRed:94.0/255.0 green:99.0/255.0 blue:111.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [headRotationCorrection setBackgroundImage:[UIImage imageNamed:@"PDWrapBtnOff"] forState:UIControlStateNormal];
    }
    
    [self refresh];
}

#pragma mark - Custom Methods

// функция переводит размер в экранных пикселях в миллиметры на основании размеров Суппорта
- (double)convertToCM:(double)pixel
{
    double length = 0.;
    int iPhoto = farPdMode.boolValue ? 0 : 2;
    length = Results.convertToCM(iPhoto, pixel);
    return length;
}

// вычисление расстояния съемки до оправы
- (double)PhotoDistance
{
    double Dist = 0;
    CPDMDataResults DataPhoto;
    int iPhoto = farPdMode.boolValue ? 0 : 2;
    if(Results.CalcPhotoResults(iPhoto)) {
        DataPhoto = Results.GetPhotoResults(iPhoto);
        // Dist - photo distance
        Dist = DataPhoto.Dist;
    }
    return Dist;
}

#pragma mark - Refresh

- (void)refresh
{
    if (initialInitPositionsAndFrames) {
        [self SaveEditResult];
    }
    //-----------------------------------------------------------------------------
    
    Results.SetHeadRotationCorrection(rotation);
    CPDMDataResults2 Data;
    CPDMDataResults DataPhoto;
    
    int iPhoto = farPdMode.boolValue ? 0 : 2;
    
    [self setPhotoData];
    
    if (![PDMDataManager sharedManager].appModeVD) {
    } else {
        
        // FRED type
        PDMSupportType typeUsedSupport = (PDMSupportType)[PDM_DATA getCurrentSessionSupportType];
        
        Results.SetPhotoData(1,
                             theNewIPad ? niPadNew : niPad,
                             farPdMode.boolValue ? nFarPD : nNearPD,
                             [[measurement objectForKey:VD_LEFT_SUPPORT_X] floatValue] - dxOffset,
                             [[measurement objectForKey:VD_LEFT_SUPPORT_Y] floatValue] - dyOffset,
                             [[measurement objectForKey:VD_RIGHT_SUPPORT_X] floatValue] - dxOffset,
                             [[measurement objectForKey:VD_RIGHT_SUPPORT_Y] floatValue] - dyOffset,
                             [[measurement objectForKey:VD_CENTER_SUPPORT_X] floatValue] - dxOffset,
                             [[measurement objectForKey:VD_CENTER_SUPPORT_Y] floatValue] - dyOffset,
                             [[measurement objectForKey:VD_RIGHT_EYE_X] floatValue] - dxOffset,
                             [[measurement objectForKey:VD_RIGHT_EYE_Y] floatValue] - dyOffset,
                             [[measurement objectForKey:VD_LEFT_EYE_X] floatValue] - dxOffset,
                             [[measurement objectForKey:VD_LEFT_EYE_Y] floatValue] - dyOffset,
                             [[measurement objectForKey:VD_LEFT_RECT_X] floatValue] - dxOffset,
                             [[measurement objectForKey:VD_LEFT_RECT_Y] floatValue] - dyOffset,
                             [[measurement objectForKey:VD_RIGHT_RECT_X] floatValue] - dxOffset,
                             [[measurement objectForKey:VD_RIGHT_RECT_Y] floatValue] - dyOffset,
                             [[measurement objectForKey:VD_LEFT_RECT_WIDTH] floatValue],
                             [[measurement objectForKey:VD_LEFT_RECT_HEIGHT] floatValue],
                             [[measurement objectForKey:VD_RIGHT_RECT_WIDTH] floatValue],
                             [[measurement objectForKey:VD_RIGHT_RECT_HEIGHT] floatValue],
                             [[measurement objectForKey:VD_RIGHT_SCREEN_POINT_X] floatValue] - dxOffset,
                             [[measurement objectForKey:VD_RIGHT_SCREEN_POINT_Y] floatValue] - dyOffset,
                             [[measurement objectForKey:VD_LEFT_SCREEN_POINT_X] floatValue] - dxOffset,
                             [[measurement objectForKey:VD_LEFT_SCREEN_POINT_Y] floatValue] - dyOffset,
                             0.,
                             0.,
                             0.,
                             0.,
                             [[measurement objectForKey:VD_SNAPPED_ANGLE] floatValue],
                             typeUsedSupport);
    }
    
    // Set Wrap angle
    if (PDMDataManager.sharedManager.topWrapValue) {
        double dblWrapAngle = (kHardFrame)? PDMDataManager.sharedManager.topWrapValue.doubleValue : PDMDataManager.sharedManager.topWrapValue.doubleValue - SEMWRAP;
        Results.SetWrapAngle(false, dblWrapAngle * M_PI / 180.);
    }
    
    // Personalized data
    BOOL results2IsExiting = Results.CalcPhotoResults2(true);
    if(results2IsExiting) {
        Data = Results.GetPhotoResults2();
        [VDLabel setHidden:NO];
        [VDTextField setHidden:NO];
        [VDTextField setBackgroundColor:[UIColor clearColor]];

#ifndef HIDE_WRAP_INFORMATION
        
        if (PDM_DATA.topWrapValue) {
            double dblWrapAngle = (kHardFrame)? PDM_DATA.topWrapValue.doubleValue : PDM_DATA.topWrapValue.doubleValue - SEMWRAP;
            wrapTextField.text = [NSString stringWithFormat:@"%.1lf°", ([PD_MANAGER inExpertMode] ? dblWrapAngle : [PD_MANAGER roundToHalf:dblWrapAngle])];
        }
        else
            wrapTextField.text = @"";
#endif
        VDTextField.text = [NSString stringWithFormat:@"%.1lf", ([PD_MANAGER inExpertMode] ? fabs(Data.LeftVD) : [PD_MANAGER roundToHalf:Data.LeftVD])];
        
        double Upfit = Results.GetCorrectedUpfit(2.5);
        if (Upfit > 0.) {
            UpfitNearPD.text = [NSString stringWithFormat:@"%.1lf", ([PD_MANAGER inExpertMode] ? fabs(Upfit) : [PD_MANAGER roundToHalf:Upfit])];
        }
        else
            UpfitNearPD.text = @"";
        if (Data.LeftInset > 0.) {
            insertLField.text = [NSString stringWithFormat:@"%.1lf", Data.LeftInset];
            insertRField.text = [NSString stringWithFormat:@"%.1lf", Data.RightInset];
        }
        else {
            insertLField.text = @"";
            insertRField.text = @"";
        }
    }
    
    else
    {
        if (PDM_DATA.topWrapValue) {
            double dblWrapAngle = (kHardFrame)? PDM_DATA.topWrapValue.doubleValue : PDM_DATA.topWrapValue.doubleValue - SEMWRAP;
            wrapTextField.text = [NSString stringWithFormat:@"%.1lf°", ([PD_MANAGER inExpertMode] ? dblWrapAngle : [PD_MANAGER roundToHalf:dblWrapAngle])];
        }
        else
            wrapTextField.text = @"";
        
        VDTextField.text = @"";
        UpfitNearPD.text = @"";
    }
    
    if (SHOW_DEVICE_ANGLE) {
        DATextField.text = [NSString stringWithFormat:@"%.1lf°", [[measurement objectForKey:farPdMode.boolValue ? FAR_SNAPPED_ANGLE : NEAR_SNAPPED_ANGLE] floatValue]];
        DAzTextField.text = [NSString stringWithFormat:@"%.1lf°", [[measurement objectForKey:farPdMode.boolValue ? FAR_SNAPPED_ANGLE_Z : NEAR_SNAPPED_ANGLE_Z] floatValue]];
    }
    
    // Far vision data
    if(Results.CalcPhotoResults(0))
    {
        DataPhoto = Results.GetPhotoResults(0);
                
        double leftHeightValue = ([PD_MANAGER inExpertMode] ? DataPhoto.LeftHeight + [self getBevelValue] : [PD_MANAGER roundToHalf:DataPhoto.LeftHeight] + [self getBevelValue]);
        double rightHeightValue = ([PD_MANAGER inExpertMode] ? DataPhoto.RightHeight + [self getBevelValue] : [PD_MANAGER roundToHalf:DataPhoto.RightHeight + [self getBevelValue]]);
        leftPD.text = [NSString stringWithFormat:@"%.1f", ([PD_MANAGER inExpertMode] ? DataPhoto.LeftPD : [PD_MANAGER roundToHalf:DataPhoto.LeftPD])];
        rightPD.text = [NSString stringWithFormat:@"%.1f", ([PD_MANAGER inExpertMode] ? DataPhoto.RightPD : [PD_MANAGER roundToHalf:DataPhoto.RightPD])];
        leftHeight.text = [NSString stringWithFormat:@"%.1f", leftHeightValue];
        rightHeight.text = [NSString stringWithFormat:@"%.1f", rightHeightValue];
        totalPDField.text = [NSString stringWithFormat:@"%.1f", ([PD_MANAGER inExpertMode] ? DataPhoto.LeftPD : [PD_MANAGER roundToHalf:DataPhoto.LeftPD]) + ([PD_MANAGER inExpertMode] ? DataPhoto.RightPD : [PD_MANAGER roundToHalf:DataPhoto.RightPD])];
        
        frameWidthTextField.text = [NSString stringWithFormat:@"%.1lf", ([PD_MANAGER inExpertMode] ? DataPhoto.FrameWidth : [PD_MANAGER roundToHalf:DataPhoto.FrameWidth])];
        frameHeightTextField.text = [NSString stringWithFormat:@"%.1lf", ([PD_MANAGER inExpertMode] ? DataPhoto.FrameHeight : [PD_MANAGER roundToHalf:DataPhoto.FrameHeight])];
        bridge.text = [NSString stringWithFormat:@"%.1f", ([PD_MANAGER inExpertMode] ? DataPhoto.Bridge : [PD_MANAGER roundToHalf:DataPhoto.Bridge])];
        
        headPosture.text = [NSString stringWithFormat:@"%.1f°", ([PD_MANAGER inExpertMode] ? (DataPhoto.alfaPantoskHor * 180. / M_PI) : [PD_MANAGER roundToHalf:(DataPhoto.alfaPantoskHor * 180. / M_PI)])];
        
        [self configureStaticViewWithPDR:rightPD.text
                                     PDL:leftPD.text
                                     FHR:rightHeight.text
                                     FHL:leftHeight.text
                                     DBL:bridge.text];
        
        [PDM_DATA setCurrentFrameWidth:([PD_MANAGER inExpertMode] ? DataPhoto.FrameWidth : [PD_MANAGER roundToHalf:DataPhoto.FrameWidth])
                                height:([PD_MANAGER inExpertMode] ? DataPhoto.FrameHeight : [PD_MANAGER roundToHalf:DataPhoto.FrameHeight])
                                bridge:([PD_MANAGER inExpertMode] ? DataPhoto.Bridge : [PD_MANAGER roundToHalf:DataPhoto.Bridge])];
        // Befocal
        double heightBifocal = (leftHeightValue + rightHeightValue) / 2. - 5.5;
        bifocalRField.text = [NSString stringWithFormat:@"%.1f", heightBifocal];
    }
    else {
        leftPD.text = @"";
        rightPD.text = @"";
        leftHeight.text = @"";
        rightHeight.text = @"";
        
        frameWidthTextField.text = @"";
        frameHeightTextField.text = @"";
        bridge.text = @"";
        headPosture.text = @"";
    }
    
    // set diameter
    if (Results.GetDiameterStatus()) {
        if (Results.GetProgressiveDiameterStatus()) {
            diametrRField.text = [NSString stringWithFormat:@"%.0lf/%.0lf", Results.GetDiameter(false), Results.GetDiameter(false) + 5];
            diametrLField.text = [NSString stringWithFormat:@"%.0lf/%.0lf", Results.GetDiameter(true), Results.GetDiameter(true) + 5];
        }
        else {
            diametrRField.text = [NSString stringWithFormat:@"%.0lf", Results.GetDiameter(false)];
            diametrLField.text = [NSString stringWithFormat:@"%.0lf", Results.GetDiameter(true)];
        }
    }
    
    // Near vision data
    if(Results.CalcPhotoResults(2))
    {
        DataPhoto = Results.GetPhotoResults(2);
        
        leftNearPD.text = [NSString stringWithFormat:@"%.1f", ([PD_MANAGER inExpertMode] ? DataPhoto.LeftPD : [PD_MANAGER roundToHalf:DataPhoto.LeftPD])];
        rightNearPD.text = [NSString stringWithFormat:@"%.1f", ([PD_MANAGER inExpertMode] ? DataPhoto.RightPD : [PD_MANAGER roundToHalf:DataPhoto.RightPD])];
        distanceToIPadTextField.text = [NSString stringWithFormat:@"%.1lf", ([PD_MANAGER inExpertMode] ? (DataPhoto.Dist / 10.f) : [PD_MANAGER roundToHalf:(DataPhoto.Dist / 10.f)])];
        
        if (!pdModeSegmentedControl.selectedSegmentIndex)
        {
            [self configureStaticViewWithPDR:rightNearPD.text
                                         PDL:leftNearPD.text
                                         FHR:[NSString stringWithFormat:@"%.1f", ([PD_MANAGER inExpertMode] ? DataPhoto.RightHeight + [self getBevelValue] : [PD_MANAGER roundToHalf:DataPhoto.RightHeight])]
                                         FHL:[NSString stringWithFormat:@"%.1f", ([PD_MANAGER inExpertMode] ? DataPhoto.LeftHeight + [self getBevelValue] : [PD_MANAGER roundToHalf:DataPhoto.LeftHeight])]
                                         DBL:[NSString stringWithFormat:@"%.1f", ([PD_MANAGER inExpertMode] ? DataPhoto.Bridge : [PD_MANAGER roundToHalf:DataPhoto.Bridge])]];
        }
    }
    else {
        leftNearPD.text = @"";
        rightNearPD.text = @"";
        distanceToIPadTextField.text = @"";
    }
    
    // Visual Controller data
    if(Results.CalcPhotoResults(iPhoto)) {
        DataPhoto = Results.GetPhotoResults(iPhoto);
        
        [leftEyeView setEyePD:DataPhoto.LeftPD];
        [rightEyeView setEyePD:DataPhoto.RightPD];
        [leftEyeView setEyeHeight:DataPhoto.LeftHeight + [self getBevelValue]];
        [rightEyeView setEyeHeight:DataPhoto.RightHeight + [self getBevelValue]];
        
        if (initialInitPositionsAndFrames) {
            [eyeArea setLeftPDString:[NSString stringWithFormat:@"%.1f", DataPhoto.LeftPD]];
            [eyeArea setLeftHeightString:[NSString stringWithFormat:@"%.1f", DataPhoto.LeftHeight + [self getBevelValue]]];
            [eyeArea setRightPDString:[NSString stringWithFormat:@"%.1f", DataPhoto.RightPD]];
            [eyeArea setRightHeightString:[NSString stringWithFormat:@"%.1f", DataPhoto.RightHeight + [self getBevelValue]]];
            [eyeArea setBridgeString:[NSString stringWithFormat:@"%.1f", DataPhoto.Bridge]];
            [eyeArea refresh];
        }
        initialInitPositionsAndFrames = YES;
        
        // alfaTilt - угол наклона головы "вправо - влево"
        double alfaTilt = DataPhoto.alfaTilt;
        // alfaPan - угол поворота головы "вправо - влево"
        double alfaPan = DataPhoto.alfaPan;
        // alfaPantosk - пантоскопический угол
        double alfaPantosk = DataPhoto.alfaPantosk;
        // alfaPantoskHor - пантоскопический угол относительно отвеса
        //double alfaPantoskHor = DataPhoto.alfaPantoskHor;
        
        
        if (!farPdMode.boolValue) {
            CGFloat xRightSupport = [[measurement objectForKey:NEAR_RIGHT_SUPPORT_X] floatValue];
            CGFloat yRightSupport = [[measurement objectForKey:NEAR_RIGHT_SUPPORT_Y] floatValue];
            
            CGPoint rightPoint = CGPointMake([eyeArea rightEye].x, [eyeArea rightEye].y);
            CGPoint leftPoint = CGPointMake([eyeArea leftEye].x, [eyeArea leftEye].y);
            
            CGFloat distToRightEyeX = (rightPoint.x - (xRightSupport - rsd.cropX)) / cos(alfaPan) / cos(alfaTilt);
            CGFloat distToRightEyeY = (rightPoint.y - (yRightSupport - rsd.cropY)) * cos(alfaTilt) / cos(alfaPantosk);
            CGFloat distToLeftEyeX = (leftPoint.x - (xRightSupport - rsd.cropX)) / cos(alfaPan) / cos(alfaTilt);
            CGFloat distToLeftEyeY = (leftPoint.y - (yRightSupport - rsd.cropY)) * cos(alfaTilt) / cos(alfaPantosk);
            
            int iPhoto = farPdMode.boolValue ? 0 : 2;
            double onePixelInMM =  Results.convertToCM(iPhoto, 1.0) / rsd.rectScale;
            
            [measurement setValue:[NSNumber numberWithFloat:distToRightEyeX * onePixelInMM] forKey:NEAR_DIST_TO_RIGHT_X];
            [measurement setValue:[NSNumber numberWithFloat:distToRightEyeY * onePixelInMM] forKey:NEAR_DIST_TO_RIGHT_Y];
            [measurement setValue:[NSNumber numberWithFloat:distToLeftEyeX * onePixelInMM] forKey:NEAR_DIST_TO_LEFT_X];
            [measurement setValue:[NSNumber numberWithFloat:distToLeftEyeY * onePixelInMM] forKey:NEAR_DIST_TO_LEFT_Y];
            
            for (UIView *crossView in crossesView.subviews) {
                if (crossView.tag == CROSS_IMAGEVIEW_TAG_RIGHT) {
                    [crossView removeFromSuperview];
                } else if (crossView.tag == CROSS_IMAGEVIEW_TAG_LEFT) {
                    [crossView removeFromSuperview];
                }
            }
        } else if ([measurement objectForKey:NEAR_DIST_TO_RIGHT_X] != nil) {
            // определяем число мм в одном пикселе
            int iPhoto = farPdMode.boolValue ? 0 : 2;
            double onePixelInMM_right_y =  Results.convertToCM(iPhoto, 1.0) / rsd.rectScale;
            double onePixelInMM_left_y =  Results.convertToCM(iPhoto, 1.0) / rsd.rectScale;
            double onePixelInMM_x = onePixelInMM_right_y;
            if (iPhoto == 0) {
                CPDMPhotoData PhData =  Results.GetPhotoData(iPhoto);
                if (PhData.nPhotoMode != nNoData) {
                    CPDMDataResults PhResults = Results.GetPhotoResults(iPhoto);
                    if(PhResults.bDataResults) {
                        onePixelInMM_right_y = (PhResults.RightHeight / (PhData.yRightRect - PhData.yRightEye)) / rsd.rectScale;
                        onePixelInMM_left_y = (PhResults.LeftHeight / (PhData.yLeftRect - PhData.yLeftEye)) / rsd.rectScale;
                        onePixelInMM_x = ((PhResults.LeftPD + PhResults.RightPD) / (PhData.xLeftEye - PhData.xRightEye)) / rsd.rectScale;
                    }
                }
            }
            
            // находим смещение в мм зрачков в разных режимах
            double dxLeft = 0., dyLeft = 0., dxRight = 0., dyRight = 0.;
            CPDMDataResults farData = Results.GetPhotoResults(0);
            CPDMDataResults nearData = Results.GetPhotoResults(2);
            if(farData.bDataResults && nearData.bDataResults) {
                dxLeft = farData.LeftPD - nearData.LeftPD;
                dyLeft = fmax(nearData.LeftHeight, 2.5) - farData.LeftHeight;
                dxRight = nearData.RightPD - farData.RightPD;
                dyRight = fmax(nearData.RightHeight, 2.5) - farData.RightHeight;
            }
            double dxAverage = (dxLeft - dxRight) / 2.;
            double dyAverage = (dyLeft + dyRight) / 2.;
            
            // ставим крестики
            CGRect rightFrame = CGRectMake([eyeArea rightEye].x + dxAverage / onePixelInMM_x - 3.5 + nearPdOffsetX, [eyeArea rightEye].y - dyAverage / onePixelInMM_right_y - 3.5 + nearPdOffsetY, 9, 9);
            CGRect leftFrame = CGRectMake([eyeArea leftEye].x - dxAverage / onePixelInMM_x - 3.5 + nearPdOffsetX, [eyeArea leftEye].y - dyAverage / onePixelInMM_left_y - 3.5 + nearPdOffsetY, 9, 9);
            
            BOOL crossesOnView = NO;
            for (UIView *crossView in crossesView.subviews) {
                if (crossView.tag == CROSS_IMAGEVIEW_TAG_RIGHT) {
                    [crossView setHidden:NO];
                    [crossView setFrame:rightFrame];
                } else if (crossView.tag == CROSS_IMAGEVIEW_TAG_LEFT) {
                    [crossView setHidden:NO];
                    [crossView setFrame:leftFrame];
                    crossesOnView = YES;
                }
            }
            
            if (!crossesOnView) {
                UIImageView *crossImageViewRight = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"RedCross9"]];
                crossImageViewRight.frame = rightFrame;
                crossImageViewRight.tag = CROSS_IMAGEVIEW_TAG_RIGHT;
                UIImageView *crossImageViewLeft = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"RedCross9"]];
                crossImageViewLeft.frame = leftFrame;
                crossImageViewLeft.tag = CROSS_IMAGEVIEW_TAG_LEFT;
                [crossImageViewLeft setHidden:YES];
                [crossImageViewRight setHidden:YES];
                [crossesView addSubview:crossImageViewLeft];
                [crossesView addSubview:crossImageViewRight];
            }
        }
    }
    
    
    [self setHeadRotateImage];
}

#pragma mark - Configure Static View

- (void)configureStaticViewWithPDR:(NSString *)pdR
                               PDL:(NSString *)pdL
                               FHR:(NSString *)fhR
                               FHL:(NSString *)fhL
                               DBL:(NSString *)dbL
{
    PDR.text = pdR;
    PDL.text = pdL;
    FHR.text = fhR;
    FHL.text = fhL;
    DBL.text = dbL;
}

- (void)configureStaticCorridors
{
    int shift = (int)(defaultProgressiveDesign - popoverProgressiveType.shiftValue) - 4;
    
    staticCorridScheme.frame = CGRectMake(staticCorridScheme.frame.origin.x,
                                          staticDesignScheme.frame.origin.y + shift * 2.f,
                                          staticCorridScheme.frame.size.width,
                                          staticCorridScheme.frame.size.height);
}

#pragma mark -

- (void)loadPlistInDocuments
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *errorFileManager = nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentsPlistPath = [documentsDirectory stringByAppendingPathComponent:@"Results.plist"];
    
    BOOL success = [fileManager fileExistsAtPath:documentsPlistPath];
    
    if (!success) {
        NSString *resourcePlistPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Results.plist"];
        [fileManager removeItemAtPath:documentsPlistPath error:&errorFileManager];
        success = [fileManager copyItemAtPath:resourcePlistPath toPath:documentsPlistPath error:&errorFileManager];
        
        if (!success) {
            NSLog(@"Failed to copy plist file with message '%@'.", [errorFileManager localizedDescription]);
        }
    } 
}

#pragma mark Save All Results Data in XML

- (NSString *)getDeviceType
{
    PDDeviceDetails details = [[PDDeviceInfo new] deviceDetails];
    
    return details.modelString;
}

- (float)recalculateValue:(float)value withSize:(float)photoSize forWidth:(BOOL)isWidth
{
    float resultValue = 0.0;
    
    if (isWidth)
        resultValue = value * photoSize / 1238;
    else
        resultValue = value * photoSize / 768;
    
    return resultValue;
}

- (NSString *)generateExportName
{
    CFUUIDRef udid = CFUUIDCreate(NULL);
    
    return (NSString *) CFBridgingRelease(CFUUIDCreateString(NULL, udid));
}

- (void)saveResultsToResources:(BOOL)bSendToFtp
{
    [self loadPlistInDocuments];
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSString *plistPathInResources;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                              NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:@"Results.plist"];
    plistPathInResources = [[NSBundle mainBundle] pathForResource:@"Results" ofType:@"plist"];
    
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization
                                          propertyListFromData:plistXML
                                          mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                          format:&format
                                          errorDescription:&errorDesc];
    if (!temp) {
        NSLog(@"Error reading plist: %@, format: %lu", errorDesc, (unsigned long)format);
    }
    
    results = [NSMutableDictionary dictionaryWithContentsOfFile: plistPath];
    
    NSString *resultFarPdModeKey = FAR_PD_XML_KEY;
    NSString *resultNearPdModeKey = NEAR_PD_XML_KEY;
    NSMutableDictionary *FarPdModeResults = [results objectForKey:resultFarPdModeKey];
    NSMutableDictionary *NearPdModeResults = [results objectForKey:resultNearPdModeKey];
    
    BOOL farExists = NO;
    
    NSString *farPDExportImageName;
    NSString *nearPDExportImageName;
    
    float farImageWidth =   [PDMDataManager sharedManager].farPDImageRotated.size.width;
    float farImageHeight =  [PDMDataManager sharedManager].farPDImageRotated.size.height;
    float nearImageWidth =  [PDMDataManager sharedManager].nearPDImageRotated.size.width;
    float nearImageHeight = [PDMDataManager sharedManager].nearPDImageRotated.size.height;
    
    CPDMDataResults DataPhoto;
    int iPhoto = 0; // Far PD mode
    
    if(Results.CalcPhotoResults(iPhoto)) {
        DataPhoto = Results.GetPhotoResults(iPhoto);
        
        if (FarPdModeResults == nil) {
            FarPdModeResults = [NSMutableDictionary dictionary];
            [results setObject:FarPdModeResults forKey:FAR_PD_XML_KEY];
        }
        
        farExists = YES;
        
        // Additional data for xml
        
        [FarPdModeResults setObject:[self getDeviceType] forKey:@"iPadType"];
        
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_LEFT_SUPPORT_X] floatValue] withSize:farImageWidth forWidth:YES]] forKey:@"marker_support_left_x"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_LEFT_SUPPORT_Y] floatValue] withSize:farImageHeight forWidth:NO]] forKey:@"marker_support_left_y"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_CENTER_SUPPORT_X] floatValue] withSize:farImageWidth forWidth:YES]] forKey:@"marker_support_center_x"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_CENTER_SUPPORT_Y] floatValue] withSize:farImageHeight forWidth:NO]] forKey:@"marker_support_center_y"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_RIGHT_SUPPORT_X] floatValue] withSize:farImageWidth forWidth:YES]] forKey:@"marker_support_right_x"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_RIGHT_SUPPORT_Y] floatValue] withSize:farImageHeight forWidth:NO]] forKey:@"marker_support_right_y"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_LEFT_EYE_X] floatValue] withSize:farImageWidth forWidth:YES]] forKey:@"marker_eye_left_x"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_LEFT_EYE_Y] floatValue] withSize:farImageHeight forWidth:NO]] forKey:@"marker_eye_left_y"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_RIGHT_EYE_X] floatValue] withSize:farImageWidth forWidth:YES]] forKey:@"marker_eye_right_x"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_RIGHT_EYE_Y] floatValue] withSize:farImageHeight forWidth:NO]] forKey:@"marker_eye_right_y"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_LEFT_RECT_X] floatValue] withSize:farImageWidth forWidth:YES]] forKey:@"marker_frame_left_x"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_LEFT_RECT_Y] floatValue] withSize:farImageHeight forWidth:NO]] forKey:@"marker_frame_left_y"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_RIGHT_SCREEN_POINT_X] floatValue] withSize:farImageWidth forWidth:YES]] forKey:@"marker_frame_up_left_x"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_RIGHT_SCREEN_POINT_Y] floatValue] withSize:farImageHeight forWidth:NO]] forKey:@"marker_frame_up_left_y"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_RIGHT_RECT_X] floatValue] withSize:farImageWidth forWidth:YES]] forKey:@"marker_frame_right_x"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_RIGHT_RECT_Y] floatValue] withSize:farImageHeight forWidth:NO]] forKey:@"marker_frame_right_y"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_LEFT_SCREEN_POINT_X] floatValue] withSize:farImageWidth forWidth:YES]] forKey:@"marker_frame_up_right_x"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:FAR_LEFT_SCREEN_POINT_Y] floatValue] withSize:farImageHeight forWidth:NO]] forKey:@"marker_frame_up_right_y"];
        
        // Wrap Markers
        
        double xLeftWrap, yLeftWrap, xRightWrap, yRightWrap;
        
        frameMarkers.GetMarkersPosition(0, true, 0, xRightWrap, yRightWrap, false);
        frameMarkers.GetMarkersPosition(0, false, 2, xLeftWrap, yLeftWrap, false);
        
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", yLeftWrap] forKey:@"marker_wrap_left_y"];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", yRightWrap] forKey:@"marker_wrap_right_y"];
        
        // Flash Type
        
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%u", Results.GetFlashType()] forKey:@"flash_type"];
        
        // Param Fcoef
        
        double fcoef = Results.GetFcoef(0) * farImageWidth / 1238;
        
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", fcoef] forKey:@"far_Fcoef"];
        
        // Get image name for export if FTP
        
        if (bSendToFtp) {
            farPDExportImageName = [self generateExportName];
            [FarPdModeResults setObject:[NSString stringWithFormat:@"%@.png", farPDExportImageName] forKey:@"far_pd_photo"];
        }
        
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.LeftPD] forKey:LEFT_PD_XML_KEY];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.RightPD] forKey:RIGHT_PD_XML_KEY];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.LeftHeight + [self getBevelValue]] forKey:LEFT_HEIGHT_XML_KEY];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.RightHeight + [self getBevelValue]] forKey:RIGHT_HEIGHT_XML_KEY];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.Bridge] forKey:BRIDGE_XML_KEY];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.FrameWidth] forKey:FRAME_WIDTH_XML_KEY];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.FrameHeight] forKey:FRAME_HEIGHT_XML_KEY];
        
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [[measurement objectForKey:FAR_SNAPPED_ANGLE] floatValue]] forKey:IPAD_ANGLE_XML_KEY];
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.Dist] forKey:DISTANCE_TO_IPAD_XML_KEY];
        
        if ([FarPdModeResults objectForKey:INITIALIZED_XML_KEY] != nil) {
            [FarPdModeResults removeObjectForKey:INITIALIZED_XML_KEY];
        }
        
        if(Results.CalcPhotoResults2(true)) {
            CPDMDataResults2 Data;
            Data = Results.GetPhotoResults2();
            
            [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", Data.LeftVD] forKey:VERTEX_DISTANCE_XML_KEY];
        }
        
        if (PDMDataManager.sharedManager.topWrapValue) {
            double dblWrapAngle = (kHardFrame)? PDM_DATA.topWrapValue.doubleValue : PDM_DATA.topWrapValue.doubleValue - SEMWRAP;
            [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1lf", dblWrapAngle] forKey:WRAP_ANGLE_XML_KEY];
        }
        
        [FarPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.alfaPantoskHor * 180. / M_PI] forKey:PANTO_XML_KEY];
    }
    
    iPhoto = 2; // Ner PD mode
    
    if(Results.CalcPhotoResults(iPhoto)) {
        DataPhoto = Results.GetPhotoResults(iPhoto);
        
        if (NearPdModeResults == nil) {
            NearPdModeResults = [NSMutableDictionary dictionary];
            [results setObject:NearPdModeResults forKey:NEAR_PD_XML_KEY];
        }
        
        // Additional data for xml
        
        [NearPdModeResults setObject:[self getDeviceType] forKey:@"iPadType"];
        
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:NEAR_LEFT_SUPPORT_X] floatValue] withSize:nearImageWidth forWidth:YES]] forKey:@"marker_support_left_x"];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:NEAR_LEFT_SUPPORT_Y] floatValue] withSize:nearImageHeight forWidth:NO]] forKey:@"marker_support_left_y"];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:NEAR_CENTER_SUPPORT_X] floatValue] withSize:nearImageWidth forWidth:YES]] forKey:@"marker_support_center_x"];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:NEAR_CENTER_SUPPORT_Y] floatValue] withSize:nearImageHeight forWidth:NO]] forKey:@"marker_support_center_y"];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:NEAR_RIGHT_SUPPORT_X] floatValue] withSize:nearImageWidth forWidth:YES]] forKey:@"marker_support_right_x"];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:NEAR_RIGHT_SUPPORT_Y] floatValue] withSize:nearImageHeight forWidth:NO]] forKey:@"marker_support_right_y"];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:NEAR_LEFT_EYE_X] floatValue] withSize:nearImageWidth forWidth:YES]] forKey:@"marker_eye_left_x"];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:NEAR_LEFT_EYE_Y] floatValue] withSize:nearImageHeight forWidth:NO]] forKey:@"marker_eye_left_y"];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:NEAR_RIGHT_EYE_X] floatValue] withSize:nearImageWidth forWidth:YES]] forKey:@"marker_eye_right_x"];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:NEAR_RIGHT_EYE_Y] floatValue] withSize:nearImageHeight forWidth:NO]] forKey:@"marker_eye_right_y"];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:NEAR_LEFT_RECT_X] floatValue] withSize:nearImageWidth forWidth:YES]] forKey:@"marker_frame_left_x"];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:NEAR_LEFT_RECT_Y] floatValue] withSize:nearImageHeight forWidth:NO]] forKey:@"marker_frame_left_y"];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:NEAR_RIGHT_RECT_X] floatValue] withSize:nearImageWidth forWidth:YES]] forKey:@"marker_frame_right_x"];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [self recalculateValue:[[measurement objectForKey:NEAR_RIGHT_RECT_Y] floatValue] withSize:nearImageHeight forWidth:NO]] forKey:@"marker_frame_right_y"];
        
        // Wrap Markers
        
        double xLeftWrap, yLeftWrap, xRightWrap, yRightWrap;
        
        frameMarkers.GetMarkersPosition(2, true, 0, xRightWrap, yRightWrap, false);
        frameMarkers.GetMarkersPosition(2, false, 2, xLeftWrap, yLeftWrap, false);
        
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", yLeftWrap] forKey:@"marker_wrap_left_y"];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", yRightWrap] forKey:@"marker_wrap_right_y"];
        
        // Flash Type
        
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%u", Results.GetFlashType()] forKey:@"flash_type"];
        
        // Param Fcoef
        
        double fcoef = Results.GetFcoef(2) * nearImageWidth / 1238;
        
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", fcoef] forKey:@"near_Fcoef"];
        
        // Get image name for export if FTP
        
        if (bSendToFtp) {
            nearPDExportImageName = [self generateExportName];
            [NearPdModeResults setObject:[NSString stringWithFormat:@"%@.png", nearPDExportImageName] forKey:@"near_pd_photo"];
        }
        
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.LeftPD] forKey:LEFT_PD_XML_KEY];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.RightPD] forKey:RIGHT_PD_XML_KEY];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.LeftHeight + [self getBevelValue]] forKey:LEFT_HEIGHT_XML_KEY];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.RightHeight + [self getBevelValue]] forKey:RIGHT_HEIGHT_XML_KEY];
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.Bridge] forKey:BRIDGE_XML_KEY];
        
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", DataPhoto.Dist] forKey:READING_DIST_XML_KEY];
        
        [NearPdModeResults setObject:[NSString stringWithFormat:@"%.1f", [[measurement objectForKey:NEAR_SNAPPED_ANGLE] floatValue]] forKey:IPAD_ANGLE_XML_KEY];
        
        if ([NearPdModeResults objectForKey:INITIALIZED_XML_KEY] != nil) {
            [NearPdModeResults removeObjectForKey:INITIALIZED_XML_KEY];
        }
        
        if (farExists)
            [NearPdModeResults setObject:[NSString stringWithFormat:@"%@", UpfitNearPD.text] forKey:@"Up_fit"];
    }
    
    // remove empty block
    if (FarPdModeResults != nil && ((NSNumber *)[FarPdModeResults objectForKey:LEFT_HEIGHT_XML_KEY]).floatValue == 0.0) {
        [results removeObjectForKey:FAR_PD_XML_KEY];
    }
    if (NearPdModeResults != nil && ((NSNumber *)[NearPdModeResults objectForKey:LEFT_HEIGHT_XML_KEY]).floatValue == 0.0) {
        [results removeObjectForKey:NEAR_PD_XML_KEY];
    }
    
    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:results
                                                                   format:NSPropertyListXMLFormat_v1_0
                                                         errorDescription:nil];
    if(plistData)
    {
        [plistData writeToFile:plistPath atomically:YES];
        
        if (bSendToFtp == YES) {
            
            CPDMDataResults farData = Results.GetPhotoResults(0);
            CPDMDataResults nearData = Results.GetPhotoResults(2);
            
            if (farData.bDataResults) {
                NSString *farPath =  [PDMResultsController saveImage:[PDMDataManager sharedManager].farPDImageRotated withName:@"far_temp.jpg"];
                [self addToUDPSendQueueWithPath:farPath fileName:[NSString stringWithFormat:@"%@.png", farPDExportImageName]];
            }
            
            if (nearData.bDataResults) {
                NSString *nearPath = [PDMResultsController saveImage:[PDMDataManager sharedManager].nearPDImageRotated withName:@"near_temp.jpg"];
                [self addToUDPSendQueueWithPath:nearPath fileName:[NSString stringWithFormat:@"%@.png", nearPDExportImageName]];
            }
            
            [self addToUDPSendQueueWithPath:plistPath fileName:kFTPsendFileName]; // Temporary old name for plist
            
            //[self addToUDPSendQueueWithPath:plistPath fileName:[NSString stringWithFormat:@"%@.plist", [self generateExportName]]];
            [self startFTPSendFromQueue];
        }
    }
}

- (void)setPhotoData
{
    int iPhoto = farPdMode.boolValue ? 0 : 2;
    
    // FRED type
    PDMSupportType typeUsedSupport = (PDMSupportType)[PDM_DATA getCurrentSessionSupportType];
    
    Results.SetPhotoData(iPhoto,
                         theNewIPad ? niPadNew : niPad,
                         farPdMode.boolValue ? nFarPD : nNearPD,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_SUPPORT_X : NEAR_LEFT_SUPPORT_X] floatValue] - dxOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_SUPPORT_Y : NEAR_LEFT_SUPPORT_Y] floatValue] - dyOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_SUPPORT_X : NEAR_RIGHT_SUPPORT_X] floatValue] - dxOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_SUPPORT_Y : NEAR_RIGHT_SUPPORT_Y] floatValue] - dyOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_CENTER_SUPPORT_X : NEAR_CENTER_SUPPORT_X] floatValue] - dxOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_CENTER_SUPPORT_Y : NEAR_CENTER_SUPPORT_Y] floatValue] - dyOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_EYE_X : NEAR_RIGHT_EYE_X] floatValue] - dxOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_EYE_Y : NEAR_RIGHT_EYE_Y] floatValue] - dyOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_EYE_X : NEAR_LEFT_EYE_X] floatValue] - dxOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_EYE_Y : NEAR_LEFT_EYE_Y] floatValue] - dyOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_RECT_X : NEAR_LEFT_RECT_X] floatValue] - dxOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_RECT_Y : NEAR_LEFT_RECT_Y] floatValue] - dyOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_RECT_X : NEAR_RIGHT_RECT_X] floatValue] - dxOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_RECT_Y : NEAR_RIGHT_RECT_Y] floatValue] - dyOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_RECT_WIDTH : NEAR_LEFT_RECT_WIDTH] floatValue],
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_RECT_HEIGHT : NEAR_LEFT_RECT_HEIGHT] floatValue],
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_RECT_WIDTH : NEAR_RIGHT_RECT_WIDTH] floatValue],
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_RECT_HEIGHT : NEAR_RIGHT_RECT_HEIGHT] floatValue],
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_SCREEN_POINT_X : NEAR_RIGHT_SCREEN_POINT_X] floatValue] - dxOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_SCREEN_POINT_Y : NEAR_RIGHT_SCREEN_POINT_Y] floatValue] - dyOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_SCREEN_POINT_X : NEAR_LEFT_SCREEN_POINT_X] floatValue] - dxOffset,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_SCREEN_POINT_Y : NEAR_LEFT_SCREEN_POINT_Y] floatValue] - dyOffset,
                         0.,
                         0.,
                         0.,
                         0.,
                         [[measurement objectForKey:farPdMode.boolValue ? FAR_SNAPPED_ANGLE : NEAR_SNAPPED_ANGLE] floatValue],
                         typeUsedSupport);
}

+ (NSString *)saveImage:(UIImage *)image withName:(NSString *)name
{
    NSData *data = UIImageJPEGRepresentation(image, 1.0);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *fullPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:name];
    [fileManager createFileAtPath:fullPath contents:data attributes:nil];
    
    return fullPath;
}

- (void)startSend:(NSString *)filePath ftpFileName : ( NSString * )ftpFileName
{
    
    BOOL                    success;
    NSURL *                 url;
    assert(filePath != nil);
    
    assert([[NSFileManager defaultManager] fileExistsAtPath:filePath]);
    
    assert(self.networkStream == nil);      // don't tap send twice in a row!
    assert(self.fileStream == nil);         // ditto
    
    // First get and check the URL.
    //m_url = @"192.168.159.1/center";      //for diag
    
    url = [[NetworkManager sharedInstance] smartURLForString:m_url];
    success = (url != nil);
    
    if (success) {
        // Add the last part of the file name to the end of the URL to form the final
        // URL that we're going to put to.
        
        url = CFBridgingRelease(
                                CFURLCreateCopyAppendingPathComponent(NULL, (__bridge CFURLRef) url, (__bridge CFStringRef) ftpFileName, false)
                                );
        success = (url != nil);
    }
    
    // If the URL is bogus, let the user know.  Otherwise kick off the connection.
    
    if ( ! success) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"BBGR EYEMIO"
                              message: @"Invalid URL"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    } else {
        
        // Open a stream for the file we're going to send.  We do not open this stream;
        // NSURLConnection will do it for us.
        
        self.fileStream = [NSInputStream inputStreamWithFileAtPath:filePath];
        //self.fileStream = [NSInputStream inputStreamWithData:[self dataStringFromResults]];
        assert(self.fileStream != nil);
        
        [self.fileStream open];
        
        // Open a CFFTPStream for the URL.
        
        self.networkStream = CFBridgingRelease(
                                               CFWriteStreamCreateWithFTPURL(NULL, (__bridge CFURLRef) url)
                                               );
        assert(self.networkStream != nil);
        
        if ([m_log length] != 0) {
            success = [self.networkStream setProperty:m_log forKey:(id)kCFStreamPropertyFTPUserName];
            assert(success);
            success = [self.networkStream setProperty:m_pas forKey:(id)kCFStreamPropertyFTPPassword];
            assert(success);
        }
        
        self.networkStream.delegate = self;
        [self.networkStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [self.networkStream open];
        
        // Tell the UI we're sending.
        
        [self sendDidStart];
    }
}

- (void)sendPlistViaEmail
{
    MFMailComposeViewController *picker = [MFMailComposeViewController new];
    picker.mailComposeDelegate = self;

    [picker setSubject:NSLocalizedString(@"PD Measurement results", @"")];
    
    NSString *timeStampString;
    {
        NSData *myData = [NSData dataWithContentsOfFile:plistPath];
        NSDate *currentDate = [NSDate date];
        NSDateFormatter *df = [NSDateFormatter new];
        [df setDateFormat:@"dd-MMMM-yyyy"];
        timeStampString = [df stringFromDate:currentDate];
        
        NSString *fileName = [NSString stringWithFormat:@"PD_%@.xml", timeStampString];
        [picker addAttachmentData:myData mimeType:@"text/plain" fileName:fileName];
    }
    
    [editButton setHidden:YES];
    [nearPdShowDesignButton setHidden:YES];
    
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(m_screen)];
    
    [picker addAttachmentData:imageData
                     mimeType:@"image/png"
                     fileName:[NSString stringWithFormat:@"PD_%@", timeStampString]];
    
    [editButton setHidden:NO];
    [nearPdShowDesignButton setHidden:NO];
	
    // Fill out the email body text
    @try {
        [self presentViewController:picker animated:YES completion:nil];
    }
    @catch (NSException *exception) {
        //
    }
    @finally {
        //
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent: {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PD Measurement", @"") message:NSLocalizedString(@"Results sent by email", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil, nil];
            [alertView show];
            break;
        }
        case MFMailComposeResultFailed:
            break;
            
        default:  
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)goToAnotherMode
{
    initialInitPositionsAndFrames = NO;
    [eyeArea removeFromSuperview];
    [self configureResultControllerDidLoad];
    [self configureResultControllerWillAppear];
    [self configureNearPdDesignFrames];
}

- (BOOL)hasNearData
{
    return [PDMDataManager sharedManager].nearPDImage != nil;
}

- (BOOL)hasFarData
{
    return [PDMDataManager sharedManager].farPDImage != nil;
}

- (void)reloadNearData
{
    if ([self hasNearData])
    {
        farPdMode = [NSNumber numberWithBool:NO];
        self.snapImage = [PDMDataManager sharedManager].nearPDImageRotated;
        [self goToAnotherMode];
    }
}

- (void)reloadFarData
{
    if ([self hasFarData])
    {
        farPdMode = [NSNumber numberWithBool:YES];
        self.snapImage = [PDMDataManager sharedManager].farPDImageRotated;
        [self goToAnotherMode];
    }
}

- (void)reloadAllData
{
    [self reloadNearData];
    [self reloadFarData];
}

- (BOOL)canChangeMode
{
    BOOL allDataPresented = [self hasFarData] && [self hasNearData];
    
    return ![PD_MANAGER isLoadedSession] || allDataPresented;
}

#pragma mark - Change PD Mode

- (IBAction)changePDMode:(id)sender
{
    //show new session alert for loaded session
    if (![self canChangeMode])
    {
        [pdModeSegmentedControl setSelectedSegmentIndex:!pdModeSegmentedControl.selectedSegmentIndex];
        
        [self newSessionPressed:nil];
        
        return;
    }
    
    // hide edit
    if (editModeEnabled) {
        [self editResults:nil];
    }
    
    bool bProgressiveStatus = Results.GetProgressiveDiameterStatus();
    if (bProgressiveStatus) {
        [self showNearPdDesign:nearPdShowDesignButton];
    }
    if (farPdMode.boolValue && pdModeSegmentedControl.selectedSegmentIndex == 0)
    {
        if (showCircDiameters)
            [self showDiameters:diametrBtn];
        
        if ([PDMDataManager sharedManager].nearPDImage != nil
            && [[PDMDataManager sharedManager].storage objectForKey:NEAR_LEFT_RECT_X] != nil)
        {
            [PDM_DATA setFarMode:NO];
            
            PDMCameraController *cameraController = [self.navigationController.viewControllers objectAtIndex:0];
            [cameraController setFarMode:@(NO)];
            
            [self reloadNearData];
        }
        else
        {
            PDMCameraController *cameraController = [self.navigationController.viewControllers objectAtIndex:0];
            
            [PDM_DATA setFarMode:NO];
            
            [PD_MANAGER setTargetScreen:PDTargetScreenCamera];
            
            [cameraController setFarMode:@(NO)];
            cameraController.returnedFromResults = YES;
            cameraController.switchToFar = NO;
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    else if (!farPdMode.boolValue && pdModeSegmentedControl.selectedSegmentIndex == 1)
    {
        if ([PDMDataManager sharedManager].farPDImage != nil
            && [[PDMDataManager sharedManager].storage objectForKey:FAR_LEFT_RECT_X] != nil)
        {
            [PDM_DATA setFarMode:YES];
            
            PDMCameraController *cameraController = [self.navigationController.viewControllers objectAtIndex:0];
            [cameraController setFarMode:@(YES)];
            
            [self reloadFarData];
        }
        else
        {
            PDMCameraController *cameraController = [self.navigationController.viewControllers objectAtIndex:0];
            
            [PDM_DATA setFarMode:YES];
            
            [PD_MANAGER setTargetScreen:PDTargetScreenCamera];
            
            [cameraController setFarMode:@(YES)];
            cameraController.returnedFromResults = YES;
            cameraController.switchToFar = YES;
            cameraController.targetNearPd = NO;
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    if (showNearPDDesign == NO && bProgressiveStatus) {
        Results.SetProgressiveDiameterStatus(!bProgressiveStatus);
        [self showNearPdDesign:nearPdShowDesignButton];
    }
}

- (IBAction)exportResultsOrSelectLensDesignPresed:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex == 0) {
        @try {
            [self saveResultsToResources:NO];
            [self sendPlistViaEmail];
        }
        @catch (NSException *exception) {
        }
        @finally {
        }
    } else {
//        NSLog(@"Select new lens design");
    }
    
    [sender setSelected:NO];
}

- (IBAction)changeLensType:(id)sender
{
    switcherImageView.image = singleVisionNow ? [UIImage imageNamed:@"track_off.png"] : [UIImage imageNamed:@"track_on.png"];
    singleVisionNow = !singleVisionNow;
}

- (IBAction)editResults:(id)sender
{
    if (showCircDiameters)
        [self showDiameters:diametrBtn];
    [zoomScrollView setZoomScale:editModeEnabled ? 1.0 : MAX_ZOOM_SCALE animated:YES];
    [infoEditLabel setHidden:editModeEnabled];
    editModeEnabled = !editModeEnabled;
    [zoomScrollView setUserInteractionEnabled:editModeEnabled];
    [zoomScrollView setScrollEnabled:editModeEnabled];
    [controllersView setUserInteractionEnabled:editModeEnabled];
    [editButton setTitle:editModeEnabled ? NSLocalizedString(@"OK", @"") : NSLocalizedString(@"Edit", @"") forState:UIControlStateNormal];
    for (UIView *view in markerViews) {
        [view setUserInteractionEnabled:editModeEnabled];
        view.backgroundColor = editModeEnabled ? [UIColor whiteColor] : [UIColor clearColor];
    }
    bool bProgressiveStatus = Results.GetProgressiveDiameterStatus();
    if (bProgressiveStatus) {
        Results.SetProgressiveDiameterStatus(!bProgressiveStatus);
        [self showNearPdDesign:nearPdShowDesignButton];
    }
}

#pragma mark Show Schema Methods

- (void)toggleScheme:(UIButton *)sender
{
    // hide edit
    if (editModeEnabled) {
        [self editResults:nil];
    }
    
    bool bProgressiveStatus = Results.GetProgressiveDiameterStatus();
    if (bProgressiveStatus)
        [self showNearPdDesign:nearPdShowDesignButton];
    
    [self hiddenDiameters];
    
    diameterStaticView.hidden = YES;
    
    if (showStaticScheme)
    {
        showStaticScheme = NO;
        
        schemeLabel.text = NSLocalizedString(@"Scheme", nil);
        
        editButton.hidden = NO;
        
        [schemeBtn setImage:[UIImage imageNamed:@"schemeBTN"] forState:UIControlStateNormal];
        [schemeBtn setImage:[UIImage imageNamed:@"schemeBTN"] forState:UIControlStateSelected];
        [schemeBtn setImage:[UIImage imageNamed:@"schemeBTN"] forState:UIControlStateHighlighted];
    }
    else
    {
        showStaticScheme = YES;
        
        schemeLabel.text = NSLocalizedString(@"Customer", nil);
        
        editButton.hidden = YES;
        
        [schemeBtn setImage:snapImage forState:UIControlStateNormal];
        [schemeBtn setImage:snapImage forState:UIControlStateSelected];
        [schemeBtn setImage:snapImage forState:UIControlStateHighlighted];
    }
    
    crossesView.hidden = showStaticScheme;
    
    [self setDimensionsView];
    
    if (bProgressiveStatus)
        [self showNearPdDesign:nearPdShowDesignButton];
}

- (void)setDimensionsView
{
    controllersView.hidden = showStaticScheme;
    imageView.hidden  = showStaticScheme;
    staticView.hidden = !showStaticScheme;
}

#pragma mark - Export Results Pressed

- (IBAction)exportResults:(id)sender
{
    if (NETWORK_MANAGER.networkAvaliable)
    {
        if ([NETWORK_MANAGER isStoreClientExists])
        {
            SMStoreClient *client = [NETWORK_MANAGER getCurrentClient];
            
            m_usrname    = client.firstNameClient;
            m_usrsurname = client.lastNameClient;
            
            [self showExportResultsDialog];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Customer name", @"")
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                                  otherButtonTitles:NSLocalizedString(@"OK", @""), nil];
            
            [alert setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
            [[alert textFieldAtIndex:1] setSecureTextEntry:NO];
            
            [alert setTag:PatientAlertTag];
            
            [alert textFieldAtIndex:0].placeholder = NSLocalizedString(@"First Name", nil);
            [alert textFieldAtIndex:1].placeholder = NSLocalizedString(@"Last Name", nil);
            
            [alert textFieldAtIndex:0].autocapitalizationType = UITextAutocapitalizationTypeSentences;
            [alert textFieldAtIndex:1].autocapitalizationType = UITextAutocapitalizationTypeSentences;
            
            [alert show];
        }
    }
    else
        [NETWORK_MANAGER isNotOnlineAlert];
}

#pragma mark Get Screenshot

- (UIImage *)screenshot
{
    UIImage *screenshot = nil;
    
    [_btnQuestion setHidden:YES];
    editButton.hidden = YES;
    nearPdShowDesignButton.hidden = YES;
    pdModeSegmentedControl.hidden = YES;
    homeBtn.hidden = YES;
    backBtn.hidden = YES;
    sessionBtn.hidden = YES;
    seleckLensBtn.hidden = YES;
    exportBtn.hidden = YES;
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
        UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, [UIScreen mainScreen].scale);
    else
        UIGraphicsBeginImageContext(self.view.bounds.size);
    
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGRect cropRect = CGRectMake(0, (69.f * [UIScreen mainScreen].scale),
                                 (self.view.bounds.size.width * [UIScreen mainScreen].scale),
                                 (self.view.bounds.size.height - 69.f) * [UIScreen mainScreen].scale);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect ([screenshot CGImage], cropRect);
    screenshot = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    [_btnQuestion setHidden:NO];
    editButton.hidden = NO;
    nearPdShowDesignButton.hidden = NO;
    pdModeSegmentedControl.hidden = NO;
    homeBtn.hidden = NO;
    backBtn.hidden = NO;
    sessionBtn.hidden = NO;
    seleckLensBtn.hidden = NO;
    exportBtn.hidden = NO;
    
    return screenshot;
}

- (void)showExportResultsDialog
{
    __weak __typeof(self)weakSelf = self;
    
    m_screen = [self screenshot];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Export results", @"")
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"")
                                                     style:UIAlertActionStyleDefault
                                                   handler:nil];
    
    UIAlertAction *Email = [UIAlertAction actionWithTitle:NSLocalizedString(@"Email",@"")
                                                    style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action) {
                                                      @try {
                                                          [weakSelf saveResultsToResources:NO];
                                                          [weakSelf sendPlistViaEmail]; }
                                                      @catch (NSException *exception) {}
                                                      @finally {}
                                                  }];
    
    UIAlertAction *Ecolumn = [UIAlertAction actionWithTitle:NSLocalizedString(@"E-column",@"")
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action) {
                                                        [weakSelf selectFtpSource];
                                                    }];
    
    UIAlertAction *LPTApp = [UIAlertAction actionWithTitle:NSLocalizedString(@"LPT App",@"")
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [weakSelf sendResultsToLPTApp];
                                                   }];
    
    UIAlertAction *UDPServer = [UIAlertAction actionWithTitle:NSLocalizedString(@"UDP Server",@"")
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          @try {
                                                              [weakSelf saveResultsToResources:NO];
                                                              [weakSelf showUDPServersPopover]; }
                                                          @catch (NSException *exception) {}
                                                          @finally {}
                                                      }];
    
    UIAlertAction *AcepWebService = [UIAlertAction actionWithTitle:NSLocalizedString(@"ACEP Web Service", nil)
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               [weakSelf saveResultsToResources:NO];
                                                               [weakSelf sendToPMS];
                                                           }];
    
    UIAlertAction *Pdf = [UIAlertAction actionWithTitle:NSLocalizedString(@"PDF to Email",@"")
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action) {
                                                    [weakSelf selectSizePDFAlert];
                                                }];
    [alertController addAction:Email];
    [alertController addAction:Ecolumn];
    
    if ([PD_MANAGER useOpSysWeb])
    {
        UIAlertAction *OpSysWeb = [UIAlertAction actionWithTitle:NSLocalizedString(@"OpSysWeb",@"")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [weakSelf sendResultFile];
                                                         }];
        [alertController addAction:OpSysWeb];
    }
    
    [alertController addAction:LPTApp];
    [alertController addAction:UDPServer];
    [alertController addAction:AcepWebService];
    [alertController addAction:Pdf];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showIncorrectPatientInfoDialog
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                    message:NSLocalizedString(@"Customer first and last name can't be empty", @"")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil];
    [alert setTag:3];
    
    [alert show];
}

- (BOOL)checkStringIsNotNil:(NSString *)string
{
    NSString *stringWithoutSpaces = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    return stringWithoutSpaces.length;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kNewSessionAlertTag)
    {
        PDMCameraController *viewController = [self.navigationController.viewControllers objectAtIndex:0];
        
        if (buttonIndex == 2)
        {
            if (SESSION_MANAGER.localTargetFromSettings)
            {
                [viewController setFarMode:@(1)];
                
                [viewController alertView:alertView clickedButtonAtIndex:2];
            }
            else
            {
                if (NETWORK_MANAGER.networkAvaliable)
                {
                    [viewController setFarMode:@(1)];
                    
                    [viewController alertView:alertView clickedButtonAtIndex:2];
                }
                else
                {
                    [NETWORK_MANAGER isNotOnlineAlert];
                }
            }

        }
        else if (buttonIndex == 3)
        {
            if (SESSION_MANAGER.localTargetFromSettings)
            {
                [self storeData];
            }
            else
            {
                if (NETWORK_MANAGER.networkAvaliable)
                {
                    [self storeData];
                }
                else
                {
                    [NETWORK_MANAGER isNotOnlineAlert];
                }
            }
        }
        else if (buttonIndex == 4)
        {
            [viewController setFarMode:@(YES)];
            
            [viewController alertView:alertView clickedButtonAtIndex:3];
        }
        else if (buttonIndex != 4)
        {
            if (SESSION_MANAGER.localTargetFromSettings)
            {
                [viewController setFarMode:@(!buttonIndex)];
                
                [viewController alertView:alertView clickedButtonAtIndex:buttonIndex];
            }
            else
            {
                if (NETWORK_MANAGER.networkAvaliable)
                {
                    [viewController setFarMode:@(!buttonIndex)];
                    
                    [viewController alertView:alertView clickedButtonAtIndex:buttonIndex];
                }
                else
                {
                    [NETWORK_MANAGER isNotOnlineAlert];
                }
            }
        }
    }
    
    if (alertView.tag == PatientAlertTag)
    {
        switch (buttonIndex) {
            case 1:
            {
                UIAlertView *currentAlert = (UIAlertView *)alertView;
                
                m_usrname    = nil;
                m_usrsurname = nil;
                
                if ([self checkStringIsNotNil:[currentAlert textFieldAtIndex:0].text]
                    && [self checkStringIsNotNil:[currentAlert textFieldAtIndex:1].text])
                {
                    m_usrname    = [currentAlert textFieldAtIndex:0].text;
                    m_usrsurname = [currentAlert textFieldAtIndex:1].text;
                    
                    [self showExportResultsDialog];
                }
                else
                {
                    [self showIncorrectPatientInfoDialog];
                }
            }
                break;
                
            default:
                break;
        }
        
        return;
    }
    
    if (alertView.tag == 3)
    {
        switch (buttonIndex) {
            case 0:
                [self exportResults:nil];
                break;
                
            default:
                break;
        }
    }
    
    if (alertView.tag == 22)
    {
        switch (buttonIndex) {
            case 1:
                [self sendPDFViaEmail:4];
                break;
            case 2:
                [self sendPDFViaEmail:5];
                break;
                
            default:
                break;
        }
    }
}

- (void)selectSizePDFAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PDF size", @"")
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                          otherButtonTitles:NSLocalizedString(@"A4",@""),
                          NSLocalizedString(@"A5",@""),
                          nil];
    
    alert.tag = 22;
    
    [alert show];
}

- (void)showFtpPopover
{
    PDFTPSelectPopover * popView = [[PDFTPSelectPopover alloc] initWithFTPDictonary:m_ftpStr];
    
    UINavigationController * popNavigationController = [[UINavigationController alloc] initWithRootViewController:popView];
    
    popView.delegate = self;
    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:popNavigationController];
    
    [self.popoverController presentPopoverFromRect:CGRectMake(512, 340, 0, 0) inView:self.view permittedArrowDirections:0 animated:YES];
}

- (void) ftpPopoverDidSelectElementAtIndex:(NSInteger)index
{
    [self ftpSendWithSelectedSourceIndex:index];
    
    [self dismissFTPPopover];
}

- (void) dismissFTPPopover
{
    [popoverController dismissPopoverAnimated:YES];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    popoverContentController = nil;
    
    self.popoverController = nil;
}

#pragma mark PMS Export

- (void)sendToPMS
{
    if (![APP_MANAGER getMeasureWebServiceToken])
    {
        [self showAlertWithError:NSLocalizedString(@"Please, check token field in settings!", nil)];
        return;
    }
    
    if (![APP_MANAGER getMeasureWedServiceStoreID])
    {
        [self showAlertWithError:NSLocalizedString(@"Please, check Store ID field in settings!", nil)];
        return;
    }
    
    [SVProgressHUD setFont:[UIFont fontWithName:@"TrebuchetMS" size:14.f]];
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", nil)];
    
    if ([NETWORK_MANAGER isStoreClientExists])
    {
        if ([NETWORK_MANAGER isStoreClientAlreadyInQueue])
            [self sendDataForExistsInQueueUser:YES];
        else
        {
            [NETWORK_MANAGER clearCurrentClient];
            [self addNewQueueCustomer];
        }
    }
    else
        [self addNewQueueCustomer];
}

- (void)addNewQueueCustomer
{
    SMStoreClient *client  = [SMStoreClient new];
    client.firstNameClient = [m_usrname copy];
    client.lastNameClient  = [m_usrsurname copy];
    
    [NETWORK_MANAGER setCurrentClient:client];
    
    [self sendDataForExistsInQueueUser:NO];
}

- (void)sendDataForExistsInQueueUser:(BOOL)exists
{
    __weak __typeof(self)weakSelf = self;
    
    NSString *token = [APP_MANAGER getMeasureWebServiceToken];
    NSString *store_id = [APP_MANAGER getMeasureWedServiceStoreID];
    
    NSMutableDictionary *sentImagesDict = [NSMutableDictionary new];
    
    if ([APP_MANAGER isNeedToBeExportedCustomerPic])
    {
        NSData *screenshotData = UIImageJPEGRepresentation([self screenshot], 1.0);
        NSString *base64EncodedScreenshot = [screenshotData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        [sentImagesDict setObject:base64EncodedScreenshot forKey:@"screenshot"];
        
        PDMDataManager *dataManager = [PDMDataManager sharedManager];
        
        if (dataManager.farPDImageRotated)
        {
            NSData *photoFarData = UIImageJPEGRepresentation(dataManager.farPDImageRotated, 1.0);
            NSString *base64EncodedFarPhoto = [photoFarData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            
            [sentImagesDict setObject:base64EncodedFarPhoto forKey:@"farPdPhoto"];
        }
        
        if (dataManager.nearPDImageRotated)
        {
            NSData *photoNearData = UIImageJPEGRepresentation(dataManager.nearPDImageRotated, 1.0);
            NSString *base64EncodedNearPhoto = [photoNearData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            
            [sentImagesDict setObject:base64EncodedNearPhoto forKey:@"nearPdPhoto"];
        }
    }
    
    NSMutableDictionary *dictionaryToSend = [NSMutableDictionary new];
    
    NSMutableDictionary *results1 = [NSMutableDictionary new];
    
    NSMutableDictionary *customerInfo = [NSMutableDictionary new];
    
    NSDictionary *resultDict = [NSDictionary new];
    NSDictionary *dictFarPd  = [NSDictionary new];
    NSDictionary *dictNearPd = [NSDictionary new];
    
    resultDict = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
    
    dictFarPd = [resultDict objectForKey:@"FarPD"];
    dictFarPd = [dictFarPd rotateKeys:@{ @"Distance to iPad" : @"Distance_to_iPad",
                                         @"iPad angle" : @"ipadAngle" }];
    
    if ([[resultDict allKeys] containsObject:@"NearPD"])
    {
        dictNearPd = [resultDict objectForKey:@"NearPD"];
        dictNearPd = [dictNearPd rotateKeys:@{ @"Distance to iPad" : @"Distance_to_iPad",
                                               @"iPad angle" : @"ipadAngle" }];
    }
    
    SMStoreClient *client = [NETWORK_MANAGER getCurrentClient];
    
    if (exists && client.clientID)
        [customerInfo setObject:client.clientID forKey:@"id"];
    
    [customerInfo setObject:client.firstNameClient forKey:@"name"];
    [customerInfo setObject:client.lastNameClient forKey:@"lastName"];
    
    if (exists && client.dataBaseID)
        [dictionaryToSend setObject:client.dataBaseID forKey:@"queue_id"];
    
    if (exists)
        [results1 setObject:customerInfo forKey:@"customer"];
    else
        [dictionaryToSend setObject:customerInfo forKey:@"customer"];
    
    // wrapAngle
    if (PDMDataManager.sharedManager.topWrapValue)
    {
        double dblWrapAngle = (kHardFrame)? PDM_DATA.topWrapValue.doubleValue : PDM_DATA.topWrapValue.doubleValue - SEMWRAP;
        [results1 setObject:[NSString stringWithFormat:@"%.1f", dblWrapAngle] forKey:@"wrapAngle"];
    }
    
    // leftDiameter && rightDiameter
    if (Results.GetDiameterStatus())
    {
        [results1 setObject:[NSNumber numberWithDouble:Results.GetDiameter(false)] forKey:@"rightDiameter"];
        [results1 setObject:[NSNumber numberWithDouble:Results.GetDiameter(true)] forKey:@"leftDiameter"];
    }
    
    // frameType
    [results1 setObject:[self getMaterial] forKey:@"frameType"];
    
    // bevelType
    [results1 setObject:[NSString stringWithFormat:@"%.1f", [self getBevelValue]] forKey:@"bevelType"];
    
    // headRotationCorrection
    [results1 setObject:[NSNumber numberWithDouble:(rotation ? 1.f : 0)] forKey:@"headRotationCorrection"];
    
    if (dictFarPd.count > 0) {
        [results1 setObject:dictFarPd forKey:@"farPD"];
    }
    
    if (dictNearPd.count > 0) {
        [results1 setObject:dictNearPd forKey:@"nearPD"];
    }
    
    [results1 setObject:sentImagesDict forKey:@"images"];
    
    [dictionaryToSend setObject:token forKey:@"token"];
    [dictionaryToSend setObject:store_id forKey:@"store_id"];
    [dictionaryToSend setObject:results1 forKey:@"results"];
    
    if (!exists)
    {
        u_int timestamp = [[NSDate date] timeIntervalSince1970];
        [dictionaryToSend setObject:@(timestamp) forKey:@"timestamp"];
    }
    
    [NETWORK_MANAGER requestToPMS:dictionaryToSend
                withQueueAddition:!exists
                   withCompletion:^(BOOL status, NSString *reason) {
                       
                       [SVProgressHUD dismiss];
                       
                       if (status)
                           [weakSelf showResponceWithText:reason];
                       else
                           [weakSelf showAlertWithError:reason];
                   }];
}

#pragma mark UDP Server

- (void)showUDPServersPopover
{
    if ([udpServers count])
    {
        PDUDPSelectPopover *popView = [[PDUDPSelectPopover alloc] initWithFTPDictonary:udpServers];
        
        UINavigationController *popNavigationController = [[UINavigationController alloc] initWithRootViewController:popView];
        popView.delegate = self;
        
        self.popoverController = [[UIPopoverController alloc] initWithContentViewController:popNavigationController];
        [self.popoverController presentPopoverFromRect:CGRectMake(512, 340, 0, 0) inView:self.view permittedArrowDirections:0 animated:YES];
    }
    else
    {
        [self showNoServersAlert];
    }
}

- (void)udpPopoverDidSelectElementAtIndex:(NSInteger)index
{
    [self sendResultsViaUDP:index];
    [self dismissUDPPopover];
}

- (void)dismissUDPPopover
{
    [self.popoverController dismissPopoverAnimated:YES];
}

- (BOOL)isCorrectPort:(NSString *)port
{
    if ([port intValue] <= 0 || [port intValue] > 65535)
    {
        return FALSE;
    }
    else {
        return TRUE;
    }
}

- (void)sendResultsViaUDP:(NSInteger)index
{
    udp_port = @"22222";
    udp_ip = [[udpServers allKeys] objectAtIndex:index];
    
    NSData *myData = [self dataXMLFromResults];
    
    udpSocketSendResults = [[AsyncUdpSocket alloc] initIPv4];
    [udpSocketSendResults setDelegate:self];
    
    NSError *error = nil;
	
	if (![udpSocketSendResults bindToPort:0 error:&error])
	{
		NSLog(@"Error binding: %@", error);
		return;
	}
    
    if (![udpSocketSendResults enableBroadcast:YES error:&error])
    {
        NSLog(@"Error binding: %@", error);
        return;
    }
    
    [udpSocketSendResults receiveWithTimeout:-1 tag:0];
	
	[udpSocketSendResults sendData:myData toHost:udp_ip port:[udp_port intValue] withTimeout:-1 tag:99];
}

#pragma mark UDP Alerts

- (void)showSuccesfullSentToUDPServeralert
{
    [udpServers removeAllObjects];
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:NSLocalizedString(@"Export", @"")
                          message:NSLocalizedString(@"Data exported successfully to UDP Server!", @"")
                          delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}

- (void)showUnsuccessfulSentToUDPServerAlert
{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:NSLocalizedString(@"Export", @"")
                          message:NSLocalizedString(@"Data export failed. Please try again later or check server.", @"")
                          delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}

- (void)showNoServersAlert
{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:NSLocalizedString(@"Error", @"")
                          message:NSLocalizedString(@"No UDP servers found", @"")
                          delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}

- (NSData *)dataXMLFromResults
{
    NSMutableString *resultsString = [NSMutableString string];
    
    NSString *headerString = [NSString stringWithFormat:
                              @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r"
                              "<plist version=\"1.0\">\r"
                              "<dict>\r"];
    
    [resultsString appendString:headerString];
    
    NSString *patientInfo = [NSString stringWithFormat:@"<PatientInfo>\r"
                             "  <FirstName>%@</FirstName>\r"
                             "  <LastName>%@</LastName>\r"
                             "</PatientInfo>\r", m_usrname, m_usrsurname];
    
    [resultsString appendString:patientInfo];
    
    NSDictionary *resultDict = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
    NSArray *allKeys = [resultDict allKeys];
    
    for (NSString *key in [[allKeys reverseObjectEnumerator] allObjects])
    {
        if ([[resultDict objectForKey:key] isKindOfClass:[NSDictionary class]]) {
            
            [resultsString appendFormat:@"<%@>\r", key ];
            
            NSDictionary * childDict = [resultDict objectForKey:key];
            NSArray * allChildDictKeys = [childDict allKeys];
            
            for (NSString * childKey in [[allChildDictKeys reverseObjectEnumerator] allObjects])
            {
                NSString *resKey;
                
                if ([childKey isEqual:@"Distance to iPad"]) {
                    resKey = @"DistanceToiPad";
                } else if ([childKey isEqual:@"iPad angle"]) {
                    resKey = @"iPadAngle";
                } else {
                    resKey = childKey;
                }
                
                [resultsString appendFormat:@"  <%@>%@</%@>\r", resKey, [childDict objectForKey:childKey], resKey];
            }
            
            [resultsString appendFormat:@"</%@>\r", key];
        } else {
            [resultsString appendFormat:@"<%@>%@</%@>\r", key, [resultDict objectForKey:key], key];
        }
    }
    
    NSString *footerString = [NSString stringWithFormat:
                              @"</dict>\r"
                              "</plist>\r"];
    
    [resultsString appendString:footerString];
    
    return [resultsString dataUsingEncoding:NSUTF8StringEncoding];
}

#pragma mark - OpSysWeb Methods

- (void)sendResultFile
{
    NSString *localeString = [[NSLocale currentLocale] localeIdentifier];
    
    NSString *dataString = [self generateExportFileWithClientName:m_usrname withClientSurname:m_usrsurname];
    NSString *exportString = [[dataString dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];
    
    NSString *username = [[NSUserDefaults standardUserDefaults] stringForKey:@"ftp_login"];
    NSString *password = [[NSUserDefaults standardUserDefaults] stringForKey:@"ftp_password"];
    
    NSString *url = @"https://int.opsysweb.eu/webservices/VWOrderService.asmx";
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"refID" ofType:@"plist"];
//    NSString *refID = [[NSMutableDictionary dictionaryWithContentsOfFile:path] objectForKey:@"refID"];
    NSString *refID = [[NSUserDefaults standardUserDefaults] stringForKey:@"opsysweb_refid"];
    
    [NETWORK_MANAGER createSOAPRequestToURL:url
                               withUsername:username
                                   password:password
                                      refID:refID
                                     locale:localeString
                                       data:exportString
                             withCompletion:^(BOOL status, NSDictionary *data, NSString *reason) {
                                 
                                 if (status)
                                 {
                                     if ([data[@"Severity"] isEqualToString:@"Error"])
                                         [self showOpSysWebExportAlertWithTitle:NSLocalizedString(@"Error", nil)
                                                                        message:data[@"Message"]];
                                     else
                                         [self showOpSysWebExportAlertWithTitle:NSLocalizedString(@"Success", nil)
                                                                        message:NSLocalizedString(@"Your data has been transfered to server", nil)];
                                 }
                                 else
                                     [self showOpSysWebExportAlertWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:reason];
                             }];
}

- (NSString*)generateExportFileWithClientName:(NSString*)clientName withClientSurname:(NSString*)clientSurname
{
    NSString *result = [NSString string];
    
    CPDMDataResults2 Data;
    CPDMDataResults DataPhoto;
    
    Data = Results.GetPhotoResults2();
    DataPhoto = Results.GetPhotoResults(0);
    
    NSString * vRightInset = @"";
    NSString * vLeftInset = @"";
    NSString * vFrameWidth = @"";
    NSString * vFrameHeight = @"";
    NSString * vFrameBridge = @"";
    NSString * vRightPD = @"";
    NSString * vLeftPD = @"";
    NSString * vRightHeight = @"";
    NSString * vLeftHeight = @"";
    NSString * vVertex = @"";
    NSString * vPantoHor = @"";
    NSString * vWrap = @"";
    
    if (DataPhoto.bDataResults) {
        vFrameWidth = [NSString stringWithFormat:@"%.1lf", DataPhoto.FrameWidth];
        vFrameHeight = [NSString stringWithFormat:@"%.1lf", DataPhoto.FrameHeight];
        vFrameBridge = [NSString stringWithFormat:@"%.1f", DataPhoto.Bridge];
        vRightPD = [NSString stringWithFormat:@"%.1f", DataPhoto.RightPD];
        vLeftPD = [NSString stringWithFormat:@"%.1f", DataPhoto.LeftPD];
        vRightHeight = [NSString stringWithFormat:@"%.1f", DataPhoto.RightHeight];
        vLeftHeight = [NSString stringWithFormat:@"%.1f", DataPhoto.LeftHeight];
        vPantoHor = [NSString stringWithFormat:@"%.1f", DataPhoto.alfaPantoskHor * 180. / M_PI];
    }
    if (Data.bDataResults) {
        vVertex = [NSString stringWithFormat:@"%.1f", Data.LeftVD];
        if (Data.LeftInset > 0.) {
            vRightInset = [NSString stringWithFormat:@"%.1lf", Data.RightInset];
            vLeftInset = [NSString stringWithFormat:@"%.1lf", Data.LeftInset];
        }
    }
    if (PDMDataManager.sharedManager.topWrapValue) {
        double dblWrapAngle = (kHardFrame)? PDM_DATA.topWrapValue.doubleValue : PDM_DATA.topWrapValue.doubleValue - SEMWRAP;
        vWrap = [NSString stringWithFormat:@"%.1f", dblWrapAngle];
    }
    
    result =[NSString stringWithFormat:@"<omavisionweb>\n"
             "<omaFile>\n"
             "<![CDATA[\n"
             "REQ=FIL\n"
             "_UMSJOB=216\n"
             "JOB=216\n"
             "CLIENT=%@\n"
             "_FNCLIENT=%@\n"
             "_READ_DIST=%@\n"
             "_INSET=%@;%@\n"
             "HBOX=%@;%@\n"
             "VBOX=%@;%@\n"
             "DBL=%@\n"
             "IPD=%@;%@\n"
             "OCHT=%@;%@\n"
             "BVD=%@\n"
             "PANTO=%@\n"
             "ZTILT=%@\n"
             "_VISIT_D=20120919095601\n"
             "SOFTVER=?\n"
             "MNAME=Meyefit\n"
             "MODEL=?\n"
             "SN=?\n"
             "VEN=ESS]]>\n"
             "</omaFile>\n"
             "</omavisionweb>\n", clientName, clientSurname, distanceToIPadTextField.text, vRightInset, vLeftInset, vFrameWidth, vFrameWidth, vFrameHeight, vFrameHeight, vFrameBridge, vRightPD, vLeftPD, vRightHeight, vLeftHeight, vVertex, vPantoHor, vWrap];
    
    return result;
}

- (void)showOpSysWebExportAlertWithTitle:(NSString *)title
                                 message:(NSString *)msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Post To FTP Methods

- (void)sendDidStart
{
    [[NetworkManager sharedInstance] didStartNetworkOperation];
}

- (void)updateStatus:(NSString *)statusString
{
    assert(statusString != nil);
}

- (void)sendDidStopWithStatus:(NSString *)statusString
{
    BOOL hasAnotherElementsInQueue = ftpSendQueue.count;
    BOOL hasNotErrors = statusString == nil;
    
    [[NetworkManager sharedInstance] didStopNetworkOperation];
    
    if (hasAnotherElementsInQueue && hasNotErrors)
        [self startFTPSendFromQueue];
    else
    {
        [self clearFTPQueue];
        
        if (hasNotErrors)
            statusString = @"Data exported successfully!";
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"PD Measurement"
                              message: statusString
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)addToUDPSendQueueWithPath:(NSString *)path fileName:(NSString *)filename
{
    if (!path || !filename)
        return;
    
    if (!ftpSendQueue)
        ftpSendQueue = [[NSMutableArray alloc] initWithCapacity:1];
    
    NSDictionary * oneRequest = [NSDictionary dictionaryWithObjectsAndKeys: path, kFTPPathKey,
                                                                            filename, kFTPNameKey,
                                 nil];
    
    [ftpSendQueue addObject:oneRequest];
}

- (void)clearFTPQueue
{
    [ftpSendQueue removeAllObjects];
}

- (void)startFTPSendFromQueue
{
    if (!ftpSendQueue)
        return;
    
    NSDictionary * oneRequest = ftpSendQueue.lastObject;
    
    NSString * requestPath = [oneRequest objectForKey:kFTPPathKey];
    NSString * requestFileName = [oneRequest objectForKey:kFTPNameKey];
    
    NSLog(@"Sending file %@ tr FTP", requestFileName);
    
    [self startSend:requestPath ftpFileName:requestFileName];
    [ftpSendQueue removeLastObject];
}

#pragma mark * Core transfer code

- (uint8_t *)buffer
{
    return self->_buffer;
}

- (BOOL)isSending
{
    return (self.networkStream != nil);
}

- (void)startSend:(NSString *)filePath
{
    BOOL                    success;
    NSURL *                 url;
    assert(filePath != nil);
    
    assert([[NSFileManager defaultManager] fileExistsAtPath:filePath]);
    
    assert(self.networkStream == nil);      // don't tap send twice in a row!
    assert(self.fileStream == nil);         // ditto
    
    // First get and check the URL.
    
    url = [[NetworkManager sharedInstance] smartURLForString:m_url];
    success = (url != nil);
    
    if (success) {
        // Add the last part of the file name to the end of the URL to form the final
        // URL that we're going to put to.
        
        url = CFBridgingRelease(
                                CFURLCreateCopyAppendingPathComponent(NULL, (__bridge CFURLRef) url, (__bridge CFStringRef) kFTPsendFileName, false)
                                );
        success = (url != nil);
    }
    
    // If the URL is bogus, let the user know.  Otherwise kick off the connection.
    
    if ( ! success) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"BBGR EYEMIO"
                              message: @"Invalid URL"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    } else {
        
        // Open a stream for the file we're going to send.  We do not open this stream;
        // NSURLConnection will do it for us.
        
        self.fileStream = [NSInputStream inputStreamWithFileAtPath:filePath];
        assert(self.fileStream != nil);
        
        [self.fileStream open];
        
        // Open a CFFTPStream for the URL.
        
        self.networkStream = CFBridgingRelease(
                                               CFWriteStreamCreateWithFTPURL(NULL, (__bridge CFURLRef) url)
                                               );
        assert(self.networkStream != nil);
        
        if ([m_log length] != 0) {
            success = [self.networkStream setProperty:m_log forKey:(id)kCFStreamPropertyFTPUserName];
            assert(success);
            success = [self.networkStream setProperty:m_pas forKey:(id)kCFStreamPropertyFTPPassword];
            assert(success);
        }
        
        self.networkStream.delegate = self;
        [self.networkStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [self.networkStream open];
        
        // Tell the UI we're sending.
        
        [self sendDidStart];
    }
}

- (void)stopSendWithStatus:(NSString *)statusString
{
    if (self.networkStream != nil)
    {
        [self.networkStream removeFromRunLoop:[NSRunLoop currentRunLoop]
                                      forMode:NSDefaultRunLoopMode];
        
        self.networkStream.delegate = nil;
        [self.networkStream close];
        self.networkStream = nil;
    }
    
    if (self.fileStream != nil)
    {
        [self.fileStream close];
        self.fileStream = nil;
    }
    
    [self sendDidStopWithStatus:statusString];
}

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode
{
#pragma unused(aStream)
    assert(aStream == self.networkStream);
    
    switch (eventCode) {
        case NSStreamEventOpenCompleted: {
            [self updateStatus:@"Opened connection"];
        } break;
        case NSStreamEventHasBytesAvailable: {
            assert(NO);     // should never happen for the output stream
        } break;
        case NSStreamEventHasSpaceAvailable: {
            [self updateStatus:@"Sending"];
            
            // If we don't have any data buffered, go read the next chunk of data.
            
            if (self.bufferOffset == self.bufferLimit) {
                NSInteger   bytesRead;
                
                bytesRead = [self.fileStream read:self.buffer maxLength:kSendBufferSize];
                
                if (bytesRead == -1) {
                    [self stopSendWithStatus:@"File read error"];
                } else if (bytesRead == 0) {
                    [self stopSendWithStatus:nil];
                } else {
                    self.bufferOffset = 0;
                    self.bufferLimit  = bytesRead;
                }
            }
            
            // If we're not out of data completely, send the next chunk.
            
            if (self.bufferOffset != self.bufferLimit) {
                NSInteger   bytesWritten;
                bytesWritten = [self.networkStream write:&self.buffer[self.bufferOffset] maxLength:self.bufferLimit - self.bufferOffset];
                assert(bytesWritten != 0);
                if (bytesWritten == -1) {
                    [self stopSendWithStatus:@"Network write error"];
                } else {
                    self.bufferOffset += bytesWritten;
                }
            }
        } break;
        case NSStreamEventErrorOccurred: {
            [self stopSendWithStatus:@"Stream open error"];
        } break;
        case NSStreamEventEndEncountered: {
            // ignore
        } break;
        default: {
            assert(NO);
        } break;
    }
}

#pragma mark - UDP methods

- (NSString *)getWiFiBroadcastAddress
{
    // Get the WiFi Broadcast Address
    NSString *String = [SSNetworkInfo WiFiBroadcastAddress];
    // Validate it
    if (String == nil || String.length <= 0) {
        // Error, no value returned
        return nil;
    }
    // Successful
    return String;
}

/*
 *  UDP RESPONCE FORMAT 
 *  <message><key>ftp</key><user>user1</user><password>123</password><path>/center</path></message>
 */

- (void)startRequestTimer
{
    udpRequestsTimer = [NSTimer scheduledTimerWithTimeInterval:kUDPUpdatePeriodInSeconds
                                                        target:self
                                                      selector:@selector(sendRequest)
                                                      userInfo:nil
                                                       repeats:YES];
}

- (void)stopRequestTimer
{
    [udpRequestsTimer invalidate];
    if (udpSocket != nil) {
        [udpSocket setDelegate:nil];
    }
    udpSocket = nil;
}

- (void)sendRequest
{
    udpSocket = [[AsyncUdpSocket alloc] initIPv4];
    [udpSocket setDelegate:self];
    
    NSError *error = nil;
	
	if (![udpSocket bindToPort:0 error:&error])
	{
		NSLog(@"Error binding: %@", error);
		return;
	}
    
    if (![udpSocket enableBroadcast:YES error:&error])
    {
        NSLog(@"Error binding: %@", error);
        return;
    }
	
	[udpSocket receiveWithTimeout:-1 tag:0];
	
	NSLog(@"Ready");
    
    NSString *host = [self getWiFiBroadcastAddress];
    
    if (!host) {
        host = @"255.255.255.255";
    }
    
    int port = 22222;
	if (port <= 0 || port > 65535)
	{
		[self showAlertWithError: @"Valid port required"];
		return;
	}
	
	NSString *msg = @"<message><key>ftp</key></message>";
	if ([msg length] == 0)
	{
        [self showAlertWithError: @"Message required"];
		return;
	}
	
	NSData *data = [msg dataUsingEncoding:NSUTF8StringEncoding];
	[udpSocket sendData:data toHost:host port:port withTimeout:-1 tag:1];
}

- (void)onUdpSocket:(AsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
    NSLog(@"Data sended with tag: %lu", tag);
}

- (void)onUdpSocket:(AsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
    if ([error code] == 49)
        return;
    
    if ([error code] == 65) NSLog( @"Network error: %@", [error localizedDescription]);
    else if ([error code] == 51) NSLog( @"Network error: %@", [error localizedDescription]);
    else [self showAlertWithError: [NSString stringWithFormat:@"Send error (%ld):%@", (long)[error code], [error localizedDescription]]];
}


- (BOOL)onUdpSocket:(AsyncUdpSocket *)sock
     didReceiveData:(NSData *)data
            withTag:(long)tag
           fromHost:(NSString *)host
               port:(UInt16)port
{
	NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    if (msg)
	{
        if ([msg isEqual:@"<message><key>fileServer</key></message>"])
        {
            NSDate *start = [NSDate date];
            
            if (!udpServers)
            {
                udpServers = [NSMutableDictionary dictionary];
            }
            
            [udpServers setObject:(NSDate *)start forKey:(NSString *)host];
            
        }
        else if ([msg isEqual:@"<message><key>YES</key></message>"])
        {
            [self showSuccesfullSentToUDPServeralert];
        }
        else if ([msg isEqual:@"<message><key>NO</key></message>"])
        {
            [self showUnsuccessfulSentToUDPServerAlert];
        }
        else
        {
            if (m_ftpStr == nil ) m_ftpStr = [[NSMutableDictionary alloc] init];
            [m_ftpStr setObject:msg forKey:(NSString *)host];
        }
	}
    
    [udpSocket receiveWithTimeout:-1 tag:0];
    
	return YES;
}

- (void)showResponceWithText:(NSString *)text
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString( @"Response", @"" )
                                                    message:text
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString( @"OK", @"" )
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)showAlertWithError:(NSString *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString( @"Error", @"" )
                                                    message:error
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString( @"OK", @"" )
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)formFtpSetupFromString:(NSString *)string url:(NSString *)url
{
    NSString * path = [string stringBetweenString:@"<path>" andString:@"</path>"];
    
    m_log = [string stringBetweenString:@"<user>" andString:@"</user>"];
    m_pas = [string stringBetweenString:@"<password>" andString:@"</password>"];
    m_url = [url stringByAppendingString:path];
}

- (void)selectFtpSource
{
    NSArray * ftpDictKeys = [m_ftpStr allKeys];
    
    if (![ftpDictKeys count])
    {
        [self showAlertWithError:NSLocalizedString( @"No FTP servers found", @"" )];
        return;
    }
    else
    {
        [self showFtpPopover];
    }
}

- (void)ftpSendWithSelectedSourceIndex:(NSInteger)index
{
    NSString * firstKey = [[m_ftpStr allKeys] objectAtIndex:index];
    NSString * selectedMsg = [m_ftpStr objectForKey:firstKey];
    
    [self formFtpSetupFromString:selectedMsg url:firstKey];
    
    if (m_log != nil && m_pas != nil && m_url != nil && ![self isSending])
    {
        [self saveResultsToResources:YES];
    }
}

#pragma mark - Show NearPD Design

- (IBAction)showNearPdDesign:(UIButton *)sender
{
    bool bProgressiveStatus = Results.GetProgressiveDiameterStatus();
    Results.SetProgressiveDiameterStatus(!bProgressiveStatus);
    m_labelLensType.text = (Results.GetProgressiveDiameterStatus())? NSLocalizedString(@"Progressive", @"") : NSLocalizedString(@"Single Vision", @"");

    BOOL bShowCircDiameters = showCircDiameters;
    if (showCircDiameters)
        [self showDiameters:diametrBtn];
    
    if (showStaticScheme)
    {
        showNearPDDesign = (!editModeEnabled && farPdMode.boolValue && Results.GetProgressiveDiameterStatus())? YES : NO;
        
        staticDesignScheme.hidden = !showNearPDDesign;
        staticCorridScheme.hidden = !showNearPDDesign;
        
        if (showNearPDDesign)
            [self configureNearPdDesignFrames];
    }
    else {
        showNearPDDesign = (!editModeEnabled && farPdMode.boolValue && Results.GetProgressiveDiameterStatus())? NO : YES;
        if (!showNearPDDesign)
        {
            progressiveScheme = YES;
            for (UIView *view in nearPdDesignViews) {
                [view setHidden:NO];
            }
            [self configureNearPdDesignFrames];
            
            showNearPDDesign = YES;
            
        } else {
            progressiveScheme = NO;
            for (UIView *view in nearPdDesignViews) {
                if ([view class] == [UIImageView class]) {
                    [view setHidden:YES];
                }
            }
            
            showNearPDDesign = NO;
        }
    }
    
    if (Results.GetProgressiveDiameterStatus()) {
        [self updateLensInfoData];
        if (Results.GetDiameterStatus()) {
            diametrRField.text = [NSString stringWithFormat:@"%.0lf/%.0lf", Results.GetDiameter(false), Results.GetDiameter(false) + 5];
            diametrLField.text = [NSString stringWithFormat:@"%.0lf/%.0lf", Results.GetDiameter(true), Results.GetDiameter(true) + 5];
        }
    }
    else {
        [self updateLensInfoData];
        if (Results.GetDiameterStatus()) {
            diametrRField.text = [NSString stringWithFormat:@"%.0lf", Results.GetDiameter(false)];
            diametrLField.text = [NSString stringWithFormat:@"%.0lf", Results.GetDiameter(true)];
        }
    }

    if (bShowCircDiameters)
        [self showDiameters:diametrBtn];
}
- (void)updateLensInfoData
{
    NSString * strCorridor = @"";
    NSString * strDiameter = @"";
    if (!editModeEnabled && farPdMode.boolValue) {
        if (Results.GetProgressiveDiameterStatus()) {
            strCorridor = [popoverProgressiveType.popoverValues objectAtIndex:(defaultProgressiveDesign - popoverProgressiveType.shiftValue)];
        }
        if (showCircDiameters == YES) {
            if (Results.GetProgressiveDiameterStatus()) {
                strDiameter = [NSString stringWithFormat:@"%@%@ %.0lf/%.0lf, %@ %.0lf/%.0lf", NSLocalizedString(@"ED: ", nil), NSLocalizedString(@"R", @""), Results.GetDiameter(false), Results.GetDiameter(false) + 5, NSLocalizedString(@"L", @""), Results.GetDiameter(true), Results.GetDiameter(true) + 5];
            }
            else {
                strDiameter = [NSString stringWithFormat:@"%@%@ %.0lf, %@ %.0lf", NSLocalizedString(@"ED: ", nil), NSLocalizedString(@"R", @""), Results.GetDiameter(false), NSLocalizedString(@"L", @""), Results.GetDiameter(true)];
            }
        }
    }

    NSString * str = [NSString stringWithFormat: @"%@  %@", strCorridor, strDiameter];
    NSString * trimmedStr = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    m_labelLensData.text = trimmedStr;
}

#pragma mark - Show Diameters

- (IBAction)showDiameters:(id)sender
{
    if (showStaticScheme)
    {
        if (farPdMode.boolValue && diameterStaticView.isHidden)
        {
            diameterStaticView.hidden = NO;
            showCircDiameters = YES;
        }
        else
        {
            diameterStaticView.hidden = YES;
            showCircDiameters = NO;
        }
    }
    else
    {
        if (showCircDiameters)
        {
            [self hiddenDiameters];
        }
        else if (farPdMode.boolValue)
        {
            NSLog(@"Show diameters");
            [lensDiameterView setUserInteractionEnabled:YES];
            
            // hide edit
            if (editModeEnabled)
            {
                [self editResults:nil];
            }
            // show Diameters
            [leftDiameterImageView setHidden:NO];
            [rightDiameterImageView setHidden:NO];
            showCircDiameters = YES;
            
            CGPoint rightPoint = CGPointMake([eyeArea rightEye].x,
                                             [eyeArea rightEye].y);
            CGPoint leftPoint = CGPointMake([eyeArea leftEye].x,
                                            [eyeArea leftEye].y);
            
            CPDMDataResults DataPhoto;
            int iPhoto = farPdMode.boolValue ? 0 : 2;
            
            if(iPhoto == 0 && Results.CalcPhotoResults(iPhoto))
            {
                DataPhoto = Results.GetPhotoResults(iPhoto);
                leftPoint.x += DataPhoto.dxLeft * rsd.rectScale;
                leftPoint.y += DataPhoto.dyLeft * rsd.rectScale;
                rightPoint.x += DataPhoto.dxRight * rsd.rectScale;
                rightPoint.y += DataPhoto.dyRight * rsd.rectScale;
            }
            
            rightCenterPoint = rightPoint;
            leftCenterPoint = leftPoint;
            [self showRightEyeDiameter:rightCenterPoint withDiameter:Results.GetDiameter(false) forProgressive: Results.GetProgressiveDiameterStatus()];
            [self showLeftEyeDiameter:leftCenterPoint withDiameter:Results.GetDiameter(true) forProgressive: Results.GetProgressiveDiameterStatus()];
            
            if (Results.GetProgressiveDiameterStatus()) {
                [self updateLensInfoData];
                if (Results.GetDiameterStatus()) {
                    diametrRField.text = [NSString stringWithFormat:@"%.0lf/%.0lf", Results.GetDiameter(false), Results.GetDiameter(false) + 5];
                    diametrLField.text = [NSString stringWithFormat:@"%.0lf/%.0lf", Results.GetDiameter(true), Results.GetDiameter(true) + 5];
                }
            }
            else {
                [self updateLensInfoData];
                if (Results.GetDiameterStatus()) {
                    diametrRField.text = [NSString stringWithFormat:@"%.0lf", Results.GetDiameter(false)];
                    diametrLField.text = [NSString stringWithFormat:@"%.0lf", Results.GetDiameter(true)];
                }
            }
            
            NSLog(@"ED: l:%f, r:%f", Results.GetDiameter(true), Results.GetDiameter(false));
            double leftDiameter = Results.GetDiameter(true);
            double rightDiameter = Results.GetDiameter(false);
            
            if (leftDiameter == rightDiameter)
            {
                editDiameterState = 0;
            }
            else
            {
                editDiameterState = 3;
            }
        }
    }
    [self updateLensInfoData];
}

- (void)hiddenDiameters
{
    [lensDiameterView setUserInteractionEnabled:NO];
    [leftDiameterImageView setHidden:YES];
    [rightDiameterImageView setHidden:YES];
    showCircDiameters = NO;
    
    double leftDiameter = Results.GetDiameter(true);
    double rightDiameter = Results.GetDiameter(false);
    // set diameter
    Results.SetDiameter(true, leftDiameter);
    Results.SetDiameter(false, rightDiameter);
    
    if (Results.GetDiameterStatus())
    {
        if (Results.GetProgressiveDiameterStatus()) {
            diametrRField.text = [NSString stringWithFormat:@"%.0lf/%.0lf", Results.GetDiameter(false), Results.GetDiameter(false) + 5];
            diametrLField.text = [NSString stringWithFormat:@"%.0lf/%.0lf", Results.GetDiameter(true), Results.GetDiameter(true) + 5];
        }
        else {
            diametrRField.text = [NSString stringWithFormat:@"%.0lf", Results.GetDiameter(false)];
            diametrLField.text = [NSString stringWithFormat:@"%.0lf", Results.GetDiameter(true)];
        }
    }
    [self updateLensInfoData];
}

- (void)showLeftEyeDiameter: (CGPoint)eyePoint withDiameter: (double)diam forProgressive: (bool)bProgressiveStatus
{
    int iPhoto = farPdMode.boolValue ? 0 : 2;
    if (iPhoto == 0) {
        double onePixelInMM =  Results.convertToCM(iPhoto, 1.0) / rsd.rectScale;
        double onePixelInMM_dx = onePixelInMM;
        double onePixelInMM_dy = onePixelInMM;
        
        if (iPhoto == 0) {
            CPDMPhotoData PhData =  Results.GetPhotoData(iPhoto);
            if (PhData.nPhotoMode != nNoData) {
                if(Results.CalcPhotoResults(iPhoto)) {
                    CPDMDataResults PhResults = Results.GetPhotoResults(iPhoto);
                    onePixelInMM_dx = (1. / PhResults.pixelInMM_dxLeft) / rsd.rectScale;
                    onePixelInMM_dy = (1. / PhResults.pixelInMM_dyLeft) / rsd.rectScale;
                }
            }
        }
        
        CGRect topFrame = leftDiameterImageView.frame;
        topFrame.size = CGSizeMake(diam / onePixelInMM_dx, diam / onePixelInMM_dy);
        topFrame.origin.x = eyePoint.x - 0.5 * (topFrame.size.width) + 0.25;
        topFrame.origin.y = eyePoint.y - 0.5 * (topFrame.size.height) + 0.5;
        // Progressive decentration
        if (bProgressiveStatus) {
            topFrame.origin.x += 2.5 / onePixelInMM_dx;
            topFrame.origin.y += 4.0 / onePixelInMM_dy;
        }
        leftDiameterImageView.frame = topFrame;
    }
}
- (void)showRightEyeDiameter: (CGPoint)eyePoint withDiameter: (double)diam forProgressive: (bool)bProgressiveStatus
{
    int iPhoto = farPdMode.boolValue ? 0 : 2;
    if (iPhoto == 0) {
        double onePixelInMM =  Results.convertToCM(iPhoto, 1.0) / rsd.rectScale;
        double onePixelInMM_dx = onePixelInMM;
        double onePixelInMM_dy = onePixelInMM;
        
        if (iPhoto == 0) {
            CPDMPhotoData PhData =  Results.GetPhotoData(iPhoto);
            if (PhData.nPhotoMode != nNoData) {
                if(Results.CalcPhotoResults(iPhoto)) {
                    CPDMDataResults PhResults = Results.GetPhotoResults(iPhoto);
                    onePixelInMM_dx = (1. / PhResults.pixelInMM_dxRight) / rsd.rectScale;
                    onePixelInMM_dy = (1. / PhResults.pixelInMM_dyRight) / rsd.rectScale;
                }
            }
        }
        
        CGRect topFrame = rightDiameterImageView.frame;
        topFrame.size = CGSizeMake(diam / onePixelInMM_dx, diam / onePixelInMM_dy);
        topFrame.origin.x = eyePoint.x - 0.5 * (topFrame.size.width) + 0.25;
        topFrame.origin.y = eyePoint.y - 0.5 * (topFrame.size.height) + 0.5;
        // Progressive decentration
        if (bProgressiveStatus) {
            topFrame.origin.x -= 2.5 / onePixelInMM_dx;
            topFrame.origin.y += 4.0 / onePixelInMM_dy;
        }
        rightDiameterImageView.frame = topFrame;
    }
}

- (void)lensDiameterControlPanGesture: (UIPanGestureRecognizer *)gesture
{
    if ([gesture state] == UIGestureRecognizerStateBegan) {
        firstPoint = CGPointMake([gesture locationInView:lensDiameterView].x, [gesture locationInView:lensDiameterView].y);
        CGSize size = lensDiameterView.frame.size;
        if (firstPoint.x > size.width / 2) {
            // left lens
            firstDiameter = Results.GetDiameter(true);
        }
        else {
            // right lens
            firstDiameter = Results.GetDiameter(false);
        }
    } else if ([gesture state] == UIGestureRecognizerStateChanged) {
        CGFloat newX = [gesture locationInView:lensDiameterView].x - firstPoint.x;
        CGFloat newY = [gesture locationInView:lensDiameterView].y - firstPoint.y;
        NSLog(@"x=%f, y=%f, xn=%f, yn=%f", firstPoint.x, firstPoint.y, newX, newY);
        
        CGSize size = lensDiameterView.frame.size;
        if (firstPoint.x > size.width / 2) {
            // left lens
            double xCenter = size.width * 3. / 4.;
            double yCenter = size.height / 2.;
            
            double R1 = sqrt((firstPoint.x - xCenter) * (firstPoint.x - xCenter) + (firstPoint.y - yCenter) * (firstPoint.y - yCenter));
            double R2 = sqrt((firstPoint.x + newX - xCenter) * (firstPoint.x + newX - xCenter) + (firstPoint.y + newY - yCenter) * (firstPoint.y + newY - yCenter));
            double dR = R2 - R1;
            
            int iPhoto = farPdMode.boolValue ? 0 : 2;
            if (iPhoto == 0) {
                double onePixelInMM =  Results.convertToCM(iPhoto, 1.0) / rsd.rectScale;
                double dRmm = dR * onePixelInMM;
                long ldRmm = floor(dRmm + 0.5);
                double Diam = firstDiameter + ldRmm;
                Results.SetDiameter(true, Diam);
                [self showLeftEyeDiameter:leftCenterPoint withDiameter:Results.GetDiameter(true) forProgressive:Results.GetProgressiveDiameterStatus()];
                if (editDiameterState == 0) {
                    editDiameterState = 1;
                }
                if (editDiameterState == 1) {
                    Results.SetDiameter(false, Diam);
                    [self showRightEyeDiameter:rightCenterPoint withDiameter:Results.GetDiameter(false) forProgressive:Results.GetProgressiveDiameterStatus()];
                }
                if (editDiameterState == 2) {
                    editDiameterState = 3;
                }
            }
        }
        else {
            // right lens
            double xCenter = size.width * 1. / 4.;
            double yCenter = size.height / 2.;
            
            double R1 = sqrt((firstPoint.x - xCenter) * (firstPoint.x - xCenter) + (firstPoint.y - yCenter) * (firstPoint.y - yCenter));
            double R2 = sqrt((firstPoint.x + newX - xCenter) * (firstPoint.x + newX - xCenter) + (firstPoint.y + newY - yCenter) * (firstPoint.y + newY - yCenter));
            double dR = R2 - R1;
            
            int iPhoto = farPdMode.boolValue ? 0 : 2;
            if (iPhoto == 0) {
                double onePixelInMM =  Results.convertToCM(iPhoto, 1.0) / rsd.rectScale;
                double dRmm = dR * onePixelInMM;
                long ldRmm = floor(dRmm + 0.5);
                double Diam = firstDiameter + ldRmm;
                Results.SetDiameter(false, Diam);
                [self showRightEyeDiameter:rightCenterPoint withDiameter:Results.GetDiameter(false) forProgressive:Results.GetProgressiveDiameterStatus()];
                if (editDiameterState == 0) {
                    editDiameterState = 2;
                }
                if (editDiameterState == 1) {
                    editDiameterState = 3;
                }
                if (editDiameterState == 2) {
                    Results.SetDiameter(true, Diam);
                    [self showLeftEyeDiameter:leftCenterPoint withDiameter:Results.GetDiameter(true) forProgressive:Results.GetProgressiveDiameterStatus()];
                }
            }
        }
        if (Results.GetProgressiveDiameterStatus()) {
            [self updateLensInfoData];
            if (Results.GetDiameterStatus()) {
                diametrRField.text = [NSString stringWithFormat:@"%.0lf/%.0lf", Results.GetDiameter(false), Results.GetDiameter(false) + 5];
                diametrLField.text = [NSString stringWithFormat:@"%.0lf/%.0lf", Results.GetDiameter(true), Results.GetDiameter(true) + 5];
            }
        }
        else {
            [self updateLensInfoData];
            if (Results.GetDiameterStatus()) {
                diametrRField.text = [NSString stringWithFormat:@"%.0lf", Results.GetDiameter(false)];
                diametrLField.text = [NSString stringWithFormat:@"%.0lf", Results.GetDiameter(true)];
            }
        }
    } else if ([gesture state] == UIGestureRecognizerStateEnded || [gesture state] == UIGestureRecognizerStateCancelled) {
    }
}

- (void)leftEyeHeightTopControlPanGesture:(UIPanGestureRecognizer *)gesture
{
    zoomScrollView.scrollEnabled = NO;
    if ([gesture state] == UIGestureRecognizerStateBegan) {
        firstPoint = CGPointMake([gesture locationInView:leftEyeHeightTopControlView].x, [gesture locationInView:leftEyeHeightTopControlView].y);
        firstWidth = [gesture locationInView:controllersView].x;
    } else if ([gesture state] == UIGestureRecognizerStateChanged) {
        CGFloat newX = firstWidth - firstPoint.x;
        CGFloat newY = [gesture locationInView:controllersView].y - firstPoint.y;
        CGFloat offsetY = leftEyeHeightTopControlView.frame.origin.y - newY;
        [leftEyeHeightTopControlView setPoint:CGPointMake(newX, newY)];
        eyeArea.leftEye = CGPointMake(eyeArea.leftEye.x, eyeArea.leftEye.y - offsetY);
        
        [eyeArea refresh];
        leftEyeHeightTopControlView.frame = CGRectMake(eyeArea.leftHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH, 
                                                       eyeArea.leftHeightPosition.y - ADDITIONAL_CONTROL_SIZE, 
                                                       CONTROL_SIZE, 
                                                       eyeArea.leftHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE);
        leftEyeHeightBottomControlView.frame = CGRectMake(eyeArea.leftHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                          eyeArea.leftHeightPosition.y + eyeArea.leftHeightLength / 2.0,
                                                          CONTROL_SIZE,
                                                          eyeArea.leftHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE + 2.0);
        leftPdWidthControlView.frame = CGRectMake(eyeArea.leftPdPosition.x,
                                                  eyeArea.leftPdPosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                  eyeArea.leftPdLength + ADDITIONAL_CONTROL_SIZE,
                                                  CONTROL_SIZE);
        rightPdWidthControlView.frame = CGRectMake(eyeArea.rightPdPosition.x - ADDITIONAL_CONTROL_SIZE,
                                                   eyeArea.rightPdPosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                   eyeArea.rightPdLength + ADDITIONAL_CONTROL_SIZE + 2.0,
                                                   CONTROL_SIZE);
        [self refresh];
    } else if ([gesture state] == UIGestureRecognizerStateEnded || [gesture state] == UIGestureRecognizerStateCancelled) {
        zoomScrollView.scrollEnabled = YES;
    }
}

- (void)leftEyeHeightBottomControlPanGesture:(UIPanGestureRecognizer *)gesture
{
    zoomScrollView.scrollEnabled = NO;
    if ([gesture state] == UIGestureRecognizerStateBegan) {
        firstPoint = CGPointMake([gesture locationInView:leftEyeHeightBottomControlView].x, [gesture locationInView:leftEyeHeightBottomControlView].y);
        firstWidth = [gesture locationInView:controllersView].x;
    } else if ([gesture state] == UIGestureRecognizerStateChanged) {
        CGFloat newX = firstWidth - firstPoint.x;
        CGFloat newY = [gesture locationInView:controllersView].y - firstPoint.y;
        CGFloat offsetY = leftEyeHeightBottomControlView.frame.origin.y - newY;
        firstPoint.y -= offsetY / 2.0;
        [leftEyeHeightBottomControlView setPoint:CGPointMake(newX, newY)];
        eyeArea.leftRect = CGPointMake(eyeArea.leftRect.x, eyeArea.leftRect.y - offsetY);
        [eyeArea refresh];
        leftEyeHeightTopControlView.frame = CGRectMake(eyeArea.leftHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH, 
                                                       eyeArea.leftHeightPosition.y - ADDITIONAL_CONTROL_SIZE, 
                                                       CONTROL_SIZE, 
                                                       eyeArea.leftHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE);
        leftEyeHeightBottomControlView.frame = CGRectMake(eyeArea.leftHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                          eyeArea.leftHeightPosition.y + eyeArea.leftHeightLength / 2.0,
                                                          CONTROL_SIZE,
                                                          eyeArea.leftHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE + 2.0);
        [self refresh];
        leftBridgeControlView.frame = CGRectMake(eyeArea.bridgePosition.x - ADDITIONAL_CONTROL_SIZE,
                                                 eyeArea.bridgePosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                 eyeArea.bridgeLength / 2.0 + ADDITIONAL_CONTROL_SIZE,
                                                 CONTROL_SIZE);
        rightBridgeControlView.frame = CGRectMake(eyeArea.bridgePosition.x + eyeArea.bridgeLength / 2.0,
                                                  eyeArea.bridgePosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                  eyeArea.bridgeLength / 2.0 + ADDITIONAL_CONTROL_SIZE + 2.0,
                                                  CONTROL_SIZE);
    } else if ([gesture state] == UIGestureRecognizerStateEnded || [gesture state] == UIGestureRecognizerStateCancelled) {
        zoomScrollView.scrollEnabled = YES;
    }
}

- (void)rightEyeHeightTopControlPanGesture:(UIPanGestureRecognizer *)gesture
{
    zoomScrollView.scrollEnabled = NO;
    if ([gesture state] == UIGestureRecognizerStateBegan) {
        firstPoint = CGPointMake([gesture locationInView:rightEyeHeightTopControlView].x, [gesture locationInView:rightEyeHeightTopControlView].y);
        firstWidth = [gesture locationInView:controllersView].x;
    } else if ([gesture state] == UIGestureRecognizerStateChanged) {
        CGFloat newX = firstWidth - firstPoint.x;
        CGFloat newY = [gesture locationInView:controllersView].y - firstPoint.y;
        CGFloat offsetY = rightEyeHeightTopControlView.frame.origin.y - newY;
        [rightEyeHeightTopControlView setPoint:CGPointMake(newX, newY)];
        eyeArea.rightEye = CGPointMake(eyeArea.rightEye.x, eyeArea.rightEye.y - offsetY);
        
        [eyeArea refresh];
        rightEyeHeightTopControlView.frame = CGRectMake(eyeArea.rightHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH, 
                                                        eyeArea.rightHeightPosition.y - ADDITIONAL_CONTROL_SIZE, 
                                                        CONTROL_SIZE, 
                                                        eyeArea.rightHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE);
        rightEyeHeightBottomControlView.frame = CGRectMake(eyeArea.rightHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                           eyeArea.rightHeightPosition.y + eyeArea.rightHeightLength / 2.0,
                                                           CONTROL_SIZE,
                                                           eyeArea.rightHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE + 2.0);
        leftPdWidthControlView.frame = CGRectMake(eyeArea.leftPdPosition.x,
                                                  eyeArea.leftPdPosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                  eyeArea.leftPdLength + ADDITIONAL_CONTROL_SIZE,
                                                  CONTROL_SIZE);
        rightPdWidthControlView.frame = CGRectMake(eyeArea.rightPdPosition.x - ADDITIONAL_CONTROL_SIZE,
                                                   eyeArea.rightPdPosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                   eyeArea.rightPdLength + ADDITIONAL_CONTROL_SIZE + 2.0,
                                                   CONTROL_SIZE);
        [self refresh];
    } else if ([gesture state] == UIGestureRecognizerStateEnded || [gesture state] == UIGestureRecognizerStateCancelled) {
        zoomScrollView.scrollEnabled = YES;
    }
}

- (void)rightEyeHeightBottomControlPanGesture:(UIPanGestureRecognizer *)gesture
{
    zoomScrollView.scrollEnabled = NO;
    if ([gesture state] == UIGestureRecognizerStateBegan) {
        firstPoint = CGPointMake([gesture locationInView:rightEyeHeightBottomControlView].x, [gesture locationInView:rightEyeHeightBottomControlView].y);
        firstWidth = [gesture locationInView:controllersView].x;
    } else if ([gesture state] == UIGestureRecognizerStateChanged) {
        CGFloat newX = firstWidth - firstPoint.x;
        CGFloat newY = [gesture locationInView:controllersView].y - firstPoint.y;
        
        CGFloat offsetY = rightEyeHeightBottomControlView.frame.origin.y - newY;
        firstPoint.y -= offsetY / 2.0;
        [rightEyeHeightBottomControlView setPoint:CGPointMake(newX, newY)];
        eyeArea.rightRect = CGPointMake(eyeArea.rightRect.x, eyeArea.rightRect.y - offsetY);
        
        [eyeArea refresh];
        rightEyeHeightTopControlView.frame = CGRectMake(eyeArea.rightHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH, 
                                                        eyeArea.rightHeightPosition.y - ADDITIONAL_CONTROL_SIZE, 
                                                        CONTROL_SIZE, 
                                                        eyeArea.rightHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE);
        rightEyeHeightBottomControlView.frame = CGRectMake(eyeArea.rightHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                           eyeArea.rightHeightPosition.y + eyeArea.rightHeightLength / 2.0,
                                                           CONTROL_SIZE,
                                                           eyeArea.rightHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE + 2.0);
        leftBridgeControlView.frame = CGRectMake(eyeArea.bridgePosition.x - ADDITIONAL_CONTROL_SIZE,
                                                 eyeArea.bridgePosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                 eyeArea.bridgeLength / 2.0 + ADDITIONAL_CONTROL_SIZE,
                                                 CONTROL_SIZE);
        rightBridgeControlView.frame = CGRectMake(eyeArea.bridgePosition.x + eyeArea.bridgeLength / 2.0,
                                                  eyeArea.bridgePosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                  eyeArea.bridgeLength / 2.0 + ADDITIONAL_CONTROL_SIZE + 2.0,
                                                  CONTROL_SIZE);
        leftBridgeControlView.frame = CGRectMake(eyeArea.bridgePosition.x - ADDITIONAL_CONTROL_SIZE,
                                                 eyeArea.bridgePosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                 eyeArea.bridgeLength / 2.0 + ADDITIONAL_CONTROL_SIZE,
                                                 CONTROL_SIZE);
        rightBridgeControlView.frame = CGRectMake(eyeArea.bridgePosition.x + eyeArea.bridgeLength / 2.0,
                                                  eyeArea.bridgePosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                  eyeArea.bridgeLength / 2.0 + ADDITIONAL_CONTROL_SIZE + 2.0,
                                                  CONTROL_SIZE);
        [self refresh];
    } else if ([gesture state] == UIGestureRecognizerStateEnded || [gesture state] == UIGestureRecognizerStateCancelled) {
        zoomScrollView.scrollEnabled = YES;
    }
}

- (void)leftPdRightWidthControlPanGesture:(UIPanGestureRecognizer *)gesture
{
    zoomScrollView.scrollEnabled = NO;
    if ([gesture state] == UIGestureRecognizerStateBegan) {
        firstPoint = CGPointMake([gesture locationInView:leftPdWidthControlView].x, [gesture locationInView:leftPdWidthControlView].y);
        firstHeight = [gesture locationInView:controllersView].y;
    } else if ([gesture state] == UIGestureRecognizerStateChanged) {
        CGFloat newX = [gesture locationInView:controllersView].x - firstPoint.x;
        CGFloat newY = firstHeight - firstPoint.y;
        CGFloat offsetX = leftPdWidthControlView.frame.origin.x - newX;
        firstPoint.x -= offsetX;
        [leftPdWidthControlView setPoint:CGPointMake(newX, newY)];
        eyeArea.leftEye = CGPointMake(eyeArea.leftEye.x - offsetX, eyeArea.leftEye.y);
        [eyeArea refresh];
        leftPdWidthControlView.frame = CGRectMake(eyeArea.leftPdPosition.x,
                                                  eyeArea.leftPdPosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                  eyeArea.leftPdLength + ADDITIONAL_CONTROL_SIZE,
                                                  CONTROL_SIZE);
        leftEyeHeightTopControlView.frame = CGRectMake(eyeArea.leftHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH, 
                                                       eyeArea.leftHeightPosition.y - ADDITIONAL_CONTROL_SIZE, 
                                                       CONTROL_SIZE, 
                                                       eyeArea.leftHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE);
        leftEyeHeightBottomControlView.frame = CGRectMake(eyeArea.leftHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                          eyeArea.leftHeightPosition.y + eyeArea.leftHeightLength / 2.0,
                                                          CONTROL_SIZE,
                                                          eyeArea.leftHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE + 2.0);
        [self refresh];
    } else if ([gesture state] == UIGestureRecognizerStateEnded || [gesture state] == UIGestureRecognizerStateCancelled) {
        zoomScrollView.scrollEnabled = YES;
    }
}

- (void)rightPdLeftWidthControlPanGesture:(UIPanGestureRecognizer *)gesture
{
    zoomScrollView.scrollEnabled = NO;
    if ([gesture state] == UIGestureRecognizerStateBegan) {
        firstPoint = CGPointMake([gesture locationInView:rightPdWidthControlView].x, [gesture locationInView:rightPdWidthControlView].y);
        firstHeight = [gesture locationInView:controllersView].y;
    } else if ([gesture state] == UIGestureRecognizerStateChanged) {
        CGFloat newX = [gesture locationInView:controllersView].x - firstPoint.x;
        CGFloat newY = firstHeight - firstPoint.y;
        CGFloat offsetX = rightPdWidthControlView.frame.origin.x - newX;
        
        [rightPdWidthControlView setPoint:CGPointMake(newX, newY)];
        eyeArea.rightEye = CGPointMake(eyeArea.rightEye.x - offsetX, eyeArea.rightEye.y);
        [eyeArea refresh];
        rightPdWidthControlView.frame = CGRectMake(eyeArea.rightPdPosition.x - ADDITIONAL_CONTROL_SIZE,
                                                   eyeArea.rightPdPosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                   eyeArea.rightPdLength + ADDITIONAL_CONTROL_SIZE + 2.0,
                                                   CONTROL_SIZE);
        rightEyeHeightTopControlView.frame = CGRectMake(eyeArea.rightHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH, 
                                                        eyeArea.rightHeightPosition.y - ADDITIONAL_CONTROL_SIZE, 
                                                        CONTROL_SIZE, 
                                                        eyeArea.rightHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE);
        rightEyeHeightBottomControlView.frame = CGRectMake(eyeArea.rightHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                           eyeArea.rightHeightPosition.y + eyeArea.rightHeightLength / 2.0,
                                                           CONTROL_SIZE,
                                                           eyeArea.rightHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE + 2.0);
        [self refresh];
    } else if ([gesture state] == UIGestureRecognizerStateEnded || [gesture state] == UIGestureRecognizerStateCancelled) {
        zoomScrollView.scrollEnabled = YES;
    }
}

- (void)leftBridgeControlPanGesture:(UIPanGestureRecognizer *)gesture
{
    zoomScrollView.scrollEnabled = NO;
    if ([gesture state] == UIGestureRecognizerStateBegan) {
        firstPoint = CGPointMake([gesture locationInView:leftBridgeControlView].x, [gesture locationInView:leftBridgeControlView].y);
        firstHeight = [gesture locationInView:controllersView].y;
    } else if ([gesture state] == UIGestureRecognizerStateChanged) {
        CGFloat newX = [gesture locationInView:controllersView].x - firstPoint.x;
        CGFloat newY = firstHeight - firstPoint.y;
        CGFloat offsetX = leftBridgeControlView.frame.origin.x - newX;
        //        firstPoint.x -= offsetX;
        [leftBridgeControlView setPoint:CGPointMake(newX, newY)];
        eyeArea.rightRect = CGPointMake(eyeArea.rightRect.x - offsetX, eyeArea.rightRect.y);
        [eyeArea refresh];
        leftBridgeControlView.frame = CGRectMake(eyeArea.bridgePosition.x - ADDITIONAL_CONTROL_SIZE,
                                                 eyeArea.bridgePosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                 eyeArea.bridgeLength / 2.0 + ADDITIONAL_CONTROL_SIZE,
                                                 CONTROL_SIZE);
        leftPdWidthControlView.frame = CGRectMake(eyeArea.leftPdPosition.x,
                                                  eyeArea.leftPdPosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                  eyeArea.leftPdLength + ADDITIONAL_CONTROL_SIZE,
                                                  CONTROL_SIZE);
        rightPdWidthControlView.frame = CGRectMake(eyeArea.rightPdPosition.x - ADDITIONAL_CONTROL_SIZE,
                                                   eyeArea.rightPdPosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                   eyeArea.rightPdLength + ADDITIONAL_CONTROL_SIZE + 2.0,
                                                   CONTROL_SIZE);
        rightBridgeControlView.frame = CGRectMake(eyeArea.bridgePosition.x + eyeArea.bridgeLength / 2.0,
                                                  eyeArea.bridgePosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                  eyeArea.bridgeLength / 2.0 + ADDITIONAL_CONTROL_SIZE + 2.0,
                                                  CONTROL_SIZE);
        [self refresh];
    } else if ([gesture state] == UIGestureRecognizerStateEnded || [gesture state] == UIGestureRecognizerStateCancelled) {
        zoomScrollView.scrollEnabled = YES;
    }
}

- (void)rightBridgeControlPanGesture:(UIPanGestureRecognizer *)gesture
{
    zoomScrollView.scrollEnabled = NO;
    if ([gesture state] == UIGestureRecognizerStateBegan) {
        firstPoint = CGPointMake([gesture locationInView:rightBridgeControlView].x, [gesture locationInView:rightBridgeControlView].y);
        firstHeight = [gesture locationInView:controllersView].y;
    } else if ([gesture state] == UIGestureRecognizerStateChanged) {
        CGFloat newX = [gesture locationInView:controllersView].x - firstPoint.x;
        CGFloat newY = firstHeight - firstPoint.y;
        CGFloat offsetX = rightBridgeControlView.frame.origin.x - newX;
        firstPoint.x -= offsetX;
        [rightBridgeControlView setPoint:CGPointMake(newX, newY)];
        eyeArea.leftRect = CGPointMake(eyeArea.leftRect.x - offsetX, eyeArea.leftRect.y);
        [eyeArea refresh];
        leftBridgeControlView.frame = CGRectMake(eyeArea.bridgePosition.x - ADDITIONAL_CONTROL_SIZE,
                                                 eyeArea.bridgePosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                 eyeArea.bridgeLength / 2.0 + ADDITIONAL_CONTROL_SIZE,
                                                 CONTROL_SIZE);
        leftPdWidthControlView.frame = CGRectMake(eyeArea.leftPdPosition.x,
                                                  eyeArea.leftPdPosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                  eyeArea.leftPdLength + ADDITIONAL_CONTROL_SIZE,
                                                  CONTROL_SIZE);
        rightPdWidthControlView.frame = CGRectMake(eyeArea.rightPdPosition.x - ADDITIONAL_CONTROL_SIZE,
                                                   eyeArea.rightPdPosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                   eyeArea.rightPdLength + ADDITIONAL_CONTROL_SIZE + 2.0,
                                                   CONTROL_SIZE);
        rightBridgeControlView.frame = CGRectMake(eyeArea.bridgePosition.x + eyeArea.bridgeLength / 2.0,
                                                  eyeArea.bridgePosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                  eyeArea.bridgeLength / 2.0 + ADDITIONAL_CONTROL_SIZE + 2.0,
                                                  CONTROL_SIZE);
        [self refresh];
    } else if ([gesture state] == UIGestureRecognizerStateEnded || [gesture state] == UIGestureRecognizerStateCancelled) {
        zoomScrollView.scrollEnabled = YES;
    }
}

#pragma mark - Add Gestures

- (void)addGestures
{
    leftEyeHeightControlPanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(leftEyeHeightTopControlPanGesture:)];
    [leftEyeHeightControlPanRecognizer setMinimumNumberOfTouches:1];
    [leftEyeHeightTopControlView addGestureRecognizer:leftEyeHeightControlPanRecognizer];
    [leftEyeHeightControlPanRecognizer setDelegate:self]; 
    
    leftEyeHeightBottomControlPanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(leftEyeHeightBottomControlPanGesture:)];
    [leftEyeHeightBottomControlPanRecognizer setMinimumNumberOfTouches:1];
    [leftEyeHeightBottomControlView addGestureRecognizer:leftEyeHeightBottomControlPanRecognizer];
    [leftEyeHeightBottomControlPanRecognizer setDelegate:self]; 
    
    rightEyeHeightTopControlPanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(rightEyeHeightTopControlPanGesture:)];
    [rightEyeHeightTopControlPanRecognizer setMinimumNumberOfTouches:1];
    [rightEyeHeightTopControlView addGestureRecognizer:rightEyeHeightTopControlPanRecognizer];
    [rightEyeHeightTopControlPanRecognizer setDelegate:self]; 
    
    rightEyeHeightBottomControlPanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(rightEyeHeightBottomControlPanGesture:)];
    [rightEyeHeightBottomControlPanRecognizer setMinimumNumberOfTouches:1];
    [rightEyeHeightBottomControlView addGestureRecognizer:rightEyeHeightBottomControlPanRecognizer];
    [rightEyeHeightBottomControlPanRecognizer setDelegate:self]; 
    
    leftPdRightWidthControlPanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(leftPdRightWidthControlPanGesture:)];
    [leftPdRightWidthControlPanRecognizer setMinimumNumberOfTouches:1];
    [leftPdWidthControlView addGestureRecognizer:leftPdRightWidthControlPanRecognizer];
    [leftPdRightWidthControlPanRecognizer setDelegate:self]; 
    
    rightPdLeftWidthControlPanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(rightPdLeftWidthControlPanGesture:)];
    [rightPdLeftWidthControlPanRecognizer setMinimumNumberOfTouches:1];
    [rightPdWidthControlView addGestureRecognizer:rightPdLeftWidthControlPanRecognizer];
    [rightPdLeftWidthControlPanRecognizer setDelegate:self]; 
    
    leftBridgeControlPanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(leftBridgeControlPanGesture:)];
    [leftBridgeControlPanRecognizer setMinimumNumberOfTouches:1];
    [leftBridgeControlView addGestureRecognizer:leftBridgeControlPanRecognizer];
    [leftBridgeControlPanRecognizer setDelegate:self]; 
    
    rightBridgeControlPanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(rightBridgeControlPanGesture:)];
    [rightBridgeControlPanRecognizer setMinimumNumberOfTouches:1];
    [rightBridgeControlView addGestureRecognizer:rightBridgeControlPanRecognizer];
    [rightBridgeControlPanRecognizer setDelegate:self]; 
    
    // Diameter
    lensDiameterControlPanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(lensDiameterControlPanGesture:)];
    [lensDiameterControlPanRecognizer setMinimumNumberOfTouches:1];
    [lensDiameterView addGestureRecognizer:lensDiameterControlPanRecognizer];
    [lensDiameterControlPanRecognizer setDelegate:self];
}

- (void)removeGestures
{
    [leftEyeHeightTopControlView removeGestureRecognizer:leftEyeHeightControlPanRecognizer];
    [leftEyeHeightBottomControlView removeGestureRecognizer:leftEyeHeightBottomControlPanRecognizer];
    [rightEyeHeightTopControlView removeGestureRecognizer:rightEyeHeightTopControlPanRecognizer];
    [rightEyeHeightBottomControlView removeGestureRecognizer:rightEyeHeightBottomControlPanRecognizer];
    [leftPdWidthControlView removeGestureRecognizer:leftPdRightWidthControlPanRecognizer];
    [rightPdWidthControlView removeGestureRecognizer:rightPdLeftWidthControlPanRecognizer];
    [leftBridgeControlView removeGestureRecognizer:leftBridgeControlPanRecognizer];
    [rightBridgeControlView removeGestureRecognizer:rightBridgeControlPanRecognizer];
    [lensDiameterView removeGestureRecognizer:lensDiameterControlPanRecognizer];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    zoomScrollView.scrollEnabled = NO;
    return YES;
}

- (void)configureControlViewFrames
{
    leftEyeHeightTopControlView.frame = CGRectMake(eyeArea.leftHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH, 
                                                   eyeArea.leftHeightPosition.y - ADDITIONAL_CONTROL_SIZE, 
                                                   CONTROL_SIZE, 
                                                   eyeArea.leftHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE);
    leftEyeHeightBottomControlView.frame = CGRectMake(eyeArea.leftHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                      eyeArea.leftHeightPosition.y + eyeArea.leftHeightLength / 2.0,
                                                      CONTROL_SIZE,
                                                      eyeArea.leftHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE + 2.0);
    
    rightEyeHeightTopControlView.frame = CGRectMake(eyeArea.rightHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH, 
                                                    eyeArea.rightHeightPosition.y - ADDITIONAL_CONTROL_SIZE, 
                                                    CONTROL_SIZE, 
                                                    eyeArea.rightHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE);
    rightEyeHeightBottomControlView.frame = CGRectMake(eyeArea.rightHeightPosition.x - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                                       eyeArea.rightHeightPosition.y + eyeArea.rightHeightLength / 2.0,
                                                       CONTROL_SIZE,
                                                       eyeArea.rightHeightLength / 2.0 + ADDITIONAL_CONTROL_SIZE + 2.0);
    
    leftPdWidthControlView.frame = CGRectMake(eyeArea.leftPdPosition.x,
                                              eyeArea.leftPdPosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                              eyeArea.leftPdLength + ADDITIONAL_CONTROL_SIZE,
                                              CONTROL_SIZE);
    
    rightPdWidthControlView.frame = CGRectMake(eyeArea.rightPdPosition.x - ADDITIONAL_CONTROL_SIZE,
                                               eyeArea.rightPdPosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                               eyeArea.rightPdLength + ADDITIONAL_CONTROL_SIZE + 2.0,
                                               CONTROL_SIZE);
    
    leftBridgeControlView.frame = CGRectMake(eyeArea.bridgePosition.x - ADDITIONAL_CONTROL_SIZE,
                                             eyeArea.bridgePosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                             eyeArea.bridgeLength / 2.0 + ADDITIONAL_CONTROL_SIZE,
                                             CONTROL_SIZE);
    rightBridgeControlView.frame = CGRectMake(eyeArea.bridgePosition.x + eyeArea.bridgeLength / 2.0,
                                              eyeArea.bridgePosition.y - CONTROL_SIZE / 2.0 + ARROW_OFFSET_LENGTH,
                                              eyeArea.bridgeLength / 2.0 + ADDITIONAL_CONTROL_SIZE + 2.0,
                                              CONTROL_SIZE);
    
    [controllersView bringSubviewToFront:leftEyeHeightTopControlView];
    [controllersView bringSubviewToFront:leftEyeHeightBottomControlView];
    [controllersView bringSubviewToFront:rightEyeHeightTopControlView];
    [controllersView bringSubviewToFront:rightEyeHeightBottomControlView];
    [controllersView bringSubviewToFront:leftPdWidthControlView];
    [controllersView bringSubviewToFront:rightPdWidthControlView];
    [controllersView bringSubviewToFront:leftBridgeControlView];
    [controllersView bringSubviewToFront:rightBridgeControlView];
}

#pragma mark - SessionManager

- (void)storeData
{
    __weak __typeof(self)weakSelf = self;
    
    [SESSION_MANAGER showNameAlertFromViewController:self fillCompletion:^(BOOL finished) {
        [weakSelf storeDataToSessionManager];
    }];
}

- (void)storeDataToSessionManager
{
    CPDMDataResults farData = Results.GetPhotoResults(0);
    CPDMDataResults nearData = Results.GetPhotoResults(2);
    
    [SESSION_MANAGER createSessionFromDataManager];
    
    //add screenshot and store local session to DB
    [self addScreenshotToSessionManager];
    
    SESSION_MANAGER.currentSession.type = [self.farPdMode boolValue] ? SessionTypeFarPD : SessionTypeNearPD;
    
    if (farData.bDataResults)
    {
        ResultsData * resultData = [ResultsData new];
        
        resultData.pdLeft = farData.LeftPD;
        resultData.pdRight = farData.RightPD;
        
        resultData.heightLeft = farData.LeftHeight;
        resultData.heightRight = farData.RightHeight;
        
        resultData.frameWidth = farData.FrameWidth;
        resultData.frameHight = farData.FrameHeight;
        
        resultData.frameBridge = farData.Bridge;
        
        resultData.photoDistance = farData.Dist;
        resultData.pantoAngle = farData.alfaPantosk;
        
        resultData.wrapAngle = -5.f;
        
        if (PDMDataManager.sharedManager.topWrapValue)
        {
            resultData.wrapAngle = [PDMDataManager.sharedManager.topWrapValue doubleValue];
        }
        
        [SESSION_MANAGER addLocalResultsData:resultData forSesionType:SessionTypeFarPD];
        
        [self addWrapForSessionType:SessionTypeFarPD];
    }
    
    if (nearData.bDataResults)
    {
        ResultsData * resultData = [ResultsData new];
        
        resultData.pdLeft = nearData.LeftPD;
        resultData.pdRight = nearData.RightPD;
        
        resultData.heightLeft = nearData.LeftHeight;
        resultData.heightRight = nearData.RightHeight;
        
        resultData.frameWidth = nearData.FrameWidth;
        resultData.frameHight = nearData.FrameHeight;
        
        resultData.frameBridge = nearData.Bridge;
        
        resultData.photoDistance = nearData.Dist;
        resultData.pantoAngle = nearData.alfaPantosk;
        
        resultData.wertexDistance = nearData.LeftVD;
        
        [SESSION_MANAGER addLocalResultsData:resultData forSesionType:SessionTypeNearPD];
        
        [self addWrapForSessionType:SessionTypeNearPD];
    }
}

- (void) addWrapForSessionType:(SessionType)sessionType
{
    int iPhoto = (sessionType == SessionTypeFarPD) ? 0 : 2;
    double xLeftWrap, yLeftWrap, xRightWrap, yRightWrap;
    
    frameMarkers.GetMarkersPosition(iPhoto, true, 0, xRightWrap, yRightWrap, false);
    frameMarkers.GetMarkersPosition(iPhoto, false, 2, xLeftWrap, yLeftWrap, false);
    
    
    WrapMarkersData * wrapData = [WrapMarkersData new];
    
    wrapData.xLeftWrap = xLeftWrap;
    wrapData.yLeftWrap = yLeftWrap;
    wrapData.xRightWrap = xRightWrap;
    wrapData.yRightWrap = yRightWrap;
    
    [SESSION_MANAGER addWrapData:wrapData forSesionType:sessionType];
}

- (void)addScreenshotToSessionManager
{
    [SESSION_MANAGER addLocalResultsScreenshot:[self screenshot]];
}

- (void)updateCurrentSessionClientFirstName:(NSString*)firstName lastName:(NSString*)lastName
{    
    [SESSION_MANAGER updateCurrentSessionClientFirstName:firstName lastName:lastName];
}

- (void)showErrorStoreAlert
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Cannot store loaded session" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    [alert show];
}

#pragma marl Send Results to LPT App

- (NSData *)getResultsData
{
    CPDMDataResults farData = Results.GetPhotoResults(0);
    CPDMDataResults nearData = Results.GetPhotoResults(2);
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    if (farData.bDataResults)
    {
        NSMutableDictionary *farDictionary = [NSMutableDictionary dictionary];
        
        [farDictionary setObject:[NSNumber numberWithDouble:farData.LeftPD] forKey:@"pdLeft"];
        [farDictionary setObject:[NSNumber numberWithDouble:farData.RightPD] forKey:@"pdRight"];
        [farDictionary setObject:[NSNumber numberWithDouble:farData.LeftHeight] forKey:@"heightLeft"];
        [farDictionary setObject:[NSNumber numberWithDouble:farData.RightHeight] forKey:@"heightRight"];
        
        [dictionary setObject:farDictionary forKey:@"FarPD"];
    }
    
    if (nearData.bDataResults)
    {
        NSMutableDictionary *nearDictionary = [NSMutableDictionary dictionary];
        
        [nearDictionary setObject:[NSNumber numberWithDouble:nearData.LeftPD] forKey:@"pdLeft"];
        [nearDictionary setObject:[NSNumber numberWithDouble:nearData.RightPD] forKey:@"pdRight"];
        [nearDictionary setObject:[NSNumber numberWithDouble:nearData.LeftHeight] forKey:@"heightLeft"];
        [nearDictionary setObject:[NSNumber numberWithDouble:nearData.RightHeight] forKey:@"heightRight"];
        
        [dictionary setObject:nearDictionary forKey:@"NearPD"];
    }
    
    if (farData.bDataResults)
    {
        NSMutableDictionary *powDictionary = [NSMutableDictionary dictionary];
        
        [powDictionary setObject:[NSNumber numberWithDouble:(farData.alfaPantoskHor * 180. / M_PI)] forKey:@"anglePanto"];
        BOOL results2IsExiting = Results.CalcPhotoResults2(true);
        if(results2IsExiting) {
            CPDMDataResults2 Data;
            Data = Results.GetPhotoResults2();
            [powDictionary setObject:[NSNumber numberWithDouble:(fabs(Data.LeftVD))] forKey:@"VertexDistance"];
        }
        if (PDMDataManager.sharedManager.topWrapValue) {
            double dblWrapAngle = (kHardFrame)? PDM_DATA.topWrapValue.doubleValue : PDM_DATA.topWrapValue.doubleValue - SEMWRAP;
            [powDictionary setObject:[NSNumber numberWithDouble:dblWrapAngle] forKey:@"angleWrap"];
        }
        
        [dictionary setObject:powDictionary forKey:@"POW"];
    }

    return [NSKeyedArchiver archivedDataWithRootObject:dictionary];
}

- (void)sendResultsToLPTApp
{
    NSData *data = [self getResultsData];
    
    [PDDataSharing sendDataToApplicationWithScheme:[PDDataSharing targetScheme] dataPackage:data
                                 completionHandler:^(BOOL sent, NSError *error) {
                                        
        if (sent)
        {
            NSLog(@"Data Package Sent");
        }
        else if (error)
        {
            NSLog(@"Error sending data package: %@", [error localizedDescription]);
        }
    }];
}

- (NSData *)createPDF:(int)sizePDF
{
    self.view.frame = CGRectIntegral(self.view.frame);
    
    BOOL a4 = YES;
    
    if (sizePDF == 5)
        a4 = NO;
    
    // Resize Image
    CGRect resizeRect = CGRectMake(0.f, 0.f, 1024.f, 699.f);
    UIGraphicsBeginImageContext( resizeRect.size );
    [[self screenshot] drawInRect:resizeRect];
    UIImage *resizeImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIFont *headerTitleTextFont = a4 ? [UIFont fontWithName:@"Helvetica-Bold" size:14.f] : [UIFont fontWithName:@"Helvetica-Bold" size:7.f];
    UIFont *titleTextFont = a4 ? [UIFont fontWithName:@"Helvetica-Bold" size:14.f] : [UIFont fontWithName:@"Helvetica-Bold" size:7.f];
    UIFont *boldTextFont = a4 ? [UIFont fontWithName:@"Helvetica-Bold" size:14.f] : [UIFont fontWithName:@"Helvetica-Bold" size:7.f];
    UIFont *regularTextFont = a4 ? [UIFont fontWithName:@"Helvetica" size:14.f] : [UIFont fontWithName:@"Helvetica" size:7.f];
    //UIFont *textFontTime = a4 ? [UIFont fontWithName:@"Helvetica" size:12.f] : [UIFont fontWithName:@"Helvetica" size:6.f];
    
    NSMutableData *pdfData = [[NSMutableData alloc] init];
    
    UIGraphicsBeginPDFContextToData(pdfData, self.view.frame, nil);
    
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, a4 ? 768.f : 384.f, a4 ? 1024.f : 512.f), nil);

    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"dd.MM.yyyy"];
    NSString *timeStampString = [df stringFromDate:[NSDate date]];
    
    // Header block
    
    UIImage *headerImage = [UIImage imageNamed:@"pdf_header"];
    
    NSString *pdf_title = NSLocalizedString(@"PD Measurement results", @"");
    
    [headerImage drawInRect:CGRectMake(0.0, 0.0, a4 ? headerImage.size.width : headerImage.size.width/2, a4 ? headerImage.size.height : headerImage.size.height/2)];
    
    NSDictionary *attributes = @{ NSFontAttributeName : headerTitleTextFont, NSForegroundColorAttributeName : [UIColor blackColor] };
    
    [pdf_title drawInRect:CGRectMake(a4 ? 768.f / 2 - [pdf_title sizeWithAttributes:attributes].width/2 : 384.f / 2 - [pdf_title sizeWithAttributes:attributes].width/2,
                                     a4 ? 40 : 20,
                                     [pdf_title sizeWithAttributes:attributes].width,
                                     [pdf_title sizeWithAttributes:attributes].height)
           withAttributes:attributes];
    
    // Customer name
    
    NSString *customerName = [NSString stringWithFormat:NSLocalizedString(@"Customer: %@ %@", nil), m_usrname, m_usrsurname];
    
    //NSDictionary *attributes1 = [[NSDictionary alloc] initWithObjectsAndKeys:textFontTime, NSFontAttributeName, nil];
    NSDictionary *attributes2 = [[NSDictionary alloc] initWithObjectsAndKeys:titleTextFont, NSFontAttributeName, nil];
    
    UIImage *bgInfo = [UIImage imageNamed:@"whiteBG"];
    [bgInfo drawInRect:CGRectMake(a4 ? headerImage.size.width - [customerName sizeWithAttributes:attributes2].width - 40.0 :
                                       headerImage.size.width/2 - [customerName sizeWithAttributes:attributes2].width - (40.0/2),
                                
                                a4 ? 19.0 : 19.0/2,
                                a4 ? 20.0 + [customerName sizeWithAttributes:attributes2].width : (20.0/2) + [customerName sizeWithAttributes:attributes2].width,
                                a4 ? 54.0 : 54.0/2)];
    
    [customerName drawInRect:CGRectMake(a4 ? headerImage.size.width - [customerName sizeWithAttributes:attributes2].width - 30.0 :
                                             headerImage.size.width/2 - [customerName sizeWithAttributes:attributes2].width - (30.0/2),
                                        a4 ? 30.0 : 30.0/2,
                                        [customerName sizeWithAttributes:attributes].width,
                                        [customerName sizeWithAttributes:attributes].height)
              withAttributes:attributes];
    
    [timeStampString drawInRect:CGRectMake(a4 ? headerImage.size.width - [timeStampString sizeWithAttributes:attributes].width - 30.f :
                                                headerImage.size.width/2 - [timeStampString sizeWithAttributes:attributes].width - 15.f,
                                           a4 ? 50.0 : 50/2,
                                           [timeStampString sizeWithAttributes:attributes].width,
                                           [timeStampString sizeWithAttributes:attributes].height)
                 withAttributes:attributes];
    
    // Screenshot block
    
    UIImage *imageBack = [UIImage imageNamed:@"pdf_scr_background"];
    
    [imageBack drawInRect:CGRectMake(a4 ? (768.f - imageBack.size.width) / 2 : (384.f - (imageBack.size.width / 2)) / 2,
                                     a4 ? 110.0 : 55.0,
                                     a4 ? imageBack.size.width : imageBack.size.width / 2,
                                     a4 ? imageBack.size.height : imageBack.size.height / 2)];
    
    [resizeImg drawInRect:CGRectMake(a4 ? (768.f - resizeImg.size.width * 0.7) / 2 : (384.f - ((resizeImg.size.width / 2) * 0.7)) / 2,
                                 a4 ? 116.0 : 58.0,
                                 a4 ? resizeImg.size.width * 0.7 : (resizeImg.size.width / 2) * 0.7,
                                 a4 ? resizeImg.size.height * 0.7 : (resizeImg.size.height / 2) * 0.7)];
    
    // Body
    
    UIImage *bodyImage = [UIImage imageNamed:@"pdf_body"];
    
    [bodyImage drawInRect:CGRectMake(a4 ? (768.f - bodyImage.size.width) / 2 : (384.f - (bodyImage.size.width / 2)) / 2,
                                     a4 ? 631.0 : 315.0,
                                     a4 ? bodyImage.size.width : bodyImage.size.width / 2,
                                     a4 ? bodyImage.size.height : bodyImage.size.height / 2)];
    
    // Labels
    
    NSString *farVisionLabel  = farVisionBlockLabel.text;
    NSString *nearVisionLabel = nearVisionBlockLabel.text;
    NSString *positionOfwear  = personalizedBlockLabel.text;
    //NSString *progressiveCor  = NSLocalizedString(@"Progressive corridor", @"");
    NSString *frameLabel      = frameBlockLabel.text;
    
    [farVisionLabel drawInRect:CGRectMake(a4 ? 20.0 : 10.0,
                                          a4 ? 718.0 : 359.0,
                                          [farVisionLabel sizeWithAttributes:attributes].width,
                                          [farVisionLabel sizeWithAttributes:attributes].height)
                withAttributes:attributes];
    
    [nearVisionLabel drawInRect:CGRectMake(a4 ? 420.0 : 210.0,
                                           a4 ? 718.0 : 359.0,
                                           [nearVisionLabel sizeWithAttributes:attributes].width,
                                           [nearVisionLabel sizeWithAttributes:attributes].height)
                 withAttributes:attributes];
    
    [positionOfwear drawInRect:CGRectMake(a4 ? 20.0 : 10.0,
                                          a4 ? 616.0 : 308.0,
                                          [positionOfwear sizeWithAttributes:attributes].width,
                                          [positionOfwear sizeWithAttributes:attributes].height)
                withAttributes:attributes];
    
//    [progressiveCor drawInRect:CGRectMake(a4 ? 23.0 : 12.0,
//                                          a4 ? 805.0 : 403.0,
//                                          [progressiveCor sizeWithAttributes:attributes].width,
//                                          [progressiveCor sizeWithAttributes:attributes].height)
//                withAttributes:attributes];
    
    [frameLabel drawInRect:CGRectMake(a4 ? 20.0 : 10.0,
                                      a4 ? 859.0 : 429.0,
                                      [frameLabel sizeWithAttributes:attributes].width,
                                      [frameLabel sizeWithAttributes:attributes].height)
            withAttributes:attributes];
    
    NSString *PD              = NSLocalizedString(@"PD", nil);
    NSString *fittingHeight   = NSLocalizedString(@"Fitting Height", nil);
    NSString *readingDistance = NSLocalizedString(@"Reading distance, cm", nil);
    NSString *panto           = NSLocalizedString(@"Panto Tilt", nil);
    NSString *vertex          = NSLocalizedString(@"Vertex", nil);
    NSString *wrap            = NSLocalizedString(@"Wrap", nil);
    NSString *A               = NSLocalizedString(@"A", nil);
    NSString *B               = NSLocalizedString(@"B", nil);
    NSString *dBL             = NSLocalizedString(@"DBL", nil);
    NSString *bevel           = NSLocalizedString(@"Bevel", nil);
    NSString *type            = NSLocalizedString(@"Type", @"");
    NSString *R               = NSLocalizedString(@"R", @"");
    NSString *L               = NSLocalizedString(@"L", @"");
    NSString *inset           = NSLocalizedString(@"Inset", @"");
    double xn = 0., yn = 0.;
    
    [[UIColor blackColor] set];
    
    NSDictionary *attributes3 = [[NSDictionary alloc] initWithObjectsAndKeys:boldTextFont, NSFontAttributeName, nil];
    
    xn = 138.;
    xn = (a4 ? xn : xn / 2.) - [PD sizeWithAttributes:attributes2].width / 2.;
    yn = 746.;
    yn = (a4 ? yn : yn / 2.);
    [PD drawInRect:CGRectMake(xn,
                              yn,
                              [PD sizeWithAttributes:attributes2].width,
                              [PD sizeWithAttributes:attributes2].height)
    withAttributes:attributes3];
    
    xn = 263.;
    xn = (a4 ? xn : xn / 2.) - [fittingHeight sizeWithAttributes:attributes2].width / 2.;
    yn = 746.;
    yn = (a4 ? yn : yn / 2.);
    [fittingHeight drawInRect:CGRectMake(xn,
                                         yn,
                                         [fittingHeight sizeWithAttributes:attributes2].width,
                                         [fittingHeight sizeWithAttributes:attributes2].height)
               withAttributes:attributes3];
    
    xn = 538.;
    xn = (a4 ? xn : xn / 2.) - [PD sizeWithAttributes:attributes2].width / 2.;
    yn = 746.;
    yn = (a4 ? yn : yn / 2.);
    [PD drawInRect:CGRectMake(xn,
                              yn,
                              [PD sizeWithAttributes:attributes2].width,
                              [PD sizeWithAttributes:attributes2].height)
    withAttributes:attributes3];
    
    xn = 663.;
    xn = (a4 ? xn : xn / 2.) - [inset sizeWithAttributes:attributes2].width / 2.;
    yn = 746.;
    yn = (a4 ? yn : yn / 2.);
    [inset drawInRect:CGRectMake(xn,
                                 yn,
                                 [inset sizeWithAttributes:attributes2].width,
                                 [inset sizeWithAttributes:attributes2].height)
       withAttributes:attributes3];
    
    xn = 112.;
    xn = (a4 ? xn : xn / 2.) - [readingDistance sizeWithAttributes:attributes2].width / 2.;
    yn = 644.;
    yn = (a4 ? yn : yn / 2.);
    [readingDistance drawInRect:CGRectMake(xn,
                                           yn,
                                           [readingDistance sizeWithAttributes:attributes2].width,
                                           [readingDistance sizeWithAttributes:attributes2].height)
                 withAttributes:attributes3];
    
    xn = 294.;
    xn = (a4 ? xn : xn / 2.) - [panto sizeWithAttributes:attributes2].width / 2.;
    yn = 644.;
    yn = (a4 ? yn : yn / 2.);
    [panto drawInRect:CGRectMake(xn,
                                 yn,
                                 [panto sizeWithAttributes:attributes2].width,
                                 [panto sizeWithAttributes:attributes2].height)
       withAttributes:attributes3];

    xn = 476.;
    xn = (a4 ? xn : xn / 2.) - [vertex sizeWithAttributes:attributes2].width / 2.;
    yn = 644.;
    yn = (a4 ? yn : yn / 2.);
    [vertex drawInRect:CGRectMake(xn,
                                  yn,
                                  [vertex sizeWithAttributes:attributes2].width,
                                  [vertex sizeWithAttributes:attributes2].height)
        withAttributes:attributes3];

    xn = 657.;
    xn = (a4 ? xn : xn / 2.) - [wrap sizeWithAttributes:attributes2].width / 2.;
    yn = 644.;
    yn = (a4 ? yn : yn / 2.);
    [wrap drawInRect:CGRectMake(xn,
                                yn,
                                [wrap sizeWithAttributes:attributes2].width,
                                [wrap sizeWithAttributes:attributes2].height)
      withAttributes:attributes3];

    xn = 93.;
    xn = (a4 ? xn : xn / 2.) - [A sizeWithAttributes:attributes2].width / 2.;
    yn = 887.;
    yn = (a4 ? yn : yn / 2.);
    [A drawInRect:CGRectMake(xn,
                             yn,
                             [A sizeWithAttributes:attributes2].width,
                             [A sizeWithAttributes:attributes2].height)
   withAttributes:attributes3];
    
    xn = 239.;
    xn = (a4 ? xn : xn / 2.) - [B sizeWithAttributes:attributes2].width / 2.;
    yn = 887.;
    yn = (a4 ? yn : yn / 2.);
    [B drawInRect:CGRectMake(xn,
                             yn,
                             [B sizeWithAttributes:attributes2].width,
                             [B sizeWithAttributes:attributes2].height)
   withAttributes:attributes3];
    
    xn = 384.;
    xn = (a4 ? xn : xn / 2.) - [dBL sizeWithAttributes:attributes2].width / 2.;
    yn = 887.;
    yn = (a4 ? yn : yn / 2.);
    [dBL drawInRect:CGRectMake(xn,
                               yn,
                               [dBL sizeWithAttributes:attributes2].width,
                               [dBL sizeWithAttributes:attributes2].height)
     withAttributes:attributes3];
    
    xn = 529.;
    xn = (a4 ? xn : xn / 2.) - [bevel sizeWithAttributes:attributes2].width / 2.;
    yn = 887.;
    yn = (a4 ? yn : yn / 2.);
    [bevel drawInRect:CGRectMake(xn,
                                 yn,
                                 [bevel sizeWithAttributes:attributes2].width,
                                 [bevel sizeWithAttributes:attributes2].height)
       withAttributes:attributes3];
    
    xn = 675.;
    xn = (a4 ? xn : xn / 2.) - [type sizeWithAttributes:attributes2].width / 2.;
    yn = 887.;
    yn = (a4 ? yn : yn / 2.);
    [type drawInRect:CGRectMake(xn,
                                yn,
                                [type sizeWithAttributes:attributes2].width,
                                [type sizeWithAttributes:attributes2].height)
      withAttributes:attributes3];
    
    NSDictionary *attributes4 = [[NSDictionary alloc] initWithObjectsAndKeys:regularTextFont, NSFontAttributeName, nil];
    
    [R drawInRect:CGRectMake(a4 ? 55.0 : 27.0,
                             a4 ? 786.0 : 393.0,
                             [R sizeWithAttributes:attributes2].width,
                             [R sizeWithAttributes:attributes2].height)
   withAttributes:attributes4];
    
    [L drawInRect:CGRectMake(a4 ? 55.0 : 27.0,
                             a4 ? 824.0 : 412.0,
                             [L sizeWithAttributes:attributes2].width,
                             [L sizeWithAttributes:attributes2].height)
   withAttributes:attributes4];
    
    [rightPD.text drawInRect:CGRectMake(a4 ? 126.0 : 63.0,
                                        a4 ? 786.0 : 393.0,
                                        self.view.frame.size.width,
                                        [PD sizeWithAttributes:attributes2].height)
              withAttributes:attributes4];

    [leftPD.text drawInRect:CGRectMake(a4 ? 126.0 : 63.0,
                                       a4 ? 824.0 : 412.0,
                                       self.view.frame.size.width,
                                       [PD sizeWithAttributes:attributes2].height)
             withAttributes:attributes4];

    [rightHeight.text drawInRect:CGRectMake(a4 ? 245.0 : 122.0,
                                            a4 ? 786.0 : 393.0,
                                            self.view.frame.size.width,
                                            [PD sizeWithAttributes:attributes2].height)
                  withAttributes:attributes4];
    
    [leftHeight.text drawInRect:CGRectMake(a4 ? 245.0 : 122.0,
                                           a4 ? 824.0 : 412.0,
                                           self.view.frame.size.width,
                                           [PD sizeWithAttributes:attributes2].height)
                 withAttributes:attributes4];

    [R drawInRect:CGRectMake(a4 ? 455.0 : 227.0,
                             a4 ? 786.0 : 393.0,
                             [R sizeWithAttributes:attributes2].width,
                             [R sizeWithAttributes:attributes2].height)
   withAttributes:attributes4];
    
    [L drawInRect:CGRectMake(a4 ? 455.0 : 227.0,
                             a4 ? 824.0 : 412.0,
                             [L sizeWithAttributes:attributes2].width,
                             [L sizeWithAttributes:attributes2].height)
   withAttributes:attributes4];

    [rightNearPD.text drawInRect:CGRectMake(a4 ? 528.0 : 264.0,
                                            a4 ? 786.0 : 393.0,
                                            self.view.frame.size.width,
                                            [PD sizeWithAttributes:attributes2].height)
                  withAttributes:attributes4];
    
    [leftNearPD.text drawInRect:CGRectMake(a4 ? 528.0 : 264.0,
                                           a4 ? 824.0 : 412.0,
                                           self.view.frame.size.width,
                                           [PD sizeWithAttributes:attributes2].height)
                 withAttributes:attributes4];
    
    [insertRField.text drawInRect:CGRectMake(a4 ? 528.0+128.0 : 264.0+64.0,
                                             a4 ? 786.0 : 393.0,
                                             self.view.frame.size.width,
                                             [PD sizeWithAttributes:attributes2].height)
                   withAttributes:attributes4];
    
    [insertLField.text drawInRect:CGRectMake(a4 ? 528.0+128.0 : 264.0+64.0,
                                             a4 ? 824.0 : 412.0,
                                             self.view.frame.size.width,
                                             [PD sizeWithAttributes:attributes2].height)
                   withAttributes:attributes4];
    
    [distanceToIPadTextField.text drawInRect:CGRectMake(a4 ? 98.0 : 49.0,
                                                        a4 ? 684.0 : 342.0,
                                                        self.view.frame.size.width,
                                                        [PD sizeWithAttributes:attributes2].height)
                              withAttributes:attributes4];

    [headPosture.text drawInRect:CGRectMake(a4 ? 274.0 : 137.0,
                                            a4 ? 684.0 : 342.0,
                                            self.view.frame.size.width,
                                            [PD sizeWithAttributes:attributes2].height)
                  withAttributes:attributes4];

    [VDTextField.text drawInRect:CGRectMake(a4 ? 454.0 : 227.0,
                                            a4 ? 684.0 : 342.0,
                                            self.view.frame.size.width,
                                            [PD sizeWithAttributes:attributes2].height)
                  withAttributes:attributes4];

    [wrapTextField.text drawInRect:CGRectMake(a4 ? 641.0 : 320.0,
                                              a4 ? 684.0 : 342.0,
                                              self.view.frame.size.width,
                                              [PD sizeWithAttributes:attributes2].height)
                    withAttributes:attributes4];
    
//    if (progressiveScheme)
//    {
//        [ProgressiveTypeLabel.text drawInRect:CGRectMake(a4 ? 90.0 : 45.0,
//                                                         a4 ? 835.0 : 418.0,
//                                                         self.view.frame.size.width,
//                                                         [PD sizeWithAttributes:attributes2].height)
//                               withAttributes:attributes4];
//    }
    
    [frameWidthTextField.text drawInRect:CGRectMake(a4 ? 73.0 : 36.0,
                                                    a4 ? 926.0 : 463.0,
                                                    self.view.frame.size.width,
                                                    [PD sizeWithAttributes:attributes2].height)
                          withAttributes:attributes4];

    [frameHeightTextField.text drawInRect:CGRectMake(a4 ? 222.0 : 111.0,
                                                     a4 ? 926.0 : 463.0,
                                                     self.view.frame.size.width,
                                                     [PD sizeWithAttributes:attributes2].height)
                           withAttributes:attributes4];

    [bridge.text drawInRect:CGRectMake(a4 ? 371.0 : 185.0,
                                       a4 ? 926.0 : 463.0,
                                       self.view.frame.size.width,
                                       [PD sizeWithAttributes:attributes2].height)
             withAttributes:attributes4];
    
    [showBevelLabel.text drawInRect:CGRectMake(a4 ? 520.0 : 260.0,
                                               a4 ? 926.0 : 463.0,
                                               self.view.frame.size.width,
                                               [PD sizeWithAttributes:attributes2].height)
                     withAttributes:attributes4];
    
    [showTypeLabel.text drawInRect:CGRectMake(a4 ? 658.0 : 329.0,
                                              a4 ? 926.0 : 463.0,
                                              self.view.frame.size.width,
                                              [PD sizeWithAttributes:attributes2].height)
                    withAttributes:attributes4];
    
    // Footer block
    
    UIImage *footerImage = [UIImage imageNamed:@"pdf_footer"];
    
    [footerImage drawInRect:CGRectMake(0.0,
                                       a4 ? 1024.0 - footerImage.size.height : 512.0 - (footerImage.size.height / 2),
                                       a4 ? footerImage.size.width : footerImage.size.width / 2,
                                       a4 ? footerImage.size.height : footerImage.size.height / 2)];
    
    UIGraphicsEndPDFContext();
    
    return pdfData;
}

- (void)drawLineFromPoint:(CGPoint)from toPoint:(CGPoint)to
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 1.0);
    
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    
    CGFloat components[] = {0.0, 0.0, 0.0, 1.0};
    
    CGColorRef color = CGColorCreate(colorspace, components);
    
    CGContextSetStrokeColorWithColor(context, color);
    
    CGContextMoveToPoint(context, from.x, from.y);
    
    CGContextAddLineToPoint(context, to.x, to.y);
    
    CGContextStrokePath(context);
}

- (CGFloat)lengthLeftPart:(NSString*)string withFont:(UIFont*)font
{
    return [[[string componentsSeparatedByString:@"."] objectAtIndex:0] sizeWithFont:font].width;
}

- (void)sendPDFViaEmail:(int)sizePDF
{
    MFMailComposeViewController *picker = [MFMailComposeViewController new];
    picker.mailComposeDelegate = self;
    
    [picker setSubject:NSLocalizedString(@"PD Measurement results", @"")];
    
    NSString *timeStampString;
    {
        NSDate *currentDate = [NSDate date];
        
        NSDateFormatter *df = [NSDateFormatter new];
        [df setDateFormat:@"dd-MMMM-yyyy"];
        timeStampString = [df stringFromDate:currentDate];
        
        NSString *fileName = [NSString stringWithFormat:@"PD_%@.pdf", timeStampString];
        
        NSData *pdfData = [self createPDF:sizePDF];
        
        if (pdfData != nil)
            [picker addAttachmentData:pdfData mimeType:@"application" fileName:fileName];
    }
    
    // Fill out the email body text
    @try {
        [self presentViewController:picker animated:YES completion:nil];
    }
    @catch (NSException *exception) {
        //
    }
    @finally {
        //
    }
}

#pragma mark Material and Bevel methods

- (void)initMaterialArray
{
    frameTypes = @[NSLocalizedString(@"Metal", @""), NSLocalizedString(@"Acetate", @""), NSLocalizedString(@"Nylor", @"")];
}

- (IBAction)selectFrameMaterial:(id)sender
{
    if (!self.popoverController)
    {
        popoverMaterialController = [[PDPopoverTableMaterialController alloc] initWithParent:self andStyle:UITableViewStylePlain];
        self.popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverMaterialController];
        self.popoverController.delegate = self;
        [self.popoverController presentPopoverFromRect:CGRectMake(390, 548, 100, 1) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft  animated:YES];
    }
    else
    {
        [self.popoverController dismissPopoverAnimated:YES];
        self.popoverController = nil;
    }
}

- (IBAction)selectBevel:(id)sender
{
    int materialIndex = usrMaterial ? [usrMaterial intValue] : [sysMaterial intValue];
    NSArray *bevels = materialIndex == 2 ? nylorBevels : metalOrAcetateBevels;
    
    if (!self.popoverController)
    {
        popoverBevelController = [[PDPopoverTableBevelController alloc] initWithParent:self withArray:bevels andStyle:UITableViewStylePlain];
        self.popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverBevelController];
        self.popoverController.delegate = self;
        [self.popoverController presentPopoverFromRect:CGRectMake(390, 594, 100, 1) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft  animated:YES];
    }
    else
    {
        [self.popoverController dismissPopoverAnimated:YES];
        self.popoverController = nil;
    }
}

- (void)setBevelLabel:(bool)bRefresh
{
    int materialIndex = usrMaterial ? [usrMaterial intValue] : [sysMaterial intValue];
    
    float bevelValue = 0.0;
    
    if (usrBevel)
    {
        bevelValue = [usrBevel floatValue];
    }
    else
    {
        switch (materialIndex) {
            case 0:
                bevelValue = [metalBevel floatValue];
                break;
            case 1:
                bevelValue = [acetateBevel floatValue];
                break;
            case 2:
                bevelValue = [nylorBevel floatValue];
                break;
            default:
                break;
        }
    }
    
    [showBevelLabel setText:[NSString stringWithFormat:@"%.1f", bevelValue]];
    
    if (bRefresh)
        [self refresh];
}

- (double)getBevelValue
{
    int materialIndex = usrMaterial ? [usrMaterial intValue] : [sysMaterial intValue];
    
    double bevelValue = 0.0;
    
    if ([PD_MANAGER inExpertMode]) {
        if (usrBevel)
        {
            bevelValue = [usrBevel floatValue];
        }
        else
        {
            switch (materialIndex) {
                case 0:
                    bevelValue = [metalBevel floatValue];
                    break;
                case 1:
                    bevelValue = [acetateBevel floatValue];
                    break;
                case 2:
                    bevelValue = -[nylorBevel floatValue];
                    break;
                default:
                    break;
            }
        }
    }
    
    return bevelValue;
}

- (void)setMaterialLabel:(bool)bRefresh
{
    if (usrMaterial)
    {
        [showTypeLabel setText:[frameTypes objectAtIndex:[usrMaterial intValue]]];
    }
    else
    {
        [showTypeLabel setText:[frameTypes objectAtIndex:[sysMaterial intValue]]];
    }
    
    [self setBevelLabel:bRefresh];
}

- (NSString *)getMaterial
{
    if (usrMaterial)
        return frameTypes[[usrMaterial intValue]];
    else
        return frameTypes[[sysMaterial intValue]];
}

#pragma mark Autorotate

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

#pragma mark Open SM module

- (IBAction)openModuleInSM:(id)sender
{
    needDetectFlashlightType = YES;
    
    [PD_MANAGER setTargetScreen:PDTargetScreenResults];
    
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
    bool bProgressiveStatus = Results.GetProgressiveDiameterStatus();
    [APP_MANAGER openMLCController:((bProgressiveStatus)? YES : NO)];
}

#pragma mark - Provision Expiration Date Checking

- (void)showProvisionAlert
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [self placeBackgoundView];
    
    if (!provisionController)
    {
        provisionController = [storyboard instantiateViewControllerWithIdentifier:@"ProvisionAlertViewController"];
        provisionController.delegate = self;
        provisionController.view.frame = CGRectMake((self.view.bounds.size.width - 440.f) / 2,
                                                    (self.view.bounds.size.height - 180.f) / 2,
                                                    440.f,
                                                    180.f);
        
        [self.view addSubview:provisionController.view];
        
        [provisionController.view showInfoViewInPoint:CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2)];
    }
}

- (void)dismissProvisionAlert
{
    [provisionController.view hideInfoViewWithAnimation];
    provisionController = nil;
    
    [self removeBackgoundView];
}

#pragma mark - Back View For Popovers

- (void)placeBackgoundView
{
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024.f, 1024.f)];
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0;
    [self.view addSubview:backgroundView];
    
    [UIView animateWithDuration:0.4f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        backgroundView.alpha = 0.4f;
    } completion:nil];
}

- (void)removeBackgoundView
{
    [UIView animateWithDuration:0.4f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        backgroundView.alpha = 0;
    } completion:^(BOOL finished){
        [backgroundView removeFromSuperview];
        backgroundView = nil;
    }];
}

@end
