#import "PDMMarkersController.h"
#import "PDConfigManager.h"
#import "PDMDataManager.h"
#import "detection.h"
#import "Utils.h"
#import "RotatePicture.h"
#import "PDHelpViewController.h"
#import "PDMResults.h"
#import "UIImage+Crop.h"
#import "UIView+Point.h"
#import "UIView+Effects.h"
#import "PDDeviceInfo.h"

//#import "detection_types.h"
//#import "opencvheaders.h"
//#import "UIImageView+Point.h"
//#import "PDMDataManager.h"
//#import "PDMCameraController.h"
//#include <string.h>

#define MAX_ZOOM_SCALE 2.0

static CGFloat markerOffset = 31.0;
static CGFloat widthScale = 1.0;
static CGFloat heightScale = 1.0;

const double X_CENTER = 619;
const double Y_CENTER = 384;
const double X_FRAME = 1238;
const double Y_FRAME = 768;

@interface PDMMarkersController ()
{
    UIPanGestureRecognizer *marker1PanRecognizer;
    UIPanGestureRecognizer *marker2PanRecognizer;
    UIPanGestureRecognizer *marker3PanRecognizer;
    UIPanGestureRecognizer *marker4PanRecognizer;
    UIPanGestureRecognizer *marker5PanRecognizer;
    
    cv::Point support_point_a, support_point_b, support_point_c;
    cv::Point left_eye, right_eye;
    CGPoint support_point_a_user, support_point_b_user, support_point_c_user;
    CGPoint left_eye_user, right_eye_user;
    
    BOOL supportIsMoved;
    BOOL eyePointsIsMoved;
}

@property (nonatomic, assign) CGPoint firstLeftSupportPoint;
@property (nonatomic, assign) CGPoint firstRightSupportPoint;
@property (nonatomic, assign) CGPoint firstCenterSupportPoint;
@property (nonatomic, assign) CGPoint firstLeftEyeMarkerPoint;
@property (nonatomic, assign) CGPoint firstRightEyeMarkerPoint;
@property (nonatomic, strong) NSMutableDictionary *measurement;

- (IBAction)donePressed:(id)sender;
- (IBAction)btnHelp_Click:(id)sender;

- (void)marker1PanGesture:(UIPanGestureRecognizer*)panRecognizer;
- (void)marker2PanGesture:(UIPanGestureRecognizer*)panRecognizer;
- (void)marker3PanGesture:(UIPanGestureRecognizer*)panRecognizer;
- (void)marker4PanGesture:(UIPanGestureRecognizer*)panRecognizer;
- (void)marker5PanGesture:(UIPanGestureRecognizer*)panRecognizer;
- (void)moveMarker:(UIView *)marker panRecognizer:(UIPanGestureRecognizer*)panRecognizer andMarkerTag: (NSInteger)markerTag;
- (void)savePositions;
- (void)saveDefaultPositions;
- (void)saveData:(BOOL)rotated;
- (void)placeMarkersOnTheirPositions;
- (void)getRotatedCoordinates;

@end

@implementation PDMMarkersController

@synthesize vdMode;
@synthesize scrollView;
@synthesize scrollContentView;
@synthesize secondPhotoMode;
@synthesize farPdMode;
@synthesize snapImage;
@synthesize imageView;
@synthesize marker1;
@synthesize marker2;
@synthesize marker3;
@synthesize marker4;
@synthesize marker5;
@synthesize measurement;
@synthesize backButton;

@synthesize firstLeftSupportPoint;
@synthesize firstRightSupportPoint;
@synthesize firstCenterSupportPoint;
@synthesize firstLeftEyeMarkerPoint;
@synthesize firstRightEyeMarkerPoint;

extern CPDMResults Results;
CRotatePicture rPic[3];
extern BOOL needDetectFlashlightType;

double xLeftSupport = 0.;
double yLeftSupport = 0.;
double xRightSupport = 0.;
double yRightSupport = 0.;
double xCenterSupport = 0.;
double yCenterSupport = 0.;
double xLeftEye = 0.;
double yLeftEye = 0.;
double xRightEye = 0.;
double yRightEye = 0.;

double xRotLeftSupport = 0.;
double yRotLeftSupport = 0.;
double xRotRightSupport = 0.;
double yRotRightSupport = 0.;
double xRotCenterSupport = 0.;
double yRotCenterSupport = 0.;
double xRotLeftEye = 0.;
double yRotLeftEye = 0;
double xRotRightEye = 0.;
double yRotRightEye = 0.;

double xDefLeftSupport = 0.;
double yDefLeftSupport = 0.;
double xDefRightSupport = 0.;
double yDefRightSupport = 0.;
double xDefCenterSupport = 0.;
double yDefCenterSupport = 0.;
double xDefLeftEye = 0.;
double yDefLeftEye = 0;
double xDefRightEye = 0.;
double yDefRightEye = 0.;

#pragma mark - Load

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    supportIsMoved = NO;
    eyePointsIsMoved = NO;
    
    measurement = [PDMDataManager sharedManager].storage;
    
    scrollView.bouncesZoom = NO;
    
    snapImage = [[UIImage alloc] initWithCGImage:snapImage.CGImage];
    
    [self saveDefaultPositions];
    
    marker1PanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(marker1PanGesture:)];
    [marker1PanRecognizer setMinimumNumberOfTouches:1];
    [marker1 addGestureRecognizer:marker1PanRecognizer];
    
    marker2PanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(marker2PanGesture:)];
    [marker2PanRecognizer setMinimumNumberOfTouches:1];
    [marker2 addGestureRecognizer:marker2PanRecognizer];
    
    marker3PanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(marker3PanGesture:)];
    [marker3PanRecognizer setMinimumNumberOfTouches:1];
    [marker3 addGestureRecognizer:marker3PanRecognizer];
    
    marker4PanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(marker4PanGesture:)];
    [marker4PanRecognizer setMinimumNumberOfTouches:1];
    [marker4 addGestureRecognizer:marker4PanRecognizer];
    
    marker5PanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(marker5PanGesture:)];
    [marker5PanRecognizer setMinimumNumberOfTouches:1];
    [marker5 addGestureRecognizer:marker5PanRecognizer];
    
    [self configureDidLoad];
    
    m_bottomLabel.text = NSLocalizedString(@"Please correct markers positions if needed", @"");
}

#pragma mark - ViewDidAppear

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark - ViewWillAppear

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([PD_MANAGER isLoadedSession]) {
        
        if (PDM_DATA.currentSessionType == PDMSessionProgressive && PDM_DATA.currentPhotoStep == PDMPhotoNear)
            [backButton setEnabled:YES];
        else
            [backButton setEnabled:NO];
    } else {
        [backButton setEnabled:YES];
    }
    
    [scrollView setZoomScale:1.0];
    scrollView.maximumZoomScale = 1.0;
    scrollView.bouncesZoom = NO;
    
    if (PDM_DATA.currentSessionType == PDMSessionProgressive && PDM_DATA.currentPhotoStep == PDMPhotoNear)
        farPdMode = @(0);
    else if (PDM_DATA.currentSessionType == PDMSessionProgressive && PDM_DATA.currentPhotoStep == PDMPhotoFar)
        farPdMode = @(1);
    else
        farPdMode = @([PDM_DATA getFarMode]);
    
    if ([farPdMode boolValue])
    {
        snapImage = [PDMDataManager sharedManager].farPDImage;
        [imageView setImage:snapImage];
    }
    else if (![farPdMode boolValue])
    {
        snapImage = [PDMDataManager sharedManager].nearPDImage;
        [imageView setImage:snapImage];
    }
    
    [self setPositions];
    [self placeMarkersOnTheirPositions];
    
    [self setControllerOrientation];
}

#pragma mark - Dealloc

- (void)dealloc
{
    [self removePanGestures];
    
    [self setSnapImage:nil];
    [self setImageView:nil];
    [self setMarker1:nil];
    [self setMarker2:nil];
    [self setMarker3:nil];
    [self setMarker4:nil];
    [self setMarker5:nil];
    [self setScrollView:nil];
    [self setScrollContentView:nil];
}

- (void)removePanGestures
{
    [marker1 removeGestureRecognizer:marker1PanRecognizer];
    [marker2 removeGestureRecognizer:marker2PanRecognizer];
    [marker3 removeGestureRecognizer:marker3PanRecognizer];
    [marker4 removeGestureRecognizer:marker4PanRecognizer];
    [marker5 removeGestureRecognizer:marker5PanRecognizer];
}

#pragma mark -

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (int) getDetectionInitialized
{
    int iPhoto = (farPdMode.boolValue)? (vdMode.boolValue ? 1 : 0) : 2;
    return rPic[iPhoto].detectionInitialized;
}

- (void) setDetectionInitialized:(int)data
{
    int iPhoto = (farPdMode.boolValue)? (vdMode.boolValue ? 1 : 0) : 2;
    rPic[iPhoto].detectionInitialized = data;
}

- (void)setControllerOrientation
{
    [[UIDevice currentDevice] setValue:@([[UIApplication sharedApplication] statusBarOrientation])
                                forKey:@"orientation"];
    
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
}

- (void)processImage:(UIImage *)testImage
{
    Detection* detect_cv = new Detection;
    
    cv::Mat testImageIpl = (cv::Mat)[Utils UIImageToMat:testImage];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"haarcascade_mcs_eyepair_big" ofType:@"xml"];
    
    if (!(path==(id) [NSNull null] || [path length]==0 || [path isEqual:@""])) {
        
        NSString *directoryPath = [path stringByDeletingLastPathComponent];
        
        std::string haarCascadesDir = std::string([directoryPath UTF8String]);
        
        haarCascadesDir.append("/");
        
        bool ok = false;
        
        ok = detect_cv->loadHaarClasifiers(haarCascadesDir);
        
        if (ok==true) {
            NSLog(@"haar cascades loaded");
        }
        
    } else {
        NSLog(@"could not find haarcascades");
    }
    
    NSDate *start = [NSDate date];
    
    try {
        detect_cv->loadImage(testImageIpl);
        
        if (!detect_cv->isImageLoaded()) {
            return;
        }
        
        // FRED type
        switch((PDMSupportType)[PDM_DATA getCurrentSessionSupportType]) {
            case nAcepMetalSupport: // Metal FRED
                detect_cv->detectSupport();
                break;
            case nAcepPlasticSupport: // new Plastic FRED
            default:
                detect_cv->detectSupport(SUPPORT_ACEP_PATTERN);
                break;
        }
        
        detect_cv->detectEyes(EYE_DETECTION_FAST, true);
        NSLog(@"detected");
        
    } catch (...) {
        NSLog(@"DETECTION  - Exception");
    }
    
	NSDate *finish = [NSDate date];
    NSTimeInterval executionTime = [finish timeIntervalSinceDate:start];
    NSLog(@"Execution time: %f", executionTime);
	
    try {
        if (detect_cv->hasSupportPoint(SUPPORT_POINT_A)) {
            
            support_point_a.x = detect_cv->getSupportPoint(SUPPORT_POINT_A).x;
            support_point_a.y = detect_cv->getSupportPoint(SUPPORT_POINT_A).y;
            xRightSupport = support_point_a.x / widthScale;
            yRightSupport = support_point_a.y / heightScale;
            xRightSupport = [self checkPositionX:xRightSupport];
            yRightSupport = [self checkPositionY:yRightSupport];
            
        } else {
            
            support_point_a = cv::Point(-1, -1);
            xRightSupport = xDefRightSupport;
            yRightSupport = yDefRightSupport;
        }
        
    } catch (...) {
        NSLog(@"SUPPORT_POINT_A DETECTION - Exception");
    }
    
    [marker1 setPoint:CGPointMake(xRightSupport - markerOffset + imageView.frame.origin.x, yRightSupport - markerOffset + imageView.frame.origin.y)];
    [self saveRightSupportDataX:xRightSupport];
    [self saveRightSupportDataY:yRightSupport];
	
    try {
        if (detect_cv->hasSupportPoint(SUPPORT_POINT_B)) {
            
            support_point_b.x = detect_cv->getSupportPoint(SUPPORT_POINT_B).x;
            support_point_b.y = detect_cv->getSupportPoint(SUPPORT_POINT_B).y;
            xLeftSupport = support_point_b.x / widthScale;
            yLeftSupport = support_point_b.y / heightScale;
            xLeftSupport = [self checkPositionX:xLeftSupport];
            yLeftSupport = [self checkPositionY:yLeftSupport];
            
        } else {
            
            support_point_b = cv::Point(-1, -1);
            xLeftSupport = xDefLeftSupport;
            yLeftSupport = yDefLeftSupport;
        }
        
    } catch (...) {
        NSLog(@"SUPPORT_POINT_B DETECTION - Exception");
    }
    
    [marker2 setPoint:CGPointMake(xLeftSupport - markerOffset + imageView.frame.origin.x, yLeftSupport - markerOffset + imageView.frame.origin.y)];
    [self saveLeftSupportDataX:xLeftSupport];
    [self saveLeftSupportDataY:yLeftSupport];
    
    try {
        if (detect_cv->hasSupportPoint(SUPPORT_POINT_C1)) {
            
            support_point_c.x = detect_cv->getSupportPoint(SUPPORT_POINT_C1).x;
            support_point_c.y = detect_cv->getSupportPoint(SUPPORT_POINT_C1).y;
            xCenterSupport = support_point_c.x / widthScale;
            yCenterSupport = support_point_c.y / heightScale;
            xCenterSupport = [self checkPositionX:xCenterSupport];
            yCenterSupport = [self checkPositionY:yCenterSupport];
            
        } else {
            
            support_point_c = cv::Point(-1, -1);
            xCenterSupport = xDefCenterSupport;
            yCenterSupport = yDefCenterSupport;
        }
        
    } catch (...) {
        NSLog(@"SUPPORT_POINT_C1 DETECTION - Exception");
    }
    
    [marker3 setPoint:CGPointMake(xCenterSupport - markerOffset + imageView.frame.origin.x, yCenterSupport - markerOffset + imageView.frame.origin.y)];
    [self saveCenterSupportDataX:xCenterSupport];
    [self saveCenterSupportDataY:yCenterSupport];
    
    try {
        if (detect_cv->hasEyePoint(EYE_POINT_LEFT)) {
            
            left_eye.x = detect_cv->getEyePoint(EYE_POINT_LEFT).x;
            left_eye.y = detect_cv->getEyePoint(EYE_POINT_LEFT).y;
            xLeftEye = left_eye.x / widthScale;
            yLeftEye = left_eye.y / heightScale;
            xLeftEye = [self checkPositionX:xLeftEye];
            yLeftEye = [self checkPositionY:yLeftEye];
            
        } else {
            
            left_eye = cv::Point(-1, -1);
            xLeftEye = xDefLeftEye;
            yLeftEye = yDefLeftEye;
        }
        
    } catch (...) {
        NSLog(@"EYE_POINT_LEFT DETECTION - Exception");
    }
    
    [marker5 setPoint:CGPointMake(xLeftEye - markerOffset + imageView.frame.origin.x, yLeftEye - markerOffset + imageView.frame.origin.y)];
    [self saveLeftEyeDataX:xLeftEye];
    [self saveLeftEyeDataY:yLeftEye];
	
    try {
        if(detect_cv->hasEyePoint(EYE_POINT_RIGHT)){
            
            right_eye.x = detect_cv->getEyePoint(EYE_POINT_RIGHT).x;
            right_eye.y = detect_cv->getEyePoint(EYE_POINT_RIGHT).y;
            xRightEye = right_eye.x / widthScale;
            yRightEye = right_eye.y / heightScale;
            xRightEye = [self checkPositionX:xRightEye];
            yRightEye = [self checkPositionY:yRightEye];
            
        } else {
            
            right_eye = cv::Point(-1, -1);
            xRightEye = xDefRightEye;
            yRightEye = yDefRightEye;
        }
        
    } catch (...) {
        NSLog(@"EYE_POINT_RIGHT DETECTION - Exception");
    }
    
    [marker4 setPoint:CGPointMake(xRightEye - markerOffset + imageView.frame.origin.x, yRightEye - markerOffset + imageView.frame.origin.y)];
    [self saveRightEyeDataX:xRightEye];
    [self saveRightEyeDataY:yRightEye];
    
    delete detect_cv;
    
    // Measurements analytics
    [self saveDetectedDataForAnalytics];
    // user positions
    support_point_a_user.x = xRightSupport * widthScale;
    support_point_a_user.y = yRightSupport * heightScale;
    support_point_b_user.x = xLeftSupport * widthScale;
    support_point_b_user.y = yLeftSupport * heightScale;
    support_point_c_user.x = xCenterSupport * widthScale;
    support_point_c_user.y = yCenterSupport * heightScale;
    left_eye_user.x = xLeftEye * widthScale;
    left_eye_user.y = yLeftEye * heightScale;
    right_eye_user.x = xRightEye * widthScale;
    right_eye_user.y = yRightEye * heightScale;
}

- (void)detectScale
{
    heightScale = snapImage.size.height / imageView.frame.size.height;
    widthScale = snapImage.size.width / imageView.frame.size.width;
}

- (void)configureDidLoad
{
    [self detectScale];
    
    [imageView setImage:snapImage];
    
    if (![self getDetectionInitialized]) {
        [self savePositions];
    }
}

- (void)placeMarkersOnTheirPositions
{
    if ([measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_SUPPORT_X : FAR_RIGHT_SUPPORT_X) : NEAR_RIGHT_SUPPORT_X] != nil) {
        xRightSupport = [self loadRightSupportDataX];
        yRightSupport = [self loadRightSupportDataY];
        [marker1 setPoint:CGPointMake(xRightSupport + imageView.frame.origin.x - markerOffset, yRightSupport + imageView.frame.origin.y - markerOffset)];
    }
    
    if ([measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_SUPPORT_X : FAR_LEFT_SUPPORT_X) : NEAR_LEFT_SUPPORT_X] != nil) {
        xLeftSupport = [self loadLeftSupportDataX];
        yLeftSupport = [self loadLeftSupportDataY];
        [marker2 setPoint:CGPointMake(xLeftSupport + imageView.frame.origin.x - markerOffset, yLeftSupport + imageView.frame.origin.y - markerOffset)];
    }
    
    if ([measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_CENTER_SUPPORT_X : FAR_CENTER_SUPPORT_X) : NEAR_CENTER_SUPPORT_X] != nil) {
        xCenterSupport = [self loadCenterSupportDataX];
        yCenterSupport = [self loadCenterSupportDataY];
        [marker3 setPoint:CGPointMake(xCenterSupport + imageView.frame.origin.x - markerOffset, yCenterSupport + imageView.frame.origin.y - markerOffset)];
    }
    
    if ([measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_EYE_X : FAR_RIGHT_EYE_X) : NEAR_RIGHT_EYE_X] != nil) {
        xRightEye = [self loadRightEyeDataX];
        yRightEye = [self loadRightEyeDataY];
        [marker4 setPoint:CGPointMake(xRightEye + imageView.frame.origin.x - markerOffset, yRightEye + imageView.frame.origin.y - markerOffset)];
    }
    
    if ([measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_EYE_X : FAR_LEFT_EYE_X) : NEAR_LEFT_EYE_X] != nil) {
        xLeftEye = [self loadLeftEyeDataX];
        yLeftEye = [self loadLeftEyeDataY];
        [marker5 setPoint:CGPointMake(xLeftEye + imageView.frame.origin.x - markerOffset, yLeftEye + imageView.frame.origin.y - markerOffset)];
    }
}

- (void)getRotatedCoordinates
{
    xRotRightSupport = [self loadRightSupportDataX];
    yRotRightSupport = [self loadRightSupportDataY];
    
    xRotLeftSupport = [self loadLeftSupportDataX];
    yRotLeftSupport = [self loadLeftSupportDataY];
    
    xRotCenterSupport = [self loadCenterSupportDataX];
    yRotCenterSupport = [self loadCenterSupportDataY];
    
    xRotRightEye = [self loadRightEyeDataX];
    yRotRightEye = [self loadRightEyeDataY];
    
    xRotLeftEye = [self loadLeftEyeDataX];
    yRotLeftEye = [self loadLeftEyeDataY];
}

- (void)setPositions
{    
    if (![self getDetectionInitialized]) {
        
        [self setDetectionInitialized:2];
        [self processImage:self.snapImage];
        
    } else {
        
        int iPhoto = farPdMode.boolValue ? (vdMode.boolValue ? 1 : 0) : 2;
        
        [self getRotatedCoordinates];
        
        if ([self getDetectionInitialized] == 1) {
            
            rPic[iPhoto].DeRotatePoint(xRotLeftSupport, yRotLeftSupport, xLeftSupport, yLeftSupport);
            rPic[iPhoto].DeRotatePoint(xRotRightSupport, yRotRightSupport, xRightSupport, yRightSupport);
            rPic[iPhoto].DeRotatePoint(xRotCenterSupport, yRotCenterSupport, xCenterSupport, yCenterSupport);
            rPic[iPhoto].DeRotatePoint(xRotLeftEye, yRotLeftEye, xLeftEye, yLeftEye);
            rPic[iPhoto].DeRotatePoint(xRotRightEye, yRotRightEye, xRightEye, yRightEye);
        
        } else {
        
            xLeftSupport = xRotLeftSupport;
            yLeftSupport = yRotLeftSupport;
            xRightSupport = xRotRightSupport;
            yRightSupport = yRotRightSupport;
            xCenterSupport = xRotCenterSupport;
            yCenterSupport = yRotCenterSupport;
            xLeftEye = xRotLeftEye;
            yLeftEye = yRotLeftEye;
            xRightEye = xRotRightEye;
            yRightEye = yRotRightEye;
        }
        
        [self setDetectionInitialized:2];
        [self saveData:NO];
    }
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *destinaton = segue.destinationViewController;
    
    if ([destinaton respondsToSelector:@selector(setSnapImage:)]) {
        
        NSInteger dxPicture = snapImage.size.width;
        NSInteger dyPicture = snapImage.size.height;

        int iPhoto = farPdMode.boolValue ? (vdMode.boolValue ? 1 : 0) : 2;
        
        rPic[iPhoto].SetPhotoData(xLeftSupport, yLeftSupport, xRightSupport, yRightSupport,
                                  xCenterSupport, yCenterSupport, xLeftEye, yLeftEye,
                                  xRightEye, yRightEye, X_CENTER, Y_CENTER, X_FRAME, Y_FRAME);
        
        int bytePerPixel = 4;
		int pic_size = (int)(dxPicture * dyPicture * bytePerPixel);
		unsigned char * pPicture = new unsigned char[pic_size];
        
        if (pPicture) {
            
            memset(pPicture, 0, pic_size);
            // Getting CGImage from UIImage
            CGImageRef imageRef1 = snapImage.CGImage;
            
            CGColorSpaceRef colorSpace1 = CGColorSpaceCreateDeviceRGB();
            CGContextRef contextRef1 = CGBitmapContextCreate(pPicture, dxPicture, dyPicture, 8, dxPicture * bytePerPixel, colorSpace1, kCGImageAlphaPremultipliedLast|kCGBitmapByteOrderDefault);
            
            // Drawing CGImage to CGContext
            CGContextDrawImage(contextRef1, CGRectMake(0, 0, dxPicture, dyPicture), imageRef1);
            
            if (rPic[iPhoto].MakeRotatePicture(pPicture, (int)dxPicture, (int)dyPicture, bytePerPixel)) {
                
                unsigned char * pRotatePicture = NULL;
                unsigned int picLength = 0;
                rPic[iPhoto].GetBitmapData(&pRotatePicture, &picLength);
            
                NSData* data = [NSData dataWithBytes:(const void *)pRotatePicture length:picLength];
                UIImage* rotatedImage2 = [[UIImage alloc] initWithData:data];
                
                NSData *imageData2 = UIImageJPEGRepresentation(rotatedImage2, 1.0);
                UIImage* rotatedImage = [UIImage imageWithData:imageData2];

                rPic[iPhoto].GetRotateData(xRotLeftSupport, yRotLeftSupport, xRotRightSupport, yRotRightSupport,
                                           xRotCenterSupport, yRotCenterSupport, xRotLeftEye, yRotLeftEye,
                                           xRotRightEye, yRotRightEye);
                double rotationAngle = rPic[iPhoto].GetRotationAngle() * 180. / M_PI;
                NSLog(@"RotationAngle = %f", rotationAngle);
                
                [self saveForAnalyticsRotationAngle:rotationAngle];
                
                [self saveData:YES];
                
                [destinaton setValue:rotatedImage forKey:@"snapImage"];
                
                // delete old rotated image
                if ([farPdMode boolValue])
                {
                    [PDMDataManager sharedManager].farPDImageRotated = nil;
                    [PDMDataManager sharedManager].farPDImageRotated = rotatedImage;
                }
                else if (![farPdMode boolValue])
                {
                    [PDMDataManager sharedManager].nearPDImageRotated = nil;
                    [PDMDataManager sharedManager].nearPDImageRotated = rotatedImage;
                }
                
                data = nil;
                rPic[iPhoto].ClearBitmapData();
            }
            
            CGContextRelease(contextRef1);
            CGColorSpaceRelease(colorSpace1);
            delete [] pPicture;
        }
    }
    
    [destinaton setValue:farPdMode forKey:@"farPdMode"];
    [destinaton setValue:secondPhotoMode forKey:@"secondPhotoMode"];
    [destinaton setValue:vdMode forKey:@"vdMode"];
    
    [self processAllDataForAnalytics];
}

#pragma mark - Custom Methods

- (void)moveMarker:(UIView *)marker panRecognizer:(UIPanGestureRecognizer*)panRecognizer andMarkerTag:(NSInteger)markerTag
{
    scrollView.bouncesZoom = YES;
    scrollView.maximumZoomScale = MAX_ZOOM_SCALE;
    scrollView.scrollEnabled = NO;
    
    if ([panRecognizer state] == UIGestureRecognizerStateBegan) {
        
        switch (markerTag) {
            case 1: {
                firstLeftSupportPoint = [panRecognizer locationInView:marker];
                break;
            }
            
            case 2: {
                firstCenterSupportPoint = [panRecognizer locationInView:marker];
                break;
            }
                
            case 3: {
                firstRightSupportPoint = [panRecognizer locationInView:marker];
                break;
            }
                
            case 4: {
                firstLeftEyeMarkerPoint = [panRecognizer locationInView:marker];
                break;
            }
                
            case 5: {
                firstRightEyeMarkerPoint = [panRecognizer locationInView:marker];
                break;
            }
                
            default:
                break;
        }
        
        CGFloat posInViewX = [panRecognizer locationInView:scrollContentView].x;
        CGFloat posInViewY = [panRecognizer locationInView:scrollContentView].y;
        CGRect rectTo = CGRectMake(posInViewX / MAX_ZOOM_SCALE, posInViewY / MAX_ZOOM_SCALE, scrollContentView.frame.size.width / MAX_ZOOM_SCALE, scrollContentView.frame.size.height / MAX_ZOOM_SCALE);
        [scrollView zoomToRect:rectTo animated:YES];
        
    } else if ([panRecognizer state] == UIGestureRecognizerStateChanged) {
        
        CGPoint movePoint;
        
        switch (markerTag) {
            case 1: {
                movePoint = firstLeftSupportPoint;
                break;
            }
                
            case 2: {
                movePoint = firstCenterSupportPoint;
                break;
            }
                
            case 3: {
                movePoint = firstRightSupportPoint;
                break;
            }
                
            case 4: {
                movePoint = firstLeftEyeMarkerPoint;
                break;
            }
                
            case 5: {
                movePoint = firstRightEyeMarkerPoint;
                break;
            }
                
            default:
                break;
        }
        
        CGFloat newX = floor(([panRecognizer locationInView:scrollContentView].x - movePoint.x) * 2. + 0.5) / 2. + markerOffset;
        CGFloat newY = floor(([panRecognizer locationInView:scrollContentView].y - movePoint.y) * 2. + 0.5) / 2. + markerOffset;
        
        [marker setPoint:CGPointMake(newX - markerOffset, newY - markerOffset)];
        
        switch (markerTag) {
            case 1: {
                xRightSupport = newX;
                yRightSupport = newY;
                [self saveRightSupportDataX:newX];
                [self saveRightSupportDataY:newY];
                support_point_a_user.x = newX * widthScale;
                support_point_a_user.y = newY * heightScale;
                break;
            }
                
            case 2: {
                xLeftSupport = newX;
                yLeftSupport = newY;
                [self saveLeftSupportDataX:newX];
                [self saveLeftSupportDataY:newY];
                support_point_b_user.x = newX * widthScale;
                support_point_b_user.y = newY * heightScale;
                break;
            }
                
                
            case 3: {
                xCenterSupport = newX;
                yCenterSupport = newY;
                [self saveCenterSupportDataX:newX];
                [self saveCenterSupportDataY:newY];
                support_point_c_user.x = newX * widthScale;
                support_point_c_user.y = newY * heightScale;
                break;
            }
                
            case 4: {
                xRightEye = newX;
                yRightEye = newY;
                [self saveRightEyeDataX:newX];
                [self saveRightEyeDataY:newY];
                right_eye_user.x = newX * widthScale;
                right_eye_user.y = newY * heightScale;
                break;
            }
                
            case 5: {
                xLeftEye = newX;
                yLeftEye = newY;
                [self saveLeftEyeDataX:newX];
                [self saveLeftEyeDataY:newY];
                left_eye_user.x = newX * widthScale;
                left_eye_user.y = newY * heightScale;
                break;
            }
                
            default:
                break;
        }
        
    } else if ([panRecognizer state] == UIGestureRecognizerStateEnded){
        
        [scrollView setZoomScale:1.0 animated:YES];
        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        scrollView.scrollEnabled = YES;
        scrollView.bouncesZoom = NO;
        scrollView.maximumZoomScale = 1.0;
        
    } else if ([panRecognizer state] == UIGestureRecognizerStateCancelled){
        scrollView.scrollEnabled = YES;
        scrollView.bouncesZoom = NO;
        [scrollView setZoomScale:1.0 animated:YES];
        scrollView.maximumZoomScale = 1.0;
        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}

- (void)saveData:(BOOL)rotated
{
    [self saveLeftSupportDataX:(rotated ? xRotLeftSupport : xLeftSupport)];
    [self saveLeftSupportDataY:(rotated ? yRotLeftSupport : yLeftSupport)];

    [self saveCenterSupportDataX:(rotated ? xRotCenterSupport : xCenterSupport)];
    [self saveCenterSupportDataY:(rotated ? yRotCenterSupport : yCenterSupport)];

    [self saveRightSupportDataX:(rotated ? xRotRightSupport : xRightSupport)];
    [self saveRightSupportDataY:(rotated ? yRotRightSupport : yRightSupport)];

    [self saveLeftEyeDataX:(rotated ? xRotLeftEye : xLeftEye)];
    [self saveLeftEyeDataY:(rotated ? yRotLeftEye : yLeftEye)];

    [self saveRightEyeDataX:(rotated ? xRotRightEye : xRightEye)];
    [self saveRightEyeDataY:(rotated ? yRotRightEye : yRightEye)];
}

// SM Mobile v1 window size
const double width_v1 = 991.;
const double height_v1 = 615.;
// SM Mobile v2 window size
const double width_v2 = 1238.;
const double height_v2 = 768.;

- (double)checkPositionX:(double)x
{
    double xRes = x;
    if (x < markerOffset + (width_v2 - 1024.) / 2.) {
        xRes = markerOffset + (width_v2 - 1024.) / 2.;
    }
    if (x > 1024. - markerOffset + (width_v2 - 1024.) / 2.) {
        xRes = 1024. - markerOffset + (width_v2 - 1024.) / 2.;
    }
    return xRes;
}
- (double)checkPositionY:(double)y
{
    double yRes = y;
    if (y < markerOffset + (height_v2 - 768.) / 2.) {
        yRes = markerOffset + (height_v2 - 768.) / 2.;
    }
    if (y > 768. - markerOffset + (height_v2 - 768.) / 2.) {
        yRes = 768. - markerOffset + (height_v2 - 768.) / 2.;
    }
    return yRes;
}

- (double)windowToDictionaryX:(double)x
{
    double xDic = x * width_v1 / width_v2;
    return xDic;
}
- (double)windowToDictionaryY:(double)y
{
    double xDic = y * height_v1 / height_v2;
    return xDic;
}
- (double)dictionaryToWindowX:(double)x
{
    double xWin = x * width_v2 / width_v1;
    return xWin;
}
- (double)dictionaryToWindowY:(double)y
{
    double xWin = y * height_v2 / height_v1;
    return xWin;
}
// Store data
- (void) saveLeftSupportDataX: (double)xWin
{
    double xData = [self windowToDictionaryX:xWin];
    
    [measurement setValue:[NSNumber numberWithFloat:xData] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_SUPPORT_X : FAR_LEFT_SUPPORT_X) : NEAR_LEFT_SUPPORT_X];
}
- (void) saveLeftSupportDataY: (double)yWin
{
    double yData = [self windowToDictionaryY:yWin];
    
    [measurement setValue:[NSNumber numberWithFloat:yData] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_SUPPORT_Y : FAR_LEFT_SUPPORT_Y) : NEAR_LEFT_SUPPORT_Y];
}
- (void) saveCenterSupportDataX: (double)xWin
{
    double xData = [self windowToDictionaryX:xWin];
    
    [measurement setValue:[NSNumber numberWithFloat:xData] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_CENTER_SUPPORT_X : FAR_CENTER_SUPPORT_X) : NEAR_CENTER_SUPPORT_X];
}
- (void) saveCenterSupportDataY: (double)yWin
{
    double yData = [self windowToDictionaryY:yWin];
    
    [measurement setValue:[NSNumber numberWithFloat:yData] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_CENTER_SUPPORT_Y : FAR_CENTER_SUPPORT_Y) : NEAR_CENTER_SUPPORT_Y];
}
- (void) saveRightSupportDataX: (double)xWin
{
    double xData = [self windowToDictionaryX:xWin];
    
    [measurement setValue:[NSNumber numberWithFloat:xData] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_SUPPORT_X : FAR_RIGHT_SUPPORT_X) : NEAR_RIGHT_SUPPORT_X];
}
- (void) saveRightSupportDataY: (double)yWin
{
    double yData = [self windowToDictionaryY:yWin];
    
    [measurement setValue:[NSNumber numberWithFloat:yData] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_SUPPORT_Y : FAR_RIGHT_SUPPORT_Y) : NEAR_RIGHT_SUPPORT_Y];
}
- (void) saveLeftEyeDataX: (double)xWin
{
    double xData = [self windowToDictionaryX:xWin];
    
    [measurement setValue:[NSNumber numberWithFloat:xData] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_EYE_X : FAR_LEFT_EYE_X) : NEAR_LEFT_EYE_X];
}
- (void) saveLeftEyeDataY: (double)yWin
{
    double yData = [self windowToDictionaryY:yWin];
    
    [measurement setValue:[NSNumber numberWithFloat:yData] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_EYE_Y : FAR_LEFT_EYE_Y) : NEAR_LEFT_EYE_Y];
}
- (void) saveRightEyeDataX: (double)xWin
{
    double xData = [self windowToDictionaryX:xWin];
    
    [measurement setValue:[NSNumber numberWithFloat:xData] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_EYE_X : FAR_RIGHT_EYE_X) : NEAR_RIGHT_EYE_X];
}
- (void) saveRightEyeDataY: (double)yWin
{
    double yData = [self windowToDictionaryY:yWin];
    
    [measurement setValue:[NSNumber numberWithFloat:yData] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_EYE_Y : FAR_RIGHT_EYE_Y) : NEAR_RIGHT_EYE_Y];
}
// Load data
- (double) loadLeftSupportDataX
{
    double xData = [[measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_SUPPORT_X : FAR_LEFT_SUPPORT_X) : NEAR_LEFT_SUPPORT_X] floatValue];
    double xWin = [self dictionaryToWindowX:xData];
    return xWin;
}
- (double) loadLeftSupportDataY
{
    double yData = [[measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_SUPPORT_Y : FAR_LEFT_SUPPORT_Y) : NEAR_LEFT_SUPPORT_Y] floatValue];
    double yWin = [self dictionaryToWindowY:yData];
    return yWin;
}
- (double) loadCenterSupportDataX
{
    double xData = [[measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_CENTER_SUPPORT_X : FAR_CENTER_SUPPORT_X) : NEAR_CENTER_SUPPORT_X] floatValue];
    double xWin = [self dictionaryToWindowX:xData];
    return xWin;
}
- (double) loadCenterSupportDataY
{
    double yData = [[measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_CENTER_SUPPORT_Y : FAR_CENTER_SUPPORT_Y) : NEAR_CENTER_SUPPORT_Y] floatValue];
    double yWin = [self dictionaryToWindowY:yData];
    return yWin;
}
- (double) loadRightSupportDataX
{
    double xData = [[measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_SUPPORT_X : FAR_RIGHT_SUPPORT_X) : NEAR_RIGHT_SUPPORT_X] floatValue];
    double xWin = [self dictionaryToWindowX:xData];
    return xWin;
}
- (double) loadRightSupportDataY
{
    double yData = [[measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_SUPPORT_Y : FAR_RIGHT_SUPPORT_Y) : NEAR_RIGHT_SUPPORT_Y] floatValue];
    double yWin = [self dictionaryToWindowY:yData];
    return yWin;
}
- (double) loadLeftEyeDataX
{
    double xData = [[measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_EYE_X : FAR_LEFT_EYE_X) : NEAR_LEFT_EYE_X] floatValue];
    double xWin = [self dictionaryToWindowX:xData];
    return xWin;
}
- (double) loadLeftEyeDataY
{
    double yData = [[measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_EYE_Y : FAR_LEFT_EYE_Y) : NEAR_LEFT_EYE_Y] floatValue];
    double yWin = [self dictionaryToWindowY:yData];
    return yWin;
}
- (double) loadRightEyeDataX
{
    double xData = [[measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_EYE_X : FAR_RIGHT_EYE_X) : NEAR_RIGHT_EYE_X] floatValue];
    double xWin = [self dictionaryToWindowX:xData];
    return xWin;
}
- (double) loadRightEyeDataY
{
    double yData = [[measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_EYE_Y : FAR_RIGHT_EYE_Y) : NEAR_RIGHT_EYE_Y] floatValue];
    double yWin = [self dictionaryToWindowY:yData];
    return yWin;
}

- (void)savePositions
{
    xLeftSupport = marker2.frame.origin.x + markerOffset - imageView.frame.origin.x;
    yLeftSupport = marker2.frame.origin.y + markerOffset - imageView.frame.origin.y;
    xRightSupport = marker1.frame.origin.x + markerOffset - imageView.frame.origin.x;
    yRightSupport = marker1.frame.origin.y + markerOffset - imageView.frame.origin.y;
    xCenterSupport = marker3.frame.origin.x + markerOffset - imageView.frame.origin.x;
    yCenterSupport = marker3.frame.origin.y + markerOffset - imageView.frame.origin.y;
    xLeftEye = marker5.frame.origin.x + markerOffset - imageView.frame.origin.x;
    yLeftEye = marker5.frame.origin.y + markerOffset - imageView.frame.origin.y;
    xRightEye = marker4.frame.origin.x + markerOffset - imageView.frame.origin.x;
    yRightEye = marker4.frame.origin.y + markerOffset - imageView.frame.origin.y;
    
    [self saveData:NO];
}

- (void)saveDefaultPositions
{
    xDefLeftSupport = marker2.frame.origin.x + markerOffset - imageView.frame.origin.x;
    yDefLeftSupport = marker2.frame.origin.y + markerOffset - imageView.frame.origin.y;
    xDefRightSupport = marker1.frame.origin.x + markerOffset - imageView.frame.origin.x;
    yDefRightSupport = marker1.frame.origin.y + markerOffset - imageView.frame.origin.y;
    xDefCenterSupport = marker3.frame.origin.x + markerOffset - imageView.frame.origin.x;
    yDefCenterSupport = marker3.frame.origin.y + markerOffset - imageView.frame.origin.y;
    xDefLeftEye = marker5.frame.origin.x + markerOffset - imageView.frame.origin.x;
    yDefLeftEye = marker5.frame.origin.y + markerOffset - imageView.frame.origin.y;
    xDefRightEye = marker4.frame.origin.x + markerOffset - imageView.frame.origin.x;
    yDefRightEye = marker4.frame.origin.y + markerOffset - imageView.frame.origin.y;
}

#pragma mark - Actions

- (void)marker1PanGesture:(UIPanGestureRecognizer *)panRecognizer
{
    supportIsMoved = YES;
    
    [self moveMarker:marker1 panRecognizer:panRecognizer andMarkerTag:1];
}

- (void)marker2PanGesture:(UIPanGestureRecognizer *)panRecognizer
{
    supportIsMoved = YES;
    
    [self moveMarker:marker2 panRecognizer:panRecognizer andMarkerTag:2];   
}

- (void)marker3PanGesture:(UIPanGestureRecognizer *)panRecognizer
{
    supportIsMoved = YES;
    
    [self moveMarker:marker3 panRecognizer:panRecognizer andMarkerTag:3];
}

- (void)marker4PanGesture:(UIPanGestureRecognizer *)panRecognizer
{
    eyePointsIsMoved = YES;
    
    [self moveMarker:marker4 panRecognizer:panRecognizer andMarkerTag:4];
}

- (void)marker5PanGesture:(UIPanGestureRecognizer *)panRecognizer
{
    eyePointsIsMoved = YES;
    
    [self moveMarker:marker5 panRecognizer:panRecognizer andMarkerTag:5];
}

- (IBAction)donePressed:(id)sender
{
    bool bNextScreen = true;
    if (backButton.enabled && farPdMode.boolValue) {
        BOOL newGenDevice = YES;
        PDDeviceDetails details = [[PDDeviceInfo alloc] deviceDetails];
        if (details.family == PDDeviceFamilyiPad) {
            switch (details.model) {
                case PDDeviceModeliPad:
                case PDDeviceModeliPad2:
                    newGenDevice = NO;
                    break;
                default:
                    newGenDevice = YES;
                    break;
            }
        }
        
        // check head pan rotation
        CPDMLineAngle panAngle;
        PDMSupportType typeUsedSupport = (PDMSupportType)[PDM_DATA getCurrentSessionSupportType];
        double DeviceAngle = [[measurement objectForKey:farPdMode.boolValue ? FAR_SNAPPED_ANGLE : NEAR_SNAPPED_ANGLE] floatValue];
        
        double xLeftSupportBase = [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_SUPPORT_X : NEAR_LEFT_SUPPORT_X] floatValue];
        double yLeftSupportBase = [[measurement objectForKey:farPdMode.boolValue ? FAR_LEFT_SUPPORT_Y : NEAR_LEFT_SUPPORT_Y] floatValue];
        double xRightSupportBase = [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_SUPPORT_X : NEAR_RIGHT_SUPPORT_X] floatValue];
        double yRightSupportBase = [[measurement objectForKey:farPdMode.boolValue ? FAR_RIGHT_SUPPORT_Y : NEAR_RIGHT_SUPPORT_Y] floatValue];
        double xCenterSupportBase = [[measurement objectForKey:farPdMode.boolValue ? FAR_CENTER_SUPPORT_X : NEAR_CENTER_SUPPORT_X] floatValue];
        double yCenterSupportBase = [[measurement objectForKey:farPdMode.boolValue ? FAR_CENTER_SUPPORT_Y : NEAR_CENTER_SUPPORT_Y] floatValue];
        
        panAngle.SetPhotoData(newGenDevice ? niPadNew : niPad, farPdMode.boolValue ? nFarPD : nNearPD, xLeftSupportBase, yLeftSupportBase, xRightSupportBase, yRightSupportBase, xCenterSupportBase, yCenterSupportBase, typeUsedSupport, DeviceAngle);
        
        double anglePan = panAngle.GetPanAngle();
        NSLog(@"Markers: anglePan = %f\n", anglePan * 180. / M_PI);
        
        if (fabs(anglePan) > 4.9 * M_PI / 180.) {
            bNextScreen = false;
            UIAlertView * panAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Attention", nil)
                                                                  message:NSLocalizedString(@"Head rotation exceeded the limit! Please consider taking new measurement photo.", nil)
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                        otherButtonTitles:NSLocalizedString(@"Сontinue with the current photo", @""), nil];
            [panAlert setTag:3];
            [panAlert show];
            
            return;
        }
    }
    if (bNextScreen) {
        [self nextScreen];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 3) {
        switch (buttonIndex) {
            case 0: // OK
                break;
            case 1: // Сontinue with the current photo
                [self nextScreen];
                break;
        }
    }
}

- (void)nextScreen
{
    [self setDetectionInitialized:1];
    [self performSegueWithIdentifier:@"lines" sender:nil];
}

- (IBAction)btnHelp_Click:(id)sender
{
    PDHelpType page;
    
    CPDMDataResults resData = Results.GetPhotoResults(0);
    
    if (farPdMode.boolValue) {
        page = PDHelpTypeFarMarkers;
    
    } else {
        
        if (resData.bDataResults) {
            page = PDHelpTypeNearMarkers;
        } else {
            page = PDHelpTypeNearMarkersNew;
        }
    }
    
    UIViewController *controller = [PDHelpViewController initWithPage:page];
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (IBAction)goBackPressed:(id)sender
{
    if (PDM_DATA.currentSessionType == PDMSessionProgressive && PDM_DATA.currentPhotoStep == PDMPhotoNear)
        PDM_DATA.currentPhotoStep = PDMPhotoFar;
    else if (PDM_DATA.currentSessionType == PDMSessionProgressive && PDM_DATA.currentPhotoStep == PDMPhotoFar)
        PDM_DATA.currentPhotoStep = PDMPhotoNear;
    
    [self setDetectionInitialized:2];
    
    int iPhoto = farPdMode.boolValue ? (vdMode.boolValue ? 1 : 0) : 2;
    
    rPic[iPhoto].SetPhotoData(xLeftSupport, yLeftSupport, xRightSupport, yRightSupport,
                              xCenterSupport, yCenterSupport, xLeftEye, yLeftEye,
                              xRightEye, yRightEye, X_CENTER, Y_CENTER, X_FRAME, Y_FRAME);
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeButtonPressed:(id)sender
{    
    [self setDetectionInitialized:2];
    
    int iPhoto = farPdMode.boolValue ? (vdMode.boolValue ? 1 : 0) : 2;
    
    rPic[iPhoto].SetPhotoData(xLeftSupport, yLeftSupport, xRightSupport, yRightSupport,
                              xCenterSupport, yCenterSupport, xLeftEye, yLeftEye,
                              xRightEye, yRightEye, X_CENTER, Y_CENTER, X_FRAME, Y_FRAME);
    
    needDetectFlashlightType = YES;
    
    [PD_MANAGER setTargetScreen:PDTargetScreenMarkers];
    
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return scrollContentView;
}

#pragma mark - PD Analytics Data Proccess

- (void)saveDetectedDataForAnalytics
{
    [PDM_DATA.analyticsHelper initUnitedDetectSession];
    
    [PDM_DATA.analyticsHelper setDetectionPhoto:snapImage
                                      frameSize:CGSizeMake(snapImage.size.width, snapImage.size.height)
                                     forFarMode:[farPdMode boolValue]];
    
    [PDM_DATA.analyticsHelper setOriginPhoto:[PDM_DATA getOriginPhoto]
                                   frameSize:CGSizeMake([PDM_DATA getOriginPhoto].size.width, [PDM_DATA getOriginPhoto].size.height)
                                  forFarMode:[farPdMode boolValue]];
    
    [PDM_DATA.analyticsHelper setBoxingPhotoRotateAngle:0];
    
    [PDM_DATA.analyticsHelper setSupportPointA:CGPointMake(support_point_a.x, support_point_a.y)
                                        pointB:CGPointMake(support_point_b.x, support_point_b.y)
                                       pointC1:CGPointMake(support_point_c.x, support_point_c.y)];
    
    [PDM_DATA.analyticsHelper setEyePointLeft:CGPointMake(left_eye.x, left_eye.y)
                                        right:CGPointMake(right_eye.x, right_eye.y)];
}

- (void)saveUserDataForAnalytics
{
    [PDM_DATA.analyticsHelper setUserSupportPointA_XValue:support_point_a_user.x];
    [PDM_DATA.analyticsHelper setUserSupportPointA_YValue:support_point_a_user.y];
    [PDM_DATA.analyticsHelper setUserSupportPointB_XValue:support_point_b_user.x];
    [PDM_DATA.analyticsHelper setUserSupportPointB_YValue:support_point_b_user.y];
    [PDM_DATA.analyticsHelper setUserSupportPointC1_XValue:support_point_c_user.x];
    [PDM_DATA.analyticsHelper setUserSupportPointC1_YValue:support_point_c_user.y];
    [PDM_DATA.analyticsHelper setUserLeftEye_XValue:left_eye_user.x];
    [PDM_DATA.analyticsHelper setUserLeftEye_YValue:left_eye_user.y];
    [PDM_DATA.analyticsHelper setUserRightEye_XValue:right_eye_user.x];
    [PDM_DATA.analyticsHelper setUserRightEye_YValue:right_eye_user.y];
}

- (void)saveForAnalyticsRotationAngle:(double)angle
{
    [PDM_DATA.analyticsHelper setBoxingPhotoRotateAngle:angle];
}

- (void)processAllDataForAnalytics
{
    // user positions
    [self saveUserDataForAnalytics];

    [PDM_DATA.analyticsHelper setSupportsWasMoved:supportIsMoved];
    [PDM_DATA.analyticsHelper setEyePointsWasMoved:eyePointsIsMoved];
}

#pragma mark - Rotate Interface Orientation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

@end
