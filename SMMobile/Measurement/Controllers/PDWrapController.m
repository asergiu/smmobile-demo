//
//  PDWrapController.m
//  Visioner_4.0
//
//  Created by Dgut on 12.12.13.
//
//

#import "PDWrapController.h"
#import "PDMDataManager.h"
#import "PDHelpViewController.h"

@interface PDWrapController()
{
    int shotCounter;
    
    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *newPhotoButton;
    IBOutlet UIButton *doneButton;
    IBOutlet UILabel *titlelabel;
}

- (IBAction)showHelp:(id)sender;

@end

@implementation PDWrapController

-( id )initWithNibName : ( NSString * )nibNameOrNil bundle : ( NSBundle * )nibBundleOrNil
{
    self = [ super initWithNibName : nibNameOrNil bundle : nibBundleOrNil ];
    if( self )
    {
    }
    return self;
}

-( void )viewDidLoad
{
    [super viewDidLoad];
    
    [titlelabel setText:NSLocalizedString( @"Frame Wrap Angle", nil )];
    [cancelButton setTitle:NSLocalizedString( @"Cancel", nil ) forState:UIControlStateNormal];
    [newPhotoButton setTitle:NSLocalizedString( @"New photo", nil ) forState:UIControlStateNormal];
    [doneButton setTitle:NSLocalizedString( @"Done", nil ) forState:UIControlStateNormal];
    
    doneButton.enabled = NO;
    
    [self.infoLabel setText:NSLocalizedString( @"Put frame on a flat surface. Hold iPad horizontally over the frame.", nil )];
    
    [ self.wrapView play ];
}

-( IBAction )dismissAnimated : ( id )sender
{
    [ self dismissViewControllerAnimated:YES completion:nil ];
}

-( IBAction )makeNewPhoto : ( id )sender
{
    [ self.wrapView reset ];
    
    self.infoLabel.hidden = NO;
    
    doneButton.enabled = NO;
    
    for( UIButton * button in self.shotButtons )
        button.hidden = NO;
}

-( IBAction )makeShot : ( id )sender
{
    for( UIButton * button in self.shotButtons )
        button.hidden = YES;
    
    shotCounter = 4;
    [ self decreaseCounter ];
}

-( void )decreaseCounter
{
    if( --shotCounter )
    {
        self.counterLabel.hidden = NO;
        self.counterLabel.text = [ NSString stringWithFormat : @"%i", shotCounter ];
        
        [ self performSelector : @selector( decreaseCounter ) withObject : self afterDelay : 1 ];
    }
    else
    {
        self.counterLabel.hidden = YES;
        self.infoLabel.hidden    = YES;
        
        doneButton.enabled = YES;
        
        [ self.wrapView shot ];
    }
}

- (IBAction)done : ( id )sender
{
    PDM_DATA.topWrapValue = @(self.wrapView.resultAngle);
    
    [self dismissAnimated:nil];
}

-( void )dealloc
{
    [ self.wrapView stop ];
}

- (void)showHelp:(id)sender
{
    PDHelpType page = (doneButton.isEnabled) ? PDHelpTypeWrapAngleAfter : PDHelpTypeWrapAngleBefore;
    
    UIViewController *controller = [PDHelpViewController initWithPage:page];
    
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark Autorotate

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
