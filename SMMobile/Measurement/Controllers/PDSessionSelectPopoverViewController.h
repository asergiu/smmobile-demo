//
//  PDSessionSelectPopoverViewController.h
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 24.03.13.
//
//

#import <UIKit/UIKit.h>

@protocol PDSessionSelectPopoverViewControllerDelegate <NSObject>

@required
- (void)didSelectedLoadButton;
- (void)didSelectedEditButton;
- (void)didSelectedDeleteButton;
- (void)didSelectedDeleteALLButton;
- (void)didSelectedFullscreenButton;

@end

@interface PDSessionSelectPopoverViewController : UIViewController
{
    IBOutlet UIButton *loadButton;
    IBOutlet UIButton *fullButton;
    IBOutlet UIButton *editButton;
    IBOutlet UIButton *deleteButton;
}

@property (nonatomic, assign) id <PDSessionSelectPopoverViewControllerDelegate> delegate;

- (IBAction)buttonDidSelected:(UIButton *)sender;

@end
