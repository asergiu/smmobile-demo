//
//  PDSessionEditViewController.m
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 07.04.13.
//
//

#import "PDSessionEditViewController.h"

#define kEditShift 320.0f

@interface PDSessionEditViewController ()

@end

@implementation PDSessionEditViewController

@synthesize session;
@synthesize doneButton;
@synthesize firstnameLabel;
@synthesize lastnameLabel;

- (id)initWithSession:(Session *)_session
{
    self = [super initWithNibName:@"PDSessionEditViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        session = _session;
    }
    return self;
}

- (void)loadDataFromCurrentSession
{
    if (!session)
        return;
    
    NSString *photoKey = session.screenShot.photoID;
    UIImage *sessionPhoto = [SESSION_MANAGER photoForPhotoId:photoKey source:SourceTypeLocal];
    image.image = sessionPhoto;
    
    firstName.text = session.clientFirstName;
    lastName.text = session.clientLastName;
}

- (IBAction)doneButtonDidPressed:(id)sender
{
    [self saveCurrentSession];
    
    if (self.navigationController)
        [self.navigationController popViewControllerAnimated:YES];
    else
    {
        NSNotification *notification = [NSNotification notificationWithName:@"reloadAllData" object:self];
        [[NSNotificationCenter defaultCenter] postNotification:notification];
        
        [UIView transitionWithView:self.view.superview duration:0.5f
                           options:UIViewAnimationOptionTransitionFlipFromRight
                        animations:^ { [self.view removeFromSuperview];}
                        completion:nil];
    }
}

- (void)saveCurrentSession
{
    session.clientFirstName = firstName.text;
    session.clientLastName = lastName.text;
    
    [session updateLastUpdateDate];
    
    [SESSION_MANAGER storeSessionToSQLite:session];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect frame = self.view.frame;
    frame.size.height += kEditShift;
    
    self.view.frame = frame;
    
    initialOrign = self.view.frame.origin;
    
    firstName.delegate = self;
    lastName.delegate = self;
    
    [doneButton setTitle:NSLocalizedString(@"Done", @"") forState:UIControlStateNormal];
    [doneButton setTitle:NSLocalizedString(@"Done", @"") forState:UIControlStateHighlighted];
    [doneButton setTitle:NSLocalizedString(@"Done", @"") forState:UIControlStateSelected];
    
    firstnameLabel.text = NSLocalizedString(@"First Name", @"");
    lastnameLabel.text = NSLocalizedString(@"Last Name", @"");
    firstName.placeholder = NSLocalizedString(@"First Name", @"");
    lastName.placeholder = NSLocalizedString(@"Last Name", @"");
    
    [self loadDataFromCurrentSession];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideKeyboard)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self slideToPoint:CGPointMake(initialOrign.x, initialOrign.y - kEditShift)];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self hideKeyboard];
    
    [super touchesBegan:touches withEvent:event];
}

- (void)slideToPoint:(CGPoint)point
{
    [UIView animateWithDuration:0.2 animations:^{
        
        CGRect frame = self.view.frame;
        frame.origin.x = point.x;
        frame.origin.y = point.y;
        
        self.view.frame = frame;
    }];
}

- (void)hideKeyboard
{
    [self slideToPoint:initialOrign];
    
    [self.view endEditing:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    image = nil;
    firstName = nil;
    lastName = nil;
}

@end
