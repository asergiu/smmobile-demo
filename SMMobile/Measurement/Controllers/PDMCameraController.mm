#import <ImageIO/ImageIO.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <QuartzCore/QuartzCore.h>
#import <AudioToolbox/AudioToolbox.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMotion/CoreMotion.h>
#import <ImageIO/ImageIO.h>
#import "AVCamCaptureManager.h"
#import "AVCamRecorder.h"
#import "SMAppManager.h"

#import "PDMCameraController.h"
#import "PDMMarkersController.h"
#import "PDMLinesController.h"
#import "PDMResultsController.h"
#import "PDSessionManagerViewController.h"

#import "PDMTouchView.h"
#import "PDCameraButton.h"
#import "PDHelpViewController.h"

#import "PDMDataManager.h"
#import "PDMResults.h"
#import "PDDeviceInfo.h"
#import "PDConfigManager.h"

#import "CAShapeLayer+Effects.h"
#import "UIView+Additions.h"
#import "UIImage+Crop.h"
#import "UIImage+Brightness.h"
#import "UIView+Effects.h"
#import "RotatePicture.h"

#import "PDMCameraView.h"
#import "SMColorTrackingCamera.h"

#import "SMNetworkManager.h"
#import "SMSupportChoiseController.h"

#import "SMStoreAndTurnController.h"
#import "SVProgressHUD.h"

#define degrees(x) (180.0 * x / M_PI)
#define noUSE_DEVICEMOTION

#define TWICE_ALERT 777

const int n_deque_size = 7;
int i_angle_deque = 0;
double angle_deque[7] = {0., 0., 0., 0., 0., 0., 0.};
int angle_deque_size = 0;

extern CPDMResults Results;
bool bPDStandard = true;
BOOL needDetectFlashlightType = YES;
BOOL needPortreitDetectFlashlightType = NO;

double zoomFactor = 1.278;
extern CRotatePicture rPic[3];

static CGPoint pointOfInterest = CGPointMake( FLT_MAX, FLT_MAX );

@interface PDMCameraController () <PDSessionManagerViewControllerDelegate, AVCamCaptureManagerDelegate, PDMTouchViewDelegate, SMSupportChoiseControllerDelegate, SMStoreAndTurnControllerDelegate>
{
    SMStoreAndTurnController *popoverStoreAndTurn;
    
    PDDeviceModel currentDevice;
    
    UIAlertView *twice_alert;
    UILabel *okLabel;
    
    NSNumber *farPdMode;
    
    IBOutlet UIImageView *sceenVisor;
    IBOutlet UILabel *brightnessLabel;
    IBOutlet UISlider *brightnessSlider;
    IBOutlet UILabel *DALabel;
    IBOutlet UITextField *DATextField;
    IBOutlet UITextField *DAzTextField;
    
    BOOL isSnapProcess;
    BOOL imageIsSnapped;
    BOOL stopAccelerometer;
    BOOL bNearDeviceAnglePresent;
    BOOL firstStart;
    BOOL faceModeCamera;
    BOOL vdMode;
    BOOL farModeIsDetermined;
    BOOL fromResults;
    bool bFarDeviceAnglePresent;
    
    // Volume control
    
    MPVolumeView *volumeView;
    
    // FarPD resources
    
    IBOutlet PDCameraButton *leftOKButton;
    IBOutlet PDCameraButton *rightOKButton;
    IBOutlet UILabel *delayLabel;
    IBOutlet UIView *delayView;
    IBOutlet UILabel *warningLabel;
    IBOutlet UILabel *secondPhotoHelperLabel;
    
    // NearPD resources
    
    double trackingAngle;
    IBOutlet UILabel *nearWarningLabel;
    IBOutlet UILabel *nearHelperLabel;
    IBOutlet UIView *roundView;
    IBOutlet UIView *nearDelayView;
    IBOutlet UILabel *nearDelayLabel;
    IBOutlet UILabel *nearTouchLabel;
    IBOutlet PDMTouchView *touchView;
    
    NSTimer * timerHideExposure;
    UITapGestureRecognizer *cameraTap;
    
    float yyyyy;
    // Front zoom factor
    float scaleFront;
    // Back zoom factor
    float scaleBack;
    
    SMSupportChoiseController *supportController;
    UIView *backgroundView;
    
    IBOutlet UIButton *turnBtn;
    IBOutlet UILabel *customerLabel;
    
    // Accelerometer
    bool bAccelerometerStarted;
    
    // iPad Flash
    IBOutlet UIImageView *retinaFlashView;
    IBOutlet UIView *retinaFlashSubView;
    float userBrightness;
}

@property (nonatomic, strong) UISlider *volumeSlider;

@property (nonatomic, strong) AVAudioPlayer *player;
@property (nonatomic, strong) IBOutlet UIImageView *staticImageView;

@property (nonatomic, assign) NSInteger delayCount;
@property (nonatomic, assign) CGRect cropArea;
@property (nonatomic, strong) NSTimer *delayTimer;
@property (nonatomic, assign) CGFloat snappedAngle;
@property (nonatomic, assign) CGFloat snappedAngleZ;

@property (retain, nonatomic) IBOutlet UIButton *photoButton;
@property (retain, nonatomic) IBOutlet UIButton *markersButton;
@property (retain, nonatomic) IBOutlet UIButton *homeButton;
@property (retain, nonatomic) IBOutlet UIButton *sessionButton;

@property (nonatomic, strong) UIImage *portrainImage;
@property (nonatomic, strong) UIImage *imageWithBrightnessPortrait;
@property (nonatomic, strong) UIImage *imageWithBrightness;
@property (nonatomic, strong) UIImage *snapImage;
@property (nonatomic, strong) UIImage *vdImage;
@property (nonatomic, strong) UIImage *vdImageWithBrightness;

- (void)lockElements;
- (void)unlockElements;
- (void)showWarning:(BOOL)needToShow andArrowsUp:(BOOL)needToArrowsUp;
- (int)Round:(double)Arg;

@end

@implementation PDMCameraController

const float STATIC_VIEW_WIDTH  = 1238.f;
const float STATIC_VIEW_HEIGHT = 768.f;

@synthesize targetNearPd;
@synthesize switchToFar;
@synthesize returnedFromResults;
@synthesize secondPhotoMode;
@synthesize vdImage;
@synthesize vdImageWithBrightness;
@synthesize player;
@synthesize portrainImage;
@synthesize staticImageView;
@synthesize imageWithBrightnessPortrait;
@synthesize imageWithBrightness;
@synthesize device_angle;
@synthesize device_angle_z;
@synthesize delayCount;
@synthesize delayTimer;
@synthesize liveSwitcher;
@synthesize photoButton;
@synthesize markersButton;
@synthesize homeButton;
@synthesize sessionButton;
@synthesize cropArea;
@synthesize snappedAngle;
@synthesize snappedAngleZ;
@synthesize pdModeSegmentedControl;
@synthesize snapImage;
@synthesize FarPhotoDeviceAngle;
@synthesize volumeSlider;
@synthesize visibleSliderFocus;
@synthesize visibleSliderExposure;
@synthesize lableSliderExposure;
@synthesize lableSliderFocus;

- (int)Round:(double)Arg
{
    return (int)floor(Arg+0.5);
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Exposure View

- (void)focusWithMode:(AVCaptureFocusMode)focusMode
       exposeWithMode:(AVCaptureExposureMode)exposureMode
        atDevicePoint:(CGPoint)point monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange
{
    AVCaptureDevice *device = self.cameraView.cameraDevice;
    NSError *error = nil;
    
    if ([device lockForConfiguration:&error])
    {
        if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:focusMode]) {
            
            [device setFocusMode:focusMode];
            [device setFocusPointOfInterest:point];
        }
        
        if ([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:exposureMode]) {
            
            [device setExposureMode:exposureMode];
            [device setExposurePointOfInterest:point];
        }
        
        [device setSubjectAreaChangeMonitoringEnabled:monitorSubjectAreaChange];
        [device unlockForConfiguration];
    }
    else
    {
        NSLog(@"%@", error);
    }
}

- (void)showExposureView:(UIGestureRecognizer *)recognizer
{
    if (!(IS_IOS8_AND_LATER))
    {
        return;
    }
    
    [self sliderExposureBegan:nil];
    
    CGPoint tappedPoint = [recognizer locationInView:self.view];
    CGFloat xCoordinate = tappedPoint.x;
    CGFloat yCoordinate = tappedPoint.y;
    
    // Don't show exposure view in header
    if (yCoordinate < 160.0)
    {
        return;
    }
    
    self.sliderExposure.minimumValue = self.cameraView.cameraDevice.minExposureTargetBias / 2.f;
    self.sliderExposure.maximumValue = self.cameraView.cameraDevice.maxExposureTargetBias / 2.6f;
    
    self.sliderExposure.value = self.cameraView.cameraDevice.exposureTargetBias;
    
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI_2 * 3);
    self.sliderExposure.transform = trans;
    
    self.settingCameraView.hidden = YES;
    
    self.settingCameraView.frame = CGRectMake(xCoordinate - (self.settingCameraView.frame.size.width / 2),
                                         yCoordinate - (self.settingCameraView.frame.size.height / 2),
                                         self.settingCameraView.frame.size.width,
                                         self.settingCameraView.frame.size.height);
        
        pointOfInterest = [recognizer locationInView:self.view];
        pointOfInterest.x /= self.view.frame.size.width;
        pointOfInterest.y /= self.view.frame.size.height;
        
        [self focusWithMode:AVCaptureFocusModeContinuousAutoFocus
             exposeWithMode:AVCaptureExposureModeContinuousAutoExposure
              atDevicePoint:pointOfInterest monitorSubjectAreaChange:NO];
    
    [UIView animateWithDuration:0.6 animations:^{
        [visibleSliderFocus setValue:0.5 animated:YES];
    }];
    
    [self showExposureSliderView];
    
    [self sliderExposureEnded:nil];
}

- (void)showExposureSliderView
{
    if ([farPdMode boolValue] && !snapImage)
    {
        [self exposureWork];
    }
    
    if (![farPdMode boolValue] && !portrainImage)
    {
        [self exposureWork];
    }
}

- (void)exposureWork
{
    [UIView animateWithDuration:0.2 animations:^{
        self.settingCameraView.alpha = 1;
    } completion: ^(BOOL finished) {
        self.settingCameraView.hidden = !finished;
    }];
}

- (void)hideExposureSliderView
{
    [UIView animateWithDuration:0.4 animations:^{
        self.settingCameraView.alpha = 0;
    } completion: ^(BOOL finished) {
        self.settingCameraView.hidden = finished;
    }];
}

- (IBAction)sliderExposureBegan:(id)sender;
{
    [timerHideExposure invalidate];
    timerHideExposure = nil;
}

- (IBAction)sliderExposureEnded:(id)sender
{
    [self starSliderCameraTimerHidden];
}

- (void)starSliderCameraTimerHidden
{
    timerHideExposure = [NSTimer scheduledTimerWithTimeInterval:2.f
                                                         target:self
                                                       selector:@selector(hideExposureSliderView)
                                                       userInfo:nil
                                                        repeats:NO];
}

- (IBAction)sliderExposureAction:(UISlider *)sender
{
    NSError *error = nil;
    
    if (IS_IOS8_AND_LATER)
    {
        if ([self.cameraView.cameraDevice lockForConfiguration:&error])
        {
            [self.cameraView.cameraDevice setExposureTargetBias:sender.value / 2.f completionHandler:nil];
            
            self.visibleSliderExposure.value = sender.value;
        }
    }
    
    [self sliderExposureBegan:nil];
}

- (void)changeExposure:(UIGestureRecognizer *)recognizer
{
    [self sliderExposureBegan:nil];
    
    CGPoint tappedPoint = [recognizer locationInView:self.view];
    CGFloat yCoordinate = tappedPoint.y;
    
    float value = self.sliderExposure.value;
    
    if (self.settingCameraView.alpha == 1) {
        
        if (yyyyy > yCoordinate) {
            [self.sliderExposure setValue:value + 0.2 animated:YES];
            [self sliderExposureAction:self.sliderExposure];
            yyyyy = yCoordinate;
        } else {
            [self.sliderExposure setValue:value - 0.2 animated:YES];
            [self sliderExposureAction:self.sliderExposure];
            yyyyy = yCoordinate;
        }
    }
    
    [self sliderExposureEnded:nil];
}

#pragma mark - ViewDidLoad

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([PD_MANAGER useIPadFlash])
        [self setupRetinaFlashView];
    
    yyyyy = 0;
    
    [self.sliderExposure setThumbImage:[UIImage imageNamed:@"ThumbSun"] forState:UIControlStateNormal];
    [self.sliderExposure setMinimumTrackImage:[UIImage imageNamed:@"sliderTrack"] forState:UIControlStateNormal];
    [self.sliderExposure setMaximumTrackImage:[UIImage imageNamed:@"sliderTrack"] forState:UIControlStateNormal];
    
    cameraTap =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                       action:@selector(showExposureView:)];
    [self.cameraView addGestureRecognizer:cameraTap];
    
    rotateStatus = NO;
    
    PDDeviceDetails details = [[PDDeviceInfo alloc] deviceDetails];
    currentDevice = details.model;
    
    if (![PD_MANAGER showDeviceAngle])
    {
        DALabel.hidden = YES;
        DATextField.hidden = YES;
        DAzTextField.hidden = YES;
    }
    
    bFarDeviceAnglePresent = false;
    FarPhotoDeviceAngle = 0.;
    trackingAngle = 0.;
    
    flashlightDetector = [FlashlightDetector new];
    flashlightDetector.delegate = self;
    needDetectFlashlightType = YES;
    
    [leftOKButton setButtonType:PDCameraButtonTypeSimple position:PDButtonPositionLeft];
    [rightOKButton setButtonType:PDCameraButtonTypeSimple position:PDButtonPositionRight];
    
    nearHelperLabel.hidden = YES;
    
    touchView.delegate = self;
    touchView.userInteractionEnabled = NO;
    
    firstStart = YES;
    farModeIsDetermined = NO;
    fromResults = NO;
    returnedFromResults = NO;
    stopAccelerometer = NO;
    faceModeCamera = NO;
    imageIsSnapped = NO;

    secondPhotoMode = @(NO);
    farPdMode = @([PDM_DATA getFarMode]);
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    [self removeXMLDataForFar:YES andForNear:YES];
    
    if (!ENABLE_LIVE_SWITCHER) {
        [liveSwitcher setHidden:YES];
        liveSwitcher.on = YES;
        [markersButton setEnabled:NO];
    } else {
        [markersButton setEnabled:YES];
    }
    
    delayView.hidden = YES;
    
    roundView.layer.cornerRadius = 55.f;
    nearDelayView.layer.cornerRadius = 22.f;
    
    [self showWarning:NO andArrowsUp:NO];
    
    if ([PD_MANAGER targetScreen] != PDTargetScreenCamera && [PDM_DATA hasSnapImage])
    {
        snapImage = [PDM_DATA getRestoreImage]; 
    }
    
    nearTouchLabel.text = NSLocalizedString(@"Please look at the red circle", @"");
    nearHelperLabel.text = NSLocalizedString(@"Please take iPad in your hands. Press OK when ready", @"");
    nearWarningLabel.text = NSLocalizedString(@"Please hold iPad in reading position", @"");
    warningLabel.text = NSLocalizedString(@"Please hold iPad at eye level of the customer", @"");
    secondPhotoHelperLabel.text = NSLocalizedString(@"Please make photo from a side", @"");
    brightnessLabel.text = NSLocalizedString(@"Brightness", @"");
    
    [photoButton setTitle:NSLocalizedString(@"New photo", @"") forState:UIControlStateNormal];
    [markersButton setTitle:NSLocalizedString(@"Markers", @"") forState:UIControlStateNormal];
    [pdModeSegmentedControl setTitle:NSLocalizedString(@"Near PD", @"") forSegmentAtIndex:0];
    [pdModeSegmentedControl setTitle:NSLocalizedString(@"Far PD", @"") forSegmentAtIndex:1];
    
    // Set video preset
    switch (currentDevice) {
        case PDDeviceModeliPad:
        case PDDeviceModeliPad2:
        case PDDeviceModeliPad3:
        case PDDeviceModeliPad4:
        case PDDeviceModeliPadMini:
            break;
        case PDDeviceModeliPadMiniRetina:
        case PDDeviceModeliPadMini4:
        case PDDeviceModeliPadAir:
        case PDDeviceModeliPadAir2:
        case PDDeviceModeliPad97_2017:
        case PDDeviceModeliPadPro97:
        case PDDeviceModeliPadPro129:
        case PDDeviceModeliPadPro105:
        case PDDeviceModeliPadPro129_2017:
        case PDDeviceModeliPadAir2019:
        case PDDeviceModeliPadPro11:
        case PDDeviceModeliPadPro129_2018:
        default:
            [ self.cameraView setCameraPreset : AVCaptureSessionPresetPhoto ];
            break;
    }
    
    // Back zoom factor
    float backZoomFactor = 1.278;
    bool bCorrected = NO;
    switch (currentDevice) {
        case PDDeviceModeliPad:
        case PDDeviceModeliPad2:
        case PDDeviceModeliPad3:
        case PDDeviceModeliPad4:
        case PDDeviceModeliPadMini:
            backZoomFactor = 1.0;
            break;
        case PDDeviceModeliPadMiniRetina:
        case PDDeviceModeliPadMini3:
        case PDDeviceModeliPadAir:
            backZoomFactor = 1.323;
            bCorrected = true;
            break;
        case PDDeviceModeliPadMini4:
            backZoomFactor = 1.333483;
            bCorrected = true;
            break;
        case PDDeviceModeliPadAir2:
        case PDDeviceModeliPad97_2017:
        case PDDeviceModeliPadAir2019:
            backZoomFactor = 1.338216;
            bCorrected = true;
            break;
        case PDDeviceModeliPadPro129:
            backZoomFactor = 1.335485;
            bCorrected = true;
            break;
        case PDDeviceModeliPadPro97:
            backZoomFactor = 1.409791;
            bCorrected = true;
            break;
        case PDDeviceModeliPadPro105:
        case PDDeviceModeliPadPro129_2017:
            backZoomFactor = 1.470648;
            bCorrected = true;
            break;
        case PDDeviceModeliPadPro11:
        case PDDeviceModeliPadPro129_2018:
            backZoomFactor = 1.4374;
            bCorrected = true;
            break;
        default:
            backZoomFactor = 1.41;
            bCorrected = true;
            break;
    }
    scaleBack = 2.4 * backZoomFactor / 1.239;

    if( bCorrected )
        scaleBack *= 1.105 * STATIC_VIEW_WIDTH / STATIC_VIEW_HEIGHT * 3. / 4.;

    // Front zoom factor
    float frontZoomFactor = 1.25;
    switch (details.model) {
        case PDDeviceModeliPad:
        case PDDeviceModeliPad2:
        case PDDeviceModeliPad3:
            frontZoomFactor = 1.25;
            break;
        case PDDeviceModeliPad4:
        case PDDeviceModeliPadMini:
            frontZoomFactor = 1.065;
            break;
        case PDDeviceModeliPadMiniRetina:
        case PDDeviceModeliPadMini3:
        case PDDeviceModeliPadAir:
            frontZoomFactor = 1.25;
            break;
        case PDDeviceModeliPadMini4:
            frontZoomFactor = 1.2335;
            break;
        case PDDeviceModeliPadAir2:
        case PDDeviceModeliPad97_2017:
            frontZoomFactor = 1.232;
            break;
        case PDDeviceModeliPadPro129:
            frontZoomFactor = 1.251;
            break;
        case PDDeviceModeliPadPro97:
            frontZoomFactor = 1.21;
            break;
        case PDDeviceModeliPadPro105:
        case PDDeviceModeliPadPro129_2017:
        case PDDeviceModeliPadAir2019:
        case PDDeviceModeliPadPro11:
        case PDDeviceModeliPadPro129_2018:
            frontZoomFactor = 1.225;
            break;
        default:
            frontZoomFactor = 1.225;
            break;
    }
    scaleFront = frontZoomFactor * 745. / 768.;
    
    [ self.cameraView setCameraScaleForBack : scaleBack andForFront:scaleFront ];
    
    [self setupMPVolumeView];
    
    [self startCamera];

    if ([PD_MANAGER targetScreen] == PDTargetScreenCamera && ![PDM_DATA hasSnapImage])
        [self showSessionTypeAlert];
}

- (void)startAccelerometer
{
    bAccelerometerStarted = false;
    NSOperationQueue *queue = [NSOperationQueue new];
    
    __weak __typeof(self)weakSelf = self;
    
    if (self.motionManager.deviceMotionAvailable) {
        
        self.motionManager.deviceMotionUpdateInterval = 1.0/60.0;
        
        [self.motionManager startDeviceMotionUpdatesToQueue:queue withHandler: ^(CMDeviceMotion *motion, NSError *error){
            
            NSString *labelText;
            
            if (error) {
                
                [weakSelf.motionManager stopDeviceMotionUpdates];
                
                labelText = [NSString stringWithFormat:@"Device Motion encountered error: %@", error];
                
            } else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (imageIsSnapped && [farPdMode boolValue]) {
                        
                        CMAttitude *attitude;
                        
                        CMDeviceMotion *motion = weakSelf.motionManager.deviceMotion;
                        
                        attitude = motion.attitude;
                        
                        snappedAngle = -(degrees(attitude.roll) + 90.);
                        
                        NSLog(@"\n device angle = %lf",snappedAngle);
                        
                        [weakSelf continueWithSuccess:YES];
                        
                        stopAccelerometer = YES;
                        
                        imageIsSnapped = NO;
                        
                        [leftOKButton setButtonState:NO];
                        [rightOKButton setButtonState:NO];
                    }
                });
            }
        }];
    }
    
    if (self.motionManager.accelerometerAvailable) {
        
        self.motionManager.accelerometerUpdateInterval = 1.0 / 30.0;
        
        [self.motionManager startAccelerometerUpdatesToQueue:queue withHandler: ^(CMAccelerometerData *accelerometerData, NSError *error){
            
            NSString *labelText;
            
            if (error) {
                
                [weakSelf.motionManager stopAccelerometerUpdates];
                
                labelText = [NSString stringWithFormat:@"Accelerometer encountered error: %@", error];
                
            } else {
                
                [leftOKButton processAccelerometerData:accelerometerData];
                [rightOKButton processAccelerometerData:accelerometerData];
                
                bAccelerometerStarted = true;
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    // формула для вычисления угла устройства в общем случае выглядит так:
                    double cur_device_angle = atan(accelerometerData.acceleration.z / sqrt(accelerometerData.acceleration.x * accelerometerData.acceleration.x + accelerometerData.acceleration.y * accelerometerData.acceleration.y)) * 180./M_PI;
                    
                    double az = accelerometerData.acceleration.z;
                    
                    if (az < -1.) {
                        az = -1;
                    } else if (az > 1.) {
                        az = 1.;
                    }
                    
                    device_angle_z = 90. - acos(az) * 180./M_PI;
                    
                    if (i_angle_deque < n_deque_size) {
                        angle_deque[i_angle_deque] = cur_device_angle;
                        i_angle_deque++;
                    }
                    
                    if (i_angle_deque >= n_deque_size) {
                        i_angle_deque = 0;
                    }
                    
                    if (angle_deque_size < n_deque_size) {
                        angle_deque_size++;
                    }
                    
                    if (angle_deque_size > 0) {
                        double angle_deque_sum = 0.;
                        int i = 0;
                        
                        for (i = 0; i < angle_deque_size; i++) {
                            angle_deque_sum += angle_deque[i];
                        }
                        
                        device_angle = angle_deque_sum / angle_deque_size;
                        
                        if (![farPdMode boolValue])
                        {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [weakSelf trackingDeviceAngle:device_angle];
                            });
                        }
                        else
                            nearWarningLabel.hidden = YES;
                        
                        if (SHOW_DEVICE_ANGLE) {
                            
                            if ([farPdMode boolValue]) {
                                
                                if (bFarDeviceAnglePresent == false) {
                                    DATextField.text = [NSString stringWithFormat:@"%.1lf°", device_angle];
                                    DAzTextField.text = [NSString stringWithFormat:@"%.1lf°", device_angle_z];
                                }
                                
                            } else {
                                
                                if (!bNearDeviceAnglePresent && ![farPdMode boolValue])
                                {
                                    DATextField.text = [NSString stringWithFormat:@"%.1lf°", -device_angle];
                                    DAzTextField.text = [NSString stringWithFormat:@"%.1lf°", -device_angle_z];
                                }
                            }
                        }
                    }
                    
                    if (imageIsSnapped) {
                        snappedAngle = device_angle;
                        snappedAngleZ = device_angle_z;
                        
                        if (farPdMode.boolValue)
                            [weakSelf continueWithSuccess:YES];
                        
                        stopAccelerometer = YES;
                        imageIsSnapped = NO;
                        
                        [leftOKButton setButtonState:NO];
                        [rightOKButton setButtonState:NO];
                        
                        if ([farPdMode boolValue]) {
                            bFarDeviceAnglePresent = true;
                            FarPhotoDeviceAngle = snappedAngle;
                        }
                    }
                });
            }
        }];
    }
}

#pragma mark - Setup MPVolumeView

- (void)setupMPVolumeView
{
    volumeView = [[MPVolumeView alloc] initWithFrame:CGRectMake(-20.f, -20.f, 10.f, 10.f)];
    volumeView.showsRouteButton = YES;
    volumeView.showsVolumeSlider = YES;
    [self.view addSubview:volumeView];
    
    __weak __typeof(self)weakSelf = self;
    
    [[volumeView subviews] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        if ([obj isKindOfClass:[UISlider class]]) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.volumeSlider = obj;
            *stop = YES;
        }
    }];
}

- (void)startCamera
{
    self.cameraView.videoMode = YES;
    [ self.cameraView play ];
    self.cameraView.frontCamera = ![farPdMode boolValue];
    
    if( pointOfInterest.x <= 1.f && pointOfInterest.y <= 1.f )
        [self focusWithMode:AVCaptureFocusModeContinuousAutoFocus exposeWithMode:AVCaptureExposureModeContinuousAutoExposure atDevicePoint:pointOfInterest monitorSubjectAreaChange:NO];
    else
    {
        switch (currentDevice)
        {
            case PDDeviceModeliPadPro11:
            case PDDeviceModeliPadPro129_2018:
                [ self.cameraView.camera setFocus : 0.59 ];
                break;
            case PDDeviceModeliPadPro105:
                [ self.cameraView.camera setFocus : 0.77 ];
                break;
            default:
                [ self.cameraView.camera setFocus : 0.69 ];
                break;
        }
    }
}

#pragma mark - ViewDidAppear

- (void)viewDidAppear:(BOOL)animated
{
    [self moduleEntered];
    
    pdModeSegmentedControl.userInteractionEnabled = YES;
    
    [self performSelector:@selector(detectFlash) withObject:nil afterDelay:0.2f];
    
    // iOS7 layer bug
    [self performSelector:@selector(setButtonsTarget) withObject:nil afterDelay:(IS_IOS8_AND_LATER ? 0 : 0.3f)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setQueueButtonState)
                                                 name:NSUserDefaultsDidChangeNotification
                                               object:nil];
    
    [self startAccelerometer];
}

- (void)setButtonsTarget
{
    [leftOKButton setButtonSelector:@selector(snapPressed:) target:self];
    [rightOKButton setButtonSelector:@selector(snapPressed:) target:self];
}

- (void)detectFlash
{
    if (needDetectFlashlightType)
    {
        needDetectFlashlightType = NO;
        
        [flashlightDetector detectFlashlightType:NO];
    }
}

#pragma mark - ViewWillAppear

- (void)viewWillAppear:(BOOL)animated
{
    if (PDM_DATA.currentSessionType == PDMSessionProgressive &&
        PDM_DATA.currentPhotoStep == PDMPhotoNear &&
        portrainImage != nil &&
        snapImage != nil)
    {
        farPdMode = @(0);
    }
    
    [self setQueueButtonState];
    
    if ([NETWORK_MANAGER isStoreClientExists])
    {
        if (![farPdMode boolValue])
            customerLabel.frame = CGRectMake(544.0, 82.0, 200.0, 20.0);
        customerLabel.text = [NETWORK_MANAGER getCurrentClientInfo];
        customerLabel.hidden = NO;
    }
    else
        customerLabel.hidden = YES;
    
    [secondPhotoHelperLabel setHidden:![secondPhotoMode boolValue]];
    
    if (firstStart)
    {
        [self hideBrightnessControl];
        
        [self showFocusExposureControl];
    }
    
    if ([farPdMode boolValue] && self.snapImage == nil)
    {
        [self unlockElements];
    }
    
    if (![self.cameraView isRunningCamera])
    {
        [self startCamera];
    }
    
    switch ([PD_MANAGER targetScreen])
    {
        case PDTargetScreenCamera:
            break;
        case PDTargetScreenMarkers:
            [self pushToMarkersView];
            break;
        case PDTargetScreenLines:
            [self pushToLinesView];
            break;
        case PDTargetScreenResults:
            [self pushResultsView];
            break;
    }
    
    if (PDM_DATA.currentSessionType == PDMSessionFar && [PDM_DATA hasSnapImage])
    {
        snapImage = [PDM_DATA getRestoreImage];
    }
    else if (PDM_DATA.currentSessionType == PDMSessionNear && PDM_DATA.nearPDImageOriginal != nil)
    {
        portrainImage = PDM_DATA.nearPDImageOriginal;
    }
    else if (PDM_DATA.currentSessionType == PDMSessionProgressive && (PDM_DATA.farPDImage || PDM_DATA.nearPDImageOriginal != nil))
    {
        snapImage = PDM_DATA.farPDImage;
        portrainImage = PDM_DATA.nearPDImageOriginal;
        
        if (PDM_DATA.currentPhotoStep == PDMPhotoNear)
            [homeButton setImage:[UIImage imageNamed:@"Back Button"] forState:UIControlStateNormal];
        else
            [homeButton setImage:[UIImage imageNamed:@"PDHome"] forState:UIControlStateNormal];
    }
    
    if (returnedFromResults && PDM_DATA.farPDImage == nil && [farPdMode boolValue] && ![PD_MANAGER isLoadedSession])
    {
        snapImage = nil;
        staticImageView.image = nil;
    }
    
    if (snapImage != nil || portrainImage != nil)
    {
        [self configureStaticImageView];
        
        if (!warningLabel.isHidden)
        {
            [self showWarning:NO andArrowsUp:NO];
        }
        
        [photoButton setEnabled:YES];
        
        if ([farPdMode boolValue])
            [markersButton setEnabled:snapImage != nil ? YES : NO];
        else
            [markersButton setEnabled:portrainImage != nil ? YES : NO];
        
        [leftOKButton setButtonState:NO];
        [rightOKButton setButtonState:NO];
    }
    
    [self setControllerOrientation];
    
    if ((IS_IOS8_AND_LATER))
    {
        self.visibleSliderExposure.minimumValue = self.cameraView.cameraDevice.minExposureTargetBias / 2.f;
        self.visibleSliderExposure.maximumValue = self.cameraView.cameraDevice.maxExposureTargetBias / 2.6f;
        
        self.visibleSliderExposure.value = self.cameraView.cameraDevice.exposureTargetBias;
    }
    
    [PDM_DATA.analyticsHelper clear];
}

#pragma mark - Controller Orientation

- (void)setControllerOrientation
{
    if (([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortraitUpsideDown
        || [[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight)
        && !returnedFromResults)
    {
        farPdMode = @(YES);

        [self changePdMode:[farPdMode boolValue]];
    }

    if (![farPdMode boolValue])
    {
        if ([[UIDevice currentDevice] orientation] != UIDeviceOrientationPortraitUpsideDown)
        {
            rotateStatus = YES;
            
            [[UIDevice currentDevice] setValue:@(UIDeviceOrientationLandscapeLeft)
                                        forKey:@"orientation"];

            [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
            
            [self performSelector:@selector(didRotateFromInterfaceOrientation:) withObject:nil afterDelay:0.2f];
        }

        warningLabel.hidden = YES;
        
    } else if ([farPdMode boolValue]) {

        if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait)
        {
            rotateStatus = YES;
        
            [[UIDevice currentDevice] setValue:@([[UIApplication sharedApplication] statusBarOrientation])
                                    forKey:@"orientation"];
        
            [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
        }
        else
        {
            rotateStatus = NO;
        }
    }

    if (returnedFromResults)
    {
        staticImageView.image = nil;
        
        [self.cameraView resetPhoto];
        
        [rightOKButton setButtonState:YES];
        [leftOKButton setButtonState:YES];
        
        [self hideBrightnessControl];
        
        [self changePdMode:[farPdMode boolValue]];
        
        [self performSelector:@selector(placeSnapButtonsForMode)
                   withObject:nil
                   afterDelay:0.2f];
    }
    else
    {
        UIImage *image;

        if ([farPdMode boolValue])
        {
            if (!secondPhotoMode.boolValue) {
                if (snapImage) {
                    image = imageWithBrightness != nil ? imageWithBrightness : snapImage;
                    [staticImageView setImage:image];
                }
            }
            else {
                if (vdImage) {
                    image = vdImageWithBrightness != nil ? vdImageWithBrightness : vdImage;
                    [staticImageView setImage:image];
                }
            }
        }
        else {
            if (portrainImage) {
                image = imageWithBrightnessPortrait != nil ? imageWithBrightnessPortrait : portrainImage;
                [staticImageView setImage:image];
            }
        }
        
        pdModeSegmentedControl.selectedSegmentIndex = [farPdMode intValue];
        
        if ([farPdMode boolValue])
            [sceenVisor setImage:[UIImage imageNamed:@"PDVisor"]];
        else
            [sceenVisor setImage:[UIImage imageNamed:@"PDVisorPortrait"]];
    }
}

#pragma mark - ViewWillDisappear

- (void)viewWillDisappear:(BOOL)animated
{
    [ self.cameraView stopWithDeallocCam:YES ];
    
    [self.motionManager stopAccelerometerUpdates];
    [self.motionManager stopDeviceMotionUpdates];
}

#pragma mark - ViewDidDisappear

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark - Cleanup

- (void)cleanup
{
    [self removeGestures];
    
    self.cameraView = nil;
    
    flashlightDetector = nil;
    volumeSlider = nil;
}

- (void)removeGestures
{
    [self.cameraView removeGestureRecognizer:cameraTap];
}

#pragma mark - Cmotion Manager

- (CMMotionManager *)motionManager
{
    CMMotionManager *motionManager = nil;
    
    id appDelegate = [UIApplication sharedApplication].delegate;
    
    if ([appDelegate respondsToSelector:@selector(motionManager)]) {
        motionManager = [appDelegate motionManager];
    }
    
    return motionManager;
}

- (void)trackingDeviceAngle:(double)angle
{
    trackingAngle = angle;
    
    if (-angle < 20 && !nearHelperLabel.isHidden)
        [self showWarning:YES andArrowsUp:NO];
    else
        [self showWarning:NO andArrowsUp:NO];
}

#pragma mark - iPad Angle Control

- (void)showWarning:(BOOL)needToShow andArrowsUp:(BOOL)needToArrowsUp
{
    if ([farPdMode boolValue])
    {
        warningLabel.hidden = NO;
    }
    else
    {
        if (!needToShow)
            nearWarningLabel.hidden = YES;
        else
            nearWarningLabel.hidden = NO;
    }
}

- (UILabel *)okLabel
{
    if (!okLabel)
    {
        okLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100.f, 100.f)];
        okLabel.text = @"OK";
        okLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:60.f];
        okLabel.textColor = [UIColor whiteColor];
        okLabel.backgroundColor = [UIColor clearColor];
        okLabel.hidden = YES;
    }
    
    return okLabel;
}

- (void)continueWithSuccess:(BOOL)success
{
    if (!warningLabel.isHidden)
    {
        [self showWarning:NO andArrowsUp:NO];
    }
    
    if (success)
    {
        [self.cameraView takePhoto:^(BOOL success) {
            
            if (success)
            {
                [ self captureManagerStillImageCaptured:nil ];
                
                [photoButton setEnabled:YES];
                
                NSMutableDictionary *measurement = [PDMDataManager sharedManager].storage;
                [measurement setValue:[NSNumber numberWithFloat:snappedAngle] forKey:[farPdMode boolValue] ? (vdMode ? VD_SNAPPED_ANGLE : FAR_SNAPPED_ANGLE) : NEAR_SNAPPED_ANGLE];
                [measurement setValue:[NSNumber numberWithFloat:snappedAngleZ] forKey:[farPdMode boolValue] ? (vdMode ? VD_SNAPPED_ANGLE_Z : FAR_SNAPPED_ANGLE_Z) : NEAR_SNAPPED_ANGLE_Z];
            }
            else
                [self showTakingPictureErrorAlert];
            
            [self showFlash:NO];
        }];
    }
    else
    {
        [self showTakingPictureErrorAlert];
        [self showFlash:NO];
    }
    
    isSnapProcess = NO;
}

- (void)showTakingPictureErrorAlert
{
    [leftOKButton setButtonState:NO];
    [rightOKButton setButtonState:NO];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error taking picture", @"")
                                                    message:NSLocalizedString(@"Wrong device orientation", @"")
                                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

- (NSString *)getSessionPreset
{
    switch (currentDevice)
    {
        case PDDeviceModeliPad:
        case PDDeviceModeliPad2:
        case PDDeviceModeliPad3:
        case PDDeviceModeliPad4:
        case PDDeviceModeliPadMini:
            return AVCaptureSessionPresetHigh;
            break;
        case PDDeviceModeliPadMiniRetina:
        case PDDeviceModeliPadMini4:
        case PDDeviceModeliPadAir:
        case PDDeviceModeliPadAir2:
        case PDDeviceModeliPad97_2017:
        case PDDeviceModeliPadPro97:
        case PDDeviceModeliPadPro129:
        case PDDeviceModeliPadPro105:
        case PDDeviceModeliPadPro129_2017:
        case PDDeviceModeliPadAir2019:
        case PDDeviceModeliPadPro11:
        case PDDeviceModeliPadPro129_2018:
        default:
            return AVCaptureSessionPresetPhoto;
            break;
    }
    
    return AVCaptureSessionPresetPhoto;
}

#pragma mark - Camera Methods

- (UIImage *)image:(UIImage *)image rotatedByDegrees:(CGFloat)degrees
{
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    
    CGAffineTransform t = CGAffineTransformMakeRotation(degrees * M_PI / 180.0);
    
    rotatedViewBox.transform = t;
    
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    UIGraphicsBeginImageContext(rotatedSize);
    
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    CGContextRotateCTM(bitmap, degrees * M_PI / 180.0);
    
    CGContextScaleCTM(bitmap, -1.0, 1.0);
    
    CGContextDrawImage(bitmap, CGRectMake(-image.size.width / 2, -image.size.height / 2, image.size.width, image.size.height), [image CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;    
}

- (void)captureManagerStillImageCaptured:(AVCamCaptureManager *)captureManager
{
    UIImage *snapManagerImage = self.cameraView.camera.photo;//[[self captureManager] stillImage];
    
    if ([farPdMode boolValue])
        PDM_DATA.farOriginPhoto = [snapManagerImage copy];
    else
        PDM_DATA.nearOriginPhoto = [snapManagerImage copy];
    
    UIView *flashView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 1024)];
    
    [flashView setBackgroundColor:[UIColor whiteColor]];
    
    [[[self view] window] addSubview:flashView];
    
    [UIView animateWithDuration:.4f
                     animations:^{
                         [flashView setAlpha:0.f];
                     }
                     completion:^(BOOL finished){
                         [flashView removeFromSuperview];
                         [touchView setBackgroundColor:[UIColor clearColor]];
                     }
     ];
    
    if (snapManagerImage != nil)
    {
        [ self.cameraView stopWithDeallocCam:YES ];
        
        CGRect rect;
        
        if (![farPdMode boolValue]) {
            rect.size.height = snapManagerImage.size.height / scaleFront;
            rect.size.width = rect.size.height * 4. / 3.;
        } else {
            rect.size.height = snapManagerImage.size.height / scaleBack;
            rect.size.width = rect.size.height * 1.6;
        }
        
        rect.origin.x = snapManagerImage.size.width / 2.f - rect.size.width / 2.f;
        rect.origin.y = snapManagerImage.size.height / 2.f - rect.size.height / 2.f;
        
        snapManagerImage = [snapManagerImage cropByRect:rect];
        
        if (![farPdMode boolValue]) {
            snapManagerImage = [self image:snapManagerImage rotatedByDegrees:-90.0];
        }
        
        if ([farPdMode boolValue]) {
            
            if (vdMode) {
                self.vdImage = [[UIImage alloc] initWithCGImage:snapManagerImage.CGImage];
                self.vdImageWithBrightness = nil;
            } else {
                self.snapImage = [[UIImage alloc] initWithCGImage:snapManagerImage.CGImage];
                self.imageWithBrightness = nil;
            }
            
            [self configureStaticImageView];
            
        } else {

            self.portrainImage = [[UIImage alloc] initWithCGImage:[snapManagerImage mirroredImage].CGImage];
            self.imageWithBrightnessPortrait = nil;
            [self configureStaticImageView];
        }
        
    } else {
        
        if ([farPdMode boolValue]) {
            [markersButton setEnabled:NO];
        }
    }
    
    [leftOKButton setButtonState:NO];
    [rightOKButton setButtonState:NO];
    
    [self showBrightnessControl];
    
    [self hideFocusExposureControl];
    
    if (snapManagerImage)
    {
        [self performSelector:@selector(markersPressed:)
                   withObject:nil
                   afterDelay:0.7f];
    }
}

#pragma mark - Support Choise

- (void)showChoiseSupportView
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [self placeBackgoundView];
    
    if (!supportController)
    {
        supportController = [storyboard instantiateViewControllerWithIdentifier:@"SMSupportChoiseController"];
        
        supportController.delegate = self;
        
        supportController.view.frame = CGRectMake((self.view.bounds.size.width - 500.f)/2,
                                                  (self.view.bounds.size.height - 480.f)/2,
                                                  500.f,
                                                  480.f);
        
        [self.view addSubview:supportController.view];
        
        [supportController.view showInfoViewInPoint:self.view.center];
    }
}

- (void)cancelSupportChoise
{
    [supportController.view hideInfoViewWithAnimation];
    supportController = nil;
    
    [self removeBackgoundView];
}

- (void)suppportTypePressed:(u_int64_t)type
{
    [self cancelSupportChoise];
    
    [PD_MANAGER setSupportFREDType:type];
    
    [self markersPressed:nil];
}

#pragma mark Back View For Popovers

- (void)placeBackgoundView
{
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024.f, 1024.f)];
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0;
    [self.view addSubview:backgroundView];
    
    [UIView animateWithDuration:0.4f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        backgroundView.alpha = 0.75f;
    } completion:nil];
}

- (void)removeBackgoundView
{
    [UIView animateWithDuration:0.4f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        backgroundView.alpha = 0;
    } completion:^(BOOL finished){
        [backgroundView removeFromSuperview];
        backgroundView = nil;
    }];
}

#pragma mark - Actions

- (void)homeButtonPressed:(id)sender
{
    if (PDM_DATA.currentSessionType == PDMSessionProgressive && PDM_DATA.currentPhotoStep == PDMPhotoNear)
    {
        [homeButton setImage:[UIImage imageNamed:@"PDHome"] forState:UIControlStateNormal];
        
        farPdMode = @(1);
        
        rotateStatus = YES;
        
        [self changePdMode:YES];
        
        PDM_DATA.currentPhotoStep = PDMPhotoFar;
    }
    else
    {
        needDetectFlashlightType = YES;
    
        [PD_MANAGER setTargetScreen:PDTargetScreenCamera];
        
        rotateStatus = YES;
        
        [[UIDevice currentDevice] setValue:@([[UIApplication sharedApplication] statusBarOrientation])
                                    forKey:@"orientation"];
        
        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
        
        rotateStatus = NO;

        [ self.cameraView killCamera ];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:NSUserDefaultsDidChangeNotification
                                                      object:nil];
        
        [self cleanup];
        
        [self.navigationController dismissViewControllerAnimated:NO completion:nil];
    }
}

- (IBAction)btnHelp_Click:(id)sender
{
    UIViewController *controller;
    
    if ([farPdMode boolValue])
    {
        controller = [PDHelpViewController initWithPage:PDHelpTypeFarCamera];
    }
    else
    {
        CPDMDataResults resData = Results.GetPhotoResults(0);
        
        if (resData.bDataResults) {
            controller = [PDHelpViewController initWithPage:PDHelpTypeNearCamera];
        } else {
            controller = [PDHelpViewController initWithPage:PDHelpTypeNearCameraNew];
        }
    }
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (IBAction)changeMode:(UISegmentedControl *)sender
{
    farPdMode = @(sender.selectedSegmentIndex);
    
    rotateStatus = YES;
    
    if (sender.selectedSegmentIndex == 0)
        [self changePdMode:NO];
    else
        [self changePdMode:YES];
}

- (void)changePdMode:(BOOL)isFar
{
    [[UIDevice currentDevice] setValue:@([[UIApplication sharedApplication] statusBarOrientation])
                                forKey:@"orientation"];

    if (!isFar) {
        
        turnBtn.frame = CGRectMake(660.0, 31.0, 40.0, 40.0);
        
        customerLabel.frame = CGRectMake(544.0, 82.0, 200.0, 20.0);
 
        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
        
        [sceenVisor setImage:[UIImage imageNamed:@"PDVisorPortrait"]];

        warningLabel.hidden = YES;
        nearHelperLabel.hidden = NO;
        
        if (portrainImage != nil)
        {
            [leftOKButton setButtonState:NO];
            [rightOKButton setButtonState:NO];
            
            staticImageView.image = portrainImage;
            markersButton.enabled = YES;
            
            [self hideFocusExposureControl];
            [self showBrightnessControl];
        }
        else
        {
            [leftOKButton setButtonState:YES];
            [rightOKButton setButtonState:YES];
            
            staticImageView.image = nil;
            markersButton.enabled = NO;
            
            [self showFocusExposureControl];
            [self hideBrightnessControl];
        }
        
    } else {
        
        turnBtn.frame = CGRectMake(882.0, 31.0, 40.0, 40.0);
        
        customerLabel.frame = CGRectMake(804.0, 82.0, 200.0, 20.0);

        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
        
        [sceenVisor setImage:[UIImage imageNamed:@"PDVisor"]];
        
        rightOKButton.hidden = NO;
        leftOKButton.hidden = NO;
        warningLabel.hidden = NO;
        nearHelperLabel.hidden = YES;
        
        if (snapImage != nil)
        {
            [leftOKButton setButtonState:NO];
            [rightOKButton setButtonState:NO];
            
            staticImageView.image = snapImage;
            markersButton.enabled = YES;
            
            [self hideFocusExposureControl];
            [self showBrightnessControl];
        }
        else
        {
            [leftOKButton setButtonState:YES];
            [rightOKButton setButtonState:YES];
            
            staticImageView.image = nil;
            markersButton.enabled = NO;
            
            [self showFocusExposureControl];
            [self hideBrightnessControl];
        }
        
        targetNearPd = NO;
    }
    
    [PDM_DATA setFarMode:isFar];
    
    [pdModeSegmentedControl setSelectedSegmentIndex:[farPdMode intValue]];
    
    self.cameraView.frontCamera = ![farPdMode boolValue];

    if( [ farPdMode boolValue ] )
    {
        if( pointOfInterest.x <= 1.f && pointOfInterest.y <= 1.f )
            [self focusWithMode:AVCaptureFocusModeContinuousAutoFocus exposeWithMode:AVCaptureExposureModeContinuousAutoExposure atDevicePoint:pointOfInterest monitorSubjectAreaChange:NO];
        else
        {
            switch (currentDevice)
            {
                case PDDeviceModeliPadPro11:
                case PDDeviceModeliPadPro129_2018:
                    [ self.cameraView.camera setFocus : 0.59 ];
                    break;
                case PDDeviceModeliPadPro105:
                    [ self.cameraView.camera setFocus : 0.77 ];
                    break;
                default:
                    [ self.cameraView.camera setFocus : 0.69 ];
                    break;
            }
        }
    }
    else
        [self focusWithMode:AVCaptureFocusModeContinuousAutoFocus exposeWithMode:AVCaptureExposureModeContinuousAutoExposure atDevicePoint:CGPointMake(0.5f, 0.5f) monitorSubjectAreaChange:NO];
    
    [self placeSnapButtonsForMode];
}

- (void)placeSnapButtonsForMode
{
    if ([farPdMode boolValue])
    {
        leftOKButton.frame = CGRectMake(0, 254.f, leftOKButton.frame.size.width, leftOKButton.frame.size.height);
        rightOKButton.frame = CGRectMake(919.f, 254.f, leftOKButton.frame.size.width, leftOKButton.frame.size.height);
    }
    else
    {
        leftOKButton.frame = CGRectMake(0, 367.f, leftOKButton.frame.size.width, leftOKButton.frame.size.height);
        rightOKButton.frame = CGRectMake(663.f, 367.f, leftOKButton.frame.size.width, leftOKButton.frame.size.height);
    }
    
    [self changeFocusExposureOrientation];
}

#pragma mark - Snap Action

- (IBAction)snapPressed:(id)sender
{
    if (!isSnapProcess) {
        
        // Check Accelerometer started
        if (bAccelerometerStarted == false) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                            message:NSLocalizedString(@"Accelerometer may have stopped operating correctly. Please re-boot device to restore accelerometer operation.", @"")
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            return;
        }
        
        firstStart = NO;
        
        int iPhoto = ([farPdMode boolValue])? (vdMode ? 1 : 0) : 2;
        
        rPic[iPhoto].NewPhoto();
        
        staticImageView.image = nil;
        
        if (([farPdMode boolValue] && (vdMode ? vdImage != nil : snapImage != nil)) || (!farPdMode.boolValue && portrainImage))
        {
            if ([farPdMode boolValue])
            {
                vdMode ? vdImage = nil : snapImage = nil;
                vdMode ? vdImageWithBrightness = nil : imageWithBrightness = nil;
                [self removeXMLDataForFar:YES andForNear:NO];
            }
            else
            {
                portrainImage = nil;
                imageWithBrightnessPortrait = nil;
                [self removeXMLDataForFar:NO andForNear:YES];
                nearHelperLabel.hidden = NO;
            }
            
        } else {
            
            [self lockElements];
            
            [flashlightDetector detectFlashlightType:YES];
            
            if (![farPdMode boolValue])
            {
                roundView.hidden = NO;
                nearTouchLabel.hidden = NO;
                
                [PDM_DATA removeNearPDPositions];
            }
            else {
                vdMode ? [PDM_DATA removeVDPositions] : [PDM_DATA removeFarPDPositions];
                !self.secondPhotoMode.boolValue ? [PDM_DATA removeFirstPDPositions] : [PDM_DATA removeSecondPDPositions];
            }
            
            delayTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                          target:self
                                                        selector:@selector(showDelay)
                                                        userInfo:nil
                                                         repeats:YES];
        }
    }
}

- (void)configurePersonalizedNear
{
    farPdMode = @(0);
    
    returnedFromResults = NO;
    
    [markersButton setEnabled:YES];
    
    PDM_DATA.currentPhotoStep = PDMPhotoNear;
    
    [self performSegueWithIdentifier:@"markers" sender:self];
}

- (IBAction)markersPressed:(id)sender
{
    [markersButton setEnabled:YES];
    
    if ([PD_MANAGER needSupportChange])
    {
        [self showChoiseSupportView];
        
        return;
    }
    
    [PDM_DATA setCurrentSessionSupportType];
    
    if (PDM_DATA.currentSessionType == PDMSessionProgressive && PDM_DATA.currentPhotoStep == PDMPhotoFar)
    {
        [homeButton setImage:[UIImage imageNamed:@"Back Button"] forState:UIControlStateNormal];
        
        farPdMode = @(0);
        
        rotateStatus = YES;
        
        [self changePdMode:NO];
        
        if (portrainImage == nil)
            [self newPhotoPressed:nil];
        
        PDM_DATA.currentPhotoStep = PDMPhotoNear;
        
        PDM_DATA.farPDImage = snapImage;
        [PDM_DATA setRestoreImage:snapImage];
    
        return;
    }
    else if (PDM_DATA.currentSessionType == PDMSessionProgressive && PDM_DATA.currentPhotoStep == PDMPhotoNear)
    {
        farPdMode = @(1);
        PDM_DATA.currentPhotoStep = PDMPhotoFar;
    }
    
    UIImage *snapManagerImage;
    
    if (![farPdMode boolValue]) {
        snapManagerImage = imageWithBrightnessPortrait != nil ? imageWithBrightnessPortrait : portrainImage;
        PDM_DATA.nearPDImageOriginal = snapManagerImage;
    }
    else if (!secondPhotoMode.boolValue) {
        snapManagerImage = imageWithBrightness != nil ? imageWithBrightness : snapImage;
    }
    else {
        snapManagerImage = vdImageWithBrightness != nil ? vdImageWithBrightness : vdImage;
    }
    
    CGRect rect;
    if (![farPdMode boolValue]) {
        // Near photo
        switch (currentDevice) {
            case PDDeviceModeliPad:
            case PDDeviceModeliPad2:
            case PDDeviceModeliPad3:
                rect.size.width = 384;
                rect.size.height = 239;
                break;
            case PDDeviceModeliPad4:
            case PDDeviceModeliPadMini:
                rect.size.width = 678;
                rect.size.height = 422;
                break;
            case PDDeviceModeliPadMiniRetina:
            case PDDeviceModeliPadMini3:
            case PDDeviceModeliPadAir:
                rect.size.width = 768;
                rect.size.height = 477;
                break;
            case PDDeviceModeliPadMini4:
            case PDDeviceModeliPadAir2:
            case PDDeviceModeliPad97_2017:
                rect.size.width = 780;
                rect.size.height = 486;
                break;
            case PDDeviceModeliPadPro129:
                rect.size.width = 769;
                rect.size.height = 478;
                break;
            case PDDeviceModeliPadPro97:
                rect.size.width = 790;
                rect.size.height = 491;
                break;
            case PDDeviceModeliPadPro105:
            case PDDeviceModeliPadPro129_2017:
            case PDDeviceModeliPadAir2019:
            case PDDeviceModeliPadPro11:
            case PDDeviceModeliPadPro129_2018:
                rect.size.width = 1902;
                rect.size.height = 1185;
                break;
            default:
                rect.size.width = [self Round:(snapManagerImage.size.width / 1.031)];
                rect.size.height = [self Round:(rect.size.width / 1.6075)];
                break;
        }
        
    } else {
        // Far photo
        switch (currentDevice) {
            case PDDeviceModeliPad:
            case PDDeviceModeliPad2:
                rect.size.width = 482;
                rect.size.height = 300;
                break;
            case PDDeviceModeliPad3:
            case PDDeviceModeliPad4:
            case PDDeviceModeliPadMini:
                rect.size.width = 720;
                rect.size.height = 450;
                break;
            case PDDeviceModeliPadMiniRetina:
            case PDDeviceModeliPadMini3:
            case PDDeviceModeliPadAir:
                rect.size.width = 738;
                rect.size.height = 462;
                break;
            case PDDeviceModeliPadMini4:
                rect.size.width = 921;
                rect.size.height = 578;
                break;
            case PDDeviceModeliPadAir2:
            case PDDeviceModeliPad97_2017:
            case PDDeviceModeliPadAir2019:
                rect.size.width = 918;
                rect.size.height = 576;
                break;
            case PDDeviceModeliPadPro129:
                rect.size.width = 920;
                rect.size.height = 577;
                break;
            case PDDeviceModeliPadPro97:
                rect.size.width = 1074;
                rect.size.height = 673;
                break;
            case PDDeviceModeliPadPro105:
            case PDDeviceModeliPadPro129_2017:
                rect.size.width = 1022;
                rect.size.height = 641;
                break;
            case PDDeviceModeliPadPro11:
            case PDDeviceModeliPadPro129_2018:
                rect.size.width = 1041;
                rect.size.height = 653;
                break;
            default:
                rect.size.width = [self Round:(snapManagerImage.size.width / 1.23)];
                rect.size.height = [self Round:(rect.size.width / 1.5956)];
                break;
        }
    }
    
    if (PDM_DATA.currentSessionType == PDMSessionProgressive)
    {
        UIImage *snapManagerImage = imageWithBrightnessPortrait != nil ? imageWithBrightnessPortrait : portrainImage;
        
        PDM_DATA.nearPDImageOriginal = snapManagerImage;
        
        CGRect rect;
        
        // Near photo
        switch (currentDevice) {
            case PDDeviceModeliPad:
            case PDDeviceModeliPad2:
            case PDDeviceModeliPad3:
                rect.size.width = 384;
                rect.size.height = 239;
                break;
            case PDDeviceModeliPad4:
            case PDDeviceModeliPadMini:
                rect.size.width = 678;
                rect.size.height = 422;
                break;
            case PDDeviceModeliPadMiniRetina:
            case PDDeviceModeliPadMini3:
            case PDDeviceModeliPadAir:
                rect.size.width = 768;
                rect.size.height = 477;
                break;
            case PDDeviceModeliPadMini4:
            case PDDeviceModeliPadAir2:
            case PDDeviceModeliPad97_2017:
                rect.size.width = 780;
                rect.size.height = 486;
                break;
            case PDDeviceModeliPadPro129:
                rect.size.width = 769;
                rect.size.height = 478;
                break;
            case PDDeviceModeliPadPro97:
                rect.size.width = 790;
                rect.size.height = 491;
                break;
            case PDDeviceModeliPadPro105:
            case PDDeviceModeliPadPro129_2017:
            case PDDeviceModeliPadAir2019:
            case PDDeviceModeliPadPro11:
            case PDDeviceModeliPadPro129_2018:
                rect.size.width = 1902;
                rect.size.height = 1185;
                break;
            default:
                rect.size.width = [self Round:(snapManagerImage.size.width / 1.031)];
                rect.size.height = [self Round:(rect.size.width / 1.6075)];
                break;
        }
        
        rect.origin.x = snapManagerImage.size.width / 2. - rect.size.width / 2.;
        rect.origin.y = snapManagerImage.size.height / 2. - rect.size.height / 2.;
        
        snapManagerImage = [snapManagerImage cropByRect:rect];
        
        PDM_DATA.nearPDImage = snapManagerImage;
    }
    
    rect.origin.x = snapManagerImage.size.width / 2. - rect.size.width / 2.;
    rect.origin.y = snapManagerImage.size.height / 2. - rect.size.height / 2.;
    
    snapManagerImage = [snapManagerImage cropByRect:rect];
    
    if (![farPdMode boolValue])
    {
        PDM_DATA.nearPDImage = snapManagerImage;
        
    }
    else if (!secondPhotoMode.boolValue)
    {
        PDM_DATA.farPDImage = snapManagerImage;
    }
    else
    {
        PDM_DATA.vdImage = snapManagerImage;
    }

    if ([farPdMode boolValue] && PDM_DATA.appModeVD && !secondPhotoMode.boolValue)
    {
        returnedFromResults = YES;
        switchToFar = YES;
        secondPhotoMode = @(YES);
        vdMode = YES;

        [markersButton setTitle:NSLocalizedString(NSLocalizedString(@"FarPhotoForm_Next_key", @""), @"") forState:UIControlStateNormal];
    }
    else
    {
        returnedFromResults = NO;
            
        [markersButton setEnabled:YES];
            
        [self performSegueWithIdentifier:@"markers" sender:self];
    }
}

- (IBAction)newPhotoPressed:(id)sender
{
    bFarDeviceAnglePresent = false;

    staticImageView.image = nil;
    
    [ self.cameraView resetPhoto ];
    
    [self unlockElements];
    
    stopAccelerometer = NO;
    
    if (![farPdMode boolValue])
    {
        portrainImage = nil;
        
        nearHelperLabel.hidden = NO;
    }
    else
    {
        vdMode ? vdImage = nil : snapImage = nil;
        
        [PDM_DATA clearRestoreImage];
        
        warningLabel.hidden = NO;
    }
    
    [self removeXMLDataForFar:YES andForNear:NO];
    
    imageWithBrightness = nil;
    
    [markersButton setEnabled:NO];
    
    [self hideBrightnessControl];
    
    [self showFocusExposureControl];
}

#pragma mark -

- (void)hideBrightnessControl
{
    [brightnessLabel setHidden:YES];
    [brightnessSlider setHidden:YES];
    [brightnessSlider setValue:0];
}

- (void)showBrightnessControl
{
    [brightnessLabel setHidden:NO];
    [brightnessSlider setHidden:NO];
}

- (void)hideFocusExposureControl
{
    visibleSliderFocus.hidden = YES;
    visibleSliderFocus.value  = 0.5;
    lableSliderFocus.hidden   = YES;
    
    visibleSliderExposure.hidden = YES;
    lableSliderExposure.hidden   = YES;
    
    if ((IS_IOS8_AND_LATER))
    {
        visibleSliderExposure.minimumValue = self.cameraView.cameraDevice.minExposureTargetBias / 2.f;
        visibleSliderExposure.maximumValue = self.cameraView.cameraDevice.maxExposureTargetBias / 2.6f;
        
        visibleSliderExposure.value = self.cameraView.cameraDevice.exposureTargetBias;
    }
    else
        visibleSliderExposure.value = 0.5;
}

- (void)showFocusExposureControl
{
    visibleSliderFocus.hidden = [farPdMode boolValue] ? NO : YES;
    lableSliderFocus.hidden   = [farPdMode boolValue] ? NO : YES;

    visibleSliderExposure.hidden = NO;
    lableSliderExposure.hidden   = NO;
}

- (void)changeFocusExposureOrientation
{
    if ([farPdMode boolValue])
    {
        visibleSliderFocus.frame = CGRectMake(197.0, 711.0, 173.0, 31.0);
        lableSliderFocus.frame   = CGRectMake(239.0, 682.0, 89.0, 21.0);
        
        visibleSliderExposure.frame = CGRectMake(655.0, 711.0, 173.0, 31.0);
        lableSliderExposure.frame   = CGRectMake(697.0, 682.0, 89.0, 21.0);
        
        brightnessSlider.frame = CGRectMake(436.0, 711.0, 173.0, 31.0);
        brightnessLabel.frame  = CGRectMake(478.0, 682.0, 89.0, 21.0);
    }
    else
    {
        visibleSliderFocus.frame = CGRectMake(167.5, 967.0, 153.0, 31.0);
        lableSliderFocus.frame   = CGRectMake(198.5, 938.0, 89.0, 21.0);
        
        visibleSliderExposure.frame = CGRectMake(487.5, 967.0, 153.0, 31.0);
        lableSliderExposure.frame   = CGRectMake(526.0, 938.0, 89.0, 21.0);
        
        brightnessSlider.frame = CGRectMake(327.5, 967.0, 153.0, 31.0);
        brightnessLabel.frame  = CGRectMake(359.5, 938.0, 89.0, 21.0);
    }
}

- (IBAction)startNewSession:(id)sender
{
    UIAlertView *alertView;

    if (bPDStandard) {
        
        alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"New session", @"")
                                               message:NSLocalizedString(@"New measurement session", @"")
                                              delegate:self
                                     cancelButtonTitle:nil
                                     otherButtonTitles:NSLocalizedString(@"Far PD", @""),
                                                        NSLocalizedString(@"Near PD", @""),
                                                        NSLocalizedString(@"Progressive (Far + Near)",@""),
                                                        NSLocalizedString(@"Session manager",@""),
                                                        NSLocalizedString(@"Cancel", @""), nil];
    
    } else {
        
        alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"New session", @"")
                                               message:NSLocalizedString(@"New measurement session", @"")
                                              delegate:self
                                     cancelButtonTitle:nil
                                     otherButtonTitles:NSLocalizedString(@"Far PD", @""),
                                                        NSLocalizedString(@"Far PD + Vertex and wrap", @""),
                                                        NSLocalizedString(@"Near PD", @""),
                                                        NSLocalizedString(@"Session manager",@""),
                                                        NSLocalizedString(@"Cancel", @""), nil];
    }
    
    [alertView show];
}

- (void)configureStaticImageView
{    
    UIImage *staticImage;
    
    if ([farPdMode boolValue]) {
        if (vdMode) {
            staticImage = vdImageWithBrightness != nil ? vdImageWithBrightness : vdImage;
        } else {
            staticImage = imageWithBrightness != nil ? imageWithBrightness : snapImage;
        }
    }
    else {
        staticImage = imageWithBrightnessPortrait != nil ? imageWithBrightnessPortrait : portrainImage;
    }

    staticImageView.image = staticImage;
}

- (IBAction)changeBrightness:(UISlider *)sender
{
    if ([farPdMode boolValue]) {
        
        if (vdMode ? vdImage != nil : snapImage != nil) {
            
            if (vdMode) {
                vdImageWithBrightness = [vdImage imageWithBrightness:[sender value]];
            } else {
                imageWithBrightness = [snapImage imageWithBrightness:[sender value]];
            }
            
            [self configureStaticImageView];
                                  
            if (!warningLabel.hidden) {
                [self showWarning:NO andArrowsUp:NO];
            }
            
            [photoButton setEnabled:YES];
            [markersButton setEnabled:YES];
        }
        
    } else {
        
        if (portrainImage != nil) {
            imageWithBrightnessPortrait = [portrainImage imageWithBrightness:[sender value]];
            [self configureStaticImageView];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    int idxButton = (int)buttonIndex;
    
    if (alertView.tag == 2309)
    {
        if (idxButton == 0 || idxButton == 4)
        {
            PDM_DATA.currentSessionType = PDMSessionFar;
            
            [self sentCancelToPmsServer];
            farModeIsDetermined = YES;
            [self startNewSessionWithNearType:NO];
        }
        else if (idxButton == 1)
        {
            PDM_DATA.currentSessionType = PDMSessionNear;
            
            [self sentCancelToPmsServer];
            farModeIsDetermined = NO;
            [self startNewSessionWithNearType:YES];
        }
        else if (idxButton == 2)
        {
            PDM_DATA.currentSessionType = PDMSessionProgressive;
            PDM_DATA.currentPhotoStep = PDMPhotoFar;
            
            [self sentCancelToPmsServer];
            farModeIsDetermined = YES;
            [self startNewSessionWithNearType:NO];
        }
        else if (idxButton == 3)
        {
            [self pushSessionManagerView];
        }
    }
    else
    {
        if (bPDStandard) {
            
            if (idxButton == 1) {
                idxButton = 2;
            } else if (idxButton == 2) {
                idxButton = 3;
            } else if (idxButton == 3) {
                idxButton = 4;
            } else if (idxButton == 4) {
                return;
            }
        }
        
        if (self.navigationController.viewControllers.count < 2)
        {
            farModeIsDetermined = YES;
            
            switch (idxButton)
            {
                case 0: {
                    
                    PDM_DATA.currentSessionType = PDMSessionFar;
                    
                    [self sentCancelToPmsServer];
                    
                    farModeIsDetermined = YES;
                    [self startNewSessionWithNearType:NO];
                    break;
                }
                    
                case 1: {
                    
                    PDM_DATA.appModeVD = YES;
                    break;
                }
                    
                case 2: {
                
                    PDM_DATA.currentSessionType = PDMSessionNear;
                    
                    [self sentCancelToPmsServer];
                    
                    farModeIsDetermined = NO;
                    [self startNewSessionWithNearType:YES];
                    break;
                }
                    
                case 3: {
                    
                    PDM_DATA.currentSessionType = PDMSessionProgressive;
                    PDM_DATA.currentPhotoStep = PDMPhotoFar;
                    
                    [self sentCancelToPmsServer];
                    
                    farModeIsDetermined = YES;
                    [self startNewSessionWithNearType:NO];
                    break;
                }
                    
                case 4: {
                    
                    if (SESSION_MANAGER.localTargetFromSettings)
                    {
                        [self pushSessionManagerView];
                    }
                    else
                    {
                        if (NETWORK_MANAGER.networkAvaliable)
                        {
                            [self pushSessionManagerView];
                        }
                        else
                        {
                            [NETWORK_MANAGER isNotOnlineAlert];
                        }
                    }
                    break;
                }
            }
            
        } else {
            
            [PD_MANAGER setTargetScreen:PDTargetScreenCamera];
            [PD_MANAGER setLoadedState:NO];
            
            [homeButton setImage:[UIImage imageNamed:@"PDHome"] forState:UIControlStateNormal];
            
            switch (idxButton)
            {
                case 0: {
                    
                    [self sentCancelToPmsServer];
                    
                    PDM_DATA.currentSessionType = PDMSessionFar;
                    
                    targetNearPd = NO;
                    Results.NewSession();
                    
                    SESSION_MANAGER.isSavedSession = NO;
                    
                    rPic[0].NewPhoto();
                    rPic[1].NewPhoto();
                    rPic[2].NewPhoto();
                    
                    bFarDeviceAnglePresent = false;
                    farModeIsDetermined = YES;
                    [PDMDataManager sharedManager].appModeVD = NO;
                    vdMode = NO;
                    [markersButton setEnabled:NO];
                    snapImage = nil;
                    vdImage = nil;
                    vdImageWithBrightness = nil;
                    imageWithBrightness = nil;
                    portrainImage = nil;
                    imageWithBrightnessPortrait = nil;
                    [self removeXMLDataForFar:YES andForNear:YES];
                    
                    [PDM_DATA removeFarPDPositions];
                    [PDM_DATA removeVDPositions];
                    [PDM_DATA removeNearPDPositions];
                    [PDM_DATA removeFirstPDPositions];
                    [PDM_DATA removeSecondPDPositions];
                    [PDM_DATA clearImages];
                    [PDM_DATA clearCurrentFrameValues];
                    
                    secondPhotoMode = [NSNumber numberWithBool:NO];
                    [secondPhotoHelperLabel setHidden:YES];
                    
                    if (self.navigationController.viewControllers.count > 2) {
                        
                        if (![farPdMode boolValue] && returnedFromResults) {
                            returnedFromResults = 2;
                        }
                        
                        faceModeCamera = NO;
                        
                        rotateStatus = YES;
                        
                        [self changePdMode:YES];
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }
                    
                    [self newPhotoPressed:nil];
                    
                    break;
                }
                    
                case 1: {
                    
                    [self sentCancelToPmsServer];
                    
                    targetNearPd = NO;
                    Results.NewSession();
                    
                    SESSION_MANAGER.isSavedSession = NO;
                    
                    rPic[0].NewPhoto();
                    rPic[1].NewPhoto();
                    rPic[2].NewPhoto();
                    
                    bFarDeviceAnglePresent = false;
                    [PDMDataManager sharedManager].appModeVD = YES;
                    vdMode = NO;
                    farModeIsDetermined = YES;
                    [markersButton setEnabled:NO];
                    snapImage = nil;
                    vdImage = nil;
                    vdImageWithBrightness = nil;
                    imageWithBrightness = nil;
                    portrainImage = nil;
                    imageWithBrightnessPortrait = nil;
                    
                    [self removeXMLDataForFar:YES andForNear:YES];
                    
                    [PDM_DATA removeFarPDPositions];
                    [PDM_DATA removeVDPositions];
                    [PDM_DATA removeNearPDPositions];
                    [PDM_DATA removeFirstPDPositions];
                    [PDM_DATA removeSecondPDPositions];
                    [PDM_DATA clearImages];
                    [PDM_DATA clearCurrentFrameValues];
                    
                    secondPhotoMode = [NSNumber numberWithBool:NO];
                    [secondPhotoHelperLabel setHidden:YES];
                    
                    if (self.navigationController.viewControllers.count > 2) {
                        
                        if (![farPdMode boolValue] && returnedFromResults) {
                            returnedFromResults = 2;
                        }
                        
                        faceModeCamera = NO;
                        
                        rotateStatus = YES;
                        
                        [self changePdMode:YES];
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }
                    
                    [self newPhotoPressed:nil];
                    
                    break;
                }
                    
                case 2: {
                    
                    [self sentCancelToPmsServer];
                    
                    PDM_DATA.currentSessionType = PDMSessionNear;

                    Results.NewSession();
                    
                    SESSION_MANAGER.isSavedSession = NO;
                    
                    rPic[0].NewPhoto();
                    rPic[1].NewPhoto();
                    rPic[2].NewPhoto();
                    
                    bFarDeviceAnglePresent = false;
                    [self newPhotoPressed:nil];
                    [PDMDataManager sharedManager].appModeVD = NO;
                    vdMode = NO;
                    [markersButton setEnabled:NO];
                    snapImage = nil;
                    vdImage = nil;
                    vdImageWithBrightness = nil;
                    imageWithBrightness = nil;
                    portrainImage = nil;
                    imageWithBrightnessPortrait = nil;
                    farModeIsDetermined = NO;
                    [self removeXMLDataForFar:YES andForNear:YES];
                    
                    [PDM_DATA removeFarPDPositions];
                    [PDM_DATA removeVDPositions];
                    [PDM_DATA removeNearPDPositions];
                    [PDM_DATA removeFirstPDPositions];
                    [PDM_DATA removeSecondPDPositions];
                    [PDM_DATA clearImages];
                    [PDM_DATA clearCurrentFrameValues];
                    
                    secondPhotoMode = [NSNumber numberWithBool:NO];
                    [secondPhotoHelperLabel setHidden:YES];
                    
                    BOOL changeMode = NO;
                    BOOL fromFirstController = NO;
                    BOOL modalPresent = NO;
                    
                    if (self.navigationController.viewControllers.count > 2) {
                        
                        changeMode = YES;
                        modalPresent = ((PDMMarkersController *)[self.navigationController.viewControllers objectAtIndex:1]).farPdMode.boolValue;
                        [self.navigationController popToRootViewControllerAnimated:YES];
                        
                    } else {
                        
                        fromFirstController = YES;
                    }
                    
                    rotateStatus = YES;
                    
                    if (changeMode && modalPresent) {
                        
                        [self changePdMode:NO];
                        
                    } else {
                        
                        [self changePdMode:NO];
                    }
                    
                    break;
                }
                    
                case 3: {
                    
                    [self sentCancelToPmsServer];
                
                    PDM_DATA.currentSessionType = PDMSessionProgressive;
                    PDM_DATA.currentPhotoStep = PDMPhotoFar;
                    
                    targetNearPd = NO;
                    Results.NewSession();
                    
                    SESSION_MANAGER.isSavedSession = NO;
                    
                    rPic[0].NewPhoto();
                    rPic[1].NewPhoto();
                    rPic[2].NewPhoto();
                    
                    bFarDeviceAnglePresent = false;
                    farModeIsDetermined = YES;
                    [PDMDataManager sharedManager].appModeVD = NO;
                    vdMode = NO;
                    [markersButton setEnabled:NO];
                    snapImage = nil;
                    vdImage = nil;
                    vdImageWithBrightness = nil;
                    imageWithBrightness = nil;
                    portrainImage = nil;
                    imageWithBrightnessPortrait = nil;
                    [self removeXMLDataForFar:YES andForNear:YES];
                    
                    [PDM_DATA removeFarPDPositions];
                    [PDM_DATA removeVDPositions];
                    [PDM_DATA removeNearPDPositions];
                    [PDM_DATA removeFirstPDPositions];
                    [PDM_DATA removeSecondPDPositions];
                    [PDM_DATA clearImages];
                    [PDM_DATA clearCurrentFrameValues];
                    
                    secondPhotoMode = [NSNumber numberWithBool:NO];
                    [secondPhotoHelperLabel setHidden:YES];
                    
                    if (self.navigationController.viewControllers.count > 2) {
                        
                        if (![farPdMode boolValue] && returnedFromResults) {
                            returnedFromResults = 2;
                        }
                        
                        faceModeCamera = NO;
                        
                        rotateStatus = YES;
                        
                        [self changePdMode:YES];
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }
                    
                    [self newPhotoPressed:nil];
                    
                    break;
                }
                    
                case 4: {
            
                    [self pushSessionManagerView];
                    break;
                }
            }
        }
    }
}

#pragma mark - Timer Methods

- (void)showDelay
{
    if (-trackingAngle < 20 && ![farPdMode boolValue])
    {
        // wrong device angle
        [delayTimer invalidate];
        nearTouchLabel.hidden = YES;
        nearDelayView.hidden = YES;
        roundView.hidden = YES;
        nearHelperLabel.hidden = NO;
        [touchView snapProcess:NO];
        [self unlockElements];
        delayTimer = nil;
        return;
    }
    
    if (delayView.isHidden && nearDelayView.isHidden)
    {
        if ([farPdMode boolValue])
        {
            delayView.hidden = NO;
            delayCount = FAR_PD_DELAY_COUNT;
        }
        else
        {
            roundView.hidden = NO;
            nearDelayView.hidden = NO;
            delayCount = NEAR_PD_DELAY_COUNT;
        }
    }
    else
    {
        delayCount--;
    }
    
    if ([farPdMode boolValue])
        delayLabel.text = [NSString stringWithFormat:@"%ld", (long)delayCount];
    else
        nearDelayLabel.text = [NSString stringWithFormat:@"%ld", (long)delayCount];
    
    if (delayCount == 0)
    {
        [delayTimer invalidate];
        delayView.hidden = YES;
        nearTouchLabel.hidden = YES;
        nearDelayView.hidden = YES;
        roundView.hidden = YES;
        
        if (![farPdMode boolValue] && [PD_MANAGER useIPadFlash])
            retinaFlashSubView.hidden = NO;
        
        nearHelperLabel.hidden = YES;
        [touchView snapProcess:NO];
        imageIsSnapped = YES;
        delayTimer = nil;
        
        if (![farPdMode boolValue])
        {
            [self continueWithSuccess:YES];
        }
    }
    
    if (delayCount == 1)
    {
        if ([PD_MANAGER useIPadFlash]) {
            // Set Flash type
            PDDeviceDetails details = [[PDDeviceInfo alloc] deviceDetails];
            if (details.family == PDDeviceFamilyiPad) {
                switch (details.model) {
                    case PDDeviceModeliPadPro11:
                        if([PD_MANAGER flashPowerIsHigh]){
                            Results.SetFlashType(nFlashType_iPadPro11SelfHigh);
                        }else{
                            Results.SetFlashType(nFlashType_iPadPro11SelfNormal);
                        }
                        break;
                    case PDDeviceModeliPadPro129_2018:
                        if([PD_MANAGER flashPowerIsHigh]){
                            Results.SetFlashType(nFlashType_iPadPro12SelfHigh);
                        }else{
                            Results.SetFlashType(nFlashType_iPadPro12SelfNormal);
                        }
                        break;
                    default:
                        break;
                }
            }
            // Show Flash
            [self showFlash:YES];
        }
        else
            [self playSound];
    }
}

- (void)playSound
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"100Hz" ofType:@"wav"];
        
        if (flashlightDetector.currentType == FlashlightTypeV2)
        {
            PDDeviceDetails details = [[PDDeviceInfo alloc] deviceDetails];
            if (details.family == PDDeviceFamilyiPad) {
                switch (details.model) {
                    case PDDeviceModeliPad:
                    case PDDeviceModeliPad2:
                    case PDDeviceModeliPad3:
                    case PDDeviceModeliPad4:
                    case PDDeviceModeliPadAir:
                        Results.SetFlashType(nFlashType2);
                        break;
                    case PDDeviceModeliPadAir2:
                    case PDDeviceModeliPadPro97:
                    case PDDeviceModeliPad97_2017:
                        Results.SetFlashType(nFlashType_iPadAir2);
                        break;
                    case PDDeviceModeliPadPro105:
                    case PDDeviceModeliPadAir2019:
                        Results.SetFlashType(nFlashType_iPadPro10);
                        break;
                    case PDDeviceModeliPadPro129:
                    case PDDeviceModeliPadPro129_2017:
                        Results.SetFlashType(nFlashType_iPadPro12);
                        break;
                    case PDDeviceModeliPadMini:
                    case PDDeviceModeliPadMiniRetina:
                    case PDDeviceModeliPadMini3:
                    case PDDeviceModeliPadMini4:
                        Results.SetFlashType(nFlashType_iPadMini);
                        break;
                    default:
                        Results.SetFlashType(nFlashType_iPadAir2);
                        break;
                }
            }
            path = [farPdMode boolValue] ? [[NSBundle mainBundle] pathForResource:@"100HzInv_right" ofType:@"wav"] : [[NSBundle mainBundle] pathForResource:@"100HzInv_left" ofType:@"wav"];
        }
        else {
            Results.SetFlashType(nFlashType1);
        }
        NSURL *url = [NSURL fileURLWithPath:path];
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        
        [player setNumberOfLoops:1];
        [player prepareToPlay];
        
        [player play];
    });
}

- (void)lockElements
{
    pdModeSegmentedControl.userInteractionEnabled = NO;
    
    [leftOKButton setButtonState:NO];
    [rightOKButton setButtonState:NO];

    [warningLabel setHidden:YES];
    
    isSnapProcess = YES;
    [photoButton setEnabled:NO];
}

- (void)unlockElements
{
    pdModeSegmentedControl.userInteractionEnabled = YES;
    
    [leftOKButton setButtonState:YES];
    [rightOKButton setButtonState:YES];
    
    isSnapProcess = NO;
}

#pragma mark - iPad Flash

- (void)showFlash:(BOOL)show
{
    if (![PD_MANAGER useIPadFlash])
        return;
    
    if ([farPdMode boolValue])
        [self.cameraView showFlash:show];
    else
        [self showScreenFlash:show];
}

- (void)showScreenFlash:(BOOL)show
{
    if (show)
        userBrightness = [UIScreen mainScreen].brightness;
    
    UIImage *image = [PD_MANAGER flashPowerIsHigh] ? [UIImage imageNamed:@"PD_near_flash_high"] : [UIImage imageNamed:@"PD_near_flash"];
    retinaFlashView.image = image;
    
    [PDM_DATA setIpadFlashType:[PD_MANAGER flashPowerIsHigh] ? PDMFlashHigh : PDMFlashNormal];
    
    [[UIScreen mainScreen] setBrightness:show ? 1.f : userBrightness];
    retinaFlashView.hidden = !show;
    if (retinaFlashView.isHidden)
        retinaFlashSubView.hidden = YES;
}

- (void)setupRetinaFlashView
{
    retinaFlashView.layer.mask = [[CAShapeLayer layer] getShapeLayerWithFrame:retinaFlashView.frame
                                                                    holeFrame:CGRectMake(329.f, 87.f, 110.f, 110.f)];
}

#pragma mark - Audio-jack Detecting

- (void)showAlertEYEKit
{
    if (![PD_MANAGER useIPadFlash])
    {
        if (![flashlightDetector flashLightConnected])
        {
            if (!twice_alert)
            {
                twice_alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Application", @"")
                                                         message:NSLocalizedString(@"Please use Smart Mirror Mobile kit with this iPad", @"")
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
                twice_alert.tag = TWICE_ALERT;
                
                [twice_alert show];
            }
        }
        else
        {
            volumeSlider.value = 1.f;
        }
    }
}

#pragma mark - FlashDetector delegate

- (void)flashligtTypeDetected:(FlashlightType)type
{
    [self showAlertEYEKit];
}

#pragma mark - SessionManager

- (void)pushSessionManagerView
{
    firstStart = NO;
    
    PDSessionManagerViewController *sessionViewController = [[PDSessionManagerViewController alloc] initWithNibName:@"PDSessionManagerViewController"
                                                                                                             bundle:[NSBundle mainBundle]];
    sessionViewController.delegate = self;
    sessionViewController.farMode = [farPdMode boolValue];
    
    [self.navigationController pushViewController:sessionViewController animated:YES];
}

#pragma mark -

- (void)setFarMode:(NSNumber *)mode
{
    farPdMode = mode;
}

#pragma mark - XML Data

- (void)removeXMLDataForFar:(BOOL)farPD andForNear:(BOOL)nearPD
{
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSString *plistPathInResources;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"Results.plist"];
    plistPathInResources = [[NSBundle mainBundle] pathForResource:@"Results" ofType:@"plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager fileExistsAtPath:plistPath];
    
    if (success) {
        NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
        NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization propertyListFromData:plistXML
                                                                              mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                                                                        format:&format
                                                                              errorDescription:&errorDesc];
        if (!temp) {
            NSLog(@"Error reading plist: %@, format: %lu", errorDesc, (unsigned long)format);
        }
        
        NSMutableDictionary *results = [NSMutableDictionary dictionaryWithContentsOfFile: plistPath];
        
        if (farPD && [results objectForKey:FAR_PD_XML_KEY] != nil) {
            [results removeObjectForKey:FAR_PD_XML_KEY];
        }
        
        if (nearPD && [results objectForKey:NEAR_PD_XML_KEY] != nil) {
            [results removeObjectForKey:NEAR_PD_XML_KEY];
        }
        
        NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:results
                                                                       format:NSPropertyListXMLFormat_v1_0
                                                             errorDescription:nil];
        if (plistData)
        {
            [plistData writeToFile:plistPath atomically:YES];
        }
    }
}

#pragma mark - New Session

- (void)startNewSessionWithNearType:(BOOL)type
{
    Results.NewSession();
    rPic[0].NewPhoto();
    rPic[1].NewPhoto();
    rPic[2].NewPhoto();
    
    bFarDeviceAnglePresent = false;
    
    PDM_DATA.appModeVD = NO;
    
    vdMode = NO;
    bNearDeviceAnglePresent = NO;
    snapImage = nil;
    vdImage = nil;
    vdImageWithBrightness = nil;
    imageWithBrightness = nil;
    portrainImage = nil;
    imageWithBrightnessPortrait = nil;
    farModeIsDetermined = NO;
    firstStart = NO;
    
    [PDM_DATA clearImages];
    [PDM_DATA clearRestoreImage];
    [PDM_DATA removeFarPDPositions];
    [PDM_DATA removeVDPositions];
    [PDM_DATA removeNearPDPositions];
    [PDM_DATA removeFirstPDPositions];
    [PDM_DATA removeSecondPDPositions];
    [PDM_DATA clearCurrentFrameValues];
    
    [self removeXMLDataForFar:YES andForNear:YES];
    
    secondPhotoMode = @(NO);
    
    [markersButton setEnabled:NO];
    [secondPhotoHelperLabel setHidden:YES];
    
    targetNearPd = type;
    pdModeSegmentedControl.selectedSegmentIndex = !type;
    
    if (![farPdMode isEqual:@(!type)])
    {
        rotateStatus = YES;
    }
    
    farPdMode = @(!type);
    
    [self changePdMode:[farPdMode boolValue]];
    
    [PD_MANAGER setLoadedState:NO];
    
    [self newPhotoPressed:nil];
}

#pragma mark - Load Session

- (void)loadSession:(Session *)session
{
    [self newPhotoPressed:nil];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    if (!session) {
        [PD_MANAGER setLoadedState:NO];
        return;
    }
    
    Results.NewSession();
    rPic[0].NewLoadedSession();
    rPic[1].NewLoadedSession();
    rPic[2].NewLoadedSession();

    [NETWORK_MANAGER clearCurrentClient];
    customerLabel.hidden = YES;
    
    [PDM_DATA clearStorage];
    [PDM_DATA clearCurrentFrameValues];
    
    [SESSION_MANAGER fillDataManagerWithSession:session];
    
    SESSION_MANAGER.saveResultDictionary = nil;
    
    fromResults = YES;
    
    BOOL hasFarSession = [PDMDataManager sharedManager].farPDImage != nil;
    BOOL hasNearSession = [PDMDataManager sharedManager].nearPDImage != nil;
    
    if (hasNearSession && hasFarSession)
        PDM_DATA.currentSessionType = PDMSessionProgressive;
    else if (hasFarSession)
        PDM_DATA.currentSessionType = PDMSessionFar;
    else if (hasNearSession)
        PDM_DATA.currentSessionType = PDMSessionNear;
    
    snapImage = hasFarSession ? session.farPhoto.image : session.nearPhoto.image;

    [self viewWillAppear:YES];
    
    [warningLabel setHidden:YES];
    
    [leftOKButton setButtonState:NO];
    [rightOKButton setButtonState:NO];
    
    if (hasFarSession) {
        [self loaWrapMarkersDataForSession:session type:SessionTypeFarPD];
    }
    
    if (hasNearSession) {
        [self loaWrapMarkersDataForSession:session type:SessionTypeNearPD];
    }
    
    farPdMode = [NSNumber numberWithBool:hasFarSession];
    
    [PDM_DATA setRestoreImage:snapImage];
    [PDM_DATA setFarMode:[farPdMode boolValue]];
    
    [PD_MANAGER setLoadedState:YES];
    
    [self.cameraView killCamera];
    
    [self pushResultsView];
}

- (void)loaWrapMarkersDataForSession:(Session*)session type:(SessionType)type
{
    WrapMarkersData *currentWrapMarkers = [session wrapMarkersForType:type];
    
    int iPhoto = (type == SessionTypeFarPD) ? 0 : 2;
    frameMarkers.SetMarkersPosition(iPhoto, true, 0, 0., currentWrapMarkers.yRightWrap, false);
    frameMarkers.SetMarkersPosition(iPhoto, false, 2, 0., currentWrapMarkers.yLeftWrap, false);
}

- (void)loadSessionDidSelected:(Session *)session
{
    [PD_MANAGER setLoadedState:YES];
    [self loadSession:session];
}

- (void)loadSessionFromNearMode:(Session *)session
{
    returnedFromResults = YES;
    switchToFar = YES;
    secondPhotoMode = [NSNumber numberWithBool:YES];
    vdMode = YES;
    
    [self loadSession:session];
}

#pragma mark - Markers Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *destinaton = segue.destinationViewController;
    
    if ([destinaton respondsToSelector:@selector(setSnapImage:)]) {
        
        if (liveSwitcher.on) {
            
            UIImage *image;
            
            if ([farPdMode boolValue])
            {
                NSLog(@"vdMode = %i", vdMode);
                
                image = PDM_DATA.farPDImage;
            }
            else
            {
                image = PDM_DATA.nearPDImage;
            }
            
            [destinaton setValue:image forKey:@"snapImage"];

            [PDM_DATA setRestoreImage:image];
        }
        else
        {
            [destinaton setValue:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:([farPdMode boolValue] ? @"photo2.jpg" : @"photo2Portrait.jpg") ofType:nil]] forKey:@"snapImage"];
        }
    }
    
    [destinaton setValue:farPdMode forKey:@"farPdMode"];
    
    [PDM_DATA setFarMode:[farPdMode boolValue]];
    
    [destinaton setValue:[NSNumber numberWithBool:NO] forKey:@"secondPhotoMode"];
    [destinaton setValue:[NSNumber numberWithBool:NO] forKey:@"vdMode"];
}

#pragma mark - Push to Target Screen

- (void)pushToMarkersView
{
    PDMMarkersController *markersVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PDMMarkersController"];
    [markersVC setValue:snapImage forKey:@"snapImage"];
    [markersVC setValue:farPdMode forKey:@"farPdMode"];
    [markersVC setValue:[NSNumber numberWithBool:NO] forKey:@"secondPhotoMode"];
    [markersVC setValue:[NSNumber numberWithBool:NO] forKey:@"vdMode"];
    [self.navigationController pushViewController:markersVC animated:NO];
    
    [PD_MANAGER setTargetScreen:PDTargetScreenCamera];
}

- (void)pushToLinesView
{
    PDMMarkersController *markersVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PDMMarkersController"];
    [markersVC setValue:snapImage forKey:@"snapImage"];
    [markersVC setValue:farPdMode forKey:@"farPdMode"];
    [markersVC setValue:[NSNumber numberWithBool:NO] forKey:@"secondPhotoMode"];
    [markersVC setValue:[NSNumber numberWithBool:NO] forKey:@"vdMode"];
    [self.navigationController pushViewController:markersVC animated:NO];
    
    PDMLinesController *linesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PDMLinesController"];
    [linesVC setValue:snapImage forKey:@"snapImage"];
    [linesVC setValue:farPdMode forKey:@"farPdMode"];
    [linesVC setValue:[NSNumber numberWithBool:NO] forKey:@"secondPhotoMode"];
    [linesVC setValue:[NSNumber numberWithBool:NO] forKey:@"vdMode"];
    [self.navigationController pushViewController:linesVC animated:NO];
    
    [PD_MANAGER setTargetScreen:PDTargetScreenCamera];
}

- (void)pushResultsView
{
    if (PDM_DATA.currentSessionType == PDMSessionProgressive)
    {
        PDMMarkersController *markersFar = [self.storyboard instantiateViewControllerWithIdentifier:@"PDMMarkersController"];
        [markersFar setValue:snapImage forKey:@"snapImage"];
        markersFar.farPdMode = @(1);
        PDM_DATA.currentPhotoStep = PDMPhotoFar;
        [self.navigationController pushViewController:markersFar animated:NO];
        
        PDMLinesController *linesFar = [self.storyboard instantiateViewControllerWithIdentifier:@"PDMLinesController"];
        [linesFar setValue:snapImage forKey:@"snapImage"];
        linesFar.farPdMode = @(1);
        PDM_DATA.currentPhotoStep = PDMPhotoFar;
        [self.navigationController pushViewController:linesFar animated:NO];
        
        PDMMarkersController *markersNear = [self.storyboard instantiateViewControllerWithIdentifier:@"PDMMarkersController"];
        [markersNear setValue:snapImage forKey:@"snapImage"];
        markersNear.farPdMode = @(0);
        PDM_DATA.currentPhotoStep = PDMPhotoNear;
        [self.navigationController pushViewController:markersNear animated:NO];
        
        PDMLinesController *linesNear = [self.storyboard instantiateViewControllerWithIdentifier:@"PDMLinesController"];
        [linesNear setValue:snapImage forKey:@"snapImage"];
        linesNear.farPdMode = @(0);
        PDM_DATA.currentPhotoStep = PDMPhotoNear;
        [self.navigationController pushViewController:linesNear animated:NO];
        
        PDMResultsController *resultsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PDMResultsController"];
        [resultsVC setValue:snapImage forKey:@"snapImage"];
        linesNear.farPdMode = @(1);
        [self.navigationController pushViewController:resultsVC animated:NO];
    }
    else
    {
        PDMMarkersController *markersVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PDMMarkersController"];
        [markersVC setValue:snapImage forKey:@"snapImage"];
        [markersVC setValue:farPdMode forKey:@"farPdMode"];
        [markersVC setValue:[NSNumber numberWithBool:NO] forKey:@"secondPhotoMode"];
        [markersVC setValue:[NSNumber numberWithBool:NO] forKey:@"vdMode"];
        [self.navigationController pushViewController:markersVC animated:NO];
        
        PDMLinesController *linesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PDMLinesController"];
        [linesVC setValue:snapImage forKey:@"snapImage"];
        [linesVC setValue:farPdMode forKey:@"farPdMode"];
        [linesVC setValue:[NSNumber numberWithBool:NO] forKey:@"secondPhotoMode"];
        [linesVC setValue:[NSNumber numberWithBool:NO] forKey:@"vdMode"];
        [self.navigationController pushViewController:linesVC animated:NO];
        
        PDMResultsController *resultsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PDMResultsController"];
        [resultsVC setValue:snapImage forKey:@"snapImage"];
        [resultsVC setValue:farPdMode forKey:@"farPdMode"];
        [resultsVC setValue:[NSNumber numberWithBool:NO] forKey:@"secondPhotoMode"];
        [self.navigationController pushViewController:resultsVC animated:NO];
        
        [PD_MANAGER setTargetScreen:PDTargetScreenCamera];
    }
}

#pragma mark - Queue Actions

- (void)setQueueButtonState
{
    if ([APP_MANAGER getMeasureWebServiceToken]
        && [APP_MANAGER getMeasureWedServiceStoreID])
    {
        if (![farPdMode boolValue])
            turnBtn.frame = CGRectMake(660.0, 31.0, 40.0, 40.0);
        
        turnBtn.hidden = NO;
    }
    else
        turnBtn.hidden = YES;
}

- (IBAction)showTableAlert:(id)sender
{
    __weak __typeof(self)weakSelf = self;
    
    if ([NETWORK_MANAGER isStoreClientExists])
    {
        [NETWORK_MANAGER sendToServerClientStatus:StoreClientStateIdle
                                   withCompletion:^(BOOL status, NSString *reason, id response) {
                                       
                                       if (status)
                                       {
                                           [weakSelf showCustomerLabel];
                                           [weakSelf showQueuePopoverOrAlert];
                                       }
                                       else
                                           [weakSelf showAlert:NSLocalizedString(@"Error", nil)
                                                   description:reason];
                                   }];
    }
    else
        [weakSelf showQueuePopoverOrAlert];
}

- (void)showQueuePopoverOrAlert
{
    __weak __typeof(self)weakSelf = self;
    
    if ([APP_MANAGER isQueueEntryModeOnDevice])
        [weakSelf showQueueAlert];
    else
        [weakSelf showQueuePopover];
}

- (void)showQueuePopover
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [self placeBackgoundView];
    
    if (!popoverStoreAndTurn)
    {
        popoverStoreAndTurn = [storyboard instantiateViewControllerWithIdentifier:@"SMStoreAndTurnController"];
        popoverStoreAndTurn.delegate = self;
        
        [self.view addSubview:popoverStoreAndTurn.view];
        
        [popoverStoreAndTurn.view showInfoViewInPoint:CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2)];
    }
}

- (void)closeTableAlert
{
    [popoverStoreAndTurn.view hideInfoViewWithAnimation];
    popoverStoreAndTurn = nil;
    
    [self removeBackgoundView];
}

- (void)showQueueAlert
{
    __weak __typeof(self)weakSelf = self;
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:NSLocalizedString(@"Customer name", nil)
                                message:nil
                                preferredStyle:UIAlertControllerStyleAlert];
    
    __weak UIAlertController *weakAlert = alert;
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action) {
                                                   
                                                   [weakSelf customerWithFirstName:weakAlert.textFields[0].text
                                                                          lastName:weakAlert.textFields[1].text];
                                                   
                                                   [SVProgressHUD setFont:[UIFont fontWithName:@"TrebuchetMS" size:14.f]];
                                                   [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait. Updating waiting line...", nil)];
                                               }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                     style:UIAlertActionStyleDefault
                                                   handler:nil];
    [alert addAction:cancel];
    [alert addAction:ok];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"First Name", @"");
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"Last Name", @"");
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    }];
    
    [self presentViewController:alert
                       animated:YES completion:nil];
}

- (void)customerWithFirstName:(NSString *)firstName
                     lastName:(NSString *)lastName
{
    __weak __typeof(self)weakSelf = self;
    
    [NETWORK_MANAGER newQueueClientWithFirstName:firstName lastName:lastName withCompletion:^(BOOL status, NSString *reason, id response) {
        
        if (status)
        {
            [weakSelf setupCurrentClient:(SMStoreClient *)response];
            
            [weakSelf selectCustomer];
        }
        else
        {
            [weakSelf hiddenCustomerLabel];
            
            [SVProgressHUD dismiss];
            
            [weakSelf showAlert:NSLocalizedString(@"Error", nil)
                    description:reason];
        }
    }];
}

- (void)selectCustomer
{
    __weak __typeof(self)weakSelf = self;
    
    [NETWORK_MANAGER sendToServerClientStatus:StoreClientStateActive
                               withCompletion:^(BOOL status, NSString *reason, id response) {
                                   
                                   if (status)
                                   {
                                       [weakSelf showCustomerLabel];
                                       
                                       [SVProgressHUD dismiss];
                                   }
                                   else
                                   {
                                       [weakSelf hiddenCustomerLabel];
                                       
                                       [SVProgressHUD dismiss];
                                       
                                       [weakSelf showAlert:NSLocalizedString(@"Error", nil)
                                               description:reason];
                                   }
                                   
                                   [weakSelf closeTableAlert];
                               }];
}

- (void)sentCancelToPmsServer
{
    if (![NETWORK_MANAGER isStoreClientExists])
        return;
    
    __weak __typeof(self)weakSelf = self;
    
    [NETWORK_MANAGER sendToServerClientStatus:StoreClientStateIdle
                               withCompletion:^(BOOL status, NSString *reason, id response) {
                                   
                                   if (status)
                                   {
                                       [weakSelf hiddenCustomerLabel];
                                   }
                                   else
                                       NSLog(@"Error %@", reason);
                               }];
}

- (void)showCustomerLabel
{
    customerLabel.text = [NETWORK_MANAGER getCurrentClientInfo];
    customerLabel.hidden = NO;
}

- (void)hiddenCustomerLabel
{
    [NETWORK_MANAGER clearCurrentClient];
    customerLabel.hidden = YES;
}

- (void)setupCurrentClient:(SMStoreClient *)client
{
    [NETWORK_MANAGER setCurrentClient:client];
}

#pragma mark - Rotate Interface Orientation

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{    
    if (fromInterfaceOrientation != UIInterfaceOrientationLandscapeLeft && ![farPdMode boolValue])
        rotateStatus = NO;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration
{
    //[captureVideoPreviewLayer.connection setVideoOrientation:(AVCaptureVideoOrientation)[UIDevice currentDevice].orientation];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight
            || interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (BOOL)shouldAutorotate
{
    return rotateStatus;
}

#pragma mark - Google Analytics

- (void)moduleEntered
{
    [APP_MANAGER sendAnalyticsEnteredModuleWithName:@"PDMeasurement"];
}

#pragma mark - Visible Exposure Slider Action

- (IBAction)visibleExposureSliderAction:(UISlider *)sender
{
    NSError *error = nil;
    
    if (IS_IOS8_AND_LATER)
    {
        if ([self.cameraView.cameraDevice lockForConfiguration:&error])
        {
            [self.cameraView.cameraDevice setExposureTargetBias:sender.value / 2.f completionHandler:nil];
            
            self.sliderExposure.value = sender.value;
        }
    }
}

- (IBAction)visibleFocusSliderAction:(UISlider *)sender
{
    AVCaptureDevice *device = self.cameraView.cameraDevice;
    
    NSError *error = nil;
    
    if ([device isFocusModeSupported:AVCaptureFocusModeLocked] && (IS_IOS8_AND_LATER))
    {
        if ([device lockForConfiguration:&error])
        {
            [device setFocusMode:AVCaptureFocusModeLocked];
            [device setFocusModeLockedWithLensPosition:sender.value completionHandler:nil];
            [device unlockForConfiguration];
        }
    }
}

- (void)showSessionTypeAlert
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"New session", @"")
                                                        message:NSLocalizedString(@"Select new session type", @"")
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Far PD", @""),
                                                                NSLocalizedString(@"Near PD", @""),
                                                                NSLocalizedString(@"Progressive (Far + Near)",@""),
                                                                NSLocalizedString(@"Session manager", @""),
                                                                NSLocalizedString(@"Cancel", @""), nil];
    
    alertView.tag = 2309;
    
    [alertView show];
}

#pragma mark - Alerts

- (void)showAlert:(NSString *)titleString
      description:(NSString *)description
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:titleString
                                message:description
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", nil)
                               style:UIAlertActionStyleDefault
                               handler:nil];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert
                       animated:YES completion:nil];
}

@end
