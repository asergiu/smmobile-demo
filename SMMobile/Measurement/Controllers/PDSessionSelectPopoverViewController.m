//
//  PDSessionSelectPopoverViewController.m
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 24.03.13.
//
//

#import "PDSessionSelectPopoverViewController.h"

@implementation PDSessionSelectPopoverViewController

@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [fullButton setTitle:NSLocalizedString(@"Fullscreen", @"") forState:UIControlStateNormal];
    [loadButton setTitle:NSLocalizedString(@"Load Session", @"") forState:UIControlStateNormal];
    [editButton setTitle:NSLocalizedString(@"Edit", @"") forState:UIControlStateNormal];
    [deleteButton setTitle:NSLocalizedString(@"Delete", @"") forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)buttonDidSelected:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 0: //load
        {
            if (delegate && [delegate respondsToSelector:@selector(didSelectedLoadButton)])
            {
                [delegate didSelectedLoadButton];
            }
        }
            break;
           
        case 1: //edit
        {
            if (delegate && [delegate respondsToSelector:@selector(didSelectedEditButton)])
            {
                [delegate didSelectedEditButton];
            }
        }
            break;
            
        case 2: //delete
        {
            if (delegate && [delegate respondsToSelector:@selector(didSelectedDeleteButton)])
            {
                [delegate didSelectedDeleteButton];
            }
        }
            break;

        case 3: //delete from all devices
        {
            if (delegate && [delegate respondsToSelector:@selector(didSelectedDeleteALLButton)])
            {
                [delegate didSelectedDeleteALLButton];
            }
        }
            break;
            
        case 4: //fullscreen
        {
            if (delegate && [delegate respondsToSelector:@selector(didSelectedFullscreenButton)])
            {
                [delegate didSelectedFullscreenButton];
            }
        }
            break;
            
        default:
            break;
    }
}

@end
