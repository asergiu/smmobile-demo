//
//  PDShareSessionsViewController.m
//  SMMobile
//
//  Created by Sergej Bogatenko on 1/15/20.
//  Copyright © 2020 ACEP. All rights reserved.
//

#import "PDShareSessionsViewController.h"

#import "SessionManager.h"
#import "ShareSessionsManager.h"

@interface PDShareSessionsViewController ()
{
    IBOutlet UIView *bgView;
    
    IBOutlet UIView *prepareContainer;
    IBOutlet UIView *exportContainer;
    
    IBOutlet UILabel *completedLabel;
    IBOutlet UILabel *exportLabel;
    
    IBOutlet UIButton *closeBtn;
    
    IBOutlet UIProgressView *progressView;
    
    NSString *appName;
    
    BOOL startShare;
    
    UIActivityViewController *activity;
}

- (IBAction)close:(id)sender;

@end

@implementation PDShareSessionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addNotification];
    [self setupBgView];
    [self setupCloseBtn];
    [self setupExportLabel];
    
    [self performSelector:@selector(startShareSessions) withObject:nil afterDelay:0.5];
}

- (void)setAppName:(NSString *)name
{
    appName = name;
}

#pragma mark -

- (void)setupBgView
{
    bgView.layer.cornerRadius = 5.f;
    bgView.layer.masksToBounds = YES;
}

- (void)setupCloseBtn
{
    closeBtn.layer.cornerRadius = 5.f;
    closeBtn.layer.masksToBounds = YES;
    closeBtn.layer.borderWidth = 1.f;
    closeBtn.layer.borderColor = [UIColor blackColor].CGColor;
}

- (void)setupExportLabel
{
    exportLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Please click - \"copy to %@\" and wait for the automatic transition to the application", nil), appName];
}

- (void)closeView
{
    [self removeNotification];
    
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (void)setupProgressView:(NSDictionary *)dict
{
    if (prepareContainer.isHidden)
        prepareContainer.hidden = NO;
    
    NSNumber *totalCount = dict[@"sessionsCount"];
    NSNumber *writeCount = dict[@"writeSessionsCount"];
    
    [progressView setProgress:[writeCount floatValue] / [totalCount floatValue]
                     animated:YES];
    
    completedLabel.text = [NSString stringWithFormat:NSLocalizedString(@"completed %i/%i", nil),
                           [writeCount intValue], [totalCount intValue]];
}

#pragma mark - Notification

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(exportSession:)
                                                 name:PDShareExportNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillResignActive)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
}

- (void)removeNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:PDShareExportNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
}

- (void)exportSession:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setupProgressView:[notification userInfo]];
    });
}

- (void)applicationWillResignActive
{
    if (startShare)
    {
        [SHARE_MANAGER removeIfNeadedOldFile];
        [SHARE_MANAGER enableScreenSleeping:NO];
        [self closeView];
        
        [activity dismissViewControllerAnimated:NO completion:nil];
    }
}

#pragma mark - Action

- (void)close:(id)sender
{
    [self closeView];
}

#pragma mark - Share Sessions

- (void)startShareSessions
{
    [SHARE_MANAGER removeIfNeadedOldFile];
    
    if ([SESSION_MANAGER isHaveSessionsForShare])
    {
        [SHARE_MANAGER enableScreenSleeping:YES];
        [SESSION_MANAGER saveLocalSessionsForShare:^(BOOL sucsess) {
            
            NSURL *sessionsURL = [SHARE_MANAGER getFileURL];
            
            if (sessionsURL)
            {
                startShare = YES;
                
                activity = [[UIActivityViewController alloc] initWithActivityItems:@[ sessionsURL ]
                                                             applicationActivities:nil];
                
                activity.popoverPresentationController.sourceView = bgView;
                activity.popoverPresentationController.sourceRect = bgView.bounds;

                NSMutableArray *options = [[NSMutableArray alloc] initWithArray:@[UIActivityTypePostToFacebook,
                                                                                  UIActivityTypePostToTwitter,
                                                                                  UIActivityTypePostToWeibo,
                                                                                  UIActivityTypeMessage,
                                                                                  UIActivityTypeMail,
                                                                                  UIActivityTypePrint,
                                                                                  UIActivityTypeCopyToPasteboard,
                                                                                  UIActivityTypeAssignToContact,
                                                                                  UIActivityTypeSaveToCameraRoll,
                                                                                  UIActivityTypeAddToReadingList,
                                                                                  UIActivityTypePostToFlickr,
                                                                                  UIActivityTypePostToVimeo,
                                                                                  UIActivityTypePostToTencentWeibo,
                                                                                  UIActivityTypeAirDrop,
                                                                                  UIActivityTypeOpenInIBooks]];
                
                if (@available(iOS 11.0, *))
                    [options addObject:UIActivityTypeMarkupAsPDF];
                
                activity.excludedActivityTypes = options;
                    
                [self presentViewController:activity
                                   animated:YES
                                 completion:^{
                    self->exportContainer.hidden = NO;
                }];
            }
        }];
    }
}

@end
