//
//  SMSupportChoiseController.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 3/5/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import "SMSupportChoiseController.h"

#define WINDOW_WIDTH  500.f
#define WINDOW_HEIGHT 480.f
#define CORNER_RADIUS 10.f

@interface SMSupportChoiseController ()
{
    IBOutlet UIView *background;
    
    IBOutletCollection(UIButton) NSArray *supportButtons;
    IBOutletCollection(UILabel) NSArray *supportLables;
}

- (IBAction)cancelPressed:(id)sender;
- (IBAction)selectSupport:(UIButton *)sender;

@end

@implementation SMSupportChoiseController

@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    background.layer.cornerRadius = CORNER_RADIUS;
    self.view.layer.cornerRadius = CORNER_RADIUS;
    self.view.frame = CGRectMake(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
    
    for (UILabel *lbl in supportLables)
        lbl.layer.cornerRadius = 3.f;
    
    for (UIButton *btn in supportButtons)
        btn.layer.cornerRadius = 3.f;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions

- (void)cancelPressed:(id)sender
{
    if (delegate && [delegate respondsToSelector:@selector(cancelSupportChoise)])
    {
        [delegate cancelSupportChoise];
    }
}

- (void)selectSupport:(UIButton *)sender
{
    if (delegate && [delegate respondsToSelector:@selector(suppportTypePressed:)])
    {
        [delegate suppportTypePressed:(u_int64_t)sender.tag];
    }
}

@end
