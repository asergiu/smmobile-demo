#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "PDMEyeView.h"
#import "PDFTPSelectPopover.h"
#import "AsyncUdpSocket.h"
#import "Session.h"

@interface PDMResultsController : UIViewController <UIScrollViewDelegate, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate, UIPopoverControllerDelegate, UIAlertViewDelegate, PDFTPSelectPopoverDelegate, NSStreamDelegate>
{
    IBOutlet UILabel *m_labelLensType;
    IBOutlet UILabel *m_labelLensData;
    IBOutlet UILabel *m_labelResults;
    IBOutlet UILabel *m_labelWrapAngle;
    IBOutlet UILabel *m_labelReadingDist;
    IBOutlet UILabel *m_labelHeight1;
    IBOutlet UILabel *m_labelBridge;
    IBOutlet UILabel *m_labelPanto;
    IBOutlet UILabel *m_labelFrameWidth;
    IBOutlet UILabel *m_labelFrameHeight;
    
    NSString *m_url;
    NSString *m_log;
    NSString *m_pas;
    NSString *m_usrname;
    NSString *m_usrsurname;
    
    NSMutableArray *ftpSendQueue;
    
    AsyncUdpSocket *udpSocket;
    AsyncUdpSocket *udpSocketSendResults;
    
    NSMutableDictionary *m_ftpStr;
    NSTimer *udpRequestsTimer;
    
    UIImage *m_screen;
    
    NSMutableDictionary *udpServers;
    
    IBOutlet UIImageView *customerFace;
}

@property (strong, nonatomic) IBOutlet PDMEyeView *rectView;
@property (strong, nonatomic) IBOutlet PDMEyeView *rightEyeView;
@property (strong, nonatomic) IBOutlet PDMEyeView *leftEyeView;

@property (strong, nonatomic) IBOutlet UITextField *rightPD;
@property (strong, nonatomic) IBOutlet UITextField *leftPD;
@property (strong, nonatomic) IBOutlet UITextField *rightHeight;
@property (strong, nonatomic) IBOutlet UITextField *leftHeight;
@property (strong, nonatomic) IBOutlet UITextField *bridge;
@property (strong, nonatomic) IBOutlet UITextField *headPosture;
@property (strong, nonatomic) IBOutlet UITextField *distanceToIPadTextField;
@property (strong, nonatomic) IBOutlet UITextField *rightNearPD;
@property (strong, nonatomic) IBOutlet UITextField *leftNearPD;
@property (strong, nonatomic) IBOutlet UITextField *UpfitNearPD;
@property (weak, nonatomic) IBOutlet UITextField *frameWidthTextField;
@property (weak, nonatomic) IBOutlet UITextField *frameHeightTextField;
@property (weak, nonatomic) IBOutlet UITextField *wrapTextField;
@property (weak, nonatomic) IBOutlet UITextField *VDTextField;
@property (weak, nonatomic) IBOutlet UITextField *DATextField;
@property (weak, nonatomic) IBOutlet UITextField *DAzTextField;

@property (strong, nonatomic) IBOutlet UIView *leftEyeHeightTopControlView;
@property (strong, nonatomic) IBOutlet UIView *leftEyeHeightBottomControlView;
@property (strong, nonatomic) IBOutlet UIView *rightEyeHeightTopControlView;
@property (strong, nonatomic) IBOutlet UIView *rightEyeHeightBottomControlView;
@property (strong, nonatomic) IBOutlet UIView *leftPdWidthControlView;
@property (strong, nonatomic) IBOutlet UIView *rightPdWidthControlView;
@property (strong, nonatomic) IBOutlet UIView *leftBridgeControlView;
@property (strong, nonatomic) IBOutlet UIView *rightBridgeControlView;
@property (strong, nonatomic) IBOutlet UIView *scrollContentView;
@property (strong, nonatomic) IBOutlet UIView *controllersView;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIImageView *switcherImageView;
@property (weak, nonatomic) IBOutlet UIImageView *nearPdTopRightImageView;
@property (weak, nonatomic) IBOutlet UIImageView *nearPdBottomRightImageView;
@property (weak, nonatomic) IBOutlet UIImageView *nearPdTopLeftImageView;
@property (weak, nonatomic) IBOutlet UIImageView *nearPdBottomLeftImageView;

@property (strong, nonatomic) IBOutlet UILabel *farVision_R;
@property (strong, nonatomic) IBOutlet UILabel *farVision_L;
@property (strong, nonatomic) IBOutlet UILabel *nearVision_R;
@property (strong, nonatomic) IBOutlet UILabel *nearVision_L;
@property (strong, nonatomic) IBOutlet UILabel *nearVision_Upfit;
@property (strong, nonatomic) IBOutlet UILabel *infoEditLabel;
@property (strong, nonatomic) IBOutlet UILabel *pdModeLabel;
@property (strong, nonatomic) IBOutlet UILabel *farVisionBlockLabel;
@property (strong, nonatomic) IBOutlet UILabel *nearVisionBlockLabel;
@property (strong, nonatomic) IBOutlet UILabel *frameBlockLabel;
@property (strong, nonatomic) IBOutlet UILabel *personalizedBlockLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceToIPadLabel;
@property (weak, nonatomic) IBOutlet UILabel *pdLabel;
@property (weak, nonatomic) IBOutlet UILabel *pdModeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *vdWrapLabel;
@property (strong, nonatomic) IBOutlet UILabel *VDLabel;
@property (strong, nonatomic) IBOutlet UILabel *ProgressiveTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *DALabel;

@property (weak, nonatomic) IBOutlet UIButton *btnQuestion;
@property (weak, nonatomic) IBOutlet UIButton *nearPdShowDesignButton;
@property (weak, nonatomic) IBOutlet UIButton *btnWrap;
@property (strong, nonatomic) IBOutlet UIButton *editButton;


@property (strong, nonatomic) IBOutlet UIScrollView *zoomScrollView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *pdModeSegmentedControl;

@property (strong, nonatomic) IBOutletCollection(id) NSArray *testElementsToHide;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *markerViews;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *nearPdDesignViews;

@property (assign, nonatomic) BOOL singleVisionNow;
@property (nonatomic, assign) BOOL theNewIPad;

@property (nonatomic, retain) NSString *udp_ip;
@property (nonatomic, retain) NSString *udp_port;

@property (strong, nonatomic) UIImage *snapImage;

@property (strong, nonatomic) NSNumber *secondPhotoMode;
@property (strong, nonatomic) NSNumber *farPdMode;

@property (strong, nonatomic) NSMutableDictionary *measurement;

@property (assign, nonatomic) NSInteger defaultProgressiveDesign;

@property (nonatomic, strong) UIPopoverController *popoverController;

@property (nonatomic, strong) IBOutlet UIView * logoView;
@property (nonatomic, strong) IBOutlet UIView * resultsView;

@property (nonatomic, strong) IBOutlet UIImageView *mainLogoView;

@property (nonatomic, assign, readonly ) BOOL isSending;
@property (nonatomic, strong, readwrite) NSOutputStream *networkStream;
@property (nonatomic, strong, readwrite) NSInputStream *fileStream;
@property (nonatomic, assign, readonly ) uint8_t *buffer;
@property (nonatomic, assign, readwrite) size_t bufferOffset;
@property (nonatomic, assign, readwrite) size_t bufferLimit;

@property (nonatomic, retain) NSArray *frameTypes;
@property (nonatomic, retain) NSArray *metalOrAcetateBevels;
@property (nonatomic ,retain) NSArray *nylorBevels;

- (IBAction)changePDMode:(id)sender;
- (IBAction)showWrapController:(id)sender;
- (IBAction)exportResultsOrSelectLensDesignPresed:(UISegmentedControl *)sender;
- (IBAction)changeLensType:(id)sender;
- (IBAction)editResults:(id)sender;
- (IBAction)exportResults:(id)sender;
- (IBAction)showNearPdDesign:(UIButton *)sender;
- (IBAction)newSessionPressed:(id)sender;
- (IBAction)vdWrapPressed:(id)sender;
- (IBAction)showDiameters:(id)sender;
- (IBAction)btnHelp_Click:(id)sender;

- (void)configureNearPdDesignFrames;
- (void) reloadAllData;

- (void)setMaterialLabel:(bool)bRefresh;
- (void)setBevelLabel:(bool)bRefresh;
- (double)getBevelValue;

@end
