//
//  PDSessionFullScreenViewController.h
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 12.05.13.
//
//

#import <UIKit/UIKit.h>
#import "Session.h"
#import "PDSessionSelectPopoverView.h"
#import "PDSessionSelectPopoverViewController.h"

@interface PDSessionFullScreenViewController : UIViewController
{
    Session *_session;
}

@property (nonatomic, weak) IBOutlet UIImageView *image;
@property (nonatomic, strong) UIViewController *currentEditViewController;

@property (nonatomic, strong) id <PDSessionSelectPopoverViewControllerDelegate> delegate;

- (id)initWithSession:(Session*)session;

@end
