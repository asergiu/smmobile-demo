
#import <UIKit/UIKit.h>

@interface PDMMarkersController : UIViewController <UIScrollViewDelegate, UIAlertViewDelegate>
{
    IBOutlet UILabel *m_bottomLabel;
    
    UILabel *backCamLabel;
}

@property (retain, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIView *marker1;
@property (strong, nonatomic) IBOutlet UIView *marker2;
@property (strong, nonatomic) IBOutlet UIView *marker3;
@property (strong, nonatomic) IBOutlet UIView *marker4;
@property (strong, nonatomic) IBOutlet UIView *marker5;
@property (strong, nonatomic) NSNumber *farPdMode;
@property (strong, nonatomic) NSNumber *secondPhotoMode;
@property (assign, nonatomic) NSNumber *vdMode;
@property (strong, nonatomic) UIImage *snapImage;

- (void)configureDidLoad;

@end
