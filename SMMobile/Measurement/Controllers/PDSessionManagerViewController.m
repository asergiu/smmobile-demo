//
//  PDSessionManagerViewController.m
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 11.03.13.
//
//

#import "PDSessionManagerViewController.h"
#import "SessionThumbnail.h"
#import "PDSessionEditViewController.h"
#import "PDSessionFullScreenViewController.h"
#import "UIColor+ColorGenerator.h"
#import "UIView+Effects.h"
#import "PDHelpViewController.h"
#import "MBProgressHUD.h"
#import "PDSessionsSyncronizer.h"


@interface PDSessionManagerViewController () <PDSessionsSyncronizerDelegate>
{
    IBOutlet UIImageView *mainBackground;
    
    IBOutlet UILabel *dateLabel;
    
    NSArray *searchResults;
    
    BOOL rotateEnable;
}

- (IBAction)filterSwitched:(UISegmentedControl *)sender;

@end

@implementation PDSessionManagerViewController

@synthesize popover;
@synthesize delegate;
@synthesize goBack;
@synthesize farMode;
@synthesize currentFilterMode;

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Controller Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setActiveSyncMode:NO];
    
    PDSessionSelectPopoverViewController *popoverView = [[PDSessionSelectPopoverViewController alloc] initWithNibName:@"PDSessionSelectPopoverViewController"
                                                                                                               bundle:[NSBundle mainBundle]];
    popoverView.delegate = self;
    
    popover = [[UIPopoverController alloc] initWithContentViewController:popoverView];
    [popover setPopoverContentSize:popoverView.view.frame.size];
    
    selectedIndex = 0;
    selectedSection = 0;
	
    thumbsView.dataSource = self;
    thumbsView.delegate_ = self;
    
    table.delegate = self;
    table.dataSource = self;
    
    table.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    rotateEnable = NO;
    
    if (![SESSION_MANAGER localTargetFromSettings])
    {
        NSError *error = nil;
        
        [SESSION_MANAGER updateDataWithError:&error forViewController:self];
    }
    
    self.searchDisplayController.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeUpdateButtonState)
                                                 name:NSUserDefaultsDidChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadData)
                                                 name:@"reloadAllData"
                                               object:nil];
    
    [updateButton setTitle:NSLocalizedString(@"Update", @"") forState:UIControlStateNormal];
    [updateButton setTitle:NSLocalizedString(@"Update", @"") forState:UIControlStateHighlighted];
    [updateButton setTitle:NSLocalizedString(@"Update", @"") forState:UIControlStateSelected];
    
    [cancelButton setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateNormal];
    [cancelButton setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateHighlighted];
    [cancelButton setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateSelected];
    
    [filtersControl setTitle:NSLocalizedString(@"Date", @"") forSegmentAtIndex:0];
    [filtersControl setTitle:NSLocalizedString(@"Name", @"") forSegmentAtIndex:1];
    
    [mainBackground addParalaxEffect];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self reloadData];
    [self changeUpdateButtonState];
    
    SESSION_MANAGER.syncronizer.delegate = self;
    
    if (!farMode)
    {
        [self setControllerOrientation];
    }
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [SESSION_MANAGER stopSync];
    
    SESSION_MANAGER.syncronizer.delegate = nil;
    
    [super viewWillDisappear:animated];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reloadAllData" object:nil];
    
    sessionBottomView = nil;
    progressBottomView = nil;
    popover = nil;
    thumbsView = nil;
    table = nil;
    sizeLabel = nil;
    syncProgressView = nil;
}

- (void)setControllerOrientation
{
    rotateEnable = YES;
    
    [[UIDevice currentDevice] setValue:@([[UIApplication sharedApplication] statusBarOrientation])
                                forKey:@"orientation"];
    
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
    
    rotateEnable = NO;
}

#pragma mark -

- (void)setSizeLabel
{
    sizeLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Used locally: %.2f GB from %.2f GB", @""),
                      SESSION_MANAGER.dataSize / 1024.f / 1024.f / 1024.f,
                      SESSION_MANAGER.dataLimit / 1024.f / 1024.f / 1024.f ];
}

- (void)setSelectedDayTitle
{
    NSArray *dataArray = [self dataArrayForSection:selectedSection];
    
    if (dataArray && selectedIndex < dataArray.count && currentFilterMode == PDFilterModeDate)
        dateLabel.text = [(Session *)[[dataArray objectAtIndex:selectedIndex] objectAtIndex:0] humanReadableData];
    else
        dateLabel.text = nil;
}

- (void)changeUpdateButtonState
{
    [updateButton setEnabled:(![SESSION_MANAGER localTargetFromSettings])];
}

- (void)reload
{
    if (self.searchDisplayController.active)
    {
        [self.searchDisplayController.searchResultsTableView reloadData];
        [thumbsView reloadData];
    }
    else
        [self reloadData];
}

- (void)reloadData
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self setSizeLabel];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        switch (currentFilterMode) {
            case PDFilterModeDate:
                dataDict = [[NSDictionary alloc] initWithDictionary:[SESSION_MANAGER sortedBySectionsArraysFromTargetSource:SourceTypeLocal]];
                break;
            case PDFilterModeName:
                dataDict = [[NSDictionary alloc] initWithDictionary:[SESSION_MANAGER sortedByLetterSectionsFromTargetSource:SourceTypeLocal]];
                break;
        }
        
        [self setSectionKeysArray];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            [table reloadData];
            [thumbsView reloadData];
            [self setSelectedDayTitle];
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        });
    });
}

- (NSString *)sectionTitleForSection:(int)section
{
    NSString *sectionString = [[self currentSectionsArray] objectAtIndex:section];
    
    if ([sectionString isEqualToString:@"0"])
        return NSLocalizedString(@"Today", @"");
    if ([sectionString isEqualToString:@"1"])
        return NSLocalizedString(@"This week", @"");
    if ([sectionString isEqualToString:@"2"])
        return NSLocalizedString(@"Past week", @"");

    return nil;
}

- (void)setSectionKeysArray
{
    sectionArray = [[NSArray alloc] initWithArray:[self sortedArrayFromArray:[dataDict allKeys]]];
}

- (NSArray *)currentSectionsArray
{
    return sectionArray;
}

- (NSArray *)dataArrayForSection:(int)section
{
    if (![self currentSectionsArray] || section >= [[self currentSectionsArray] count])
        return nil;
    
    NSString *key = [[self currentSectionsArray] objectAtIndex:section];
    
    return [self dataArrayForSectionKey:key];
}

- (NSArray *)dataArrayForSectionKey:(NSString*)section
{
    return [dataDict objectForKey:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [searchResults count];
    }
    else
    {
        NSArray *dataArray = [self dataArrayForSection:(int)section];
        
        return dataArray.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return 1;
    }
    
    return [[self currentSectionsArray] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"Trebuchet MS" size:17.f];
    
    if (self.searchDisplayController.active)
    {
        Session *session = [searchResults objectAtIndex:indexPath.row];
        
        [cell.textLabel setFont:[UIFont systemFontOfSize:14.0]];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@ %@", session.clientFirstName,
                               session.clientLastName,
                               [session humanReadableData]];
        
        cell.textLabel.textColor = [UIColor blackColor];
    }
    else
    {
        NSArray * dataArray = [self dataArrayForSection:(int)indexPath.section];
        
        if (currentFilterMode == 0)
            cell.textLabel.text = [(Session*)[[dataArray objectAtIndex:indexPath.row] objectAtIndex:0] humanReadableData];
        else
            cell.textLabel.text = [[self currentSectionsArray] objectAtIndex:indexPath.section];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (currentFilterMode == PDFilterModeName)
    {
        return 0;
    }
    
    return 20.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300.f, 20.f)];
    
    header.backgroundColor = [UIColor R:255.f G:255.f B:255.f A:100.f];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.f, 2.f, 260.f, 16.f)];
    
    titleLabel.text = [self sectionTitleForSection:(int)section];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont fontWithName:@"Trebuchet MS" size:14.f];
    
    [header addSubview:titleLabel];
    
    return header;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedIndex != indexPath.row || selectedSection != indexPath.section)
    {
        selectedIndex   = (int)indexPath.row;
        selectedSection = (int)indexPath.section;
    
        [thumbsView reloadData];
        [self setSelectedDayTitle];
    }
}

- (NSInteger)thumbsViewNumberOfThumbs:(SessionThumbsView *)thumbsView
{
    NSArray *secArray = [self dataArrayForSection:selectedSection];
    
    if (self.searchDisplayController.active)
    {
        return searchResults.count ? 1 : 0;
    }
    
    if (!secArray || secArray.count <= selectedIndex)
    {
        return 0;
    }
    
    NSArray *dataArray = [secArray objectAtIndex:selectedIndex];
    
    if (!dataArray.count)
        return 0;
    
    return [dataArray count];
}

- (SessionThumbnail *)thumbsView:(SessionThumbsView *)_thumbsView thumbForIndex:(NSInteger)index
{
    SessionThumbnail *thumb = [_thumbsView dequeueReusableSessionThumbView];
    if (!thumb)
    {
        thumb = [[SessionThumbnail alloc] initWithFrame:CGRectZero];

        Session *currentSesion;
        
        if (self.searchDisplayController.active)
        {
            currentSesion = [searchResults objectAtIndex:selectedIndex];
        }
        else
        {
            NSArray * dataArray = [[self dataArrayForSection:selectedSection] objectAtIndex:selectedIndex];
            
            currentSesion = [dataArray objectAtIndex:index];
        }
        
        NSString *fullName = [NSString stringWithFormat:@"%@ %@", currentSesion.clientFirstName, currentSesion.clientLastName];
        
        thumb.nameLabel.text = fullName;
        
        thumb.timeLabel.text = currentSesion.humanReadableTime;
        
        NSString *nearId = currentSesion.nearPhoto.photoID;
        
        UIImage *nearPhoto = [SESSION_MANAGER photoForPhotoId:nearId source:SourceTypeLocal];
        
        if (currentSesion.type == SessionTypeFarPD) {
            thumb.typeLabel.text = (nearPhoto) ? NSLocalizedString(@"Far PD & Near PD", @"") : NSLocalizedString(@"Far PD", @"");
        } else {
            thumb.typeLabel.text = NSLocalizedString(@"Near PD", @"");
        }
        
        NSString *photoKey = currentSesion.screenShot.photoID;
        
        UIImage *onePhoto = [SESSION_MANAGER photoForPhotoId:photoKey source:SourceTypeLocal];
        
        thumb.image = onePhoto;
    }
    
    return thumb;
}

- (void)thumbsView:(SessionThumbsView *)thumbsView didSelectThumb:(SessionThumbnail *)thumb atIndex:(NSInteger)index
{
    [self.searchDisplayController.searchBar resignFirstResponder];
    
    selectedElementIndex = (int)index;
    
    [self showSessionPopower:thumb];
}

-  (void)showSessionPopower:(SessionThumbnail *)sender
{
    CGRect rect = CGRectMake(thumbsView.frame.origin.x + sender.frame.origin.x,
                             thumbsView.frame.origin.y + sender.frame.origin.y - thumbsView.contentOffset.y,
                             sender.frame.size.width, sender.frame.size.height);
    
    [popover presentPopoverFromRect:rect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp | UIPopoverArrowDirectionDown animated:YES];
}

#pragma mark - Popover View delegate methods

- (void)didSelectedLoadButton
{
    [popover dismissPopoverAnimated:NO];
        
    if (delegate && [delegate respondsToSelector:@selector(managerWillDismiss)])
       [delegate managerWillDismiss];
    
    [self.navigationController popViewControllerAnimated:NO];
    
    if (delegate && [delegate respondsToSelector:@selector(loadSessionDidSelected:)])
        [delegate loadSessionDidSelected:[self currentSelectedFilledSession]];
}

- (Session *)currentSelectedFilledSession
{
    Session *currentSesion = [self currentSession];
    
    [currentSesion fillAllData];
    
    return currentSesion;
}

- (void)didSelectedEditButton
{
    [popover dismissPopoverAnimated:NO];
    
    self.currentEditViewController = nil;
    
    PDSessionEditViewController *editViewController = [[PDSessionEditViewController alloc] initWithSession:[self currentSelectedFilledSession]];
    
    self.currentEditViewController = editViewController;
    
    [editViewController.view setFrame: thumbsView.bounds];
    
    [UIView transitionWithView:thumbsView duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft //change to whatever animation you like
                    animations:^ { [thumbsView addSubview:editViewController.view]; }
                    completion:nil];
}

- (void)didSelectedDeleteButton
{
    [popover dismissPopoverAnimated:NO];
    
    if (![SESSION_MANAGER localTargetFromSettings] && [[self currentSession] sessionFromCurrentStore])
    {
        [self deleteFromServer];
    }
    
    [[self currentSession] deleteSession];
    
    [self reloadData];
}

- (void)deleteFromServer
{
    [[self currentSession] deleteSessionFromServer];
}

- (Session *)currentSession
{
    if (self.searchDisplayController.active)
    {
        return searchResults[selectedIndex];
    }
    
    NSArray *dataArray = [self dataArrayForSection:selectedSection];
    
    return [[dataArray objectAtIndex:selectedIndex] objectAtIndex:selectedElementIndex];
}

- (void)didSelectedDeleteALLButton
{
    [popover dismissPopoverAnimated:NO];
    
    [self deleteFromServer];
    
    [self didSelectedDeleteButton];
}

- (void)didSelectedFullscreenButton
{
    [popover dismissPopoverAnimated:NO];
    
    PDSessionFullScreenViewController *fullscreen = [[PDSessionFullScreenViewController alloc] initWithSession:[self currentSession]];
    fullscreen.delegate = self;
        
    [self.navigationController pushViewController:fullscreen animated:YES];
}

- (void)setActiveSyncMode:(BOOL)activeSync
{
    sessionBottomView.hidden = activeSync;
    progressBottomView.hidden = !activeSync;
}

- (void)setProgress:(int)index outOfTotal:(int)total animated:(BOOL)animated
{
    float currentProgress = (float)index / (float)total;
    
    [syncProgressView setProgress:currentProgress animated:animated];
    
    [self setLabelForCurrentIndex:index outOfTotal:total];
}

- (void)setLabelForCurrentIndex:(int)index outOfTotal:(int)total
{
    NSString *format = NSLocalizedString(@"Processing %i from %i", @"");
    
    progressLabel.text = [NSString stringWithFormat:format, index, total];
}

#pragma mark - syncronizer delegate methods

- (void)syncDidStartedWithState:(NSString *)state
{
    [self setActiveSyncMode:YES];
    
    progressLabel.text = state;
    
    syncProgressView.hidden = YES;
}

- (void)syncDidStartedWithTotalCount:(int)total
{
    [self setActiveSyncMode:YES];
    
    [self setProgress:0 outOfTotal:total animated:NO];
    
    syncProgressView.hidden = NO;
}

- (void)loadItemNumber:(int)index outTotalCount:(int)total
{
    [self setProgress:index outOfTotal:total animated:YES];
}

- (void)syncDidFinished
{
    [self reloadData];
    [self setActiveSyncMode:NO];
}

#pragma mark - actions

- (void)filterSwitched:(UISegmentedControl *)sender
{
    currentFilterMode = (int)sender.selectedSegmentIndex;
    
    [self reloadData];
}

- (IBAction)buttonPressed:(UIButton *)sender
{
    NSError *error = nil;
    
    switch (sender.tag)
    {
        case 0://import
        {
            [SESSION_MANAGER updateDataWithError:&error forViewController:self];
        }
            break;
            
        case 1://export
        {
            [SESSION_MANAGER updateDataWithError:&error forViewController:self];
        }
            break;
            
        case 2://cancel sync
        {
            [self setActiveSyncMode:NO];
            
            [SESSION_MANAGER stopSync];
        }
            break;
            
        case 3: //import and export summary (Update button)
        {
            [SESSION_MANAGER updateDataWithError:&error forViewController:self];
        }
            break;
    }
    
    if (!error)
    {
        [self reloadData];
    }
    else
        [self errorAlertWithError:error];
}

- (void)doneAlert
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil
                                                     message:NSLocalizedString(@"Done", @"")
                                                    delegate:nil
                                           cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                           otherButtonTitles:nil];
    [alert show];
}

- (void)errorAlertWithError:(NSError*)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:error.localizedDescription
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil];
    [alert show];
}

- (IBAction)exitSessionManager:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
     
- (NSArray *)sortedArrayFromArray:(NSArray *)inputArray
{
     return [inputArray sortedArrayUsingComparator:^NSComparisonResult(NSString *str1, NSString *str2) {
         return [str1 compare:str2 options:(NSNumericSearch)];
     }];
}

#pragma mark - Search Filter

- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView
{
    tableView.frame = CGRectMake(tableView.bounds.origin.x,
                                 tableView.bounds.origin.y + 190.f,
                                 table.frame.size.width,
                                 tableView.bounds.size.height);
}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller
{
    [thumbsView reloadData];
    
    CGRect rect = CGRectMake(0.0, 0.0, 300.0, 768.0);
    
    for(UIView * v in controller.searchContentsController.view.subviews)
    {
        if ([v isKindOfClass:NSClassFromString(@"UISearchDisplayControllerContainerView")])
            v.frame = rect;
    }
}

- (void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope
{
    NSString *text = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *firstName = text;
    NSString *lastName = text;
    
    NSArray *searchArray = [searchText componentsSeparatedByString:@" "];
    
    NSPredicate *predicate = nil;
    
    if ([searchArray count] > 1 && [searchArray[1] length] > 0)
    {
        firstName = searchArray[0];
        lastName = searchArray[1];
        
        predicate = [NSPredicate predicateWithFormat:@"(clientFirstName CONTAINS[cd] %@ AND clientLastName CONTAINS[cd] %@) OR (clientLastName CONTAINS[cd] %@ AND clientFirstName CONTAINS[cd] %@)", firstName, lastName, firstName, lastName];
    }
    else
    {
        predicate = [NSPredicate predicateWithFormat:@"clientFirstName CONTAINS[c] %@ OR clientLastName CONTAINS[c] %@", firstName, lastName];
    }
    
    searchResults = [[self allSessionsArray] filteredArrayUsingPredicate:predicate];
    
    selectedIndex = 0;
    selectedSection = 0;
    
    [thumbsView reloadData];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    selectedIndex = 0;
    selectedSection = 0;
    searchResults = nil;
    
    [self reloadData];
}

- (NSArray *)allSessionsArray
{
    NSMutableArray *resultArray = [NSMutableArray array];
    
    for (NSString *key in dataDict.allKeys)
    {
        NSArray *tempArray = [NSArray arrayWithArray:[dataDict objectForKey:key]];
        
        for (NSArray *oneArray in tempArray)
        {
            resultArray = (NSMutableArray *)[resultArray arrayByAddingObjectsFromArray:oneArray];
        }
    }
    
    return resultArray;
}

#pragma mark - Help Action

- (IBAction)btnHelp_Click:(id)sender
{
    UIViewController *controller = [PDHelpViewController initWithPage:PDHelpTypeSessionsManager];
    
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - Autorotate Methods

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotate
{
    return rotateEnable;
}

@end
