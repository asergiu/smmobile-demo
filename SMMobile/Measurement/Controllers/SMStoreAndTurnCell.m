//
//  SMStoreAndTurnCell.m
//  SMMobile
//
//  Created by Work Inteleks on 5/24/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import "SMStoreAndTurnCell.h"

@implementation SMStoreAndTurnCell

@synthesize titleLabel;
@synthesize descriptionLabel;

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    self.selectionStyle = UITableViewCellSelectionStyleGray;
    
    UIView *selectedBackgroundView = [[UIView alloc] init];
    [selectedBackgroundView setBackgroundColor:[UIColor colorWithRed:216.0/255.0 green:224.0/255.0 blue:244.0/255.0 alpha:1.0]];
    [self setSelectedBackgroundView:selectedBackgroundView];
}

@end
