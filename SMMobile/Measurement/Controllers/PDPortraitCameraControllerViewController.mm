//
//  UIPortraitCameraControllerViewController.m
//  PD-Measurement
//
//  Created by Pavel Stoma on 6/8/12.
//

#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <QuartzCore/QuartzCore.h>

#include "PDMResults.h"
#import "PDPortraitCameraControllerViewController.h"
#import "PDMDataManager.h"
#import "UIImage+Crop.h"
#import "PDMCameraController.h"
#import "PDDeviceInfo.h"
#import "PDHelpViewController.h"
#import "RotatePicture.h"

extern CPDMResults Results;
extern CRotatePicture rPic[3];

extern bool bPDStandard;
extern BOOL needDetectFlashlightType;
extern BOOL needPortreitDetectFlashlightType;

@interface PDPortraitCameraControllerViewController ()

@property CGRect cropArea;
@property (strong, nonatomic) UIImage *snapImage;
@property (strong, nonatomic) NSNumber *farPdMode;
@property (nonatomic, assign) CGFloat snappedAngle;
@property (nonatomic, assign) CGFloat snappedAngleZ;
@property (nonatomic, assign) BOOL imageIsSnapped;
@property (strong, nonatomic) NSTimer *delayTimer;
@property (nonatomic, strong) UIImageView *staticImageView;
@property NSInteger delayCount;
@property BOOL isSnapProcess;
@property (strong, nonatomic) PDDeviceInfo *deviceInfo;
@property double trackingAngle;

- (void)loadCamera;
- (void)lockElements;
- (void)unlockElements;

@end

@implementation PDPortraitCameraControllerViewController

@synthesize needToDismiss;
@synthesize navItem;
@synthesize staticImageView;
@synthesize delayCount;
@synthesize delayTimer;
@synthesize snappedAngle;
@synthesize snappedAngleZ;
@synthesize isSnapProcess;
@synthesize cropArea;
@synthesize farPdMode;
@synthesize imageIsSnapped;
@synthesize parentController;
@synthesize touchView;
@synthesize roundView;
@synthesize testImageView;
@synthesize testCropImage;
@synthesize helperLabel;
@synthesize cameraPreview;
@synthesize captureManager;
@synthesize delayLabel;
@synthesize delayView;
@synthesize leftArrowImageView;
@synthesize rightArrowImageView;
@synthesize touchLabel;
@synthesize snapButtons;
@synthesize warningLabel;
@synthesize motionManager;
@synthesize captureVideoPreviewLayer;
@synthesize snapImage;
@synthesize deviceInfo;
@synthesize DALabel;
@synthesize DATextField;
@synthesize DAzTextField;
@synthesize bNearDeviceAnglePresent;
@synthesize NearPhotoDeviceAngle;
@synthesize trackingAngle;

- (void)configureStaticImageView
{
    BOOL containStatic = NO;
    
    for (UIView *view in self.cameraPreview.subviews) {
        
        if ([view isEqual:self.staticImageView]) {
            containStatic = YES;
            break;
        }
    }
    
    if (!containStatic) {
        [self.staticImageView removeFromSuperview];
        self.staticImageView = [[UIImageView alloc] initWithImage: self.parentController.imageWithBrightnessPortrait != nil ? self.parentController.imageWithBrightnessPortrait : self.parentController.portrainImage];
        self.staticImageView.frame = CGRectMake(0, 0, self.cameraPreview.frame.size.width, self.cameraPreview.frame.size.height);
        
        [self.cameraPreview addSubview:self.staticImageView];
        [self.staticImageView setContentMode:UIViewContentModeScaleAspectFill];
        [self.staticImageView setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    } else {
        self.staticImageView.image = self.parentController.imageWithBrightnessPortrait != nil ? self.parentController.imageWithBrightnessPortrait : self.parentController.portrainImage;
    }
}

- (void)continueWithSuccess: (BOOL)success
{
    [self.staticImageView removeFromSuperview];
    
    if (!self.warningLabel.hidden) {
        [self showWarning:NO andArrowsUp:NO];
    }
    
    if (success) {
        
        [[self captureManager] captureStillImage];
        
        [self.photoButton setEnabled:YES];
        [self.markersButton setEnabled:YES];
            
        snappedAngle = -self.parentController.device_angle;
        snappedAngleZ = -self.parentController.device_angle_z;
        bNearDeviceAnglePresent = true;
        
        NSMutableDictionary *measurement = [PDMDataManager sharedManager].storage;
        [measurement setValue:[NSNumber numberWithFloat:snappedAngle] forKey:NEAR_SNAPPED_ANGLE];
        [measurement setValue:[NSNumber numberWithFloat:snappedAngleZ] forKey:NEAR_SNAPPED_ANGLE_Z];

    } else {
    
        for (UIButton *button in self.snapButtons) {
            [button setEnabled:YES];
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error taking picture", @"") 
                                                        message:NSLocalizedString(@"Wrong device orientation", @"")
                                                       delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles: nil];
        [alert show];
        
    }
    
    self.isSnapProcess = NO;
    [self showBrightnessControl];
}


- (void)loadCamera
{
    [self.staticImageView removeFromSuperview];

    [self.captureVideoPreviewLayer removeFromSuperlayer];

    UIView *view = [self cameraPreview];
    CALayer *viewLayer = [view layer];
    [viewLayer setMasksToBounds:YES];

    CGRect bounds = [view bounds];
    PDDeviceDetails details = [deviceInfo deviceDetails];
   
    if (details.family == PDDeviceFamilyiPad) {
        
        switch (details.model) {
            case PDDeviceModeliPad:
            case PDDeviceModeliPad2:
            case PDDeviceModeliPad3:
                bounds.size.width *= 1.25;
                bounds.size.height *= 1.25;
                break;
            case PDDeviceModeliPad4:
            case PDDeviceModeliPadMini:
                bounds.size.width *= 1.065;
                bounds.size.height *= 1.065;
                break;
            case PDDeviceModeliPadMiniRetina:
            case PDDeviceModeliPadAir:
            default:
                bounds.size.width *= 1.25;
                bounds.size.height *= 1.25;
                break;
        }
    }

    bounds.origin.x = (view.frame.size.width / 2) -  (bounds.size.width / 2);
    bounds.origin.y = (view.frame.size.height / 2) -  (bounds.size.height / 2);
    
    self.cropArea = bounds;
    
    [self.captureVideoPreviewLayer setFrame:bounds];
    
//    if ([self.captureVideoPreviewLayer isOrientationSupported]) {
//        [self.captureVideoPreviewLayer setOrientation:(AVCaptureVideoOrientationPortrait)];
//    }
    
    [captureVideoPreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationPortrait];
    
    
    [self.captureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    [viewLayer insertSublayer:self.captureVideoPreviewLayer below:[[viewLayer sublayers] objectAtIndex:0]];	
    
    if (self.captureManager.session.isRunning) {
        [[self captureManager] continuousExposureAtPoint:CGPointMake(.5f, .5f)];
    }
}

 
- (void)viewWillAppear:(BOOL)animated
{
    if (transitionFromSessionManager) {
        transitionFromSessionManager = NO;
        return;
    }
    
    [self loadCamera];
    [super viewWillAppear:animated];
    self.parentController.faceModeCamera = YES;
    
    if (self.pdModeSegmentedControl.selectedSegmentIndex == 1) {
        [self.pdModeSegmentedControl setSelectedSegmentIndex:0];
        [self hideBrightnessControl];
    }
    
    [self configureStaticImageView];
    
    if (self.captureManager.session.isRunning) {
        [[self captureManager] continuousExposureAtPoint:CGPointMake(.5f, .5f)];
    }
}

- (void)lockElements
{
    for (UIButton *button in self.snapButtons) {
        [button setEnabled:NO];
    }        
    
    self.isSnapProcess = YES;
    [self showWarning:NO andArrowsUp:YES];
    [self.photoButton setEnabled:NO];
    
}

- (void)unlockElements
{
    for (UIButton *button in self.snapButtons) {
        [button setEnabled:YES];
    }        
    
    self.isSnapProcess = NO;
}

- (IBAction)markersPressed:(id)sender
{
    [self.staticImageView removeFromSuperview];
    [self.parentController markersPressed:nil];
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)newPhotoPressed:(id)sender
{
    bNearDeviceAnglePresent = false;
    [self.staticImageView removeFromSuperview];
    [self unlockElements];

    [[[self captureManager] session] startRunning];
    
    parentController.portrainImage = nil;
    parentController.imageWithBrightnessPortrait = nil;
    [self.helperLabel setHidden:NO];
    [parentController removeXMLDataForFar:NO andForNear:YES];
    [self.markersButton setEnabled:NO];
    [self hideBrightnessControl];
    
    // set continuous Exposure
    if (self.captureManager.session.isRunning) {
        [[self captureManager] continuousExposureAtPoint:CGPointMake(.5f, .5f)];
    }
}

- (void)pushToSession
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        parentController.farPdMode = [NSNumber numberWithBool:YES];
        self.parentController.targetNearPd = YES;
        [self.staticImageView removeFromSuperview];
        self.parentController.fromResults = NO;
        [self dismissViewControllerAnimated:YES completion:nil];
        
        [self.captureManager toggleCamera:parentController.farPdMode.boolValue];
    });
    
    [self.parentController pushSessionManagerView];
}

- (void)viewDidAppear:(BOOL)animated
{
    if (needDetectFlashlightType) {
        needDetectFlashlightType = NO;
        needPortreitDetectFlashlightType = NO;
        [parentController->flashlightDetector detectFlashlightType:NO];
    }
    
    if (parentController.portrainImage == nil) {
        [self newPhotoPressed:nil];
    }
    
    if (self.needToDismiss) {
        self.needToDismiss = NO;
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];

    PDDeviceDetails details = [deviceInfo deviceDetails];
    
    if (details.family == PDDeviceFamilyiPad) {
        switch (details.model) {
            case PDDeviceModeliPad:
            case PDDeviceModeliPad2:
            case PDDeviceModeliPad3:
            case PDDeviceModeliPad4:
            case PDDeviceModeliPadMini:
                [captureManager.session setSessionPreset:AVCaptureSessionPresetHigh];
                break;
            case PDDeviceModeliPadMiniRetina:
            case PDDeviceModeliPadAir:
            default:
                [captureManager.session setSessionPreset:AVCaptureSessionPresetPhoto];
                break;
        }
    }
}

- (void)viewDidLoad
{
//    [super viewDidLoad];
    deviceInfo = [PDDeviceInfo alloc];
    trackingAngle = 0.;
    
    if (SHOW_DEVICE_ANGLE == 0) {
        [DALabel setHidden:YES];
        [DATextField setHidden:YES];
        [DAzTextField setHidden:YES];
    }
    
    bNearDeviceAnglePresent = false;
    NearPhotoDeviceAngle = 0.;
    
//    UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithTitle:nil
//                                                                   style:UIBarButtonItemStyleBordered
//                                                                  target:self
//                                                                  action:@selector(homeButtonPressedLandscape:)];
//    [homeButton setImage:[UIImage imageNamed:@"homeButton"]];
//
//    UIBarButtonItem *newItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"New session", @"") style:UIBarButtonItemStyleBordered target:self action:@selector(startNewSession:)];
//    self.navItem.leftBarButtonItems = [NSArray arrayWithObjects:homeButton, newItem, self.barNewPhoto, nil];
    
    roundView.layer.cornerRadius = 55.0;
    self.delayView.layer.cornerRadius = 22.0;
    self.farPdMode = [NSNumber numberWithBool:NO];
    
    self.imageIsSnapped = NO;
    
    // Скроем элементы для задержки
    [self.delayView setHidden:YES];
    [self showWarning:NO andArrowsUp:NO];
    
    // Обнулим хранилище для снимка (отключено для уже сделанного снимка)
    
    if (self.parentController.portrainImage != nil) {
        [self showBrightnessControl];
        [self.helperLabel setHidden:YES];
        [self.markersButton setEnabled:YES];
    } else {
        self.parentController.portrainImage = nil;
        self.parentController.imageWithBrightnessPortrait = nil;
        [self.parentController removeXMLDataForFar:NO andForNear:YES];
        [self.helperLabel setHidden:NO];
        [self hideBrightnessControl];
    }

    self.touchView.cameraController = self;
    
    //localization
    self.helperLabel.text = NSLocalizedString(@"Please take iPad in your hands. Press OK when ready", @"");
    self.warningLabel.text = NSLocalizedString(@"Please hold iPad in reading position", @"");
    m_pleaseLookLabel.text = NSLocalizedString(@"Please look at the red circle", @"");
    [self.pdModeSegmentedControl setTitle:NSLocalizedString(@"Near PD", @"") forSegmentAtIndex:0];
    [self.pdModeSegmentedControl setTitle:NSLocalizedString(@"Far PD", @"") forSegmentAtIndex:1];
    self.brightnessLabel.text = NSLocalizedString(@"Brightness", @"");
    [self.photoButton setTitle:NSLocalizedString(@"New photo", @"") forState:UIControlStateNormal];
    [self.markersButton setTitle:NSLocalizedString(NSLocalizedString(@"NearPhotoForm_Next_key", @""), @"") forState:UIControlStateNormal];
}

- (void)captureManagerStillImageCaptured:(AVCamCaptureManager *)captureManager
{
    self.snapImage = [[self captureManager] stillImage];
    UIView *flashView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 1024)];
    [flashView setBackgroundColor:[UIColor whiteColor]];
    [[[self view] window] addSubview:flashView];
    [self configureStaticImageView];
    [UIView animateWithDuration:.4f
                     animations:^{
                         [flashView setAlpha:0.f];
                     }
                     completion:^(BOOL finished){
                         [flashView removeFromSuperview];
                         //[self unlockElements];
                         [[[self captureManager] session] stopRunning];
                     }
     ];

    if (self.snapImage != nil) {
        
        CGFloat k = (self.snapImage.size.width / self.cropArea.size.width);
        CGRect rect;
 
        rect = CGRectMake(self.cropArea.origin.x * (k * 1.05) * -1, self.cropArea.origin.y * (k * 0.90) * -1, self.cameraPreview.frame.size.width * (k * 0.9), self.cameraPreview.frame.size.height * (k * 0.9));

        self.snapImage = [self.snapImage cropByRect:rect];
        
        // Изменим надписи на кнопках
        for (UIButton *button in self.snapButtons) {
            [button setTitle:@"Resnap" forState:UIControlStateNormal];
        }
    }
    
    [self showBrightnessControl];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *destinaton = segue.destinationViewController;
    
    if ([destinaton respondsToSelector:@selector(setSnapImage:)]) {
        [destinaton setValue:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"photo2.jpg" ofType:nil]] forKey:@"snapImage"];
    }
    
    [destinaton setValue:self.farPdMode forKey:@"farPdMode"];
}

#pragma mark - SessionManagerViewController delegate methods

- (void)managerWillDismiss
{
    self.needToDismiss = YES;
    transitionFromSessionManager = YES;
}

- (void)loadSessionDidSelected:(Session *)session
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.parentController loadSessionFromNearMode:session];
}

@end
