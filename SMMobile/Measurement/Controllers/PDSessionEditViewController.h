//
//  PDSessionEditViewController.h
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 07.04.13.
//
//

#import <UIKit/UIKit.h>
#import "PDSessionManagerViewController.h"

@interface PDSessionEditViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet UIImageView *image;
    IBOutlet UITextField *firstName;
    IBOutlet UITextField *lastName;
    
    CGPoint initialOrign;
}

@property (nonatomic, strong) Session * session;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UILabel *firstnameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastnameLabel;

- (id)initWithSession:(Session *)_session;
- (IBAction)doneButtonDidPressed:(id)sender;

@end
