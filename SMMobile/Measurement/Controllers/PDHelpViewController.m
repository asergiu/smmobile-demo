
#import "PDHelpViewController.h"

@interface PDHelpViewController () <UIScrollViewDelegate>
{
    UIScrollView *scrollView;
    MPMoviePlayerController *player;
    UITextView *textView;
    UIImageView *imageView;
    
    PDHelpType currentType;
}

- (void)close:(id)sender;

@end

@implementation PDHelpViewController

+ (UIViewController *)initWithPage:(PDHelpType)helpType
{
    PDHelpViewController *controller = [PDHelpViewController new];
    
    [controller setHelpForType:helpType];
    
    controller.modalPresentationStyle = UIModalPresentationFormSheet;
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    return controller;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 40.f, self.view.frame.size.width,
                                                                self.view.frame.size.height)];
    scrollView.backgroundColor = [UIColor whiteColor];
    scrollView.scrollEnabled = YES;
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(400.f, 5.f, 120.f, 30.f)];
    
    closeButton.backgroundColor = [UIColor clearColor];
    closeButton.titleLabel.font = [UIFont fontWithName:@"Trebuchet MS" size:18.f];
    
    closeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    [closeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [closeButton setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    [closeButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    
    [closeButton setTitle:NSLocalizedString(@"Close", nil) forState:UIControlStateNormal];
    [closeButton setTitle:NSLocalizedString(@"Close", nil) forState:UIControlStateSelected];
    [closeButton setTitle:NSLocalizedString(@"Close", nil) forState:UIControlStateHighlighted];
    
    [closeButton addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:closeButton];
    
    [self loadViews];
}

- (void)setHelpForType:(PDHelpType)type
{
    currentType = type;
}

- (void)loadViews
{
    switch (currentType)
    {
        case PDHelpTypeFarCamera:
            [self setImage:@"Screen_1_pic"];
            [self setText:@"Screen_1"];
            [self setImage:@"Screen_1_pic_1"];
            [scrollView setContentSize:CGSizeMake(540.f, imageView.frame.size.height * 2 + textView.frame.size.height + 300.f)];
            break;
            
        case PDHelpTypeFarMarkers:
            // [self showVideo:@"Screen_2_video" extension:@"mp4"];
            [self setImage:@"Screen_4_markers_far"];
            [self setText:@"Screen_2"];
            [scrollView setContentSize:CGSizeMake(540.f, player.view.frame.size.height + textView.frame.size.height)];
            break;
            
        case PDHelpTypeFarLines:
            //  [self showVideo:@"Screen_3_video" extension:@"mp4"];
            [self setImage:@"1lines"];
            [self setText:@"Screen_3"];
            [scrollView setContentSize:CGSizeMake(540.f, player.view.frame.size.height + textView.frame.size.height)];
            break;
            
        case PDHelpTypeFarResults:
            [self setImage:@"Screen_4_pic"];
            [self setText:@"Screen_4"];
            [scrollView setContentSize:CGSizeMake(540.f, imageView.frame.size.height + textView.frame.size.height)];
            break;
            
        case PDHelpTypeNearCamera:
            [self setImage:@"Screen_5_pic"];
            [self setText:@"Screen_5"];
            [scrollView setContentSize:CGSizeMake(540.f, imageView.frame.size.height + textView.frame.size.height + 500.f)];
            break;
            
        case PDHelpTypeNearMarkers:
            // [self showVideo:@"Screen_6_video" extension:@"mp4"];
            [self setImage:@"Screen_4_markers_near"];
            [self setText:@"Screen_6"];
            [scrollView setContentSize:CGSizeMake(540.f, player.view.frame.size.height + textView.frame.size.height)];
            break;
            
        case PDHelpTypeNearLines:
            //[self showVideo:@"Screen_7_video" extension:@"mp4"];
            [self setImage:@"2lines"];
            [self setText:@"Screen_7"];
            [scrollView setContentSize:CGSizeMake(540.f, player.view.frame.size.height + textView.frame.size.height)];
            break;
            
        case PDHelpTypeNearResults:
            [self setText:@"Screen_8_1"];
            [self setImage:@"Screen_8_pic_1"];
            [self setText:@"Screen_8_2"];
            [self setImage:@"Screen_8_pic_2"];
            [self setText:@"Screen_8_3"];
            [scrollView setContentSize:CGSizeMake(540.f, 2 * imageView.frame.size.height + 3 * textView.frame.size.height + 300.f)];
            break;
            
        case PDHelpTypeNearCameraNew:
            [self setImage:@"Screen_5_pic"];
            [self setText:@"Screen_9"];
            [scrollView setContentSize:CGSizeMake(540.f, imageView.frame.size.height + textView.frame.size.height + 500.f)];
            break;
            
        case PDHelpTypeNearMarkersNew:
            // [self showVideo:@"Screen_6_video" extension:@"mp4"];
            [self setImage:@"Screen_4_markers_near"];
            [self setText:@"Screen_10"];
            [scrollView setContentSize:CGSizeMake(540.f, player.view.frame.size.height + textView.frame.size.height)];
            break;
            
        case PDHelpTypeNearLinesNew:
            //[self showVideo:@"Screen_7_video" extension:@"mp4"];
            [self setImage:@"2lines"];
            [self setText:@"Screen_11"];
            [scrollView setContentSize:CGSizeMake(540.f, player.view.frame.size.height + textView.frame.size.height)];
            break;
            
        case PDHelpTypeNearResultsNew:
            [self setImage:@"Screen_12_pic"];
            [self setText:@"Screen_12"];
            [scrollView setContentSize:CGSizeMake(540.f, imageView.frame.size.height + textView.frame.size.height)];
            break;
            
        case PDHelpTypeSessionsManager:
            [self setImage:@"Screen_15_pic"];
            [self setText:@"Screen_15"];
            [scrollView setContentSize:CGSizeMake(540.f, imageView.frame.size.height + textView.frame.size.height)];
            break;
            
        case PDHelpTypeWrapAngleBefore:
            [self setImage:@"Screen_16_1"];
            [self setText:@"Screen_16_1"];
            [scrollView setContentSize:CGSizeMake(540.f, imageView.frame.size.height +  textView.frame.size.height)];
            break;
            
        case PDHelpTypeWrapAngleAfter:
            [self setImage:@"Screen_16_2"];
            [self setText:@"Screen_16_2"];
            [scrollView setContentSize:CGSizeMake(540.f, imageView.frame.size.height + textView.frame.size.height)];
            break;
            
        case PDHelpTypeNearResultsExpert:
            [self setText:@"Screen_8_1"];
            [self setImage:@"Screen_8_pic_1_expert_mode"];
            [self setText:@"Screen_8_2"];
            [self setImage:@"Screen_8_pic_2_expert_mode"];
            [self setText:@"Screen_8_3"];
            [scrollView setContentSize:CGSizeMake(540.f, 2 * imageView.frame.size.height + 3 * textView.frame.size.height + 300.f)];
            break;
            
        case PDHelpTypeFarResultsExpert:
            [self setImage:@"Screen_4_pic_expert_mode"];
            [self setText:@"Screen_4"];
            [scrollView setContentSize:CGSizeMake(540.f, imageView.frame.size.height + textView.frame.size.height)];
            break;
            
        case PDHelpTypeNearResultsNewExpert:
            [self setImage:@"Screen_12_pic_expert_mode"];
            [self setText:@"Screen_12"];
            [scrollView setContentSize:CGSizeMake(540.f, imageView.frame.size.height + textView.frame.size.height)];
            break;
            
        case PDHelpTypeMainScreen:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showVideo:(NSString *)name extension:(NSString *)extension
{
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:extension];
    
    player = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:path]];
    
    player.shouldAutoplay = NO;

    [player prepareToPlay];
    
    [player.view setFrame:CGRectMake(10.f, 5.f, 520.f, 408.f)];
    [scrollView addSubview:player.view];
}

- (void)setText:(NSString *)text
{
    CGRect textViewSize = [NSLocalizedString(text, nil) boundingRectWithSize:CGSizeMake(500.f, FLT_MAX)
                                                                     options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                                                  attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"Trebuchet MS" size:15.f] }
                                                                     context:nil];
    
    textView = [[UITextView alloc] initWithFrame:CGRectMake(20.f, player.view.frame.size.height
                                                            + player.view.frame.origin.y
                                                            + imageView.frame.size.height
                                                            + imageView.frame.origin.y + 15.f, 500.f, textViewSize.size.height + 50.f)];
    
    textView.editable = NO;
    textView.scrollEnabled = NO;
    textView.backgroundColor = [UIColor clearColor];
    textView.font = [UIFont fontWithName:@"Trebuchet MS" size:15.f];
    textView.text = NSLocalizedString(text, nil);
    
    [scrollView addSubview:textView];
}

- (void)setImage:(NSString *)fileName
{
    BOOL isNarrow = NO;
    BOOL isHigh = NO;
    
    if ([fileName isEqualToString:@"Screen_1_pic"])
        isNarrow = YES;
    else if ([fileName isEqualToString:@"Screen_5_pic"])
        isHigh = YES;
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(isHigh ? (540 - 300) / 2 : !isNarrow ? (540 - 384) / 2 : 10,
                                                              textView.frame.size.height + textView.frame.origin.y + 20.f,
                                                              isNarrow ? 520 : isHigh ? 300 : 384,
                                                              isHigh ? 408.f : 300.f)];
    
    [imageView setImage:[UIImage imageNamed:fileName]];
    
    [scrollView addSubview:imageView];
}

@end
