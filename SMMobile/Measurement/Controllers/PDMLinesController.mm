#import <QuartzCore/QuartzCore.h>

#import "PDMLinesController.h"
#import "UIImageView+Point.h"
#import "UIView+Point.h"
#import "UIImage+Crop.h"
#import "UIView+Effects.h"
#import "PDMDataManager.h"
#import "PDMCameraController.h"
#import "PDMMarkersController.h"
#import "PDConfigManager.h"
#import "PDMLinesView.h"
#import "detection.h"
#import "detection_types.h"
#import "opencvheaders.h"
#import "Utils.h"
#import "PDHelpViewController.h"
#import "UIImage+Extensions.h"

#include "PDMResults.h"

#define HideWrapMarkers
#define MAX_ZOOM_SCALE 2.0
#define GESTURE_HEIGHT_OFFSET 59.
#define GESTURE_WIDTH_OFFSET 42.

static CGFloat markerOffset = 31.0;

CPDMLineAngle lineAngle;
extern BOOL needDetectFlashlightType;
extern CPDMResults Results;

BOOL detectLeftPoint = NO;
BOOL detectRightPoint = NO;

@interface PDMLinesController () <PDMLinesViewDelegate>
{
    CGPoint analytics_leftEye;
    CGPoint analytics_rightEye;
    
    CGPoint analytics_supportPointA;
    CGPoint analytics_supportPointB;
    CGPoint analytics_supportPointC1;
    
    CGPoint analytics_leftboxingTop;
    CGPoint analytics_rightboxingTop;
    CGPoint analytics_leftboxingBottom;
    CGPoint analytics_rightboxingBottom;
    CGPoint analytics_leftboxingBridge;
    CGPoint analytics_rightboxingBridge;
    CGPoint analytics_leftboxingExt;
    CGPoint analytics_rightboxingExt;
    
    CGPoint analytics_leftControl;
    CGPoint analytics_rightControl;
    CGPoint analytics_leftTopControl;
    CGPoint analytics_rightTopControl;
    
    BOOL boxingIsMoved;

    double heightScale;
    double widthScale;
}

- (IBAction)resultsPressed:(id)sender;
- (IBAction)btnHelp_Click:(id)sender;

- (void)savePositions;
- (void)detectEyeControlsPoints;
- (void)placeCornersOnTheirPositions;
- (void)initLineAngle;

@property (nonatomic, strong) NSMutableDictionary *measurement;

@end

@implementation PDMLinesController

@synthesize vdMode;
@synthesize snapImage;
@synthesize farPdMode;
@synthesize secondPhotoMode;
@synthesize measurement;
@synthesize resultButton;

#pragma mark - ViewDidLoad

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    boxingIsMoved = NO;
    
    analytics_leftEye = CGPointMake(-1, -1);
    analytics_rightEye = CGPointMake(-1, -1);
    
    analytics_supportPointA = CGPointMake(-1, -1);
    analytics_supportPointB = CGPointMake(-1, -1);
    analytics_supportPointC1 = CGPointMake(-1, -1);
    
    analytics_leftboxingTop = CGPointMake(-1, -1);
    analytics_rightboxingTop = CGPointMake(-1, -1);
    analytics_leftboxingBottom = CGPointMake(-1, -1);
    analytics_rightboxingBottom = CGPointMake(-1, -1);
    analytics_leftboxingBridge = CGPointMake(-1, -1);
    analytics_rightboxingBridge = CGPointMake(-1, -1);
    analytics_leftboxingExt = CGPointMake(-1, -1);
    analytics_rightboxingExt = CGPointMake(-1, -1);
    
    measurement = [PDMDataManager sharedManager].storage;
    
    self.linesView.controller = self;
    
    [self placeCornersOnTheirPositions];
    
    [self setLeftAngle];
    [self setRightAngle];

    m_lensLabel.text = NSLocalizedString(@"Please set frame sizes", @"");
}

#pragma mark - Button Actions

- (IBAction)goBackPressed
{    
    [self savePositions];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeButtonPressed:(id)sender
{
    needDetectFlashlightType = YES;
    
    [PD_MANAGER setTargetScreen:PDTargetScreenLines];
    
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

// SM Mobile v1 window size
const double width_v1 = 991.;
const double height_v1 = 615.;
// SM Mobile v2 window size
const double width_v2 = 1238.;
const double height_v2 = 768.;

const double X_CENTER = 619;
const double Y_CENTER = 384;

- (double)checkPositionX:(double)x
{
    double xRes = x;
    if (x < markerOffset) {
        xRes = markerOffset;
    }
    if (x > 1024. - markerOffset) {
        xRes = 1024. - markerOffset;
    }
    return xRes;
}
- (double)checkPositionY:(double)y
{
    double yRes = y;
    if (y < markerOffset) {
        yRes = markerOffset;
    }
    if (y > 768. - markerOffset) {
        yRes = 768. - markerOffset;
    }
    return yRes;
}

- (double)windowToDictionaryX:(double)x
{
    double xDic = x * width_v1 / width_v2;
    return xDic;
}
- (double)windowToDictionaryY:(double)y
{
    double xDic = y * height_v1 / height_v2;
    return xDic;
}
- (double)dictionaryToWindowX:(double)x
{
    double xWin = x * width_v2 / width_v1;
    return xWin;
}
- (double)dictionaryToWindowY:(double)y
{
    double xWin = y * height_v2 / height_v1;
    return xWin;
}

- (void)initLineAngle
{
    PDMSupportType typeUsedSupport = (PDMSupportType)[PDM_DATA getCurrentSessionSupportType];

    CGPoint centerSupport = CGPointMake([[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_CENTER_SUPPORT_X : FAR_CENTER_SUPPORT_X) : NEAR_CENTER_SUPPORT_X] floatValue],
                                        [[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_CENTER_SUPPORT_Y : FAR_CENTER_SUPPORT_Y) : NEAR_CENTER_SUPPORT_Y] floatValue]);
    
    CGPoint rightSupport = CGPointMake([[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_SUPPORT_X : FAR_LEFT_SUPPORT_X) : NEAR_LEFT_SUPPORT_X] floatValue],
                                       [[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_SUPPORT_Y : FAR_LEFT_SUPPORT_Y) : NEAR_LEFT_SUPPORT_Y] floatValue]);
    
    CGPoint leftSupport = CGPointMake([[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_SUPPORT_X : FAR_RIGHT_SUPPORT_X) : NEAR_RIGHT_SUPPORT_X] floatValue],
                                      [[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_SUPPORT_Y : FAR_RIGHT_SUPPORT_Y) : NEAR_RIGHT_SUPPORT_Y] floatValue]);

    double DeviceAngle = [[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? FAR_SNAPPED_ANGLE : NEAR_SNAPPED_ANGLE] floatValue];
    
    lineAngle.SetPhotoData(PD_MANAGER.theNewiPad ? niPadNew : niPad, farPdMode.boolValue ? nFarPD : nNearPD, leftSupport.x, leftSupport.y, rightSupport.x, rightSupport.y, centerSupport.x, centerSupport.y, typeUsedSupport, DeviceAngle);
}

- (void)detectScale
{
    heightScale = snapImage.size.height / height_v1;
    widthScale = snapImage.size.width / width_v1;
}

- (void)detectEyeControlsPoints
{
    CGPoint rightSupportPoint = CGPointMake([[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_SUPPORT_X : FAR_LEFT_SUPPORT_X) : NEAR_LEFT_SUPPORT_X] floatValue],
                                            [[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_SUPPORT_Y : FAR_LEFT_SUPPORT_Y) : NEAR_LEFT_SUPPORT_Y] floatValue]);
    CGPoint leftSupportPoint = CGPointMake([[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_SUPPORT_X : FAR_RIGHT_SUPPORT_X) : NEAR_RIGHT_SUPPORT_X] floatValue],
                                           [[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_SUPPORT_Y : FAR_RIGHT_SUPPORT_Y) : NEAR_RIGHT_SUPPORT_Y] floatValue]);
    CGPoint centerSupportPoint = CGPointMake([[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_CENTER_SUPPORT_X : FAR_CENTER_SUPPORT_X) : NEAR_CENTER_SUPPORT_X] floatValue],
                                             [[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_CENTER_SUPPORT_Y : FAR_CENTER_SUPPORT_Y) : NEAR_CENTER_SUPPORT_Y] floatValue]);
    CGPoint rightEye = CGPointMake([[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_EYE_X : FAR_LEFT_EYE_X) : NEAR_LEFT_EYE_X] floatValue],
                                   [[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_EYE_Y : FAR_LEFT_EYE_Y) : NEAR_LEFT_EYE_Y] floatValue]);
    CGPoint leftEye = CGPointMake([[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_EYE_X : FAR_RIGHT_EYE_X) : NEAR_RIGHT_EYE_X] floatValue],
                                  [[[PDMDataManager sharedManager].storage objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_EYE_Y : FAR_RIGHT_EYE_Y) : NEAR_RIGHT_EYE_Y] floatValue]);
    
    double distBetweenPoints = sqrt(pow(leftSupportPoint.x - rightSupportPoint.x, 2.) + pow(leftSupportPoint.y - rightSupportPoint.y, 2.));
    double scaleFactor = distBetweenPoints / 110.;
    double distBenweeEyesCenter = sqrt(pow(leftEye.x - rightEye.x, 2.) + pow(leftEye.y - rightEye.y, 2.)) /  (scaleFactor * 2.);
    double distToLeftEye = distBenweeEyesCenter - 9.;
    double distToRightEye = distBenweeEyesCenter + 9.;
    double scaleLeftAll = distToLeftEye / (distBenweeEyesCenter * 2.);
    double scaleRightAll = distToRightEye / (distBenweeEyesCenter * 2.);
    double leftX = (rightEye.x - leftEye.x) * scaleLeftAll + leftEye.x;
    double rightX = (rightEye.x - leftEye.x) * scaleRightAll + leftEye.x;
    double leftY = (rightEye.y - leftEye.y) * scaleLeftAll + leftEye.y;
    double rightY = (rightEye.y - leftEye.y) * scaleRightAll + leftEye.y;
    double normalDist = 18.;
    double AC = sqrt(pow(distToLeftEye, 2.) + pow(normalDist, 2.));
    double widthDifferences = fabs(leftX - leftEye.x) / scaleFactor;
    double heightDifferences = fabs(leftY - leftEye.y) / scaleFactor;
    double alphaAngel = acos(normalDist / AC);
    double BK = normalDist * sin(alphaAngel) / sin(135. * M_PI/180 - alphaAngel);
    double IK = BK - heightDifferences;
    double AK = sqrt(pow(widthDifferences, 2.) + pow(IK, 2.));
    double EC = IK * AC / AK;
    double AE = sqrt(pow(AC, 2.) - pow(EC, 2.));
    double eyeDiffX = AE - widthDifferences;
    double eyeDiffY = EC + heightDifferences;
    
    CGPoint leftPointControl = CGPointMake(leftX + eyeDiffX * scaleFactor, leftY + eyeDiffY * scaleFactor);
    CGPoint rightPointControl = CGPointMake(rightX + eyeDiffX * scaleFactor, rightY + eyeDiffY * scaleFactor);
    CGPoint leftTopPointControl;
    CGPoint rightTopPointControl;
    
    CGFloat xc, yc;
    
    xc = rightPointControl.x + 300.0;
    yc = rightPointControl.y - 150.0;
    
//    if (xc >= imageView.frame.size.width - topRightEyeControl.frame.size.width / 2 - 10.) {
//        xc = imageView.frame.size.width - topRightEyeControl.frame.size.width / 2 - 10.;
//    }
    
    rightTopPointControl = CGPointMake(xc, yc);
    
    xc = leftPointControl.x - 300.0;
    yc = leftPointControl.y - 150.0;
    
//    if (xc < topLeftEyeControl.frame.size.width / 2 + 10.) {
//        xc = topLeftEyeControl.frame.size.width / 2 + 10.;
//    }
    
    leftTopPointControl = CGPointMake(xc, yc);
    
    // -- Frame Detection --
    
    Detection* detect_cv = new Detection;
    
    cv::Mat testImageIpl = (cv::Mat)[Utils UIImageToMat:snapImage];
    
    NSDate *start = [NSDate date];
    
    try
    {
        detect_cv->loadImage(testImageIpl);
        
        if(!detect_cv->isImageLoaded())
            return;
        
        detect_cv->setEyePoint(EYE_POINT_RIGHT, cv::Point2d(leftEye.x * widthScale, leftEye.y * heightScale));
        detect_cv->setEyePoint(EYE_POINT_LEFT, cv::Point2d(rightEye.x * widthScale, rightEye.y * heightScale));
        detect_cv->setSupportPoint(SUPPORT_POINT_B, cv::Point2d(rightSupportPoint.x * widthScale, rightSupportPoint.y * heightScale));
        detect_cv->setSupportPoint(SUPPORT_POINT_A, cv::Point2d(leftSupportPoint.x * widthScale, leftSupportPoint.y * heightScale));
        detect_cv->setSupportPoint(SUPPORT_POINT_C1, cv::Point2d(centerSupportPoint.x * widthScale, centerSupportPoint.y * heightScale));
        detect_cv->detectBoxing();
        
        NSLog(@"frame detected");
        
        analytics_rightEye = CGPointMake(leftEye.x * widthScale, leftEye.y * heightScale);
        analytics_leftEye = CGPointMake(rightEye.x * widthScale, rightEye.y * heightScale);
        
        analytics_supportPointA = CGPointMake(leftSupportPoint.x * widthScale, leftSupportPoint.y * heightScale);
        analytics_supportPointB = CGPointMake(rightSupportPoint.x * widthScale, rightSupportPoint.y * heightScale);
        analytics_supportPointC1 = CGPointMake(centerSupportPoint.x * widthScale, centerSupportPoint.y * heightScale);
    }
    catch (...)
    {
        NSLog(@"FRAME DETECTION  - Exception");
    }
    
    NSDate *finish = [NSDate date];
    NSTimeInterval executionTime = [finish timeIntervalSinceDate:start];
    NSLog(@"Execution time: %f", executionTime);
    
    if (detect_cv->hasBoxingPoint(EYE_POINT_LEFT, BOXING_POINT_BOTTOM) && detect_cv->hasBoxingPoint(EYE_POINT_LEFT, BOXING_POINT_BRIDGE))
    {
        cv::Point boxingBottom = detect_cv->getBoxingPoint(EYE_POINT_LEFT, BOXING_POINT_BOTTOM);
        cv::Point boxingBridge = detect_cv->getBoxingPoint(EYE_POINT_LEFT, BOXING_POINT_BRIDGE);
        rightPointControl = CGPointMake(boxingBridge.x / widthScale, boxingBottom.y / heightScale);
        
        analytics_leftboxingBottom.x = boxingBottom.x;
        analytics_leftboxingBottom.y = boxingBottom.y;
        analytics_leftboxingBridge.x = boxingBridge.x;
        analytics_leftboxingBridge.y = boxingBridge.y;
        analytics_rightControl = rightPointControl;
    }
    
    if (detect_cv->hasBoxingPoint(EYE_POINT_LEFT, BOXING_POINT_TOP) && detect_cv->hasBoxingPoint(EYE_POINT_LEFT, BOXING_POINT_EXT))
    {
        cv::Point boxingTop = detect_cv->getBoxingPoint(EYE_POINT_LEFT, BOXING_POINT_TOP);
        cv::Point boxingExt = detect_cv->getBoxingPoint(EYE_POINT_LEFT, BOXING_POINT_EXT);
        rightTopPointControl = CGPointMake(boxingExt.x / widthScale, boxingTop.y / heightScale);
        double shift = 0.;
        double lineHeight = boxingExt.y / widthScale - boxingTop.y / heightScale;
        double xPoint = rightTopPointControl.x;
        double yPoint = rightTopPointControl.y + lineHeight;
        double Angle = lineAngle.CalcLineAngle(xPoint, yPoint) * M_PI / 180.;
        shift = tan(Angle) * lineHeight;
        rightTopPointControl.x = rightTopPointControl.x + shift;
        
        analytics_leftboxingTop.x = boxingTop.x;
        analytics_leftboxingTop.y = boxingTop.y;
        analytics_leftboxingExt.x = boxingExt.x;
        analytics_leftboxingExt.y = boxingExt.y;
        analytics_rightTopControl = rightTopPointControl;
    }
    
    if (detect_cv->hasBoxingPoint(EYE_POINT_RIGHT, BOXING_POINT_BOTTOM) && detect_cv->hasBoxingPoint(EYE_POINT_RIGHT, BOXING_POINT_BRIDGE))
    {
        cv::Point boxingBottom = detect_cv->getBoxingPoint(EYE_POINT_RIGHT, BOXING_POINT_BOTTOM);
        cv::Point boxingBridge = detect_cv->getBoxingPoint(EYE_POINT_RIGHT, BOXING_POINT_BRIDGE);
        leftPointControl = CGPointMake(boxingBridge.x / widthScale, boxingBottom.y / heightScale);
        
        analytics_rightboxingBottom.x = boxingBottom.x;
        analytics_rightboxingBottom.y = boxingBottom.y;
        analytics_rightboxingBridge.x = boxingBridge.x;
        analytics_rightboxingBridge.y = boxingBridge.y;
        analytics_leftControl = leftPointControl;
    }
    
    if (detect_cv->hasBoxingPoint(EYE_POINT_RIGHT, BOXING_POINT_TOP) && detect_cv->hasBoxingPoint(EYE_POINT_RIGHT, BOXING_POINT_EXT))
    {
        cv::Point boxingTop = detect_cv->getBoxingPoint(EYE_POINT_RIGHT, BOXING_POINT_TOP);
        cv::Point boxingExt = detect_cv->getBoxingPoint(EYE_POINT_RIGHT, BOXING_POINT_EXT);
        leftTopPointControl = CGPointMake(boxingExt.x / widthScale, boxingTop.y / heightScale);
        double shift = 0.;
        double lineHeight = boxingExt.y / widthScale - boxingTop.y / heightScale;
        double xPoint = leftTopPointControl.x;
        double yPoint = leftTopPointControl.y + lineHeight;
        double Angle = lineAngle.CalcLineAngle(xPoint, yPoint) * M_PI / 180.;
        shift = tan(Angle) * lineHeight;
        leftTopPointControl.x = leftTopPointControl.x + shift;
        
        analytics_rightboxingTop.x = boxingTop.x;
        analytics_rightboxingTop.y = boxingTop.y;
        analytics_rightboxingExt.x = boxingExt.x;
        analytics_rightboxingExt.y = boxingExt.y;
        analytics_leftTopControl = leftTopPointControl;
    }
    
    delete detect_cv;
    
    {
        CGPoint rightBottomPoint = CGPointMake([self dictionaryToWindowX:(rightPointControl.x)] - 107, 768 - [self dictionaryToWindowY:(rightPointControl.y)]);
        rightBottomPoint.x = [self checkPositionX:rightBottomPoint.x];
        rightBottomPoint.y = [self checkPositionY:rightBottomPoint.y];
        [self.linesView setRightCornerBottomPoint:rightBottomPoint];
    }

    if (farPdMode.boolValue)
    {
        CGPoint rightTopPoint = CGPointMake([self dictionaryToWindowX:(rightTopPointControl.x)] - 107, 768 - [self dictionaryToWindowY:(rightTopPointControl.y)]);
        rightTopPoint.x = [self checkPositionX:rightTopPoint.x];
        rightTopPoint.y = [self checkPositionY:rightTopPoint.y];
        [self.linesView setRightCornerTopPoint:rightTopPoint];
    }
    
    {
        CGPoint leftBottomPoint = CGPointMake([self dictionaryToWindowX:(leftPointControl.x)] - 107, 768 - [self dictionaryToWindowY:(leftPointControl.y)]);
        leftBottomPoint.x = [self checkPositionX:leftBottomPoint.x];
        leftBottomPoint.y = [self checkPositionY:leftBottomPoint.y];
        [self.linesView setLeftCornerBottomPoint:leftBottomPoint];
    }
    
    if (farPdMode.boolValue)
    {
        CGPoint leftTopPoint = CGPointMake([self dictionaryToWindowX:(leftTopPointControl.x)] - 107, 768 - [self dictionaryToWindowY:(leftTopPointControl.y)]);
        leftTopPoint.x = [self checkPositionX:leftTopPoint.x];
        leftTopPoint.y = [self checkPositionY:leftTopPoint.y];
        [self.linesView setLeftCornerTopPoint:leftTopPoint];
    }
    // Measurements Analytics
    [self saveDetectedDataForAnalytics];
}

#pragma mark - PDMLinesViewDelegate

- (void)leftTopPointMoved:(BOOL)left
{
    boxingIsMoved = YES;
    
    if (left)
        [self setLeftAngle];
    else
        [self setRightAngle];
}

#pragma mark - Set Angles

- (void)setLeftAngle
{
    CGPoint pointTop = [self.linesView leftCornerMarker:2];
    double xPointTop = [self windowToDictionaryX:(pointTop.x + 107.f)] + 1.0;
    double yPointTop = [self windowToDictionaryY:(768.0 - pointTop.y)];

    CGPoint pointDown = [self.linesView leftCornerMarker:0];
    double yPointDown = [self windowToDictionaryY:(768.0 - pointDown.y)];

    double lineHeight = yPointDown - yPointTop;
    
    double Angle = lineAngle.CalcLineAngle(xPointTop, yPointTop + lineHeight / 2.);
    
    [self.linesView setLeftCornerAngle:Angle];
}

- (void)setRightAngle
{
    CGPoint pointTop = [self.linesView rightCornerMarker:2];
    double xPointTop = [self windowToDictionaryX:(pointTop.x + 107.f)] + 1.0;
    double yPointTop = [self windowToDictionaryY:(768.0 - pointTop.y)];
    
    CGPoint pointDown = [self.linesView rightCornerMarker:0];
    double yPointDown = [self windowToDictionaryY:(768.0 - pointDown.y)];
    
    double lineHeight = yPointDown - yPointTop;
    
    double Angle = -lineAngle.CalcLineAngle(xPointTop, yPointTop + lineHeight / 2.);
    
    [self.linesView setRightCornerAngle:Angle];
}

- (void)placeCornersOnTheirPositions
{
    [self initLineAngle];
    [self detectScale];
    
    if ([measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_RECT_X : FAR_LEFT_RECT_X) : NEAR_LEFT_RECT_X] != nil)
    {
        [self loadLeftRect];
        
        if (farPdMode.boolValue)
        {
            [self loadRightScreenPoint];
        }
    }
    
    if ([measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_RECT_X : FAR_RIGHT_RECT_X) : NEAR_RIGHT_RECT_X] != nil)
    {
        [self loadRightRect];
        
        if (farPdMode.boolValue)
        {
            [self loadLeftScreenPoint];
        }
    }
    else
    {
        [self detectEyeControlsPoints];
    }
}

- (void)loadLeftRect
{
    CGFloat savedX;
    CGFloat savedY;
    
    savedX = [[measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_RECT_X : FAR_LEFT_RECT_X) : NEAR_LEFT_RECT_X] floatValue];
    savedY = [[measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_RECT_Y : FAR_LEFT_RECT_Y) : NEAR_LEFT_RECT_Y] floatValue];
    
    double X = [self dictionaryToWindowX:savedX];
    double Y = [self dictionaryToWindowY:savedY];
    [ self.linesView setRightCornerBottomPoint : CGPointMake( X - 107, 768 - Y ) ];
}

- (void)loadRightRect
{
    CGFloat savedX;
    CGFloat savedY;
    
    savedX = [[measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_RECT_X : FAR_RIGHT_RECT_X) : NEAR_RIGHT_RECT_X] floatValue];
    savedY = [[measurement objectForKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_RECT_Y : FAR_RIGHT_RECT_Y) : NEAR_RIGHT_RECT_Y] floatValue];
    
    double X = [self dictionaryToWindowX:savedX];
    double Y = [self dictionaryToWindowY:savedY];
    [ self.linesView setLeftCornerBottomPoint : CGPointMake( X - 107, 768 - Y ) ];
}

- (void)loadLeftScreenPoint
{
    CGFloat savedX;
    CGFloat savedY;
    
    savedX = [[measurement objectForKey:farPdMode.boolValue ? (secondPhotoMode.boolValue ? VD_LEFT_SCREEN_POINT_X : FAR_LEFT_SCREEN_POINT_X) : NEAR_LEFT_SCREEN_POINT_X] floatValue];
    savedY = [[measurement objectForKey:farPdMode.boolValue ? (secondPhotoMode.boolValue ? VD_LEFT_SCREEN_POINT_Y : FAR_LEFT_SCREEN_POINT_Y) : NEAR_LEFT_SCREEN_POINT_Y] floatValue];
    
    double X = [self dictionaryToWindowX:savedX];
    double Y = [self dictionaryToWindowY:savedY];
    [ self.linesView setLeftCornerTopPoint:CGPointMake( X - 107, 768 - Y ) ];
}

- (void)loadRightScreenPoint
{    
    CGFloat savedX;
    CGFloat savedY;
    
    savedX = [[measurement objectForKey:farPdMode.boolValue ? (secondPhotoMode.boolValue ? VD_RIGHT_SCREEN_POINT_X : FAR_RIGHT_SCREEN_POINT_X) : NEAR_RIGHT_SCREEN_POINT_X] floatValue];
    savedY = [[measurement objectForKey:farPdMode.boolValue ? (secondPhotoMode.boolValue ? VD_RIGHT_SCREEN_POINT_Y : FAR_RIGHT_SCREEN_POINT_Y) : NEAR_RIGHT_SCREEN_POINT_Y] floatValue];
    
    double X = [self dictionaryToWindowX:savedX];
    double Y = [self dictionaryToWindowY:savedY];
    [ self.linesView setRightCornerTopPoint:CGPointMake( X - 107, 768 - Y ) ];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (PDM_DATA.currentSessionType == PDMSessionProgressive && PDM_DATA.currentPhotoStep == PDMPhotoNear)
        farPdMode = @(0);
    else if (PDM_DATA.currentSessionType == PDMSessionProgressive && PDM_DATA.currentPhotoStep == PDMPhotoFar)
        farPdMode = @(1);
    else
        farPdMode = @([PDM_DATA getFarMode]);
    
    if ([farPdMode boolValue] && [PDMDataManager sharedManager].farPDImageRotated)
        snapImage = [PDMDataManager sharedManager].farPDImageRotated;
    else if (![farPdMode boolValue] && [PDMDataManager sharedManager].nearPDImageRotated)
        snapImage = [PDMDataManager sharedManager].nearPDImageRotated;
    
//  use Scale to Fill mode
    CGSize scaleSize = CGSizeMake(snapImage.size.height * width_v2 / height_v2, snapImage.size.height);
    [ self.linesView setBackgroundImage : [snapImage imageByScalingToSize:scaleSize]];
    self.linesView.showTopCorners = farPdMode.boolValue;
    [ self.linesView play ];
    
    [self placeCornersOnTheirPositions];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [ self.linesView stopWithDeallocCam: YES ];
}

#pragma mark - Save Methods

- (void)savePositions
{
    [self saveLeftRect];
    [self saveRightRect];
    
    if (farPdMode.boolValue)
    {
        [self saveTopLeftRect];
        [self saveTopRightRect];
    }
    
    [measurement setValue:[NSNumber numberWithInt:0] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_RECT_WIDTH : FAR_RIGHT_RECT_WIDTH) : NEAR_RIGHT_RECT_WIDTH];
    [measurement setValue:[NSNumber numberWithInt:0] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_RECT_HEIGHT : FAR_RIGHT_RECT_HEIGHT) : NEAR_RIGHT_RECT_HEIGHT];
    
    [measurement setValue:[NSNumber numberWithInt:0] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_RECT_WIDTH : FAR_LEFT_RECT_WIDTH) : NEAR_LEFT_RECT_WIDTH];
    [measurement setValue:[NSNumber numberWithInt:0] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_RECT_HEIGHT : FAR_LEFT_RECT_HEIGHT) : NEAR_LEFT_RECT_HEIGHT];
}

- (void)saveRightRect
{
    CGPoint point = [self.linesView leftCornerMarker:0];
    
    double rightX = [self windowToDictionaryX:(point.x + 107.f)];
    double rightY = [self windowToDictionaryY:(768.0 - point.y)];
    
    [PDM_DATA.analyticsHelper setUserBoxesBottomRightPoint:CGPointMake(rightX * widthScale, rightY * heightScale)];
    
    [measurement setValue:[NSNumber numberWithFloat:rightX] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_RECT_X : FAR_RIGHT_RECT_X) : NEAR_RIGHT_RECT_X];
    [measurement setValue:[NSNumber numberWithFloat:rightY] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_RIGHT_RECT_Y : FAR_RIGHT_RECT_Y) : NEAR_RIGHT_RECT_Y];
    
    currentFrame.rightEyeX = rightX;
    currentFrame.rightEyeY = rightY;
}

- (void)saveLeftRect
{
    CGPoint point = [self.linesView rightCornerMarker:0];
    
    float leftX = [self windowToDictionaryX:(point.x + 107.f)];
    float leftY = [self windowToDictionaryY:(768.0 - point.y)];
    
    [PDM_DATA.analyticsHelper setUserBoxesBottomLeftPoint:CGPointMake(leftX * widthScale, leftY * heightScale)];
    
    [measurement setValue:[NSNumber numberWithFloat:leftX] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_RECT_X : FAR_LEFT_RECT_X) : NEAR_LEFT_RECT_X];
    [measurement setValue:[NSNumber numberWithFloat:leftY] forKey:farPdMode.boolValue ? (vdMode.boolValue ? VD_LEFT_RECT_Y : FAR_LEFT_RECT_Y) : NEAR_LEFT_RECT_Y];
    
    currentFrame.leftEyeX = leftX;
    currentFrame.leftEyeY = leftY;
}

- (void)saveTopLeftRect
{
    CGPoint point = [self.linesView leftCornerMarker:2];
    
    float leftX = [self windowToDictionaryX:(point.x + 107.f)];
    float leftY = [self windowToDictionaryX:(768.0 - point.y)];
    
    [PDM_DATA.analyticsHelper setUserBoxesTopRightPoint:CGPointMake(leftX * widthScale, leftY * heightScale)];
    
    [measurement setValue:[NSNumber numberWithFloat:leftX] forKey:farPdMode.boolValue ? (secondPhotoMode.boolValue ? VD_LEFT_SCREEN_POINT_X : FAR_LEFT_SCREEN_POINT_X) : NEAR_LEFT_SCREEN_POINT_X];
    [measurement setValue:[NSNumber numberWithFloat:leftY] forKey:farPdMode.boolValue ? (secondPhotoMode.boolValue ? VD_LEFT_SCREEN_POINT_Y : FAR_LEFT_SCREEN_POINT_Y) : NEAR_LEFT_SCREEN_POINT_Y];
    
    currentFrame.leftTopEyeX = leftX;
    currentFrame.leftTopEyeY = leftY;
}

- (void)saveTopRightRect
{
    CGPoint point = [self.linesView rightCornerMarker:2];
    
    float rightX = [self windowToDictionaryX:(point.x + 107.f)];
    float rightY = [self windowToDictionaryX:(768.0 - point.y)];

    [PDM_DATA.analyticsHelper setUserBoxesTopLeftPoint:CGPointMake(rightX * widthScale, rightY * heightScale)];
    
    [measurement setValue:[NSNumber numberWithFloat:rightX] forKey:farPdMode.boolValue ? (secondPhotoMode.boolValue ? VD_RIGHT_SCREEN_POINT_X : FAR_RIGHT_SCREEN_POINT_X) : NEAR_RIGHT_SCREEN_POINT_X];
    [measurement setValue:[NSNumber numberWithFloat:rightY] forKey:farPdMode.boolValue ? (secondPhotoMode.boolValue ? VD_RIGHT_SCREEN_POINT_Y : FAR_RIGHT_SCREEN_POINT_Y) : NEAR_RIGHT_SCREEN_POINT_Y];

    currentFrame.rightTopEyeX = rightX;
    currentFrame.rightTopEyeY = rightY;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self savePositions];
    [self processAllDataForAnalytics];
    
    UIViewController *destinaton = segue.destinationViewController;
    
    if (PDM_DATA.currentSessionType == PDMSessionProgressive)
    {
        if ([destinaton respondsToSelector:@selector(setSnapImage:)])
            [destinaton setValue:[PDMDataManager sharedManager].farPDImageRotated forKey:@"snapImage"];
        
        [destinaton setValue:@(1) forKey:@"farPdMode"];
        [destinaton setValue:secondPhotoMode forKey:@"secondPhotoMode"];
    }
    else
    {
        
    if ([destinaton respondsToSelector:@selector(setSnapImage:)])
    {
        if (farPdMode.boolValue)
        {
            [destinaton setValue:[PDMDataManager sharedManager].farPDImageRotated forKey:@"snapImage"];
            NSLog(@"farPDImageRotated = %@", [PDMDataManager sharedManager].farPDImageRotated);
        }
        else
        {
            [destinaton setValue:[PDMDataManager sharedManager].nearPDImageRotated forKey:@"snapImage"];
        }
    }
    
    [destinaton setValue:farPdMode forKey:@"farPdMode"];
    [destinaton setValue:secondPhotoMode forKey:@"secondPhotoMode"];
        
    }
}

#pragma mark - Actions

- (IBAction)resultsPressed:(id)sender
{
    if (PDM_DATA.currentSessionType == PDMSessionProgressive && farPdMode.boolValue)
    {
        [PDMDataManager sharedManager].farPDImageRotated = [[UIImage alloc] initWithCGImage:snapImage.CGImage];
        
        [self savePositions];
        [self processAllDataForAnalytics];
        
        if ([PD_MANAGER isLoadedSession])
        {
            PDMMarkersController *markersNear = [self.storyboard instantiateViewControllerWithIdentifier:@"PDMMarkersController"];
            markersNear.farPdMode = @(0);
            PDM_DATA.currentPhotoStep = PDMPhotoNear;
            [self.navigationController pushViewController:markersNear animated:YES];
        }
        else
        {
            PDMCameraController *cameraController = [self.navigationController.viewControllers objectAtIndex:0];
            [cameraController configurePersonalizedNear];
        }
    }
    else if (PDM_DATA.currentSessionType == PDMSessionProgressive && !farPdMode.boolValue)
    {
        [PDMDataManager sharedManager].nearPDImageRotated = [[UIImage alloc] initWithCGImage:snapImage.CGImage];
        
        [self performSegueWithIdentifier:@"results" sender:self];
    }
    else
    {
        
    if (farPdMode.boolValue) {
        
        if (secondPhotoMode.boolValue) {
            [PDMDataManager sharedManager].vdImageRotated = [[UIImage alloc] initWithCGImage:snapImage.CGImage];
        } else {
            [PDMDataManager sharedManager].farPDImageRotated = [[UIImage alloc] initWithCGImage:snapImage.CGImage];
        }
        
    } else {

        [PDMDataManager sharedManager].nearPDImageRotated = [[UIImage alloc] initWithCGImage:snapImage.CGImage];
    }

    if ([PDMDataManager sharedManager].appModeVD && farPdMode.boolValue && !secondPhotoMode.boolValue) {

        PDMMarkersController *markersController = [self.navigationController.viewControllers objectAtIndex:1];
        markersController.secondPhotoMode = [NSNumber numberWithBool:YES];
        markersController.vdMode = [NSNumber numberWithBool:YES];
        markersController.snapImage = [[UIImage alloc] initWithCGImage:[PDMDataManager sharedManager].vdImage.CGImage];
        [markersController configureDidLoad];
        [self.navigationController popToViewController:markersController animated:YES];
        
    } else {
        
        [self performSegueWithIdentifier:@"results" sender:self];
    }
        
    }
    
    
}

- (IBAction)btnHelp_Click:(id)sender
{
    PDHelpType page;
    
    CPDMDataResults resData = Results.GetPhotoResults(0);
    
    if (farPdMode.boolValue) {
        page = PDHelpTypeFarLines;
    } else {
    
        if (resData.bDataResults) {
            page = PDHelpTypeNearLines;
        } else {
            page = PDHelpTypeNearLinesNew;
        }
    }
    
    UIViewController *controller = [PDHelpViewController initWithPage:page];
    
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - Status Bar Style

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - PD Analytics Data Proccess

- (void)saveDetectedDataForAnalytics
{
    [PDM_DATA.analyticsHelper setBoxesEyePointLeft:analytics_leftEye
                                boxesEyePointRight:analytics_rightEye];
    
    [PDM_DATA.analyticsHelper setBoxesSupportPointA:analytics_supportPointA
                                             pointB:analytics_supportPointB
                                            pointC1:analytics_supportPointC1];
    
    [PDM_DATA.analyticsHelper setBoxesTopLeft:analytics_leftboxingTop
                                boxesTopRight:analytics_rightboxingTop
                              boxesBottomLeft:analytics_leftboxingBottom
                             boxesBottomRight:analytics_rightboxingBottom
                              boxesBridgeLeft:analytics_leftboxingBridge
                             boxesBridgeRight:analytics_rightboxingBridge
                                 boxesExtLeft:analytics_leftboxingExt
                                boxesExtRight:analytics_rightboxingExt];
}

- (void)processAllDataForAnalytics
{
    [PDM_DATA.analyticsHelper boxingIsMoved:boxingIsMoved];
    
    if (boxingIsMoved)
        boxingIsMoved = NO;
    
    [PDM_DATA.analyticsHelper processData];
}

#pragma mark - Rotate Interface Orientation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

@end
