//
//  PDPopoverTableBevelController.h
//  Optovision
//
//  Created by Oleg Bogatenko on 18.06.13.
//
//

#import <UIKit/UIKit.h>

@class PDMResultsController;

@interface PDPopoverTableBevelController : UITableViewController

@property (nonatomic, strong) NSArray *bevelArray;
@property float selectedBevel;
@property int selectedMaterial;

@property (strong, nonatomic) PDMResultsController *parentController;

- (id)initWithParent: (UIViewController *)parent withArray:(NSArray *)array andStyle:(UITableViewStyle)style;

@end
