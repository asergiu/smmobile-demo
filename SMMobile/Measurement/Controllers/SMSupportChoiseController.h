//
//  SMSupportChoiseController.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 3/5/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SMSupportChoiseControllerDelegate <NSObject>

@optional
- (void)cancelSupportChoise;
- (void)suppportTypePressed:(u_int64_t)type;
@end

@interface SMSupportChoiseController : UIViewController

@property (nonatomic, assign) id <SMSupportChoiseControllerDelegate> delegate;

@end
