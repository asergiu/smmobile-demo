//
//  SMStoreAndTurnController.m
//  SMMobile
//
//  Created by Work Inteleks on 5/24/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import "SMStoreAndTurnController.h"
#import "SMStoreAndTurnCell.h"
#import "SMNetworkManager.h"
#import "SMStoreClient.h"
#import "SVProgressHUD.h"

static NSString * const CellIdentifier = @"CustomCell";

@interface SMStoreAndTurnController () <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *infoTableView;
    IBOutlet UIButton *closeBtn;
    
    NSArray *tableDataArray;
}

@end

@implementation SMStoreAndTurnController

@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.layer.cornerRadius = 10.0;
    self.view.frame = CGRectMake(0, 0, 400.f, 400.f);
    
    infoTableView.clipsToBounds = YES;
    infoTableView.layer.cornerRadius = 10.0;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    __weak __typeof(self)weakSelf = self;
    
    [SVProgressHUD setFont:[UIFont fontWithName:@"TrebuchetMS" size:14.f]];
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait. Updating waiting line...", nil)];
    
    tableDataArray = [NSMutableArray new];
    
    [NETWORK_MANAGER getAllQueueClients:^(BOOL status, NSString *reason, id response) {
        
        if (status)
        {
            [weakSelf setupTableDataArray:(NSArray *)response];
            [weakSelf reloadTableViewData];
        }
        else
            NSLog(@"Error status %@", reason);
        
        [SVProgressHUD dismiss];
    }];
}

- (void)setupTableDataArray:(NSArray *)array
{
    tableDataArray = array;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tableDataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self basicCellAtIndexPath:indexPath];
}

- (SMStoreAndTurnCell *)basicCellAtIndexPath:(NSIndexPath *)indexPath
{
    SMStoreAndTurnCell *cell = [infoTableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureBasicCell:(SMStoreAndTurnCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    SMStoreClient *client = [tableDataArray objectAtIndex:indexPath.row];
        
    cell.titleLabel.text = [NSString stringWithFormat:@"%@ %@", client.firstNameClient, client.lastNameClient];
    cell.descriptionLabel.text = [NSString stringWithFormat:@"%@", client.numberClient];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    [NETWORK_MANAGER setCurrentClient:tableDataArray[indexPath.row]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (delegate && [delegate respondsToSelector:@selector(closeTableAlert)])
        [delegate selectCustomer];
}

- (void)reloadTableViewData
{
    [infoTableView reloadData];
}

- (IBAction)closePopover:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (delegate && [delegate respondsToSelector:@selector(closeTableAlert)])
        [delegate closeTableAlert];
}

- (void)dealloc
{
    tableDataArray = nil;
}

@end
