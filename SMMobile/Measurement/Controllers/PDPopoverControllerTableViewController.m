//
//  PDPopoverControllerTableViewController.m
//  PD-Measurement
//
//  Created by Pavel Stoma on 6/26/12.
//

#import "PDPopoverControllerTableViewController.h"
#import "PDMResultsController.h"

@implementation PDMProgressiveType

@synthesize popoverValues;
@synthesize shiftValue;

- (id)init {
    self = [super init];
    if (self) {
        shiftValue = 11;
        NSString* str = NSLocalizedString(@"mm", @"");
        self.popoverValues = [NSArray arrayWithObjects:
                              [NSString stringWithFormat:@"11 %@", str],
                              [NSString stringWithFormat:@"12 %@", str],
                              [NSString stringWithFormat:@"13 %@", str],
                              [NSString stringWithFormat:@"14 %@", str],
                              [NSString stringWithFormat:@"15 %@", str],
                              [NSString stringWithFormat:@"16 %@", str],
                              [NSString stringWithFormat:@"17 %@", str],
                              [NSString stringWithFormat:@"18 %@", str],
                              [NSString stringWithFormat:@"19 %@", str],
                              [NSString stringWithFormat:@"20 %@", str],
                              nil];
    }
    return self;
}

@end

@interface PDPopoverControllerTableViewController ()

@property (weak, nonatomic) PDMResultsController *parentController;

@property (strong, nonatomic) PDMProgressiveType *dataProgressiveType;

@end

@implementation PDPopoverControllerTableViewController

@synthesize parentController;
@synthesize dataProgressiveType;

- (id)initWithParent: (UIViewController *)parent withProgressiveTypeData:(PDMProgressiveType *)pr_type {
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        self.preferredContentSize = CGSizeMake(200, 10 * 44 - 1);
        self.contentSizeForViewInPopover = CGSizeMake(200, 10 * 44 - 1);
        self.parentController = (PDMResultsController *)parent;
        self.dataProgressiveType = (PDMProgressiveType *)pr_type;
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
       self.preferredContentSize = CGSizeMake(200, 10 * 44 - 1);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.scrollEnabled = NO;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = (parentController.defaultProgressiveDesign == (indexPath.row + self.dataProgressiveType.shiftValue)) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    }
    cell.textLabel.text = [self.dataProgressiveType.popoverValues objectAtIndex:indexPath.row];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.parentController.defaultProgressiveDesign = indexPath.row + self.dataProgressiveType.shiftValue;
    [self.parentController configureNearPdDesignFrames];
    [self.parentController.popoverController dismissPopoverAnimated:YES];
    self.parentController.popoverController = nil;
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
