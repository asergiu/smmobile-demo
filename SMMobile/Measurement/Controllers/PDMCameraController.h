#import <UIKit/UIKit.h>

#import "FlashlightDetector.h"
#import "SessionManager.h"

@class PDMCameraView;

@interface PDMCameraController : UIViewController <UIAlertViewDelegate, FlashlightDetectorDelegate>
{
    BOOL targetNearPd;
    BOOL rotateStatus;
    
    FlashlightDetector *flashlightDetector;
} 

@property (nonatomic, strong) IBOutlet UISwitch *liveSwitcher;
@property (nonatomic, strong) IBOutlet UISegmentedControl *pdModeSegmentedControl;

@property (nonatomic, strong) NSNumber *secondPhotoMode;

@property (nonatomic, assign) BOOL targetNearPd;
@property (nonatomic, assign) BOOL returnedFromResults;
@property (nonatomic, assign) BOOL switchToFar;

@property (nonatomic, assign) double device_angle;
@property (nonatomic, assign) double device_angle_z;
@property (nonatomic, assign) double FarPhotoDeviceAngle;

@property (nonatomic, strong) IBOutlet PDMCameraView * cameraView;

@property (nonatomic, strong) IBOutlet UIView *settingCameraView;
@property (nonatomic, strong) IBOutlet UIImageView *frameImage;
@property (nonatomic, strong) IBOutlet UISlider *sliderExposure;
@property (nonatomic, strong) IBOutlet UISlider *visibleSliderExposure;
@property (nonatomic, strong) IBOutlet UISlider *visibleSliderFocus;

@property (nonatomic, strong) IBOutlet UILabel *lableSliderExposure;
@property (nonatomic, strong) IBOutlet UILabel *lableSliderFocus;

- (IBAction)changePdMode:(BOOL)isFar;
- (IBAction)snapPressed:(id)sender;
- (IBAction)markersPressed:(id)sender;
- (IBAction)newPhotoPressed:(id)sender;
- (IBAction)startNewSession:(id)sender;
- (IBAction)changeBrightness:(UISlider *)sender;
- (IBAction)homeButtonPressed:(id)sender;
- (IBAction)btnHelp_Click:(id)sender;

- (void)setFarMode:(NSNumber *)mode;
- (void)playSound;
- (void)configureStaticImageView;
- (void)removeXMLDataForFar:(BOOL)farPD andForNear:(BOOL)nearPD;
- (void)pushSessionManagerView;
- (void)loadSessionFromNearMode:(Session *)session;
- (void)hideBrightnessControl;
- (void)showBrightnessControl;
- (void)loadSession:(Session *)session;
- (void)configurePersonalizedNear;

@end
