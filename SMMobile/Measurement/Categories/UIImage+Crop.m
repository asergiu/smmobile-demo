#import "UIImage+Crop.h"

@implementation UIImage (Crop)

- (UIImage *)crop:(CGSize)cropSize {
    
    CGFloat k = self.size.width / cropSize.width;
    CGFloat newAreaHeight = cropSize.height * k;
    CGFloat newAreaWidth = cropSize.width * k;    
    
    // Create rectangle that represents a cropped image  
    // from the middle of the existing image
    CGRect rect = CGRectMake(0, 0, newAreaWidth, newAreaHeight);
    
     // Create bitmap image from original image data,
    // using rectangle to specify desired crop area
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], rect);
    UIImage *img = [UIImage imageWithCGImage:imageRef]; 
    CGImageRelease(imageRef);
  
//    NSLog(@"%f, %f", newAreaWidth, newAreaHeight);
//    NSLog(@"crop to: %@",NSStringFromCGSize(cropSize));
//    NSLog(@"crop area: %@",NSStringFromCGRect(rect));
//    NSLog(@"old: %@",NSStringFromCGSize(self.size));
    
    return img;
}

- (UIImage *)cropByRect:(CGRect)cropRect {

    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], cropRect);
    UIImage *img = [UIImage imageWithCGImage:imageRef]; 
    CGImageRelease(imageRef);    
    
    return img;
}

- (UIImage *)scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    [self drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();    
    UIGraphicsEndImageContext();
    return newImage;
}

@end
