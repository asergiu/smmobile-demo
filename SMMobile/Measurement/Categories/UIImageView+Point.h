#import <UIKit/UIKit.h>

@interface UIImageView (Point)

- (void)setPoint:(CGPoint)point;

@end
