//
//  NSString+SMConvertJSON.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 2/24/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SMConvertJSON)

- (NSDictionary *)getDictionaryFromJSONString;

@end
