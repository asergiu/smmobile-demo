//
//  UIImage+Border.h
//
//  Created by Levey on 11/10/11.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage(Border)

- (UIImage *)imageWithColoredBorder:(NSUInteger)borderThickness borderColor:(UIColor *)color withShadow:(BOOL)withShadow;
- (UIImage *)imageWithTransparentBorder:(NSUInteger)thickness;

@end
