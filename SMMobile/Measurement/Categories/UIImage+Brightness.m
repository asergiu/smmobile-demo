
#import "UIImage+Brightness.h"

@implementation UIImage (UIImageBrightness)

- (UIImage *) mirroredImage {
    UIImageView *myImageView = [[UIImageView alloc] initWithImage:self];
    myImageView.transform = CGAffineTransformIdentity;
    myImageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    return myImageView.image;
//    CGSize imageSize = self.size;
//    UIGraphicsBeginImageContextWithOptions(imageSize, YES, 1.0);
//    CGContextRef ctx = UIGraphicsGetCurrentContext();
//    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0,imageSize.height);
//    CGContextConcatCTM(ctx, flipVertical);
//    CGContextDrawImage(ctx, CGRectMake(0.0, 0.0, imageSize.width, imageSize.height), self.CGImage);
//    return UIGraphicsGetImageFromCurrentImageContext();
}

- (UIImage *) imageWithBrightness:(CGFloat)brightnessFactor {
    
    if ( brightnessFactor == 0 ) {
        return self;
    }
    
    CGImageRef imgRef = [self CGImage];
    
    size_t width = CGImageGetWidth(imgRef);
    size_t height = CGImageGetHeight(imgRef);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    size_t bitsPerComponent = 8;
    size_t bytesPerPixel = 4;
    size_t bytesPerRow = bytesPerPixel * width;
    size_t totalBytes = bytesPerRow * height;
    
    //Allocate Image space
    uint8_t* rawData = malloc(totalBytes);
    
    //Create Bitmap of same size
    CGContextRef context = CGBitmapContextCreate(rawData, width, height, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    //Draw our image to the context
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imgRef);
    
    //Perform Brightness Manipulation
    for ( int i = 0; i < totalBytes; i += 4 ) {

        uint8_t* red = rawData + i; 
        uint8_t* green = rawData + (i + 1); 
        uint8_t* blue = rawData + (i + 2); 
//        NSLog(@"r = %i g = %i b = %i", *red, *green, *blue);

        
        *red = MIN(255,MAX(0,roundf(*red + (*red * brightnessFactor))));
        *green = MIN(255,MAX(0,roundf(*green + (*green * brightnessFactor))));
        *blue = MIN(255,MAX(0,roundf(*blue + (*blue * brightnessFactor))));

    }
    
    //Create Image
    CGImageRef newImg = CGBitmapContextCreateImage(context);
    
    //Release Created Data Structs
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    free(rawData);
        
    //Create UIImage struct around image
    UIImage* image = [UIImage imageWithCGImage:newImg];
    
    //Release our hold on the image
    CGImageRelease(newImg);
    
    //return new image!
    return image;
    
}

@end
