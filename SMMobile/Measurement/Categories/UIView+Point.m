#import "UIView+Point.h"

@implementation UIView (Point)

- (void)setPoint:(CGPoint)point {
    CGRect localFrame = self.frame;
    
    localFrame.origin = point;
    
    [self setFrame:localFrame];
}

- (void)setSizeHeight:(CGFloat)height {
    CGRect localFrame = self.frame;
    
    localFrame.size.height = height;
    
    [self setFrame:localFrame];
}

- (void)setSizeWidth:(CGFloat)width {
    CGRect localFrame = self.frame;
    
    localFrame.size.width = width;
    
    [self setFrame:localFrame];
}
@end
