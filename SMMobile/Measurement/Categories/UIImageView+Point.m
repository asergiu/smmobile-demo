#import "UIImageView+Point.h"

@implementation UIImageView (Point)

- (void)setPoint:(CGPoint)point {
    CGRect localFrame = self.frame;
    
    localFrame.origin = point;
    
    [self setFrame:localFrame];
}

@end
