#import <UIKit/UIKit.h>

@interface UIImage (Crop)

- (UIImage *)crop:(CGSize)cropSize;
- (UIImage *)cropByRect:(CGRect)cropRect;
- (UIImage *)scaledToSize:(CGSize)newSize;
@end
