//
//  NSArray+Grouped.h
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 21.03.13.
//
//

#import <Foundation/Foundation.h>

@interface NSArray (Grouped)

- (NSArray*) groupUsingBlock:(NSString* (^)(id object)) block;

@end
