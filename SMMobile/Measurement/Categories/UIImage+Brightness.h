
#import <UIKit/UIKit.h>

@interface UIImage (UIImageBrightness)

- (UIImage*) imageWithBrightness:(CGFloat)brightnessFactor;
- (UIImage *) mirroredImage;

@end
