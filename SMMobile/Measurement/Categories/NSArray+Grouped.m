//
//  NSArray+Grouped.m
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 21.03.13.
//
//

#import "NSArray+Grouped.h"

@implementation NSArray (Grouped)

- (NSArray*) groupUsingBlock:(NSString* (^)(id object)) block
{
    NSMutableArray* groupedArray = [NSMutableArray array];
    
    NSMutableArray* keys = [NSMutableArray array];
    
    NSMutableDictionary* dictionary = [NSMutableDictionary new];
    if (dictionary != nil)
    {
        for (id item in self)
        {
            id key = block(item);
            if (key != nil)
            {
                if (![keys containsObject:key])
                    [keys addObject:key];
                
                NSMutableArray* array = [dictionary objectForKey: key];
                if (array == nil) {
                    array = [NSMutableArray arrayWithObject: item];
                    if (array != nil) {
                        [dictionary setObject: array forKey: key];
                    }
                } else {
                    [array addObject: item];
                }
            }
        }
        
        for (NSString * oneKey in keys)
        {
            [groupedArray addObject:[dictionary objectForKey:oneKey]];
        }
    }
    
    return groupedArray;
}

@end
