#import <UIKit/UIKit.h>

@interface UIView (Point)

- (void)setPoint:(CGPoint)point;
- (void)setSizeHeight:(CGFloat)height;
- (void)setSizeWidth:(CGFloat)width;

@end
