//
//  NSDictionary+SMConvertJSON.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 2/24/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import "NSDictionary+SMConvertJSON.h"

@implementation NSDictionary (SMConvertJSON)

- (NSString *)getJSONString
{
    NSError *error = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:0
                                                         error:&error];
    if (error)
        return nil;
    
    return [[NSString alloc] initWithData:jsonData
                                 encoding:NSUTF8StringEncoding];
}

@end
