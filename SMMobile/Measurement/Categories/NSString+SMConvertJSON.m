//
//  NSString+SMConvertJSON.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 2/24/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import "NSString+SMConvertJSON.h"

@implementation NSString (SMConvertJSON)

- (NSDictionary *)getDictionaryFromJSONString
{
    NSError *error = nil;
    
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    
    id dict = [NSJSONSerialization JSONObjectWithData:data
                                              options:0
                                                error:&error];
    if (error)
        return nil;
    
    return (NSDictionary *)dict;
}

@end
