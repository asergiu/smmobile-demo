//
//  UIView+Additions.h
//  PDMeasurement
//
//  Created by Vitalii Bogdan on 15.11.12.
//
//

#import <UIKit/UIKit.h>

@interface UIView (Additions)

/**
 Handy getters and setters
 */
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGFloat left;
@property (nonatomic, assign) CGFloat right;
@property (nonatomic, assign) CGFloat bottom;
@property (nonatomic, assign) CGFloat top;
@property (nonatomic, assign) CGSize size;

@end
