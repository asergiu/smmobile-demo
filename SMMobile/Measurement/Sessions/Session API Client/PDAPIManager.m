//
//  PDAPIManager.m
//
//  Created by Oleg Bogatenko on 3/23/15.
//
//

#import "PDAPIManager.h"
#import "NSString+Hash.h"

@implementation PDAPIManager

const NSString *SECRET = @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9";

+ (instancetype)sharedManager
{
    static dispatch_once_t onceToken = 0;
    static id _sharedManager = nil;
    
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] initWithBaseURL:[NSURL URLWithString:PD_SERVER_URL]];
    });
    
    return _sharedManager;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self)
    {
        self.requestSerializer  = [AFJSONRequestSerializer serializer];
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [self addSecurityTokenForHeaders];
    }
    
    return self;
}

#pragma mark - Security Methods

- (void)addSecurityTokenForHeaders
{
    NSString *securitySrc = [NSString stringWithFormat:@"%@%@", [self getBundle], SECRET];
    
    [self.requestSerializer setValue:[securitySrc SHA256] forHTTPHeaderField:@"Authorization"];
}

- (NSString *)getBundle
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"bundle"];
}

#pragma mark - Requests
#pragma mark - Check Store Code

- (void)requestExistsStoreCode:(NSString *)storeCode
                withCompletion:(void (^)(BOOL status, NSString *reason, NSDictionary *response))completion
{
    _requestCompletion = [completion copy];
    
    NSDictionary *params = @{ @"id" : storeCode, @"bundle" : [self getBundle] };
    
    [self POST:@"/api/store/exists" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self log:responseObject];
        
        if ([responseObject[@"error"] boolValue])
        {
            _requestCompletion(NO, responseObject[@"message"], nil);
        }
        else
        {
            if ([responseObject[@"exists"] boolValue])
                _requestCompletion(YES, nil, responseObject);
            else
                _requestCompletion(NO, nil, nil);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        _requestCompletion(NO, error.localizedDescription, nil);
    }];
}

#pragma mark - Get Sessions For Store ID

- (void)requestSessionsForStoreId:(NSString *)storeId
                   withCompletion:(void (^)(BOOL status, NSString *reason, NSDictionary *response))completion
{
    _requestCompletion = [completion copy];
    
    NSDictionary *params = @{ @"id" : storeId, @"bundle" : [self getBundle] };
    
    [self POST:@"/api/store/sessions" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self log:responseObject];
        
        if ([responseObject[@"error"] boolValue])
        {
            _requestCompletion(NO, responseObject[@"message"], nil);
        }
        else
        {
            if ([responseObject[@"count"] boolValue])
                _requestCompletion(YES, nil, responseObject);
            else
                _requestCompletion(NO, nil, nil);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        _requestCompletion(NO, error.localizedDescription, nil);
    }];
}

#pragma mark - Save Session to Database

- (void)requestSaveSessionDict:(NSDictionary *)sessionDict
                withCompletion:(void (^)(BOOL status, NSString *reason, NSDictionary *response))completion
{
    _requestCompletion = [completion copy];
    
    NSDictionary *params = @{ @"session" : sessionDict, @"bundle" : [self getBundle] };
    
    [self POST:@"/api/session/" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self log:responseObject];
        
        if ([responseObject[@"error"] boolValue])
            _requestCompletion(NO, responseObject[@"message"], nil);
        else
            _requestCompletion(YES, nil, nil);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        _requestCompletion(NO, error.localizedDescription, nil);
    }];
}

#pragma mark - Sync Database

- (void)requestSyncSessions:(NSArray *)sessionsArray
                    storeId:(NSString *)storeId
             withCompletion:(void (^)(BOOL status, NSString *reason, NSDictionary *response))completion
{
    _requestCompletion = [completion copy];
    
    NSDictionary *params = @{ @"sessions" : sessionsArray, @"store_id" : storeId, @"bundle" : [self getBundle] };
    
    [self POST:@"/api/session/sync" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self log:responseObject];
        
        if ([responseObject[@"error"] boolValue])
            _requestCompletion(NO, responseObject[@"message"], nil);
        else
            _requestCompletion(YES, nil, responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        _requestCompletion(NO, error.localizedDescription, nil);
    }];
}

#pragma mark - Delete Session from Database

- (void)requestDeleteSessionWithID:(NSString *)sessionID
                    withCompletion:(void (^)(BOOL status, NSString *reason, NSDictionary *response))completion
{
    _requestCompletion = [completion copy];
    
    NSString *url = [NSString stringWithFormat:@"/api/session/%@/%@", sessionID, [self getBundle]];
    
    [self DELETE:url parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self log:responseObject];
        
        if ([responseObject[@"error"] boolValue])
            _requestCompletion(NO, responseObject[@"message"], nil);
        else
            _requestCompletion(YES, nil, nil);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        _requestCompletion(NO, error.localizedDescription, nil);
    }];
}

#pragma mark - Response Log

- (void)log:(NSDictionary *)dict
{
    NSLog(@"Response: %@", dict);
}

@end
