//
//  PDAPIManager.h
//
//  Created by Oleg Bogatenko on 3/23/15.
//
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

#define PD_API_MANAGER [PDAPIManager sharedManager]

#define ShowNetworkActivityIndicator() [UIApplication sharedApplication].networkActivityIndicatorVisible = YES
#define HideNetworkActivityIndicator() [UIApplication sharedApplication].networkActivityIndicatorVisible = NO

#define PD_SERVER_URL @"https://sessionscloud.opticvideo.com"

@interface PDAPIManager : AFHTTPSessionManager
{
    void (^_requestCompletion)(BOOL status, NSString *reason, NSDictionary *response);
}

+ (instancetype)sharedManager;

- (void)requestExistsStoreCode:(NSString *)storeCode
                withCompletion:(void (^)(BOOL status, NSString *reason, NSDictionary *response))completion;

- (void)requestSessionsForStoreId:(NSString *)storeId
                   withCompletion:(void (^)(BOOL status, NSString *reason, NSDictionary *response))completion;

- (void)requestSaveSessionDict:(NSDictionary *)sessionDict
                withCompletion:(void (^)(BOOL status, NSString *reason, NSDictionary *response))completion;

- (void)requestSyncSessions:(NSArray *)sessionsArray
                    storeId:(NSString *)storeId
             withCompletion:(void (^)(BOOL status, NSString *reason, NSDictionary *response))completion;

- (void)requestDeleteSessionWithID:(NSString *)sessionID
                    withCompletion:(void (^)(BOOL status, NSString *reason, NSDictionary *response))completion;

@end