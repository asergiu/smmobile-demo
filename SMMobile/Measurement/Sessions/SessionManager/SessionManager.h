//
//  SessionManager.h
//  PDdatabase
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DatabaseRequestsData.h"
#import "Session.h"
#import "PDAPIManager.h"
#import "DatabaseRows.h"
#import "AmazonHelper.h"
#import <MessageUI/MessageUI.h>
#import "PDSessionsSyncronizer.h"

#define SESSION_MANAGER [SessionManager sharedSessionManager]

@interface SessionManager : NSObject <MFMailComposeViewControllerDelegate>
{
    void (^_nameCompletion)();
    void (^_storeCompletion)();
}

@property (nonatomic, assign) BOOL isSavedSession;
@property (nonatomic, assign) BOOL userNotSaveSession;
@property (nonatomic, strong) NSMutableDictionary *saveResultDictionary;

@property (nonatomic, strong) Session *currentSession;

@property (nonatomic, strong) PDSessionsSyncronizer *syncronizer;

+ (SessionManager *)sharedSessionManager;

- (void)createSessionFromDataManager;

- (void)addLocalResultsData:(ResultsData *)resultsData forSesionType:(SessionType)type;
- (void)addLocalResultsScreenshot:(UIImage *)screenshot;
- (void)addWrapData:(WrapMarkersData *)wrapData forSesionType:(SessionType)type;
- (void)updateCurrentSessionClientFirstName:(NSString *)firstName lastName:(NSString *)lastName;

- (void)setStoreName:(NSString *)storeName
             address:(NSString *)storeAddress
               phone:(NSString *)storePhone
                mail:(NSString *)storeMail;

- (void)showNameAlertFromViewController:(UIViewController *)viewController
                         fillCompletion:(void (^)(BOOL finished))completion;

- (void)showIncorrectStoreCodeError;

- (void)storeCurrentSessionToDB;
- (void)storeSessionToSQLite:(Session *)session;
- (void)storeSessionToServer:(Session *)session;

- (void)removeSession:(Session *)session
         targetSource:(SourceType)target;

- (void)fillSession:(Session *)session
      forSourceType:(SourceType)source;

- (void)fillDataManagerWithSession:(Session *)session;

- (BOOL)localSessionsContainSession:(Session *)session;

- (NSArray *)sessionsFromTargetSource:(SourceType)target;
- (NSArray *)sessionsGroupedByDaysFromTargetSource:(SourceType)target;

- (NSDictionary *)sortedBySectionsArraysFromTargetSource:(SourceType)target;
- (NSDictionary *)sortedByLetterSectionsFromTargetSource:(SourceType)target;

- (Session *)sessionFromDBAtIndex:(NSInteger)index
                        inSection:(NSInteger)section;

- (UIImage *)photoForPhotoId:(NSString *)photoId
                      source:(SourceType)target;

- (void)markAsServerTypeSession:(Session *)session;

//store data
- (NSString *)currentECP;
- (NSString *)currentStoreCode;

- (void)saveStoreCode:(NSString *)storeId;
- (BOOL)currentStoreIdIsChecked;
- (BOOL)localTargetFromSettings;

- (BOOL)networkAvaliable;

//sync
- (void)updateDataWithError:(NSError **)error forViewController:(UIViewController *)controller;
- (void)startSync;
- (void)stopSync;

//size
- (uint64_t)dataSize;
- (uint64_t)dataLimit;

//Share sessions
- (BOOL)isHaveSessionsForShare;
- (void)saveLocalSessionsForShare:(void(^)(BOOL sucsess))handler;

- (void)importSessionFromShare:(NSDictionary *)dict;

- (void)showShareLoadingView;
- (void)dismissShareLoadingView;

- (void)enableScreenSleeping:(BOOL)isSleeping;

@end
