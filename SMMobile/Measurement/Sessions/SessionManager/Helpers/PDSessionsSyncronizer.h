//
//  PDSessionsSyncronizer.h
//  Nform
//
//  Created by Oleg Bogatenko on 4/8/15.
//
//

#import <Foundation/Foundation.h>

@protocol PDSessionsSyncronizerDelegate <NSObject>

@optional

- (void)syncDidStartedWithState:(NSString *)state;
- (void)loadItemNumber:(int)index outTotalCount:(int)total;
- (void)syncDidStartedWithTotalCount:(int)total;
- (void)syncDidFinished;
@end

@interface PDSessionsSyncronizer : NSObject
{
    int count;
    int current;
    
    NSString *currentState;
}

@property (nonatomic, assign) id <PDSessionsSyncronizerDelegate> delegate;

- (void)setSyncState:(NSString *)state;
- (void)setTotal:(int)total;
- (void)increaseCurrent;
- (void)clear;

@end
