//
//  SQLiteDBHelper.m
//  BankomatDistantion
//
//  Created by админ on 20.07.12.
//

#import "SQLiteDBHelper.h"
#import "DatabaseRequests.h"


@implementation SQLiteDBHelper

@synthesize databasePath;

+ (SQLiteDBHelper *)sharedSQLiteDBHelper
{
    static dispatch_once_t onceToken = 0;
    static id _sharedSQLiteDBHelper  = nil;
    
    dispatch_once(&onceToken, ^{
        _sharedSQLiteDBHelper = [SQLiteDBHelper new];
    });
    
    return _sharedSQLiteDBHelper;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        [self createDBPath];
        
        //create db
        [self initDB];
    }
    return self;
}

- (void)createDBPath
{
    NSString *libsDir;
    NSArray *dirPaths;
    
    // Get the lib directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    
    libsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [libsDir stringByAppendingPathComponent: @"sessionDB2.sql"]];
}

#pragma mark - DB Main Methods

- (void)removeDB
{
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databasePath ])
        [filemgr removeItemAtPath:databasePath error:NULL];
}

- (void) initDB
{
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &mainDB) == SQLITE_OK)
        {
            char *errMsg;
            
            NSString * createRequest = [DatabaseRequests tableCreateRequestAll];
            
            const char *sql_stmt = [createRequest UTF8String];
            
            if (sqlite3_exec(mainDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                NSLog(@"Failed to create table");
            }
            
            sqlite3_close(mainDB);
            
        } else {
            NSLog(@"Failed to open/create database");
        }
    }
    else
    {
        if (![self checkColumnExists:@"SESSION_BUNDLE_ID"])
        {
            if (sqlite3_open([databasePath UTF8String], &mainDB) == SQLITE_OK) {
                
                NSString *updateSQL = [DatabaseRequests updateDatabaseForBundleSupport];
                
                char *errMsg;
                
                const char *sql_stmt = [updateSQL UTF8String];
                
                if (sqlite3_exec(mainDB, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
                {
                    NSLog(@"Database was updated!");
                }
                
                sqlite3_close(mainDB);
            }
        }
        
        if (![self checkColumnExists:@"SESSION_ADDITIONAL"])
        {
            if (sqlite3_open([databasePath UTF8String], &mainDB) == SQLITE_OK) {
                
                NSString *updateSQL = [DatabaseRequests updateDatabaseForAdditionalDataSupport];
                
                char *errMsg;
                
                const char *sql_stmt = [updateSQL UTF8String];
                
                if (sqlite3_exec(mainDB, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
                {
                    NSLog(@"Database was updated!");
                }
                
                sqlite3_close(mainDB);
            }
        }
    }
}

- (BOOL)checkColumnExists:(NSString *)name
{
    BOOL columnExists = NO;
    
    sqlite3_stmt *selectStmt;
    
    if (sqlite3_open([databasePath UTF8String], &mainDB) == SQLITE_OK)
    {
        NSString *request = [NSString stringWithFormat:@"SELECT %@ FROM PD_SESSION", name];
        
        const char *sqlStatement = [request UTF8String];
        
        if (sqlite3_prepare_v2(mainDB, sqlStatement, -1, &selectStmt, NULL) == SQLITE_OK)
        {
            columnExists = YES;
        }
        
        sqlite3_finalize(selectStmt);
        sqlite3_close(mainDB);
    }
    
    return columnExists;
}

- (BOOL)insertRequest:(NSString *)insertRequest
{
    NSLog(@"\n\n insert %@", insertRequest);
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &mainDB) == SQLITE_OK)
    {
        char *error;
        NSString *insertSQL = insertRequest; // request body
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        
        if (sqlite3_exec(mainDB, insert_stmt, NULL, NULL, &error) != SQLITE_OK)
        {
            NSLog(@"Failed insert %s", error);
            sqlite3_close(mainDB);
            return NO;
        }
        
        sqlite3_close(mainDB);
        
    }
    
    return YES;
}

- (NSArray *)selectRequest:(NSString *)selectRequest
{
    @synchronized(self)
    {
        NSMutableArray * dataArray = [NSMutableArray array];
        
        const char *dbpath = [databasePath UTF8String];
        sqlite3_stmt    *statement;
        
        if (sqlite3_open(dbpath, &mainDB) == SQLITE_OK)
        {
            NSString *querySQL = selectRequest;
            
            const char *query_stmt = [querySQL UTF8String];
            
            if (sqlite3_prepare_v2(mainDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
            {
                while (sqlite3_step(statement) == SQLITE_ROW)
                {
                    NSMutableDictionary * rowDict = [NSMutableDictionary dictionary];
                    
                    int i = 0;
                    
                    while (sqlite3_column_text(statement, i) != NULL)
                    {
                        [rowDict setObject:
                         [NSString stringWithUTF8String:(const char *) sqlite3_column_text(statement, i)]
                                    forKey:
                         [NSString stringWithUTF8String:(const char *) sqlite3_column_name(statement, i)]];
                        
                        i++;
                    }
                    
                    [dataArray addObject:rowDict];
                }
                
                sqlite3_finalize(statement);
            }
            
            sqlite3_close(mainDB);
        }
        
        return dataArray;
    }
}

@end
