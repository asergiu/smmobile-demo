//
//  SQLiteDBHelper.h
//  BankomatDistantion
//
//  Created by админ on 20.07.12.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

#define SQLiteDB [SQLiteDBHelper sharedSQLiteDBHelper]


@interface SQLiteDBHelper : NSObject
{
    NSString *databasePath;
    sqlite3 *mainDB;
}

@property (nonatomic, strong) NSString *databasePath;

+ (SQLiteDBHelper *)sharedSQLiteDBHelper;

- (BOOL)insertRequest:(NSString *)insertRequest;
- (NSArray *)selectRequest:(NSString *)selectRequest;

@end
