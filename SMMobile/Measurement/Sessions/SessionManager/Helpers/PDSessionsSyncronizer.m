//
//  PDSessionsSyncronizer.m
//  Nform
//
//  Created by Oleg Bogatenko on 4/8/15.
//
//

#import "PDSessionsSyncronizer.h"

@implementation PDSessionsSyncronizer

@synthesize delegate;

- (void)setSyncState:(NSString *)state
{
    if (state)
    {
        currentState = state;
        
        if (delegate && [delegate respondsToSelector:@selector(syncDidStartedWithState:)])
        {
            [delegate syncDidStartedWithState:currentState];
        }
    }
}

- (void)setTotal:(int)total
{
    count = total;
    current = 0;
    
    if (delegate && [delegate respondsToSelector:@selector(syncDidStartedWithTotalCount:)])
    {
        if (total > 0)
            [delegate syncDidStartedWithTotalCount:count];
    }
}

- (void)increaseCurrent
{
    current++;
    
    if (current != count)
    {
        if (delegate && [delegate respondsToSelector:@selector(loadItemNumber:outTotalCount:)])
        {
            [delegate loadItemNumber:current outTotalCount:count];
        }
    }
}

- (void)clear
{
    count = 0;
    current = 0;
    currentState = nil;
    
    if (delegate && [delegate respondsToSelector:@selector(syncDidFinished)])
    {
        [delegate syncDidFinished];
    }
}

- (void)dealloc
{
    delegate = nil;
}

@end
