
#import <Foundation/Foundation.h>
#import <AWSCore/AWSCore.h>
#import <AWSS3/AWSS3.h>

@interface AmazonHelper : NSObject
{
    AWSS3TransferManagerUploadRequest *uploadRequest;
    
    AWSServiceConfiguration *configuration;
    
    NSData * (^_downloadCompletion)();
}

@property (readwrite, atomic) dispatch_queue_t downloadQueue;

+ (void)sendImageWithData:(NSData *)data
                  andName:(NSString *)name
              synchronize:(BOOL)sync;

+ (UIImage *)loadImageWithName:(NSString *)name;

+ (void)deleteImageWithName:(NSString *)name;

- (void)listImagesInBucketWithCompletion:(void(^)(NSArray *))completion;

@end
