
#import "AmazonHelper.h"
#import "SMAmazonConfig.h"

@implementation AmazonHelper

@synthesize downloadQueue;

+ (void)sendImageWithData:(NSData *)data
                  andName:(NSString *)name
              synchronize:(BOOL)sync
{
    AmazonHelper *tmpHelper = [AmazonHelper new];
    
    [tmpHelper sendImageWithData:data andName:name synchronize:sync];
}

+ (UIImage *)loadImageWithName:(NSString *)name
{
    AmazonHelper *tmpHelper = [AmazonHelper new];
    
    return [tmpHelper loadImageWithName:name];
}

+ (void)deleteImageWithName:(NSString *)name
{
    AmazonHelper *tmpHelper = [AmazonHelper new];
    
    return [tmpHelper deleteImageWithName:name];
}

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        AWSStaticCredentialsProvider *credentialsProvider = [[AWSStaticCredentialsProvider alloc] initWithAccessKey:kS3AccessKey
                                                                                                          secretKey:kS3SecretKey];
        
        configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionEUWest1
                                                    credentialsProvider:credentialsProvider];
        
        [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
        
        downloadQueue = dispatch_queue_create("downloadQueue", NULL);
    }
    
    return self;
}

- (NSString *)pictureBucket
{
    return kS3Bukcet;
}

- (NSURL *)tempImageWithData:(NSData *)data name:(NSString *)name
{
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:name];
    
    [data writeToFile:path atomically:YES];
    
    return [[NSURL alloc] initFileURLWithPath:path];
}

- (void)listImagesInBucketWithCompletion:(void(^)(NSArray *))completion
{
    AWSS3 *s3 = [AWSS3 defaultS3];
    
    AWSS3ListObjectsRequest *listRequest = [AWSS3ListObjectsRequest new];
    
    listRequest.bucket = [self pictureBucket];
    
    [[s3 listObjects:listRequest] continueWithBlock:^id(AWSTask *task) {
        
        if (task.error)
        {
            NSLog(@"%@", task.error.localizedDescription);
            
            completion(nil);
        }
        else
        {
            AWSS3ListObjectsOutput *result = task.result;
            
            completion(result.contents);
        }
        return nil;
    }];
}

- (void)sendImageWithData:(NSData *)data
                  andName:(NSString *)name
              synchronize:(BOOL)sync
{
    NSURL *url = [self tempImageWithData:data name:name];
    
    uploadRequest = [AWSS3TransferManagerUploadRequest new];
    
    uploadRequest.bucket = [self pictureBucket];
    
    uploadRequest.ACL = AWSS3ObjectCannedACLPrivate;
    
    uploadRequest.key = name;
    
    uploadRequest.contentType = @"image/jpeg";
    
    uploadRequest.body = url;
    
    uploadRequest.uploadProgress =^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
        });
    };
    
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    dispatch_group_t group = dispatch_group_create();
    
    if (sync)
    {
        dispatch_group_enter(group);
    }
    
    [[transferManager upload:uploadRequest] continueWithBlock:^id(AWSTask *task) {
        
        if (task.error)
         {
             NSLog(@"Error upload: %@", task.error);
             
             if (sync)
                 dispatch_group_leave(group);
         }
         else
         {
             NSLog(@"Uploaded image: %@", name);
             
             if (sync)
                 dispatch_group_leave(group);
         }
         
         return nil;
     }];
    
    if (sync)
        dispatch_group_wait(group,  DISPATCH_TIME_FOREVER);
}

- (UIImage *)loadImageWithName:(NSString *)name
{
    __block NSURL *downloadedURL = nil;
    
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    AWSS3TransferManagerDownloadRequest *downloadRequest = [AWSS3TransferManagerDownloadRequest new];
    
    downloadRequest.bucket = [self pictureBucket];
    
    downloadRequest.key = name;
    
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    
    [[transferManager download:downloadRequest] continueWithBlock:^id(AWSTask *task) {
        
        if (task.error)
        {
            NSLog(@"Error: %@", task.error);
            
            dispatch_group_leave(group);
        }
        else
        {
            NSLog(@"loaded image %@", name);
            
            downloadedURL = [task.result body];
            
            dispatch_group_leave(group);
        }
        
        return nil;
    }];
    
    dispatch_group_wait(group,  DISPATCH_TIME_FOREVER);
    
    return [UIImage imageWithData:[NSData dataWithContentsOfURL:downloadedURL]];
}

- (void)deleteImageWithName:(NSString *)name
{
    if (!name)
        return;
    
    AWSS3 *s3 = [AWSS3 defaultS3];
    
    AWSS3DeleteObjectRequest *deleteRequest = [AWSS3DeleteObjectRequest new];
    
    deleteRequest.bucket = [self pictureBucket];
    
    deleteRequest.key = name;
    
    [[s3 deleteObject:deleteRequest] continueWithBlock:^id(AWSTask *task) {
                                                                 
                       if (task.error)
                       {
                           NSLog(@"Error: %@", task.error);
                       }
                       else
                       {
                           NSLog(@"Deleted %@", name);
                       }
                                                                 
                        return nil;
                    }];
}

@end
