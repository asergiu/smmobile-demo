//
//  FileHelper.m

#import "FileHelper.h"


@implementation FileHelper

+ (FileHelper *)defaultHelper
{
    static dispatch_once_t onceToken = 0;
    static id _defaultFileHelper = nil;
    
    dispatch_once(&onceToken, ^{
        _defaultFileHelper = [FileHelper new];
    });
    
    return _defaultFileHelper;
}

+ (NSString *)fileNameFromURL:(NSString *)url
{
	return [NSString stringWithString:[url stringByReplacingOccurrencesOfString:@"/" withString:@"_"]];
}

+ (NSString *)libraryDirectory
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	
	if ([paths count] > 0)
		return [paths objectAtIndex:0];
	
	NSLog(@"documentsDirectory %@", @"Directory doesn't exist.");
	
	return nil;
}

+ (NSString *)getDirectoryWithName:(NSString *)directoryName
{
	NSString* documentsDirectory = [FileHelper libraryDirectory];
	
	if (!documentsDirectory)
		return nil;
	
	NSString* pathToCacheDirectory = [documentsDirectory stringByAppendingPathComponent:directoryName];
	
	NSFileManager* fileManager = [NSFileManager defaultManager];
	
	BOOL isDirectory;
	
	if (![fileManager fileExistsAtPath:pathToCacheDirectory isDirectory:&isDirectory] || !isDirectory)
	{
		NSError* error = nil;
		
		if (![fileManager createDirectoryAtPath:pathToCacheDirectory withIntermediateDirectories:YES attributes:nil error:&error])
		{
			NSString* errorMessage = [NSString stringWithFormat:@"Can't create a directory at path: %@.\nError: %@.", 
									  pathToCacheDirectory, [error description]];
			NSLog(@"getDirectoryWithName %@", errorMessage);
			return nil;
		}
	}
	
	return pathToCacheDirectory;
}

+ (NSString *)imagesDirectory
{
	return [self getDirectoryWithName:kImagesDirectoryName];
}

+ (BOOL)storeFileToPath:(NSString *)pathToFile fileContent:(NSData *)fileContent
{
	return [[NSFileManager defaultManager] createFileAtPath:pathToFile contents:fileContent attributes:nil];
}

+ (void)removeFileAtPath:(NSString *)pathToFile
{
	NSError* error = nil;

	if (![[NSFileManager defaultManager] fileExistsAtPath:pathToFile])
	{
		NSString* errorMessage = [NSString stringWithFormat:@"File doesn't exist at path: %@", pathToFile];
		NSLog(@"%@",errorMessage);
		return;
	}
	
	if (![[NSFileManager defaultManager] removeItemAtPath:pathToFile error:&error])
	{
		NSLog(@"removeFileAtPath: %@", [error description]);
	}
}

+ (void)removeFileWithName:(NSString *)fileName fromDirectory:(NSString *)directory
{
    if (!directory || !fileName)
		return;
	
	[self removeFileAtPath:[directory stringByAppendingPathComponent:fileName]];
}

+ (NSData *)readFileWithName:(NSString *)fileName fromDirectory:(NSString *)directory
{
	if (!directory || !fileName)
		return nil;
	
	return [NSData dataWithContentsOfFile:[directory stringByAppendingPathComponent:fileName]];
}

+ (BOOL)fileExistAtPath:(NSString *)pathToFile
{
	return [[NSFileManager defaultManager] fileExistsAtPath:pathToFile];
}

+ (BOOL)fileExistWithName:(NSString *)fileName atDirectory:(NSString *)directory
{
	return [self fileExistAtPath:[directory stringByAppendingPathComponent:fileName]];
}

@end
