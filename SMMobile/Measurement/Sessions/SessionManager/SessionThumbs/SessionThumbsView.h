//
//  SessionThumbsView.h
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 16.03.13.
//
//

#import <UIKit/UIKit.h>
#import "SessionThumbnail.h"

@class SessionThumbsView;

@protocol SessionThumbsViewDataSource <NSObject>

@required
- (NSInteger)thumbsViewNumberOfThumbs:(SessionThumbsView *)thumbsView;
- (SessionThumbnail *)thumbsView:(SessionThumbsView *)thumbsView thumbForIndex:(NSInteger)index;
@end

@protocol SessionThumbsViewDelegate <NSObject>

@required
- (void)thumbsView:(SessionThumbsView *)thumbsView didSelectThumb:(SessionThumbnail *)thumb atIndex:(NSInteger)index;
@end

@interface SessionThumbsView : UIScrollView
{
    NSMutableSet *_reusableThumbViews;
    
    int _firstVisibleIndex;
    int _lastVisibleIndex;
    int _lastItemsPerRow;
}

@property (nonatomic, weak) id <SessionThumbsViewDataSource> dataSource;
@property (nonatomic, weak) id <SessionThumbsViewDelegate> delegate_;

- (SessionThumbnail *)dequeueReusableSessionThumbView;
- (void)reloadData;

@end
