//
//  SessionThumbsView.m
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 16.03.13.
//
//

#import "SessionThumbsView.h"

#define kThumbSize CGSizeMake(230, 230)
#define kThumbSpacer 10.0f

@implementation SessionThumbsView

@synthesize dataSource;
@synthesize delegate_;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _reusableThumbViews = [[NSMutableSet alloc] init];
    }
    return self;
}

- (void)reloadData
{
    [self queueReusableSessionThumbView];
    [self setNeedsLayout];
}

- (SessionThumbnail *)dequeueReusableSessionThumbView
{
    SessionThumbnail *thumbView = [_reusableThumbViews anyObject];
    if (thumbView != nil)
    {
        [_reusableThumbViews removeObject:thumbView];
    }
    return thumbView;
}

- (void)queueReusableSessionThumbView
{
    for (UIView *view in [self subviews])
    {
        if ([view isKindOfClass:[SessionThumbnail class]])
        {
            [_reusableThumbViews addObject:view];
            [view removeFromSuperview];
        }
    }
    
    _firstVisibleIndex = NSIntegerMax;
    _lastVisibleIndex  = NSIntegerMin;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect visibleBounds = [self bounds];
    
    int visibleWidth = visibleBounds.size.width;
    int visibleHeight = visibleBounds.size.height;
    
    int itemsPerRow = floor(visibleWidth / kThumbSize.width);
    
    if (itemsPerRow != _lastItemsPerRow)
    {
        [self queueReusableSessionThumbView];
    }
    _lastItemsPerRow = itemsPerRow;
    
    int minimumSpace = 5;
    if (visibleWidth - itemsPerRow * kThumbSize.width < minimumSpace)
    {
        itemsPerRow--;
    }
    
    if (itemsPerRow < 1) itemsPerRow = 1;  
    
    int spaceWidth = round((visibleWidth - kThumbSize.width * itemsPerRow) / (itemsPerRow + 1));
    int spaceHeight = spaceWidth;
    
    int thumbCount = [dataSource thumbsViewNumberOfThumbs:self];
    
    int rowCount = ceil(thumbCount / (float)itemsPerRow);
    int rowHeight = kThumbSize.height + spaceHeight;
    
    CGSize contentSize = CGSizeMake(visibleWidth, (rowHeight * rowCount + spaceHeight) + kThumbSpacer);
    [self setContentSize:contentSize];
    
    NSInteger rowsPerView = visibleHeight / rowHeight;
    NSInteger topRow = MAX(0, floorf(visibleBounds.origin.y / rowHeight));
    NSInteger bottomRow = topRow + rowsPerView;
    
    CGRect extendedVisibleBounds = CGRectMake(visibleBounds.origin.x, MAX(0, visibleBounds.origin.y), visibleBounds.size.width, visibleBounds.size.height + rowHeight);
    
    for (UIView *view in [self subviews])
    {
        if ([view isKindOfClass:[SessionThumbnail class]])
        {
            CGRect viewFrame = [view frame];
            
            if (! CGRectIntersectsRect(viewFrame, extendedVisibleBounds))
            {
                [_reusableThumbViews addObject:view];
                [view removeFromSuperview];
            }
        }
    }
    
    NSInteger startAtIndex = MAX(0, topRow * itemsPerRow);
    NSInteger stopAtIndex = MIN(thumbCount, ((bottomRow + 1) * itemsPerRow) + itemsPerRow);
    
    int x = spaceWidth;
    int y = spaceHeight + (topRow * rowHeight) + kThumbSpacer;
    
    for (int index = startAtIndex; index < stopAtIndex; index++)
    {
        BOOL isThumbViewMissing = !(index >= _firstVisibleIndex && index < _lastVisibleIndex);
        
        if (isThumbViewMissing)
        {
            SessionThumbnail *thumbView = [dataSource thumbsView:self thumbForIndex:index];
            
            CGRect newFrame = CGRectMake(x, y, kThumbSize.width, kThumbSize.height);
            [thumbView setFrame:newFrame];
            
            [thumbView setTag:index];
            [thumbView addTarget:self action:@selector(didSelectThumb:) forControlEvents:UIControlEventTouchUpInside];
            
            [self addSubview:thumbView];
            [self sendSubviewToBack:thumbView];
        }
        
            if ( (index+1) % itemsPerRow == 0)
            {
                x = spaceWidth;
                y += kThumbSize.height + spaceHeight;
            }
            else
            {
                x += kThumbSize.width + spaceWidth;
            }
    }
    
    _firstVisibleIndex = startAtIndex;
    _lastVisibleIndex  = stopAtIndex;
}

- (void)didSelectThumb:(SessionThumbnail *)sender
{
    if (delegate_ && [delegate_ respondsToSelector:@selector(thumbsView:didSelectThumb:atIndex:)])
        [delegate_ thumbsView:self didSelectThumb:sender atIndex:sender.tag];
}

@end
