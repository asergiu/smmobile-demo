//
//  SMStoreClient.h
//  SMMobile
//
//  Created by Work Inteleks on 5/24/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMStoreClient : NSObject

@property (nullable, nonatomic, strong) NSString *dataBaseID;
@property (nullable, nonatomic, strong) NSString *clientID;
@property (nullable, nonatomic, strong) NSString *numberClient;
@property (nullable, nonatomic, strong) NSString *firstNameClient;
@property (nullable, nonatomic, strong) NSString *lastNameClient;

+ ( SMStoreClient * _Nonnull )clientFromDict:(NSDictionary * _Nonnull)dict;

+ (NSString *_Nullable)generateClientUUID;

- (void)setClientQueueNumber:(NSNumber * _Nonnull)number;

- (void)setProcessedClient;
- (BOOL)isProcessedClient;

- (void)setClientID:(NSString *_Nullable)cID;

- (void)setQueueClient;
- (BOOL)isInQueueClient;

- (BOOL)isClientFromServer;

@end
