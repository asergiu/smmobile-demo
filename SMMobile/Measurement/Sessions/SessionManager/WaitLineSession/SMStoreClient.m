//
//  SMStoreClient.m
//  SMMobile
//
//  Created by Work Inteleks on 5/24/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import "SMStoreClient.h"

@interface SMStoreClient ()
{
    BOOL processed;
    BOOL inQueue;
}

@end

@implementation SMStoreClient

@synthesize clientID;
@synthesize numberClient;
@synthesize firstNameClient;
@synthesize lastNameClient;
@synthesize dataBaseID;

+ (SMStoreClient *)clientFromDict:(NSDictionary *)dict
{
    SMStoreClient *client = [SMStoreClient new];
    
    client.dataBaseID = dict[@"_id"];
    client.clientID = dict[@"customer"][@"id"];
    client.firstNameClient = dict[@"customer"][@"name"];
    client.lastNameClient = dict[@"customer"][@"lastName"];
    client.numberClient = dict[@"number"];
    
    client->inQueue = YES;
    
    return client;
}

+ (NSString * _Nullable)generateClientUUID
{
    return [[NSUUID UUID] UUIDString];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        processed = NO;
        inQueue = NO;
    }
    return self;
}

- (void)setClientQueueNumber:(NSNumber *)number
{
    numberClient = [NSString stringWithFormat:@"%@", number];
}

- (void)setClientID:(NSString *_Nullable)cID
{
    clientID = cID;
}

- (void)setQueueClient
{
    inQueue = YES;
}

- (void)setProcessedClient
{
    processed = YES;
}

- (BOOL)isProcessedClient
{
    return processed;
}

- (BOOL)isInQueueClient
{
    return inQueue;
}

- (BOOL)isClientFromServer
{
    return (dataBaseID.length > 0 && clientID.length > 0 && numberClient.length > 0);
}

@end
