//
//  SessionThumbnail.h
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 16.03.13.
//
//

#import <UIKit/UIKit.h>

@interface SessionThumbnail : UIControl
{
    IBOutlet UIImageView *thumbImageView;
}

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *typeLabel;
@property (strong, nonatomic) IBOutlet UIImage *image;

@end
