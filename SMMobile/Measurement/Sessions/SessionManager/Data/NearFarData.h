//
//  NearFarData.h
//  SeikoMultiTool
//
//  Created by Владимир Малашенков on 13.08.13.
//
//

#import <Foundation/Foundation.h>
#import "DatabaseRequestsData.h"

@interface NearFarData : NSObject

@property (nonatomic, assign) SessionType dataType;

- (BOOL)isFarMode;

@end
