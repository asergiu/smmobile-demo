//
//  ProgressiveData.h
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 07.04.13.
//
//

#import <Foundation/Foundation.h>

@interface ProgressiveData : NSObject

@property (nonatomic, strong) NSString *progressiveID;
@property (nonatomic, strong) NSString *corridor;
@property (nonatomic, strong) NSString *name;

@end
