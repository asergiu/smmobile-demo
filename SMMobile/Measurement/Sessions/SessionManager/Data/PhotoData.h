//
//  PhotoData.h
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 18.03.13.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DatabaseRequestsData.h"
#import "NearFarData.h"

@interface PhotoData : NearFarData

@property (nonatomic, strong) NSDate *lastUpdateDate;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, assign) PhotoType type;
@property (nonatomic, strong) NSString *photoID;
@property (nonatomic, strong) NSString *filename;

- (id)initWithImage:(UIImage *)photoImage;
- (void)updateImage:(UIImage *)photoImage;

@end
