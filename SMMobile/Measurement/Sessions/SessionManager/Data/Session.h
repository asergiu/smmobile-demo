//
//  Session.h
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 13.03.13.
//
//

#import <UIKit/UIKit.h>

#import <Foundation/Foundation.h>
#import "FrameMarkersData.h"
#import "MarkersData.h"
#import "WrapMarkersData.h"
#import "DatabaseRequestsData.h"
#import "PhotoData.h"
#import "ResultsData.h"
#import "StoreData.h"
#import "ProgressiveData.h"
#import "PDMDataManager.h"

#define kDBTypeLocale @"0"
#define kDBTypeServer @"1"

@interface Session : NSObject
{
    BOOL filled;
}

@property (nonatomic, assign) SessionType type;

@property (nonatomic, assign) BOOL farMode;

@property (nonatomic, assign) BOOL loadedSession;
@property (nonatomic, assign) BOOL allreadyOnServer;
@property (nonatomic, readonly) BOOL sessionFromCurrentStore;
@property (nonatomic, assign) SourceType source;

@property (nonatomic, strong) NSString * sessionId;

@property (nonatomic, strong) NSString * clientFirstName;
@property (nonatomic, strong) NSString * clientLastName;

@property (nonatomic, strong) NSString * macAddress;

@property (nonatomic, strong) NSDate * date;
@property (nonatomic, strong) NSDate * lastUpdateDate;

@property (nonatomic, readonly) NSString * lastUpdateDateString;
@property (nonatomic, readonly) NSString * dateString;

@property (nonatomic, strong) NSString * humanReadableData;
@property (nonatomic, strong) NSString * humanReadableTime;

@property (nonatomic, strong) MarkersData * nearMarkers;
@property (nonatomic, strong) MarkersData * farMarkers;

@property (nonatomic) MarkersData * currentSessionTypeMarkers;

@property (nonatomic, strong) FrameMarkersData * nearFrameMarkers;
@property (nonatomic, strong) FrameMarkersData * farFrameMarkers;

@property (nonatomic) FrameMarkersData * currentSessionTypeFrameMarkers;

@property (nonatomic, strong) WrapMarkersData * nearWrapMarkers;
@property (nonatomic, strong) WrapMarkersData * farWrapMarkers;

@property (nonatomic, strong) ResultsData * resultsDataFar;
@property (nonatomic, strong) ResultsData * resultsDataNear;

@property (nonatomic, strong) ResultsData * positionOfWearResultsData;

@property (nonatomic, strong) PhotoData * nearPhoto;
@property (nonatomic, strong) PhotoData * farPhoto;

@property (nonatomic, strong) PhotoData * screenShot;

@property (nonatomic, strong) NSString * farDataId;
@property (nonatomic, strong) NSString * nearDataId;

@property (nonatomic, strong) NSString * resultsId;

@property (nonatomic, strong) StoreData * storeData;
@property (nonatomic, strong) ProgressiveData * progressiveData;

@property (nonatomic, assign) double snappedAngleFar;
@property (nonatomic, assign) double snappedAngleNear;

+ (Session *)createFromSessionDict:(NSDictionary *)sessionDict;

+ (Session *)createSessionFromServerDict:(NSDictionary *)dictionary;
+ (NSDictionary *)createDictFromSession:(Session *)session;

- (void)updateLastUpdateDate;
- (void)updateHumanReadableData;

- (void)setNearPhotoImage:(UIImage *)nearImage;
- (void)setFarPhotoImage:(UIImage *)farImage;
- (void)setScreenShotImage:(UIImage *)screenImage;

- (void)fillAllData;
- (void)storeSession;
- (void)deleteSession;

- (void)deleteSessionFromServer;

- (void)updateCurrentStoreIfNull;
- (BOOL)storeIsNull;

- (void)setAdditionalDataWith:(NSDictionary *)dict;
- (NSString *)getAdditionalDataAsJSONString;
- (id)getAdditionalDataForKey:(NSString *)key;

- (WrapMarkersData *)wrapMarkersForType:(SessionType)type;

- (void)loadToDataManager:(PDMDataManager *)dataManager;

- (void)fillAngleValuesFromDataManager:(PDMDataManager *)dataManager;

@end
