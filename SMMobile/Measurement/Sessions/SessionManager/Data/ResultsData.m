//
//  ResultsData.m
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 18.03.13.
//
//

#import "ResultsData.h"

@implementation ResultsData

@synthesize dataId;
@synthesize pdLeft, pdRight;
@synthesize heightLeft, heightRight;
@synthesize frameBridge, frameHight, frameWidth;
@synthesize photoDistance, pantoAngle;
@synthesize readindDistance, wertexDistance, wrapAngle;

@end
