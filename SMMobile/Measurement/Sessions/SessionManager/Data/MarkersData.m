//
//  m
//  PDdatabase
//
//  Created by Владимир Малашенков on 07.03.13.
//

#import "MarkersData.h"
#import "PDMDataManager.h"

@implementation MarkersData

@synthesize xCenterSupport, yCenterSupport;
@synthesize xLeftEye, xRightEye, yLeftEye, yRightEye;
@synthesize xLeftSupport, xRightSupport, yLeftSupport, yRightSupport;

+ (MarkersData *)markersDataFromMeasurementDictonary:(NSDictionary *)measurementDictonary
                                            dataType:(SessionType)dataType
{
    MarkersData *markersData = [MarkersData new];
    
    [markersData fillDataFromMeasurementDictonary:measurementDictonary dataType:dataType];
    
    return markersData;
}


- (void)fillDataFromMeasurementDictonary:(NSDictionary *)measurementDictonary
                                dataType:(SessionType)dataType
{
    BOOL isFarMode = (dataType == SessionTypeFarPD);
    BOOL isVdMode = 0;
    
    xLeftSupport = [[measurementDictonary objectForKey: isFarMode ? (isVdMode ? VD_LEFT_SUPPORT_X : FAR_LEFT_SUPPORT_X) : NEAR_LEFT_SUPPORT_X] doubleValue];
    yLeftSupport = [[measurementDictonary objectForKey:  isFarMode ? (isVdMode ? VD_LEFT_SUPPORT_Y : FAR_LEFT_SUPPORT_Y) : NEAR_LEFT_SUPPORT_Y] doubleValue];
    xCenterSupport = [[measurementDictonary objectForKey:  isFarMode ? (isVdMode ? VD_CENTER_SUPPORT_X : FAR_CENTER_SUPPORT_X) : NEAR_CENTER_SUPPORT_X] doubleValue];
    yCenterSupport = [[measurementDictonary objectForKey:  isFarMode ? (isVdMode ? VD_CENTER_SUPPORT_Y : FAR_CENTER_SUPPORT_Y) : NEAR_CENTER_SUPPORT_Y] doubleValue];
    xRightSupport = [[measurementDictonary objectForKey:  isFarMode ? (isVdMode ? VD_RIGHT_SUPPORT_X : FAR_RIGHT_SUPPORT_X) : NEAR_RIGHT_SUPPORT_X] doubleValue];
    yRightSupport = [[measurementDictonary objectForKey:  isFarMode ? (isVdMode ? VD_RIGHT_SUPPORT_Y : FAR_RIGHT_SUPPORT_Y) : NEAR_RIGHT_SUPPORT_Y] doubleValue];
    xLeftEye = [[measurementDictonary objectForKey:  isFarMode ? (isVdMode ? VD_LEFT_EYE_X : FAR_LEFT_EYE_X) : NEAR_LEFT_EYE_X] doubleValue];
    yLeftEye = [[measurementDictonary objectForKey:  isFarMode ? (isVdMode ? VD_LEFT_EYE_Y : FAR_LEFT_EYE_Y) : NEAR_LEFT_EYE_Y] doubleValue];
    xRightEye = [[measurementDictonary objectForKey:  isFarMode ? (isVdMode ? VD_RIGHT_EYE_X : FAR_RIGHT_EYE_X) : NEAR_RIGHT_EYE_X] doubleValue];
    yRightEye = [[measurementDictonary objectForKey:  isFarMode ? (isVdMode ? VD_RIGHT_EYE_Y : FAR_RIGHT_EYE_Y) : NEAR_RIGHT_EYE_Y] doubleValue];
}

- (void)loadToMeasurementDictonary:(NSMutableDictionary *)measurementDictonary
{
    BOOL isFarMode = [self isFarMode];
    BOOL isVdMode = 0;
    
    [measurementDictonary setValue:[NSNumber numberWithFloat:xLeftSupport] forKey:isFarMode ? (isVdMode ? VD_LEFT_SUPPORT_X : FAR_LEFT_SUPPORT_X) : NEAR_LEFT_SUPPORT_X];
    [measurementDictonary setValue:[NSNumber numberWithFloat:yLeftSupport] forKey:isFarMode ? (isVdMode ? VD_LEFT_SUPPORT_Y : FAR_LEFT_SUPPORT_Y) : NEAR_LEFT_SUPPORT_Y];
    
    [measurementDictonary setValue:[NSNumber numberWithFloat:xCenterSupport] forKey:isFarMode ? (isVdMode ? VD_CENTER_SUPPORT_X : FAR_CENTER_SUPPORT_X) : NEAR_CENTER_SUPPORT_X];
    [measurementDictonary setValue:[NSNumber numberWithFloat:yCenterSupport] forKey:isFarMode ? (isVdMode ? VD_CENTER_SUPPORT_Y : FAR_CENTER_SUPPORT_Y) : NEAR_CENTER_SUPPORT_Y];
    
    [measurementDictonary setValue:[NSNumber numberWithFloat:xRightSupport] forKey:isFarMode ? (isVdMode ? VD_RIGHT_SUPPORT_X : FAR_RIGHT_SUPPORT_X) : NEAR_RIGHT_SUPPORT_X];
    [measurementDictonary setValue:[NSNumber numberWithFloat:yRightSupport] forKey:isFarMode ? (isVdMode ? VD_RIGHT_SUPPORT_Y : FAR_RIGHT_SUPPORT_Y) : NEAR_RIGHT_SUPPORT_Y];
    
    [measurementDictonary setValue:[NSNumber numberWithFloat:xLeftEye] forKey:isFarMode ? (isVdMode ? VD_LEFT_EYE_X : FAR_LEFT_EYE_X) : NEAR_LEFT_EYE_X];
    [measurementDictonary setValue:[NSNumber numberWithFloat:yLeftEye] forKey:isFarMode ? (isVdMode ? VD_LEFT_EYE_Y : FAR_LEFT_EYE_Y) : NEAR_LEFT_EYE_Y];
    
    [measurementDictonary setValue:[NSNumber numberWithFloat:xRightEye] forKey:isFarMode ? (isVdMode ? VD_RIGHT_EYE_X : FAR_RIGHT_EYE_X) : NEAR_RIGHT_EYE_X];
    [measurementDictonary setValue:[NSNumber numberWithFloat:yRightEye] forKey:isFarMode ? (isVdMode ? VD_RIGHT_EYE_Y : FAR_RIGHT_EYE_Y) : NEAR_RIGHT_EYE_Y];
}


@end
