//
//  ResultsData.h
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 18.03.13.
//
//

#import <Foundation/Foundation.h>
#import "NearFarData.h"

@interface ResultsData : NearFarData

@property (nonatomic, strong) NSString *dataId;

@property (nonatomic, assign) double pdLeft;
@property (nonatomic, assign) double pdRight;

@property (nonatomic, assign) double heightLeft;
@property (nonatomic, assign) double heightRight;

@property (nonatomic, assign) double frameWidth;
@property (nonatomic, assign) double frameHight;

@property (nonatomic, assign) double frameBridge;

@property (nonatomic, assign) double photoDistance;
@property (nonatomic, assign) double pantoAngle;
@property (nonatomic, assign) double readindDistance;

@property (nonatomic, assign) double wertexDistance;
@property (nonatomic, assign) double wrapAngle;

@end
