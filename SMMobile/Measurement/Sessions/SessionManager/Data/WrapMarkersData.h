//
//  WrapMarkersData.h
//  PDdatabase
//
//  Created by Владимир Малашенков on 09.03.13.
//

#import <Foundation/Foundation.h>
#import "NearFarData.h"


@interface WrapMarkersData : NearFarData

@property (nonatomic, assign) double xLeftWrap;
@property (nonatomic, assign) double yLeftWrap;
@property (nonatomic, assign) double xRightWrap;
@property (nonatomic, assign) double yRightWrap;

@end
