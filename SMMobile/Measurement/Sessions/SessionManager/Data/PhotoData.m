//
//  PhotoData.m
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 18.03.13.
//
//

#import "PhotoData.h"
#import "NSString+Hash.h"

@implementation PhotoData

@synthesize lastUpdateDate;
@synthesize image;
@synthesize type;
@synthesize photoID;
@synthesize filename;

- (id)initWithImage:(UIImage *)photoImage
{
    self = [super init];
    if (self)
    {
        [self updateImage:photoImage];
        type = PhotoTypeJPEG;
        photoID = UUID;
    }
    return self;
}

- (void)generateFilename
{
    self.filename = [UUID MD5];
}

- (void)updateImage:(UIImage *)photoImage
{
    [self generateFilename];
    
    lastUpdateDate = [NSDate date];
    
    image = photoImage;
}

@end
