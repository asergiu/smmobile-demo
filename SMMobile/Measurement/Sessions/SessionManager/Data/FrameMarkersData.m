//
//  FrameMarkersData.m
//  PDdatabase
//
//  Created by Владимир Малашенков on 09.03.13.
//

#import "FrameMarkersData.h"
#import "PDMDataManager.h"

@implementation FrameMarkersData

@synthesize xLeftRect, xRightRect;
@synthesize xLeftUpRect, xRightUpRect;
@synthesize yLeftRect, yRightRect;
@synthesize yLeftUpRect, yRightUpRect;

+ (FrameMarkersData *)frameMarkersDataFromMeasurementDictonary:(NSDictionary *)measurementDictonary
                                                      dataType:(SessionType)dataType;

{
    FrameMarkersData *markersData = [FrameMarkersData new];
    
    [markersData fillDataFromMeasurementDictonary:measurementDictonary
                                         dataType:dataType];
    
    return markersData;
}

- (void)fillDataFromMeasurementDictonary:(NSDictionary *)measurementDictonary
                                dataType:(SessionType)dataType
{
    BOOL isFarMode = (dataType == SessionTypeFarPD);
    BOOL isVdMode = 0;
    BOOL secondPhotoMode = 0;
    
    self.xLeftRect = [[measurementDictonary objectForKey:isFarMode ? (isVdMode ? VD_LEFT_RECT_X : FAR_LEFT_RECT_X) : NEAR_LEFT_RECT_X] doubleValue];
    self.yLeftRect = [[measurementDictonary objectForKey:isFarMode ? (isVdMode ? VD_LEFT_RECT_Y : FAR_LEFT_RECT_Y) : NEAR_LEFT_RECT_Y] doubleValue];
    
    self.xRightUpRect = [[measurementDictonary objectForKey:isFarMode ? (secondPhotoMode ? VD_RIGHT_SCREEN_POINT_X : FAR_RIGHT_SCREEN_POINT_X) : NEAR_RIGHT_SCREEN_POINT_X] doubleValue];
    self.yRightUpRect = [[measurementDictonary objectForKey:isFarMode ? (secondPhotoMode ? VD_RIGHT_SCREEN_POINT_Y : FAR_RIGHT_SCREEN_POINT_Y) : NEAR_RIGHT_SCREEN_POINT_Y] doubleValue];
    
    self.xRightRect = [[measurementDictonary objectForKey:isFarMode ? (isVdMode ? VD_RIGHT_RECT_X : FAR_RIGHT_RECT_X) : NEAR_RIGHT_RECT_X] doubleValue];
    self.yRightRect = [[measurementDictonary objectForKey:isFarMode ? (isVdMode ? VD_RIGHT_RECT_Y : FAR_RIGHT_RECT_Y) : NEAR_RIGHT_RECT_Y] doubleValue];
    
    self.xLeftUpRect = [[measurementDictonary objectForKey:isFarMode ? (secondPhotoMode ? VD_LEFT_SCREEN_POINT_X : FAR_LEFT_SCREEN_POINT_X) : NEAR_LEFT_SCREEN_POINT_X] doubleValue];
    self.yLeftUpRect = [[measurementDictonary objectForKey:isFarMode ? (secondPhotoMode ? VD_LEFT_SCREEN_POINT_Y : FAR_LEFT_SCREEN_POINT_Y) : NEAR_LEFT_SCREEN_POINT_Y] doubleValue];
}

- (void)loadToMeasurementDictonary:(NSMutableDictionary *)measurementDictonary
{
    BOOL isFarMode = [self isFarMode];
    BOOL isVdMode = 0;
    BOOL secondPhotoMode = 0;
    
    [measurementDictonary setValue:[NSNumber numberWithFloat:xLeftRect] forKey:isFarMode ? (isVdMode ? VD_LEFT_RECT_X : FAR_LEFT_RECT_X) : NEAR_LEFT_RECT_X];
    [measurementDictonary setValue:[NSNumber numberWithFloat:yLeftRect] forKey:isFarMode ? (isVdMode ? VD_LEFT_RECT_Y : FAR_LEFT_RECT_Y) : NEAR_LEFT_RECT_Y];
    
    [measurementDictonary setValue:[NSNumber numberWithFloat:xRightUpRect] forKey:isFarMode ? (secondPhotoMode ? VD_RIGHT_SCREEN_POINT_X : FAR_RIGHT_SCREEN_POINT_X) : NEAR_RIGHT_SCREEN_POINT_X];
    [measurementDictonary setValue:[NSNumber numberWithFloat:yRightUpRect] forKey:isFarMode ? (secondPhotoMode ? VD_RIGHT_SCREEN_POINT_Y : FAR_RIGHT_SCREEN_POINT_Y) : NEAR_RIGHT_SCREEN_POINT_Y];
    
    [measurementDictonary setValue:[NSNumber numberWithFloat:xRightRect] forKey:isFarMode ? (isVdMode ? VD_RIGHT_RECT_X : FAR_RIGHT_RECT_X) : NEAR_RIGHT_RECT_X];
    [measurementDictonary setValue:[NSNumber numberWithFloat:yRightRect] forKey:isFarMode ? (isVdMode ? VD_RIGHT_RECT_Y : FAR_RIGHT_RECT_Y) : NEAR_RIGHT_RECT_Y];
    
    [measurementDictonary setValue:[NSNumber numberWithFloat:xLeftUpRect] forKey:isFarMode ? (secondPhotoMode ? VD_LEFT_SCREEN_POINT_X : FAR_LEFT_SCREEN_POINT_X) : NEAR_LEFT_SCREEN_POINT_X];
    [measurementDictonary setValue:[NSNumber numberWithFloat:yLeftUpRect] forKey:isFarMode ? (secondPhotoMode ? VD_LEFT_SCREEN_POINT_Y : FAR_LEFT_SCREEN_POINT_Y) : NEAR_LEFT_SCREEN_POINT_Y];
}

@end
