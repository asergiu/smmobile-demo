//
//  NearFarData.m
//  SeikoMultiTool
//
//  Created by Владимир Малашенков on 13.08.13.
//
//

#import "NearFarData.h"

@implementation NearFarData

- (BOOL)isFarMode
{
    return self.dataType == SessionTypeFarPD;
}

@end
