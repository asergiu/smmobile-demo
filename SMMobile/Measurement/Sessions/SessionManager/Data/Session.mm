//
//  Session.m
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 13.03.13.
//
//

#import "Session.h"
#import "UIDevice+IdentifierAddition.h"
#import "SessionManager.h"
#import "PDMResults.h"
#import "NSDictionary+SMConvertJSON.h"
#import "NSString+SMConvertJSON.h"

#define bundleID [[NSUserDefaults standardUserDefaults] stringForKey:@"bundle"]

@interface Session ()
{
    NSDictionary *additionalSessionData;
}

@end

@implementation Session

@synthesize allreadyOnServer;
@synthesize type;
@synthesize source;
@synthesize farMode;
@synthesize nearMarkers = _nearMarkers, farMarkers = _farMarkers;
@synthesize sessionId;
@synthesize farDataId, nearDataId;
@synthesize nearFrameMarkers = _nearFrameMarkers, farFrameMarkers = _farFrameMarkers;
@synthesize nearWrapMarkers = _nearWrapMarkers, farWrapMarkers = _farWrapMarkers;
@synthesize nearPhoto = _nearPhoto, farPhoto = _farPhoto, screenShot= _screenShot;
@synthesize clientFirstName, clientLastName;
@synthesize date, lastUpdateDate;
@synthesize macAddress;
@synthesize resultsDataFar, resultsDataNear, positionOfWearResultsData;
@synthesize loadedSession;
@dynamic currentSessionTypeFrameMarkers, currentSessionTypeMarkers;
@synthesize humanReadableData, humanReadableTime;
@synthesize storeData;
@synthesize progressiveData;
@dynamic lastUpdateDateString, dateString;

+ (Session *)createFromSessionDict:(NSDictionary *)sessionDict
{
    if (!sessionDict || ![sessionDict isKindOfClass:[NSDictionary class]])
    {
        NSLog(@"'createFromSessionDict' error, check input data");
        
        return nil;
    }
    
    SessionType type = [[sessionDict objectForKey:kTableRowSession_Type] intValue];
    
    NSString * firstName = [sessionDict objectForKey:kTableRowSession_ClienfFirstname];
    NSString * lastName = [sessionDict objectForKey:kTableRowSession_ClienfLastname];
    
    id date = [sessionDict objectForKey:kTableRowSession_Date];
    id dateLastUpdate = [sessionDict objectForKey:kTableRowSession_LastUpdate];
    
    id datePhotoFar = [sessionDict objectForKey:kTableRowSession_DatePhotoFar];
    id datePhotoNear = [sessionDict objectForKey:kTableRowSession_DatePhotoNear];
    
    NSString * macAddress = [sessionDict objectForKey:kTableRowSession_DeviceMac];
    
    NSString * farDataIdString = [sessionDict objectForKey:kTableRowSession_DataIdFar];
    NSString * nearDataIdString = [sessionDict objectForKey:kTableRowSession_DataIdNear];
    
    NSString * photoFar = [sessionDict objectForKey:kTableRowSession_PhotoFar];
    NSString * photoNear = [sessionDict objectForKey:kTableRowSession_PhotoNear];
    NSString * screenshot = [sessionDict objectForKey:kTableRowSession_Screenshot];
    
    //NSString * progressiveId = [sessionDict objectForKey:kTableRowSession_ProgressiveId];
    NSString * resultId = [sessionDict objectForKey:kTableRowSession_ResultsId];
    
    NSString * storeId = [sessionDict objectForKey:kTableRowSession_StoreId];
    
    NSString * netStatus = [sessionDict objectForKey:kTableRowSession_NetStatus];
    
    NSString * sessionID = [sessionDict objectForKey:kTableRowSession_Id];
    
    NSString *additionalData = [sessionDict objectForKey:kTableRowSession_AdditionalData];
    
    // create
    
    Session * newSession = [Session new];
    [newSession createAllData];
    
    newSession.type = type;
    
    newSession.macAddress = macAddress;
    newSession.sessionId = sessionID;
    newSession.loadedSession = YES;
    
    newSession.clientFirstName = firstName;
    newSession.clientLastName = lastName;
    
    if ([dateLastUpdate isKindOfClass:[NSDate class]])
        newSession.lastUpdateDate = dateLastUpdate;
    else
        newSession.lastUpdateDate = [DatabaseRequestsData getDateForValue:dateLastUpdate];
    
    if ([date isKindOfClass:[NSDate class]])
        newSession.date = date;
    else
        newSession.date = [DatabaseRequestsData getDateForValue:date];
    
    if ([datePhotoNear stringByReplacingOccurrencesOfString:@" " withString:@""].length)
        newSession.nearPhoto.lastUpdateDate = [newSession dateFromDateObject:datePhotoNear];
    
    if ([datePhotoFar stringByReplacingOccurrencesOfString:@" " withString:@""].length)
        newSession.farPhoto.lastUpdateDate = [newSession dateFromDateObject:datePhotoFar];
    
    newSession.nearPhoto.photoID = photoNear;
    newSession.farPhoto.photoID = photoFar;
    newSession.screenShot.photoID = screenshot;
    
    newSession.resultsDataFar = [ResultsData new];
    newSession.resultsDataNear = [ResultsData new];
    
    newSession.resultsDataFar.dataId = resultId;
    newSession.resultsDataNear.dataId = resultId;
    
    newSession.resultsId = resultId;
    
    newSession.nearDataId = nearDataIdString;
    newSession.farDataId = farDataIdString;
    
    [newSession updateHumanReadableData];
    
    newSession.allreadyOnServer = [netStatus intValue] == 1; //1 - Server key
    
    if ([additionalData stringByReplacingOccurrencesOfString:@" " withString:@""].length)
        [newSession setAdditionalDataWith:[additionalData getDictionaryFromJSONString]];
    
    //store
    StoreData * storeData = [StoreData new];
    
    storeData.storeID = storeId;
    storeData.code = storeId;
    
    newSession.storeData = storeData;
    
    return newSession;
}

- (id)init
{
    self = [super init];
    if (self) {
        macAddress = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        sessionId = UUID;
        loadedSession = NO;
        
        nearDataId = UUID;
        farDataId = UUID;
        
        source = SourceTypeLive;
        
        date = [NSDate date];
        
        filled = NO;
        
        allreadyOnServer = NO;
    }
    return self;
}

- (NSDate*) dateFromDateObject:(id)dateObject
{
    if ([dateObject isKindOfClass:[NSDate class]])
        return dateObject;
    else
        return [DatabaseRequestsData getDateForValue:dateObject];
}

- (void) createAllData
{
    [self createNearData];
    
    [self createFarData];
    
    //    self.resultsData = [ResultsData new];
    //    self.positionOfWearResultsData = [ResultsData new];
    
    self.screenShot = [PhotoData new];
}

- (void) createNearData
{
    self.nearMarkers = [MarkersData new];
    self.nearFrameMarkers = [FrameMarkersData new];
    self.nearWrapMarkers = [WrapMarkersData new];
    self.nearPhoto = [PhotoData new];
}

- (void) createFarData
{
    self.farMarkers = [MarkersData new];
    self.farFrameMarkers = [FrameMarkersData new];
    self.farWrapMarkers = [WrapMarkersData new];
    self.farPhoto = [PhotoData new];
}

- (void) updateHumanReadableData
{
    self.humanReadableData = [[DatabaseRequestsData timeFormatterForHumanReadableDate] stringFromDate:lastUpdateDate];
    self.humanReadableTime = [[DatabaseRequestsData timeFormatterForHumanReadableTime] stringFromDate:lastUpdateDate];
    
}

- (void) updateLastUpdateDate
{
    lastUpdateDate = [NSDate date];
}

- (void) updateCurrentStoreIfNull
{
    if ([self storeIsNull])
    {
        self.storeData.storeID = [SESSION_MANAGER currentStoreCode];
    }
}

- (BOOL) storeIsNull
{
    return ( [self.storeData.storeID isEqualToString:@"(null)"] ||
            [self.storeData.storeID isEqualToString:@""] );
}

- (void) setNearPhotoImage:(UIImage*)nearImage
{
    if (!_nearPhoto)
        _nearPhoto = [[PhotoData alloc] initWithImage:nearImage];
    else
        _nearPhoto.image = nearImage;
}

- (void) setFarPhotoImage:(UIImage*)farImage
{
    if (!_farPhoto)
        _farPhoto = [[PhotoData alloc] initWithImage:farImage];
    else
        _farPhoto.image = farImage;
}

- (void) setScreenShotImage:(UIImage*)screenImage
{
    if (!_screenShot)
        _screenShot = [[PhotoData alloc] initWithImage:screenImage];
    else
        _screenShot.image = screenImage;
}

#pragma mark - Additional Data

- (void)setAdditionalDataWith:(NSDictionary *)dict
{
    NSMutableDictionary *resultData = [NSMutableDictionary dictionary];
    
    if (additionalSessionData)
        [resultData addEntriesFromDictionary:additionalSessionData];
    
    [resultData addEntriesFromDictionary:dict];
    
    additionalSessionData = resultData;
}

- (NSString *)getAdditionalDataAsJSONString
{
    if (!additionalSessionData)
        return [[NSDictionary dictionary] getJSONString];
    
    return [additionalSessionData getJSONString];
}

- (id)getAdditionalDataForKey:(NSString *)key
{
    if (!additionalSessionData)
        return nil;
    
    return additionalSessionData[key];
}

#pragma mark -

- (MarkersData*) currentSessionTypeMarkers
{
    switch (type)
    {
        case SessionTypeFarPD:
            return _farMarkers;
            
        case SessionTypeNearPD:
            return _nearMarkers;
            
        default:
            return nil;
    }}

- (FrameMarkersData*) currentSessionTypeFrameMarkers
{
    switch (type)
    {
        case SessionTypeFarPD:
            return _farFrameMarkers;
            
        case SessionTypeNearPD:
            return _nearFrameMarkers;
            
        default:
            return nil;
    }
}

- (WrapMarkersData*) wrapMarkersForType:(SessionType)aType;
{
    switch (aType)
    {
        case SessionTypeFarPD:
            return _farWrapMarkers;
            
        case SessionTypeNearPD:
            return _nearWrapMarkers;
            
        default:
            return nil;
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Session ID= %@, LastUpdadeDate = %@, NetStatus = %d", self.sessionId, self.lastUpdateDate, self.allreadyOnServer];
}

- (void)fillAllData
{
    if (filled)
    {
        NSLog(@"Already filled");
        return;
    }
    
    [self fillFromLocal];
    
    filled = YES;
}

- (void) storeSession
{
    switch (source) {
        case SourceTypeLive:
        case SourceTypeServer:
        {
            //store to sqlite
            NSLog(@"Store to sqlite");
            
            [SESSION_MANAGER storeSessionToSQLite:self];
        }
            break;
            
        case SourceTypeLocal:
        {
            //store to mysql
            NSLog(@"Store to mysql");
            
            [SESSION_MANAGER storeSessionToServer:self];
        }
            break;
    }
}

- (void) deleteSession
{
    [SESSION_MANAGER removeSession:self targetSource:SourceTypeLocal];
}

- (void) deleteSessionFromServer
{
    [SESSION_MANAGER removeSession:self targetSource:SourceTypeServer];
}

- (NSString*) lastUpdateDateString
{
    return [DatabaseRequestsData dateStringFromDate:self.lastUpdateDate];
}

- (NSString*) dateString
{
    return [DatabaseRequestsData dateStringFromDate:self.date];
}

#pragma mark Fill methods

- (void) fillFromServer
{
    [SESSION_MANAGER fillSession:self forSourceType:SourceTypeServer];
}

- (void) fillFromLocal
{
    [SESSION_MANAGER fillSession:self forSourceType:SourceTypeLocal];
}

- (void) loadToDataManager:(PDMDataManager*)dataManager
{
    //BOOL isFarMode = (self.type == SessionTypeFarPD);
    //BOOL isVdMode = 0;
    
    [dataManager clearImages];
    
    NSNumber *support_type = [self getAdditionalDataForKey:@"fred_type"];
    
    if (support_type)
        [dataManager loadCurrentSessionSupportType:[support_type intValue]];
    else
        [dataManager loadCurrentSessionSupportType:0];
    
    NSDictionary *use_ipad_flash = [self getAdditionalDataForKey:@"ipad_flash"];
    
    if (use_ipad_flash)
        [dataManager setFlashType:[use_ipad_flash[@"ipad_flash_type"] integerValue]];
    
    if (![self.farDataId isEqualToString:[DatabaseRequestsData nullUUID]])
    {
        [self.farMarkers loadToMeasurementDictonary:dataManager.storage];
        [self.farFrameMarkers loadToMeasurementDictonary:dataManager.storage];
        
        dataManager.farPDImageRotated = self.farPhoto.image;
        dataManager.farPDImage = self.farPhoto.image;
        
        [dataManager.storage setValue:[NSNumber numberWithFloat:_snappedAngleFar] forKey:FAR_SNAPPED_ANGLE];
        
        if (self.resultsDataFar.wrapAngle >= 0)
        {
            dataManager.topWrapValue = [NSNumber numberWithFloat:self.resultsDataFar.wrapAngle];
        }
        
        [self loadAdditionalToDataManager:dataManager dataType:SessionTypeFarPD];
    }
    
    if (![self.nearDataId isEqualToString:[DatabaseRequestsData nullUUID]])
    {
        [self.nearMarkers loadToMeasurementDictonary:dataManager.storage];
        [self.nearFrameMarkers loadToMeasurementDictonary:dataManager.storage];
        
        dataManager.nearPDImageRotated = self.nearPhoto.image;
        dataManager.nearPDImage = self.nearPhoto.image;
        
        [dataManager.storage setValue:[NSNumber numberWithFloat:_snappedAngleNear] forKey:NEAR_SNAPPED_ANGLE];
        
        [self loadAdditionalToDataManager:dataManager dataType:SessionTypeNearPD];
    }
}

- (void) loadAdditionalToDataManager:(PDMDataManager*)dataManager
                            dataType:(SessionType)dataType
{
    BOOL isFarMode = (dataType == SessionTypeFarPD);
    BOOL isVdMode = 0;
    
    [dataManager.storage setValue:[NSNumber numberWithInt:0] forKey:isFarMode ? (isVdMode ? VD_RIGHT_RECT_WIDTH : FAR_RIGHT_RECT_WIDTH) : NEAR_RIGHT_RECT_WIDTH];
    [dataManager.storage setValue:[NSNumber numberWithInt:0] forKey:isFarMode ? (isVdMode ? VD_RIGHT_RECT_HEIGHT : FAR_RIGHT_RECT_HEIGHT) : NEAR_RIGHT_RECT_HEIGHT];
    
    [dataManager.storage setValue:[NSNumber numberWithInt:0] forKey:isFarMode ? (isVdMode ? VD_LEFT_RECT_WIDTH : FAR_LEFT_RECT_WIDTH) : NEAR_LEFT_RECT_WIDTH];
    [dataManager.storage setValue:[NSNumber numberWithInt:0] forKey:isFarMode ? (isVdMode ? VD_LEFT_RECT_HEIGHT : FAR_LEFT_RECT_HEIGHT) : NEAR_LEFT_RECT_HEIGHT];
}

- (void) fillAngleValuesFromDataManager:(PDMDataManager*)dataManager
{
    _snappedAngleFar = [[dataManager.storage objectForKey:FAR_SNAPPED_ANGLE] doubleValue];
    
    _snappedAngleNear = [[dataManager.storage objectForKey:NEAR_SNAPPED_ANGLE] doubleValue];
}


#pragma mark - Setters

- (void) setNearFrameMarkers:(FrameMarkersData *)nearFrameMarkers
{
    if (_nearFrameMarkers != nearFrameMarkers)
    {
        _nearFrameMarkers = nearFrameMarkers;
    }
    
    _nearFrameMarkers.dataType = SessionTypeNearPD;
}

- (void) setNearMarkers:(MarkersData *)nearMarkers
{
    if (_nearMarkers != nearMarkers)
    {
        _nearMarkers = nearMarkers;
    }
    
    _nearMarkers.dataType = SessionTypeNearPD;
}

- (void) setNearWrapMarkers:(WrapMarkersData *)nearWrapMarkers
{
    if (_nearWrapMarkers != nearWrapMarkers)
    {
        _nearWrapMarkers = nearWrapMarkers;
    }
    
    _nearWrapMarkers.dataType = SessionTypeNearPD;
}

- (void) setNearPhoto:(PhotoData *)nearPhoto
{
    if (_nearPhoto != nearPhoto)
    {
        _nearPhoto = nearPhoto;
    }
    
    _nearPhoto.dataType = SessionTypeNearPD;
}

- (void) setFarFrameMarkers:(FrameMarkersData *)farFrameMarkers
{
    if (_farFrameMarkers != farFrameMarkers)
    {
        _farFrameMarkers = farFrameMarkers;
    }
    
    _farFrameMarkers.dataType = SessionTypeFarPD;
}

- (void) setFarMarkers:(MarkersData *)farMarkers
{
    if (_farMarkers != farMarkers)
    {
        _farMarkers = farMarkers;
    }
    
    _farMarkers.dataType = SessionTypeFarPD;
}

- (void) setFarWrapMarkers:(WrapMarkersData *)farWrapMarkers
{
    if (_farWrapMarkers != farWrapMarkers)
    {
        _farWrapMarkers = farWrapMarkers;
    }
    
    _farWrapMarkers.dataType = SessionTypeFarPD;
}

- (void) setFarPhoto:(PhotoData *)farPhoto
{
    if (_farPhoto != farPhoto)
    {
        _farPhoto = farPhoto;
    }
    
    _farPhoto.dataType = SessionTypeFarPD;
}

- (BOOL)sessionFromCurrentStore
{
    NSString * currentStoreId = [SESSION_MANAGER currentStoreCode];
    
    if (!self)
        return NO;
    
    return (currentStoreId &&
            [self.storeData.storeID isEqualToString:currentStoreId]);
}

#pragma mark - Session Dictionary

+ (Session *)createSessionFromServerDict:(NSDictionary *)dictionary
{
    Session *newSession = [Session new];
    
    [newSession createAllData];
    
    if (!dictionary)
    {
        return nil;
    }
    
    NSDictionary *sess = [dictionary objectForKey:@"SESSION"];
    
    newSession.sessionId = [sess objectForKey:kTableRowSession_Id];
    newSession.type = [[sess objectForKey:kTableRowSession_Type] intValue];
    newSession.clientFirstName = [sess objectForKey:kTableRowSession_ClienfFirstname];
    newSession.clientLastName  = [sess objectForKey:kTableRowSession_ClienfLastname];
    newSession.macAddress = [sess objectForKey:kTableRowSession_DeviceMac];
    
    id date = [sess objectForKey:kTableRowSession_Date];
    id dateLastUpdate = [sess objectForKey:kTableRowSession_LastUpdate];
    
    if ([date isKindOfClass:[NSDate class]])
        newSession.date = date;
    else
        newSession.date = [DatabaseRequestsData dateForString:date];
    
    if (!newSession.date)
        newSession.date = [DatabaseRequestsData getDateForValue:date];
    
    if ([dateLastUpdate isKindOfClass:[NSDate class]])
        newSession.lastUpdateDate = dateLastUpdate;
    else
        newSession.lastUpdateDate = [DatabaseRequestsData dateForString:dateLastUpdate];

    if (!newSession.lastUpdateDate)
        newSession.lastUpdateDate = [DatabaseRequestsData getDateForValue:dateLastUpdate];
    
    if (!([[sess objectForKey:kTableRowSession_AdditionalData] isEqual:@"(null)"]))
    {
        NSString *additionalData = [sess objectForKey:kTableRowSession_AdditionalData];
        
        if ([additionalData stringByReplacingOccurrencesOfString:@" " withString:@""].length)
            [newSession setAdditionalDataWith:[additionalData getDictionaryFromJSONString]];
    }
    
    newSession.screenShot.photoID = [sess objectForKey:kTableRowSession_Screenshot];
    
    if (!([[sess objectForKey:kTableRowSession_PhotoNear] isEqual:@"(null)"]
          && [[sess objectForKey:kTableRowSession_PhotoNear] isEqualToString:UUID_NULL]))
    {
        newSession.nearPhoto.photoID = [sess objectForKey:kTableRowSession_PhotoNear];
    }
    
    if (newSession.nearPhoto.photoID)
    {
        newSession.resultsDataNear = [ResultsData new];
    }
    
    if (!([[sess objectForKey:kTableRowSession_PhotoFar] isEqual:@"(null)"]
          && [[sess objectForKey:kTableRowSession_PhotoFar] isEqualToString:UUID_NULL]))
    {
        newSession.farPhoto.photoID = [sess objectForKey:kTableRowSession_PhotoFar];
    }
    
    if (newSession.farPhoto.photoID)
    {
        newSession.resultsDataFar = [ResultsData new];
    }
    
    newSession.resultsDataFar.dataId  = [sess objectForKey:kTableRowSession_ResultsId];
    newSession.resultsDataNear.dataId = [sess objectForKey:kTableRowSession_ResultsId];
    
    newSession.resultsId = [sess objectForKey:kTableRowSession_ResultsId];
    
    if (!([[sess objectForKey:kTableRowSession_DataIdNear] isEqual:@"(null)"]
          && [[sess objectForKey:kTableRowSession_DataIdNear] isEqualToString:UUID_NULL]))
    {
        newSession.nearDataId = [sess objectForKey:kTableRowSession_DataIdNear];
    }
    
    if (!([[sess objectForKey:kTableRowSession_DataIdFar] isEqual:@"(null)"]
          && [[sess objectForKey:kTableRowSession_DataIdFar] isEqualToString:UUID_NULL]))
    {
        newSession.farDataId  = [sess objectForKey:kTableRowSession_DataIdFar];
    }
    
    [newSession updateHumanReadableData];
    
    newSession.allreadyOnServer = YES;
    
    //store
    StoreData * storeData = [StoreData new];
    
    storeData.storeID = [sess objectForKey:kTableRowSession_StoreId];
    storeData.code    = [sess objectForKey:kTableRowSession_StoreId];
    
    newSession.storeData = storeData;
    
    
    NSDictionary *results = [dictionary objectForKey:@"RESULTS"];
    
    newSession.resultsDataFar.pdLeft = [[results objectForKey:kTableRowResults_FarPDL] doubleValue];
    newSession.resultsDataFar.pdRight = [[results objectForKey:kTableRowResults_FarPDR] doubleValue];
    newSession.resultsDataFar.heightLeft = [[results objectForKey:kTableRowResults_FarHL] doubleValue];
    newSession.resultsDataFar.heightRight = [[results objectForKey:kTableRowResults_FarHR] doubleValue];
    newSession.resultsDataFar.frameWidth = [[results objectForKey:kTableRowResults_FarFW] doubleValue];
    newSession.resultsDataFar.frameHight = [[results objectForKey:kTableRowResults_FarFH] doubleValue];
    newSession.resultsDataFar.frameBridge = [[results objectForKey:kTableRowResults_FarFBridge] doubleValue];
    newSession.resultsDataFar.photoDistance = [[results objectForKey:kTableRowResults_FarPhotoDist] doubleValue];
    newSession.resultsDataFar.pantoAngle = [[results objectForKey:kTableRowResults_FarPanto] doubleValue];
    newSession.resultsDataFar.wrapAngle = [[results objectForKey:kTableRowResults_WrapAngle] doubleValue];
    
    newSession.resultsDataNear.pdLeft = [[results objectForKey:kTableRowResults_NearPDL] doubleValue];
    newSession.resultsDataNear.pdRight = [[results objectForKey:kTableRowResults_NearPDR] doubleValue];
    newSession.resultsDataNear.heightLeft = [[results objectForKey:kTableRowResults_NearHL] doubleValue];
    newSession.resultsDataNear.heightRight = [[results objectForKey:kTableRowResults_NearHR] doubleValue];
    newSession.resultsDataNear.frameBridge = [[results objectForKey:kTableRowResults_NearFBridge] doubleValue];
    newSession.resultsDataNear.readindDistance = [[results objectForKey:kTableRowResults_NearReadingDist] doubleValue];
    
    newSession.resultsId = [results objectForKey:kTableRowResults_Id];
    
    
    NSDictionary *far_photo = [dictionary objectForKey:@"PHOTO_FAR"];
    
    if (far_photo)
    {
        newSession.farPhoto.photoID = [far_photo objectForKey:kTableRowPhoto_Id];
        newSession.farPhoto.filename = [far_photo objectForKey:kTableRowPhoto_Data];
        newSession.farPhoto.lastUpdateDate = [far_photo objectForKey:kTableRowSession_DatePhotoFar];
        newSession.farPhoto.type = PhotoTypeJPEG;
    }
    
    
    NSDictionary *near_photo = [dictionary objectForKey:@"PHOTO_NEAR"];
    
    if (near_photo)
    {
        newSession.nearPhoto.photoID = [near_photo objectForKey:kTableRowPhoto_Id];
        newSession.nearPhoto.filename = [near_photo objectForKey:kTableRowPhoto_Data];
        newSession.nearPhoto.lastUpdateDate = [near_photo objectForKey:kTableRowSession_DatePhotoFar];
        newSession.nearPhoto.type = PhotoTypeJPEG;
    }
    
    
    NSDictionary *screenshot = [dictionary objectForKey:@"SCREENSHOT"];
    
    if (screenshot)
    {
        newSession.screenShot.photoID = [screenshot objectForKey:kTableRowPhoto_Id];
        newSession.screenShot.filename = [screenshot objectForKey:kTableRowPhoto_Data];
        newSession.screenShot.lastUpdateDate = [screenshot objectForKey:kTableRowSession_DatePhotoFar];
        newSession.screenShot.type = PhotoTypeJPEG;
    }
    
    
    NSDictionary *data_far = [dictionary objectForKey:@"DATA_FAR"];
    
    if (data_far)
    {
        newSession.snappedAngleFar = [[data_far objectForKey:kTableRowData_iPadAngle] doubleValue];
        newSession.farMarkers.xLeftSupport = [[data_far objectForKey:kTableRowData_MarkerSupportLX] doubleValue];
        newSession.farMarkers.yLeftSupport = [[data_far objectForKey:kTableRowData_MarkerSupportLY] doubleValue];
        newSession.farMarkers.xCenterSupport = [[data_far objectForKey:kTableRowData_MarkerSupportCX] doubleValue];
        newSession.farMarkers.yCenterSupport = [[data_far objectForKey:kTableRowData_MarkerSupportCY] doubleValue];
        newSession.farMarkers.xRightSupport = [[data_far objectForKey:kTableRowData_MarkerSupportRX] doubleValue];
        newSession.farMarkers.yRightSupport = [[data_far objectForKey:kTableRowData_MarkerSupportRY] doubleValue];
        newSession.farMarkers.xLeftEye = [[data_far objectForKey:kTableRowData_MarkerEyeLX] doubleValue];
        newSession.farMarkers.yLeftEye = [[data_far objectForKey:kTableRowData_MarkerEyeLY] doubleValue];
        newSession.farMarkers.xRightEye = [[data_far objectForKey:kTableRowData_MarkerEyeRX] doubleValue];
        newSession.farMarkers.yRightEye = [[data_far objectForKey:kTableRowData_MarkerEyeRY] doubleValue];
        
        newSession.farFrameMarkers.xLeftRect = [[data_far objectForKey:kTableRowData_FrameLX] doubleValue];
        newSession.farFrameMarkers.yLeftRect = [[data_far objectForKey:kTableRowData_FrameLY] doubleValue];
        newSession.farFrameMarkers.xRightRect = [[data_far objectForKey:kTableRowData_FrameRX] doubleValue];
        newSession.farFrameMarkers.yRightRect = [[data_far objectForKey:kTableRowData_FrameRY] doubleValue];
        newSession.farFrameMarkers.xLeftUpRect = [[data_far objectForKey:kTableRowData_FrameULX] doubleValue];
        newSession.farFrameMarkers.yLeftUpRect = [[data_far objectForKey:kTableRowData_FrameULY] doubleValue];
        newSession.farFrameMarkers.xRightUpRect = [[data_far objectForKey:kTableRowData_FrameURX] doubleValue];
        newSession.farFrameMarkers.yRightUpRect = [[data_far objectForKey:kTableRowData_FrameURY] doubleValue];
        
        newSession.farWrapMarkers.xLeftWrap = [[data_far objectForKey:kTableRowData_WrapLX] doubleValue];
        newSession.farWrapMarkers.yLeftWrap = [[data_far objectForKey:kTableRowData_WrapLY] doubleValue];
        newSession.farWrapMarkers.xRightWrap = [[data_far objectForKey:kTableRowData_WrapRX] doubleValue];
        newSession.farWrapMarkers.yRightWrap = [[data_far objectForKey:kTableRowData_WrapRY] doubleValue];
    }
    
    NSDictionary *data_near = [dictionary objectForKey:@"DATA_NEAR"];
    
    if (data_near)
    {
        newSession.snappedAngleNear = [[data_near objectForKey:kTableRowData_iPadAngle] doubleValue];
        newSession.nearMarkers.xLeftSupport = [[data_near objectForKey:kTableRowData_MarkerSupportLX] doubleValue];
        newSession.nearMarkers.yLeftSupport = [[data_near objectForKey:kTableRowData_MarkerSupportLY] doubleValue];
        newSession.nearMarkers.xCenterSupport = [[data_near objectForKey:kTableRowData_MarkerSupportCX] doubleValue];
        newSession.nearMarkers.yCenterSupport = [[data_near objectForKey:kTableRowData_MarkerSupportCY] doubleValue];
        newSession.nearMarkers.xRightSupport = [[data_near objectForKey:kTableRowData_MarkerSupportRX] doubleValue];
        newSession.nearMarkers.yRightSupport = [[data_near objectForKey:kTableRowData_MarkerSupportRY] doubleValue];
        newSession.nearMarkers.xLeftEye = [[data_near objectForKey:kTableRowData_MarkerEyeLX] doubleValue];
        newSession.nearMarkers.yLeftEye = [[data_near objectForKey:kTableRowData_MarkerEyeLY] doubleValue];
        newSession.nearMarkers.xRightEye = [[data_near objectForKey:kTableRowData_MarkerEyeRX] doubleValue];
        newSession.nearMarkers.yRightEye = [[data_near objectForKey:kTableRowData_MarkerEyeRY] doubleValue];
        
        newSession.nearFrameMarkers.xLeftRect = [[data_near objectForKey:kTableRowData_FrameLX] doubleValue];
        newSession.nearFrameMarkers.yLeftRect = [[data_near objectForKey:kTableRowData_FrameLY] doubleValue];
        newSession.nearFrameMarkers.xRightRect = [[data_near objectForKey:kTableRowData_FrameRX] doubleValue];
        newSession.nearFrameMarkers.yRightRect = [[data_near objectForKey:kTableRowData_FrameRY] doubleValue];
        newSession.nearFrameMarkers.xLeftUpRect = [[data_near objectForKey:kTableRowData_FrameULX] doubleValue];
        newSession.nearFrameMarkers.yLeftUpRect = [[data_near objectForKey:kTableRowData_FrameULY] doubleValue];
        newSession.nearFrameMarkers.xRightUpRect = [[data_near objectForKey:kTableRowData_FrameURX] doubleValue];
        newSession.nearFrameMarkers.yRightUpRect = [[data_near objectForKey:kTableRowData_FrameURY] doubleValue];
        
        newSession.nearWrapMarkers.xLeftWrap = [[data_near objectForKey:kTableRowData_WrapLX] doubleValue];
        newSession.nearWrapMarkers.yLeftWrap = [[data_near objectForKey:kTableRowData_WrapLY] doubleValue];
        newSession.nearWrapMarkers.xRightWrap = [[data_near objectForKey:kTableRowData_WrapRX] doubleValue];
        newSession.nearWrapMarkers.yRightWrap = [[data_near objectForKey:kTableRowData_WrapRY] doubleValue];
    }
    
    return newSession;
}

+ (NSDictionary *)createDictFromSession:(Session *)session
{
    if (!session)
    {
        return nil;
    }
    
    NSMutableDictionary *sessionDict = [NSMutableDictionary new];
    
    // Add SESSION
    NSMutableDictionary *sess = [NSMutableDictionary new];
    
    [sess setObject:session.sessionId forKey:kTableRowSession_Id];
    [sess setObject:[NSString stringWithFormat:@"%i", session.type ] forKey:kTableRowSession_Type];
    [sess setObject:session.clientFirstName forKey:kTableRowSession_ClienfFirstname];
    [sess setObject:session.clientLastName forKey:kTableRowSession_ClienfLastname];
    [sess setObject:session.macAddress forKey:kTableRowSession_DeviceMac];
    [sess setObject:bundleID forKey:kTableRowSession_BundleId];
    [sess setObject:session.dateString forKey:kTableRowSession_Date];
    [sess setObject:session.lastUpdateDateString forKey:kTableRowSession_LastUpdate];
    
    [sess setObject:@(session.allreadyOnServer) forKey:kTableRowSession_NetStatus];
    
    [sess setObject:[session getAdditionalDataAsJSONString] forKey:kTableRowSession_AdditionalData];
    
    // Add RESULTS
    NSMutableDictionary *results = [NSMutableDictionary new];
    
    BOOL hasFarData = session.resultsDataFar ? YES : NO;
    BOOL hasNearData = session.resultsDataNear ? YES : NO;
    
    if (!hasFarData && !hasNearData)
    {
        return nil;
    }
    
    [results setObject:[NSString stringWithFormat:@"%d", hasFarData] forKey:kTableRowResults_FarDataPresent];
    
    [results setObject:(hasFarData ? [NSString stringWithFormat:@"%f", session.resultsDataFar.pdLeft] : @"0") forKey:kTableRowResults_FarPDL];
    [results setObject:(hasFarData ? [NSString stringWithFormat:@"%f", session.resultsDataFar.pdRight] : @"0") forKey:kTableRowResults_FarPDR];
    [results setObject:(hasFarData ? [NSString stringWithFormat:@"%f", session.resultsDataFar.heightLeft] : @"0") forKey:kTableRowResults_FarHL];
    [results setObject:(hasFarData ? [NSString stringWithFormat:@"%f", session.resultsDataFar.heightRight] : @"0") forKey:kTableRowResults_FarHR];
    [results setObject:(hasFarData ? [NSString stringWithFormat:@"%f", session.resultsDataFar.frameWidth] : @"0") forKey:kTableRowResults_FarFW];
    [results setObject:(hasFarData ? [NSString stringWithFormat:@"%f", session.resultsDataFar.frameHight] : @"0") forKey:kTableRowResults_FarFH];
    [results setObject:(hasFarData ? [NSString stringWithFormat:@"%f", session.resultsDataFar.frameBridge] : @"0") forKey:kTableRowResults_FarFBridge];
    [results setObject:(hasFarData ? [NSString stringWithFormat:@"%f", session.resultsDataFar.photoDistance] : @"0") forKey:kTableRowResults_FarPhotoDist];
    [results setObject:(hasFarData ? [NSString stringWithFormat:@"%f", session.resultsDataFar.pantoAngle] : @"0") forKey:kTableRowResults_FarPanto];
    [results setObject:(hasFarData ? [NSString stringWithFormat:@"%f", session.resultsDataFar.wrapAngle] : @"-5") forKey:kTableRowResults_WrapAngle];
    
    [results setObject:[NSString stringWithFormat:@"%d", hasNearData] forKey:kTableRowResults_NearDataPresent];
    
    [results setObject:(hasNearData ? [NSString stringWithFormat:@"%f", session.resultsDataNear.pdLeft] : @"0") forKey:kTableRowResults_NearPDL];
    [results setObject:(hasNearData ? [NSString stringWithFormat:@"%f", session.resultsDataNear.pdRight] : @"0") forKey:kTableRowResults_NearPDR];
    [results setObject:(hasNearData ? [NSString stringWithFormat:@"%f", session.resultsDataNear.heightLeft] : @"0") forKey:kTableRowResults_NearHL];
    [results setObject:(hasNearData ? [NSString stringWithFormat:@"%f", session.resultsDataNear.heightRight] : @"0") forKey:kTableRowResults_NearHR];
    [results setObject:(hasNearData ? [NSString stringWithFormat:@"%f", session.resultsDataNear.frameBridge] : @"0") forKey:kTableRowResults_NearFBridge];
    [results setObject:(hasNearData ? [NSString stringWithFormat:@"%f", session.resultsDataNear.readindDistance] : @"0") forKey:kTableRowResults_NearReadingDist];
    
    [results setObject:@"0" forKey:kTableRowResults_PositionOfWearPresent];
    [results setObject:@"0" forKey:kTableRowResults_VertexDist];
    
    [results setObject:session.sessionId forKey:kTableRowResults_Id];
    
    [sessionDict setObject:results forKey:@"RESULTS"];
    
    [sess setObject:session.sessionId forKey:kTableRowSession_ResultsId];
    
    // Add PHOTO_FAR
    PhotoData *photoFar = session.farPhoto;
    
    if (photoFar.photoID && ![photoFar.photoID isEqualToString:UUID_NULL] && ![photoFar.photoID isEqualToString:@"(null)"])
    {
        NSMutableDictionary *photo_far = [NSMutableDictionary new];
        
        [photo_far setObject:photoFar.photoID forKey:kTableRowPhoto_Id];
        [photo_far setObject:@"0" forKey:kTableRowPhoto_Type];
        [photo_far setObject:photoFar.filename forKey:kTableRowPhoto_Data];
        
        [sessionDict setObject:photo_far forKey:@"PHOTO_FAR"];
        
        [sess setObject:photoFar.photoID forKey:kTableRowSession_PhotoFar];
        
        if ([photoFar.lastUpdateDate isKindOfClass:[NSDate class]])
            [sess setObject:[[DatabaseRequestsData timeFormatterForSaveDate] stringFromDate:photoFar.lastUpdateDate]
                     forKey:kTableRowSession_DatePhotoFar];
        else
            [sess setObject:photoFar.lastUpdateDate forKey:kTableRowSession_DatePhotoFar];
    }
    
    // Add PHOTO_NEAR
    PhotoData *photoNear = session.nearPhoto;
    
    if (photoNear.photoID && ![photoNear.photoID isEqualToString:UUID_NULL] && ![photoNear.photoID isEqualToString:@"(null)"])
    {
        NSMutableDictionary *photo_near = [NSMutableDictionary new];
        
        [photo_near setObject:photoNear.photoID forKey:kTableRowPhoto_Id];
        [photo_near setObject:@"0" forKey:kTableRowPhoto_Type];
        [photo_near setObject:photoNear.filename forKey:kTableRowPhoto_Data];
        
        [sessionDict setObject:photo_near forKey:@"PHOTO_NEAR"];
        
        [sess setObject:photoNear.photoID forKey:kTableRowSession_PhotoNear];
        
        if ([photoNear.lastUpdateDate isKindOfClass:[NSDate class]])
            [sess setObject:[[DatabaseRequestsData timeFormatterForSaveDate] stringFromDate:photoNear.lastUpdateDate]
                     forKey:kTableRowSession_DatePhotoFar];
        else
            [sess setObject:photoNear.lastUpdateDate forKey:kTableRowSession_DatePhotoNear];
    }
    
    // Add SCREENSHOT
    PhotoData *screenshot = session.screenShot;
    
    if (screenshot.photoID && ![screenshot.photoID isEqualToString:UUID_NULL] && ![screenshot.photoID isEqualToString:@"(null)"])
    {
        NSMutableDictionary *photo_screenshot = [NSMutableDictionary new];
        
        [photo_screenshot setObject:screenshot.photoID forKey:kTableRowPhoto_Id];
        [photo_screenshot setObject:@"0" forKey:kTableRowPhoto_Type];
        [photo_screenshot setObject:screenshot.filename forKey:kTableRowPhoto_Data];
        
        [sessionDict setObject:photo_screenshot forKey:@"SCREENSHOT"];
        
        [sess setObject:screenshot.photoID forKey:kTableRowSession_Screenshot];
    }
    
    // Add DATA_FAR
    MarkersData *markersFar = session.farMarkers;
    
    if (markersFar && ![session.farDataId isEqualToString:UUID_NULL] && ![session.farDataId isEqual:@"(null)"])
    {
        NSMutableDictionary *far_data = [NSMutableDictionary new];
        
        [far_data setObject:session.farDataId forKey:kTableRowData_Id];
        
        [far_data setObject:@"1" forKey:kTableRowData_PhotoMarkersPresent];
        [far_data setObject:[NSString stringWithFormat:@"%f", session.snappedAngleFar] forKey:kTableRowData_iPadAngle];
        [far_data setObject:[NSString stringWithFormat:@"%f", markersFar.xLeftSupport] forKey:kTableRowData_MarkerSupportLX];
        [far_data setObject:[NSString stringWithFormat:@"%f", markersFar.yLeftSupport] forKey:kTableRowData_MarkerSupportLY];
        [far_data setObject:[NSString stringWithFormat:@"%f", markersFar.xCenterSupport] forKey:kTableRowData_MarkerSupportCX];
        [far_data setObject:[NSString stringWithFormat:@"%f", markersFar.yCenterSupport] forKey:kTableRowData_MarkerSupportCY];
        [far_data setObject:[NSString stringWithFormat:@"%f", markersFar.xRightSupport] forKey:kTableRowData_MarkerSupportRX];
        [far_data setObject:[NSString stringWithFormat:@"%f", markersFar.yRightSupport] forKey:kTableRowData_MarkerSupportRY];
        [far_data setObject:[NSString stringWithFormat:@"%f", markersFar.xLeftEye] forKey:kTableRowData_MarkerEyeLX];
        [far_data setObject:[NSString stringWithFormat:@"%f", markersFar.yLeftEye] forKey:kTableRowData_MarkerEyeLY];
        [far_data setObject:[NSString stringWithFormat:@"%f", markersFar.xRightEye] forKey:kTableRowData_MarkerEyeRX];
        [far_data setObject:[NSString stringWithFormat:@"%f", markersFar.yRightEye] forKey:kTableRowData_MarkerEyeRY];
        
        FrameMarkersData *frameFar = session.farFrameMarkers;
        
        [far_data setObject:@"1" forKey:kTableRowData_FrameMarkersPresent];
        [far_data setObject:[NSString stringWithFormat:@"%f", frameFar.xLeftRect] forKey:kTableRowData_FrameLX];
        [far_data setObject:[NSString stringWithFormat:@"%f", frameFar.yLeftRect] forKey:kTableRowData_FrameLY];
        [far_data setObject:[NSString stringWithFormat:@"%f", frameFar.xRightRect] forKey:kTableRowData_FrameRX];
        [far_data setObject:[NSString stringWithFormat:@"%f", frameFar.yRightRect] forKey:kTableRowData_FrameRY];
        [far_data setObject:[NSString stringWithFormat:@"%f", frameFar.xLeftUpRect] forKey:kTableRowData_FrameULX];
        [far_data setObject:[NSString stringWithFormat:@"%f", frameFar.yLeftUpRect] forKey:kTableRowData_FrameULY];
        [far_data setObject:[NSString stringWithFormat:@"%f", frameFar.xRightUpRect] forKey:kTableRowData_FrameURX];
        [far_data setObject:[NSString stringWithFormat:@"%f", frameFar.yRightUpRect] forKey:kTableRowData_FrameURY];
        
        WrapMarkersData *wrapFar = session.farWrapMarkers;
        
        [far_data setObject:@"1" forKey:kTableRowData_WrapMarkersPresent];
        [far_data setObject:[NSString stringWithFormat:@"%f", wrapFar.xLeftWrap] forKey:kTableRowData_WrapLX];
        [far_data setObject:[NSString stringWithFormat:@"%f", wrapFar.yLeftWrap] forKey:kTableRowData_WrapLY];
        [far_data setObject:[NSString stringWithFormat:@"%f", wrapFar.xRightWrap] forKey:kTableRowData_WrapRX];
        [far_data setObject:[NSString stringWithFormat:@"%f", wrapFar.yRightWrap] forKey:kTableRowData_WrapRY];
        
        [sessionDict setObject:far_data forKey:@"DATA_FAR"];
        
        [sess setObject:session.farDataId forKey:kTableRowSession_DataIdFar];
    }
    
    // Add DATA_NEAR
    MarkersData *markersNear = session.nearMarkers;
    
    if (markersNear && ![session.nearDataId isEqualToString:UUID_NULL] && ![session.nearDataId isEqual:@"(null)"])
    {
        NSMutableDictionary *near_data = [NSMutableDictionary new];
        
        [near_data setObject:session.nearDataId forKey:kTableRowData_Id];
        
        [near_data setObject:@"1" forKey:kTableRowData_PhotoMarkersPresent];
        [near_data setObject:[NSString stringWithFormat:@"%f", session.snappedAngleNear] forKey:kTableRowData_iPadAngle];
        [near_data setObject:[NSString stringWithFormat:@"%f", markersNear.xLeftSupport] forKey:kTableRowData_MarkerSupportLX];
        [near_data setObject:[NSString stringWithFormat:@"%f", markersNear.yLeftSupport] forKey:kTableRowData_MarkerSupportLY];
        [near_data setObject:[NSString stringWithFormat:@"%f", markersNear.xCenterSupport] forKey:kTableRowData_MarkerSupportCX];
        [near_data setObject:[NSString stringWithFormat:@"%f", markersNear.yCenterSupport] forKey:kTableRowData_MarkerSupportCY];
        [near_data setObject:[NSString stringWithFormat:@"%f", markersNear.xRightSupport] forKey:kTableRowData_MarkerSupportRX];
        [near_data setObject:[NSString stringWithFormat:@"%f", markersNear.yRightSupport] forKey:kTableRowData_MarkerSupportRY];
        [near_data setObject:[NSString stringWithFormat:@"%f", markersNear.xLeftEye] forKey:kTableRowData_MarkerEyeLX];
        [near_data setObject:[NSString stringWithFormat:@"%f", markersNear.yLeftEye] forKey:kTableRowData_MarkerEyeLY];
        [near_data setObject:[NSString stringWithFormat:@"%f", markersNear.xRightEye] forKey:kTableRowData_MarkerEyeRX];
        [near_data setObject:[NSString stringWithFormat:@"%f", markersNear.yRightEye] forKey:kTableRowData_MarkerEyeRY];
        
        FrameMarkersData *frameNear = session.nearFrameMarkers;
        
        [near_data setObject:@"1" forKey:kTableRowData_FrameMarkersPresent];
        [near_data setObject:[NSString stringWithFormat:@"%f", frameNear.xLeftRect] forKey:kTableRowData_FrameLX];
        [near_data setObject:[NSString stringWithFormat:@"%f", frameNear.yLeftRect] forKey:kTableRowData_FrameLY];
        [near_data setObject:[NSString stringWithFormat:@"%f", frameNear.xRightRect] forKey:kTableRowData_FrameRX];
        [near_data setObject:[NSString stringWithFormat:@"%f", frameNear.yRightRect] forKey:kTableRowData_FrameRY];
        [near_data setObject:[NSString stringWithFormat:@"%f", frameNear.xLeftUpRect] forKey:kTableRowData_FrameULX];
        [near_data setObject:[NSString stringWithFormat:@"%f", frameNear.yLeftUpRect] forKey:kTableRowData_FrameULY];
        [near_data setObject:[NSString stringWithFormat:@"%f", frameNear.xRightUpRect] forKey:kTableRowData_FrameURX];
        [near_data setObject:[NSString stringWithFormat:@"%f", frameNear.yRightUpRect] forKey:kTableRowData_FrameURY];
        
        WrapMarkersData *wrapNear = session.nearWrapMarkers;
        
        [near_data setObject:@"1" forKey:kTableRowData_WrapMarkersPresent];
        [near_data setObject:[NSString stringWithFormat:@"%f", wrapNear.xLeftWrap] forKey:kTableRowData_WrapLX];
        [near_data setObject:[NSString stringWithFormat:@"%f", wrapNear.yLeftWrap] forKey:kTableRowData_WrapLY];
        [near_data setObject:[NSString stringWithFormat:@"%f", wrapNear.xRightWrap] forKey:kTableRowData_WrapRX];
        [near_data setObject:[NSString stringWithFormat:@"%f", wrapNear.yRightWrap] forKey:kTableRowData_WrapRY];
        
        [sessionDict setObject:near_data forKey:@"DATA_NEAR"];
        
        [sess setObject:session.nearDataId forKey:kTableRowSession_DataIdNear];
    }
    
    if (session.storeData.code)
        [sess setObject:session.storeData.code forKey:kTableRowSession_StoreId];
    
    [sessionDict setObject:sess forKey:@"SESSION"];
    
    return sessionDict;
}

@end
