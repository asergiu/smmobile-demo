//
//  FrameMarkersData.h
//  PDdatabase
//
//  Created by Владимир Малашенков on 09.03.13.
//

#import <Foundation/Foundation.h>
#import "NearFarData.h"

@interface FrameMarkersData : NearFarData

@property (nonatomic, assign) double xLeftRect;
@property (nonatomic, assign) double yLeftRect;
@property (nonatomic, assign) double xRightRect;
@property (nonatomic, assign) double yRightRect;
@property (nonatomic, assign) double xLeftUpRect;
@property (nonatomic, assign) double yLeftUpRect;
@property (nonatomic, assign) double xRightUpRect;
@property (nonatomic, assign) double yRightUpRect;

+ (FrameMarkersData *)frameMarkersDataFromMeasurementDictonary:(NSDictionary *)measurementDictonary
                                                      dataType:(SessionType)dataType;

- (void)loadToMeasurementDictonary:(NSMutableDictionary *)measurementDictonary;

@end
