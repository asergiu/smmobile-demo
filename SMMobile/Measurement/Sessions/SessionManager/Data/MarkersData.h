//
//  MarkersData.h
//  PDdatabase
//
//  Created by Владимир Малашенков on 07.03.13.
//

#import <Foundation/Foundation.h>
#import "NearFarData.h"

@interface MarkersData : NearFarData

@property (nonatomic, assign) double xLeftSupport;
@property (nonatomic, assign) double yLeftSupport;
@property (nonatomic, assign) double xRightSupport;
@property (nonatomic, assign) double yRightSupport;
@property (nonatomic, assign) double xCenterSupport;
@property (nonatomic, assign) double yCenterSupport;
@property (nonatomic, assign) double xLeftEye;
@property (nonatomic, assign) double yLeftEye;
@property (nonatomic, assign) double xRightEye;
@property (nonatomic, assign) double yRightEye;

+ (MarkersData *)markersDataFromMeasurementDictonary:(NSDictionary*)measurementDictonary dataType:(SessionType)dataType;

- (void)loadToMeasurementDictonary:(NSMutableDictionary *)measurementDictonary;

@end
