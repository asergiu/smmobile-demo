//
//  WrapMarkersData.m
//  PDdatabase
//
//  Created by Владимир Малашенков on 09.03.13.
//

#import "WrapMarkersData.h"
#import "PDMDataManager.h"

@implementation WrapMarkersData

@synthesize xLeftWrap, xRightWrap;
@synthesize yLeftWrap, yRightWrap;

@end
