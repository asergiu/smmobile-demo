//
//  StoreData.h
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 07.04.13.
//
//

#import <Foundation/Foundation.h>

@interface StoreData : NSObject

@property (nonatomic, strong) NSString *ECPName;
@property (nonatomic, strong) NSString *ECPID;
@property (nonatomic, strong) NSString *storeID;
@property (nonatomic, strong) NSString *code;

@end
