//
//  SessionManager.m
//  PDdatabase
//

#import "SessionManager.h"
#import "DatabaseRequests.h"
#import "DatabaseRequestsData.h"

#import "UIDevice+IdentifierAddition.h"
#import "FileHelper.h"
#import "NSString+Hash.h"
#import "NSArray+Grouped.h"
#import "AmazonHelper.h"
#import "NSDate-Utilities.h"

#import "DoubleTextAlertView.h"
#import "SingleTextWithButtonAlertView.h"

#import "PDMDataManager.h"

#import "Reachability.h"
#import "UIWindow+VisibleController.h"
#import "PDConfigManager.h"

#import "SVProgressHUD.h"
#import "ShareSessionsManager.h"

#define kTypeJPEG [self typeStringFromType:0]
#define kTypePNG [self typeStringFromType:1]

#define SessionAlertTag 123321
#define SessionStoreAlertTag 123

#define bundleID [[NSUserDefaults standardUserDefaults] stringForKey:@"bundle"]

@interface SessionManager()
{
    BOOL syncAllowed;
    
    void (^_requestCompletion)(BOOL status, NSString *reason, NSMutableArray *response);
    
    UIAlertController *alertController;
}

- (void)addSessionToDBforSession:(Session *)session
                        toTarget:(SourceType)target;

- (void)addPhotosFromSessionToDBforSession:(Session *)session
                                  toTarget:(SourceType)target
                               synchronize:(BOOL)sync;

- (void)addPhotoToDB:(PhotoData *)photo
    photoSessionType:(PhotoSessionType)sessionType
          forSession:(Session *)session
            toTarget:(SourceType)target
         synchronize:(BOOL)sync;

- (void)addPhotoMarkersData:(MarkersData *)markers
            withSessionType:(SessionType)sesionType
                 forSession:(Session *)session
                   toTarget:(SourceType)target;

- (void)addFrameMarkersData:(FrameMarkersData *)frameMarkers
            withSessionType:(SessionType)sesionType
                 forSession:(Session *)session
                   toTarget:(SourceType)target;

- (void)addWrapMarkersData:(WrapMarkersData *)wrapMarkers
           withSessionType:(SessionType)sesionType
                forSession:(Session *)session
                  toTarget:(SourceType)target;

- (void)addResultsDataForSession:(Session *)session
                        toTarget:(SourceType)target;

@end

@implementation SessionManager

@synthesize currentSession;
@synthesize syncronizer;
@synthesize isSavedSession;

+ (SessionManager *)sharedSessionManager
{
    static dispatch_once_t onceToken = 0;
    static id _sharedInstance = nil;
    
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        syncronizer = [PDSessionsSyncronizer new];
    }
    
    return self;
}

#pragma mark - Local or Cloud target

- (BOOL)localTargetFromSettings
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"targets"];

    return ![[dict objectForKey:@"boolValue"] boolValue];
}

#pragma mark - Get current ECP

- (NSString *)currentECP
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *ecpString = [[NSUserDefaults standardUserDefaults] objectForKey:@"ECP_name"];
    
    return ecpString;
}

#pragma mark - Checked store code

- (NSString *)checkedStoreCode
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString * storeNameString = [[NSUserDefaults standardUserDefaults] objectForKey:@"store_name_checked"];
    
    return storeNameString;
}

- (void)saveCheckedStoreCode:(NSString *)storeId
{
    [[NSUserDefaults standardUserDefaults] setObject:storeId forKey:@"store_name_checked"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)removeCheckedStoreCode:(NSString *)storeId
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"store_name_checked"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Get current Store code

- (void)checkCurrentStoreCode
{    
    [PD_API_MANAGER requestExistsStoreCode:[self currentStoreCode]
                            withCompletion:^(BOOL success, NSString *message, NSDictionary *response){
                            
                                if (success)
                                {
                                    [self saveCheckedStoreCode:[self currentStoreCode]];
                                    [self showNameAlert];
                                }
                                else
                                {
                                    [self showIncorrectStoreCodeError];
                                }
                            }];
}

- (NSString *)currentStoreCode
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *storeNameString = [[NSUserDefaults standardUserDefaults] objectForKey:@"store_name"];
    
    return storeNameString;
}

- (void)saveStoreCode:(NSString *)storeId
{
    [[NSUserDefaults standardUserDefaults] setObject:storeId forKey:@"store_name"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)hasStoreDataInSettings
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *storeCodeString = [self currentStoreCode];

    BOOL hasAllData = [self stringIsNotNil:storeCodeString];
    
    return hasAllData;
}

- (BOOL)stringIsNotNil:(NSString *)string
{
    return [string stringByReplacingOccurrencesOfString:@" " withString:@""].length;
}

- (BOOL)networkAvaliable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    return (networkStatus == NotReachable) ? NO : YES;
}

#pragma mark - Alerts

- (void)showSuccessfulStoreAlertLocal
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Done", @"")
                                                    message:NSLocalizedString(@"Session saved in local storage", @"")
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil];
    [alert performSelector:@selector(show)
                withObject:nil
                afterDelay:1.5f];
}

- (void)showSuccessfulStoreAlertServer
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Done", @"")
                                                    message:NSLocalizedString(@"Session saved on the cloud", @"")
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil];
    [alert performSelector:@selector(show)
                withObject:nil
                afterDelay:1.5f];
}

- (void)showIncorrectStoreCodeError
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                    message:NSLocalizedString(@"Incorrect store code", @"")
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil];
    
    [alert performSelector:@selector(show) withObject:nil afterDelay:0.5f];
}

- (void)showNotSelectedStoreError
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                    message:NSLocalizedString(@"Store code error", nil)
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)showServerErrorAlertWithMessage:(NSString *)message title:(NSString *)title
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)showNotSelectedStoreErrorWithFillCompletion:(void (^)(BOOL finished))completion
{
    _storeCompletion = [completion copy];
    
    __weak __typeof(self)weakSelf = self;

    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Store code error", @"")
                                                          message:nil
                                                   preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"")
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             
                                                             _storeCompletion = NULL;
                                                             _nameCompletion = NULL;
                                                             alertController = nil;
                                                             
                                                         }];
    
    UIAlertAction *OKButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"")
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         
                                                         _storeCompletion();
                                                         
                                                         [SESSION_MANAGER saveStoreCode:alertController.textFields[0].text];
                                                         
                                                         _storeCompletion = NULL;
                                                         alertController = nil;

                                                     }];
    
    UIAlertAction *contactButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Store id contact", @"")
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              
                                                              [weakSelf sendRequestStoreIdMail];
                                                              
                                                              _storeCompletion = NULL;
                                                              _nameCompletion = NULL;
                                                              alertController = nil;
                                                              
                                                          }];
    
    [alertController addAction:contactButton];
    [alertController addAction:OKButton];
    [alertController addAction:cancelButton];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"Store code", @"");
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.text = [weakSelf currentStoreCode];
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    }];
    
    [[UIApplication.sharedApplication.windows[0] visibleViewController] presentViewController:alertController
                                                                                     animated:YES
                                                                                   completion:nil];
}

- (void)showNameAlertFromViewController:(UIViewController*)viewController fillCompletion:(void (^)(BOOL finished))completion
{
    _nameCompletion = [completion copy];
    
    if (![self networkAvaliable] && ![self localTargetFromSettings])
    {
        [self showConnectionProblemsAlert];
        return;
    }
    
    if (![self localTargetFromSettings])
    {
        if (![self hasStoreDataInSettings])
        {
            [self showNotSelectedStoreErrorWithFillCompletion:^(BOOL finished) {
                [self checkCurrentStoreCode];
            }];
        }
        else
        {
            [self checkCurrentStoreCode];
        }
        
    }
    else
    {
        [self showNameAlert];
    }
}

- (void)showConnectionProblemsAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                    message:NSLocalizedString(@"There are problems connecting to the cloud session storage. Please check your network and Internet connection.", @"")
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil];
    
    [alert performSelector:@selector(show) withObject:nil afterDelay:0.5f];
}

- (void)showNameAlert
{
    if (IS_IOS8_AND_LATER)
    {
        __weak __typeof(self)weakSelf = self;
        
        alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Customer name", @"")
                                                              message:nil
                                                       preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"SAVE", @"")
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                                                       
                                                       _nameCompletion();
                                                       
                                                       [weakSelf updateCurrentSessionClientFirstName:alertController.textFields[0].text
                                                                                            lastName:alertController.textFields[1].text];
                                                       [weakSelf storeCurrentSessionToDB];
                                                       
                                                       _nameCompletion = NULL;
                                                       alertController = nil;

                                                   }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"")
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           
                                                           _nameCompletion = NULL;
                                                           alertController = nil;
                                                           
                                                       }];
        [alertController addAction:cancel];
        [alertController addAction:ok];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = NSLocalizedString(@"First Name", @"");
            textField.keyboardType = UIKeyboardTypeDefault;
            textField.text = SESSION_MANAGER.currentSession.clientFirstName;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        }];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = NSLocalizedString(@"Last Name", @"");
            textField.keyboardType = UIKeyboardTypeDefault;
            textField.text = SESSION_MANAGER.currentSession.clientLastName;
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        }];
        
        [[UIApplication.sharedApplication.windows[0] visibleViewController] presentViewController:alertController
                                                                                         animated:YES
                                                                                       completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Customer name", @"")
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                              otherButtonTitles:NSLocalizedString(@"SAVE", @""), nil];
        
        [alert setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
        [[alert textFieldAtIndex:1] setSecureTextEntry:NO];
        
        alert.tag = SessionAlertTag;
        
        [alert textFieldAtIndex:0].placeholder = NSLocalizedString(@"First Name", @"");
        [alert textFieldAtIndex:1].placeholder = NSLocalizedString(@"Last Name", @"");
        
        [alert textFieldAtIndex:0].text = SESSION_MANAGER.currentSession.clientFirstName;
        [alert textFieldAtIndex:1].text = SESSION_MANAGER.currentSession.clientLastName;
        
        [alert textFieldAtIndex:0].autocorrectionType = UITextAutocorrectionTypeNo;
        [alert textFieldAtIndex:1].autocorrectionType = UITextAutocorrectionTypeNo;
        
        [alert textFieldAtIndex:0].autocapitalizationType = UITextAutocapitalizationTypeSentences;
        [alert textFieldAtIndex:1].autocapitalizationType = UITextAutocapitalizationTypeSentences;
        
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == SessionAlertTag)
    {
        switch (buttonIndex) {
            case 1:
            {
                _nameCompletion();
                
                UIAlertView* currentAlert = (UIAlertView*)alertView;
                
                [self updateCurrentSessionClientFirstName:[currentAlert textFieldAtIndex:0].text
                                                 lastName:[currentAlert textFieldAtIndex:1].text];
                
                [self storeCurrentSessionToDB];
                
                _nameCompletion = NULL;
            }
                break;
                
            default:
                break;
        }
        
        return;
    }
}

#pragma mark Mail Methods

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
	[controller dismissViewControllerAnimated:YES completion:nil];
    
    if (result == MFMailComposeResultSent)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:NSLocalizedString(@"Your email has been sent to …", @"")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)sendRequestStoreIdMail
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    [picker setSubject:NSLocalizedString(@"Nform Contact", @"")];
    [picker setToRecipients:[NSArray arrayWithObject:NSLocalizedString(@"infos@opticvideo.com", @"")]];
    
    @try {
        [[UIApplication.sharedApplication.windows[0] visibleViewController] presentViewController:picker
                                                                                         animated:YES
                                                                                       completion:nil];
    }
    @catch (NSException *exception) {
        //
    }
    @finally {
        //
    }
}

#pragma mark - Save session Methods

- (void)storeCurrentSessionToDB
{
    if (currentSession)
    {
        isSavedSession = YES;
        
        if ([self localTargetFromSettings])
        {
            [self storeSessionToSQLite:currentSession];
            
            [self showSuccessfulStoreAlertLocal];
        }
        else
        {
            [self storeSessionToServer:currentSession];
        }
        
        currentSession = nil;
    }
}

- (void)storeSessionToSQLite:(Session *)session
{
    [self trimDatabase];
    
    [self addSessionToDBforSession:session toTarget:SourceTypeLocal];
        
    [self addPhotosFromSessionToDBforSession:session toTarget:SourceTypeLocal synchronize:NO];
    
    [self addPhotoMarkersData:session.farMarkers withSessionType:SessionTypeFarPD forSession:session toTarget:SourceTypeLocal];
    [self addPhotoMarkersData:session.nearMarkers withSessionType:SessionTypeNearPD forSession:session toTarget:SourceTypeLocal];
    
    [self addFrameMarkersData:session.farFrameMarkers withSessionType:SessionTypeFarPD forSession:session toTarget:SourceTypeLocal];
    [self addFrameMarkersData:session.nearFrameMarkers withSessionType:SessionTypeNearPD forSession:session toTarget:SourceTypeLocal];

    // Wrap markers
    [self addWrapMarkersData:session.farWrapMarkers withSessionType:SessionTypeFarPD forSession:session toTarget:SourceTypeLocal];
    [self addWrapMarkersData:session.nearWrapMarkers withSessionType:SessionTypeNearPD forSession:session toTarget:SourceTypeLocal];
    
    [self addResultsDataForSession:session toTarget:SourceTypeLocal];
    
    [self addStoreDataForSession:session toTarget:SourceTypeLocal];
}

- (void)storeSessionToServer:(Session *)session
{    
    if (session)
    {        
        [self trimDatabase];
        
        [self addSessionToDBforSession:session toTarget:SourceTypeLocal];
        
        [self addPhotosFromSessionToDBforSession:session toTarget:SourceTypeServer synchronize:NO];
        
        [self addPhotoMarkersData:session.farMarkers withSessionType:SessionTypeFarPD forSession:session toTarget:SourceTypeLocal];
        [self addPhotoMarkersData:session.nearMarkers withSessionType:SessionTypeNearPD forSession:session toTarget:SourceTypeLocal];
        
        [self addFrameMarkersData:session.farFrameMarkers withSessionType:SessionTypeFarPD forSession:session toTarget:SourceTypeLocal];
        [self addFrameMarkersData:session.nearFrameMarkers withSessionType:SessionTypeNearPD forSession:session toTarget:SourceTypeLocal];
        
        // Wrap markers
        [self addWrapMarkersData:session.farWrapMarkers withSessionType:SessionTypeFarPD forSession:session toTarget:SourceTypeLocal];
        [self addWrapMarkersData:session.nearWrapMarkers withSessionType:SessionTypeNearPD forSession:session toTarget:SourceTypeLocal];
        
        [self addResultsDataForSession:session toTarget:SourceTypeLocal];
        
        [self markAsServerTypeSession:session];
        
        [self addStoreDataToSession:session];
        
        [self addStoreDataForSession:session toTarget:SourceTypeLocal];
        
        [PD_API_MANAGER requestSaveSessionDict:[Session createDictFromSession:session]
                                withCompletion:^(BOOL success, NSString *message, NSDictionary *response){
                                    
                                    if (success)
                                    {
                                        [self showSuccessfulStoreAlertServer];
                                    }
                                    else
                                    {
                                        [self showServerErrorAlertWithMessage:message title:NSLocalizedString(@"Error", nil)];
                                    }
                                }];
    }
}

- (void)markAsServerTypeSession:(Session *)session
{
    if (session)
    {
        [DatabaseRequests updateTypeForSessionId:session.sessionId newType:kDBTypeServer toTarget:SourceTypeLocal];
    }
}

#pragma mark - Local session
#pragma mark CREATE with Meashurements dictonary

- (void)createSessionFromDataManager
{
    PDMDataManager * currentDataManager = [PDMDataManager sharedManager];
    
    [self createLocalSessionWithType:SessionTypeNearPD clientFirstName:nil clientLastName:nil];
    
    [currentSession fillAngleValuesFromDataManager:currentDataManager];
        
    if (currentDataManager.farPDImage)
    {
        [self addLocalPhoto:currentDataManager.farPDImageRotated forType:PhotoTypeJPEG forSesionType:SessionTypeFarPD];
        
        [self addLocalPhotoMarkersData:[MarkersData markersDataFromMeasurementDictonary:currentDataManager.storage
                                                                               dataType:SessionTypeFarPD]
                         forSesionType:SessionTypeFarPD];
        
        [self addLocalFrameMarkersData:[FrameMarkersData frameMarkersDataFromMeasurementDictonary:currentDataManager.storage dataType:SessionTypeFarPD]
                         forSesionType:SessionTypeFarPD];

         //[self addLocalWrapMarkersData:[WrapMarkersData wrapMarkersDataFromMeasurementDictonary:currentDataManager.storage dataType:SessionTypeFarPD] forSesionType:SessionTypeFarPD];
    }
        
    if (currentDataManager.nearPDImage)
    {
        [self addLocalPhoto:currentDataManager.nearPDImageRotated forType:PhotoTypeJPEG forSesionType:SessionTypeNearPD];
        
        [self addLocalPhotoMarkersData:[MarkersData markersDataFromMeasurementDictonary:currentDataManager.storage dataType:SessionTypeNearPD]
                         forSesionType:SessionTypeNearPD];
        
        [self addLocalFrameMarkersData:[FrameMarkersData frameMarkersDataFromMeasurementDictonary:currentDataManager.storage dataType:SessionTypeNearPD]
                         forSesionType:SessionTypeNearPD];
        
        //[self addLocalWrapMarkersData:[WrapMarkersData wrapMarkersDataFromMeasurementDictonary:currentDataManager.storage dataType:SessionTypeNearPD] forSesionType:SessionTypeNearPD];
    }
}

#pragma mark 1 STEP - CREATE

- (void)createLocalSessionWithType:(SessionType)type
                   clientFirstName:(NSString *)firstName
                    clientLastName:(NSString *)lastName
{
    currentSession = [Session new];
    
    currentSession.type = type;
    
    if (firstName)
        currentSession.clientFirstName = firstName;
    if (lastName)
        currentSession.clientLastName = lastName;
    
    [self addAdditionalDataToSession:currentSession];
    
    [self addUseIpadFlash:currentSession];
    
    //store data
    [self addStoreDataToSession:currentSession];
    
    [currentSession updateLastUpdateDate];
}

- (void)addStoreDataToSession:(Session *)session
{
//    if (session.sessionFromCurrentStore)
//    {
        //store data
        StoreData * storeData = [StoreData new];
        
        storeData.ECPName = [self currentECP];
        storeData.code = [self currentStoreCode];
        
        session.storeData = storeData;
    //}
}

- (void)addAdditionalDataToSession:(Session *)session
{
    [session setAdditionalDataWith:@{ @"fred_type" : @([PDM_DATA getCurrentSessionSupportType]) }];
}

#pragma mark 2 STEP - TAKE PHOTO

- (void) addLocalPhoto:(UIImage *)image
               forType:(PhotoType)fileType
         forSesionType:(SessionType)type
{    
    if (type == SessionTypeNearPD)
    {
        [currentSession setNearPhotoImage:image];
        
        currentSession.nearPhoto.type = fileType;
    }
    
    else if (type == SessionTypeFarPD)
    {
        [currentSession setFarPhotoImage:image];
        
        currentSession.farPhoto.type = fileType;
    }
    
    [currentSession updateLastUpdateDate];
}

#pragma mark 3 STEP - ADD MARKERS

- (void)addLocalPhotoMarkersData:(MarkersData *)markers
                   forSesionType:(SessionType)type
{
    if (type == SessionTypeNearPD)
        currentSession.nearMarkers = markers;
    
    else if (type == SessionTypeFarPD)
        currentSession.farMarkers = markers;
    
    [currentSession updateLastUpdateDate];
}

- (void)addLocalFrameMarkersData:(FrameMarkersData *)frameMarkers
                   forSesionType:(SessionType)type
{
    if (type == SessionTypeNearPD)
        currentSession.nearFrameMarkers = frameMarkers;
    
    else if (type == SessionTypeFarPD)
        currentSession.farFrameMarkers = frameMarkers;
    
    [currentSession updateLastUpdateDate];
}

- (void)addLocalWrapMarkersData:(WrapMarkersData *)wrapMarkers
                  forSesionType:(SessionType)type
{
    if (type == SessionTypeNearPD)
        currentSession.nearWrapMarkers = wrapMarkers;
    
    else if (type == SessionTypeFarPD)
        currentSession.farWrapMarkers = wrapMarkers;
    
    [currentSession updateLastUpdateDate];
}

#pragma mark 4 STEP - ADD RESULTS

- (void)addLocalResultsData:(ResultsData *)resultsData
              forSesionType:(SessionType)type
{
    if (type == SessionTypeFarPD)
        currentSession.resultsDataFar = resultsData;
    else
        currentSession.resultsDataNear = resultsData;

    
    [currentSession updateLastUpdateDate];
}

- (void)addLocalResultsScreenshot:(UIImage *)screenshot
{
    [currentSession setScreenShotImage:screenshot];
    
    [currentSession updateLastUpdateDate];
}

#pragma mark 5 STEP - ADD STORE DATA

- (void)setStoreName:(NSString *)storeName
             address:(NSString *)storeAddress
               phone:(NSString *)storePhone
                mail:(NSString *)storeMail
{
    StoreData *storeData = [StoreData new];
    
    storeData.code = storeName;
    
    currentSession.storeData = storeData;
}

#pragma mark 6 STEP - ADD WRAP

- (void)addWrapData:(WrapMarkersData *)wrapData
      forSesionType:(SessionType)type
{
    if (type == SessionTypeFarPD)
        currentSession.farWrapMarkers = wrapData;
    else
        currentSession.nearWrapMarkers = wrapData;
}

#pragma mark STEP - ADD USE IPAD FLASH

- (void)addUseIpadFlash:(Session *)session
{
    [session setAdditionalDataWith:@{ @"ipad_flash" : @{ @"ipad_flash_type" : @([PDM_DATA getFlashType]) } }];
}

#pragma mark STEP - UPDATE USER

- (void)updateCurrentSessionClientFirstName:(NSString *)firstName
                                   lastName:(NSString *)lastName
{
    if ([[firstName noSpaceString] length] && [[lastName noSpaceString] length])
    {
        currentSession.clientFirstName = firstName;
        currentSession.clientLastName = lastName;
    }
    else if ([[firstName noSpaceString] length])
    {
        currentSession.clientFirstName = firstName;
        currentSession.clientLastName = @"";
    }
    else if ([[lastName noSpaceString] length])
    {
        currentSession.clientFirstName = @"";
        currentSession.clientLastName = lastName;
    }
    else
    {
        currentSession.clientFirstName = @"";
        currentSession.clientLastName = @"";
    }
}

#pragma mark - DB session
#pragma mark CREATE

- (void)addSessionToDBforSession:(Session *)session toTarget:(SourceType)target
{
    NSLog(@"SESSION CREATED");
    
    [DatabaseRequests sessionInsertRequestWithSessionid:session.sessionId
                                            sessionType:[self typeStringFromType: session.type]
                                             serverType:session.allreadyOnServer
                                        clientFirstName:session.clientFirstName
                                         clientLastName:session.clientLastName
                                             macAddress:session.macAddress
                                                storeId:session.storeData.storeID
                                               bundleId:bundleID
                                         additionalData:[session getAdditionalDataAsJSONString]
                                                   date:session.dateString
                                             updateDate:session.lastUpdateDateString
                                          markAsDeleted:NO
                                               toTarget:target];
}

#pragma mark PHOTO

- (void)addPhotosFromSessionToDBforSession:(Session *)session
                                  toTarget:(SourceType)target
                               synchronize:(BOOL)sync
{
    [self addPhotoToDB:session.nearPhoto photoSessionType:PhotoSessionTypeNear forSession:session toTarget:target synchronize:sync];
    [self addPhotoToDB:session.farPhoto photoSessionType:PhotoSessionTypeFar forSession:session toTarget:target synchronize:sync];
    [self addPhotoToDB:session.screenShot photoSessionType:PhotoSessionTypeScreenshot forSession:session toTarget:target synchronize:sync];
}

- (void)addPhotoToDB:(PhotoData *)photo photoSessionType:(PhotoSessionType)sessionType
          forSession:(Session *)session
            toTarget:(SourceType)target
         synchronize:(BOOL)sync
{
    if (!photo || [photo.photoID isEqualToString:UUID_NULL])
    {
        NSLog(@"NO PHOTO");
        return;
    }
    
    PhotoType type = photo.type;
    
    NSData * photoData = (type == PhotoTypeJPEG) ? UIImageJPEGRepresentation(photo.image, 0.85f) : UIImagePNGRepresentation(photo.image);
    
    NSString * fileName = photo.filename;
    
    NSString * nameWithExt = [self photoFileNameForName:fileName forType:type];
    
    [DatabaseRequests addPhotoUpdateRequestWithSessionid:session.sessionId
                                                photoUID:photo.photoID
                                               photoType:[self typeStringFromType:type]
                                                fileName:fileName
                                        photoSessionType:sessionType
                                              updateDate:session.lastUpdateDateString
                                                toTarget:SourceTypeLocal];
    
    NSString * fullPath = [self fullPhotoFileNameFromFileName:fileName forType:type];
    [self storePhotoWithFullPath:fullPath data:photoData];
    
    if (target == SourceTypeServer)
    {
        [AmazonHelper sendImageWithData:photoData andName:nameWithExt synchronize:sync];
    }
}

- (void) addPhotoMarkersData:(MarkersData*)markers withSessionType:(SessionType)sesionType forSession:(Session*)session toTarget:(SourceType)target
{
     NSString * dataId = (sesionType == SessionTypeFarPD) ? session.farDataId : session.nearDataId;
    
    if (!markers || [dataId isEqualToString:UUID_NULL])
        return;
    
    double snappedAngle = (sesionType == SessionTypeFarPD) ? session.snappedAngleFar : session.snappedAngleNear;

    //set session ID for data row
    [DatabaseRequests updateSessionDataID:dataId
                            withSessionId:session.sessionId
                              sessionType:sesionType
                               updateDate:[DatabaseRequestsData dateStringFromDate:session.lastUpdateDate]
                                 toTarget:target];
    
    //add data from MarkersData instance
    
    [DatabaseRequests addMarkersDataForSessionId:dataId
                                     deviceAngle:[self stringFromDouble:snappedAngle]
                                           leftX:[self stringFromDouble:markers.xLeftSupport]
                                           leftY:[self stringFromDouble:markers.yLeftSupport]
                                         centerX:[self stringFromDouble:markers.xCenterSupport]
                                         centerY:[self stringFromDouble:markers.yCenterSupport]
                                          rightX:[self stringFromDouble:markers.xRightSupport]
                                          rightY:[self stringFromDouble:markers.yRightSupport]
                                        eyeLeftX:[self stringFromDouble:markers.xLeftEye]
                                        eyeLeftY:[self stringFromDouble:markers.yLeftEye]
                                       eyeRightX:[self stringFromDouble:markers.xRightEye]
                                       eyeRightY:[self stringFromDouble:markers.yRightEye]
                                        toTarget:target];
    NSLog(@"MARKERS ADDED");
}

- (void) addFrameMarkersData:(FrameMarkersData*)frameMarkers withSessionType:(SessionType)sesionType forSession:(Session*)session toTarget:(SourceType)target
{
    NSString * dataId = (sesionType == SessionTypeFarPD) ? session.farDataId : session.nearDataId;

    if (!frameMarkers || [dataId isEqualToString:UUID_NULL])
        return;
    
    //set session ID for data row
    [DatabaseRequests updateSessionDataID:dataId withSessionId:session.sessionId sessionType:sesionType updateDate:session.lastUpdateDateString toTarget:target];
    
    //add data from FrameMarkersData instance
    
    [DatabaseRequests addFrameMarkersDataForSessionId:dataId
                                            xLeftRect:[self stringFromDouble:frameMarkers.xLeftRect]
                                            yLeftRect:[self stringFromDouble:frameMarkers.yLeftRect]
                                           xRightRect:[self stringFromDouble:frameMarkers.xRightRect]
                                           yRightRect:[self stringFromDouble:frameMarkers.yRightRect]
                                          xLeftUpRect:[self stringFromDouble:frameMarkers.xLeftUpRect]
                                          yLeftUpRect:[self stringFromDouble:frameMarkers.yLeftUpRect]
                                         xRightUpRect:[self stringFromDouble:frameMarkers.xRightUpRect]
                                         yRightUpRect:[self stringFromDouble:frameMarkers.yRightUpRect]
                                             toTarget:target];
}

- (void) addWrapMarkersData:(WrapMarkersData*)wrapMarkers withSessionType:(SessionType)sesionType forSession:(Session*)session toTarget:(SourceType)target
{
    NSString * dataId = (sesionType == SessionTypeFarPD) ? session.farDataId : session.nearDataId;
    
    if (!wrapMarkers || [dataId isEqualToString:UUID_NULL])
        return;
    
    //set session ID for data row
    [DatabaseRequests updateSessionDataID:dataId withSessionId:session.sessionId sessionType:sesionType updateDate:session.lastUpdateDateString toTarget:target];
    
    //add data from FrameMarkersData instance
    
    [DatabaseRequests addWrapMarkersDataForSessionId:dataId
                                           xLeftWrap:[self stringFromDouble:wrapMarkers.xLeftWrap]
                                           yLeftWrap:[self stringFromDouble:wrapMarkers.yLeftWrap]
                                          xRightWrap:[self stringFromDouble:wrapMarkers.xRightWrap]
                                          yRightWrap:[self stringFromDouble:wrapMarkers.yRightWrap]
                                            toTarget:target];
}

- (void) addStoreDataForSession:(Session*)session toTarget:(SourceType)target
{
    [DatabaseRequests storeInsertRequestToSessionid:session.sessionId withStoreName:session.storeData.code address:nil phone:nil mail:nil toTarget:target];
}

- (void) addResultsDataForSession:(Session*)session toTarget:(SourceType)target
{
    BOOL hasFarData = session.resultsDataFar ? YES : NO;
    BOOL hasNearData = session.resultsDataNear ? YES : NO;
    
    if (!hasFarData && !hasNearData)
        return;
    
    //set session ID for results row
    [DatabaseRequests updateResultsDataIDWithSessionId:session.sessionId updateData:session.lastUpdateDateString toTarget:target];
    
    [DatabaseRequests addResultsDataForSessionId:session.sessionId
                                     sessionType:session.type
                                          pdLeft:hasFarData ? [self stringFromDouble:session.resultsDataFar.pdLeft] : @"0"
                                         pdRight:hasFarData ? [self stringFromDouble:session.resultsDataFar.pdRight] : @"0"
                                      heightLeft:hasFarData ? [self stringFromDouble:session.resultsDataFar.heightLeft] : @"0"
                                     heightRight:hasFarData ? [self stringFromDouble:session.resultsDataFar.heightRight] : @"0"
                                      frameWidth:hasFarData ? [self stringFromDouble:session.resultsDataFar.frameWidth] : @"0"
                                      frameHight:hasFarData ? [self stringFromDouble:session.resultsDataFar.frameHight] : @"0"
                                     frameBridge:hasFarData ? [self stringFromDouble:session.resultsDataFar.frameBridge] : @"0"
                                   photoDistance:hasFarData ? [self stringFromDouble:session.resultsDataFar.photoDistance] : @"0"
                                      pantoAngle:hasFarData ? [self stringFromDouble:session.resultsDataFar.pantoAngle] : @"0"
                                 readingDistance:hasFarData ? [self stringFromDouble:session.resultsDataFar.readindDistance] : @"0"
                                       wrapAngle:hasFarData ? [self stringFromDouble:session.resultsDataFar.wrapAngle] : @"-5"
                                      pdLeftNear:hasNearData ? [self stringFromDouble:session.resultsDataNear.pdLeft] : @"0"
                                     pdRightNear:hasNearData ? [self stringFromDouble:session.resultsDataNear.pdRight] : @"0"
                                  heightLeftNear:hasNearData ? [self stringFromDouble:session.resultsDataNear.heightLeft] : @"0"
                                 heightRightNear:hasNearData ? [self stringFromDouble:session.resultsDataNear.heightRight] : @"0"
                                 frameBridgeNear:hasNearData ? [self stringFromDouble:session.resultsDataNear.frameBridge] : @"0"
                             readingDistanceNear:hasNearData ? [self stringFromDouble:session.resultsDataNear.readindDistance] : @"0"
                                        toTarget:target];
}




#pragma mark - get methods

- (void)fillDataManagerWithSession:(Session *)session
{
    PDMDataManager *currentDataManager = [PDMDataManager sharedManager];
    
    [session loadToDataManager:currentDataManager];
}

- (BOOL)localSessionsContainSession:(Session *)session
{
    NSArray * localDataArray = [SESSION_MANAGER sessionsFromTargetSource:SourceTypeLocal];
    
    for (Session * oneSession in localDataArray)
    {
        if ([oneSession.sessionId isEqualToString:session.sessionId])
            return YES;
    }
    
    return NO;
}

- (NSArray *)sessionsFromTargetSource:(SourceType)target
{
    NSArray * dataArray = [self arrayOfDataForSessionFromTargetSource:target];
    
    if (!dataArray)
        return nil;
    
    NSMutableArray * finalArray = [NSMutableArray arrayWithCapacity:dataArray.count];
    
    for (NSDictionary * oneDataDict in dataArray)
    {
        Session * session = [Session createFromSessionDict:oneDataDict];
        session.source = target;
        
        [finalArray addObject:session];
    }
    
    return finalArray;
}

- (Session *)sessionForId:(NSString *)stringId
{
    NSDictionary *sessionData = [DatabaseRequests getSessionForId:stringId];
    
    return [Session createFromSessionDict:sessionData];
}

- (NSArray *)sessionsGroupedByDaysFromTargetSource:(SourceType)target
{
    return [[self sessionsFromTargetSource:target] groupUsingBlock:^NSString *(Session* object)
    {
       return object.humanReadableData;
    }];
}

- (NSArray *)sessionsGroupedByNameLetterFromTargetSource:(SourceType)target
{
    return [[self sessionsFromTargetSource:target] groupUsingBlock:^NSString *(Session* object) {
        
        return (object.clientLastName.length) ? [object.clientLastName substringToIndex:1] : nil;
    }];
}

- (NSArray *)arrayOfDataForSessionFromTargetSource:(SourceType)target
{
     return [DatabaseRequests sessionArrayForStoreId:[self currentStoreCode] sorceType:target];
}

- (NSDictionary *)sortedByLetterSectionsFromTargetSource:(SourceType)target
{
    NSArray *input = [self sessionsGroupedByNameLetterFromTargetSource:target];
    
    NSMutableDictionary *finalDict = [NSMutableDictionary dictionary];
    
    for (NSArray *oneGroup in input)
    {
        Session *oneSession = [oneGroup objectAtIndex:0];
        
        NSString *key = [oneSession.clientLastName substringToIndex:1];
        
        if (![finalDict objectForKey:key])
            [finalDict setObject:[NSMutableArray arrayWithObject:oneGroup] forKey:key];
        else
        {
            NSMutableArray *current = [finalDict objectForKey:key];
            [current addObject:oneGroup];
        }
    }
    
    return finalDict;
}

- (NSDictionary *)sortedBySectionsArraysFromTargetSource:(SourceType)target
{
    NSArray * input = [self sessionsGroupedByDaysFromTargetSource:target];
    
    NSMutableDictionary * finalDict = [NSMutableDictionary dictionary];
    
    for (NSArray * oneGroup in input)
    {
        int counter = 2;
        
        Session * oneSession = [oneGroup objectAtIndex:0];
        
        NSDate * lastUpdate = oneSession.lastUpdateDate;
        
        if([lastUpdate isToday])
            counter = 0;
        else if([lastUpdate isThisWeek])
            counter = 1;
        else if([lastUpdate isLastWeek])
            counter = 2;

        NSString * key = [NSString stringWithFormat:@"%i", counter];
        
        if (![finalDict objectForKey:key])
            [finalDict setObject:[NSMutableArray arrayWithObject:oneGroup] forKey:key];
        else
        {
            NSMutableArray * current = [finalDict objectForKey:key];
            [current addObject:oneGroup];
        }
    }
    
    return finalDict;
}

- (void)fillSession:(Session *)session forSourceType:(SourceType)source
{
    NSString * farPhotoId = session.farPhoto.photoID;
    NSString * nearPhotoId = session.nearPhoto.photoID;
    NSString * screenshotId = session.screenShot.photoID;
    
    NSString * farPhotoFilename = [self photoFileNameForPhotoId:farPhotoId source:source];
    NSString * nearPhotoFilename = [self photoFileNameForPhotoId:nearPhotoId source:source];
    NSString * screenshotPhotoFilename = [self photoFileNameForPhotoId:screenshotId source:source];
    
    NSString * farDataId = session.farDataId ;
    NSString * nearDataId = session.nearDataId;
    
    NSString * resultsId = session.resultsDataFar.dataId;
    
    if (!resultsId)
        resultsId = session.resultsDataNear.dataId;
    
    session.farPhoto.filename = farPhotoFilename;
    session.nearPhoto.filename = nearPhotoFilename;
    session.screenShot.filename = screenshotPhotoFilename;
    
    [session setFarPhotoImage: [self photoForPhotoFilename:farPhotoFilename source:source]];
    [session setNearPhotoImage: [self photoForPhotoId:nearPhotoId source:source]];
    [session setScreenShotImage: [self photoForPhotoId:screenshotId source:source]];
    
    session.farPhoto.photoID = farPhotoId;
    session.nearPhoto.photoID = nearPhotoId;
    session.screenShot.photoID = screenshotId;
    
            
    NSLog(@"saved to mem image %@",farPhotoFilename);
    
    NSDictionary * allFarData = [DatabaseRequests dataDictForDataID:farDataId sorceType:SourceTypeLocal];
    NSDictionary * allNearData = [DatabaseRequests dataDictForDataID:nearDataId sorceType:SourceTypeLocal];
    
    NSDictionary * results = [DatabaseRequests resultsDictForResultsID:resultsId sorceType:SourceTypeLocal];
    
    for (int i = 0 ; i<=1 ; i++) //1-far 0-near
    {
        NSDictionary * data = i ? allFarData : allNearData;
        
        if (!data)
            continue;
        
        if ([data count])
        {
            MarkersData * photoMarkers = [MarkersData new];
            photoMarkers.xLeftSupport = [[data objectForKey:kTableRowData_MarkerSupportLX] doubleValue];
            photoMarkers.yLeftSupport = [[data objectForKey:kTableRowData_MarkerSupportLY] doubleValue];
            photoMarkers.xRightSupport = [[data objectForKey:kTableRowData_MarkerSupportRX] doubleValue];
            photoMarkers.yRightSupport = [[data objectForKey:kTableRowData_MarkerSupportRY] doubleValue];
            photoMarkers.xCenterSupport = [[data objectForKey:kTableRowData_MarkerSupportCX] doubleValue];
            photoMarkers.yCenterSupport = [[data objectForKey:kTableRowData_MarkerSupportCY] doubleValue];
            photoMarkers.xLeftEye = [[data objectForKey:kTableRowData_MarkerEyeLX] doubleValue];
            photoMarkers.yLeftEye = [[data objectForKey:kTableRowData_MarkerEyeLY] doubleValue];
            photoMarkers.xRightEye = [[data objectForKey:kTableRowData_MarkerEyeRX] doubleValue];
            photoMarkers.yRightEye = [[data objectForKey:kTableRowData_MarkerEyeRY] doubleValue];
            
            FrameMarkersData * frameMarkers = [FrameMarkersData new];
            frameMarkers.xLeftRect = [[data objectForKey:kTableRowData_FrameLX] doubleValue];
            frameMarkers.yLeftRect = [[data objectForKey:kTableRowData_FrameLY] doubleValue];
            frameMarkers.xLeftUpRect = [[data objectForKey:kTableRowData_FrameULX] doubleValue];
            frameMarkers.yLeftUpRect = [[data objectForKey:kTableRowData_FrameULY] doubleValue];
            frameMarkers.xRightRect = [[data objectForKey:kTableRowData_FrameRX] doubleValue];
            frameMarkers.yRightRect = [[data objectForKey:kTableRowData_FrameRY] doubleValue];
            frameMarkers.xRightUpRect = [[data objectForKey:kTableRowData_FrameURX] doubleValue];
            frameMarkers.yRightUpRect = [[data objectForKey:kTableRowData_FrameURY] doubleValue];
            
            WrapMarkersData * wrapMarkers = [WrapMarkersData new];
            wrapMarkers.xLeftWrap = [[data objectForKey:kTableRowData_WrapLX] doubleValue];
            wrapMarkers.yLeftWrap = [[data objectForKey:kTableRowData_WrapLY] doubleValue];
            wrapMarkers.xRightWrap = [[data objectForKey:kTableRowData_WrapRX] doubleValue];
            wrapMarkers.yRightWrap = [[data objectForKey:kTableRowData_WrapRY] doubleValue];
            
            ResultsData * resultsData = [ResultsData new];
            resultsData.dataId = resultsId;
            resultsData.pdLeft = [[results objectForKey:kTableRowResults_FarPDL] doubleValue];
            resultsData.pdRight = [[results objectForKey:kTableRowResults_FarPDR] doubleValue];
            resultsData.heightLeft = [[results objectForKey:kTableRowResults_FarHL] doubleValue];
            resultsData.heightRight = [[results objectForKey:kTableRowResults_FarHL] doubleValue];
            resultsData.frameWidth = [[results objectForKey:kTableRowResults_FarFW] doubleValue];
            resultsData.frameHight = [[results objectForKey:kTableRowResults_FarFH] doubleValue];
            resultsData.frameBridge = [[results objectForKey:kTableRowResults_FarFBridge] doubleValue];
            resultsData.photoDistance = [[results objectForKey:kTableRowResults_FarPhotoDist] doubleValue];
            resultsData.pantoAngle = [[results objectForKey:kTableRowResults_FarPanto] doubleValue];
            
            resultsData.wrapAngle = [[results objectForKey:kTableRowResults_WrapAngle] doubleValue];
            
            double angle = [[data objectForKey:kTableRowData_iPadAngle] doubleValue];
            
            if (i)
            {
                session.resultsDataFar = resultsData;
                
                session.farMarkers = photoMarkers;
                session.farFrameMarkers = frameMarkers;
                session.farWrapMarkers = wrapMarkers;
                
                session.snappedAngleFar = angle;
            }
            else
            {
                session.resultsDataNear = resultsData;
                
                session.nearMarkers = photoMarkers;
                session.nearFrameMarkers = frameMarkers;
                session.nearWrapMarkers = wrapMarkers;
                
                session.snappedAngleNear = angle;
            }
        }
    }
}

- (Session *)sessionFromDBAtIndex:(NSInteger)index inSection:(NSInteger)section
{
    NSDictionary * sessionDict = [[[self sessionsGroupedByDaysFromTargetSource:SourceTypeLocal] objectAtIndex:section] objectAtIndex:index];
    
    SessionType type = [[sessionDict objectForKey:@"SESSION_TYPE"] intValue];
    
    NSString * farDataId = [sessionDict objectForKey:@"SESSION_FAR_DATA_ID"];
    NSString * nearDataId = [sessionDict objectForKey:@"SESSION_NEAR_DATA_ID"];
    
    NSString * farPhotoId = [sessionDict objectForKey:@"SESSION_PHOTO_FAR"];
    NSString * nearPhotoId = [sessionDict objectForKey:@"SESSION_PHOTO_NEAR"];
    
    NSString * resultsId = [sessionDict objectForKey:@"SESSION_RESULTS_ID"];
    
    NSDictionary * allFarData = [DatabaseRequests dataDictForDataID:farDataId sorceType:SourceTypeLocal];
    NSDictionary * allNearData = [DatabaseRequests dataDictForDataID:nearDataId sorceType:SourceTypeLocal];
    
    NSDictionary * results = [DatabaseRequests resultsDictForResultsID:resultsId sorceType:SourceTypeLocal];
    
    NSLog(@"%@",allFarData);
    
    ///////-------
    
    Session * loadedSesison = [Session new];
    loadedSesison.loadedSession = YES;
    
    loadedSesison.type = type;
    
    [loadedSesison setFarPhotoImage: [self photoForPhotoId:farPhotoId source:SourceTypeLocal]];
    [loadedSesison setNearPhotoImage: [self photoForPhotoId:nearPhotoId source:SourceTypeLocal]];
    
    
    for (int i = 0 ; i<=1 ; i++) //1-far 0-near
    {
        NSDictionary * data = i ? allFarData : allNearData;
    
        if (!data)
            continue;
        
        MarkersData * photoMarkers = [MarkersData new];
        photoMarkers.xLeftSupport = [[data objectForKey:@"data_marker_support_left_x"] doubleValue];
        photoMarkers.yLeftSupport = [[data objectForKey:@"data_marker_support_left_y"] doubleValue];
        photoMarkers.xRightSupport = [[data objectForKey:@"data_marker_support_right_x"] doubleValue];
        photoMarkers.yRightSupport = [[data objectForKey:@"data_marker_support_right_y"] doubleValue];
        photoMarkers.xCenterSupport = [[data objectForKey:@"data_marker_support_center_x"] doubleValue];
        photoMarkers.yCenterSupport = [[data objectForKey:@"data_marker_support_center_y"] doubleValue];
        photoMarkers.xLeftEye = [[data objectForKey:@"data_marker_eye_left_x"] doubleValue];
        photoMarkers.yLeftEye = [[data objectForKey:@"data_marker_eye_left_y"] doubleValue];
        photoMarkers.xRightEye = [[data objectForKey:@"data_marker_eye_right_x"] doubleValue];
        photoMarkers.yRightEye = [[data objectForKey:@"data_marker_eye_right_y"] doubleValue];
        
        FrameMarkersData * frameMarkers = [FrameMarkersData new];
        frameMarkers.xLeftRect = [[data objectForKey:@"data_marker_frame_left_x"] doubleValue];
        frameMarkers.yLeftRect = [[data objectForKey:@"data_marker_frame_left_y"] doubleValue];
        frameMarkers.xLeftUpRect = [[data objectForKey:@"data_marker_frame_up_left_x"] doubleValue];
        frameMarkers.yLeftUpRect = [[data objectForKey:@"data_marker_frame_up_left_y"] doubleValue];
        frameMarkers.xRightRect = [[data objectForKey:@"data_marker_frame_right_x"] doubleValue];
        frameMarkers.yRightRect = [[data objectForKey:@"data_marker_frame_right_y"] doubleValue];
        frameMarkers.xRightUpRect = [[data objectForKey:@"data_marker_frame_up_right_x"] doubleValue];
        frameMarkers.yRightUpRect = [[data objectForKey:@"data_marker_frame_up_right_y"] doubleValue];
        
        WrapMarkersData * wrapMarkers = [WrapMarkersData new];
        wrapMarkers.xLeftWrap = [[data objectForKey:@"data_marker_wrap_left_x"] doubleValue];
        wrapMarkers.yLeftWrap = [[data objectForKey:@"data_marker_wrap_left_y"] doubleValue];
        wrapMarkers.xRightWrap = [[data objectForKey:@"data_marker_wrap_right_x"] doubleValue];
        wrapMarkers.yRightWrap = [[data objectForKey:@"data_marker_wrap_right_y"] doubleValue];

        ResultsData * resultsData = [ResultsData new];
        resultsData.pdLeft = [[results objectForKey:@"results_far_pd_left"] doubleValue];
        resultsData.pdRight = [[results objectForKey:@"results_far_pd_right"] doubleValue];
        resultsData.heightLeft = [[results objectForKey:@"results_far_height_left"] doubleValue];
        resultsData.heightRight = [[results objectForKey:@"results_far_height_right"] doubleValue];
        resultsData.frameWidth = [[results objectForKey:@"results_far_frame_width"] doubleValue];
        resultsData.frameHight = [[results objectForKey:@"results_far_frame_hright"] doubleValue];
        resultsData.frameBridge = [[results objectForKey:@"results_far_frame_bridge"] doubleValue];
        resultsData.photoDistance = [[results objectForKey:@"results_far_photo_distance"] doubleValue];
        resultsData.pantoAngle = [[results objectForKey:@"results_far_panto_angle"] doubleValue];
        
        
        if (i)
        {
            loadedSesison.resultsDataFar = resultsData;
            
            loadedSesison.farMarkers = photoMarkers;
            loadedSesison.farFrameMarkers = frameMarkers;
            loadedSesison.farWrapMarkers = wrapMarkers;
        }
        else
        {
            loadedSesison.resultsDataNear= resultsData;
            
            loadedSesison.nearMarkers = photoMarkers;
            loadedSesison.nearFrameMarkers = frameMarkers;
            loadedSesison.nearWrapMarkers = wrapMarkers;
        }
    }
    
    return loadedSesison;
}


- (UIImage *)photoForPhotoFilename:(NSString *)photoFilename source:(SourceType)target
{
    if (photoFilename)
    {
        if (target == SourceTypeLocal)
        {
            NSData * imageData = [FileHelper readFileWithName:photoFilename fromDirectory:[FileHelper imagesDirectory]];
            
            return [UIImage imageWithData:imageData];
        }
        else
        {
            UIImage * image = [AmazonHelper loadImageWithName:photoFilename];
            
            return image;
        }
    }
    
    return nil;
}

- (UIImage *)photoForPhotoId:(NSString *)photoId source:(SourceType)target
{
    NSString *fileName = [self photoFileNameForPhotoId:photoId source:target];
    
    return [self photoForPhotoFilename:fileName source:target];
}

- (NSString *)photoFileNameForPhotoId:(NSString *)photoId source:(SourceType)target
{
    NSDictionary * photoDict = [DatabaseRequests photoDictForPhotoID:photoId sorceType:target];
    
    if (photoDict)
    {
        NSString * fileName = [photoDict objectForKey:@"PHOTO_DATA"];
        
        if (![fileName pathExtension] || ![fileName pathExtension].length)
            fileName = [fileName stringByAppendingPathExtension:@"JPEG"];
        
        return fileName;
    }
    
    return nil;
}

#pragma mark - Remove Session methods

- (void)removeLastSession
{
    Session *session = [[self sessionsFromTargetSource:SourceTypeLocal] lastObject];
    [session fillAllData];
    
    [session deleteSession];
}

- (void)removeSession:(Session *)session targetSource:(SourceType)target
{
    if (!session)
        return;
    
    NSString * farPhotoId = session.farPhoto.photoID;
    NSString * nearPhotoId = session.nearPhoto.photoID;
    NSString * screenshotId = session.screenShot.photoID;
       
    NSString * nearPhotoFileName = [self photoFileNameForPhotoId:nearPhotoId source:SourceTypeLocal];
    NSString * farPhotoFileName = [self photoFileNameForPhotoId:farPhotoId source:SourceTypeLocal];
    NSString * screenshotFileName = [self photoFileNameForPhotoId:screenshotId source:SourceTypeLocal];
    
    [session fillAllData];
    
    //removing photo
    [self removePhotoWithFilename:nearPhotoFileName];
    [self removePhotoWithFilename:farPhotoFileName];
    [self removePhotoWithFilename:screenshotFileName];
    
    if (target == SourceTypeServer)
    {
        [PD_API_MANAGER requestDeleteSessionWithID:session.sessionId
                                withCompletion:^(BOOL success, NSString *message, NSDictionary *response){
                                    
                                    if (success)
                                    {
                                        [self removeAmazonPhotosWithNamesNear:nearPhotoFileName
                                                                          far:farPhotoFileName
                                                                   screenshot:screenshotFileName];
                                    }
                                    else
                                    {
                                        [self showServerErrorAlertWithMessage:message title:NSLocalizedString(@"Error", nil)];
                                    }
                                }];
    }
    
    //removing session
    [DatabaseRequests deleteSession:session sorceType:SourceTypeLocal];
}

- (void)removeAmazonPhotosWithNamesNear:(NSString *)nearPhotoFileName
                                    far:(NSString *)farPhotoFileName
                             screenshot:(NSString *)screenshotFileName
{    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        if (nearPhotoFileName)
            [AmazonHelper deleteImageWithName:nearPhotoFileName];
        
        if (farPhotoFileName)
            [AmazonHelper deleteImageWithName:farPhotoFileName];
        
        if (screenshotFileName)
            [AmazonHelper deleteImageWithName:screenshotFileName];
    });
}

- (BOOL)storePhotoWithFullPath:(NSString *)fullPath data:(NSData *)photoData
{
    return [FileHelper storeFileToPath:fullPath fileContent:photoData];
}

- (void)removePhotoWithFilename:(NSString *)filename
{
    [FileHelper removeFileWithName:filename fromDirectory:[FileHelper imagesDirectory]];
}

#pragma mark - Check Methods

- (BOOL)currentStoreIdIsChecked
{
    return [[self currentStoreCode] isEqualToString:[self checkedStoreCode]];
}

#pragma mark - common methods

- (NSString *)typeStringFromType:(int)type
{
    return [NSString stringWithFormat:@"%i", type ];
}

- (NSString *)stringFromDouble:(double)doubleValue
{
    return [NSString stringWithFormat:@"%f", doubleValue];
}

- (NSString *)fullPhotoFileNameFromFileName:(NSString*)fileName forType:(PhotoType)type
{
    NSString * fileNamePath = [[FileHelper imagesDirectory] stringByAppendingPathComponent: fileName];
    
    NSString * fullPath = [self photoFileNameForName:fileNamePath forType:type];
    
    return fullPath;
}

- (NSString *)photoFileNameForName:(NSString*)name forType:(PhotoType)type
{
    NSString * fileType = !type ? @"JPEG" : @"PNG";
    
    NSString * fullName = name;
    
    if (![fullName pathExtension] || ![fullName pathExtension].length)
        fullName = [name stringByAppendingPathExtension:fileType];
    
    return fullName;
}

#pragma mark - Syncronize Methods

- (void)currentStoreIdExistsThenStartSync
{
    [PD_API_MANAGER requestExistsStoreCode:[self currentStoreCode]
                            withCompletion:^(BOOL success, NSString *message, NSDictionary *response){
                                
                                if (success)
                                {
                                    [self saveCheckedStoreCode:[self currentStoreCode]];
                                    [self startSync];
                                }
                                else
                                {
                                    [self removeCheckedStoreCode:[self currentStoreCode]];
                                    [self showIncorrectStoreCodeError];
                                }
                            }];
}

- (void)updateDataWithError:(NSError **)error forViewController:(UIViewController *)controller
{
    if (![self networkAvaliable] && ![self localTargetFromSettings])
    {
        [self showConnectionProblemsAlert];
        return;
    }
    
    if (![self hasStoreDataInSettings])
    {
        [self showAlertAndTryToUpdateWith:error];
    }
    else
    {
        [self currentStoreIdExistsThenStartSync];
    }
}

- (void)showAlertAndTryToUpdateWith:(NSError **)error
{
    [self showNotSelectedStoreErrorWithFillCompletion:^(BOOL finished) {
        
        [self currentStoreIdExistsThenStartSync];
    }];
}

#pragma mark - Start Sync

- (int)getNewSessionsCount:(NSArray *)sessions
{
    int count = 0;
    
    for (Session *session in sessions)
    {
        if (!session.allreadyOnServer)
            count++;
    }
    
    return count;
}

- (void)startSync
{
    [syncronizer setSyncState:NSLocalizedString(@"Connecting to server...", nil)];
    
    syncAllowed = YES;
    
    NSArray *sessions = [self sessionsFromTargetSource:SourceTypeLocal];
    
    NSLog(@"New sessions count: %d", [self getNewSessionsCount:sessions]);
    
    [syncronizer setTotal:[self getNewSessionsCount:sessions]];

    dispatch_queue_t queue = dispatch_queue_create("prepare.block", NULL);
    
    dispatch_block_t sessionBlock = ^{
    
        NSMutableArray *requestSessions = [NSMutableArray new];
        
        for (Session *session in sessions)
        {
            [session fillAllData];
            
            if (!session.allreadyOnServer)
            {
                dispatch_queue_t main = dispatch_get_main_queue();
                dispatch_block_t block = ^
                {
                    [syncronizer increaseCurrent];
                };
                
                [self markAsServerTypeSession:session];
                
                [self addStoreDataToSession:session];
                
                [self addStoreDataForSession:session toTarget:SourceTypeLocal];
                
                [self addPhotosFromSessionToDBforSession:session
                                                toTarget:SourceTypeServer
                                             synchronize:YES];
                
                dispatch_sync(main, block);
            }
            
            [requestSessions addObject:[Session createDictFromSession:session]];
        }
        
        NSLog(@"sessions : %@", requestSessions);
        
        [self uploadData:requestSessions];
    };
    
    dispatch_async(queue, sessionBlock);
}

- (void)uploadData:(NSArray *)data
{
    [PD_API_MANAGER requestSyncSessions:data
                                storeId:[self currentStoreCode]
                         withCompletion:^(BOOL success, NSString *message, NSDictionary *response){
                             
                             if (success)
                             {
                                 [self saveSessionsFromServer:response];
                             }
                             else
                             {
                                 [self showServerErrorAlertWithMessage:message title:NSLocalizedString(@"Error", nil)];
                             }
                         }];
}

#pragma mark - Stop Sync

- (void)stopSync
{
    [[PD_API_MANAGER operationQueue] cancelAllOperations];
    
    syncAllowed = NO;
}

#pragma mark - Save session from Server when sync

- (void)saveSessionsFromServer:(NSDictionary *)dict
{
    NSArray *removedArray = [dict objectForKey:@"removed"];
    
    if (removedArray.count)
    {
        for (NSString *sessionId in removedArray)
        {
            Session *doomedSession = [self sessionForId:sessionId];
            
            [self removeSession:doomedSession targetSource:SourceTypeLocal];
        }
    }
    
    NSArray *sessionsArray = [dict objectForKey:@"sessions"];

    if (sessionsArray.count)
    {
        [syncronizer setTotal:(int)sessionsArray.count];
        
        dispatch_queue_t queue2 = dispatch_queue_create("sessions.block", NULL);
        
        dispatch_block_t sessionBlock = ^{
        
            for (NSDictionary *sessionDict in sessionsArray)
            {
                dispatch_queue_t main = dispatch_get_main_queue();
                dispatch_block_t block = ^
                {
                    [syncronizer increaseCurrent];
                };
                
                dispatch_sync(main, block);
                
                if (syncAllowed)
                {
                    Session *session = [Session createSessionFromServerDict:sessionDict];
                    
                    NSString *farPhotoFilename = session.farPhoto.filename;
                    NSString *nearPhotoFilename = session.nearPhoto.filename;
                    NSString *screenshotFilename = session.screenShot.filename;
                    
                    if (![farPhotoFilename pathExtension] || ![farPhotoFilename pathExtension].length)
                        farPhotoFilename = [farPhotoFilename stringByAppendingPathExtension:@"JPEG"];
                    
                    if (![nearPhotoFilename pathExtension] || ![nearPhotoFilename pathExtension].length)
                        nearPhotoFilename = [nearPhotoFilename stringByAppendingPathExtension:@"JPEG"];
                    
                    if (![screenshotFilename pathExtension] || ![screenshotFilename pathExtension].length)
                        screenshotFilename = [screenshotFilename stringByAppendingPathExtension:@"JPEG"];
                    
                    [session setFarPhotoImage:[self photoForPhotoFilename:farPhotoFilename source:SourceTypeServer]];
                    [session setNearPhotoImage:[self photoForPhotoFilename:nearPhotoFilename source:SourceTypeServer]];
                    [session setScreenShotImage:[self photoForPhotoFilename:screenshotFilename source:SourceTypeServer]];
                    
                    [self storeSessionToSQLite:session];
                }
            }
            
            dispatch_queue_t main = dispatch_get_main_queue();
            dispatch_block_t blockClear = ^
            {
                [syncronizer clear];
            };
            
            dispatch_sync(main, blockClear);
        };
        
        dispatch_async(queue2, sessionBlock);
    }
    else
    {
        [syncronizer clear];
    }
}

#pragma mark - Size

- (uint64_t)dataSize
{
    return [self databaseSize] + [self photosSize];
}

- (uint64_t)databaseSize
{
    NSString * persistentStorePath = SQLiteDB.databasePath;
    NSError * error = nil;
    NSDictionary * fileAttributes = [ [ NSFileManager defaultManager ] attributesOfItemAtPath : persistentStorePath error : &error ];
    return [ [ fileAttributes objectForKey : NSFileSize ] unsignedLongLongValue ];
}

- (uint64_t)photosSize
{
    NSError * error = nil;
    
    NSArray * contents;
    
    unsigned long long size = 0;
    
    NSString * path = [FileHelper imagesDirectory];
    
    NSFileManager * manager = [NSFileManager defaultManager];
    
    BOOL isDirectory;
    
    if ([manager fileExistsAtPath:path isDirectory:&isDirectory] && isDirectory)
    {
        contents = [manager subpathsAtPath:path];
    }
    else
    {
        contents = [NSArray array];
    }
    
    for (NSString * onePath in contents)
    {
        NSDictionary * fattrs = [manager attributesOfItemAtPath: [path stringByAppendingPathComponent:onePath] error:&error];
        size += [[fattrs objectForKey:NSFileSize] unsignedLongLongValue];
    }
    
    return size;
}

- (uint64_t)dataLimit
{
    NSString * storageSize = [ [ NSUserDefaults standardUserDefaults ] stringForKey : @"storage_size" ];
    
    if( [ storageSize isEqualToString : @"0.5" ] )
        return 512ull * 1024ull * 1024ull;
    else if( [ storageSize isEqualToString : @"1" ] )
        return 1024ull * 1024ull * 1024ull;
    else if( [ storageSize isEqualToString : @"3" ] )
        return 3ull * 1024ull * 1024ull * 1024ull;
    
    return 1024ull * 1024ull * 1024ull;
}

- (void)trimDatabase
{
    while([self dataSize] > [self dataLimit])
        [self removeLastSession];
}

#pragma mark - Share sessions

- (BOOL)isHaveSessionsForShare
{
    return [SESSION_MANAGER sessionsFromTargetSource:SourceTypeLocal].count > 0;
}

- (void)saveLocalSessionsForShare:(void(^)(BOOL sucsess))handler
{
    if ([self isHaveSessionsForShare])
    {
        NSArray *sessions = [SESSION_MANAGER sessionsFromTargetSource:SourceTypeLocal];
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            
            [sessions enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                Session *session = sessions[idx];
                [session fillAllData];
                
                NSDictionary *sessionDict = [Session createDictFromSession:session];
                NSDictionary *photosDict = [self getPhotosDictionary:sessionDict];
                
                NSMutableDictionary *fullDict = [NSMutableDictionary dictionaryWithDictionary:sessionDict];
                
                if (photosDict)
                    [fullDict addEntriesFromDictionary:@{ @"BASE64_PHOTO_DATA" : photosDict }];
                
                [SHARE_MANAGER writeSesionToFile:fullDict sessionsCount:sessions.count];
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (handler != NULL)
                    handler(YES);
            });
        });
    }
}

- (NSDictionary *)getPhotosDictionary:(NSDictionary *)session
{
    if (![session.allKeys containsObject:@"SESSION"])
        return nil;
    
    NSMutableDictionary *photosDict = [NSMutableDictionary new];
    
    // Far Photo
    if ([self isHavePhoto:session[@"SESSION"][@"SESSION_PHOTO_FAR"]])
    {
        NSString *base64Photo = [self convertPhotoToBase64:[SESSION_MANAGER photoForPhotoId:session[@"SESSION"][@"SESSION_PHOTO_FAR"]
                                                                                     source:SourceTypeLocal]];
        
        if (base64Photo.length > 0)
            [photosDict addEntriesFromDictionary:@{ @"FAR_PHOTO_DATA" : base64Photo }];
    }
    
    // Near Photo
    if ([self isHavePhoto:session[@"SESSION"][@"SESSION_PHOTO_NEAR"]])
    {
        NSString *base64Photo = [self convertPhotoToBase64:[SESSION_MANAGER photoForPhotoId:session[@"SESSION"][@"SESSION_PHOTO_NEAR"]
                                                                                     source:SourceTypeLocal]];
        
        if (base64Photo.length > 0)
            [photosDict addEntriesFromDictionary:@{ @"NEAR_PHOTO_DATA" : base64Photo }];
    }
    
    // Screenshot Photo
    if ([self isHavePhoto:session[@"SESSION"][@"SESSION_SCREENSHOT"]])
    {
        NSString *base64Photo = [self convertPhotoToBase64:[SESSION_MANAGER photoForPhotoId:session[@"SESSION"][@"SESSION_SCREENSHOT"]
                                                                                     source:SourceTypeLocal]];
        
        if (base64Photo.length > 0)
            [photosDict addEntriesFromDictionary:@{ @"SCREENSHOT_PHOTO_DATA" : base64Photo }];
    }
    
    return photosDict;
}

- (BOOL)isHavePhoto:(NSString *)photoID
{
    if (photoID.length > 0)
        return ![photoID isEqualToString:[DatabaseRequestsData nullUUID]];
    
    return NO;
}

- (NSString *)convertPhotoToBase64:(UIImage *)photo
{
    NSData *date = UIImageJPEGRepresentation(photo, 1.0);
    
    return [date base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

- (UIImage *)convertBase64ToPhoto:(NSString *)base64Str
{
    NSData *data = [[NSData alloc] initWithBase64EncodedString:base64Str
                                                       options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    return [UIImage imageWithData:data];
}

- (void)importSessionFromShare:(NSDictionary *)dict
{
    Session *session = [Session createSessionFromServerDict:dict];
    session.allreadyOnServer = NO;

    if ([dict.allKeys containsObject:@"BASE64_PHOTO_DATA"])
    {
        NSDictionary *base64Dict = dict[@"BASE64_PHOTO_DATA"];

        // Far Photo
        if ([base64Dict.allKeys containsObject:@"FAR_PHOTO_DATA"])
        {
            UIImage *photo = [self convertBase64ToPhoto:base64Dict[@"FAR_PHOTO_DATA"]];

            if (photo)
                [session setFarPhotoImage:photo];
        }

        // Near Photo
        if ([base64Dict.allKeys containsObject:@"NEAR_PHOTO_DATA"])
        {
            UIImage *photo = [self convertBase64ToPhoto:base64Dict[@"NEAR_PHOTO_DATA"]];

            if (photo)
                [session setNearPhotoImage:photo];
        }

        // Screenshot Photo
        if ([base64Dict.allKeys containsObject:@"SCREENSHOT_PHOTO_DATA"])
        {
            UIImage *photo = [self convertBase64ToPhoto:base64Dict[@"SCREENSHOT_PHOTO_DATA"]];

            if (photo)
                [session setScreenShotImage:photo];
        }
    }

    [self storeSessionToSQLite:session];
}

#pragma mark - Loading View

- (void)showShareLoadingView
{
    [SVProgressHUD setFont:[UIFont fontWithName:@"TrebuchetMS" size:14.f]];
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait. Share data...", nil)];
    
    [self enableScreenSleeping:YES];
}

- (void)dismissShareLoadingView
{
    [SVProgressHUD dismiss];
    
    [self enableScreenSleeping:NO];
}

- (void)enableScreenSleeping:(BOOL)isSleeping
{
    if (![UIApplication sharedApplication].idleTimerDisabled)
        [[UIApplication sharedApplication] setIdleTimerDisabled:isSleeping];
}

@end
