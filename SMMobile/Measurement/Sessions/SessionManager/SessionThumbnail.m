//
//  SessionThumbnail.m
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 16.03.13.
//
//

#import "SessionThumbnail.h"


@implementation SessionThumbnail

@synthesize nameLabel, timeLabel;
@dynamic image;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self loadViewFromNib];
    }
    return self;
}

- (void)loadViewFromNib
{
    UIView *firstViewUIView = [[[NSBundle mainBundle] loadNibNamed:@"SessionThumbnail" owner:self options:nil] objectAtIndex:0];
    firstViewUIView.userInteractionEnabled = NO;
    [self addSubview:firstViewUIView];
}

- (UIImage *)image
{
    return thumbImageView.image;
}

- (void)setImage:(UIImage *)image
{
    thumbImageView.image = image;
}

@end
