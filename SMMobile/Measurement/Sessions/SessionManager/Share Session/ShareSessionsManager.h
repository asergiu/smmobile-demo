//
//  ShareSessionsManager.h
//  SMMobile
//
//  Created by Sergej Bogatenko on 8/19/19.
//  Copyright © 2019 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SHARE_MANAGER [ShareSessionsManager sharedManager]

extern NSString * const PDShareImportNotification;
extern NSString * const PDShareExportNotification;
extern NSString * const PDEndExportNotification;

@interface ShareSessionsManager : NSObject

+ (ShareSessionsManager *)sharedManager;

// Path
- (NSURL *)getFileURL;

// Export
- (void)writeSesionToFile:(NSDictionary *)session
            sessionsCount:(NSInteger)sessionsCount;

// Import
- (void)importSessionFromURL:(NSURL *)url;

// Delete
- (void)removeIfNeadedOldFile;

- (void)enableScreenSleeping:(BOOL)isSleeping;

@end

