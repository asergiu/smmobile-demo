//
//  ShareSessionsManager.m
//  SMMobile
//
//  Created by Sergej Bogatenko on 8/19/19.
//  Copyright © 2019 ACEP. All rights reserved.
//

#import "ShareSessionsManager.h"
#import "SessionFileReader.h"
#import "SessionManager.h"

#define TICK_TIME 0.1

NSString * const PDShareImportNotification = @"PDShareImportNotification";
NSString * const PDShareExportNotification = @"PDShareExportNotification";
NSString * const PDEndExportNotification = @"PDEndExportNotification";


@interface ShareSessionsManager()
{
    NSFileManager *fileManager;
    
    SessionFileReader *reader;
    
    NSTimer *readTimer;
    
    int sessionsCount;
    int readedSessionsCount;
    int writeSessionsCount;
    
    NSURL *iportURL;
}

@end

@implementation ShareSessionsManager

+ (ShareSessionsManager *)sharedManager
{
    static dispatch_once_t onceToken = 0;
    static id _sharedInstance = nil;
    
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

#pragma mark - Init

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        fileManager = [NSFileManager defaultManager];
        
        sessionsCount = 0;
        readedSessionsCount = 0;
        writeSessionsCount = 0;
    }
    
    return self;
}

#pragma mark - Path

- (NSURL *)getFileURL
{
    return [NSURL fileURLWithPath:[self getPath]];
}

- (NSString *)getPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:@"export_pd.session"];
    
    return filePath;
}

#pragma mark - Export

- (void)writeSesionToFile:(NSDictionary *)session
            sessionsCount:(NSInteger)sessionsCount
{
    if ([fileManager fileExistsAtPath:[self getPath]])
    {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:session options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSData *lengthData = [self getCurrentSessionLengthData:jsonString];
        NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        // Add the text at the end of the file.
        NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:[self getPath]];
        
        [fileHandler seekToEndOfFile];
        [fileHandler writeData:lengthData];
        [fileHandler writeData:data];
        [fileHandler closeFile];
        
        writeSessionsCount++;
        
        [self postExportNotification:@{ @"sessionsCount" : @(sessionsCount),
                                        @"writeSessionsCount" : @(writeSessionsCount) }];
    }
    else
    {
        writeSessionsCount = 0;
        
        NSData *sessionCountData = [self getSessionsCountData:sessionsCount];
        
        // Create the file and write text to it.
        [sessionCountData writeToFile:[self getPath] atomically:YES];
        
        [self writeSesionToFile:session sessionsCount:sessionsCount];
    }
}

- (NSData *)getSessionsCountData:(NSInteger)sessionsCount
{
    NSDictionary *dict = @{ @"sessionsCount" : @(sessionsCount) };
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSUInteger bytes = [jsonString lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    
    while (bytes < 100) {
        jsonString = [jsonString stringByAppendingString:@" "];
        bytes = [jsonString lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    }
    
    return [jsonString dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSData *)getCurrentSessionLengthData:(NSString *)string
{
    NSUInteger sessionBytes = [string lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error;
    NSDictionary *dict = @{ @"sessionsLength" : @(sessionBytes) };
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSUInteger bytes = [jsonString lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    
    while (bytes < 100) {
        jsonString = [jsonString stringByAppendingString:@" "];
        bytes = [jsonString lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    }
    
    return [jsonString dataUsingEncoding:NSUTF8StringEncoding];
}

#pragma mark - Import

- (void)importSessionFromURL:(NSURL *)url
{
    iportURL = url;
    
    NSError *error;
    
    reader = [[SessionFileReader alloc] initWithFilePath:url];
    
    NSData *sessionsCountData = [reader readDataOfLength:100];
    
    id parsObj = [NSJSONSerialization JSONObjectWithData:sessionsCountData options:NSJSONReadingMutableContainers error:&error];
    
    sessionsCount = 0;
    
    if ([parsObj isKindOfClass:[NSDictionary class]])
        sessionsCount = [parsObj[@"sessionsCount"] intValue];
    
    [self startTimer];
}

- (void)readSession
{
    if (readedSessionsCount < sessionsCount)
    {
        NSError *error;
        NSInteger sessionLength = 0;
        
        NSData *sessionLengthData = [reader readDataOfLength:100];
        id obj = [NSJSONSerialization JSONObjectWithData:sessionLengthData options:NSJSONReadingMutableContainers error:&error];
        
        if ([obj isKindOfClass:[NSDictionary class]])
            sessionLength = [obj[@"sessionsLength"] integerValue];
        
        NSData *sessionData = [reader readDataOfLength:sessionLength];
        id session = [NSJSONSerialization JSONObjectWithData:sessionData options:NSJSONReadingMutableContainers error:&error];
        
        if ([session isKindOfClass:[NSDictionary class]])
            [SESSION_MANAGER importSessionFromShare:session];
        
        readedSessionsCount++;
        
        [self postImportNotification:@{ @"sessionsCount" : @(sessionsCount),
                                        @"readedSessionsCount" : @(readedSessionsCount) }];
    }
    else
    {
        [self stopTimer];
        [self removeImportFile];
        [self resetAndCloseFile];

        [self postEndExportNotification];
    }
}

- (void)removeImportFile
{
    NSError *error;
    
    [fileManager removeItemAtURL:iportURL error:&error];

    if (error != nil)
        NSLog(@"REMOVE: error - %@", error);
}

- (void)resetAndCloseFile
{
    sessionsCount = 0;
    readedSessionsCount = 0;
    writeSessionsCount = 0;
    
    [reader closeFile];
    reader = nil;
    
    iportURL = nil;
}

#pragma mark - Read Timer

- (void)startTimer
{
    if (!readTimer)
        readTimer = [NSTimer scheduledTimerWithTimeInterval:TICK_TIME
                                                     target:self
                                                   selector:@selector(readSession)
                                                   userInfo:nil
                                                    repeats:YES];
}

- (void)stopTimer
{
    [readTimer invalidate];
    readTimer = nil;
}

#pragma mark - Delete

- (void)removeIfNeadedOldFile
{
    NSError *error;

    if ([fileManager fileExistsAtPath:[self getPath]])
        [fileManager removeItemAtPath:[self getPath] error:&error];
    
    if (error != nil)
        NSLog(@"REMOVE Error: %@", error);
}

#pragma mark - Notifications

- (void)postImportNotification:(NSDictionary *)dict
{
    [[NSNotificationCenter defaultCenter] postNotificationName:PDShareImportNotification
                                                        object:self
                                                      userInfo:dict];
}

- (void)postExportNotification:(NSDictionary *)dict
{
    [[NSNotificationCenter defaultCenter] postNotificationName:PDShareExportNotification
                                                        object:self
                                                      userInfo:dict];
}

- (void)postEndExportNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:PDEndExportNotification
                                                        object:self
                                                      userInfo:nil];
}

#pragma mark -

- (void)enableScreenSleeping:(BOOL)isSleeping
{
    [SESSION_MANAGER enableScreenSleeping:isSleeping];
}

@end
