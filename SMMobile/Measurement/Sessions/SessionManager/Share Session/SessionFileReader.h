//
//  SessionFileReader.h
//  SMMobile
//
//  Created by Sergej Bogatenko on 8/15/19.
//  Copyright © 2019 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SessionFileReader : NSObject

- (id)initWithFilePath:(NSURL *)URLPath;

- (NSData *)readDataOfLength:(NSInteger)length;
- (void)seekToFileOffset:(unsigned long long)offset;
- (void)closeFile;

@end

