//
//  SessionFileReader.m
//  SMMobile
//
//  Created by Sergej Bogatenko on 8/15/19.
//  Copyright © 2019 ACEP. All rights reserved.
//

#import "SessionFileReader.h"

@interface SessionFileReader ()
{
    NSFileHandle *fileHandle;
    
    unsigned long long fileOffset;
}

@end

@implementation SessionFileReader

- (id)initWithFilePath:(NSURL *)URLPath
{
    if (self = [super init]) {
        NSError *error;
        fileHandle = [NSFileHandle fileHandleForReadingFromURL:URLPath error:&error];
        fileOffset = 0;
        
        if (error != nil)
            NSLog(@"READER error = %@", error);
    }
    return self;
}

- (NSData *)readDataOfLength:(NSInteger)length;
{
    NSData *data = [fileHandle readDataOfLength:length];
    
    [self seekToFileOffset:length];
    
    return data;
}

- (void)seekToFileOffset:(unsigned long long)offset
{
    fileOffset += offset;
    
    [fileHandle seekToFileOffset:fileOffset];
}

- (void)closeFile
{
    [fileHandle closeFile];
}

@end
