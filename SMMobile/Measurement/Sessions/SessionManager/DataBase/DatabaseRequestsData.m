//
//  DatabaseRequestsData.m
//  PDdatabase
//
//  Created by Владимир Малашенков on 09.03.13.
//  Copyright (c) 2013 Владимир Малашенков. All rights reserved.
//

#import "DatabaseRequestsData.h"
#import "NSDateFormatter+SMLocale.h"

@implementation DatabaseRequestsData

+ (NSString *)nullUUID
{
    return @"00000000-0000-0000-0000-000000000000";
}

+ (NSString *)UUIDs
{
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    
    NSString *uuid = [NSString stringWithString:(__bridge NSString *) uuidStringRef];
    CFRelease(uuidStringRef);
    
    return uuid;
}

+ (NSString *)timestamp
{
    NSDate *now = [NSDate date];
    
    return [self dateStringFromDate:now];
}

+ (NSDateFormatter *)timeFormatterForSaveDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] initWithSafeLocale];
    [formatter setDateFormat: @"yyyy-MM-dd HH:mm:ss zzz"];
    
    return formatter;
}

+ (NSDateFormatter *) timeFormatterForSaveDateInAmPm
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] initWithSafeLocale];
    [formatter setDateFormat: @"yyyy-MM-dd h:mm:ss a"];
    
    return formatter;
}

+ (NSDateFormatter *)timeFormatterForHumanReadableDate
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat: @"dd MMMM yyyy"];
    
    return formatter;
}

+ (NSDateFormatter *)timeFormatterForHumanReadableTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] initWithSafeLocale];
    [formatter setDateFormat: @"HH:mm"];
    
    return formatter;
}

+ (NSDate *)getDateForValue:(id)value
{
    NSDateFormatter * dbDateFormat = [DatabaseRequestsData timeFormatterForSaveDate];
    NSDateFormatter * dbDateFormatAmPm = [DatabaseRequestsData timeFormatterForSaveDateInAmPm];
    
    NSDate *currentSessionDate;
    
    if ([value isKindOfClass:[NSDate class]])
        currentSessionDate = (NSDate *)value;
    else
        currentSessionDate = [dbDateFormat dateFromString:(NSString *)value];
    
    if (!currentSessionDate)
        currentSessionDate = [dbDateFormatAmPm dateFromString:(NSString *)value];
    
    if (!currentSessionDate)
        currentSessionDate = [DatabaseRequestsData createDateWithString:(NSString *)value];
    
    if (!currentSessionDate)
        currentSessionDate = [NSDate date];
    
    return currentSessionDate;
}

+ (NSDate *)createDateWithString:(NSString *)dateString
{
    NSDateComponents *dateComponents = [NSDateComponents new];
    
    NSArray *dateParts = [dateString componentsSeparatedByString:@" "];
    
    if (!dateParts.count)
    {
        return nil;
    }
    
    NSArray *datePart = [dateParts[0] componentsSeparatedByString:@"-"];
    
    if (datePart.count >= 3)
    {
        [dateComponents setYear:[datePart[0] intValue]];
        [dateComponents setMonth:[datePart[1] intValue]];
        [dateComponents setDay:[datePart[2] intValue]];
    }
    
    NSArray *hoursPart = [dateParts[1] componentsSeparatedByString:@":"];
    
    if (hoursPart.count >= 3)
    {
        [dateComponents setHour:[hoursPart[0] intValue]];
        [dateComponents setMinute:[hoursPart[1] intValue]];
        [dateComponents setSecond:[hoursPart[2] intValue]];
    }
    
    NSCalendar *gregorianCal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    [gregorianCal setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    
    return [gregorianCal dateFromComponents:dateComponents];
}

+ (NSString *)dateStringFromDate:(NSDate *)date
{
    NSDateFormatter *formatter = [self timeFormatterForSaveDate];
    
    NSString *timeString = [formatter stringFromDate:date];
    
    return timeString;
}

+ (NSDate *)dateForString:(NSString *)dateString
{
    NSString *dString = [dateString substringToIndex:19];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] initWithSafeLocale];
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    NSDate *date = [formatter dateFromString:dString];
    
    if (date)
    {
        return date;
    }
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    return [formatter dateFromString:dateString];
}

@end
