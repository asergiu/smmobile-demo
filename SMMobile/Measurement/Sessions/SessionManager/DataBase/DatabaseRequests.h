//
//  DatabaseRequests.h
//  PDdatabase
//
//  Created by Владимир Малашенков on 27.02.13.
//  Copyright (c) 2013 Владимир Малашенков. All rights reserved.
//

#import "DBTableField.h"
#import "SQLiteDBHelper.h"
#import <Foundation/Foundation.h>
#import "DatabaseRequestsData.h"
#import "DatabaseRows.h"
#import "Session.h"

@interface DatabaseRequests : NSObject

//create
+ (NSString *)tableCreateRequestAll;

//Update local database
+ (NSString *)updateDatabaseForBundleSupport;
+ (NSString *)updateDatabaseForAdditionalDataSupport;

//1
+ (void)sessionInsertRequestWithSessionid:(NSString *)sessionId
                              sessionType:(NSString *)type
                               serverType:(BOOL)savedToServer
                          clientFirstName:(NSString *)firstName
                           clientLastName:(NSString *)lastName
                               macAddress:(NSString *)macAddress
                                  storeId:(NSString *)storeId
                                 bundleId:(NSString *)bundleId
                           additionalData:(NSString *)additional
                                     date:(NSString *)date
                               updateDate:(NSString *)updateDate
                            markAsDeleted:(BOOL)deleted
                                 toTarget:(SourceType)targetType;
//2
+ (void) addPhotoUpdateRequestWithSessionid:(NSString*)sessionId
                                   photoUID:(NSString*)photoUID
                                  photoType:(NSString*)photoType
                                   fileName:(NSString*)fileName
                           photoSessionType:(SessionType)photoSession
                                 updateDate:(NSString*)updateDate
                                   toTarget:(SourceType)targetType;

//3
+ (void)updateSessionDataID:(NSString *)dataId
              withSessionId:(NSString *)sessionId
                sessionType:(SessionType)sessionType
                 updateDate:(NSString *)updateDate
                   toTarget:(SourceType)targetType;

+ (void)storeInsertRequestToSessionid:(NSString *)sessionId
                        withStoreName:(NSString *)name
                              address:(NSString *)address
                                phone:(NSString *)phone
                                 mail:(NSString *)mail
                             toTarget:(SourceType)targetType;

+ (void)addMarkersDataForSessionId:(NSString *)sessionId
                       deviceAngle:(NSString *)angle
                             leftX:(NSString *)leftX
                             leftY:(NSString *)leftY
                           centerX:(NSString *)centerX
                           centerY:(NSString *)centerY
                            rightX:(NSString *)rightX
                            rightY:(NSString *)rightY
                          eyeLeftX:(NSString *)eyeLeftX
                          eyeLeftY:(NSString *)eyeLeftY
                         eyeRightX:(NSString *)eyeRightX
                         eyeRightY:(NSString *)eyeRightY
                          toTarget:(SourceType)targetType;

+ (void)addFrameMarkersDataForSessionId:(NSString *)sessionId
                              xLeftRect:(NSString *)xLeftRect
                              yLeftRect:(NSString *)yLeftRect
                             xRightRect:(NSString *)xRightRect
                             yRightRect:(NSString *)yRightRect
                            xLeftUpRect:(NSString *)xLeftUpRect
                            yLeftUpRect:(NSString *)yLeftUpRect
                           xRightUpRect:(NSString *)xRightUpRect
                           yRightUpRect:(NSString *)yRightUpRect
                               toTarget:(SourceType)targetType;

+ (void)addWrapMarkersDataForSessionId:(NSString *)sessionId
                             xLeftWrap:(NSString *)xLeftWrap
                             yLeftWrap:(NSString *)yLeftWrap
                            xRightWrap:(NSString *)xRightWrap
                            yRightWrap:(NSString *)yRightWrap
                              toTarget:(SourceType)targetType;

+ (void)updateResultsDataIDWithSessionId:(NSString *)sessionId
                              updateData:(NSString *)updateData
                                toTarget:(SourceType)targetType;

+ (void)addResultsDataForSessionId:(NSString *)sessionId
                       sessionType:(SessionType)type
                            pdLeft:(NSString *)pdLeft
                           pdRight:(NSString *)pdRight
                        heightLeft:(NSString *)heightLeft
                       heightRight:(NSString *)heightRight
                        frameWidth:(NSString *)frameWidth
                        frameHight:(NSString *)frameHight
                       frameBridge:(NSString *)frameBridge
                     photoDistance:(NSString *)photoDistance
                        pantoAngle:(NSString *)pantoAngle
                   readingDistance:(NSString *)readingDistance
                         wrapAngle:(NSString *)wrapAngle
                        pdLeftNear:(NSString *)pdLeftNear
                       pdRightNear:(NSString *)pdRightNear
                    heightLeftNear:(NSString *)heightLeftNear
                   heightRightNear:(NSString *)heightRightNear
                   frameBridgeNear:(NSString *)frameBridgeNear
               readingDistanceNear:(NSString *)readingDistanceNear
                          toTarget:(SourceType)targetType;

//delete

+ (void)updateTypeForSessionId:(NSString *)sessionId
                       newType:(NSString *)type
                      toTarget:(SourceType)targetType;

+ (void)deleteSession:(Session *)session
            sorceType:(SourceType)sorce;

+ (void)deletePhotoForPhotoId:(NSString *)photoID
                     toTarget:(SourceType)targetType;

+ (void)deleteDataForDataId:(NSString *)dataID
                   toTarget:(SourceType)targetType;

+ (void)deleteResultsForResultId:(NSString *)resultID
                        toTarget:(SourceType)targetType;


+ (NSDictionary *)getSessionForId:(NSString *)sessionId;

+ (NSArray *)sessionArrayForStoreId:(NSString *)storeId
                          sorceType:(SourceType)source;

+ (NSDictionary *)photoDictForPhotoID:(NSString *)photoID
                            sorceType:(SourceType)source;

+ (NSDictionary *)dataDictForDataID:(NSString *)dataID
                          sorceType:(SourceType)source;

+ (NSDictionary *)resultsDictForResultsID:(NSString *)resultsID
                                sorceType:(SourceType)source;

@end
