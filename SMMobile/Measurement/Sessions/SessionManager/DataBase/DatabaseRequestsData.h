//
//  DatabaseRequestsData.h
//  PDdatabase
//
//  Created by Владимир Малашенков on 09.03.13.
//  Copyright (c) 2013 Владимир Малашенков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define UUID_NULL [DatabaseRequestsData nullUUID]
#define UUID [DatabaseRequestsData UUIDs]
#define TIMESTAMP [DatabaseRequestsData timestamp]

typedef int SessionType;
typedef int PhotoType;
typedef int PhotoSessionType;
typedef int SourceType;

enum {
    SessionTypeNearPD = 0,
    SessionTypeFarPD = 1,
    SessionTypePersonalized = 2
};

enum {
    PhotoTypeJPEG = 0,
    PhotoTypePNG = 1,
};

enum  {
    PhotoSessionTypeNear = 0,
    PhotoSessionTypeFar = 1,
    PhotoSessionTypeScreenshot = 2
};

enum
{
    SourceTypeServer,
    SourceTypeLocal,
    SourceTypeLive,
};

@interface DatabaseRequestsData : NSObject

+ (NSString *)nullUUID;
+ (NSString *)UUIDs;
+ (NSString *)timestamp;

//dateformatters
+ (NSDateFormatter *)timeFormatterForSaveDate;
+ (NSDateFormatter *) timeFormatterForSaveDateInAmPm;
+ (NSDateFormatter *)timeFormatterForHumanReadableDate;
+ (NSDateFormatter *)timeFormatterForHumanReadableTime;

+ (NSDate *)getDateForValue:(id)value;

+ (NSString *)dateStringFromDate:(NSDate *)date;
+ (NSDate *)dateForString:(NSString *)dateString;

@end
