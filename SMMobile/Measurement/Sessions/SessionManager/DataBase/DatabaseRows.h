//
//
//  DatabaseRows.h
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 01.04.13.
//
//

//************* Table Rows *************//

//pd_ECP
#define kTableRowECP_Id                    @"ECP_ID"
#define kTableRowECP_Name                  @"ECP_NAME"

//pd_Stores
#define kTableRowStore_ECPId               @"STORE_ECP_ID"
#define kTableRowStore_Email               @"STORE_EMAIL"
#define kTableRowStore_Phone               @"STORE_PHONE_NUMBER"
#define kTableRowStore_Address             @"STORE_ADDRESS"
#define kTableRowStore_Name                @"STORE_NAME"
#define kTableRowStore_Id                  @"STORE_ID"

//pd_Progressive
#define kTableRowProgressive_Id             @"PROGRESSIVE_ID"
#define kTableRowProgressive_Corridor       @"PROGRESSIVE_CORRIDOR"
#define kTableRowProgressive_Name           @"PROGRESSIVE_NAME"

//pd_Photo
#define kTableRowPhoto_Id                   @"PHOTO_ID"
#define kTableRowPhoto_Type                 @"PHOTO_TYPE"
#define kTableRowPhoto_Data                 @"PHOTO_DATA"

//pd_Session
#define kTableRowSession_Id                 @"SESSION_ID"
#define kTableRowSession_Type               @"SESSION_TYPE"
#define kTableRowSession_NetStatus          @"SESSION_NET_STATUS"
#define kTableRowSession_Date               @"SESSION_DATE"
#define kTableRowSession_LastUpdate         @"SESSION_DATE_LAST_UPDATE"
#define kTableRowSession_DatePhotoFar       @"SESSION_DATE_PHOTO_FAR"
#define kTableRowSession_DatePhotoNear      @"SESSION_DATE_PHOTO_NEAR"
#define kTableRowSession_PhotoFar           @"SESSION_PHOTO_FAR"
#define kTableRowSession_PhotoNear          @"SESSION_PHOTO_NEAR"
#define kTableRowSession_Screenshot         @"SESSION_SCREENSHOT"
#define kTableRowSession_ClienfFirstname    @"SESSION_CLIENT_FIRSTNAME"
#define kTableRowSession_ClienfLastname     @"SESSION_CLIENT_LASTNAME"
#define kTableRowSession_DeviceMac          @"SESSION_DEVICE_MAC_ADDRESS"
#define kTableRowSession_StoreId            @"SESSION_STORE_ID"
#define kTableRowSession_ProgressiveId      @"SESSION_PROGRESSIVE_ID"
#define kTableRowSession_DataIdFar          @"SESSION_FAR_DATA_ID"
#define kTableRowSession_DataIdNear         @"SESSION_NEAR_DATA_ID"
#define kTableRowSession_ResultsId          @"SESSION_RESULTS_ID"
#define kTableRowSession_BundleId           @"SESSION_BUNDLE_ID"  // Application Bundle ID
#define kTableRowSession_AdditionalData     @"SESSION_ADDITIONAL" // Additional session data (fred_type)

//pd_Data
#define kTableRowData_Id                    @"DATA_ID"
#define kTableRowData_iPadAngle             @"DATA_IPAD_ANGLE"
#define kTableRowData_PhotoMarkersPresent   @"DATA_PHOTO_MARKERS_PRESENT"
#define kTableRowData_MarkerSupportLX       @"DATA_MARKER_SUPPORT_LEFT_X"
#define kTableRowData_MarkerSupportLY       @"DATA_MARKER_SUPPORT_LEFT_Y"
#define kTableRowData_MarkerSupportCX       @"DATA_MARKER_SUPPORT_CENTER_X"
#define kTableRowData_MarkerSupportCY       @"DATA_MARKER_SUPPORT_CENTER_Y"
#define kTableRowData_MarkerSupportRX       @"DATA_MARKER_SUPPORT_RIGHT_X"
#define kTableRowData_MarkerSupportRY       @"DATA_MARKER_SUPPORT_RIGHT_Y"
#define kTableRowData_MarkerEyeLX           @"DATA_MARKER_EYE_LEFT_X"
#define kTableRowData_MarkerEyeLY           @"DATA_MARKER_EYE_LEFT_Y"
#define kTableRowData_MarkerEyeRX           @"DATA_MARKER_EYE_RIGHT_X"
#define kTableRowData_MarkerEyeRY           @"DATA_MARKER_EYE_RIGHT_Y"
#define kTableRowData_FrameMarkersPresent   @"DATA_FRAME_MARKERS_PRESENT"
#define kTableRowData_FrameLX               @"DATA_MARKER_FRAME_LEFT_X"
#define kTableRowData_FrameLY               @"DATA_MARKER_FRAME_LEFT_Y"
#define kTableRowData_FrameULX              @"DATA_MARKER_FRAME_UP_LEFT_X"
#define kTableRowData_FrameULY              @"DATA_MARKER_FRAME_UP_LEFT_Y"
#define kTableRowData_FrameRX               @"DATA_MARKER_FRAME_RIGHT_X"
#define kTableRowData_FrameRY               @"DATA_MARKER_FRAME_RIGHT_Y"
#define kTableRowData_FrameURX              @"DATA_MARKER_FRAME_UP_RIGHT_X"
#define kTableRowData_FrameURY              @"DATA_MARKER_FRAME_UP_RIGHT_Y"
#define kTableRowData_WrapMarkersPresent    @"DATA_WRAP_MARKERS_PRESENT"
#define kTableRowData_WrapLX                @"DATA_MARKER_WRAP_LEFT_X"
#define kTableRowData_WrapLY                @"DATA_MARKER_WRAP_LEFT_Y"
#define kTableRowData_WrapRX                @"DATA_MARKER_WRAP_RIGHT_X"
#define kTableRowData_WrapRY                @"DATA_MARKER_WRAP_RIGHT_Y"

//pd_Results
#define kTableRowResults_Id                 @"RESULTS_ID"
#define kTableRowResults_FarDataPresent     @"RESULTS_FAR_DATA_PRESENT"
#define kTableRowResults_FarPDL             @"RESULTS_FAR_PD_LEFT"
#define kTableRowResults_FarPDR             @"RESULTS_FAR_PD_RIGHT"
#define kTableRowResults_FarHL              @"RESULTS_FAR_HEIGHT_LEFT"
#define kTableRowResults_FarHR              @"RESULTS_FAR_HEIGHT_RIGHT"
#define kTableRowResults_FarFW              @"RESULTS_FAR_FRAME_WIDTH"
#define kTableRowResults_FarFH              @"RESULTS_FAR_FRAME_HRIGHT"
#define kTableRowResults_FarFBridge         @"RESULTS_FAR_FRAME_BRIDGE"
#define kTableRowResults_FarPhotoDist       @"RESULTS_FAR_PHOTO_DISTANCE"
#define kTableRowResults_FarPanto           @"RESULTS_FAR_PANTO_ANGLE"
#define kTableRowResults_NearDataPresent    @"RESULTS_NEAR_DATA_PRESENT"
#define kTableRowResults_NearPDL            @"RESULTS_NEAR_PD_LEFT"
#define kTableRowResults_NearPDR            @"RESULTS_NEAR_PD_RIGHT"
#define kTableRowResults_NearHL             @"RESULTS_NEAR_HEIGHT_LEFT"
#define kTableRowResults_NearHR             @"RESULTS_NEAR_HEIGHT_RIGHT"
#define kTableRowResults_NearFBridge        @"RESULTS_NEAR_FRAME_BRIDGE"
#define kTableRowResults_NearReadingDist    @"RESULTS_NEAR_READING_DISTANCE"
#define kTableRowResults_PositionOfWearPresent      @"RESULTS_POSITION_OF_WEAR_PRESENT"
#define kTableRowResults_VertexDist         @"RESULTS_VERTEX_DISTANCE"
#define kTableRowResults_WrapAngle          @"RESULTS_WRAP_ANGLE"

//custom

#define kTableRowSessionHumanDate           @"SESSION_DATE_LAST_UPDATE_HUMAN_READABLE"
#define kTableRowSessionHumanTime           @"SESSION_TIME_LAST_UPDATE_HUMAN_READABLE"






