//
//  DBTableField.m
//  PDdatabase
//

#import "DBTableField.h"

@implementation DBTableField

@dynamic name, type;

@synthesize dataType;

+ (DBTableField *)tableFieldWithName:(NSString *)name
                            dataType:(DBTableFieldDataType)type
{
    DBTableField *field = [[DBTableField alloc] initWithName:name dataType:type];
    
    return field;
}

- (id)initWithName:(NSString *)name dataType:(DBTableFieldDataType)type
{
    self = [super init];
    
    if (self)
    {
        NSString *typeString = [self dataTypeStringFromType:type];
        
        fieldName = [[NSString alloc] initWithString:name];
        fieldType = [[NSString alloc] initWithString:typeString];
        
        dataType = type;
    }
    return self;
}

- (NSString *)dataTypeStringFromType:(DBTableFieldDataType)type
{
    switch (type)
    {
        case DB_INTEGER:
            return @"INTEGER";
            break;
            
        case DB_PrymaryINTEGER:
            return @"INTEGER PRIMARY KEY";
            break;
            
        case DB_REAL:
            return @"REAL";
            break;
            
        case DB_PrymaryREAL:
            return @"REAL PRIMARY KEY";
            break;
            
        case DB_BLOB:
            return @"BLOB";
            break;
            
        case DB_TEXT:
            return @"TEXT";
            break;
            
        case DB_PrymaryTEXT:
            return @"TEXT PRIMARY KEY";
            break;
            
        default:
            return @"NULL";
            break;
    }
}

- (NSString *)name
{
    return fieldName;
}

- (NSString *)type
{
    return fieldType;
}

@end
