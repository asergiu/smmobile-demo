//
//  DatabaseRequests.m
//  PDdatabase
//
//  Created by Владимир Малашенков on 27.02.13.
//  Copyright (c) 2013 Владимир Малашенков. All rights reserved.
//

#import "DatabaseRequests.h"
#import "Session.h"

#define nullString @""
#define nullREAL @"''"

#define storeCode [[NSUserDefaults standardUserDefaults] objectForKey:@"store_name"]

@implementation DatabaseRequests

#pragma mark - Create Requests

/*
 
 CREATE TABLE pd_ECP (ecp_id TEXT PRIMARY KEY, ecp_name TEXT);
 
 CREATE TABLE pd_Stores (store_ecp_id TEXT PRIMARY KEY, store_email TEXT, store_phone_number TEXT, store_address TEXT, store_name TEXT, store_id TEXT);
 
 CREATE TABLE pd_Progressive (progressive_id TEXT PRIMARY KEY,  progressive_corridor REAL, progressive_name TEXT);
 
 CREATE TABLE pd_Photo (photo_id TEXT PRIMARY KEY, photo_type NUMERIC, photo_data TEXT);
 
 CREATE TABLE pd_Session (session_id TEXT PRIMARY KEY, session_type NUMERIC, session_net_status NUMERIC, session_date TEXT, session_date_last_update TEXT, session_date_photo_far TEXT,session_date_photo_near TEXT, session_photo_far TEXT, session_photo_near TEXT, session_client_firstname TEXT, session_client_lastname TEXT, session_device_mac_address  TEXT, session_store_id TEXT, session_progressive_id TEXT, session_far_data_id TEXT, session_near_data_id TEXT, session_results_id TEXT);
 
 CREATE TABLE pd_Data (data_id TEXT PRIMARY KEY, data_iPad_angle REAL, data_photo_markers_present NUMERIC, data_marker_support_left_x REAL, data_marker_support_left_y REAL, data_marker_support_center_x REAL, data_marker_support_center_y REAL, data_marker_support_right_x REAL, data_marker_support_right_y REAL, data_marker_eye_left_x REAL, data_marker_eye_left_y REAL, data_marker_eye_right_x REAL, data_marker_eye_right_y REAL, data_frame_markers_present NUMERIC, data_marker_frame_left_x REAL, data_marker_frame_left_y REAL, data_marker_frame_up_left_x REAL, data_marker_frame_up_left_y REAL, data_marker_frame_right_x REAL, data_marker_frame_right_y REAL, data_marker_frame_up_right_x REAL, data_marker_frame_up_right_y REAL, data_wrap_markers_present NUMERIC, data_marker_wrap_left_x REAL, data_marker_wrap_left_y REAL, data_marker_wrap_right_x REAL, data_marker_wrap_right_y REAL);
 
 CREATE TABLE pd_Results (results_id TEXT PRIMARY KEY,results_far_data_present NUMERIC,results_far_pd_left REAL,results_far_pd_right REAL,results_far_height_left REAL,results_far_height_right REAL,results_far_frame_width REAL,results_far_frame_hright REAL,results_far_frame_bridge REAL,results_far_photo_distance REAL,results_far_panto_angle REAL,results_near_data_present NUMERIC,results_near_pd_left REAL,results_near_pd_right REAL,results_near_height_left REAL,results_near_height_right REAL,results_near_frame_bridge REAL,results_near_reading_distance REAL,results_position_of_wear_present NUMERIC,results_vertex_distance REAL,results_wrap_angle REAL);
 
 */

#pragma mark - Concrete requests
#pragma mark - Create

+ (NSString *)tableCreateRequestAll
{
    NSMutableString *finalRequest = [NSMutableString string];
    
    [finalRequest appendString:[self createRequestForTableName:@"pd_ECP" fieldsArray:[self pd_ECP] dbSource:SourceTypeLocal]];
    [finalRequest appendString:[self createRequestForTableName:@"pd_Stores" fieldsArray:[self pd_Stores]dbSource:SourceTypeLocal]];
    [finalRequest appendString:[self createRequestForTableName:@"pd_Progressive" fieldsArray:[self pd_Progressive]dbSource:SourceTypeLocal]];
    [finalRequest appendString:[self createRequestForTableName:@"pd_Photo" fieldsArray:[self pd_Photo]dbSource:SourceTypeLocal]];
    [finalRequest appendString:[self createRequestForTableName:@"pd_Session" fieldsArray:[self pd_Session]dbSource:SourceTypeLocal]];
    [finalRequest appendString:[self createRequestForTableName:@"pd_Data" fieldsArray:[self pd_Data]dbSource:SourceTypeLocal]];
    [finalRequest appendString:[self createRequestForTableName:@"pd_Results" fieldsArray:[self pd_Results]dbSource:SourceTypeLocal]];
    
    return finalRequest;
}

+ (NSString *)updateDatabaseForBundleSupport
{
    NSMutableString *finalRequest = [NSMutableString string];
    
    [finalRequest appendString:[self addForTableName:@"pd_Session"
                                            newField:fieldWithNameAndTypes(kTableRowSession_BundleId, DB_TEXT)
                                            dbSource:SourceTypeLocal]];
    return finalRequest;
}

+ (NSString *)updateDatabaseForAdditionalDataSupport
{
    NSMutableString *finalRequest = [NSMutableString string];
    
    [finalRequest appendString:[self addForTableName:@"pd_Session"
                                            newField:fieldWithNameAndTypes(kTableRowSession_AdditionalData, DB_TEXT)
                                            dbSource:SourceTypeLocal]];
    return finalRequest;
}

#pragma mark - Insert

+ (NSString *)tableInsertRequest_pd_ECPForTarget:(SourceType)target
{
    return [self insertRequestForTableName:@"pd_ECP" fieldsArray:[self pd_ECP]];
}

+ (NSString *)tableInsertRequest_pd_StoresForTarget:(SourceType)target
{
    return [self insertRequestForTableName:@"pd_Stores" fieldsArray:[self pd_Stores]];
}

+ (NSString *)tableInsertRequest_pd_ProgressiveForTarget:(SourceType)target
{
    return [self insertRequestForTableName:@"pd_Progressive" fieldsArray:[self pd_Progressive]];
}

+ (NSString *)tableInsertRequest_pd_PhotoForTarget:(SourceType)target
{
    return [self insertRequestForTableName:@"pd_Photo" fieldsArray:[self pd_Photo]];
}

+ (NSString *)tableInsertRequest_pd_SessionForTarget:(SourceType)target
{
    return [self insertRequestForTableName:@"pd_Session" fieldsArray:[self pd_Session]];
}

+ (NSString *)tableInsertRequest_pd_DataForTarget:(SourceType)target
{
    return [self insertRequestForTableName:@"pd_Data" fieldsArray:[self pd_Data]];
}

+ (NSString *)tableInsertRequest_pd_ResultsForTarget:(SourceType)target
{
    return [self insertRequestForTableName:@"pd_Results" fieldsArray:[self pd_Results]];
}

#pragma mark Session

+ (void)sessionInsertRequestWithSessionid:(NSString *)sessionId
                              sessionType:(NSString *)type
                               serverType:(BOOL)savedToServer
                          clientFirstName:(NSString *)firstName
                           clientLastName:(NSString *)lastName
                               macAddress:(NSString *)macAddress
                                  storeId:(NSString *)storeId
                                 bundleId:(NSString *)bundleId
                           additionalData:(NSString *)additional
                                     date:(NSString *)date
                               updateDate:(NSString *)updateDate
                            markAsDeleted:(BOOL)deleted
                                 toTarget:(SourceType)targetType
{
    NSString *targetString = kDBTypeServer;
    
    if (targetType == SourceTypeLocal)
    {
        targetString = savedToServer ? kDBTypeServer : kDBTypeLocale;
    }
    
    NSString * requestFormat = [self tableInsertRequest_pd_SessionForTarget:targetType];
    NSString * request = [NSString stringWithFormat:requestFormat,
                          sessionId ,
                          type ,
                          targetString , //locale db
                          date ,
                          updateDate ,        //modification date
                          nullString ,
                          nullString ,
                          UUID_NULL ,
                          UUID_NULL ,
                          UUID_NULL ,       //screenshot
                          firstName ,
                          lastName ,
                          macAddress ,
                          storeCode ,    //store id
                          UUID_NULL ,    //session data
                          UUID_NULL ,    //session data
                          UUID_NULL ,    //session data
                          UUID_NULL ,    //session data
                          bundleId,
                          additional
                          ];
    
    [self performRequest:request];
}

+ (void)updateTypeForSessionId:(NSString *)sessionId
                       newType:(NSString *)type
                      toTarget:(SourceType)targetType
{
    NSArray * updateFields = @[fieldWithNameAndTypes (kTableRowSession_NetStatus, DB_TEXT)];
    
    NSString * updateFormat = [self updateRequestForTableName:@"pd_Session"
                                                searchByField:[[self pd_Session] objectAtIndex:0]
                                                  fieldsArray:updateFields];
    
    NSString * updateRequest = [NSString stringWithFormat:updateFormat,
                                type, //type
                                sessionId
                                ];
    
    [self performRequest:updateRequest];
}

#pragma mark Store

+ (void) storeInsertRequestToSessionid:(NSString *)sessionId
                         withStoreName:(NSString *)name
                               address:(NSString *)address
                                 phone:(NSString *)phone
                                  mail:(NSString *)mail
                              toTarget:(SourceType)targetType
{
    NSString * requestFormat = [self tableInsertRequest_pd_StoresForTarget:targetType];
    NSString * request = [NSString stringWithFormat:requestFormat,
                          @"" ,//ecp
                          mail ,
                          phone ,
                          address,
                          name,
                          name //id
                          ];
    
    [self performRequest:request];
    
    NSArray * updateFields = @[fieldWithNameAndTypes (kTableRowSession_StoreId, DB_TEXT)];
    
    NSString * updateFormat = [self updateRequestForTableName:@"pd_Session"
                                                searchByField:[[self pd_Session] objectAtIndex:0]
                                                  fieldsArray:updateFields];
    
    NSString * updateRequest = [NSString stringWithFormat:updateFormat,
                                name, //id
                                sessionId
                                ];
    
    [self performRequest:updateRequest];
}


#pragma mark Photo

+ (void)addPhotoUpdateRequestWithSessionid:(NSString*)sessionId
                                  photoUID:(NSString*)photoUID
                                 photoType:(NSString*)photoType
                                  fileName:(NSString*)fileName
                          photoSessionType:(SessionType)photoSession
                                updateDate:(NSString*)updateDate
                                  toTarget:(SourceType)targetType
{
    NSArray * updateFields = nil;
    NSString * updateRequest = nil;
    
    
    switch (photoSession) {
        case PhotoSessionTypeFar:
        {
            updateFields = @[
                             fieldWithNameAndTypes (@"session_date_photo_far", DB_TEXT),
                             fieldWithNameAndTypes (@"session_photo_far", DB_TEXT)
                             ];
            
            NSString * updateFormat = [self updateRequestForTableName:@"pd_Session"
                                                        searchByField:[[self pd_Session] objectAtIndex:0]
                                                          fieldsArray:updateFields];
            
            updateRequest = [NSString stringWithFormat:updateFormat,
                             updateDate ,
                             photoUID ,
                             sessionId
                             ];
        }
            break;
        case PhotoSessionTypeNear:
        {
            updateFields = @[
                             fieldWithNameAndTypes (@"session_date_photo_near", DB_TEXT),
                             fieldWithNameAndTypes (@"session_photo_near", DB_TEXT)
                             ];
            
            NSString * updateFormat = [self updateRequestForTableName:@"pd_Session"
                                                        searchByField:[[self pd_Session] objectAtIndex:0]
                                                          fieldsArray:updateFields];
            
            updateRequest = [NSString stringWithFormat:updateFormat,
                             updateDate ,
                             photoUID ,
                             sessionId
                             ];
        }
            break;
        case PhotoSessionTypeScreenshot:
        {
            updateFields = @[fieldWithNameAndTypes (@"session_screenshot", DB_TEXT)];
            
            NSString * updateFormat = [self updateRequestForTableName:@"pd_Session"
                                                        searchByField:[[self pd_Session] objectAtIndex:0]
                                                          fieldsArray:updateFields];
            
            updateRequest = [NSString stringWithFormat:updateFormat,
                             photoUID ,
                             sessionId
                             ];
        }
            break;
    }
    
    NSString * insertFormat = [self tableInsertRequest_pd_PhotoForTarget:targetType];
    
    NSString * insertRequest = [NSString stringWithFormat:insertFormat,
                                photoUID ,
                                photoType ,
                                fileName
                                ];
    
    NSString * finalRequest = [insertRequest stringByAppendingString:updateRequest];
    
    [self performRequest:finalRequest];
}

#pragma mark Markers

+ (void)updateSessionDataID:(NSString *)dataId
              withSessionId:(NSString *)sessionId
                sessionType:(SessionType)sessionType
                 updateDate:(NSString *)updateDate
                   toTarget:(SourceType)targetType
{
    NSArray * updateFields = nil;
    
    if (sessionType == SessionTypeFarPD)
        updateFields = [NSArray arrayWithObjects:
                        [[self pd_Session] objectAtIndex:4],
                        [[self pd_Session] objectAtIndex:15]
                        ,nil
                        ];
    else
        updateFields = [NSArray arrayWithObjects:
                        [[self pd_Session] objectAtIndex:4],
                        [[self pd_Session] objectAtIndex:16]
                        ,nil
                        ];
    
    NSString * updateFormat = [self updateRequestForTableName:@"pd_Session"
                                                searchByField:[[self pd_Session] objectAtIndex:0]
                                                  fieldsArray:updateFields];
    
    NSString * updateRequest = [NSString stringWithFormat:updateFormat,
                                updateDate ,
                                dataId ,
                                sessionId
                                ];
    
    [self performRequest:updateRequest];
}

+ (void) addMarkersDataForSessionId:(NSString *)sessionId
                        deviceAngle:(NSString *)angle
                              leftX:(NSString *)leftX
                              leftY:(NSString *)leftY
                            centerX:(NSString *)centerX
                            centerY:(NSString *)centerY
                             rightX:(NSString *)rightX
                             rightY:(NSString *)rightY
                           eyeLeftX:(NSString *)eyeLeftX
                           eyeLeftY:(NSString *)eyeLeftY
                          eyeRightX:(NSString *)eyeRightX
                          eyeRightY:(NSString *)eyeRightY
                           toTarget:(SourceType)targetType
{
    NSString * requestFormat = [self tableInsertRequest_pd_DataForTarget:targetType];
    
    NSString * request = [NSString stringWithFormat:requestFormat,
                          sessionId,
                          angle,//data_iPad_angle
                          @"1",//data_photo_markers_present
                          leftX,//data_marker_support_left_x
                          leftY,//data_marker_support_left_y
                          centerX,//data_marker_support_center_x
                          centerY,//data_marker_support_center_y
                          rightX,//data_marker_support_right_x
                          rightY,//data_marker_support_right_y
                          eyeLeftX,//data_marker_eye_left_x
                          eyeLeftY,//data_marker_eye_left_y
                          eyeRightX,//data_marker_eye_right_x
                          eyeRightY,//data_marker_eye_right_y
                          @"0",//data_frame_markers_present
                          nullREAL,//data_marker_frame_left_x
                          nullREAL,//data_marker_frame_left_y
                          nullREAL,//data_marker_frame_up_left_x
                          nullREAL,//data_marker_frame_up_left_y
                          nullREAL,//data_marker_frame_right_x
                          nullREAL,//data_marker_frame_right_y
                          nullREAL,//data_marker_frame_up_right_x
                          nullREAL,//data_marker_frame_up_right_y
                          @"0",//data_wrap_markers_present
                          nullREAL,//data_marker_wrap_left_x
                          nullREAL,//data_marker_wrap_left_y
                          nullREAL,//data_marker_wrap_right_x
                          nullREAL,//data_marker_wrap_right_y
                          
                          nil];
    
    [self performRequest:request];
}

+ (void)addFrameMarkersDataForSessionId:(NSString *)sessionId
                              xLeftRect:(NSString *)xLeftRect
                              yLeftRect:(NSString *)yLeftRect
                             xRightRect:(NSString *)xRightRect
                             yRightRect:(NSString *)yRightRect
                            xLeftUpRect:(NSString *)xLeftUpRect
                            yLeftUpRect:(NSString *)yLeftUpRect
                           xRightUpRect:(NSString *)xRightUpRect
                           yRightUpRect:(NSString *)yRightUpRect
                               toTarget:(SourceType)targetType
{
    NSIndexSet * fieldsIndexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(13, 9)];
    
    NSArray * updateFields = [NSArray arrayWithArray:
                              [[self pd_Data] objectsAtIndexes:fieldsIndexes] ];
    
    NSString * updateFormat = [self updateRequestForTableName:@"pd_Data"
                                                searchByField:[[self pd_Data] objectAtIndex:0]
                                                  fieldsArray:updateFields];
    
    NSString * updateRequest = [NSString stringWithFormat:updateFormat,
                                @"1" ,
                                xLeftRect ,
                                yLeftRect ,
                                xLeftUpRect ,
                                yLeftUpRect,
                                xRightRect,
                                yRightRect,
                                xRightUpRect,
                                yRightUpRect,
                                sessionId
                                ];
    
    [self performRequest:updateRequest];
}

+ (void)addWrapMarkersDataForSessionId:(NSString *)sessionId
                             xLeftWrap:(NSString *)xLeftWrap
                             yLeftWrap:(NSString *)yLeftWrap
                            xRightWrap:(NSString *)xRightWrap
                            yRightWrap:(NSString *)yRightWrap
                              toTarget:(SourceType)targetType

{
    NSIndexSet * fieldsIndexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(22, 5)];
    
    NSArray * updateFields = [NSArray arrayWithArray:
                              [[self pd_Data] objectsAtIndexes:fieldsIndexes] ];
    
    NSString * updateFormat = [self updateRequestForTableName:@"pd_Data"
                                                searchByField:[[self pd_Data] objectAtIndex:0]
                                                  fieldsArray:updateFields];
    
    NSString * updateRequest = [NSString stringWithFormat:updateFormat,
                                @"1" ,
                                xLeftWrap ,
                                yLeftWrap ,
                                xRightWrap ,
                                yRightWrap,
                                sessionId
                                ];
    
    [self performRequest:updateRequest];
}


#pragma mark Results

+ (void)updateResultsDataIDWithSessionId:(NSString *)sessionId
                              updateData:(NSString *)updateData
                                toTarget:(SourceType)targetType
{
    NSArray * updateFields = @[
                               fieldWithNameAndTypes (@"session_date_last_update", DB_TEXT),
                               fieldWithNameAndTypes (@"session_results_id", DB_TEXT)
                               ];
    
    NSString * updateFormat = [self updateRequestForTableName:@"pd_Session"
                                                searchByField:[[self pd_Session] objectAtIndex:0]
                                                  fieldsArray:updateFields];
    
    NSString * updateRequest = [NSString stringWithFormat:updateFormat,
                                updateData ,
                                sessionId ,
                                sessionId
                                ];
    
    [self performRequest:updateRequest];
}

+ (void)addResultsDataForSessionId:(NSString *)sessionId
                       sessionType:(SessionType)type
                            pdLeft:(NSString *)pdLeft
                           pdRight:(NSString *)pdRight
                        heightLeft:(NSString *)heightLeft
                       heightRight:(NSString *)heightRight
                        frameWidth:(NSString *)frameWidth
                        frameHight:(NSString *)frameHight
                       frameBridge:(NSString *)frameBridge
                     photoDistance:(NSString *)photoDistance
                        pantoAngle:(NSString *)pantoAngle
                   readingDistance:(NSString *)readingDistance
                         wrapAngle:(NSString *)wrapAngle
                        pdLeftNear:(NSString *)pdLeftNear
                       pdRightNear:(NSString *)pdRightNear
                    heightLeftNear:(NSString *)heightLeftNear
                   heightRightNear:(NSString *)heightRightNear
                   frameBridgeNear:(NSString *)frameBridgeNear
               readingDistanceNear:(NSString *)readingDistanceNear
                          toTarget:(SourceType)targetType
{
    NSString * requestFormat = [self tableInsertRequest_pd_ResultsForTarget:targetType];
    
    NSString * request = nil;
    
    request = [NSString stringWithFormat:requestFormat,
               sessionId,
               [pdLeft isEqualToString:@"0"] ? @"0" : @"1", //results_far_data_present
               pdLeft,
               pdRight,
               heightLeft,
               heightRight,
               frameWidth,
               frameHight,
               frameBridge,
               photoDistance,
               pantoAngle,
               [pdLeftNear isEqualToString:@"0"] ? @"0" : @"1",
               pdLeftNear,
               pdRightNear,
               heightLeftNear,
               heightRightNear,
               frameBridgeNear,
               readingDistanceNear,
               @"0",
               @"0",
               wrapAngle];
    
    [self performRequest:request];
}

#pragma mark - Delete

+ (void)deleteSession:(Session *)session
            sorceType:(SourceType)sorce
{
    NSMutableString *request = [NSMutableString string];
    
    //session
    
    [request appendString:[self deleteRequestForTable:@"pd_Session"
                                              byField:fieldWithNameAndTypes (@"session_id", DB_PrymaryTEXT)
                                          searchValue:session.sessionId]];
    //photos
    
    [request appendString:[self deleteRequestForTable:@"pd_Photo"
                                              byField:fieldWithNameAndTypes (@"photo_id", DB_PrymaryTEXT)
                                          searchValue:session.farPhoto.photoID]];
    
    
    [request appendString:[self deleteRequestForTable:@"pd_Photo"
                                              byField:fieldWithNameAndTypes (@"photo_id", DB_PrymaryTEXT)
                                          searchValue:session.nearPhoto.photoID]];
    
    [request appendString:[self deleteRequestForTable:@"pd_Photo"
                                              byField:fieldWithNameAndTypes (@"photo_id", DB_PrymaryTEXT)
                                          searchValue:session.screenShot.photoID]];
    
    //data
    
    [request appendString:[self deleteRequestForTable:@"pd_Data"
                                              byField:fieldWithNameAndTypes (@"data_id", DB_PrymaryTEXT)
                                          searchValue:session.farDataId]];
    
    [request appendString:[self deleteRequestForTable:@"pd_Data"
                                              byField:fieldWithNameAndTypes (@"data_id", DB_PrymaryTEXT)
                                          searchValue:session.nearDataId]];
    
    //results
    
    [request appendString:[self deleteRequestForTable:@"pd_Results"
                                              byField:fieldWithNameAndTypes (@"results_id", DB_PrymaryTEXT)
                                          searchValue:session.resultsId]];
    
    [self performRequest:request];
}

+ (void)deletePhotoForPhotoId:(NSString *)photoID
                     toTarget:(SourceType)targetType
{
    NSString * request = [self deleteRequestForTable:@"pd_Photo"
                                             byField:fieldWithNameAndTypes (@"photo_id", DB_PrymaryTEXT)
                                         searchValue:photoID];
    
    [self performRequest:request];
}

+ (void) deleteDataForDataId:(NSString *)dataID
                    toTarget:(SourceType)targetType
{
    NSString * request = [self deleteRequestForTable:@"pd_Data"
                                             byField:fieldWithNameAndTypes (@"data_id", DB_PrymaryTEXT)
                                         searchValue:dataID];
    
    [self performRequest:request];
}

+ (void) deleteResultsForResultId:(NSString *)resultID
                         toTarget:(SourceType)targetType
{
    NSString * request = [self deleteRequestForTable:@"pd_Results"
                                             byField:fieldWithNameAndTypes (@"results_id", DB_PrymaryTEXT)
                                         searchValue:resultID];
    
    [self performRequest:request];
}

#pragma mark - Tables
#pragma mark pd_ECP

+ (NSArray *)pd_ECP
{
    NSArray * array = @[
                        fieldWithNameAndTypes (kTableRowECP_Id, DB_PrymaryTEXT) ,
                        fieldWithNameAndTypes (kTableRowECP_Name, DB_TEXT)
                        ];
    return array;
}


#pragma mark pd_Stores

+ (NSArray *)pd_Stores
{
    NSArray * array = @[
                        fieldWithNameAndTypes (kTableRowStore_ECPId, DB_PrymaryTEXT) ,
                        fieldWithNameAndTypes (kTableRowStore_Email, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowStore_Phone, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowStore_Address, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowStore_Name, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowStore_Id, DB_TEXT)
                        ];
    return array;
}


#pragma mark pd_Progressive

+ (NSArray *)pd_Progressive
{
    NSArray * array = @[
                        fieldWithNameAndTypes (kTableRowProgressive_Id, DB_PrymaryTEXT) ,
                        fieldWithNameAndTypes (kTableRowProgressive_Corridor, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowProgressive_Name, DB_TEXT)
                        ];
    return array;
}


#pragma mark pd_Photo

+ (NSArray *)pd_Photo
{
    NSArray * array = @[
                        fieldWithNameAndTypes (kTableRowPhoto_Id, DB_PrymaryTEXT) ,
                        fieldWithNameAndTypes (kTableRowPhoto_Type, DB_INTEGER) ,
                        fieldWithNameAndTypes (kTableRowPhoto_Data, DB_TEXT)
                        ];
    return array;
}


#pragma mark pd_Session

+ (NSArray *)pd_Session
{
    NSArray * array = @[
                        fieldWithNameAndTypes (kTableRowSession_Id, DB_PrymaryTEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_Type, DB_INTEGER) ,
                        fieldWithNameAndTypes (kTableRowSession_NetStatus, DB_INTEGER) ,
                        fieldWithNameAndTypes (kTableRowSession_Date, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_LastUpdate, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_DatePhotoFar, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_DatePhotoNear, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_PhotoFar, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_PhotoNear, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_Screenshot, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_ClienfFirstname, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_ClienfLastname, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_DeviceMac, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_StoreId, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_ProgressiveId, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_DataIdFar, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_DataIdNear, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_ResultsId, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_BundleId, DB_TEXT) ,
                        fieldWithNameAndTypes (kTableRowSession_AdditionalData, DB_TEXT)
                        ];
    return array;
}


#pragma mark pd_Data

+ (NSArray *)pd_Data
{
    NSArray * array = @[
                        fieldWithNameAndTypes (kTableRowData_Id, DB_PrymaryTEXT) ,
                        fieldWithNameAndTypes (kTableRowData_iPadAngle, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_PhotoMarkersPresent, DB_INTEGER) ,
                        fieldWithNameAndTypes (kTableRowData_MarkerSupportLX, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_MarkerSupportLY, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_MarkerSupportCX, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_MarkerSupportCY, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_MarkerSupportRX, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_MarkerSupportRY, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_MarkerEyeLX, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_MarkerEyeLY, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_MarkerEyeRX, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_MarkerEyeRY, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_FrameMarkersPresent, DB_INTEGER) ,
                        fieldWithNameAndTypes (kTableRowData_FrameLX, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_FrameLY, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_FrameULX, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_FrameULY, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_FrameRX, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_FrameRY, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_FrameURX, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_FrameURY, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_WrapMarkersPresent, DB_INTEGER) ,
                        fieldWithNameAndTypes (kTableRowData_WrapLX, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_WrapLY, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_WrapRX, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowData_WrapRY, DB_REAL)
                        ];
    return array;
}


#pragma mark pd_Results

+ (NSArray *)pd_Results
{
    NSArray * array = @[
                        fieldWithNameAndTypes (kTableRowResults_Id, DB_PrymaryTEXT) ,
                        fieldWithNameAndTypes (kTableRowResults_FarDataPresent, DB_INTEGER) ,
                        fieldWithNameAndTypes (kTableRowResults_FarPDL, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_FarPDR, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_FarHL, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_FarHR, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_FarFW, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_FarFH, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_FarFBridge, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_FarPhotoDist, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_FarPanto, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_NearDataPresent, DB_INTEGER) ,
                        fieldWithNameAndTypes (kTableRowResults_NearPDL, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_NearPDR, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_NearHL, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_NearHR, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_NearFBridge, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_NearReadingDist, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_PositionOfWearPresent, DB_INTEGER) ,
                        fieldWithNameAndTypes (kTableRowResults_VertexDist, DB_REAL) ,
                        fieldWithNameAndTypes (kTableRowResults_WrapAngle, DB_REAL)
                        ];
    return array;
}

#pragma mark - From DB methods

+ (NSDictionary *)getSessionForId:(NSString *)sessionId
{
    NSString *request = [NSString stringWithFormat:@"SELECT * FROM PD_SESSION WHERE SESSION_ID = '%@'", sessionId];
    
    NSArray *sessionArrayFromDB = [self performSelectRequest:request];
    
    NSArray *finalArray = [DatabaseRequests arrayWithDataToArray:sessionArrayFromDB];
    
    return [finalArray firstObject];
}

+ (NSArray *)sessionArrayForStoreId:(NSString *)storeId
                          sorceType:(SourceType)source
{
    NSString * request;
    
    if (source == SourceTypeLocal) {
        
        request = [NSString stringWithFormat:@"SELECT * FROM PD_SESSION ORDER BY %@ DESC;", kTableRowSession_LastUpdate];
        
    } else {
        
        request = [NSString stringWithFormat:@"SELECT * FROM PD_SESSION WHERE SESSION_STORE_ID = '%@' ORDER BY %@ DESC;", storeId, kTableRowSession_LastUpdate];
    }
    
    NSArray * sessionArrayFromDB = [self performSelectRequest:request];
    
    NSArray * finalArray = [DatabaseRequests arrayWithDataToArray:sessionArrayFromDB];
    
    return finalArray;
}

+ (NSDictionary *)photoDictForPhotoID:(NSString *)photoID
                            sorceType:(SourceType)source
{
    if ([photoID isEqualToString:UUID_NULL])
        return [NSDictionary dictionary];
    
    NSString * request = [NSString stringWithFormat:@"SELECT * FROM PD_PHOTO WHERE PHOTO_ID = '%@';", photoID];
    
    NSArray * photoArrayFromDB = [self performSelectRequest:request];
    
    
    return [self firstElementOfArray:photoArrayFromDB];
}

+ (NSDictionary *)dataDictForDataID:(NSString *)dataID sorceType:(SourceType)source
{
    if ([dataID isEqualToString:UUID_NULL])
        return [NSDictionary dictionary];
    
    NSString * request = [NSString stringWithFormat:@"SELECT * FROM PD_DATA WHERE DATA_ID = '%@';", dataID];
    
    NSArray * dataArrayFromDB = [self performSelectRequest:request];
    
    return [self firstElementOfArray:dataArrayFromDB];
}

+ (NSDictionary*) resultsDictForResultsID:(NSString *)resultsID sorceType:(SourceType)source
{
    NSString * request = [NSString stringWithFormat:@"SELECT * FROM PD_RESULTS WHERE RESULTS_ID = '%@';", resultsID];
    
    NSArray * dataArrayFromDB = [self performSelectRequest:request];
    
    
    return [self firstElementOfArray:dataArrayFromDB];
}

#pragma mark - Constructor methods

+ (NSString*) createRequestForTableName:(NSString*)tableName fieldsArray:(NSArray*)fieldsArray dbSource:(SourceType)source
{
    if (![self arrayVerified:fieldsArray])
    {
        return nil;
    }
    
    NSMutableString * finalRequest = [NSMutableString string];
    
    [finalRequest appendString:@"CREATE TABLE IF NOT EXISTS "];
    [finalRequest appendFormat:@"%@ ", [tableName uppercaseString]];
    [finalRequest appendString:@"("];
    
    for (int i=0; i<fieldsArray.count; i++)
    {
        DBTableField * oneField = [fieldsArray objectAtIndex:i];
        
        NSString * oneFieldName = oneField.name;
        NSString * oneFieldType = oneField.type;
        
        [finalRequest appendFormat:@"%@ %@, ", [oneFieldName uppercaseString], oneFieldType];
    }
    
    [self trimLastTwoSymbolsInString:finalRequest];
    [finalRequest appendString:@");"];
    
    return finalRequest;
}

+ (NSString *)addForTableName:(NSString *)tableName newField:(DBTableField *)field dbSource:(SourceType)source
{
    NSMutableString *finalRequest = [NSMutableString string];
    
    [finalRequest appendFormat:@"ALTER TABLE %@ ADD ", [tableName uppercaseString]];
    
    NSString *oneFieldName = field.name;
    NSString *oneFieldType = field.type;
    
    [finalRequest appendFormat:@"%@ %@", [oneFieldName uppercaseString], oneFieldType];
    
    [finalRequest appendString:@";"];
    
    return finalRequest;
}

+ (NSString *)insertRequestForTableName:(NSString *)tableName
                            fieldsArray:(NSArray *)fieldsArray
{
    if (![self arrayVerified:fieldsArray])
    {
        return nil;
    }
    
    NSMutableString * finalRequest = [NSMutableString string];
    
    [finalRequest appendString:@"INSERT OR REPLACE INTO "];
    
    [finalRequest appendFormat:@"%@ ", [tableName uppercaseString]];
    [finalRequest appendString:@"("];
    
    for (int i=0; i<fieldsArray.count; i++)
    {
        DBTableField * oneField = [fieldsArray objectAtIndex:i];
        
        NSString * oneFieldName = oneField.name;
        
        [finalRequest appendFormat:@"%@, ", [oneFieldName uppercaseString]];
    }
    
    [self trimLastTwoSymbolsInString:finalRequest];
    [finalRequest appendString:@") "];
    
    [finalRequest appendString:@"VALUES "];
    [finalRequest appendString:@"("];
    
    for (int i=0; i<fieldsArray.count; i++)
    {
        DBTableField * oneField = [fieldsArray objectAtIndex:i];
        
        if (oneField.dataType == DB_TEXT || oneField.dataType == DB_PrymaryTEXT)
            [finalRequest appendFormat:@"'%%@', "];
        else
            [finalRequest appendFormat:@"%%@, "];
    }
    
    [self trimLastTwoSymbolsInString:finalRequest];
    
    [finalRequest appendString:@");"];
    
    return finalRequest;
}

+ (NSString *)updateRequestForTableName:(NSString *)tableName
                          searchByField:(DBTableField *)searchField
                            fieldsArray:(NSArray *)fieldsArray
{
    NSMutableString * finalRequest = [NSMutableString string];
    
    [finalRequest appendString:@"UPDATE "];
    [finalRequest appendFormat:@"%@ ", [tableName uppercaseString]];
    [finalRequest appendString:@"SET "];
    
    for (int i=0; i<fieldsArray.count; i++)
    {
        DBTableField * oneField = [fieldsArray objectAtIndex:i];
        
        NSString * oneFieldName = oneField.name;
        
        if (oneField.dataType == DB_TEXT || oneField.dataType == DB_PrymaryTEXT)
            [finalRequest appendFormat:@"%@ = '%%@', ", [oneFieldName uppercaseString]];
        else
            [finalRequest appendFormat:@"%@ = %%@, ", [oneFieldName uppercaseString]];
    }
    
    [self trimLastTwoSymbolsInString:finalRequest];
    
    [finalRequest appendString:@" "];
    [finalRequest appendString:@"WHERE "];
    [finalRequest appendFormat:@"%@ = ", [searchField.name uppercaseString]];
    
    if (searchField.dataType == DB_TEXT || searchField.dataType == DB_PrymaryTEXT)
        [finalRequest appendFormat:@"'%%@', "];
    else
        [finalRequest appendFormat:@"%%@, "];
    
    [self trimLastTwoSymbolsInString:finalRequest];
    [finalRequest appendString:@";"];
    
    return finalRequest;
}

+ (void)trimLastTwoSymbolsInString:(NSMutableString *)string
{
    [string replaceCharactersInRange:NSMakeRange(string.length - 2, 2) withString:nullString];
}

+ (NSDictionary *)firstElementOfArray:(NSArray *)array
{
    if (!array.count)
        return nil;
    
    return [array objectAtIndex:0];
}

+ (BOOL)arrayVerified:(NSArray *)array
{
    if (!array.count || [[array objectAtIndex:0] class] != [DBTableField class])
    {
        @throw [NSException exceptionWithName:@"InputArrayError"
                                       reason:@"Request constructor Fields Array must contain objects of DBTableField Class"
                                     userInfo:nil];
        return NO;
    }
    else
        return YES;
}

+ (NSArray *)arrayWithDataToArray:(NSArray *)array
{
    if (!array)
        return nil;
    
    NSMutableArray * finalArray = [NSMutableArray array];
    
    NSDateFormatter * humanDateFormat = [DatabaseRequestsData timeFormatterForHumanReadableDate];
    NSDateFormatter * humanTimeFormat = [DatabaseRequestsData timeFormatterForHumanReadableTime];
    
    for (int i = 0; i<array.count; i++)
    {
        NSMutableDictionary * finalDict = [NSMutableDictionary dictionary];
        
        NSDictionary * oneDict = [array objectAtIndex:i];
        
        NSArray * arrayOfKeys = [oneDict allKeys];
        
        for (int j = 0; j<arrayOfKeys.count; j++)
        {
            NSString * key = [arrayOfKeys objectAtIndex:j];
            NSString * value = [oneDict objectForKey:key];
            
            //adding human readable Date
            if ([key isEqualToString:kTableRowSession_LastUpdate])
            {
                NSDate *currentSessionDate = [DatabaseRequestsData getDateForValue:value];
                
                NSString * humanDate = [humanDateFormat stringFromDate:currentSessionDate];
                [finalDict setObject:humanDate forKey:kTableRowSessionHumanDate];
                
                NSString * humanTime = [humanTimeFormat stringFromDate:currentSessionDate];
                [finalDict setObject:humanTime forKey:kTableRowSessionHumanTime];
            }
            
            [finalDict setObject:value forKey:key];
        }
        
        [finalArray addObject:finalDict];
    }
    
    return finalArray;
}

+ (NSString *)deleteRequestForTable:(NSString *)tableName
                            byField:(DBTableField *)field
                        searchValue:(NSString *)value
{
    return [NSString stringWithFormat: @"DELETE FROM %@ WHERE %@ = '%@';", [tableName uppercaseString], [field.name uppercaseString], value];
}

+ (void)performRequest:(NSString *)request
{
    [SQLiteDB insertRequest:request];
}

+ (NSArray *)performSelectRequest:(NSString *)request
{
    return [SQLiteDB selectRequest:request];
}


@end
