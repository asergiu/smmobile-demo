//
//  DBTableField.h
//  PDdatabase
//

#import <Foundation/Foundation.h>

#define fieldWithNameAndTypes(name, type) [DBTableField tableFieldWithName:name dataType:type]

typedef int DBTableFieldDataType;
enum DBTableFieldDataType
{
    DB_NULL = 0,
    DB_INTEGER = 1,
    DB_PrymaryINTEGER = 2,
    DB_PrymaryREAL = 3,
    DB_REAL = 4,
    DB_TEXT = 5,
    DB_PrymaryTEXT = 6,
    DB_BLOB = 7,
    
};

@interface DBTableField : NSObject
{
    NSString *fieldName;
    NSString *fieldType;
}

+ (DBTableField *)tableFieldWithName:(NSString *)name
                            dataType:(DBTableFieldDataType)type;

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *type;
@property (nonatomic, readonly) DBTableFieldDataType dataType;

@end
