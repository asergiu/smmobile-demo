//
//  PDMResults.cpp
//  PD-Measurement
//
//#include "stdafx.h"
#include "PDMResults.h"

CPDMFrameMarkers frameMarkers;

// check properties value
template<typename T>
inline bool isnan_check(T value)
{
    return value != value;
    
}
#include <limits>
template<typename T>
inline bool isinf_check(T value)
{
    return std::numeric_limits<T>::has_infinity &&
    value == std::numeric_limits<T>::infinity();
}
// true - исходное значение переменной правильное
// false - исходное значение переменной не правильное
inline bool checkDoubleValue(double value)
{
    bool bRes = false;
    bRes = !(isinf_check(value) || isnan_check(value));
    return bRes;
}
inline bool checkDoubleValue2(double& value, double def_value)
{
    bool bRes = checkDoubleValue(value);
    if (bRes == false) {
        value = def_value;
    }
    return bRes;
}
inline bool checkDoubleValue2_limits(double& value, double def_value, double min_value, double max_value)
{
    bool bRes = checkDoubleValue(value);
    if (bRes == false) {
        value = def_value;
    }
    else {
        if (value < min_value || value > max_value) {
            value = def_value;
            bRes = false;
        }
    }
    return bRes;
}





CPDMPhotoData::CPDMPhotoData()
{
    // Device type
    nDevice = niPadNew;
    // Photo mode
    nPhotoMode = nNoData;
    // Support data
    xLeftSupport = 0.;
    yLeftSupport = 0.;
    xRightSupport = 0.;
    yRightSupport = 0.;
    xCenterSupport = 0.;
    yCenterSupport = 0.;
    // Eyes data
    xRightEye = 0.;
    yRightEye = 0.;
    xLeftEye = 0.;
    yLeftEye = 0.;
    // Frame data
    xLeftRect = 0.;
    yLeftRect = 0.;
    xRightRect = 0.;
    yRightRect = 0.;
    xLeftUpRect = 0.;
    yLeftUpRect = 0.;
    xRightUpRect = 0.;
    yRightUpRect = 0.;
    // Device angle
    DeviceAngle = 0.;

    double kZoom = 1.25 / 1.7;
    kFarPD = 360.;//376.;
    kNearPD = 239.6 * kZoom;
}
// Проверяем корректность данных
void CPDMPhotoData::checkPhotoData()
{
    if (nPhotoMode == nNoData) {
        return;
    }
    else if (nPhotoMode == nFarPD) {
        // Support data
        checkDoubleValue2(xLeftSupport, 842.);
        checkDoubleValue2(yLeftSupport, 225.);
        checkDoubleValue2(xRightSupport, 171.);
        checkDoubleValue2(yRightSupport, 225.);
        checkDoubleValue2(xCenterSupport, 509.);
        checkDoubleValue2(yCenterSupport, 230.);
        // Eyes data
        checkDoubleValue2(xRightEye, 307.);
        checkDoubleValue2(yRightEye, 308.);
        checkDoubleValue2(xLeftEye, 702.);
        checkDoubleValue2(yLeftEye, 315.);
        // Frame data
        checkDoubleValue2(xLeftRect, 567.);
        checkDoubleValue2(yLeftRect, 458.);
        checkDoubleValue2(xRightRect, 446.);
        checkDoubleValue2(yRightRect, 459.);
        checkDoubleValue2(xLeftUpRect, 836.);
        checkDoubleValue2(yLeftUpRect, 262.);
        checkDoubleValue2(xRightUpRect, 172.);
        checkDoubleValue2(yRightUpRect, 264.);
        // Device angle
        checkDoubleValue2(DeviceAngle, -0.01765);
    }
    else
    {
        // Support data
        checkDoubleValue2(xLeftSupport, 820.);
        checkDoubleValue2(yLeftSupport, 175.);
        checkDoubleValue2(xRightSupport, 154.);
        checkDoubleValue2(yRightSupport, 175.);
        checkDoubleValue2(xCenterSupport, 484.);
        checkDoubleValue2(yCenterSupport, 54.);
        // Eyes data
        checkDoubleValue2(xRightEye, 305.);
        checkDoubleValue2(yRightEye, 354.);
        checkDoubleValue2(xLeftEye, 679.);
        checkDoubleValue2(yLeftEye, 359.);
        // Frame data
        checkDoubleValue2(xLeftRect, 548.);
        checkDoubleValue2(yLeftRect, 432.);
        checkDoubleValue2(xRightRect, 428.);
        checkDoubleValue2(yRightRect, 434.);
        checkDoubleValue2(xLeftUpRect, 1.5);
        checkDoubleValue2(yLeftUpRect, -1.5);
        checkDoubleValue2(xRightUpRect, -1.5);
        checkDoubleValue2(yRightUpRect, -1.5);
        // Device angle
        checkDoubleValue2(DeviceAngle, -0.01765);
    }
}
// функция переводит размер в экранных пикселях в миллиметры на основании размеров Суппорта
// Dist – расстояние от камеры до оправы
// alfaPan - угол поворота головы "вправо - влево"
double CPDMPhotoData::convertToCM(double pixel, double alfaPan, double Dist)
{
    if (nPhotoMode == nNoData)
        return 0.;
    // корректируем расстояние
    double DistPixel = 0.;
    if (nPhotoMode == nFarPD) {
        // корректное расстояние между крайними точками Суппорта на глубине оправы в пикселях
        DistPixel = 1000. * kFarPD / Dist;
    }
    else {
        // корректное расстояние между крайними точками Суппорта на глубине оправы в пикселях
        DistPixel = 1000. * kNearPD / Dist;
    }
    // вычисляем Scale factor
    double delta = DistPixel / kPDmm;
    // расстояние в мм для PD
    return pixel / delta;
}
// функция переводит размер в миллиметрах в экранные пиксели на основании размеров Суппорта
double CPDMPhotoData::convertToPixel(double mm, double Dist)
{
    double DistPixel = 0.;
    if (nPhotoMode == nFarPD) {
        // корректное расстояние между крайними точками Суппорта на глубине оправы в пикселях
        DistPixel = 1000. * kFarPD / Dist;
    }
    else {
        // корректное расстояние между крайними точками Суппорта на глубине оправы в пикселях
        DistPixel = 1000. * kNearPD / Dist;
    }
    // вычисляем Scale factor
    double delta = mm / kPDmm;
    // расстояние в пикселях
    return DistPixel * delta;
}
// вычисление расстояния съемки до оправы
double CPDMPhotoData::PhotoDistance(double PanAngle)
{
    if (nPhotoMode == nNoData)
        return 0.;
    double Dist = 0;
    // Зная координаты левой и правой точек Суппорта, определяем расстояние между ними
    // alpha - угол наклона головы "вправо - влево"
    double alpha = CalcHeadTiltAngle();
    // PanAngle - угол поворота головы "вправо - влево"
    // Теперь мы знаем расстояние между точками Суппорта в пикселах.
    // Определяем расстояние от Суппорда до iPad
    // Используем табличное соотношение: Расстояние 1000 мм соответствует 424 пикселей между точками
    // Используем линейную интерполяцию, и учитываем, что при увеличении дистанции съемки , расстояние между точками уменьшается
//    double DistPixel = ((xLeftSupport - xRightSupport) / cos(alpha)) / cos(beta);
//    if (nPhotoMode == nFarPD) {
//        Dist = 1000. * kFarPD / DistPixel;
//    }
//    else {
//        Dist = 1000. * kNearPD / DistPixel;
//    }
    
    double k = 0.;
    if (nPhotoMode == nFarPD) {
        k = kPDmm / (1000. * kFarPD);
    }
    else {
        k = kPDmm / (1000. * kNearPD);
    }
    double dxp = (xLeftSupport - xRightSupport) / cos(alpha);
    double T = (dxp * k) / (sup.acDist * cos(PanAngle));
    double a = T;
    double b = -2.;
    double c = -T * sup.acDist * sup.acDist * sin(PanAngle) * sin(PanAngle);
    
    Dist = (-b + sqrt(b * b - 4. * a * c)) / (2. * a);
    
    return Dist;
}
double CPDMPhotoData::CalcHeadPanAngle2(double PanAngle, double PantoAngle)
{
	if (nPhotoMode == nNoData)
		return 0.;
	// PanAngle - угол поворота головы "вправо - влево"

	// делаем поворот, выравниваем наклон головы "вправо - влево"
	double xl, yl, xc, yc, xr, yr;
	double dx, dy;
	double fi = -CalcHeadTiltAngle();   // угол поворота
	// поворот делаем относительно точки Right
	xr = xRightSupport;
	yr = yRightSupport;
	// поворачиваем Левую точку относительно правой
	dx = xLeftSupport - xr;
	dy = yLeftSupport - yr;
	xl = dx * cos(fi) - dy * sin(fi);
	yl = dx * sin(fi) + dy * cos(fi);
	xl += xr;
	yl += yr;
	// поворачиваем Центральную точку относительно правой
	dx = xCenterSupport - xr;
	dy = yCenterSupport - yr;
	xc = dx * cos(fi) - dy * sin(fi);
	yc = dx * sin(fi) + dy * cos(fi);
	xc += xr;
	yc += yr;

	double k = 0.;
	if (nPhotoMode == nFarPD) {
		k = kPDmm / (1000. * kFarPD);
	}
	else {
		k = kPDmm / (1000. * kNearPD);
	}

	int i = 0;
	double Angle[400], Delta[400];
	double AngleStart = PanAngle - 0.1;
	double AngleStop = PanAngle + 0.1;
	double dAngle = 0.0005;

	for(i = 0; i < 400; i++) {
		Angle[i] = AngleStart + dAngle * i;

		double dxLR = (xl - xr);
		double a = k * dxLR;
		double b = -2. * sup.acDist * cos(Angle[i]);
		double c = -k * dxLR * sup.acDist * sup.acDist * sin(Angle[i]) * sin(Angle[i]);

		double Dist = (-b + sqrt(b * b - 4. * a * c)) / (2. * a);

		double dxLP = (xl - xc);
		double x2 = sup.SupportHookB * cos(PantoAngle);

		Delta[i] = (sup.acDist * cos(Angle[i])) / (Dist - sup.acDist * sin(Angle[i])) + 
			(x2 * sin(Angle[i])) / (Dist - x2 * cos(Angle[i])) - k * dxLP;
	}

	double dMin = fabs(Delta[0]);
	int iMin = 0;
	for(i = 0; i < 400; i++) {
		if(dMin > fabs(Delta[i])) {
			dMin = fabs(Delta[i]);
			iMin = i;
		}
	}

	return Angle[iMin];
}
double CPDMPhotoData::CalcHeadPanAngle3(double PanAngle, double PantoAngle)
{
    if (nPhotoMode == nNoData)
        return 0.;
    // PanAngle - угол поворота головы "вправо - влево"
    
    // делаем поворот, выравниваем наклон головы "вправо - влево"
    double xl, yl, xc, yc, xr, yr;
    double dx, dy;
    double fi = -CalcHeadTiltAngle();   // угол поворота
    // поворот делаем относительно точки Right
    xr = xRightSupport;
    yr = yRightSupport;
    // поворачиваем Левую точку относительно правой
    dx = xLeftSupport - xr;
    dy = yLeftSupport - yr;
    xl = dx * cos(fi) - dy * sin(fi);
    yl = dx * sin(fi) + dy * cos(fi);
    xl += xr;
    yl += yr;
    // поворачиваем Центральную точку относительно правой
    dx = xCenterSupport - xr;
    dy = yCenterSupport - yr;
    xc = dx * cos(fi) - dy * sin(fi);
    yc = dx * sin(fi) + dy * cos(fi);
    xc += xr;
    yc += yr;
    
    int i = 0;
    double Angle[400], Delta[400];
    double AngleStart = PanAngle - 0.1;
    double dAngle = 0.0005;
    
    // Центральная точка Суппорта
    double xCpt = sup.SupportHookB * cos(PantoAngle) + sup.SupportHookA * sin(PantoAngle);
    double yCpt = -sup.SupportHookB * sin(PantoAngle) + sup.SupportHookA * cos(PantoAngle);

    for(i = 0; i < 400; i++) {
        Angle[i] = AngleStart + dAngle * i;
        
        double Dist = PhotoDistance(Angle[i]);
        // Camera
        xyzPoint ptCamera;
        ptCamera.x = 0.;
        ptCamera.y = -Dist;
        ptCamera.z = 0.;
        // Правая на экране точка Суппорта
        xyzPoint ptSupL;
        ptSupL.x = sup.acDist * cos(Angle[i]);
        ptSupL.y = -sup.acDist * sin(Angle[i]);
        ptSupL.z = 0.;
        // Центральная точка Суппорта
        xyzPoint ptSupC;
        ptSupC.x = -xCpt * sin(Angle[i]);
        ptSupC.y = -xCpt * cos(Angle[i]);
        ptSupC.z = yCpt;
        
        double kX1, bX1, kZ1, bZ1;
        double kX2, bX2, kZ2, bZ2;
        CPDMResults::CalcLineCoefficients(ptCamera, ptSupL, kX1, bX1, kZ1, bZ1);
        CPDMResults::CalcLineCoefficients(ptCamera, ptSupC, kX2, bX2, kZ2, bZ2);
        Delta[i] = (bX1 - bX2) - convertToCM(xl - xc, Angle[i], Dist);
    }
    
    double dMin = fabs(Delta[0]);
    int iMin = 0;
    for(i = 0; i < 400; i++) {
        if(dMin > fabs(Delta[i])) {
            dMin = fabs(Delta[i]);
            iMin = i;
        }
    }
    
    return Angle[iMin];
}
// функция Конвергенции
double CPDMPhotoData::correctPD(double srcPD, double Dist, double VD)
{
    if (nPhotoMode == nNoData)
        return 0.;
    // VD - vertex distance
    // Вычисляем PD с учетом конвергенции
    double corPD = srcPD * (Dist + EyeRadius + VD) / Dist;
    return corPD;
}
// вычисляем угол наклона головы "вправо - влево"
double CPDMPhotoData::CalcHeadTiltAngle()
{
    if (nPhotoMode == nNoData)
        return 0.;
    // alfa - угол наклона головы
    double alfa = 0.;
    // Зная координаты левой и правой точек Суппорта, определяем расстояние между ними
    double dx = xLeftSupport - xRightSupport;
    double dy = yLeftSupport - yRightSupport;
    double DistPixel = sqrt(dx * dx + dy * dy);
    // определяем угол
    alfa = asin(dy / DistPixel);
    return alfa;
}
// вычисляем угол поворота головы "вправо - влево"
double CPDMPhotoData::CalcHeadPanAngle()
{
    if (nPhotoMode == nNoData)
        return 0.;
    // alfa - угол поворота головы
    double alfa = 0.;
    // делаем поворот, выравниваем наклон головы "вправо - влево"
    double xl, yl, xc, yc, xr, yr;
    double dx, dy;
    double fi = -CalcHeadTiltAngle();   // угол поворота
    // поворот делаем относительно точки Right
    xr = xRightSupport;
    yr = yRightSupport;
    // поворачиваем Левую точку относительно правой
    dx = xLeftSupport - xr;
    dy = yLeftSupport - yr;
    xl = dx * cos(fi) - dy * sin(fi);
    yl = dx * sin(fi) + dy * cos(fi);
    xl += xr;
    yl += yr;
    // поворачиваем Центральную точку относительно правой
    dx = xCenterSupport - xr;
    dy = yCenterSupport - yr;
    xc = dx * cos(fi) - dy * sin(fi);
    yc = dx * sin(fi) + dy * cos(fi);
    xc += xr;
    yc += yr;
    
    // определяем положение точки, находящейся посередине между Правой и Левой точками Суппорта
    double xcSup = (xl + xr) / 2.;
    // учитываем пантоскопический угол 0 градусов
    double PanAngle = 0.;
    // в идеале тут надо учитывать реальный пантоскопический угол оправы, но в данной точке мы его не знаем. А чтобы вычислить его надо знать угол вопорота головы, который вычисляется данной функцией
    // x1, y1 - координаты центральной точки при наклоне оправы равном 0
    // x2, y2 - координаты центральной точки при наклоне оправы равном Пантоскопическому углу
    double x1, y1, x2, y2;
    fi = -M_PI * PanAngle / 180.;   // угол поворота
    x1 = sup.SupportHookB;
    y1 = sup.SupportHookA;
    x2 = x1 * cos(fi) - y1 * sin(fi);
    y2 = x1 * sin(fi) + y1 * cos(fi);
    // определяем угол
    alfa = atan(((xcSup - xc) * sup.acDist) / ((xcSup - xr) * x2));

    return alfa;
}
// вычисляем пантоскопический угол
double CPDMPhotoData::CalcPantoskopicheskyCorner(double PanAngle, double Dist)
{
    if (nPhotoMode == nNoData)
        return 0.;
    // alfa - пантоскопический угол
    double alfa = 0.;
    // вычисляем возвышение центральной точки Суппорта над линей, соединяющей крайние
    // учитываем наклон головы "вправо - влево", для этого делаем поворот
    double fi = -CalcHeadTiltAngle();   // угол поворота
    double dxCS = xCenterSupport - xRightSupport;
    double dyCS = yCenterSupport - yRightSupport;
    double Y = dxCS * sin(fi) + dyCS * cos(fi);
    // учитываем параллакс
    double CenterSupportElevation = convertToCM(Y, PanAngle, Dist - sup.SupportHookB);
//    double HP = CalcParallax();
//    CenterSupportElevation -= HP;
    // x1, y1 - координаты центральной точки при наклоне оправы равном 0
    // x2, y2 - координаты центральной точки при наклоне оправы равном Пантоскопическому углу
    double x1, y1, x2, y2;
    x1 = sup.SupportHookB;
    y1 = -sup.SupportHookA;
    x2 = sqrt((pow(sup.SupportHookB, 2) + pow(sup.SupportHookA, 2)) - pow(CenterSupportElevation, 2));
    y2 = CenterSupportElevation;
    // alfa - пантоскопический угол относительно взгляда камеры
    alfa = -atan((y2 * x1 - x2 * y1) / (x2 * x1 + y2 * y1));
    return alfa;
}
double CPDMPhotoData::CalcPantoskopicheskyCorner3(double PanAngle, double Dist)
{
    if (nPhotoMode == nNoData)
        return 0.;
    // alfa - пантоскопический угол
    double alfa = 0.;

    // делаем поворот, выравниваем наклон головы "вправо - влево"
    double xl, yl, xc, yc, xr, yr;
    double dx, dy;
    double fi = -CalcHeadTiltAngle();   // угол поворота
    // поворот делаем относительно точки Right
    xr = xRightSupport;
    yr = yRightSupport;
    // поворачиваем Левую точку относительно правой
    dx = xLeftSupport - xr;
    dy = yLeftSupport - yr;
    xl = dx * cos(fi) - dy * sin(fi);
    yl = dx * sin(fi) + dy * cos(fi);
    xl += xr;
    yl += yr;
    // поворачиваем Центральную точку относительно правой
    dx = xCenterSupport - xr;
    dy = yCenterSupport - yr;
    xc = dx * cos(fi) - dy * sin(fi);
    yc = dx * sin(fi) + dy * cos(fi);
    xc += xr;
    yc += yr;
    
    double kX1, bX1, kZ1, bZ1;
    double kX2, bX2, kZ2, bZ2;
    // Camera
    xyzPoint ptCamera;
    ptCamera.x = 0.;
    ptCamera.y = -Dist;
    ptCamera.z = 0.;
    // Правая на экране точка Суппорта
    xyzPoint ptSupL;
    ptSupL.x = sup.acDist * cos(PanAngle);
    ptSupL.y = -sup.acDist * sin(PanAngle);
    ptSupL.z = 0.;

    CPDMResults::CalcLineCoefficients(ptCamera, ptSupL, kX1, bX1, kZ1, bZ1);
    xyzPoint ptSupLp;
    ptSupLp.y = 0.;
    ptSupLp.x = kX1 * ptSupLp.y + bX1;
    ptSupLp.z = kZ1 * ptSupLp.y + bZ1;
    
    // Центральная точка Суппорта
    xyzPoint ptSupCp;
    ptSupCp.x = ptSupLp.x + convertToCM(xc - xl, PanAngle, Dist);
    ptSupCp.y = 0.;
    ptSupCp.z = convertToCM(yl - yc, PanAngle, Dist);
    
    CPDMResults::CalcLineCoefficients(ptCamera, ptSupCp, kX2, bX2, kZ2, bZ2);
    
    double l2 = sup.SupportHookA * sup.SupportHookA + sup.SupportHookB * sup.SupportHookB;
    double l1 = sqrt(l2);
    double a = 1. + kX2 * kX2 + kZ2 * kZ2;
    double b = 2. * bX2 * kX2 + 2. * bZ2 * kZ2;
    double c = bX2 * bX2 + bZ2 * bZ2 - l2;
    
    xyzPoint ptSupC;
    ptSupC.y = (-b - sqrt(b * b - 4. * a * c)) / (2. * a);
    ptSupC.x = kX2 * ptSupC.y + bX2;
    ptSupC.z = kZ2 * ptSupC.y + bZ2;
    
    double xyLen = sqrt(ptSupC.x * ptSupC.x + ptSupC.y * ptSupC.y);
    double xyzLen = sqrt(ptSupC.x * ptSupC.x + ptSupC.y * ptSupC.y + ptSupC.z * ptSupC.z);

    // alfa - пантоскопический угол относительно взгляда камеры
    alfa = atan(sup.SupportHookA / sup.SupportHookB) - atan(ptSupC.z / xyLen);
    return alfa;
}
// пантоскопический угол относительно отвеса
double CPDMPhotoData::CalcPantoskopicheskyCornerHor(double PanAngle, double Dist, double PantoskAngle)
{
    // alfa - пантоскопический угол
    double alfa = 0.;
    // размеры предыдущего окна картинки (см. storyboard)
    // width = 991; height = 615;
    double xCenterPhoto = 496;  // центральная точка окна по оси X
    double yCenterPhoto = 308;  // центральная точка окна по оси Y
/*
    if (nPhotoMode == nFarPD) {
        // вносим коррекцию положения центра при переходе фото от 1-го экрана ко 2-му
        xCenterPhoto += 1;
        yCenterPhoto += 4;
    }
*/ 
    // вначале делаем поворот относительно центральной точки фото, выравниваем наклон головы "вправо - влево"
    double xlSupport, ylSupport, xrSupport, yrSupport;
    double dx, dy;
    double fi = -CalcHeadTiltAngle();   // угол поворота
    // поворачиваем Левую точку Суппорта
    dx = xLeftSupport - xCenterPhoto;
    dy = yLeftSupport - yCenterPhoto;
    xlSupport = dx * cos(fi) - dy * sin(fi);
    ylSupport = dx * sin(fi) + dy * cos(fi);
    xlSupport += xCenterPhoto;
    ylSupport += yCenterPhoto;
    // поворачиваем Правую точку Суппорта
    dx = xRightSupport - xCenterPhoto;
    dy = yRightSupport - yCenterPhoto;
    xrSupport = dx * cos(fi) - dy * sin(fi);
    yrSupport = dx * sin(fi) + dy * cos(fi);
    xrSupport += xCenterPhoto;
    yrSupport += yCenterPhoto;
    // определяем в пикселях возвышение конуса Суппорта
    double H = convertToPixel(sup.SupportHookA, Dist);
    double dhCenter = convertToCM(yCenterPhoto - (yrSupport - H), PanAngle, Dist);
    double vzg = atan(dhCenter / Dist);
    alfa = PantoskAngle + DeviceAngle + vzg;
    return alfa;
}
// вычисляем параллакс центральной точки конуса суппорта
double CPDMPhotoData::CalcParallax(double PanAngle, double Dist)
{
    if (nPhotoMode == nNoData)
        return 0.;
    double HP = 0.; // величина параллакса по оси Y
    // размеры предыдущего окна картинки (см. storyboard)
    // width = 991; height = 615;
    double xCenterPhoto = 496;  // центральная точка окна по оси X
    double yCenterPhoto = 308;  // центральная точка окна по оси Y
    // Для определения параллакса нам необходимо знать отклонение центральной точки суппорта от центральной точки окна. Если отклонение равно 0, то и параллакс равен 0.
    // но вначале делаем поворот, выравниваем наклон головы "вправо - влево"
    double xc, yc;
    double dx, dy;
    double fi = -CalcHeadTiltAngle();   // угол поворота
    // поворот делаем относительно центральной точки фото (центральная точка окна)
    // поворачиваем Центральную точку суппорта
    dx = xCenterSupport - xCenterPhoto;
    dy = yCenterSupport - yCenterPhoto;
    xc = dx * cos(fi) - dy * sin(fi);
    yc = dx * sin(fi) + dy * cos(fi);
    xc += xCenterPhoto;
    yc += yCenterPhoto;
    // вычисляем смещение в мм
    double H = convertToCM(yc - yCenterPhoto, PanAngle, Dist);
    double Phi = atan(H / Dist);
    // вычисляем параллакс
    HP = sup.SupportHookB * tan(Phi);
    return HP;
}
// вычисляем угол сгиба оправы
// алгоритм опирается на наличие угла поворота
double CPDMPhotoData::CalcWrapAngle(double alfaPan, double Dist)
{
    double angleWrap = 0.;
    if (nPhotoMode == nNoData)
        return 0.;
    
    // делаем поворот, выравниваем наклон головы "вправо - влево"
    double xlSupport, ylSupport, xrSupport, yrSupport;
    double xlRect, ylRect, xrRect, yrRect;
    double xlUpRect, ylUpRect, xrUpRect, yrUpRect;
    double dx, dy;
    double fi = -CalcHeadTiltAngle();   // угол поворота
    // поворот делаем относительно точки Right Суппорта
    xrSupport = xRightSupport;
    yrSupport = yRightSupport;
    // поворачиваем Левую точку Суппорта относительно правой точки Суппорта
    dx = xLeftSupport - xrSupport;
    dy = yLeftSupport - yrSupport;
    xlSupport = dx * cos(fi) - dy * sin(fi);
    ylSupport = dx * sin(fi) + dy * cos(fi);
    xlSupport += xrSupport;
    ylSupport += yrSupport;
    // поворачиваем точку RightRect относительно правой точки Суппорта
    dx = xRightRect - xrSupport;
    dy = yRightRect - yrSupport;
    xrRect = dx * cos(fi) - dy * sin(fi);
    yrRect = dx * sin(fi) + dy * cos(fi);
    xrRect += xrSupport;
    yrRect += yrSupport;
    // поворачиваем точку LeftRect относительно правой точки Суппорта
    dx = xLeftRect - xrSupport;
    dy = yLeftRect - yrSupport;
    xlRect = dx * cos(fi) - dy * sin(fi);
    ylRect = dx * sin(fi) + dy * cos(fi);
    xlRect += xrSupport;
    ylRect += yrSupport;
    // поворачиваем точку RightUpRect относительно правой точки Суппорта
    dx = xRightUpRect - xrSupport;
    dy = yRightUpRect - yrSupport;
    xrUpRect = dx * cos(fi) - dy * sin(fi);
    yrUpRect = dx * sin(fi) + dy * cos(fi);
    xrUpRect += xrSupport;
    yrUpRect += yrSupport;
    // поворачиваем точку LeftUpRect относительно правой точки Суппорта
    dx = xLeftUpRect - xrSupport;
    dy = yLeftUpRect - yrSupport;
    xlUpRect = dx * cos(fi) - dy * sin(fi);
    ylUpRect = dx * sin(fi) + dy * cos(fi);
    xlUpRect += xrSupport;
    ylUpRect += yrSupport;
    
    // beta - угол поворота головы "вправо - влево"
    
    // Центральная точка
    double centerX = CalcCenterPoint(xrRect, xlRect, Dist, alfaPan);
    
    double xObjective[2], yObjective[2];
    xObjective[0] = Dist * sin(alfaPan);
    yObjective[0] = -Dist * cos(alfaPan);
    xObjective[1] = Dist * sin(-alfaPan);
    yObjective[1] = -Dist * cos(-alfaPan);

    double CP2, CP3;
    double xCP2[2], yCP2[2], xCP3[2], yCP3[2];
    CP2 = convertToCM(centerX - xrUpRect, alfaPan, Dist);
    xCP2[0] = -CP2 * cos(alfaPan);
    yCP2[0] = -CP2 * sin(alfaPan);
    CP3 = convertToCM(centerX - xrRect, alfaPan, Dist);
    xCP3[0] = -CP3 * cos(alfaPan);
    yCP3[0] = -CP3 * sin(alfaPan);
    
    CP2 = convertToCM(xlUpRect - centerX, alfaPan, Dist);
    xCP2[1] = -CP2 * cos(alfaPan);   // с переворотом
    yCP2[1] = CP2 * sin(alfaPan);
    CP3 = convertToCM(xlRect - centerX, alfaPan, Dist);
    xCP3[1] = -CP3 * cos(alfaPan);   // с переворотом
    yCP3[1] = CP3 * sin(alfaPan);
    
    double kA1[2], kA2[2], kB1[2], kB2[2], kC1[2], kC2[2];
    // коэффициенты линии
    kA1[0] = yCP2[0] - yObjective[0];
    kB1[0] = xObjective[0] - xCP2[0];
    kC1[0] = yObjective[0] * (xCP2[0] - xObjective[0]) - xObjective[0] * (yCP2[0] - yObjective[0]);
    kA1[1] = yCP2[1] - yObjective[1];
    kB1[1] = xObjective[1] - xCP2[1];
    kC1[1] = yObjective[1] * (xCP2[1] - xObjective[1]) - xObjective[1] * (yCP2[1] - yObjective[1]);
    
    kA2[0] = yCP3[0] - yObjective[0];
    kB2[0] = xObjective[0] - xCP3[0];
    kC2[0] = yObjective[0] * (xCP3[0] - xObjective[0]) - xObjective[0] * (yCP3[0] - yObjective[0]);
    kA2[1] = yCP3[1] - yObjective[1];
    kB2[1] = xObjective[1] - xCP3[1];
    kC2[1] = yObjective[1] * (xCP3[1] - xObjective[1]) - xObjective[1] * (yCP3[1] - yObjective[1]);
    
    // точка пересечения прямых
    // Up rect
    double del1 = kA1[0] * kB1[1] - kA1[1] * kB1[0];
    if(fabs(del1) < 0.001)
        return 0.;
    double dxUpRect = (kC1[1] * kB1[0] - kC1[0] * kB1[1]) / del1;
    double dyUpRect = (kA1[1] * kC1[0] - kA1[0] * kC1[1]) / del1;
    // левый глаз
    double del2 = kA2[0] * kB2[1] - kA2[1] * kB2[0];
    if(fabs(del2) < 0.001)
        return 0.;
    double dxRect = (kC2[1] * kB2[0] - kC2[0] * kB2[1]) / del2;
    double dyRect = (kA2[1] * kC2[0] - kA2[0] * kC2[1]) / del2;

    // вычисляем Wrap угол
    double dxFrame = fabs(dxRect - dxUpRect);
    double dyFrame = fabs(dyUpRect - dyRect);
    angleWrap = atan(dyFrame / dxFrame);
    
    if (angleWrap * 180. / M_PI < 0. || angleWrap * 180. / M_PI > maxWrapAngle) {
        angleWrap = 0.;
    }
    
    return angleWrap;
}
// определение центральной точки на проекции отрезка, расположенного под углом
double CPDMPhotoData::CalcCenterPoint(double RightPoint, double LeftPoint, double Dist, double PanAngle)
{
    if (nPhotoMode == nNoData)
        return 0.;
    double k = 0.;
    if (nPhotoMode == nFarPD) {
        k = kPDmm / (1000. * kFarPD);
    }
    else {
        k = kPDmm / (1000. * kNearPD);
    }
    double dxp = LeftPoint - RightPoint;
    double a = k * dxp * sin(PanAngle) * sin(PanAngle);
    double b = 2. * Dist * cos(PanAngle);
    double c = -k * dxp * Dist * Dist;
    double R = (-b + sqrt(b * b - 4. * a * c)) / (2. * a);
    double X1 = (R * Dist * cos(PanAngle)) / (Dist + R * sin(PanAngle));
    double X2 = (R * Dist * cos(PanAngle)) / (Dist - R * sin(PanAngle));
    double T = X1 / X2;
    return RightPoint + (T * dxp) / (1. + T);
}
// определение точки пересечения 2-ух прямых, заданных 2-мя точками
bool CPDMPhotoData::CalcPointOfIntersection(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double& x, double& y)
{
    bool bRes = false;
    
    // коэффициенты линий
    double kA1 = y2 - y1;
    double kB1 = x1 - x2;
    double kC1 = y1 * (x2 - x1) - x1 * (y2 - y1);
    double kA2 = y4 - y3;
    double kB2 = x3 - x4;
    double kC2 = y3 * (x4 - x3) - x3 * (y4 - y3);
    // точка пересечения прямых
    double del = kA1 * kB2 - kA2 * kB1;
    if(fabs(del) < 0.001)
        return false;
    x = (kC2 * kB1 - kC1 * kB2) / del;
    y = (kA2 * kC1 - kA1 * kC2) / del;
    bRes = true;
    
    return bRes;
}





CPDMDataResults::CPDMDataResults()
{
    // Data results presents
    bDataResults = false;
    // alfaTilt - угол наклона головы "вправо - влево"
    alfaTilt = 0.;
    // alfaPan - угол поворота головы "вправо - влево"
    alfaPan = 0.;
    // alfaPantosk - пантоскопический угол
    alfaPantosk = 0.;
    // alfaPantoskHor - пантоскопический угол относительно отвеса
    alfaPantoskHor = 0.;
    // alfaWrap - угол сгиба оправы
    alfaWrap = 0.;
    // Dist - расстояние съемки
    Dist = 0.;
    // PD - межцентровое расстояние
    LeftPD = 0.;
    RightPD = 0.;
    // Высоты
    LeftHeight = 0.;
    RightHeight = 0.;
    // Расстояние между линзами
    Bridge = 0.;
    // Размеры оправы
    FrameWidth = 0.;
    FrameHeight = 0.;
    // VD - vertex distance
    LeftVD = 14.;
    RightVD = 14.;
    // Head rotation correction
    bHeadRotation = true;
    // default Wrap angle - 5 degree
    defWrapAngle = 5. * M_PI / 180.;
    // default Vertex
    defVertex = 14.;
    // Lens center points shift
    dxLeft = dyLeft = dxRight = dyRight = 0.;
    // pixel in mm
    pixelInMM_dxLeft = pixelInMM_dyLeft = pixelInMM_dxRight = pixelInMM_dyRight = 1.;
}
void CPDMDataResults::checkResultsData(int iPhoto)
{
    if (bDataResults) {
        if (iPhoto == 0 || iPhoto == 1) {
            // alfaTilt - угол наклона головы "вправо - влево"
            checkDoubleValue2(alfaTilt, 0.);
            // alfaPan - угол поворота головы "вправо - влево"
            checkDoubleValue2(alfaPan, 0.);
            // alfaPantosk - пантоскопический угол
            checkDoubleValue2(alfaPantosk, 0.176);
            // alfaPantoskHor - пантоскопический угол относительно отвеса
            checkDoubleValue2(alfaPantoskHor, 0.194);
            // alfaWrap - угол сгиба оправы
            checkDoubleValue2(alfaWrap, 0.);
            // Dist - расстояние съемки
            checkDoubleValue2(Dist, 536.);
            // PD - межцентровое расстояние
            checkDoubleValue2(LeftPD, 34.7);
            checkDoubleValue2(RightPD, 34.7);
            // Высоты
            checkDoubleValue2(LeftHeight, 25.);
            checkDoubleValue2(RightHeight, 25.);
            // Расстояние между линзами
            checkDoubleValue2(Bridge, 20.);
            // Размеры оправы
            checkDoubleValue2(FrameWidth, 45.);
            checkDoubleValue2(FrameHeight, 33.);
            // VD - vertex distance
            checkDoubleValue2(LeftVD, 12.);
            checkDoubleValue2(RightVD, 12.);
            // default Wrap angle - 5 degree
            checkDoubleValue2(defWrapAngle, 5. * M_PI / 180.);
            // default Vertex
            checkDoubleValue2(defVertex, 14.);
            // Lens center points shift
            checkDoubleValue2(dxLeft, 0.);
            checkDoubleValue2(dyLeft, 0.);
            checkDoubleValue2(dxRight, 0.);
            checkDoubleValue2(dyRight, 0.);
            // pixel in mm
            checkDoubleValue2(pixelInMM_dxLeft, 5.9);
            checkDoubleValue2(pixelInMM_dyLeft, 5.9);
            checkDoubleValue2(pixelInMM_dxRight, 5.9);
            checkDoubleValue2(pixelInMM_dyRight, 5.9);
        }
        else {
            // alfaTilt - угол наклона головы "вправо - влево"
            checkDoubleValue2(alfaTilt, 0.);
            // alfaPan - угол поворота головы "вправо - влево"
            checkDoubleValue2(alfaPan, 0.);
            // alfaPantosk - пантоскопический угол
            checkDoubleValue2(alfaPantosk, -0.35);
            // alfaPantoskHor - пантоскопический угол относительно отвеса
            checkDoubleValue2(alfaPantoskHor, -0.269);
            // alfaWrap - угол сгиба оправы
            checkDoubleValue2(alfaWrap, 0.);
            // Dist - расстояние съемки
            checkDoubleValue2(Dist, 265.);
            // PD - межцентровое расстояние
            checkDoubleValue2(LeftPD, 31.2);
            checkDoubleValue2(RightPD, 31.2);
            // Высоты
            checkDoubleValue2(LeftHeight, 5.);
            checkDoubleValue2(RightHeight, 5.);
            // Расстояние между линзами
            checkDoubleValue2(Bridge, 20.);
            // Размеры оправы
            checkDoubleValue2(FrameWidth, 45.);
            checkDoubleValue2(FrameHeight, 33.);
            // VD - vertex distance
            checkDoubleValue2(LeftVD, 12.);
            checkDoubleValue2(RightVD, 12.);
            // default Wrap angle - 5 degree
            checkDoubleValue2(defWrapAngle, 5. * M_PI / 180.);
            // default Vertex
            checkDoubleValue2(defVertex, 14.);
            // Lens center points shift
            checkDoubleValue2(dxLeft, 0.);
            checkDoubleValue2(dyLeft, 0.);
            checkDoubleValue2(dxRight, 0.);
            checkDoubleValue2(dyRight, 0.);
            // pixel in mm
            checkDoubleValue2(pixelInMM_dxLeft, 5.9);
            checkDoubleValue2(pixelInMM_dyLeft, 5.9);
            checkDoubleValue2(pixelInMM_dxRight, 5.9);
            checkDoubleValue2(pixelInMM_dyRight, 5.9);
        }
    }
}





CPDMDataResults2::CPDMDataResults2()
{
    // Data results presents
    bDataResults = false;
    // VD - vertex distance
    LeftVD = 0.;
    RightVD = 0.;
    // Wrap angle - угол сгиба оправы
    WrapAngle = 5. * M_PI / 180.;;
    WrapAngleMarkers[0] = WrapAngleMarkers[1] = WrapAngleMarkers[2] = 0.;
    WrapAngleMarkers[3] = WrapAngleMarkers[4] = 0.;
	// Размеры оправы
	FrameWidth = 0.;
	FrameHeight = 0.;
    // Progressive corridor
    Upfit = 0.;
    // Inset
    LeftInset = 0.;
    RightInset = 0.;
    // Diameter
    bDataDiameter = false;
    LeftDiameter = 75.;
    RightDiameter = 75.;
    bProgressiveDiameter = false;

	// Data work points presents
	bPointsResults = false;
    
    // Calculate WRAP angle status
    bWrapCalc = true;
    // Calculate Vertex status
    bVertexCalc = true;
}
void CPDMDataResults2::checkResults2Data()
{
    if (bDataResults) {
        // VD - vertex distance
        checkDoubleValue2(LeftVD, 12.);
        checkDoubleValue2(RightVD, 12.);
        // Wrap angle - угол сгиба оправы
        checkDoubleValue2(WrapAngle, 5. * M_PI / 180.);
        // Размеры оправы
        checkDoubleValue2(FrameWidth, 0.);
        checkDoubleValue2(FrameHeight, 0.);
        // Progressive corridor
        checkDoubleValue2(Upfit, 20.);
        // Inset
        checkDoubleValue2(LeftInset, 3.5);
        checkDoubleValue2(RightInset, 3.5);
        // Diameter
        checkDoubleValue2(LeftDiameter, 75.);
        checkDoubleValue2(RightDiameter, 75.);
    }
}





CPDMDevice::CPDMDevice()
{
	// Set Flash type 2 as default
    nFlashType = nFlashType_iPadAir2;
    nFarPhotoMode = nFarPD;
    nNearPhotoMode = nNearPD;
    UpdateData();
}
void CPDMDevice::SetFlashType(PDMFlashType useFlashType)
{
    nFlashType = useFlashType;
    UpdateData();
}
PDMFlashType CPDMDevice::GetFlashType()
{
    return nFlashType;
}
void CPDMDevice::SetPhotoMode(PDMPhotoMode nPhotoMode)
{
    switch (nPhotoMode) {
        case nNoData:
            break;
        case nFarPD:
            nFarPhotoMode = nPhotoMode;
            break;
        case nNearPD:
        case nNearPD180:
            nNearPhotoMode = nPhotoMode;
            break;
    }
    UpdateData();
}
void CPDMDevice::UpdateData()
{
    // Set Far Flash data
	switch(nFlashType) {
		case nFlashType1:
			zFarFlash = -52.;
			yFarFlash = 25.;
            xFarFlash = 0.;
			break;
		case nFlashType2:
			zFarFlash = -70.;
			yFarFlash = 20.;
            xFarFlash = 0.;
			break;
        case nFlashType_iPadAir2:
        case nFlashType_iPadPro10:
            zFarFlash = -25.;
            yFarFlash = 17.;
            xFarFlash = 0.;
            break;
        case nFlashType_iPadPro12:
            zFarFlash = -27.;
            yFarFlash = 17.;
            xFarFlash = 0.;
            break;
        case nFlashType_iPadMini:
            zFarFlash = -55.;
            yFarFlash = 17.;
            xFarFlash = 0.;
            break;
        case nFlashType_iPadPro11SelfNormal:
        case nFlashType_iPadPro12SelfNormal:
        case nFlashType_iPadPro11SelfHigh:
        case nFlashType_iPadPro12SelfHigh:
            zFarFlash = 0.;
            yFarFlash = 0.;
            xFarFlash = 15.;
            break;
	}
    // Set Near Flash data
	switch(nFlashType) {
		case nFlashType1:
			zNearFlash = 20.;
			yNearFlash = -8.;
			break;
		case nFlashType2:
			zNearFlash = 19.;
			yNearFlash = -8.;
            break;
        case nFlashType_iPadAir2:
        case nFlashType_iPadPro10:
        case nFlashType_iPadPro12:
            zNearFlash = 17.5;
            yNearFlash = 0.;
			break;
        case nFlashType_iPadMini:
            zNearFlash = 22.;
            yNearFlash = 0.;
            break;
        case nFlashType_iPadPro11SelfNormal:
        case nFlashType_iPadPro12SelfNormal:
            zNearFlash = -66.5;
            yNearFlash = 0.;
            break;
        case nFlashType_iPadPro11SelfHigh:
        case nFlashType_iPadPro12SelfHigh:
            zNearFlash = -120.0;
            yNearFlash = 0.;
            break;
	}
    if (nNearPhotoMode == nNearPD180) {
        zNearFlash *= -1;
    }
    // Set red circle position
    switch(nFlashType) {
        case nFlashType1:
        case nFlashType2:
        case nFlashType_iPadAir2:
            zRedCircle = -38.;
            break;
        case nFlashType_iPadPro10:
            zRedCircle = -40.;
            break;
        case nFlashType_iPadPro12:
            zRedCircle = -47.;
            break;
        case nFlashType_iPadMini:
            zRedCircle = -32.;
            break;
        case nFlashType_iPadPro11SelfNormal:
        case nFlashType_iPadPro12SelfNormal:
        case nFlashType_iPadPro11SelfHigh:
        case nFlashType_iPadPro12SelfHigh:
            zRedCircle = -43.;
            break;
    }
    if (nNearPhotoMode == nNearPD180) {
        zRedCircle = 109.5;
    }
    // Set device center position
	zDeviceCenter = -109.5;
    if (nNearPhotoMode == nNearPD180) {
        zDeviceCenter = 109.5;
    }
}





CPDMResults::CPDMResults()
{
    bHeadRotation = true;
    // Near correction using Far result
    bNearCorrection = false;
	// Near emulation using Far result
	bNearEmulation = false;
}
void CPDMResults::SetHeadRotationCorrection(bool bHeadRotation)
{
    CPDMResults::bHeadRotation = bHeadRotation;
}
void CPDMResults::SetNearCorrection(bool bNearCorrection)
{
    CPDMResults::bNearCorrection = bNearCorrection;
}
void CPDMResults::SetNearEmulation(bool bNearEmulation)
{
	CPDMResults::bNearEmulation = bNearEmulation;
}
void CPDMResults::NewSession()
{
    // Set calculation results to false
    Photo[0].nPhotoMode = nNoData;
    Photo[1].nPhotoMode = nNoData;
    Photo[2].nPhotoMode = nNoData;
    Results[0].bDataResults = false;
    Results[1].bDataResults = false;
    Results[2].bDataResults = false;
    Results2.bDataResults = false;
	Results2.bPointsResults = false;

    frameMarkers.NewSession();
    
    Results2.bDataDiameter = false;
    Results2.LeftDiameter = 75.;
    Results2.RightDiameter = 75.;
    Results2.bProgressiveDiameter = false;

    // Calculate WRAP angle status
    Results2.bWrapCalc = true;
    // Calculate Vertex status
    Results2.bVertexCalc = true;
    // default Wrap angle - 5 degree
    Results[0].defWrapAngle = 5. * M_PI / 180.;
    Results[1].defWrapAngle = 5. * M_PI / 180.;
    Results[2].defWrapAngle = 5. * M_PI / 180.;
    Results2.WrapAngle = 5. * M_PI / 180.;
    // default Vertex - 14 mm
    Results[0].defVertex = 14.;
    Results[1].defVertex = 14.;
    Results[2].defVertex = 14.;
}
// iPhoto: 0 - front Far mode photo; 1 - second Far mode photo; 2 - Near mode photo;
void CPDMResults::SetPhotoData(int iPhoto, PDMiPadDevice nDevice, PDMPhotoMode nPhotoMode, double xLeftSupport, double yLeftSupport, double xRightSupport, double yRightSupport, double xCenterSupport, double yCenterSupport, double xRightEye, double yRightEye, double xLeftEye, double yLeftEye, double xLeftRect, double yLeftRect, double xRightRect, double yRightRect, double dxLeftRect, double dyLeftRect, double dxRightRect, double dyRightRect, double xLeftUpRect, double yLeftUpRect, double xRightUpRect, double yRightUpRect, double dxLeftUpRect, double dyLeftUpRect, double dxRightUpRect, double dyRightUpRect, double DeviceAngle, PDMSupportType supType, bool bEnableCorrection)
{
    if (iPhoto == 0 || iPhoto == 1 || iPhoto == 2) {
		// Set Support type
		Photo[iPhoto].sup.SetSupportType(supType);
        // Device type
        Photo[iPhoto].nDevice = nDevice;
		double kZoomNear = 1.25 / 1.7, kZoomFar = 1.;
		switch (supType) {
			case nAcepMetalSupport:
			case nAcepPlasticSupport:
				kZoomFar = 1.;
				kZoomNear = 1.25 / 1.7;
				break;
			case nZeissSupport:
				kZoomFar = (1.9 / 2.4);
				kZoomNear = (1.0 / 1.7);
				break;
		}
		switch (nDevice) {
			case niPad:
				Photo[iPhoto].kFarPD = 272.0 * kZoomFar;
				// old value = 233.2
				Photo[iPhoto].kNearPD = 239.6 * kZoomNear;
				break;
			default:
				Photo[iPhoto].kFarPD = 360.0 * kZoomFar;
				Photo[iPhoto].kNearPD = 239.6 * kZoomNear;
				break;
		}
        // Photo mode
        Photo[iPhoto].nPhotoMode = nPhotoMode;
        Device.SetPhotoMode(nPhotoMode);
        // Support data
        double * xSupport[3], * ySupport[3];
        xSupport[0] = &xLeftSupport;
        ySupport[0] = &yLeftSupport;
        xSupport[1] = &xRightSupport;
        ySupport[1] = &yRightSupport;
        xSupport[2] = &xCenterSupport;
        ySupport[2] = &yCenterSupport;
        int i = 0, ismin = 1, ismax = 0, iscenter = 2;
        for (i = 0; i < 3; i++) {
            if (*(xSupport[i]) < *(xSupport[ismin])) {
                ismin = i;
            }
            if (*(xSupport[i]) > *(xSupport[ismax])) {
                ismax = i;
            }
        }
        for (i = 0; i < 3; i++) {
            if (i != ismin && i != ismax) {
                iscenter = i;
                break;
            }
        }
        
        Photo[iPhoto].xLeftSupport = *(xSupport[ismax]);
        Photo[iPhoto].yLeftSupport = *(ySupport[ismax]);
        Photo[iPhoto].xRightSupport = *(xSupport[ismin]);
        Photo[iPhoto].yRightSupport = *(ySupport[ismin]);
        Photo[iPhoto].xCenterSupport = *(xSupport[iscenter]);
        Photo[iPhoto].yCenterSupport = *(ySupport[iscenter]);
        // Eyes data
        if (xLeftEye > xRightEye) {
            Photo[iPhoto].xRightEye = xRightEye;
            Photo[iPhoto].yRightEye = yRightEye;
            Photo[iPhoto].xLeftEye = xLeftEye;
            Photo[iPhoto].yLeftEye = yLeftEye;
        }
        else {
            Photo[iPhoto].xRightEye = xLeftEye;
            Photo[iPhoto].yRightEye = yLeftEye;
            Photo[iPhoto].xLeftEye = xRightEye;
            Photo[iPhoto].yLeftEye = yRightEye;
        }
        // Frame data
        Photo[iPhoto].xLeftRect = xLeftRect;
        Photo[iPhoto].yLeftRect = yLeftRect + dyLeftRect;
        Photo[iPhoto].xRightRect = xRightRect + dxRightRect;
        Photo[iPhoto].yRightRect = yRightRect + dyRightRect;
        Photo[iPhoto].xLeftUpRect = xLeftUpRect + dxLeftUpRect;
        Photo[iPhoto].yLeftUpRect = yLeftUpRect;
        Photo[iPhoto].xRightUpRect = xRightUpRect;
        Photo[iPhoto].yRightUpRect = yRightUpRect;
		if(bEnableCorrection) {
			// Correct Frame data
			Photo[iPhoto].xLeftRect -= 1.5;
			Photo[iPhoto].yLeftRect += 1.0;
			Photo[iPhoto].xRightRect += 1.5;
			Photo[iPhoto].yRightRect += 1.0;
			Photo[iPhoto].xLeftUpRect += 1.5;
			Photo[iPhoto].yLeftUpRect -= 1.5;
			Photo[iPhoto].xRightUpRect -= 1.5;
			Photo[iPhoto].yRightUpRect -= 1.5;
		}
        // Device angle
        Photo[iPhoto].DeviceAngle = DeviceAngle * M_PI / 180.;
        // Set calculation results to false
        Results[iPhoto].bDataResults = false;
        Results[iPhoto].LeftVD = Results[iPhoto].defVertex;
        Results[iPhoto].RightVD = Results[iPhoto].defVertex;
        // Head rotation correction
        Results[iPhoto].bHeadRotation = bHeadRotation;
        // Lens center points shift
        Results[iPhoto].dxLeft = Results[iPhoto].dyLeft = Results[iPhoto].dxRight = Results[iPhoto].dyRight = 0.;
        if (Results2.bWrapCalc) {
            // default Wrap angle - 5 degree
            Results[iPhoto].defWrapAngle = 5. * M_PI / 180.;
        }
        // pixel in mm
        Results[iPhoto].pixelInMM_dxLeft = Results[iPhoto].pixelInMM_dyLeft = Results[iPhoto].pixelInMM_dxRight = Results[iPhoto].pixelInMM_dyRight = 1.;
        Results2.bDataResults = false;
		Results2.bPointsResults = false;
        
        // check photo data
        Photo[iPhoto].checkPhotoData(); // -----------------------------> check data
    }
}
bool CPDMResults::CalcPhotoResults(int iPhoto)
{
	bool bRes_xyz = CalcPhotoResults_xyz(iPhoto);
	return bRes_xyz;
}
bool CPDMResults::CalcPhotoResults_xy(int iPhoto)
{
    bool bRes = false;
    if (iPhoto == 0 || iPhoto == 1 || iPhoto == 2) {
        if (Results[iPhoto].bDataResults == true && Results[iPhoto].bHeadRotation == bHeadRotation) {
            bRes = true;
        }
        else if (Photo[iPhoto].nPhotoMode == nNoData) {
            bRes = false;
        }
        else {
            // alfaTilt - угол наклона головы "вправо - влево"
            Results[iPhoto].alfaTilt = Photo[iPhoto].CalcHeadTiltAngle();
			int i = 0;
			Results[iPhoto].alfaPantosk = 0.;
			Results[iPhoto].alfaPan = Photo[iPhoto].CalcHeadPanAngle();
			for (i = 0; i < 2; i++) {
				// alfaPan - угол поворота головы "вправо - влево"
				Results[iPhoto].alfaPan = Photo[iPhoto].CalcHeadPanAngle2(Results[iPhoto].alfaPan, Results[iPhoto].alfaPantosk);
				// Dist - расстояние съемки до Суппорта
				Results[iPhoto].Dist = Photo[iPhoto].PhotoDistance(Results[iPhoto].alfaPan);
				// alfaPantosk - пантоскопический угол
				Results[iPhoto].alfaPantosk = -Photo[iPhoto].CalcPantoskopicheskyCorner(Results[iPhoto].alfaPan, Results[iPhoto].Dist);
			}
			
            // alfaPantoskHor - пантоскопический угол относительно отвеса
            Results[iPhoto].alfaPantoskHor = Photo[iPhoto].CalcPantoskopicheskyCornerHor(Results[iPhoto].alfaPan, Results[iPhoto].Dist, Results[iPhoto].alfaPantosk);
            
            double Dist = Results[iPhoto].Dist;

            // Центральная точка
//            double centerX = (Photo[iPhoto].xLeftRect + Photo[iPhoto].xRightRect) / 2.;
            double centerX = Photo[iPhoto].CalcCenterPoint(Photo[iPhoto].xRightRect, Photo[iPhoto].xLeftRect, Dist + Photo[iPhoto].sup.SupportDistance, Results[iPhoto].alfaPan);
            
            double dLeftPDPix = (Photo[iPhoto].xLeftEye - centerX);
            double dRightPDPix = (centerX - Photo[iPhoto].xRightEye);
            // учитываем наклон головы
            double dLeftPD = Photo[iPhoto].convertToCM(dLeftPDPix, Results[iPhoto].alfaPan, Dist + Photo[iPhoto].sup.SupportDistance);
            double dRightPD = Photo[iPhoto].convertToCM(dRightPDPix, Results[iPhoto].alfaPan, Dist + Photo[iPhoto].sup.SupportDistance);
            
            Results[iPhoto].pixelInMM_dxLeft = dLeftPDPix / dLeftPD;
            Results[iPhoto].pixelInMM_dxRight = dRightPDPix / dRightPD;
            
            dLeftPD /= cos(Results[iPhoto].alfaTilt);
            dRightPD /= cos(Results[iPhoto].alfaTilt);

            // PD с учетом поворота головы
            dLeftPD /= cos (Results[iPhoto].alfaPan);
            dRightPD /= cos (Results[iPhoto].alfaPan);

            Results[iPhoto].bHeadRotation = bHeadRotation;
            if (Results[iPhoto].bHeadRotation) {
                // учитываем поворот головы
                double drLeft = (Results[iPhoto].LeftVD + EyeRadius) * tan(-Results[iPhoto].alfaPan);
                double drRight = (Results[iPhoto].RightVD + EyeRadius) * tan(-Results[iPhoto].alfaPan);
                dLeftPD += drLeft;
                dRightPD -= drRight;
            }
            
            // при отображении значений PD учитываем Конвергенцию
            double ndLeftPD;
            double ndRightPD;
            if (Photo[iPhoto].nPhotoMode == nFarPD) {
                ndLeftPD = Photo[iPhoto].correctPD(dLeftPD, Dist + Photo[iPhoto].sup.SupportDistance, Results[iPhoto].LeftVD);
                ndRightPD = Photo[iPhoto].correctPD(dRightPD, Dist + Photo[iPhoto].sup.SupportDistance, Results[iPhoto].RightVD);
            } else {
                ndLeftPD = dLeftPD;
                ndRightPD = dRightPD;
            }
            // Set PD
            Results[iPhoto].LeftPD = ndLeftPD;
            Results[iPhoto].RightPD = ndRightPD;

            // set Lens center points shift
            Results[iPhoto].dxLeft = (centerX + Results[iPhoto].LeftPD * Results[iPhoto].pixelInMM_dxLeft * cos(Results[iPhoto].alfaPan)) - Photo[iPhoto].xLeftEye;
            Results[iPhoto].dxRight = (centerX - Results[iPhoto].RightPD * Results[iPhoto].pixelInMM_dxRight * cos(Results[iPhoto].alfaPan)) - Photo[iPhoto].xRightEye;
            // corrected scale factor
            double angle = 0.;
            angle = Results[iPhoto].defWrapAngle - fabs(Results[iPhoto].defWrapAngle - Results[iPhoto].alfaPan);
            if (angle > 0.) {
                Results[iPhoto].pixelInMM_dxLeft /= cos(angle);
            }
            else {
                Results[iPhoto].pixelInMM_dxLeft *= cos(angle);
            }
            angle = Results[iPhoto].defWrapAngle - (Results[iPhoto].defWrapAngle + Results[iPhoto].alfaPan);
            if (angle > 0.) {
                Results[iPhoto].pixelInMM_dxRight /= cos(angle);
            }
            else {
                Results[iPhoto].pixelInMM_dxRight *= cos(angle);
            }
            
            // определяем расстояние Bridge с учетом наклона головы
            double dBridge = (Photo[iPhoto].xLeftRect - Photo[iPhoto].xRightRect) / cos(Results[iPhoto].alfaTilt) / cos(Results[iPhoto].alfaPan);
            Results[iPhoto].Bridge = Photo[iPhoto].convertToCM(dBridge, Results[iPhoto].alfaPan, Dist + Photo[iPhoto].sup.SupportDistance);
            
            // определяем Высоты
            double dLeftHeightPix = Photo[iPhoto].yLeftRect - Photo[iPhoto].yLeftEye;
            double dLeftHeight = Photo[iPhoto].convertToCM(dLeftHeightPix, Results[iPhoto].alfaPan, Dist + Photo[iPhoto].sup.SupportDistance - Results[iPhoto].LeftPD * sin(Results[iPhoto].alfaPan));
            double dRightHeightPix = Photo[iPhoto].yRightRect - Photo[iPhoto].yRightEye;
            double dRightHeight = Photo[iPhoto].convertToCM(dRightHeightPix, Results[iPhoto].alfaPan, Dist + Photo[iPhoto].sup.SupportDistance + Results[iPhoto].RightPD * sin(Results[iPhoto].alfaPan));
            
            Results[iPhoto].pixelInMM_dyLeft = (dLeftHeightPix / dLeftHeight) * cos(Results[iPhoto].alfaPantosk);
            Results[iPhoto].pixelInMM_dyRight = (dRightHeightPix / dRightHeight) * cos(Results[iPhoto].alfaPantosk);
            
            // Correct Heights. Marker was set to cornea reflex.
            double lCor = 0., rCor = 0.;
            switch (iPhoto) {
                case 0:
                    CalcCorneaReflexCorrection_far(lCor, rCor);
                    break;
                case 1:
                    break;
            }

            double zBlick = 0.;
            double yBlick = 0.;
            // Позиционирование маркеров происходит по бликам вспышки на зрачках
            // Исходная точка должна лежать на линии, проходящей через блик и центр глаза. Она не совпадает с положением камеры.
            // Correct source point coordinate
            const double zFarFlash = -52., yFarFlash = 25.;
            const double zNearFlash = 20., yNearFlash = -8.;
            const double zRedCircle = -40.;
//			const double CorneaRadius = 7.8;
//			const double DistBetweenCorneaEyeCenters = 5.7;
			const double CorneaRadius = 7.;
			const double DistBetweenCorneaEyeCenters = 6.1;
            double kFar = 2. * (DistBetweenCorneaEyeCenters + CorneaRadius) / CorneaRadius;
            double kNear = (DistBetweenCorneaEyeCenters + CorneaRadius) / CorneaRadius;
            switch (iPhoto) {
                case 0:
                    // Calculate if client look to device camera.
                    zBlick = zFarFlash / kFar;
                    yBlick = yFarFlash / kFar;
                    break;
                case 2:
                    // Calculate if client look to device center.
                    {
                        double y1, z1, y2, z2;
                        z1 = zNearFlash / 2.;
                        y1 = yNearFlash / 2.;
                        z2 = zRedCircle;
                        y2 = 0.;
                        double dz = (z1 + z2) / 2.;
                        double dy = (y1 + y2) / 2.;
                        zBlick = dz - dz / kNear + 109.5;
                        yBlick = dy - dy / kNear;
                    }
                    break;
            }
            lCor = (Results[iPhoto].LeftVD + EyeRadius) * (zBlick / (Results[iPhoto].LeftVD + EyeRadius + Dist - yBlick));
            rCor = (Results[iPhoto].RightVD + EyeRadius) * (zBlick / (Results[iPhoto].RightVD + EyeRadius + Dist - yBlick));
            dLeftHeight -= lCor;
            dRightHeight -= rCor;
            
            // учитываем наклон головы
            dLeftHeight *= cos(Results[iPhoto].alfaTilt);
            dRightHeight *= cos(Results[iPhoto].alfaTilt);
            // учитываем кивок головы
            dLeftHeight /= cos(Results[iPhoto].alfaPantosk);
            dRightHeight /= cos(Results[iPhoto].alfaPantosk);
            // Откорректируем высоты с учетом пантоскопического угла
            // >>> Коррекция удалена 8-04-2013. Принято решение, что ее наличие не корректно, т.к. голова клиента является не зафиксированной к горизонту
            //if (Photo[iPhoto].nPhotoMode == nFarPD) {
            //    dLeftHeight += (Results[iPhoto].LeftVD + EyeRadius) * (tan(Results[iPhoto].alfaPantoskHor) - tan(Results[iPhoto].alfaPantosk));
            //    dRightHeight += (Results[iPhoto].RightVD + EyeRadius) * (tan(Results[iPhoto].alfaPantoskHor) - tan(Results[iPhoto].alfaPantosk));
            //}
            // <<< Коррекция удалена 8-04-2013.
            // Set Height
            Results[iPhoto].LeftHeight = dLeftHeight;
            Results[iPhoto].RightHeight = dRightHeight;
            
            // set Lens center points shift
            Results[iPhoto].dyLeft = (Photo[iPhoto].yLeftRect - Results[iPhoto].LeftHeight * Results[iPhoto].pixelInMM_dyLeft) - Photo[iPhoto].yLeftEye;
            Results[iPhoto].dyRight = (Photo[iPhoto].yRightRect - Results[iPhoto].RightHeight * Results[iPhoto].pixelInMM_dyRight) - Photo[iPhoto].yRightEye;
            
            // определяем Wrap угол
            Results[iPhoto].alfaWrap = Photo[iPhoto].CalcWrapAngle(Results[iPhoto].alfaPan, Dist);
            
            // определяем размеры оправы
            if (Photo[iPhoto].nPhotoMode == nFarPD) {
                CalcFrameSize(iPhoto);
            }
            else {
                Results[iPhoto].FrameWidth = 0.;
                Results[iPhoto].FrameHeight = 0.;
            }
            
            Results[iPhoto].bDataResults = true;
            bRes = true;
        }
    }
    return bRes;
}
bool CPDMResults::CalcCorneaReflexCorrection_far(double& lCor, double& rCor)
{
    bool bRes = false;
    int iPhoto = 0;
    
    double Dist = Results[iPhoto].Dist;
    
    // Center point
    double centerX = Photo[iPhoto].CalcCenterPoint(Photo[iPhoto].xRightRect, Photo[iPhoto].xLeftRect, Dist + Photo[iPhoto].sup.SupportDistance, Results[iPhoto].alfaPan);
    double dLeftPD = (Photo[iPhoto].xLeftEye - centerX);
    double dRightPD = (centerX - Photo[iPhoto].xRightEye);
    dLeftPD = Photo[iPhoto].convertToCM(dLeftPD, Results[iPhoto].alfaPan, Dist + Photo[iPhoto].sup.SupportDistance);
    dRightPD = Photo[iPhoto].convertToCM(dRightPD, Results[iPhoto].alfaPan, Dist + Photo[iPhoto].sup.SupportDistance);

	// Wrap correction
	double lWrapCorrection = (Photo[iPhoto].sup.xFastening - dLeftPD) * tan(Results[iPhoto].defWrapAngle);
	double rWrapCorrection = (Photo[iPhoto].sup.xFastening - dRightPD) * tan(Results[iPhoto].defWrapAngle);

    double zFarFlash = Device.zFarFlash;

    // left correction
    double lA = (Results[iPhoto].LeftVD + CorneaRadius + DistBetweenCorneaEyeCenters) / cos(Results[iPhoto].alfaPantosk) - DistBetweenCorneaEyeCenters - lWrapCorrection;
    double lB = (Photo[iPhoto].sup.SupportDistance / cos(Results[iPhoto].alfaPantosk));
    double lH = Photo[iPhoto].convertToCM(Photo[iPhoto].yLeftEye - (Photo[iPhoto].yLeftSupport + Photo[iPhoto].yRightSupport) / 2., Results[iPhoto].alfaPan, Dist);
    double lAlfa = atan(lH / Dist);
    double lC = Dist * cos(lAlfa) + lH * sin(Results[iPhoto].alfaPantosk) - dLeftPD * sin(Results[iPhoto].alfaPan);
    double lLen = lA + lB + lC;
    double lhCorneaRexlex = CorneaRadius * (zFarFlash / 2.) / lLen;
    lCor = (Dist + Photo[iPhoto].sup.SupportDistance - dLeftPD * sin(Results[iPhoto].alfaPan)) * lhCorneaRexlex / (lLen - CorneaRadius);
    
    // right correction
    double rA = (Results[iPhoto].RightVD + CorneaRadius + DistBetweenCorneaEyeCenters) / cos(Results[iPhoto].alfaPantosk) - DistBetweenCorneaEyeCenters - rWrapCorrection;
    double rB = (Photo[iPhoto].sup.SupportDistance / cos(Results[iPhoto].alfaPantosk));
    double rH = Photo[iPhoto].convertToCM(Photo[iPhoto].yRightEye - (Photo[iPhoto].yLeftSupport + Photo[iPhoto].yRightSupport) / 2., Results[iPhoto].alfaPan, Dist);
    double rAlfa = atan(rH / Dist);
    double rC = Dist * cos(rAlfa) + rH * sin(Results[iPhoto].alfaPantosk) + dRightPD * sin(Results[iPhoto].alfaPan);
    double rLen = rA + rB + rC;
    double rhCorneaRexlex = CorneaRadius * (zFarFlash / 2.) / rLen;
    rCor = (Dist + Photo[iPhoto].sup.SupportDistance + dRightPD * sin(Results[iPhoto].alfaPan)) * rhCorneaRexlex / (rLen - CorneaRadius);
    
    bRes = true;
    
    return bRes;
}
CPDMDataResults CPDMResults::GetPhotoResults(int iPhoto)
{
    CPDMDataResults res;
    if (iPhoto == 0 || iPhoto == 1 || iPhoto == 2) {
        res = Results[iPhoto];
        // check results data
        res.checkResultsData(iPhoto); // -----------------------------> check data
    }
    return res;
}
double CPDMResults::convertToCM(int iPhoto, double pixel)
{
    double length = 0.;
    if (Results[iPhoto].bDataResults == true && Photo[iPhoto].nPhotoMode != nNoData) {
        // check photo data
        Photo[iPhoto].checkPhotoData(); // -----------------------------> check data
        // check results data
        Results[iPhoto].checkResultsData(iPhoto); // -----------------------------> check data
        length = Photo[iPhoto].convertToCM(pixel, Results[iPhoto].alfaPan, Results[iPhoto].Dist + Photo[iPhoto].sup.SupportDistance);
    }
    return length;
}
CPDMPhotoData CPDMResults::GetPhotoData(int iPhoto)
{
    CPDMPhotoData res;
    if (iPhoto == 0 || iPhoto == 1 || iPhoto == 2) {
        res = Photo[iPhoto];
        // check photo data
        res.checkPhotoData(); // -----------------------------> check data
    }
    return res;
}
bool CPDMResults::CalcPhotoResults_xyz(int iPhoto, bool bCalculation)
{
	bool bRes = false;
	if (iPhoto == 0 || iPhoto == 1 || iPhoto == 2) {
		if (Results[iPhoto].bDataResults == true && Results[iPhoto].bHeadRotation == bHeadRotation && bCalculation == false) {
			bRes = true;
		}
		else if (Photo[iPhoto].nPhotoMode == nNoData) {
			bRes = false;
		}
		else {
			double lensRadius = GetLensBaseRadius(Results[iPhoto].defWrapAngle);
			int iEye = 0; // eye identifier
			// alfaTilt - угол наклона головы "вправо - влево"
			Results[iPhoto].alfaTilt = Photo[iPhoto].CalcHeadTiltAngle();
			int i = 0;
			Results[iPhoto].alfaPantosk = 0.;
			Results[iPhoto].alfaPan = Photo[iPhoto].CalcHeadPanAngle();
			for (i = 0; i < 3; i++) {
				// alfaPan - угол поворота головы "вправо - влево"
				Results[iPhoto].alfaPan = Photo[iPhoto].CalcHeadPanAngle2(Results[iPhoto].alfaPan, Results[iPhoto].alfaPantosk);
//                Results[iPhoto].alfaPan = Photo[iPhoto].CalcHeadPanAngle3(Results[iPhoto].alfaPan, Results[iPhoto].alfaPantosk);
				// Dist - расстояние съемки до Суппорта
				Results[iPhoto].Dist = Photo[iPhoto].PhotoDistance(Results[iPhoto].alfaPan);
				// alfaPantosk - пантоскопический угол
				Results[iPhoto].alfaPantosk = -Photo[iPhoto].CalcPantoskopicheskyCorner(Results[iPhoto].alfaPan, Results[iPhoto].Dist);
//                Results[iPhoto].alfaPantosk = Photo[iPhoto].CalcPantoskopicheskyCorner3(Results[iPhoto].alfaPan, Results[iPhoto].Dist);
//                double AAAA = /*Results[iPhoto].alfaPantosk =*/ Photo[iPhoto].CalcPantoskopicheskyCorner3(Results[iPhoto].alfaPan, Results[iPhoto].Dist);
//                double BBBB = Results[iPhoto].alfaPantosk;
//                double CCCC = AAAA - BBBB;
			}

			// alfaPantoskHor - пантоскопический угол относительно отвеса
			Results[iPhoto].alfaPantoskHor = Photo[iPhoto].CalcPantoskopicheskyCornerHor(Results[iPhoto].alfaPan, Results[iPhoto].Dist, Results[iPhoto].alfaPantosk);

			// Constants
			const double zFarFlash = Device.zFarFlash * cos(Results[0].alfaPantosk), yFarFlash = Device.yFarFlash + (Device.zFarFlash * sin(Results[0].alfaPantosk)), xFarFlash = Device.xFarFlash; // flash position concerning the back camera
			const double zNearFlash = Device.zNearFlash * cos(Results[2].alfaPantosk), yNearFlash = Device.yNearFlash + (Device.zNearFlash * sin(Results[2].alfaPantosk)); // flash position concerning the front camera
			const double zRedCircle = Device.zRedCircle * cos(Results[2].alfaPantosk), yRedCircle = Device.zRedCircle * sin(Results[2].alfaPantosk); // red circle position concerning the front camera
			const double zDeviceCenter = Device.zDeviceCenter * cos(Results[2].alfaPantosk), yDeviceCenter = Device.zDeviceCenter * sin(Results[2].alfaPantosk); // center device position concerning the front camera
			// Points
			xyzPoint ptCamera; // Camera position
			xyzPoint ptFlash; // Flash position
			xyzPoint ptClientLook; // Client eyes look position on device
			// Spatial position
			double Dist = Results[iPhoto].Dist; // Photo distance
			double PanAngle = Results[iPhoto].alfaPan; // Support rotation angle
			double PantoAngle = Results[iPhoto].alfaPantosk; // Support panto angle
			// Camera position
            ptCamera.x = Dist * sin(PanAngle);
            ptCamera.y = -Dist * cos(PanAngle) * cos(PantoAngle);
            ptCamera.z = Dist * sin(PantoAngle) * cos(PanAngle);
			// Flash position
			switch(iPhoto) {
				case 0:
				case 1:
					// FarPD photo
                    ptFlash.x = ptCamera.x + xFarFlash;
					ptFlash.y = ptCamera.y + yFarFlash;
					ptFlash.z = ptCamera.z + zFarFlash;
					break;
				case 2:
					// NearPD photo
					ptFlash.x = ptCamera.x;
					ptFlash.y = ptCamera.y + yNearFlash;
					ptFlash.z = ptCamera.z + zNearFlash;
					break;
			}
			// Client eyes look position on device
			switch(iPhoto) {
				case 0:
				case 1:
					// FarPD photo
					ptClientLook = ptCamera;
					break;
				case 2:
					// NearPD photo
					ptClientLook = ptCamera;
					ptClientLook.y = ptCamera.y + yRedCircle;
					ptClientLook.z = ptCamera.z + zRedCircle;
					break;
			}

			double AO = sqrt(Dist * Dist + Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist - 2. * Photo[iPhoto].sup.acDist * Dist * cos(M_PI_2 + Results[iPhoto].alfaPan));
			double alpha1 = acos((Dist * Dist + AO * AO - Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist) / (2. * Dist * AO));
			double CP1 = Dist * tan(alpha1);
			double xCP1 = -CP1 * cos(Results[iPhoto].alfaPan);
			double yCP1 = -CP1 * sin(Results[iPhoto].alfaPan);
			double zCP1 = 0.;

			double BO = sqrt(Dist * Dist + Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist - 2. * Photo[iPhoto].sup.acDist * Dist * cos(M_PI_2 - Results[iPhoto].alfaPan));
			double alpha4 = acos((Dist * Dist + BO * BO - Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist) / (2. * Dist * BO));
			double CP4 = Dist * tan(alpha4);
			double xCP4 = CP4 * cos(Results[iPhoto].alfaPan);
			double yCP4 = CP4 * sin(Results[iPhoto].alfaPan);
			double zCP4 = 0.;

			// let's make turn
			xyzPoint lSupport, rSupport;
			xyzPoint lEye, rEye;
			xyzPoint lRect, lUpRect, rRect, rUpRect;
			double dx, dy;
			double fi = -Results[iPhoto].alfaTilt;   // rotation angle
			// Turn center - Right Support marker point
			rSupport.x = Photo[iPhoto].xRightSupport;
			rSupport.y = Photo[iPhoto].yRightSupport;
			// Left Support marker point
			dx = Photo[iPhoto].xLeftSupport - rSupport.x;
			dy = Photo[iPhoto].yLeftSupport - rSupport.y;
			lSupport.x = dx * cos(fi) - dy * sin(fi);
			lSupport.y = dx * sin(fi) + dy * cos(fi);
			lSupport.x += rSupport.x;
			lSupport.y += rSupport.y;
			// Right Eye marker point
			dx = Photo[iPhoto].xRightEye - rSupport.x;
			dy = Photo[iPhoto].yRightEye - rSupport.y;
			rEye.x = dx * cos(fi) - dy * sin(fi);
			rEye.y = dx * sin(fi) + dy * cos(fi);
			rEye.x += rSupport.x;
			rEye.y += rSupport.y;
			// Left Eye marker point
			dx = Photo[iPhoto].xLeftEye - rSupport.x;
			dy = Photo[iPhoto].yLeftEye - rSupport.y;
			lEye.x = dx * cos(fi) - dy * sin(fi);
			lEye.y = dx * sin(fi) + dy * cos(fi);
			lEye.x += rSupport.x;
			lEye.y += rSupport.y;
			// Right NoseRect marker point
			dx = Photo[iPhoto].xRightRect - rSupport.x;
			dy = Photo[iPhoto].yRightRect - rSupport.y;
			rRect.x = dx * cos(fi) - dy * sin(fi);
			rRect.y = dx * sin(fi) + dy * cos(fi);
			rRect.x += rSupport.x;
			rRect.y += rSupport.y;
			// Right UpRect marker point
			dx = Photo[iPhoto].xRightUpRect - rSupport.x;
			dy = Photo[iPhoto].yRightUpRect - rSupport.y;
			rUpRect.x = dx * cos(fi) - dy * sin(fi);
			rUpRect.y = dx * sin(fi) + dy * cos(fi);
			rUpRect.x += rSupport.x;
			rUpRect.y += rSupport.y;
			// Left NoseRect marker point
			dx = Photo[iPhoto].xLeftRect - rSupport.x;
			dy = Photo[iPhoto].yLeftRect - rSupport.y;
			lRect.x = dx * cos(fi) - dy * sin(fi);
			lRect.y = dx * sin(fi) + dy * cos(fi);
			lRect.x += rSupport.x;
			lRect.y += rSupport.y;
			// Left UpRect marker point
			dx = Photo[iPhoto].xLeftUpRect - rSupport.x;
			dy = Photo[iPhoto].yLeftUpRect - rSupport.y;
			lUpRect.x = dx * cos(fi) - dy * sin(fi);
			lUpRect.y = dx * sin(fi) + dy * cos(fi);
			lUpRect.x += rSupport.x;
			lUpRect.y += rSupport.y;

			// Cornea reflex points projection position
            double yLen = 0.;
			xyzPoint ptFlashReflex[2];
			double hlEye = Photo[iPhoto].convertToCM(lEye.y - rSupport.y, PanAngle, Dist);
			double hrEye = Photo[iPhoto].convertToCM(rEye.y - rSupport.y, PanAngle, Dist);
            
			ptFlashReflex[0].x = xCP1 + (xCP4 - xCP1) * (rEye.x - rSupport.x) / (lSupport.x - rSupport.x);
            yLen = yCP1 + (yCP4 - yCP1) * (rEye.x - rSupport.x) / (lSupport.x - rSupport.x);
			ptFlashReflex[0].y = yLen * cos(-PantoAngle) - (-hrEye) * sin(-PantoAngle);
			ptFlashReflex[0].z = yLen * sin(-PantoAngle) + (-hrEye) * cos(-PantoAngle);
            
			ptFlashReflex[1].x = xCP1 + (xCP4 - xCP1) * (lEye.x - rSupport.x) / (lSupport.x - rSupport.x);
            yLen = yCP1 + (yCP4 - yCP1) * (lEye.x - rSupport.x) / (lSupport.x - rSupport.x);
			ptFlashReflex[1].y = yLen * cos(-PantoAngle) - (-hlEye) * sin(-PantoAngle);
			ptFlashReflex[1].z = yLen * sin(-PantoAngle) + (-hlEye) * cos(-PantoAngle);

			// Cornea reflex points projection position with shift: dx = 1pix
			xyzPoint ptFlashReflexDx1[2];
			hlEye = Photo[iPhoto].convertToCM(lEye.y - rSupport.y, PanAngle, Dist);
			hrEye = Photo[iPhoto].convertToCM(rEye.y - rSupport.y, PanAngle, Dist);
            
			ptFlashReflexDx1[0].x = xCP1 + (xCP4 - xCP1) * (rEye.x + 1 - rSupport.x) / (lSupport.x - rSupport.x);
            yLen = yCP1 + (yCP4 - yCP1) * (rEye.x + 1 - rSupport.x) / (lSupport.x - rSupport.x);
			ptFlashReflexDx1[0].y = yLen * cos(-PantoAngle) - (-hrEye) * sin(-PantoAngle);
			ptFlashReflexDx1[0].z = yLen * sin(-PantoAngle) + (-hrEye) * cos(-PantoAngle);
            
			ptFlashReflexDx1[1].x = xCP1 + (xCP4 - xCP1) * (lEye.x - 1 - rSupport.x) / (lSupport.x - rSupport.x);
            yLen = yCP1 + (yCP4 - yCP1) * (lEye.x - 1 - rSupport.x) / (lSupport.x - rSupport.x);
			ptFlashReflexDx1[1].y = yLen * cos(-PantoAngle) - (-hlEye) * sin(-PantoAngle);
			ptFlashReflexDx1[1].z = yLen * sin(-PantoAngle) + (-hlEye) * cos(-PantoAngle);
            
			// Cornea reflex points projection position with shift: dy = 1pix
			xyzPoint ptFlashReflexDy1[2];
			hlEye = Photo[iPhoto].convertToCM(lEye.y + 1 - rSupport.y, PanAngle, Dist);
			hrEye = Photo[iPhoto].convertToCM(rEye.y + 1 - rSupport.y, PanAngle, Dist);
            
			ptFlashReflexDy1[0].x = xCP1 + (xCP4 - xCP1) * (rEye.x - rSupport.x) / (lSupport.x - rSupport.x);
            yLen = yCP1 + (yCP4 - yCP1) * (rEye.x - rSupport.x) / (lSupport.x - rSupport.x);
			ptFlashReflexDy1[0].y = yLen * cos(-PantoAngle) - (-hrEye) * sin(-PantoAngle);
			ptFlashReflexDy1[0].z = yLen * sin(-PantoAngle) + (-hrEye) * cos(-PantoAngle);
            
			ptFlashReflexDy1[1].x = xCP1 + (xCP4 - xCP1) * (lEye.x - rSupport.x) / (lSupport.x - rSupport.x);
            yLen = yCP1 + (yCP4 - yCP1) * (lEye.x - rSupport.x) / (lSupport.x - rSupport.x);
			ptFlashReflexDy1[1].y = yLen * cos(-PantoAngle) - (-hlEye) * sin(-PantoAngle);
			ptFlashReflexDy1[1].z = yLen * sin(-PantoAngle) + (-hlEye) * cos(-PantoAngle);
            
			// Rectangles points projection position
			xyzPoint ptRect[2];
			double hlRect = Photo[iPhoto].convertToCM(lRect.y - rSupport.y, PanAngle, Dist);
			double hrRect = Photo[iPhoto].convertToCM(rRect.y - rSupport.y, PanAngle, Dist);
            
			ptRect[0].x = xCP1 + (xCP4 - xCP1) * (rRect.x - rSupport.x) / (lSupport.x - rSupport.x);
            yLen = yCP1 + (yCP4 - yCP1) * (rRect.x - rSupport.x) / (lSupport.x - rSupport.x);
			ptRect[0].y = yLen * cos(-PantoAngle) - (-hrRect) * sin(-PantoAngle);
			ptRect[0].z = yLen * sin(-PantoAngle) + (-hrRect) * cos(-PantoAngle);
            
			ptRect[1].x = xCP1 + (xCP4 - xCP1) * (lRect.x - rSupport.x) / (lSupport.x - rSupport.x);
            yLen = yCP1 + (yCP4 - yCP1) * (lRect.x - rSupport.x) / (lSupport.x - rSupport.x);
			ptRect[1].y = yLen * cos(-PantoAngle) - (-hlRect) * sin(-PantoAngle);
			ptRect[1].z = yLen * sin(-PantoAngle) + (-hlRect) * cos(-PantoAngle);
            
			xyzPoint ptUpRect[2];
			double hlUpRect, hrUpRect;
			const double defFrameWidth = 50.;
			double lenSupport = sqrt((xCP4 - xCP1) * (xCP4 - xCP1) + (yCP4 - yCP1) * (yCP4 - yCP1));
			switch(iPhoto) {
				case 0:
				case 1:
					// FarPD photo
					hlUpRect = Photo[iPhoto].convertToCM(lUpRect.y - rSupport.y, PanAngle, Dist);
					hrUpRect = Photo[iPhoto].convertToCM(rUpRect.y - rSupport.y, PanAngle, Dist);
                    
					ptUpRect[0].x = xCP1 + (xCP4 - xCP1) * (rUpRect.x - rSupport.x) / (lSupport.x - rSupport.x);
                    yLen = yCP1 + (yCP4 - yCP1) * (rUpRect.x - rSupport.x) / (lSupport.x - rSupport.x);
					ptUpRect[0].y = yLen * cos(-PantoAngle) - (-hrUpRect) * sin(-PantoAngle);
					ptUpRect[0].z = yLen * sin(-PantoAngle) + (-hrUpRect) * cos(-PantoAngle);
                    
					ptUpRect[1].x = xCP1 + (xCP4 - xCP1) * (lUpRect.x - rSupport.x) / (lSupport.x - rSupport.x);
                    yLen = yCP1 + (yCP4 - yCP1) * (lUpRect.x - rSupport.x) / (lSupport.x - rSupport.x);
					ptUpRect[1].y = yLen * cos(-PantoAngle) - (-hlUpRect) * sin(-PantoAngle);
					ptUpRect[1].z = yLen * sin(-PantoAngle) + (-hlUpRect) * cos(-PantoAngle);
					break;
				case 2:
					// NearPD photo
					hlUpRect = -9.;
					hrUpRect = -9.;
                    
					ptUpRect[0].x = ptRect[0].x - defFrameWidth * ((xCP4 - xCP1) / lenSupport);
                    yLen = (yCP1 + (yCP4 - yCP1) * (rRect.x - rSupport.x) / (lSupport.x - rSupport.x)) - defFrameWidth * ((yCP4 - yCP1) / lenSupport);
					ptUpRect[0].y = yLen * cos(-PantoAngle) - (-hrUpRect) * sin(-PantoAngle);
					ptUpRect[0].z = yLen * sin(-PantoAngle) + (-hrUpRect) * cos(-PantoAngle);
                    
					ptUpRect[1].x = ptRect[1].x + defFrameWidth * ((xCP4 - xCP1) / lenSupport);
                    yLen = (yCP1 + (yCP4 - yCP1) * (lRect.x - rSupport.x) / (lSupport.x - rSupport.x)) + defFrameWidth * ((yCP4 - yCP1) / lenSupport);
					ptUpRect[1].y = yLen * cos(-PantoAngle) - (-hlUpRect) * sin(-PantoAngle);
					ptUpRect[1].z = yLen * sin(-PantoAngle) + (-hlUpRect) * cos(-PantoAngle);
					break;
			}

			xyzPoint ptCalcUp[2], ptCalcDown[2];
			ptCalcDown[0].x = xCP1 + (xCP4 - xCP1) * (rEye.x - rSupport.x) / (lSupport.x - rSupport.x);
            yLen = yCP1 + (yCP4 - yCP1) * (rEye.x - rSupport.x) / (lSupport.x - rSupport.x);
			ptCalcDown[0].y = yLen * cos(-PantoAngle) - (-hrRect) * sin(-PantoAngle);
			ptCalcDown[0].z = yLen * sin(-PantoAngle) + (-hrRect) * cos(-PantoAngle);
            
			ptCalcDown[1].x = xCP1 + (xCP4 - xCP1) * (lEye.x - rSupport.x) / (lSupport.x - rSupport.x);
            yLen = yCP1 + (yCP4 - yCP1) * (lEye.x - rSupport.x) / (lSupport.x - rSupport.x);
			ptCalcDown[1].y = yLen * cos(-PantoAngle) - (-hlRect) * sin(-PantoAngle);
			ptCalcDown[1].z = yLen * sin(-PantoAngle) + (-hlRect) * cos(-PantoAngle);
            
			ptCalcUp[0].x = xCP1 + (xCP4 - xCP1) * (rEye.x - rSupport.x) / (lSupport.x - rSupport.x);
            yLen = yCP1 + (yCP4 - yCP1) * (rEye.x - rSupport.x) / (lSupport.x - rSupport.x);
			ptCalcUp[0].y = yLen * cos(-PantoAngle) - (-hrUpRect) * sin(-PantoAngle);
			ptCalcUp[0].z = yLen * sin(-PantoAngle) + (-hrUpRect) * cos(-PantoAngle);
            
			ptCalcUp[1].x = xCP1 + (xCP4 - xCP1) * (lEye.x - rSupport.x) / (lSupport.x - rSupport.x);
            yLen = yCP1 + (yCP4 - yCP1) * (lEye.x - rSupport.x) / (lSupport.x - rSupport.x);
			ptCalcUp[1].y = yLen * cos(-PantoAngle) - (-hlUpRect) * sin(-PantoAngle);
			ptCalcUp[1].z = yLen * sin(-PantoAngle) + (-hlUpRect) * cos(-PantoAngle);

			// >>>>>>>>>>>>>>> Calculate Eyes center points position =============================
			// Eyes center points position
			xyzPoint ptEyeCenter[2];
			if(Results2.bDataResults && Results2.bPointsResults) {
				ptEyeCenter[0] = Results2.ptEyeCenter[0];
				ptEyeCenter[1] = Results2.ptEyeCenter[1];
			}
			else {
				// Trek of the eyes center points
				const int nTrekPts = 800; // points in trek
				xyzPoint ptEyeTrek[2][nTrekPts];
				// Calculate treks
				int iy = 0; // y position identifier. Changed from 0 to 800. Or from 0 to 80 mm.
				for(iEye = 0; iEye < 2; iEye++) {
					for(iy = 0; iy < nTrekPts; iy++) {
						// line from camera to flash reflection
						// line coefficients: x = kX * y + bX ; z = kZ * y + bZ
						double kX, bX, kZ, bZ;
						kX = (ptFlashReflex[iEye].x - ptCamera.x) / (ptFlashReflex[iEye].y - ptCamera.y);
						kZ = (ptFlashReflex[iEye].z - ptCamera.z) / (ptFlashReflex[iEye].y - ptCamera.y);
						bX = ptCamera.x - kX * ptCamera.y;
						bZ = ptCamera.z - kZ * ptCamera.y;
						// flash reflection point on cornea surface
						xyzPoint ptFRCornea;
						ptFRCornea.y = (double)iy / 10.;
						ptFRCornea.x = kX * ptFRCornea.y + bX;
						ptFRCornea.z = kZ * ptFRCornea.y + bZ;
						// median point between ptFlash and ptCamera
						xyzPoint ptMedian;
						ptMedian.x = (ptFlash.x + ptCamera.x) / 2.;
						ptMedian.y = (ptFlash.y + ptCamera.y) / 2.;
						ptMedian.z = (ptFlash.z + ptCamera.z) / 2.;
						// calculate cornea sphere center
						xyzPoint ptCorneaCenter;
						// center is placed on line: ptMedian to ptFRCornea
						double dx, dy, dz, Len;
						dx = ptFRCornea.x - ptMedian.x;
						dy = ptFRCornea.y - ptMedian.y;
						dz = ptFRCornea.z - ptMedian.z;
						Len = sqrt(dx * dx + dy * dy + dz * dz); // piece length
						ptCorneaCenter.x = ptMedian.x + dx * (Len + CorneaRadius) / Len;
						ptCorneaCenter.y = ptMedian.y + dy * (Len + CorneaRadius) / Len;
						ptCorneaCenter.z = ptMedian.z + dz * (Len + CorneaRadius) / Len;
						// calculate eye sphere center
						xyzPoint ptEyeCenter;
						// center is placed on line: ptClientLook to ptCorneaCenter
						dx = ptCorneaCenter.x - ptClientLook.x;
						dy = ptCorneaCenter.y - ptClientLook.y;
						dz = ptCorneaCenter.z - ptClientLook.z;
						Len = sqrt(dx * dx + dy * dy + dz * dz); // piece length
						ptEyeCenter.x = ptClientLook.x + dx * (Len + DistBetweenCorneaEyeCenters) / Len;
						ptEyeCenter.y = ptClientLook.y + dy * (Len + DistBetweenCorneaEyeCenters) / Len;
						ptEyeCenter.z = ptClientLook.z + dz * (Len + DistBetweenCorneaEyeCenters) / Len;
						// save trek point
						ptEyeTrek[iEye][iy] = ptEyeCenter;
					}
				}
				// find eyes sphere center points
				int iPtMin[2] = {0, 0};
				for(iEye = 0; iEye < 2; iEye++) {
					double LenMin = 1000.;
					for(iy = 0; iy < nTrekPts; iy++) {
						double lrWrapCorrection = (Photo[iPhoto].sup.xFastening - (ptEyeTrek[1][iy].x - ptEyeTrek[0][iy].x) / 2.) * tan(Results[iPhoto].defWrapAngle);
						double dVD = ptEyeTrek[iEye][iy].y - Photo[iPhoto].sup.SupportDistance - CorneaRadius - DistBetweenCorneaEyeCenters + lrWrapCorrection;
						double Len;
						switch(iEye) {
							case 0:
								Len = fabs(dVD - Results[iPhoto].RightVD);
								break;
							case 1:
								Len = fabs(dVD - Results[iPhoto].LeftVD);
								break;
						}
						if(Len < LenMin) {
							LenMin = Len;
							iPtMin[iEye] = iy;
						}
					}
				}
				ptEyeCenter[0] = ptEyeTrek[0][iPtMin[0]];
				ptEyeCenter[1] = ptEyeTrek[1][iPtMin[1]];
			}
			// <<<<<<<<<<<<<<< Calculate Eyes center points position =============================

			// >>>>>>>>>>>>>>> Calculate lenses points position ================================
			// Lenses points position
			xyzPoint ptLensCenter[2], ptLensNose[2], ptLensTemple[2], ptWrapMarker[2];
			if(Results2.bDataResults && Results2.bPointsResults) {
				ptLensCenter[0] = Results2.ptLensCenter[0];
				ptWrapMarker[0] = Results2.ptWrapMarker[0];
				ptLensNose[0] = Results2.ptLensNose[0];
				ptLensTemple[0] = Results2.ptLensTemple[0];
				ptLensCenter[1] = Results2.ptLensCenter[1];
				ptWrapMarker[1] = Results2.ptWrapMarker[1];
				ptLensNose[1] = Results2.ptLensNose[1];
				ptLensTemple[1] = Results2.ptLensTemple[1];
			}
			else {
				xyzPoint pt0[2], pt1[2], pt3[2];
				for(iEye = 0; iEye < 2; iEye++) {
					xyzPoint pt2;
					const int nStep = 25;
					int iStep = 0;
					double angle = 0., dStep;
					double oa, ob, oc;
					double kX, bX, kZ, bZ;
					double kXup, bXup, kZup, bZup;
					switch(iEye) {
						case 0:
							pt2.x = -Photo[iPhoto].sup.xFastening;
							pt2.y = Photo[iPhoto].sup.yFastening;
							dStep = 5. * M_PI / 180.;
							break;
						case 1:
							pt2.x = Photo[iPhoto].sup.xFastening;
							pt2.y = Photo[iPhoto].sup.yFastening;
							dStep = -5. * M_PI / 180.;
							break;
					}
					CalcLineCoefficients(ptCamera, ptRect[iEye], kX, bX, kZ, bZ);
					CalcLineCoefficients(ptCamera, ptUpRect[iEye], kXup, bXup, kZup, bZup);
					double par = 2 * Results[iPhoto].defWrapAngle;
					for(iStep = 0; iStep < nStep; iStep++) {
						pt0[iEye].x = pt2.x + lensRadius * sin(angle);
						pt0[iEye].y = pt2.y + lensRadius * cos(angle);
						// Nose point
						// solution of the square equation
						oa = kX * kX + 1.;
						ob = 2. * kX * (bX - pt0[iEye].x) - 2. * pt0[iEye].y;
						oc = (bX - pt0[iEye].x) * (bX - pt0[iEye].x) + pt0[iEye].y * pt0[iEye].y - lensRadius * lensRadius;
						pt1[iEye].y = (-ob - sqrt(ob * ob - 4. * oa * oc)) / (2. * oa);
						pt1[iEye].x = kX * pt1[iEye].y + bX;
						pt1[iEye].z = kZ * pt1[iEye].y + bZ;
						// Temple point
						// solution of the square equation
						oa = kXup * kXup + 1.;
						ob = 2. * kXup * (bXup - pt0[iEye].x) - 2. * pt0[iEye].y;
						oc = (bXup - pt0[iEye].x) * (bXup - pt0[iEye].x) + pt0[iEye].y * pt0[iEye].y - lensRadius * lensRadius;
						pt3[iEye].y = (-ob - sqrt(ob * ob - 4. * oa * oc)) / (2. * oa);
						pt3[iEye].x = kXup * pt3[iEye].y + bXup;
						pt3[iEye].z = kZup * pt3[iEye].y + bZup;
						// wrap angle
						double wrap = atan((pt3[iEye].y - pt1[iEye].y) / (pt3[iEye].x - pt1[iEye].x)) * (pt2.x / fabs(pt2.x));
						if(fabs(wrap - Results[iPhoto].defWrapAngle) < 0.002) {
							break;
						}
						else {
							if((par > 0. && (Results[iPhoto].defWrapAngle - wrap) < 0.) || (par < 0. && (Results[iPhoto].defWrapAngle - wrap) > 0.))
								dStep = -(dStep / 2.);
							angle += dStep;
							par = Results[iPhoto].defWrapAngle - wrap;
						}
					}
				}
				ptLensCenter[0] = pt0[0];
				ptWrapMarker[0] = ptLensNose[0] = pt1[0];
				ptLensTemple[0] = pt3[0];
				ptLensCenter[1] = pt0[1];
				ptWrapMarker[1] = ptLensNose[1] = pt1[1];
				ptLensTemple[1] = pt3[1];
                // Lens center Y corrected. Use back lens sufrace.
                ptLensCenter[0].y += Photo[iPhoto].sup.SupportDistance - Photo[iPhoto].sup.SupportDistanceForfard;
                ptLensCenter[1].y += Photo[iPhoto].sup.SupportDistance - Photo[iPhoto].sup.SupportDistanceForfard;
			}
			// <<<<<<<<<<<<<<< Calculate lenses points position ==================================

			// >>>>>>>>>>>>>>> Calculation =======================================================
			// center point
			xyzPoint ptCenter;
			ptCenter.x = ((ptWrapMarker[1].x + ptWrapMarker[0].x) / 2. + (ptLensNose[1].x + ptLensNose[0].x) / 2.) / 2.;
			ptCenter.y = ((ptWrapMarker[1].y + ptWrapMarker[0].y) / 2. + (ptLensNose[1].y + ptLensNose[0].y) / 2.) / 2.;
			ptCenter.z = ((ptWrapMarker[1].z + ptWrapMarker[0].z) / 2. + (ptLensNose[1].z + ptLensNose[0].z) / 2.) / 2.;
			// PD
			xyzPoint ptLensPD[2]; // точка взгяда на линзе
			xyzPoint ptFramePD[2]; // точка взгяда на оправе
			xyzPoint ptLensCamera[2]; // точка взгяда на линзе, если смотреть на камеру
			xyzPoint ptLensEye[2]; // точка взгляда на плоскости проекции
			xyzPoint ptClientHasToLook; // Client eyes look position for PD calculation
			xyzPoint ptClientHasToLookCamera; // Client eyes look position for Heights calculation
			xyzPoint ptLensEye_pp[2], ptFlashReflex_pp[2]; // в координатах плоскости проекции
			Results[iPhoto].bHeadRotation = bHeadRotation;
			for(iEye = 0; iEye < 2; iEye++) {
				switch(iPhoto) {
					case 0:
					case 1:
						// FarPD photo
						if(Results[iPhoto].bHeadRotation) {
							ptClientHasToLook.x = ptEyeCenter[iEye].x;
							ptClientHasToLook.y = ptCamera.y;
							ptClientHasToLook.z = ptCamera.z;
						}
						else {
							ptClientHasToLook.x = ptEyeCenter[iEye].x + (ptCamera.x - ptCenter.x);
							ptClientHasToLook.y = ptCamera.y;
							ptClientHasToLook.z = ptCamera.z;
						}
						ptClientHasToLookCamera = ptCamera;
						break;
					case 2:
						// NearPD photo
						if(Results[iPhoto].bHeadRotation) {
//							ptClientHasToLook.x = ptCenter.x;
							ptClientHasToLook.x = (ptEyeCenter[0].x + ptEyeCenter[1].x) / 2.;
							ptClientHasToLook.y = ptCamera.y + yDeviceCenter;
							ptClientHasToLook.z = ptCamera.z + zDeviceCenter;
							ptClientHasToLookCamera = ptClientHasToLook;
							ptClientHasToLookCamera.x = ptCamera.x;
						}
						else {
							ptClientHasToLook.x = ptCamera.x;
							ptClientHasToLook.y = ptCamera.y + yDeviceCenter;
							ptClientHasToLook.z = ptCamera.z + zDeviceCenter;
							ptClientHasToLookCamera = ptClientHasToLook;
						}
						break;
				}
				double oa, ob, oc;
				double kX, bX, kZ, bZ;
				CalcLineCoefficients(ptClientHasToLook, ptEyeCenter[iEye], kX, bX, kZ, bZ);
				// solution of the square equation
				oa = kX * kX + 1.;
				ob = 2. * kX * (bX - ptLensCenter[iEye].x) - 2. * ptLensCenter[iEye].y;
				oc = (bX - ptLensCenter[iEye].x) * (bX - ptLensCenter[iEye].x) + ptLensCenter[iEye].y * ptLensCenter[iEye].y - lensRadius * lensRadius;
				ptLensPD[iEye].y = (-ob - sqrt(ob * ob - 4. * oa * oc)) / (2. * oa);
				ptLensPD[iEye].x = kX * ptLensPD[iEye].y + bX;
				ptLensPD[iEye].z = kZ * ptLensPD[iEye].y + bZ;
				// точки взгляда на оправе, для определения положения центров диаметров
				ptFramePD[iEye].y = ptWrapMarker[iEye].y + (ptLensTemple[iEye].y - ptWrapMarker[iEye].y) * (ptEyeCenter[iEye].x - ptWrapMarker[iEye].x) / (ptLensTemple[iEye].x - ptWrapMarker[iEye].x);
				ptFramePD[iEye].x = kX * ptFramePD[iEye].y + bX;
				ptFramePD[iEye].z = kZ * ptFramePD[iEye].y + bZ;
				// точка взгяда на линзе, если смотреть на камеру
				CalcLineCoefficients(ptClientHasToLookCamera, ptEyeCenter[iEye], kX, bX, kZ, bZ);
				// solution of the square equation
				oa = kX * kX + 1.;
				ob = 2. * kX * (bX - ptLensCenter[iEye].x) - 2. * ptLensCenter[iEye].y;
				oc = (bX - ptLensCenter[iEye].x) * (bX - ptLensCenter[iEye].x) + ptLensCenter[iEye].y * ptLensCenter[iEye].y - lensRadius * lensRadius;
				ptLensCamera[iEye].y = (-ob - sqrt(ob * ob - 4. * oa * oc)) / (2. * oa);
				ptLensCamera[iEye].x = kX * ptLensCamera[iEye].y + bX;
				ptLensCamera[iEye].z = kZ * ptLensCamera[iEye].y + bZ;
				// точка центра диаметра на плоскости проекции
				oa = ptCamera.x;
				ob = ptCamera.y;
				oc = ptCamera.z;
				CalcLineCoefficients(ptCamera, ptFramePD[iEye], kX, bX, kZ, bZ);
				ptLensEye[iEye].y = -(oa * bX + oc * bZ) / (oa * kX + ob + oc * kZ);
				ptLensEye[iEye].x = kX * ptLensEye[iEye].y + bX;
				ptLensEye[iEye].z = kZ * ptLensEye[iEye].y + bZ;
				// Shift Calculation: (точка взгляда на плоскости проекции) - (Cornea reflex points projection position)
				// Преобразовываем точки в систему координат плоскости проекции
				ptLensEye_pp[iEye] = ConvertPointToProjectionPlane(ptLensEye[iEye], PantoAngle);
				ptFlashReflex_pp[iEye] = ConvertPointToProjectionPlane(ptFlashReflex[iEye], PantoAngle);
			}
            xyzPoint ptFrameFlashReflex[2]; // точка блика на оправе
            xyzPoint ptFrameFlashReflexDx1[2]; // точка блика на оправе при сдвижке на 1 пикселю по X
            xyzPoint ptFrameFlashReflexDy1[2]; // точка блика на оправе при сдвижке на 1 пикселю по Y
			for(iEye = 0; iEye < 2; iEye++) {
				double kXf, bXf, kZf, bZf;
				CalcLineCoefficients(ptLensTemple[iEye], ptWrapMarker[iEye], kXf, bXf, kZf, bZf);
				double kX, bX, kZ, bZ;
				// точки блика на оправе
				CalcLineCoefficients(ptCamera, ptFlashReflex[iEye], kX, bX, kZ, bZ);
                ptFrameFlashReflex[iEye].y = (bXf - bX) / (kX - kXf);
				ptFrameFlashReflex[iEye].x = kX * ptFrameFlashReflex[iEye].y + bX;
				ptFrameFlashReflex[iEye].z = kZ * ptFrameFlashReflex[iEye].y + bZ;
				// точки блика на оправе при сдвижке на 1 пикселю по X
				CalcLineCoefficients(ptCamera, ptFlashReflexDx1[iEye], kX, bX, kZ, bZ);
                ptFrameFlashReflexDx1[iEye].y = (bXf - bX) / (kX - kXf);
				ptFrameFlashReflexDx1[iEye].x = kX * ptFrameFlashReflex[iEye].y + bX;
				ptFrameFlashReflexDx1[iEye].z = kZ * ptFrameFlashReflex[iEye].y + bZ;
				// точки блика на оправе при сдвижке на 1 пикселю по Y
				CalcLineCoefficients(ptCamera, ptFlashReflexDy1[iEye], kX, bX, kZ, bZ);
                ptFrameFlashReflexDy1[iEye].y = (bXf - bX) / (kX - kXf);
				ptFrameFlashReflexDy1[iEye].x = kX * ptFrameFlashReflex[iEye].y + bX;
				ptFrameFlashReflexDy1[iEye].z = kZ * ptFrameFlashReflex[iEye].y + bZ;
            }
			// -----> Set Lens center points shift
			Results[iPhoto].dxLeft = Photo[iPhoto].convertToPixel(ptLensEye_pp[1].x - ptFlashReflex_pp[1].x, Dist);
			Results[iPhoto].dxRight = Photo[iPhoto].convertToPixel(ptLensEye_pp[0].x - ptFlashReflex_pp[0].x, Dist);
			Results[iPhoto].dyLeft = Photo[iPhoto].convertToPixel(ptFlashReflex_pp[1].z - ptLensEye_pp[1].z, Dist);
			Results[iPhoto].dyRight = Photo[iPhoto].convertToPixel(ptFlashReflex_pp[0].z - ptLensEye_pp[0].z, Dist);
			// -----> Set Scale factor
            double dx1p, dy1p, dz1p;
            dx1p = ptFrameFlashReflexDx1[1].x - ptFrameFlashReflex[1].x;
            dy1p = ptFrameFlashReflexDx1[1].y - ptFrameFlashReflex[1].y;
            dz1p = ptFrameFlashReflexDx1[1].z - ptFrameFlashReflex[1].z;
            Results[iPhoto].pixelInMM_dxLeft = 1. / sqrt(dx1p * dx1p + dy1p * dy1p + dz1p * dz1p);
            dx1p = ptFrameFlashReflexDx1[0].x - ptFrameFlashReflex[0].x;
            dy1p = ptFrameFlashReflexDx1[0].y - ptFrameFlashReflex[0].y;
            dz1p = ptFrameFlashReflexDx1[0].z - ptFrameFlashReflex[0].z;
            Results[iPhoto].pixelInMM_dxRight = 1. / sqrt(dx1p * dx1p + dy1p * dy1p + dz1p * dz1p);
            dx1p = ptFrameFlashReflexDy1[1].x - ptFrameFlashReflex[1].x;
            dy1p = ptFrameFlashReflexDy1[1].y - ptFrameFlashReflex[1].y;
            dz1p = ptFrameFlashReflexDy1[1].z - ptFrameFlashReflex[1].z;
            Results[iPhoto].pixelInMM_dyLeft = 1. / sqrt(dx1p * dx1p + dy1p * dy1p + dz1p * dz1p);
            dx1p = ptFrameFlashReflexDy1[0].x - ptFrameFlashReflex[0].x;
            dy1p = ptFrameFlashReflexDy1[0].y - ptFrameFlashReflex[0].y;
            dz1p = ptFrameFlashReflexDy1[0].z - ptFrameFlashReflex[0].z;
            Results[iPhoto].pixelInMM_dyRight = 1. / sqrt(dx1p * dx1p + dy1p * dy1p + dz1p * dz1p);

/*
            double pixelInMM_dxLeft, pixelInMM_dxRight, pixelInMM_dyLeft, pixelInMM_dyRight;
            double corrector = (ptLensEye[1].x - ptLensEye[0].x) / (ptFramePD[1].x - ptFramePD[0].x);
            // corrector - scale factor вычисляется для правильного масштабирования шаблона прогрессива с кругами контуров линз. Т.к. шаблон приставляется к оправе, то его проекция на плоскость проекции отличается от оригинала. corrector нужен для корректного переноса шаблона на плоскость проекции.
			pixelInMM_dxLeft = corrector * Results[iPhoto].dxLeft / (ptLensEye_pp[1].x - ptFlashReflex_pp[1].x);
			pixelInMM_dxRight = corrector * Results[iPhoto].dxRight / (ptLensEye_pp[0].x - ptFlashReflex_pp[0].x);
			pixelInMM_dyLeft = corrector * Results[iPhoto].dyLeft / (ptFlashReflex_pp[1].z - ptLensEye_pp[1].z) * cos(Results[iPhoto].alfaPantosk);
			pixelInMM_dyRight = corrector * Results[iPhoto].dyRight / (ptFlashReflex_pp[0].z - ptLensEye_pp[0].z) * cos(Results[iPhoto].alfaPantosk);
			// corrected scale factor
			double corAngle = 0.;
			corAngle = Results[iPhoto].defWrapAngle - Results[iPhoto].alfaPan;
            pixelInMM_dxLeft *= cos(corAngle);
			corAngle = Results[iPhoto].defWrapAngle + Results[iPhoto].alfaPan;
            pixelInMM_dxRight *= cos(corAngle);

			corAngle = Results[iPhoto].defWrapAngle - fabs(Results[iPhoto].defWrapAngle - Results[iPhoto].alfaPan);
			if (corAngle > 0.) {
				Results[iPhoto].pixelInMM_dxLeft /= cos(corAngle);
			}
			else {
				Results[iPhoto].pixelInMM_dxLeft *= cos(corAngle);
			}
			corAngle = Results[iPhoto].defWrapAngle - fabs(Results[iPhoto].defWrapAngle + Results[iPhoto].alfaPan);
            if (corAngle > 0.) {
                Results[iPhoto].pixelInMM_dxRight /= cos(corAngle);
            }
            else {
                Results[iPhoto].pixelInMM_dxRight *= cos(corAngle);
            }
*/
			// -----> Set Bridge
			Results[iPhoto].Bridge = ptWrapMarker[1].x - ptWrapMarker[0].x;
			double BridgeScale = ((lRect.x - rRect.x - 2) / (lRect.x - rRect.x));
			Results[iPhoto].Bridge = Results[iPhoto].Bridge * BridgeScale;
			// -----> Set PD
			Results[iPhoto].LeftPD = ptLensPD[1].x - ptCenter.x;
			Results[iPhoto].RightPD = ptCenter.x - ptLensPD[0].x;
			// lenses height points position
			xyzPoint ptLensConvexUp[2], ptLensConvexDown[2];
			xyzPoint ptLensNoseDown[2];
			for(iEye = 0; iEye < 2; iEye++) {
				double kXdown, bXdown, kZdown, bZdown;
				double kXup, bXup, kZup, bZup;
//				CalcLineCoefficients(ptCamera, ptRect[iEye], kXdown, bXdown, kZdown, bZdown);
//				CalcLineCoefficients(ptCamera, ptUpRect[iEye], kXup, bXup, kZup, bZup);
				CalcLineCoefficients(ptCamera, ptCalcDown[iEye], kXdown, bXdown, kZdown, bZdown);
				CalcLineCoefficients(ptCamera, ptCalcUp[iEye], kXup, bXup, kZup, bZup);
				ptLensConvexDown[iEye].y = ptLensCamera[iEye].y;
				ptLensConvexDown[iEye].x = kXdown * ptLensConvexDown[iEye].y + bXdown;
				ptLensConvexDown[iEye].z = kZdown * ptLensConvexDown[iEye].y + bZdown;
				ptLensConvexUp[iEye].y = ptLensCamera[iEye].y;
				ptLensConvexUp[iEye].x = kXup * ptLensConvexUp[iEye].y + bXup;
				ptLensConvexUp[iEye].z = kZup * ptLensConvexUp[iEye].y + bZup;

				CalcLineCoefficients(ptCamera, ptLensNose[iEye], kXdown, bXdown, kZdown, bZdown);
				ptLensNoseDown[iEye].y = ptLensCamera[iEye].y;
				ptLensNoseDown[iEye].x = kXdown * ptLensNoseDown[iEye].y + bXdown;
				ptLensNoseDown[iEye].z = kZdown * ptLensNoseDown[iEye].y + bZdown;
			}
			// -----> Set Height
			Results[iPhoto].LeftHeight = ptLensCamera[1].z - ptLensConvexDown[1].z;
			Results[iPhoto].RightHeight = ptLensCamera[0].z - ptLensConvexDown[0].z;
			// -----> Set Frame sizes
			xyzPoint ptAverageNose, ptAverageTemple, ptAverageWrapMarker, ptAverageConvexUp, ptAverageConvexDown;
			// calculate average points
			ptAverageNose.x = (ptLensNose[1].x - ptLensNose[0].x) / 2.;
			ptAverageNose.y = (ptLensNose[1].y + ptLensNose[0].y) / 2.;
			ptAverageNose.z = (ptLensNose[1].z + ptLensNose[0].z) / 2.;
			ptAverageTemple.x = (ptLensTemple[1].x - ptLensTemple[0].x) / 2.;
			ptAverageTemple.y = (ptLensTemple[1].y + ptLensTemple[0].y) / 2.;
			ptAverageTemple.z = (ptLensTemple[1].z + ptLensTemple[0].z) / 2.;
			ptAverageWrapMarker.x = (ptWrapMarker[1].x - ptWrapMarker[0].x) / 2.;
			ptAverageWrapMarker.y = (ptWrapMarker[1].y + ptWrapMarker[0].y) / 2.;
			ptAverageWrapMarker.z = (ptWrapMarker[1].z + ptWrapMarker[0].z) / 2.;

			ptAverageConvexDown.x = (ptLensConvexDown[1].x - ptLensConvexDown[0].x) / 2.;
			ptAverageConvexDown.y = (ptLensConvexDown[1].y + ptLensConvexDown[0].y) / 2.;
			ptAverageConvexDown.z = (ptLensConvexDown[1].z + ptLensConvexDown[0].z) / 2.;
			ptAverageConvexUp.x = (ptLensConvexUp[1].x - ptLensConvexUp[0].x) / 2.;
			ptAverageConvexUp.y = (ptLensConvexUp[1].y + ptLensConvexUp[0].y) / 2.;
			ptAverageConvexUp.z = (ptLensConvexUp[1].z + ptLensConvexUp[0].z) / 2.;
			switch(iPhoto) {
				case 0:
				case 1:
					// FarPD photo
					Results[iPhoto].FrameWidth = sqrt((ptAverageTemple.x - ptAverageWrapMarker.x) * (ptAverageTemple.x - ptAverageWrapMarker.x) + (ptAverageTemple.y - ptAverageWrapMarker.y) * (ptAverageTemple.y - ptAverageWrapMarker.y));
					Results[iPhoto].FrameHeight = ptAverageConvexUp.z - ptAverageConvexDown.z;
					break;
				case 2:
					// NearPD photo
					Results[iPhoto].FrameWidth = 0.;
					Results[iPhoto].FrameHeight = 0.;
					break;
			}
			// Calculate simple Wrap angle
			Results[iPhoto].alfaWrap = Photo[iPhoto].CalcWrapAngle(Results[iPhoto].alfaPan, Dist);
			// <<<<<<<<<<<<<<< Calculation =======================================================

			Results[iPhoto].bDataResults = true;
            
            // >>>>>>>>>>>>>>> Вычисленые пораметров Near по данным Far
            if (bNearCorrection && iPhoto == 0 && Results[2].bDataResults == true) {
                // Near camera position
                double Dist = Results[2].Dist; // Photo distance - Near data
                double PanAngle = Results[0].alfaPan; // Support rotation angle - Far data
                double PantoAngle = Results[2].alfaPantosk; // Support panto angle - Near data
                ptCamera.x = Dist * sin(PanAngle);
                ptCamera.y = -Dist * cos(PanAngle) * cos(PantoAngle);
                ptCamera.z = Dist * sin(PantoAngle) * cos(PanAngle);

                for(iEye = 0; iEye < 2; iEye++) {
                    // NearPD photo
                    if(Results[iPhoto].bHeadRotation) {
                        ptClientHasToLook.x = (ptEyeCenter[0].x + ptEyeCenter[1].x) / 2.;
                        ptClientHasToLook.y = ptCamera.y + yDeviceCenter;
                        ptClientHasToLook.z = ptCamera.z + zDeviceCenter;
                        ptClientHasToLookCamera = ptClientHasToLook;
                        ptClientHasToLookCamera.x = ptCamera.x;
                    }
                    else {
                        ptClientHasToLook.x = ptCamera.x;
                        ptClientHasToLook.y = ptCamera.y + yDeviceCenter;
                        ptClientHasToLook.z = ptCamera.z + zDeviceCenter;
                        ptClientHasToLookCamera = ptClientHasToLook;
                    }
                    double oa, ob, oc;
                    double kX, bX, kZ, bZ;
                    CalcLineCoefficients(ptClientHasToLook, ptEyeCenter[iEye], kX, bX, kZ, bZ);
                    // solution of the square equation
                    oa = kX * kX + 1.;
                    ob = 2. * kX * (bX - ptLensCenter[iEye].x) - 2. * ptLensCenter[iEye].y;
                    oc = (bX - ptLensCenter[iEye].x) * (bX - ptLensCenter[iEye].x) + ptLensCenter[iEye].y * ptLensCenter[iEye].y - lensRadius * lensRadius;
                    ptLensPD[iEye].y = (-ob - sqrt(ob * ob - 4. * oa * oc)) / (2. * oa);
                    ptLensPD[iEye].x = kX * ptLensPD[iEye].y + bX;
                    ptLensPD[iEye].z = kZ * ptLensPD[iEye].y + bZ;
                    // точка взгяда на линзе, если смотреть на камеру
                    CalcLineCoefficients(ptClientHasToLookCamera, ptEyeCenter[iEye], kX, bX, kZ, bZ);
                    // solution of the square equation
                    oa = kX * kX + 1.;
                    ob = 2. * kX * (bX - ptLensCenter[iEye].x) - 2. * ptLensCenter[iEye].y;
                    oc = (bX - ptLensCenter[iEye].x) * (bX - ptLensCenter[iEye].x) + ptLensCenter[iEye].y * ptLensCenter[iEye].y - lensRadius * lensRadius;
                    ptLensCamera[iEye].y = (-ob - sqrt(ob * ob - 4. * oa * oc)) / (2. * oa);
                    ptLensCamera[iEye].x = kX * ptLensCamera[iEye].y + bX;
                    ptLensCamera[iEye].z = kZ * ptLensCamera[iEye].y + bZ;
                }
                // -----> Set PD
                Results[2].LeftPD = ptLensPD[1].x - ptCenter.x;
                Results[2].RightPD = ptCenter.x - ptLensPD[0].x;
                // -----> Set Height
                Results[2].LeftHeight = ptLensCamera[1].z - ptLensConvexDown[1].z;
                Results[2].RightHeight = ptLensCamera[0].z - ptLensConvexDown[0].z;
            }
            if (bNearEmulation && iPhoto == 0 && Photo[2].nPhotoMode == nNoData) {
                // Near camera position
                Results[2].Dist = 400.;
                Results[2].alfaPantosk = -20. * M_PI / 180.;
                double Dist = Results[2].Dist; // Photo distance - Near data
                double PanAngle = Results[0].alfaPan; // Support rotation angle - Far data
                double PantoAngle = Results[2].alfaPantosk; // Support panto angle - Near data
                ptCamera.x = Dist * sin(PanAngle);
                ptCamera.y = -Dist * cos(PanAngle) * cos(PantoAngle);
                ptCamera.z = Dist * sin(PantoAngle) * cos(PanAngle);
                
                for(iEye = 0; iEye < 2; iEye++) {
                    // NearPD photo
                    if(Results[iPhoto].bHeadRotation) {
                        ptClientHasToLook.x = (ptEyeCenter[0].x + ptEyeCenter[1].x) / 2.;
                        ptClientHasToLook.y = ptCamera.y + yDeviceCenter;
                        ptClientHasToLook.z = ptCamera.z + zDeviceCenter;
                        ptClientHasToLookCamera = ptClientHasToLook;
                        ptClientHasToLookCamera.x = ptCamera.x;
                    }
                    else {
                        ptClientHasToLook.x = ptCamera.x;
                        ptClientHasToLook.y = ptCamera.y + yDeviceCenter;
                        ptClientHasToLook.z = ptCamera.z + zDeviceCenter;
                        ptClientHasToLookCamera = ptClientHasToLook;
                    }
                    double oa, ob, oc;
                    double kX, bX, kZ, bZ;
                    CalcLineCoefficients(ptClientHasToLook, ptEyeCenter[iEye], kX, bX, kZ, bZ);
                    // solution of the square equation
                    oa = kX * kX + 1.;
                    ob = 2. * kX * (bX - ptLensCenter[iEye].x) - 2. * ptLensCenter[iEye].y;
                    oc = (bX - ptLensCenter[iEye].x) * (bX - ptLensCenter[iEye].x) + ptLensCenter[iEye].y * ptLensCenter[iEye].y - lensRadius * lensRadius;
                    ptLensPD[iEye].y = (-ob - sqrt(ob * ob - 4. * oa * oc)) / (2. * oa);
                    ptLensPD[iEye].x = kX * ptLensPD[iEye].y + bX;
                    ptLensPD[iEye].z = kZ * ptLensPD[iEye].y + bZ;
                    // точка взгяда на линзе, если смотреть на камеру
                    CalcLineCoefficients(ptClientHasToLookCamera, ptEyeCenter[iEye], kX, bX, kZ, bZ);
                    // solution of the square equation
                    oa = kX * kX + 1.;
                    ob = 2. * kX * (bX - ptLensCenter[iEye].x) - 2. * ptLensCenter[iEye].y;
                    oc = (bX - ptLensCenter[iEye].x) * (bX - ptLensCenter[iEye].x) + ptLensCenter[iEye].y * ptLensCenter[iEye].y - lensRadius * lensRadius;
                    ptLensCamera[iEye].y = (-ob - sqrt(ob * ob - 4. * oa * oc)) / (2. * oa);
                    ptLensCamera[iEye].x = kX * ptLensCamera[iEye].y + bX;
                    ptLensCamera[iEye].z = kZ * ptLensCamera[iEye].y + bZ;
                }
                // -----> Set PD
                Results[2].LeftPD = ptLensPD[1].x - ptCenter.x;
                Results[2].RightPD = ptCenter.x - ptLensPD[0].x;
                // Осредняем
                double dPD = ((Results[0].LeftPD - Results[2].LeftPD) + (Results[0].RightPD - Results[2].RightPD)) / 2.;
                Results[2].LeftPD = Results[0].LeftPD - dPD;
                Results[2].RightPD = Results[0].RightPD - dPD;
                // -----> Set Height
                Results[2].LeftHeight = ptLensCamera[1].z - ptLensConvexDown[1].z;
                Results[2].RightHeight = ptLensCamera[0].z - ptLensConvexDown[0].z;
                
                Results[2].bHeadRotation = bHeadRotation;
                Results[2].bDataResults = true;
            }
            // <<<<<<<<<<<<<<< Вычисленые пораметров Near по данным Far

			bRes = true;
		}
	}
	return bRes;
}
void CPDMResults::CalcLineCoefficients(xyzPoint ptFirst, xyzPoint ptSecond, double& kX, double& bX, double& kZ, double& bZ)
{
	kX = (ptSecond.x - ptFirst.x) / (ptSecond.y - ptFirst.y);
	kZ = (ptSecond.z - ptFirst.z) / (ptSecond.y - ptFirst.y);
	bX = ptFirst.x - kX * ptFirst.y;
	bZ = ptFirst.z - kZ * ptFirst.y;
}
xyzPoint CPDMResults::ConvertPointToProjectionPlane(xyzPoint pt, double PantoAngle)
{
	xyzPoint ptPro;
	ptPro.z = pt.z / cos(PantoAngle);
	ptPro.y = pt.y - ptPro.z * sin(PantoAngle);
	ptPro.x = pt.x;
	if(pt.x >= 0.)
		ptPro.x = sqrt(ptPro.x * ptPro.x + ptPro.y * ptPro.y);
	else
		ptPro.x = -sqrt(ptPro.x * ptPro.x + ptPro.y * ptPro.y);
	ptPro.y = 0.;
	return ptPro;
}
void CPDMResults::SetWrapAngle(bool bWrapCalc, double WrapAngle)
{
    Results2.bWrapCalc = bWrapCalc;
    if (bWrapCalc == false && checkDoubleValue(WrapAngle)) {
        Results2.WrapAngle = WrapAngle;
        Results[0].defWrapAngle = Results2.WrapAngle;
        Results[2].defWrapAngle = Results2.WrapAngle;
    }
}
// Vertex functional
void CPDMResults::SetVertex(bool bVertexCalc, double dblVertex)
{
    Results2.bVertexCalc = bVertexCalc;
    if (bVertexCalc == false && checkDoubleValue(dblVertex)) {
        Results2.LeftVD = dblVertex;
        Results2.RightVD = dblVertex;
    }
}
void CPDMResults::SetDefVertex(double dblVertex)
{
    Results[0].defVertex = dblVertex;
    Results[2].defVertex = dblVertex;
}
double CPDMResults::GetCorrectedUpfit(double minNearHeight)
{
    double Upfit = 0.;
    if (Results[0].bDataResults && Results[2].bDataResults) {
        // check results data
        Results[0].checkResultsData(0);
        Results[2].checkResultsData(2);
        // Progressive corridor
        Upfit = floor((((Results[0].LeftHeight - fmax(Results[2].LeftHeight, minNearHeight)) + (Results[0].RightHeight - fmax(Results[2].RightHeight, minNearHeight))) / 2.) + 0.5);
    }
    return Upfit;
}

bool CPDMResults::CalcPhotoResults2(bool bUseNearPhoto)
{
    bool bRes = false;
    bool bWrap = false;
    bool bVD = false;
//	double calcWrap = 5. * M_PI / 180.;
	double calcLeftVD = 14.;
	double calcRightVD = 14.;
	// Wrap calculate
    if (bUseNearPhoto) {
        if (Results2.bWrapCalc) {
            // Need calculate Wrap angle
            bWrap = CalcWrapAngle3_far_near();
            double wrapAngle2 = Results2.WrapAngleMarkers[4];
            Results2.WrapAngle = wrapAngle2;
        }
    }
    else {
        bWrap = CalcWrapAngle2();
    }
//	calcWrap = Results2.WrapAngle;
    if (bWrap) {
        if (Results2.WrapAngle < minLimitWrapAngle) {
            Results2.WrapAngle = minLimitWrapAngle;
        }
        else if (Results2.WrapAngle > maxLimitWrapAngle) {
            Results2.WrapAngle = maxLimitWrapAngle;
        }
    }
	// Vertex calculate
	if (bUseNearPhoto) {
        if (Results2.bVertexCalc) {
            bVD = CalcVD3_far_near();
        }
        else {
            bVD = true;
        }
	}
	else {
		bVD = CalcVD2();
	}
	calcLeftVD = Results2.LeftVD;
	calcRightVD = Results2.RightVD;
    if (bVD) {
        if (Results2.LeftVD < minLimitVertex) {
            Results2.LeftVD = minLimitVertex;
        }
        else if (Results2.LeftVD > maxLimitVertex) {
            Results2.LeftVD = maxLimitVertex;
        }
        if (Results2.RightVD < minLimitVertex) {
            Results2.RightVD = minLimitVertex;
        }
        else if (Results2.RightVD > maxLimitVertex) {
            Results2.RightVD = maxLimitVertex;
        }
    }
    
    if (bVD) {
		if (bUseNearPhoto) {
			Results2.bDataResults = true;
			Results2.bPointsResults = false;
//			if(fabs(calcWrap - Results2.WrapAngle) < 0.5 && fabs(calcLeftVD - Results2.LeftVD) < 0.5 && fabs(calcRightVD - Results2.RightVD) < 0.5)
//				Results2.bPointsResults = true;
		}
        if (Results2.LeftVD >= 5. && Results2.LeftVD <= 21. && Results2.RightVD >= 5. && Results2.RightVD <= 21.) {
            int iPhoto;
            iPhoto = 2;
            Results[iPhoto].LeftVD = Results2.LeftVD;
            Results[iPhoto].RightVD = Results2.RightVD;
            Results[iPhoto].defWrapAngle = Results2.WrapAngle;
            Results[iPhoto].bDataResults = false;
            CalcPhotoResults(iPhoto);
            iPhoto = 0;
            Results[iPhoto].LeftVD = Results2.LeftVD;
            Results[iPhoto].RightVD = Results2.RightVD;
            Results[iPhoto].defWrapAngle = Results2.WrapAngle;
            Results[iPhoto].bDataResults = false;
            CalcPhotoResults(iPhoto);
        }
        if (Results[0].bDataResults && Results[2].bDataResults) {
            // Progressive corridor
            Results2.Upfit = floor((((Results[0].LeftHeight - Results[2].LeftHeight) + (Results[0].RightHeight - Results[2].RightHeight)) / 2.) + 0.5);
            // Inset
            Results2.LeftInset = Results[0].LeftPD - Results[2].LeftPD;
            Results2.RightInset = Results[0].RightPD - Results[2].RightPD;
        }
        else {
            Results2.Upfit = 0.;
            Results2.LeftInset = 0.;
            Results2.RightInset = 0.;
        }
		Results2.bDataResults = true;
        bRes = true;
    }
    return bRes;
}
CPDMDataResults2 CPDMResults::GetPhotoResults2()
{
    CPDMDataResults2 Data = Results2;
    double VD = (Data.LeftVD + Data.RightVD) / 2.;
    Data.LeftVD = VD;
    Data.RightVD = VD;
    // check results data
    Data.checkResults2Data(); // -----------------------------> check data
    return Data;
}
void CPDMResults::SetFlashType(PDMFlashType nFlashType)
{
    Device.SetFlashType(nFlashType);
}
PDMFlashType CPDMResults::GetFlashType()
{
    return Device.GetFlashType();
}
double CPDMResults::GetFcoef(int iPhoto)
{
    double Fcoef = 0.;
    if (Photo[iPhoto].nPhotoMode == nFarPD) {
        // корректное расстояние между крайними точками Суппорта на глубине оправы в пикселях
        Fcoef = 1000. * Photo[iPhoto].kFarPD / kPDmm;
    }
    else {
        // корректное расстояние между крайними точками Суппорта на глубине оправы в пикселях
        Fcoef = 1000. * Photo[iPhoto].kNearPD / kPDmm;
    }
    return Fcoef;
}
void CPDMResults::SetDiameter(bool bLeft, double Diam)
{
    if (Diam > 0. ) {
        if (bLeft) {
            Results2.LeftDiameter = Diam;
        }
        else {
            Results2.RightDiameter = Diam;
        }
        Results2.bDataDiameter = true;
    }
}
double CPDMResults::GetDiameter(bool bLeft)
{
    double Diam;
    if (bLeft) {
        Diam = Results2.LeftDiameter;
    }
    else {
        Diam = Results2.RightDiameter;
    }
    return Diam;
}
bool CPDMResults::GetDiameterStatus()
{
    return Results2.bDataDiameter;
}
void CPDMResults::SetProgressiveDiameterStatus(bool bProgressiveStatus)
{
    Results2.bProgressiveDiameter = bProgressiveStatus;
}
bool CPDMResults::GetProgressiveDiameterStatus()
{
    return Results2.bProgressiveDiameter;
}
bool CPDMResults::CalcVD2()
{
    bool bRes = false;
    double xObjective[2] = {0., 0.};
    double yObjective[2] = {0., 0.};
    double xCP2[2] = {0., 0.};
    double yCP2[2] = {0., 0.};
    double xCP3[2] = {0., 0.};
    double yCP3[2] = {0., 0.};
    double kA1[2] = {0., 0.}, kB1[2] = {0., 0.}, kC1[2] = {0., 0.};
    double kA2[2] = {0., 0.}, kB2[2] = {0., 0.}, kC2[2] = {0., 0.};
    double hlEye = 0., hrEye = 0.;
    double dxlSupportEye = 0., dxrSupportEye = 0.;
    int iPhoto = 0;
    for (iPhoto = 0; iPhoto < 2; iPhoto++) {
        if(CalcPhotoResults(iPhoto) == false)
                return false;
        double Dist = Results[iPhoto].Dist;
		double PanAngle = Results[iPhoto].alfaPan;
        xObjective[iPhoto] = Dist * sin(Results[iPhoto].alfaPan);
        yObjective[iPhoto] = -Dist * cos(Results[iPhoto].alfaPan);
        
        double AO = sqrt(Dist * Dist + Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist - 2. * Photo[iPhoto].sup.acDist * Dist * cos(M_PI_2 + Results[iPhoto].alfaPan));
        double alpha1 = acos((Dist * Dist + AO * AO - Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist) / (2. * Dist * AO));
        double CP1 = Dist * tan(alpha1);
        double xCP1 = -CP1 * cos(Results[iPhoto].alfaPan);
        double yCP1 = -CP1 * sin(Results[iPhoto].alfaPan);
        
        double BO = sqrt(Dist * Dist + Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist - 2. * Photo[iPhoto].sup.acDist * Dist * cos(M_PI_2 - Results[iPhoto].alfaPan));
        double alpha4 = acos((Dist * Dist + BO * BO - Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist) / (2. * Dist * BO));
        double CP4 = Dist * tan(alpha4);
        double xCP4 = CP4 * cos(Results[iPhoto].alfaPan);
        double yCP4 = CP4 * sin(Results[iPhoto].alfaPan);
        
        // делаем поворот, выравниваем наклон головы "вправо - влево"
        double xlSupport, ylSupport, xrSupport, yrSupport;
        double xlEye, ylEye, xrEye, yrEye;
        double dx, dy;
        double fi = -Results[iPhoto].alfaTilt;   // угол поворота
        // поворот делаем относительно точки Right
        xrSupport = Photo[iPhoto].xRightSupport;
        yrSupport = Photo[iPhoto].yRightSupport;
        // поворачиваем Левую точку Суппорта относительно правой точки Суппорта
        dx = Photo[iPhoto].xLeftSupport - xrSupport;
        dy = Photo[iPhoto].yLeftSupport - yrSupport;
        xlSupport = dx * cos(fi) - dy * sin(fi);
        ylSupport = dx * sin(fi) + dy * cos(fi);
        xlSupport += xrSupport;
        ylSupport += yrSupport;
        // поворачиваем точку Правого зрачка относительно правой точки Суппорта
        dx = Photo[iPhoto].xRightEye - xrSupport;
        dy = Photo[iPhoto].yRightEye - yrSupport;
        xrEye = dx * cos(fi) - dy * sin(fi);
        yrEye = dx * sin(fi) + dy * cos(fi);
        xrEye += xrSupport;
        yrEye += yrSupport;
        // поворачиваем точку Левого зрачка относительно правой точки Суппорта
        dx = Photo[iPhoto].xLeftEye - xrSupport;
        dy = Photo[iPhoto].yLeftEye - yrSupport;
        xlEye = dx * cos(fi) - dy * sin(fi);
        ylEye = dx * sin(fi) + dy * cos(fi);
        xlEye += xrSupport;
        ylEye += yrSupport;
        
        if (iPhoto == 0) {
            hlEye = Photo[0].convertToCM(ylEye - yrSupport, PanAngle, Dist + Photo[iPhoto].sup.SupportDistance);
            hrEye = Photo[0].convertToCM(yrEye - yrSupport, PanAngle, Dist + Photo[iPhoto].sup.SupportDistance);
            dxlSupportEye = Photo[0].convertToCM(xlSupport - xlEye, PanAngle, Dist) - (Photo[iPhoto].sup.abDist / 2. - Photo[iPhoto].sup.xFastening);
            dxrSupportEye = Photo[0].convertToCM(xrEye - xrSupport, PanAngle, Dist) - (Photo[iPhoto].sup.abDist / 2. - Photo[iPhoto].sup.xFastening);
        }
        
        xCP2[iPhoto] = xCP1 + (xCP4 - xCP1) * (xrEye - xrSupport) / (xlSupport - xrSupport);
        yCP2[iPhoto] = yCP1 + (yCP4 - yCP1) * (xrEye - xrSupport) / (xlSupport - xrSupport);
        xCP3[iPhoto] = xCP1 + (xCP4 - xCP1) * (xlEye - xrSupport) / (xlSupport - xrSupport);
        yCP3[iPhoto] = yCP1 + (yCP4 - yCP1) * (xlEye - xrSupport) / (xlSupport - xrSupport);
        
        // коэффициенты линии
        kA1[iPhoto] = yCP2[iPhoto] - yObjective[iPhoto];
        kB1[iPhoto] = xObjective[iPhoto] - xCP2[iPhoto];
        kC1[iPhoto] = yObjective[iPhoto] * (xCP2[iPhoto] - xObjective[iPhoto]) - xObjective[iPhoto] * (yCP2[iPhoto] - yObjective[iPhoto]);
        kA2[iPhoto] = yCP3[iPhoto] - yObjective[iPhoto];
        kB2[iPhoto] = xObjective[iPhoto] - xCP3[iPhoto];
        kC2[iPhoto] = yObjective[iPhoto] * (xCP3[iPhoto] - xObjective[iPhoto]) - xObjective[iPhoto] * (yCP3[iPhoto] - yObjective[iPhoto]);
    }
    // точка пересечения прямых
    // правый глаз
    double del1 = kA1[0] * kB1[1] - kA1[1] * kB1[0];
    if(fabs(del1) < 0.001)
        return false;
    double dxrEye = (kC1[1] * kB1[0] - kC1[0] * kB1[1]) / del1;
    double dyrEye = (kA1[1] * kC1[0] - kA1[0] * kC1[1]) / del1;
    // левый глаз
    double del2 = kA2[0] * kB2[1] - kA2[1] * kB2[0];
    if(fabs(del2) < 0.001)
        return false;
    double dxlEye = (kC2[1] * kB2[0] - kC2[0] * kB2[1]) / del2;
    double dylEye = (kA2[1] * kC2[0] - kA2[0] * kC2[1]) / del2;
    
    // среднее значение Wrap угла 5 градусов
    double WrapAngle = 5. * M_PI / 180.;
    // VD - vertex distance
//    Results2.LeftVD = (dylEye - hlEye * tan(Results[0].alfaPantosk) - EyeRadius) * cos(Results[0].alfaPantosk);
//    Results2.RightVD = (dyrEye - hrEye * tan(Results[0].alfaPantosk) - EyeRadius) * cos(Results[0].alfaPantosk);
    double LeftVD = (dylEye - Photo[iPhoto].sup.SupportDistance - EyeRadius) * cos(Results[0].alfaPantoskHor);
    double RightVD = (dyrEye - Photo[iPhoto].sup.SupportDistance - EyeRadius) * cos(Results[0].alfaPantoskHor);
//    double dxlrSupportEye = (dxlSupportEye + dxrSupportEye) / 2.;
//    LeftVD += dxlrSupportEye * tan(WrapAngle);
//    RightVD += dxlrSupportEye * tan(WrapAngle);
    
    double lrVD = (LeftVD + RightVD) / 2.;
    
    Results2.LeftVD = lrVD;
    Results2.RightVD = lrVD;
    bRes = true;
    
    return bRes;
}
bool CPDMResults::CalcWrapAngle2()
{
    bool bRes = false;
    double xObjective[2] = {0., 0.};
    double yObjective[2] = {0., 0.};
    double xCP2[2] = {0., 0.}, xUpCP2[2] = {0., 0.};
    double yCP2[2] = {0., 0.}, yUpCP2[2] = {0., 0.};
    double xCP3[2] = {0., 0.}, xUpCP3[2] = {0., 0.};
    double yCP3[2] = {0., 0.}, yUpCP3[2] = {0., 0.};
    double kA1[2] = {0., 0.}, kB1[2] = {0., 0.}, kC1[2] = {0., 0.};
    double kA2[2] = {0., 0.}, kB2[2] = {0., 0.}, kC2[2] = {0., 0.};
    double kUpA1[2] = {0., 0.}, kUpB1[2] = {0., 0.}, kUpC1[2] = {0., 0.};
    double kUpA2[2] = {0., 0.}, kUpB2[2] = {0., 0.}, kUpC2[2] = {0., 0.};
    int iPhoto = 0;
    for (iPhoto = 0; iPhoto < 2; iPhoto++) {
        if(CalcPhotoResults(iPhoto) == false)
            return false;
        double Dist = Results[iPhoto].Dist;
        xObjective[iPhoto] = Dist * sin(Results[iPhoto].alfaPan);
        yObjective[iPhoto] = -Dist * cos(Results[iPhoto].alfaPan);
        
        double AO = sqrt(Dist * Dist + Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist - 2. * Photo[iPhoto].sup.acDist * Dist * cos(M_PI_2 + Results[iPhoto].alfaPan));
        double alpha1 = acos((Dist * Dist + AO * AO - Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist) / (2. * Dist * AO));
        double CP1 = Dist * tan(alpha1);
        double xCP1 = -CP1 * cos(Results[iPhoto].alfaPan);
        double yCP1 = -CP1 * sin(Results[iPhoto].alfaPan);
        
        double BO = sqrt(Dist * Dist + Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist - 2. * Photo[iPhoto].sup.acDist * Dist * cos(M_PI_2 - Results[iPhoto].alfaPan));
        double alpha4 = acos((Dist * Dist + BO * BO - Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist) / (2. * Dist * BO));
        double CP4 = Dist * tan(alpha4);
        double xCP4 = CP4 * cos(Results[iPhoto].alfaPan);
        double yCP4 = CP4 * sin(Results[iPhoto].alfaPan);
        
        // делаем поворот, выравниваем наклон головы "вправо - влево"
        double xlSupport, ylSupport, xrSupport, yrSupport;
        double xlRect, ylRect, xrRect, yrRect;
        double xlUpRect, ylUpRect, xrUpRect, yrUpRect;
        double dx, dy;
        double fi = -Results[iPhoto].alfaTilt;   // угол поворота
        // поворот делаем относительно точки Right
        xrSupport = Photo[iPhoto].xRightSupport;
        yrSupport = Photo[iPhoto].yRightSupport;
        // поворачиваем Левую точку Суппорта относительно правой точки Суппорта
        dx = Photo[iPhoto].xLeftSupport - xrSupport;
        dy = Photo[iPhoto].yLeftSupport - yrSupport;
        xlSupport = dx * cos(fi) - dy * sin(fi);
        ylSupport = dx * sin(fi) + dy * cos(fi);
        xlSupport += xrSupport;
        ylSupport += yrSupport;
        // поворачиваем точку Правого верхнего угла относительно правой точки Суппорта
        dx = Photo[iPhoto].xRightUpRect - xrSupport;
        dy = Photo[iPhoto].yRightUpRect - yrSupport;
        xrUpRect = dx * cos(fi) - dy * sin(fi);
        yrUpRect = dx * sin(fi) + dy * cos(fi);
        xrUpRect += xrSupport;
        yrUpRect += yrSupport;
        // поворачиваем точку Левого верхнего угла относительно правой точки Суппорта
        dx = Photo[iPhoto].xLeftUpRect - xrSupport;
        dy = Photo[iPhoto].yLeftUpRect - yrSupport;
        xlUpRect = dx * cos(fi) - dy * sin(fi);
        ylUpRect = dx * sin(fi) + dy * cos(fi);
        xlUpRect += xrSupport;
        ylUpRect += yrSupport;
        // поворачиваем точку Правого нижнего угла относительно правой точки Суппорта
        dx = Photo[iPhoto].xRightRect - xrSupport;
        dy = Photo[iPhoto].yRightRect - yrSupport;
        xrRect = dx * cos(fi) - dy * sin(fi);
        yrRect = dx * sin(fi) + dy * cos(fi);
        xrRect += xrSupport;
        yrRect += yrSupport;
        // поворачиваем точку Левого нижнего угла относительно правой точки Суппорта
        dx = Photo[iPhoto].xLeftRect - xrSupport;
        dy = Photo[iPhoto].yLeftRect - yrSupport;
        xlRect = dx * cos(fi) - dy * sin(fi);
        ylRect = dx * sin(fi) + dy * cos(fi);
        xlRect += xrSupport;
        ylRect += yrSupport;
        
        xCP2[iPhoto] = xCP1 + (xCP4 - xCP1) * (xrRect - xrSupport) / (xlSupport - xrSupport);
        yCP2[iPhoto] = yCP1 + (yCP4 - yCP1) * (xrRect - xrSupport) / (xlSupport - xrSupport);
        xCP3[iPhoto] = xCP1 + (xCP4 - xCP1) * (xlRect - xrSupport) / (xlSupport - xrSupport);
        yCP3[iPhoto] = yCP1 + (yCP4 - yCP1) * (xlRect - xrSupport) / (xlSupport - xrSupport);
        xUpCP2[iPhoto] = xCP1 + (xCP4 - xCP1) * (xrUpRect - xrSupport) / (xlSupport - xrSupport);
        yUpCP2[iPhoto] = yCP1 + (yCP4 - yCP1) * (xrUpRect - xrSupport) / (xlSupport - xrSupport);
        xUpCP3[iPhoto] = xCP1 + (xCP4 - xCP1) * (xlUpRect - xrSupport) / (xlSupport - xrSupport);
        yUpCP3[iPhoto] = yCP1 + (yCP4 - yCP1) * (xlUpRect - xrSupport) / (xlSupport - xrSupport);
        
        // коэффициенты линии
        kA1[iPhoto] = yCP2[iPhoto] - yObjective[iPhoto];
        kB1[iPhoto] = xObjective[iPhoto] - xCP2[iPhoto];
        kC1[iPhoto] = yObjective[iPhoto] * (xCP2[iPhoto] - xObjective[iPhoto]) - xObjective[iPhoto] * (yCP2[iPhoto] - yObjective[iPhoto]);
        kA2[iPhoto] = yCP3[iPhoto] - yObjective[iPhoto];
        kB2[iPhoto] = xObjective[iPhoto] - xCP3[iPhoto];
        kC2[iPhoto] = yObjective[iPhoto] * (xCP3[iPhoto] - xObjective[iPhoto]) - xObjective[iPhoto] * (yCP3[iPhoto] - yObjective[iPhoto]);

        kUpA1[iPhoto] = yUpCP2[iPhoto] - yObjective[iPhoto];
        kUpB1[iPhoto] = xObjective[iPhoto] - xUpCP2[iPhoto];
        kUpC1[iPhoto] = yObjective[iPhoto] * (xUpCP2[iPhoto] - xObjective[iPhoto]) - xObjective[iPhoto] * (yUpCP2[iPhoto] - yObjective[iPhoto]);
        kUpA2[iPhoto] = yUpCP3[iPhoto] - yObjective[iPhoto];
        kUpB2[iPhoto] = xObjective[iPhoto] - xUpCP3[iPhoto];
        kUpC2[iPhoto] = yObjective[iPhoto] * (xUpCP3[iPhoto] - xObjective[iPhoto]) - xObjective[iPhoto] * (yUpCP3[iPhoto] - yObjective[iPhoto]);
    }
    // точка пересечения прямых
    // правый глаз
    double del1 = kA1[0] * kB1[1] - kA1[1] * kB1[0];
    if(fabs(del1) < 0.001)
        return false;
    double dxrRect = (kC1[1] * kB1[0] - kC1[0] * kB1[1]) / del1;
    double dyrRect = (kA1[1] * kC1[0] - kA1[0] * kC1[1]) / del1;
    // левый глаз
    double del2 = kA2[0] * kB2[1] - kA2[1] * kB2[0];
    if(fabs(del2) < 0.001)
        return false;
    double dxlRect = (kC2[1] * kB2[0] - kC2[0] * kB2[1]) / del2;
    double dylRect = (kA2[1] * kC2[0] - kA2[0] * kC2[1]) / del2;
    // правый глаз
    double delUp1 = kUpA1[0] * kUpB1[1] - kUpA1[1] * kUpB1[0];
    if(fabs(delUp1) < 0.001)
        return false;
    double dxrUpRect = (kUpC1[1] * kUpB1[0] - kUpC1[0] * kUpB1[1]) / delUp1;
    double dyrUpRect = (kUpA1[1] * kUpC1[0] - kUpA1[0] * kUpC1[1]) / delUp1;
    // левый глаз
    double delUp2 = kUpA2[0] * kUpB2[1] - kUpA2[1] * kUpB2[0];
    if(fabs(delUp2) < 0.001)
        return false;
    double dxlUpRect = (kUpC2[1] * kUpB2[0] - kUpC2[0] * kUpB2[1]) / delUp2;
    double dylUpRect = (kUpA2[1] * kUpC2[0] - kUpA2[0] * kUpC2[1]) / delUp2;
    
    // угол сгиба оправы
    double aWrapRight = atan(fabs(dyrRect - dyrUpRect) / fabs(dxrRect - dxrUpRect));
    double aWrapLeft = atan(fabs(dylRect - dylUpRect) / fabs(dxlRect - dxlUpRect));
//    double agRight = aWrapRight * 180. / M_PI;
//    double agLeft = aWrapLeft * 180. / M_PI;
//    double ag1 = Results[1].alfaWrap * 180. / M_PI;
    double aWrapS = (aWrapLeft + aWrapRight) / 2.;
    if (aWrapS * 180. / M_PI < 0. || aWrapS * 180. / M_PI > maxWrapAngle) {
        aWrapS = 0.;
    }
    
    // определяем угол сгиба оправы учитывая все возможные комбинации
    if (aWrapS > 0. && Results[1].alfaWrap > 0.) {
        Results2.WrapAngle = aWrapS * 0.75 + Results[1].alfaWrap * 0.25;
        bRes = true;
    }
    else if (aWrapS > 0.) {
        Results2.WrapAngle = aWrapS;
        bRes = true;
    }
    else {
        int nCw = 0;
        double aWrap[3];
        if (aWrapS > 0.) {
            aWrap[nCw] = aWrapS;
            nCw++;
        }
        if (Results[0].alfaWrap > 0.) {
            aWrap[nCw] = Results[0].alfaWrap;
            nCw++;
        }
        if (Results[1].alfaWrap > 0.) {
            aWrap[nCw] = Results[1].alfaWrap;
            nCw++;
        }
        if (nCw == 0) {
            Results2.WrapAngle = 0.;
        }
        else {
            int i = 0, iMin = 0, iMax = 0;
            double dMin = aWrap[i], dMax = aWrap[i];
            for (i = 1; i < nCw; i++) {
                if (aWrap[i] < dMin) {
                    dMin = aWrap[i];
                    iMin = i;
                }
                if (aWrap[i] > dMax) {
                    dMax = aWrap[i];
                    iMax = i;
                }
            }
            switch (nCw) {
                case 1:
                    Results2.WrapAngle = aWrap[0];
                    break;
                case 2:
                    Results2.WrapAngle = (aWrap[0] + aWrap[1]) / 2.;
                    break;
                case 3:
                    for (i = 0; i < 3; i++) {
                        if (i != iMin && i != iMax) {
                            Results2.WrapAngle = aWrap[i];
                            break;
                        }
                    }
                    break;
                    
                default:
                    break;
            }
            
            bRes = true;
        }
    }
    
    return bRes;
}
// вычисление размеров оправы
bool CPDMResults::CalcFrameSize(int iPhoto)
{
    bool bRes = false;

    double Dist = Results[iPhoto].Dist;
    
    // делаем поворот, выравниваем наклон головы "вправо - влево"
    double xlSupport, ylSupport, xrSupport, yrSupport;
    double xlRect, ylRect, xrRect, yrRect;
    double xlUpRect, ylUpRect, xrUpRect, yrUpRect;
    double dx, dy;
    double fi = -Results[iPhoto].alfaTilt;   // угол поворота
    // поворот делаем относительно точки Right
    xrSupport = Photo[iPhoto].xRightSupport;
    yrSupport = Photo[iPhoto].yRightSupport;
    // поворачиваем Левую точку Суппорта относительно правой точки Суппорта
    dx = Photo[iPhoto].xLeftSupport - xrSupport;
    dy = Photo[iPhoto].yLeftSupport - yrSupport;
    xlSupport = dx * cos(fi) - dy * sin(fi);
    ylSupport = dx * sin(fi) + dy * cos(fi);
    xlSupport += xrSupport;
    ylSupport += yrSupport;
    // поворачиваем точку Правого верхнего угла относительно правой точки Суппорта
    dx = Photo[iPhoto].xRightUpRect - xrSupport;
    dy = Photo[iPhoto].yRightUpRect - yrSupport;
    xrUpRect = dx * cos(fi) - dy * sin(fi);
    yrUpRect = dx * sin(fi) + dy * cos(fi);
    xrUpRect += xrSupport;
    yrUpRect += yrSupport;
    // поворачиваем точку Левого верхнего угла относительно правой точки Суппорта
    dx = Photo[iPhoto].xLeftUpRect - xrSupport;
    dy = Photo[iPhoto].yLeftUpRect - yrSupport;
    xlUpRect = dx * cos(fi) - dy * sin(fi);
    ylUpRect = dx * sin(fi) + dy * cos(fi);
    xlUpRect += xrSupport;
    ylUpRect += yrSupport;
    // поворачиваем точку Правого нижнего угла относительно правой точки Суппорта
    dx = Photo[iPhoto].xRightRect - xrSupport;
    dy = Photo[iPhoto].yRightRect - yrSupport;
    xrRect = dx * cos(fi) - dy * sin(fi);
    yrRect = dx * sin(fi) + dy * cos(fi);
    xrRect += xrSupport;
    yrRect += yrSupport;
    // поворачиваем точку Левого нижнего угла относительно правой точки Суппорта
    dx = Photo[iPhoto].xLeftRect - xrSupport;
    dy = Photo[iPhoto].yLeftRect - yrSupport;
    xlRect = dx * cos(fi) - dy * sin(fi);
    ylRect = dx * sin(fi) + dy * cos(fi);
    xlRect += xrSupport;
    ylRect += yrSupport;
    // Центральная точка
//    double centerX = Photo[iPhoto].CalcCenterPoint(xrRect, xlRect, Dist + SupportDistance, Results[iPhoto].alfaPan);
    
    double WrapAngle = Results[iPhoto].defWrapAngle;

    double dxLFrame = Photo[iPhoto].convertToCM((xlUpRect - xlRect + 4.) / cos(Results[iPhoto].alfaPan), Results[iPhoto].alfaPan, Dist + Photo[iPhoto].sup.SupportDistance) / cos(WrapAngle);
    double dxRFrame = Photo[iPhoto].convertToCM((xrRect - xrUpRect + 4.) / cos(Results[iPhoto].alfaPan), Results[iPhoto].alfaPan, Dist + Photo[iPhoto].sup.SupportDistance) / cos(WrapAngle);
    double dyLFrame = Photo[iPhoto].convertToCM((ylRect - ylUpRect + 1.5), Results[iPhoto].alfaPan, Dist + Photo[iPhoto].sup.SupportDistance) / cos(Results[iPhoto].alfaPantosk);
    double dyRFrame = Photo[iPhoto].convertToCM((yrRect - yrUpRect + 1.5), Results[iPhoto].alfaPan, Dist + Photo[iPhoto].sup.SupportDistance) / cos(Results[iPhoto].alfaPantosk);

    Results[iPhoto].FrameWidth = (dxLFrame + dxRFrame) / 2.;
    Results[iPhoto].FrameHeight = (dyLFrame + dyRFrame) / 2.;

    return bRes;
}

xyzPoint CPDMResults::CalcMinDistPoint(xyzPoint pt11, xyzPoint pt12, xyzPoint pt21, xyzPoint pt22)
{
    xyzPoint ptRes;
    double kX1, bX1, kZ1, bZ1;
    double kX2, bX2, kZ2, bZ2;
    CalcLineCoefficients(pt11, pt12, kX1, bX1, kZ1, bZ1);
    CalcLineCoefficients(pt21, pt22, kX2, bX2, kZ2, bZ2);
    double dLen[800];
    int i = 0;
    for (i = 0; i < 800; i++) {
        double y = i / 10.;
        double x1 = kX1 * y + bX1;
        double z1 = kZ1 * y + bZ1;
        double x2 = kX2 * y + bX2;
        double z2 = kZ2 * y + bZ2;
        double dx = x2 - x1;
        double dz = z2 - z1;
        dLen[i] = sqrt(dx * dx + dz * dz);
    }
    // Find min dist
    int iMin = 0;
    for (i = 0; i < 800; i++) {
        if (dLen[i] < dLen[iMin]) {
            iMin = i;
        }
    }
    ptRes.y = iMin / 10.;
    ptRes.x = kX1 * ptRes.y + bX1;
    ptRes.z = kZ1 * ptRes.y + bZ1;
    return ptRes;
}

// calculate VD using FarPD front photo and NearPD front photo
bool CPDMResults::CalcVD3_far_near()
{
	bool bRes = false;
	// Check data presents
	int iMeasure = 0;
	for (iMeasure = 0; iMeasure < 2; iMeasure++) {
		if(CalcPhotoResults((iMeasure == 0)? 0 : 2) == false)
			return false;
	}
    // размеры предыдущего окна картинки (см. storyboard)
    // width = 991; height = 615;
//    double xCenterPhoto = 496;  // центральная точка окна по оси X
//    double yCenterPhoto = 308;  // центральная точка окна по оси Y
	// Constants
	const double zFarFlash = Device.zFarFlash * cos(Results[0].alfaPantosk), yFarFlash = Device.yFarFlash + (Device.zFarFlash * sin(Results[0].alfaPantosk)), xFarFlash = Device.xFarFlash; // flash position concerning the back camera
	const double zNearFlash = Device.zNearFlash * cos(Results[2].alfaPantosk), yNearFlash = Device.yNearFlash + (Device.zNearFlash * sin(Results[2].alfaPantosk)); // flash position concerning the front camera
	const double zRedCircle = Device.zRedCircle * cos(Results[2].alfaPantosk), yRedCircle = Device.zRedCircle * sin(Results[2].alfaPantosk); // red circle position concerning the front camera
//    const double CorneaRadius = 7.8; // radius of a cornea of an eye
//    const double DistBetweenCorneaEyeCenters = 4.7; // distance between centers
//	const double CorneaRadius = 7.;
//	const double DistBetweenCorneaEyeCenters = 6.1;
	// Points
	xyzPoint ptCamera; // Camera position
	xyzPoint ptFlash; // Flash position
	xyzPoint ptClientLook; // Client eyes look position on device
    xyzPoint ptC1[2], ptL2[2], ptR2[2];
	// Trek of the eyes center points
	const int nTrekPts = 800; // points in trek
	xyzPoint ptEyeTrek[2][2][nTrekPts];
	// Calculate treks
	int iEye = 0; // eye identifier
	int iy = 0; // y position identifier. Changed from 0 to 800. Or from 0 to 80 mm.
	for (iMeasure = 0; iMeasure < 2; iMeasure++) {
		int iPhoto = (iMeasure == 0)? 0 : 2;
		double Dist = Results[iPhoto].Dist; // Photo distance
		double PanAngle = Results[iPhoto].alfaPan; // Support rotation angle
		double PantoAngle = Results[iPhoto].alfaPantosk; // Support panto angle
		// Camera position
		ptCamera.x = Dist * sin(PanAngle);
		ptCamera.y = -Dist * cos(PanAngle) * cos(PantoAngle);
		ptCamera.z = Dist * sin(PantoAngle) * cos(PanAngle);
        ptC1[iMeasure] = ptCamera;
		// Flash position
		if (iMeasure == 0) {
			// FarPD photo
            ptFlash.x = ptCamera.x + xFarFlash;
			ptFlash.y = ptCamera.y + yFarFlash;
			ptFlash.z = ptCamera.z + zFarFlash;
		}
		else {
			// NearPD photo
			ptFlash.x = ptCamera.x;
			ptFlash.y = ptCamera.y + yNearFlash;
			ptFlash.z = ptCamera.z + zNearFlash;
		}
		// Client eyes look position on device
		if (iMeasure == 0) {
			// FarPD photo
			ptClientLook = ptCamera;
		}
		else {
			// NearPD photo
			ptClientLook = ptCamera;
			ptClientLook.y = ptCamera.y + yRedCircle;
			ptClientLook.z = ptCamera.z + zRedCircle;
		}
		// Flash reflection points position
		xyzPoint ptFlashReflection[2];
        
		double AO = sqrt(Dist * Dist + Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist - 2. * Photo[iPhoto].sup.acDist * Dist * cos(M_PI_2 + Results[iPhoto].alfaPan));
		double alpha1 = acos((Dist * Dist + AO * AO - Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist) / (2. * Dist * AO));
		double CP1 = Dist * tan(alpha1);
		double xCP1 = -CP1 * cos(Results[iPhoto].alfaPan);
		double yCP1 = -CP1 * sin(Results[iPhoto].alfaPan);
		double zCP1 = 0.;
        
		double BO = sqrt(Dist * Dist + Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist - 2. * Photo[iPhoto].sup.acDist * Dist * cos(M_PI_2 - Results[iPhoto].alfaPan));
		double alpha4 = acos((Dist * Dist + BO * BO - Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist) / (2. * Dist * BO));
		double CP4 = Dist * tan(alpha4);
		double xCP4 = CP4 * cos(Results[iPhoto].alfaPan);
		double yCP4 = CP4 * sin(Results[iPhoto].alfaPan);
		double zCP4 = 0.;
        
		// делаем поворот, выравниваем наклон головы "вправо - влево"
		double xlSupport, ylSupport, xrSupport, yrSupport;
		double xlEye, ylEye, xrEye, yrEye;
		double dx, dy;
		double fi = -Results[iPhoto].alfaTilt;   // ratation angle
		// поворот делаем относительно точки Right
		xrSupport = Photo[iPhoto].xRightSupport;
		yrSupport = Photo[iPhoto].yRightSupport;
		// поворачиваем Левую точку Суппорта относительно правой точки Суппорта
		dx = Photo[iPhoto].xLeftSupport - xrSupport;
		dy = Photo[iPhoto].yLeftSupport - yrSupport;
		xlSupport = dx * cos(fi) - dy * sin(fi);
		ylSupport = dx * sin(fi) + dy * cos(fi);
		xlSupport += xrSupport;
		ylSupport += yrSupport;
		// поворачиваем точку Правого зрачка относительно правой точки Суппорта
		dx = Photo[iPhoto].xRightEye - xrSupport;
		dy = Photo[iPhoto].yRightEye - yrSupport;
		xrEye = dx * cos(fi) - dy * sin(fi);
		yrEye = dx * sin(fi) + dy * cos(fi);
		xrEye += xrSupport;
		yrEye += yrSupport;
		// поворачиваем точку Левого зрачка относительно правой точки Суппорта
		dx = Photo[iPhoto].xLeftEye - xrSupport;
		dy = Photo[iPhoto].yLeftEye - yrSupport;
		xlEye = dx * cos(fi) - dy * sin(fi);
		ylEye = dx * sin(fi) + dy * cos(fi);
		xlEye += xrSupport;
		ylEye += yrSupport;
        
		double hlEye = Photo[iPhoto].convertToCM(ylEye - yrSupport, PanAngle, Dist);
		double hrEye = Photo[iPhoto].convertToCM(yrEye - yrSupport, PanAngle, Dist);
        
        double yLen = 0.;
		ptFlashReflection[0].x = xCP1 + (xCP4 - xCP1) * (xrEye - xrSupport) / (xlSupport - xrSupport);
        yLen = yCP1 + (yCP4 - yCP1) * (xrEye - xrSupport) / (xlSupport - xrSupport);
        ptFlashReflection[0].y = yLen * cos(-PantoAngle) - (-hrEye) * sin(-PantoAngle);
        ptFlashReflection[0].z = yLen * sin(-PantoAngle) + (-hrEye) * cos(-PantoAngle);
        
//		ptFlashReflection[0].y = yCP1 + (yCP4 - yCP1) * (xrEye - xrSupport) / (xlSupport - xrSupport) - hrEye * sin(PantoAngle);
//		ptFlashReflection[0].z = -hrEye * cos(PantoAngle);
        
		ptFlashReflection[1].x = xCP1 + (xCP4 - xCP1) * (xlEye - xrSupport) / (xlSupport - xrSupport);
        yLen = yCP1 + (yCP4 - yCP1) * (xlEye - xrSupport) / (xlSupport - xrSupport);
        ptFlashReflection[1].y = yLen * cos(-PantoAngle) - (-hlEye) * sin(-PantoAngle);
        ptFlashReflection[1].z = yLen * sin(-PantoAngle) + (-hlEye) * cos(-PantoAngle);
        
//		ptFlashReflection[1].y = yCP1 + (yCP4 - yCP1) * (xlEye - xrSupport) / (xlSupport - xrSupport) - hlEye * sin(PantoAngle);
//		ptFlashReflection[1].z = -hlEye * cos(PantoAngle);
        
        ptR2[iMeasure] = ptFlashReflection[0];
        ptL2[iMeasure] = ptFlashReflection[1];
        
		// Calculate trek
		for(iEye = 0; iEye < 2; iEye++) {
			for(iy = 0; iy < nTrekPts; iy++) {
				// line from camera to flash reflection
				// line coefficients: x = kX * y + bX ; z = kZ * y + bZ
				double kX, bX, kZ, bZ;
				kX = (ptFlashReflection[iEye].x - ptCamera.x) / (ptFlashReflection[iEye].y - ptCamera.y);
				kZ = (ptFlashReflection[iEye].z - ptCamera.z) / (ptFlashReflection[iEye].y - ptCamera.y);
				bX = ptCamera.x - kX * ptCamera.y;
				bZ = ptCamera.z - kZ * ptCamera.y;
				// flash reflection point on cornea surface
				xyzPoint ptFRCornea;
				ptFRCornea.y = (double)iy / 10.;
				ptFRCornea.x = kX * ptFRCornea.y + bX;
				ptFRCornea.z = kZ * ptFRCornea.y + bZ;
				// median point between ptFlash and ptCamera
				xyzPoint ptMedian;
				ptMedian.x = (ptFlash.x + ptCamera.x) / 2.;
				ptMedian.y = (ptFlash.y + ptCamera.y) / 2.;
				ptMedian.z = (ptFlash.z + ptCamera.z) / 2.;
				// calculate cornea sphere center
				xyzPoint ptCorneaCenter;
				// center is placed on line: ptMedian to ptFRCornea
				double dx, dy, dz, Len;
				dx = ptFRCornea.x - ptMedian.x;
				dy = ptFRCornea.y - ptMedian.y;
				dz = ptFRCornea.z - ptMedian.z;
				Len = sqrt(dx * dx + dy * dy + dz * dz); // piece length
				ptCorneaCenter.x = ptMedian.x + dx * (Len + CorneaRadius) / Len;
				ptCorneaCenter.y = ptMedian.y + dy * (Len + CorneaRadius) / Len;
				ptCorneaCenter.z = ptMedian.z + dz * (Len + CorneaRadius) / Len;
				// calculate eye sphere center
				xyzPoint ptEyeCenter;
				// center is placed on line: ptClientLook to ptCorneaCenter
				dx = ptCorneaCenter.x - ptClientLook.x;
				dy = ptCorneaCenter.y - ptClientLook.y;
				dz = ptCorneaCenter.z - ptClientLook.z;
				Len = sqrt(dx * dx + dy * dy + dz * dz); // piece length
				ptEyeCenter.x = ptClientLook.x + dx * (Len + DistBetweenCorneaEyeCenters) / Len;
				ptEyeCenter.y = ptClientLook.y + dy * (Len + DistBetweenCorneaEyeCenters) / Len;
				ptEyeCenter.z = ptClientLook.z + dz * (Len + DistBetweenCorneaEyeCenters) / Len;
				// save trek point
				ptEyeTrek[iMeasure][iEye][iy] = ptEyeCenter;
			}
		}
	}
	// find eyes sphere center points
	// it is located in a place of the minimum distance between treks
	int iPtMin[2];
	for(iEye = 0; iEye < 2; iEye++) {
		double LenMin = 1000.;
		iPtMin[iEye] = 0;
		for(iy = 0; iy < nTrekPts; iy++) {
			double dx, dy, dz, Len;
			dx = ptEyeTrek[0][iEye][iy].x - ptEyeTrek[1][iEye][iy].x;
			dy = ptEyeTrek[0][iEye][iy].y - ptEyeTrek[1][iEye][iy].y;
			dz = ptEyeTrek[0][iEye][iy].z - ptEyeTrek[1][iEye][iy].z;
			Len = sqrt(dx * dx + dy * dy + dz * dz); // piece length
			if(Len < LenMin) {
				LenMin = Len;
				iPtMin[iEye] = iy;
			}
		}
	}
    
	// Eyes center points position
	xyzPoint ptEyeCenter[2];
//	ptEyeCenter[0].x = (ptEyeTrek[0][0][iPtMin[0]].x + ptEyeTrek[1][0][iPtMin[0]].x) / 2.;
//	ptEyeCenter[0].y = (ptEyeTrek[0][0][iPtMin[0]].y + ptEyeTrek[1][0][iPtMin[0]].y) / 2.;
//	ptEyeCenter[0].z = (ptEyeTrek[0][0][iPtMin[0]].z + ptEyeTrek[1][0][iPtMin[0]].z) / 2.;
//	ptEyeCenter[1].x = (ptEyeTrek[0][1][iPtMin[1]].x + ptEyeTrek[1][1][iPtMin[1]].x) / 2.;
//	ptEyeCenter[1].y = (ptEyeTrek[0][1][iPtMin[1]].y + ptEyeTrek[1][1][iPtMin[1]].y) / 2.;
//	ptEyeCenter[1].z = (ptEyeTrek[0][1][iPtMin[1]].z + ptEyeTrek[1][1][iPtMin[1]].z) / 2.;
	ptEyeCenter[0] = ptEyeTrek[0][0][iPtMin[0]];
	ptEyeCenter[1] = ptEyeTrek[0][1][iPtMin[1]];

//	int iyy0 = iPtMin[0];
//	double yy0 = ptEyeTrek[0][0][iPtMin[0]].y;
//	int iyy1 = iPtMin[1];
//	double yy1 = ptEyeTrek[0][1][iPtMin[1]].y;
	double lrVD = ((ptEyeTrek[0][0][iPtMin[0]].y + ptEyeTrek[0][1][iPtMin[1]].y) / 2.) - Photo[0].sup.SupportDistance - CorneaRadius - DistBetweenCorneaEyeCenters;
	// Wrap correction
	double lrWrapCorrection = (Photo[0].sup.xFastening - (ptEyeCenter[1].x - ptEyeCenter[0].x) / 2.) * tan(Results2.WrapAngle);
	lrVD = lrVD + lrWrapCorrection;
    
    xyzPoint resLeft = CalcMinDistPoint(ptC1[0], ptL2[0], ptC1[1], ptL2[1]);
    xyzPoint resRight = CalcMinDistPoint(ptC1[0], ptR2[0], ptC1[1], ptR2[1]);
    
    double aaaaa = 0.;
    /*
     CString strFileName = "vdData.txt";
     DeleteFile(strFileName);
     CFile vdFile;
     if(vdFile.Open(strFileName, CFile::modeCreate|CFile::modeWrite)) {
     CString strTitle = "#;y;x - far;x - near;y - far;y - near;z - far;z - near\n";
     vdFile.Write(strTitle, strTitle.GetLength());
     for(iy = 0; iy < nTrekPts; iy++) {
     CString strData;
     //			strData.Format("%d;%f;%f;%f;%f;%f;%f;%f\n", iy, iy / 10., ptEyeTrek[0][0][iy].x, ptEyeTrek[1][0][iy].x, ptEyeTrek[0][0][iy].y, ptEyeTrek[1][0][iy].y, ptEyeTrek[0][0][iy].z, ptEyeTrek[1][0][iy].z);
     strData.Format("%d;%f;%f;%f;%f;%f;%f;%f\n", iy, iy / 10., ptEyeTrek[0][1][iy].x, ptEyeTrek[1][1][iy].x, ptEyeTrek[0][1][iy].y, ptEyeTrek[1][1][iy].y, ptEyeTrek[0][1][iy].z, ptEyeTrek[1][1][iy].z);
     vdFile.Write(strData, strData.GetLength());
     }
     vdFile.Close();
     }
     */
/*
    printf("Start 111111 ====================\n");
    for(iy = 0; iy < nTrekPts; iy++) {
        printf("%d;%f;%f;%f;%f;%f;%f;%f\n", iy, iy / 10., ptEyeTrek[0][1][iy].x, ptEyeTrek[1][1][iy].x, ptEyeTrek[0][1][iy].y, ptEyeTrek[1][1][iy].y, ptEyeTrek[0][1][iy].z, ptEyeTrek[1][1][iy].z);
    }
    printf("End 11111111 ====================\n");


     printf("Start 222222 ====================\n");
     for(iy = 0; iy < nTrekPts; iy++) {
     printf("%d;%f;%f;%f;%f;%f;%f;%f\n", iy, iy / 10., ptEyeTrek[0][0][iy].x, ptEyeTrek[1][0][iy].x, ptEyeTrek[0][0][iy].y, ptEyeTrek[1][0][iy].y, ptEyeTrek[0][0][iy].z, ptEyeTrek[1][0][iy].z);
     }
     printf("End 222222 ====================\n");
*/
	Results2.LeftVD = lrVD;
	Results2.RightVD = lrVD;
	Results2.ptEyeCenter[0] = ptEyeCenter[0];
	Results2.ptEyeCenter[1] = ptEyeCenter[1];
	bRes = true;
    
	return bRes;
}

double CPDMResults::GetLensBaseRadius(double angleWrap)
{
	double lensRadius = lensBaseRadius[0];
	if(angleWrap >= lensWrapAngle[0]) {
		lensRadius = lensBaseRadius[1];
		if(angleWrap >= lensWrapAngle[1]) {
			lensRadius = lensBaseRadius[2];
		}
	}
	return lensRadius;
}

// calculate Wrap using FarPD front photo and NearPD front photo
bool CPDMResults::CalcWrapAngle3_far_near()
{
	bool bRes = false;
    
	double xObjective[2] = {0., 0.};
	double yObjective[2] = {0., 0.};
	double zObjective[2] = {0., 0.};
	double xNearCP2[2] = {0., 0.}, xUpCP2[2] = {0., 0.}, xRectCP2[2] = {0., 0.};
	double yNearCP2[2] = {0., 0.}, yUpCP2[2] = {0., 0.}, yRectCP2[2] = {0., 0.};
	double zNearCP2[2] = {0., 0.}, zUpCP2[2] = {0., 0.}, zRectCP2[2] = {0., 0.};
	double xNearCP3[2] = {0., 0.}, xUpCP3[2] = {0., 0.}, xRectCP3[2] = {0., 0.};
	double yNearCP3[2] = {0., 0.}, yUpCP3[2] = {0., 0.}, yRectCP3[2] = {0., 0.};
	double zNearCP3[2] = {0., 0.}, zUpCP3[2] = {0., 0.}, zRectCP3[2] = {0., 0.};
    
	double kXrNearMarker[2] = {0., 0.}, kZrNearMarker[2] = {0., 0.}, kXlNearMarker[2] = {0., 0.}, kZlNearMarker[2] = {0., 0.};
	double bXrNearMarker[2] = {0., 0.}, bZrNearMarker[2] = {0., 0.}, bXlNearMarker[2] = {0., 0.}, bZlNearMarker[2] = {0., 0.};
	double kXrUpRect[2] = {0., 0.}, kZrUpRect[2] = {0., 0.}, kXlUpRect[2] = {0., 0.}, kZlUpRect[2] = {0., 0.};
	double bXrUpRect[2] = {0., 0.}, bZrUpRect[2] = {0., 0.}, bXlUpRect[2] = {0., 0.}, bZlUpRect[2] = {0., 0.};
	double kXrRect[2] = {0., 0.}, kZrRect[2] = {0., 0.}, kXlRect[2] = {0., 0.}, kZlRect[2] = {0., 0.};
	double bXrRect[2] = {0., 0.}, bZrRect[2] = {0., 0.}, bXlRect[2] = {0., 0.}, bZlRect[2] = {0., 0.};
	double hlNearMarker = 0., hrNearMarker = 0.;
	double hlUpRect = 0., hrUpRect = 0.;
	double hlRect = 0., hrRect = 0.;
    
	int iEye = 0;
	for (iEye = 0; iEye < 2; iEye++) {
		int iPhoto = (iEye == 0)? 0 : 2;
		if(CalcPhotoResults(iPhoto) == false)
			return false;
		double Dist = Results[iPhoto].Dist;
		double PanAngle = Results[iPhoto].alfaPan;
		double PantoAngle = Results[iPhoto].alfaPantosk;
        
		xObjective[iEye] = Dist * sin(PanAngle);
		yObjective[iEye] = -Dist * cos(PanAngle) * cos(PantoAngle);
		zObjective[iEye] = Dist * sin(PantoAngle) * cos(PanAngle);
        
		double AO = sqrt(Dist * Dist + Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist - 2. * Photo[iPhoto].sup.acDist * Dist * cos(M_PI_2 + Results[iPhoto].alfaPan));
		double alpha1 = acos((Dist * Dist + AO * AO - Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist) / (2. * Dist * AO));
		double CP1 = Dist * tan(alpha1);
		double xCP1 = -CP1 * cos(Results[iPhoto].alfaPan);
		double yCP1 = -CP1 * sin(Results[iPhoto].alfaPan);
		double zCP1 = 0.;
        
		double BO = sqrt(Dist * Dist + Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist - 2. * Photo[iPhoto].sup.acDist * Dist * cos(M_PI_2 - Results[iPhoto].alfaPan));
		double alpha4 = acos((Dist * Dist + BO * BO - Photo[iPhoto].sup.acDist * Photo[iPhoto].sup.acDist) / (2. * Dist * BO));
		double CP4 = Dist * tan(alpha4);
		double xCP4 = CP4 * cos(Results[iPhoto].alfaPan);
		double yCP4 = CP4 * sin(Results[iPhoto].alfaPan);
		double zCP4 = 0.;
        
		// делаем поворот, выравниваем наклон головы "вправо - влево"
		double xlSupport, ylSupport, xrSupport, yrSupport;
		double xlNearMarker, ylNearMarker, xrNearMarker, yrNearMarker;
		double xlUpRect, ylUpRect, xrUpRect, yrUpRect;
		double xlRect, ylRect, xrRect, yrRect;
		double dx, dy;
		double fi = -Results[iPhoto].alfaTilt;   // угол поворота
		// поворот делаем относительно точки Right
		xrSupport = Photo[iPhoto].xRightSupport;
		yrSupport = Photo[iPhoto].yRightSupport;
		// поворачиваем Левую точку Суппорта относительно правой точки Суппорта
		dx = Photo[iPhoto].xLeftSupport - xrSupport;
		dy = Photo[iPhoto].yLeftSupport - yrSupport;
		xlSupport = dx * cos(fi) - dy * sin(fi);
		ylSupport = dx * sin(fi) + dy * cos(fi);
		xlSupport += xrSupport;
		ylSupport += yrSupport;

		// поворачиваем точку Правого нижнего угла относительно правой точки Суппорта
		dx = Photo[iPhoto].xRightRect - xrSupport;
		dy = Photo[iPhoto].yRightRect + frameMarkers.yLeftMarker[iPhoto][0] - yrSupport;
		xrNearMarker = dx * cos(fi) - dy * sin(fi);
		yrNearMarker = dx * sin(fi) + dy * cos(fi);
		xrNearMarker += xrSupport;
		yrNearMarker += yrSupport;
		// поворачиваем точку Левого нижнего угла относительно правой точки Суппорта
		dx = Photo[iPhoto].xLeftRect - xrSupport;
		dy = Photo[iPhoto].yLeftRect + frameMarkers.yRightMarker[iPhoto][2] - yrSupport;
		xlNearMarker = dx * cos(fi) - dy * sin(fi);
		ylNearMarker = dx * sin(fi) + dy * cos(fi);
		xlNearMarker += xrSupport;
		ylNearMarker += yrSupport;

		// поворачиваем точку Правого верхнего угла относительно правой точки Суппорта
        dx = Photo[iPhoto].xRightUpRect - xrSupport;
        dy = Photo[iPhoto].yRightUpRect - yrSupport;
        xrUpRect = dx * cos(fi) - dy * sin(fi);
        yrUpRect = dx * sin(fi) + dy * cos(fi);
        xrUpRect += xrSupport;
        yrUpRect += yrSupport;
        // поворачиваем точку Левого верхнего угла относительно правой точки Суппорта
        dx = Photo[iPhoto].xLeftUpRect - xrSupport;
        dy = Photo[iPhoto].yLeftUpRect - yrSupport;
        xlUpRect = dx * cos(fi) - dy * sin(fi);
        ylUpRect = dx * sin(fi) + dy * cos(fi);
        xlUpRect += xrSupport;
        ylUpRect += yrSupport;

		// поворачиваем точку Правого нижнего угла относительно правой точки Суппорта
		dx = Photo[iPhoto].xRightRect - xrSupport;
		dy = Photo[iPhoto].yRightRect - yrSupport;
		xrRect = dx * cos(fi) - dy * sin(fi);
		yrRect = dx * sin(fi) + dy * cos(fi);
		xrRect += xrSupport;
		yrRect += yrSupport;
		// поворачиваем точку Левого нижнего угла относительно правой точки Суппорта
		dx = Photo[iPhoto].xLeftRect - xrSupport;
		dy = Photo[iPhoto].yLeftRect - yrSupport;
		xlRect = dx * cos(fi) - dy * sin(fi);
		ylRect = dx * sin(fi) + dy * cos(fi);
		xlRect += xrSupport;
		ylRect += yrSupport;

		hlNearMarker = Photo[iPhoto].convertToCM(ylNearMarker - yrSupport, PanAngle, Dist);
		hrNearMarker = Photo[iPhoto].convertToCM(yrNearMarker - yrSupport, PanAngle, Dist);
		hlUpRect = Photo[iPhoto].convertToCM(ylUpRect - yrSupport, PanAngle, Dist);
		hrUpRect = Photo[iPhoto].convertToCM(yrUpRect - yrSupport, PanAngle, Dist);
		hlRect = Photo[iPhoto].convertToCM(ylRect - yrSupport, PanAngle, Dist);
		hrRect = Photo[iPhoto].convertToCM(yrRect - yrSupport, PanAngle, Dist);
        
        double yLen = 0.;
		xNearCP2[iEye] = xCP1 + (xCP4 - xCP1) * (xrNearMarker - xrSupport) / (xlSupport - xrSupport);
        yLen = yCP1 + (yCP4 - yCP1) * (xrNearMarker - xrSupport) / (xlSupport - xrSupport);
		yNearCP2[iEye] = yLen * cos(-PantoAngle) - (-hrNearMarker) * sin(-PantoAngle);
		zNearCP2[iEye] = yLen * sin(-PantoAngle) + (-hrNearMarker) * cos(-PantoAngle);
        
		xNearCP3[iEye] = xCP1 + (xCP4 - xCP1) * (xlNearMarker - xrSupport) / (xlSupport - xrSupport);
        yLen = yCP1 + (yCP4 - yCP1) * (xlNearMarker - xrSupport) / (xlSupport - xrSupport);
		yNearCP3[iEye] = yLen * cos(-PantoAngle) - (-hlNearMarker) * sin(-PantoAngle);
		zNearCP3[iEye] = yLen * sin(-PantoAngle) + (-hlNearMarker) * cos(-PantoAngle);

		xUpCP2[iEye] = xCP1 + (xCP4 - xCP1) * (xrUpRect - xrSupport) / (xlSupport - xrSupport);
        yLen = yCP1 + (yCP4 - yCP1) * (xrUpRect - xrSupport) / (xlSupport - xrSupport);
		yUpCP2[iEye] = yLen * cos(-PantoAngle) - (-hrUpRect) * sin(-PantoAngle);
		zUpCP2[iEye] = yLen * sin(-PantoAngle) + (-hrUpRect) * cos(-PantoAngle);
        
		xUpCP3[iEye] = xCP1 + (xCP4 - xCP1) * (xlUpRect - xrSupport) / (xlSupport - xrSupport);
        yLen = yCP1 + (yCP4 - yCP1) * (xlUpRect - xrSupport) / (xlSupport - xrSupport);
		yUpCP3[iEye] = yLen * cos(-PantoAngle) - (-hlUpRect) * sin(-PantoAngle);
		zUpCP3[iEye] = yLen * sin(-PantoAngle) + (-hlUpRect) * cos(-PantoAngle);

		xRectCP2[iEye] = xCP1 + (xCP4 - xCP1) * (xrRect - xrSupport) / (xlSupport - xrSupport);
        yLen = yCP1 + (yCP4 - yCP1) * (xrRect - xrSupport) / (xlSupport - xrSupport);
		yRectCP2[iEye] = yLen * cos(-PantoAngle) - (-hrRect) * sin(-PantoAngle);
		zRectCP2[iEye] = yLen * sin(-PantoAngle) + (-hrRect) * cos(-PantoAngle);
        
		xRectCP3[iEye] = xCP1 + (xCP4 - xCP1) * (xlRect - xrSupport) / (xlSupport - xrSupport);
        yLen = yCP1 + (yCP4 - yCP1) * (xlRect - xrSupport) / (xlSupport - xrSupport);
		yRectCP3[iEye] = yLen * cos(-PantoAngle) - (-hlRect) * sin(-PantoAngle);
		zRectCP3[iEye] = yLen * sin(-PantoAngle) + (-hlRect) * cos(-PantoAngle);

		// коэффициенты линии
		// line coefficients: x = kX * y + bX ; z = kZ * y + bZ
		kXrNearMarker[iEye] = (xNearCP2[iEye] - xObjective[iEye]) / (yNearCP2[iEye] - yObjective[iEye]);
		kZrNearMarker[iEye] = (zNearCP2[iEye] - zObjective[iEye]) / (yNearCP2[iEye] - yObjective[iEye]);
		bXrNearMarker[iEye] = xObjective[iEye] - kXrNearMarker[iEye] * yObjective[iEye];
		bZrNearMarker[iEye] = zObjective[iEye] - kZrNearMarker[iEye] * yObjective[iEye];
        
		kXlNearMarker[iEye] = (xNearCP3[iEye] - xObjective[iEye]) / (yNearCP3[iEye] - yObjective[iEye]);
		kZlNearMarker[iEye] = (zNearCP3[iEye] - zObjective[iEye]) / (yNearCP3[iEye] - yObjective[iEye]);
		bXlNearMarker[iEye] = xObjective[iEye] - kXlNearMarker[iEye] * yObjective[iEye];
		bZlNearMarker[iEye] = zObjective[iEye] - kZlNearMarker[iEye] * yObjective[iEye];

		kXrUpRect[iEye] = (xUpCP2[iEye] - xObjective[iEye]) / (yUpCP2[iEye] - yObjective[iEye]);
		kZrUpRect[iEye] = (zUpCP2[iEye] - zObjective[iEye]) / (yUpCP2[iEye] - yObjective[iEye]);
		bXrUpRect[iEye] = xObjective[iEye] - kXrUpRect[iEye] * yObjective[iEye];
		bZrUpRect[iEye] = zObjective[iEye] - kZrUpRect[iEye] * yObjective[iEye];
        
		kXlUpRect[iEye] = (xUpCP3[iEye] - xObjective[iEye]) / (yUpCP3[iEye] - yObjective[iEye]);
		kZlUpRect[iEye] = (zUpCP3[iEye] - zObjective[iEye]) / (yUpCP3[iEye] - yObjective[iEye]);
		bXlUpRect[iEye] = xObjective[iEye] - kXlUpRect[iEye] * yObjective[iEye];
		bZlUpRect[iEye] = zObjective[iEye] - kZlUpRect[iEye] * yObjective[iEye];

		kXrRect[iEye] = (xRectCP2[iEye] - xObjective[iEye]) / (yRectCP2[iEye] - yObjective[iEye]);
		kZrRect[iEye] = (zRectCP2[iEye] - zObjective[iEye]) / (yRectCP2[iEye] - yObjective[iEye]);
		bXrRect[iEye] = xObjective[iEye] - kXrRect[iEye] * yObjective[iEye];
		bZrRect[iEye] = zObjective[iEye] - kZrRect[iEye] * yObjective[iEye];

		kXlRect[iEye] = (xRectCP3[iEye] - xObjective[iEye]) / (yRectCP3[iEye] - yObjective[iEye]);
		kZlRect[iEye] = (zRectCP3[iEye] - zObjective[iEye]) / (yRectCP3[iEye] - yObjective[iEye]);
		bXlRect[iEye] = xObjective[iEye] - kXlRect[iEye] * yObjective[iEye];
		bZlRect[iEye] = zObjective[iEye] - kZlRect[iEye] * yObjective[iEye];
	}
	// минимальное расстояние между прямыми
	double kXnear[2] = {0., 0.}, kZnear[2] = {0., 0.}, bXnear[2] = {0., 0.}, bZnear[2] = {0., 0.};
	// правый глаз
	kXnear[0] = kXrNearMarker[1] - kXrNearMarker[0];
	kZnear[0] = kZrNearMarker[1] - kZrNearMarker[0];
	bXnear[0] = bXrNearMarker[1] - bXrNearMarker[0];
	bZnear[0] = bZrNearMarker[1] - bZrNearMarker[0];
	double dyrNear = - (kXnear[0] * bXnear[0] + kZnear[0] * bZnear[0]) / (kXnear[0] * kXnear[0] + kZnear[0] * kZnear[0]);
	double dxrNear = kXrNearMarker[0] * dyrNear + bXrNearMarker[0];
	double dzrNear = kZrNearMarker[0] * dyrNear + bZrNearMarker[0];
	// левый глаз
	kXnear[1] = kXlNearMarker[1] - kXlNearMarker[0];
	kZnear[1] = kZlNearMarker[1] - kZlNearMarker[0];
	bXnear[1] = bXlNearMarker[1] - bXlNearMarker[0];
	bZnear[1] = bZlNearMarker[1] - bZlNearMarker[0];
	double dylNear = - (kXnear[1] * bXnear[1] + kZnear[1] * bZnear[1]) / (kXnear[1] * kXnear[1] + kZnear[1] * kZnear[1]);
	double dxlNear = kXlNearMarker[0] * dylNear + bXlNearMarker[0];
	double dzlNear = kZlNearMarker[0] * dylNear + bZlNearMarker[0];

	// Wrap угол по точкам у носа и крепления Суппорта
    double angle, A, B;
	double x1, y1, z1, x3, y3, z3;
	// Lenses points position
	xyzPoint ptLensCenter[2], ptLensNose[2], ptLensTemple[2], ptWrapMarker[2];
	double angleWrap = 0.;

	for(int iBase = 0; iBase < 3; iBase++) {
		double lensRadius = lensBaseRadius[iBase];
		// find outers points
		xyzPoint pt0[2], pt1[2], pt3[2], ptNose[2];
		for (iEye = 0; iEye < 2; iEye++) {
			xyzPoint pt2, ptC;
			double oa, ob, oc;
			double kX, bX, kZ, bZ;
			double kXup, bXup, kZup, bZup;
			switch(iEye) {
			case 0:
				pt1[iEye] = xyzPoint(dxrNear, dyrNear, dzrNear);
				pt2.x = -Photo[0].sup.xFastening;
				pt2.y = Photo[0].sup.yFastening;
				kXup = kXrUpRect[0];
				bXup = bXrUpRect[0];
				kZup = kZrUpRect[0];
				bZup = bZrUpRect[0];
				kX = kXrRect[0];
				bX = bXrRect[0];
				kZ = kZrRect[0];
				bZ = bZrRect[0];
				break;
			case 1:
				pt1[iEye] = xyzPoint(dxlNear, dylNear, dzlNear);
				pt2.x = Photo[0].sup.xFastening;
				pt2.y = Photo[0].sup.yFastening;
				kXup = kXlUpRect[0];
				bXup = bXlUpRect[0];
				kZup = kZlUpRect[0];
				bZup = bZlUpRect[0];
				kX = kXlRect[0];
				bX = bXlRect[0];
				kZ = kZlRect[0];
				bZ = bZlRect[0];
				break;
			}
			angle = atan((pt2.y - pt1[iEye].y) / (pt2.x - pt1[iEye].x));
			ptC.x = (pt1[iEye].x + pt2.x) / 2.;
			ptC.y = (pt1[iEye].y + pt2.y) / 2.;
			A = sqrt((ptC.x - pt1[iEye].x) * (ptC.x - pt1[iEye].x) + (ptC.y - pt1[iEye].y) * (ptC.y - pt1[iEye].y));
			B = sqrt(lensRadius * lensRadius - A * A);
			pt0[iEye].x = ptC.x - B * sin(angle);
			pt0[iEye].y = ptC.y + B * cos(angle);
			// Temple point
			// solution of the square equation
			oa = kXup * kXup + 1.;
			ob = 2. * kXup * (bXup - pt0[iEye].x) - 2. * pt0[iEye].y;
			oc = (bXup - pt0[iEye].x) * (bXup - pt0[iEye].x) + pt0[iEye].y * pt0[iEye].y - lensRadius * lensRadius;
			pt3[iEye].y = (-ob - sqrt(ob * ob - 4. * oa * oc)) / (2. * oa);
			pt3[iEye].x = kXup * pt3[iEye].y + bXup;
			pt3[iEye].z = kZup * pt3[iEye].y + bZup;
			// Nose point
			// solution of the square equation
			oa = kX * kX + 1.;
			ob = 2. * kX * (bX - pt0[iEye].x) - 2. * pt0[iEye].y;
			oc = (bX - pt0[iEye].x) * (bX - pt0[iEye].x) + pt0[iEye].y * pt0[iEye].y - lensRadius * lensRadius;
			ptNose[iEye].y = (-ob - sqrt(ob * ob - 4. * oa * oc)) / (2. * oa);
			ptNose[iEye].x = kX * pt3[iEye].y + bX;
			ptNose[iEye].z = kZ * pt3[iEye].y + bZ;
		}
		ptWrapMarker[0] = pt1[0];
		ptWrapMarker[1] = pt1[1];
		ptLensCenter[0] = pt0[0];
		ptLensCenter[1] = pt0[1];
		ptLensNose[0] = ptNose[0];
		ptLensNose[1] = ptNose[1];
		ptLensTemple[0] = pt3[0];
		ptLensTemple[1] = pt3[1];
		// calculate average points
		x1 = (ptWrapMarker[1].x - ptWrapMarker[0].x) / 2.;
		y1 = (ptWrapMarker[1].y + ptWrapMarker[0].y) / 2.;
		z1 = (ptLensNose[1].z + ptLensNose[0].z) / 2.;
		x3 = (ptLensTemple[1].x - ptLensTemple[0].x) / 2.;
		y3 = (ptLensTemple[1].y + ptLensTemple[0].y) / 2.;
		z3 = (ptLensTemple[1].z + ptLensTemple[0].z) / 2.;
		// calculate Wrap angle
		angleWrap = atan(fabs(y3 - y1) / fabs(x3 - x1));
		if (angleWrap < lensWrapAngle[iBase])
			break;
	}
	// set Wrap angle
	Results2.WrapAngleMarkers[4] = angleWrap;
	// calculate Frame width
	Results2.FrameWidth = sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1));
	Results2.FrameHeight = z3 - z1;
	// Set lenses points position
	Results2.ptWrapMarker[0] = ptWrapMarker[0];
	Results2.ptWrapMarker[1] = ptWrapMarker[1];
	Results2.ptLensCenter[0] = ptLensCenter[0];
	Results2.ptLensCenter[1] = ptLensCenter[1];
	Results2.ptLensNose[0] = ptLensNose[0];
	Results2.ptLensNose[1] = ptLensNose[1];
	Results2.ptLensTemple[0] = ptLensTemple[0];
	Results2.ptLensTemple[1] = ptLensTemple[1];

	bRes = true;
    
	return bRes;
}

// ===================================================================================
CPDMLineAngle::CPDMLineAngle()
{
    // Data results presents
    bDataResults = false;
    // alfaTilt - угол наклона головы "вправо - влево"
    alfaTilt = 0.;
    // alfaPan - угол поворота головы "вправо - влево"
    alfaPan = 0.;
    // alfaPantosk - пантоскопический угол
    alfaPantosk = 0.;
    // alfaPantoskHor - пантоскопический угол относительно отвеса
    alfaPantoskHor = 0.;
    // Dist - расстояние съемки
    Dist = 0.;
    // pdmDevice - инструмент для измерения
    pdmDevice = nSupport;

    xObjective = 0.;
    yObjective = 0.;
    xCP1 = 0.;
    yCP1 = 0.;
    xCP4 = 0.;
    yCP4 = 0.;
    xlSupport = 0.;
    ylSupport = 0.;
    xrSupport = 0.;
    yrSupport = 0.;
}

void CPDMLineAngle::SetPhotoData(PDMiPadDevice nDevice, PDMPhotoMode nPhotoMode, double xLeftSupport, double yLeftSupport, double xRightSupport, double yRightSupport, double xCenterSupport, double yCenterSupport, PDMSupportType supType, double DeviceAngle, bool bEnableCorrection)
{
    bDataResults = false;
    // Device type
    Photo.nDevice = nDevice;
	// Set Support type
	sup.SetSupportType(supType);
	double kZoomNear = 1.25 / 1.7, kZoomFar = 1.;
	switch (supType) {
		case nAcepMetalSupport:
		case nAcepPlasticSupport:
			kZoomFar = 1.;
			kZoomNear = 1.25 / 1.7;
			break;
		case nZeissSupport:
			kZoomFar = (1.9 / 2.4);
			kZoomNear = (1.0 / 1.7);
			break;
	}
	switch (nDevice) {
		case niPad:
			Photo.kFarPD = 272.0 * kZoomFar;
			// old value = 233.2
			Photo.kNearPD = 239.6 * kZoomNear;
			break;
		default:
			Photo.kFarPD = 360.0 * kZoomFar;
			Photo.kNearPD = 239.6 * kZoomNear;
			break;
	}
    // Photo mode
    Photo.nPhotoMode = nPhotoMode;
    // Support data
    double * xSupport[3], * ySupport[3];
    xSupport[0] = &xLeftSupport;
    ySupport[0] = &yLeftSupport;
    xSupport[1] = &xRightSupport;
    ySupport[1] = &yRightSupport;
    xSupport[2] = &xCenterSupport;
    ySupport[2] = &yCenterSupport;
    int i = 0, ismin = 1, ismax = 0, iscenter = 2;
    for (i = 0; i < 3; i++) {
        if (*(xSupport[i]) < *(xSupport[ismin])) {
            ismin = i;
        }
        if (*(xSupport[i]) > *(xSupport[ismax])) {
            ismax = i;
        }
    }
    for (i = 0; i < 3; i++) {
        if (i != ismin && i != ismax) {
            iscenter = i;
            break;
        }
    }
    
    Photo.xLeftSupport = *(xSupport[ismax]);
    Photo.yLeftSupport = *(ySupport[ismax]);
    Photo.xRightSupport = *(xSupport[ismin]);
    Photo.yRightSupport = *(ySupport[ismin]);
    Photo.xCenterSupport = *(xSupport[iscenter]);
    Photo.yCenterSupport = *(ySupport[iscenter]);
    if(bEnableCorrection) {
        // Correct Frame data
        Photo.xLeftRect -= 1.5;
        Photo.yLeftRect += 1.0;
        Photo.xRightRect += 1.5;
        Photo.yRightRect += 1.0;
        Photo.xLeftUpRect += 1.5;
        Photo.yLeftUpRect -= 1.5;
        Photo.xRightUpRect -= 1.5;
        Photo.yRightUpRect -= 1.5;
    }
    
    // check photo data
    Photo.checkPhotoData(); // -----------------------------> check data
    
    // Device angle
    Photo.DeviceAngle = DeviceAngle * M_PI / 180.;
    
    // Set calculation results to false
    bDataResults = false;
    
    CalcPhotoResults();
}

double CPDMLineAngle::GetPantoskHor()
{
    double Angle = 0.;
    if (CalcPhotoResults() == true) {
        Angle = alfaPantoskHor * (180. / M_PI);
    }
    return Angle;
}

double CPDMLineAngle::GetPantoskAngle()
{
    double Angle = 0.;
    if (CalcPhotoResults() == true) {
        Angle = alfaPantosk;
    }
    return Angle;
}

double CPDMLineAngle::GetPanAngle()
{
    double Angle = 0.;
    if (CalcPhotoResults() == true) {
        Angle = alfaPan;
    }
    return Angle;
}

bool CPDMLineAngle::CalcPhotoResults()
{
    bool bRes = false;

    if (bDataResults == true) {
        bRes = true;
    }
    else if (Photo.nPhotoMode == nNoData) {
        bRes = false;
    }
    else {
        // alfaTilt - угол наклона головы "вправо - влево"
        alfaTilt = Photo.CalcHeadTiltAngle();
        int i = 0;
        alfaPantosk = 0.;
        alfaPan = Photo.CalcHeadPanAngle();
        for (i = 0; i < 3; i++) {
            // alfaPan - угол поворота головы "вправо - влево"
            alfaPan = Photo.CalcHeadPanAngle2(alfaPan, alfaPantosk);
            // Dist - расстояние съемки до Суппорта
            Dist = Photo.PhotoDistance(alfaPan);
            // alfaPantosk - пантоскопический угол
            alfaPantosk = -Photo.CalcPantoskopicheskyCorner(alfaPan, Dist);
        }
        // alfaPantoskHor - пантоскопический угол относительно отвеса
        alfaPantoskHor = Photo.CalcPantoskopicheskyCornerHor(alfaPan, Dist, alfaPantosk);
        
        //-------------------------------------------------------------------------------
        xObjective = Dist * sin(alfaPan);
        yObjective = -Dist * cos(alfaPan);
        
        double AO = sqrt(Dist * Dist + Photo.sup.acDist * Photo.sup.acDist - 2. * Photo.sup.acDist * Dist * cos(M_PI_2 + alfaPan));
        double alpha1 = acos((Dist * Dist + AO * AO - Photo.sup.acDist * Photo.sup.acDist) / (2. * Dist * AO));
        double CP1 = Dist * tan(alpha1);
        xCP1 = -CP1 * cos(alfaPan);
        yCP1 = -CP1 * sin(alfaPan);
        
        double BO = sqrt(Dist * Dist + Photo.sup.acDist * Photo.sup.acDist - 2. * Photo.sup.acDist * Dist * cos(M_PI_2 - alfaPan));
        double alpha4 = acos((Dist * Dist + BO * BO - Photo.sup.acDist * Photo.sup.acDist) / (2. * Dist * BO));
        double CP4 = Dist * tan(alpha4);
        xCP4 = CP4 * cos(alfaPan);
        yCP4 = CP4 * sin(alfaPan);
        
        // do turn, level the image
        double dx, dy;
        double fi = -alfaTilt;   // угол поворота
        // turn is done concerning a point of Right of the Support
        xrSupport = Photo.xRightSupport;
        yrSupport = Photo.yRightSupport;
        // turn the Left point of the Support of rather Right point
        dx = Photo.xLeftSupport - xrSupport;
        dy = Photo.yLeftSupport - yrSupport;
        xlSupport = dx * cos(fi) - dy * sin(fi);
        ylSupport = dx * sin(fi) + dy * cos(fi);
        xlSupport += xrSupport;
        ylSupport += yrSupport;
        //-------------------------------------------------------------------------------

        bDataResults = true;
        bRes = true;
    }
    return bRes;
}

double CPDMLineAngle::CalcLineAngle(double xPoint, double yPoint)
{
    double Angle = 0.;
    if (pdmDevice == nSonar) {
        return 0.;
    }
    if (CalcPhotoResults() == true) {
        // do turn, level the image
        double xp, yp;
        double dx, dy;
        double fi = -alfaTilt;   // угол поворота
        // turn is done concerning a point of Right of the Support
        // turn the Point of rather Right point
        dx = xPoint - xrSupport;
        dy = yPoint - yrSupport;
        xp = dx * cos(fi) - dy * sin(fi);
        yp = dx * sin(fi) + dy * cos(fi);
        xp += xrSupport;
        yp += yrSupport;
        
        double xCP, yCP;
        xCP = xCP1 + (xCP4 - xCP1) * (xp - xrSupport) / (xlSupport - xrSupport);
        yCP = yCP1 + (yCP4 - yCP1) * (xp - xrSupport) / (xlSupport - xrSupport);
        
        double k = (xCP - xObjective) / (yCP - yObjective);
        double x = xObjective - yObjective * k;
        
        double dyAngle = atan((Photo.convertToCM((yPoint - yrSupport), alfaPan, Dist)) / Dist);
        
        Angle = ((alfaPantosk + dyAngle) * atan((x - xObjective) / (-yObjective)) / M_PI_2) * (180. / M_PI);
    }
    // check Angle value
    checkDoubleValue2_limits(Angle, 0., -45., 45.); // -----------------------------> check data
    return Angle;
}
void CPDMLineAngle::SetPDMDevice(PDMDevice pdm_device)
{
    pdmDevice = pdm_device;
}

// ===================================================================================
CPDMFrameMarkers::CPDMFrameMarkers()
{
    NewSession();
}
void CPDMFrameMarkers::SetMarkersPosition(int iPhoto, double xLeftMarker1, double yLeftMarker1, double xLeftMarker2, double yLeftMarker2, double xLeftMarker3, double yLeftMarker3, double xRightMarker1, double yRightMarker1, double xRightMarker2, double yRightMarker2, double xRightMarker3, double yRightMarker3)
{
    xLeftMarker[iPhoto][0] = xLeftMarker1 - 1.5;
    yLeftMarker[iPhoto][0] = yLeftMarker1 - 1.5;
    xLeftMarker[iPhoto][1] = xLeftMarker2 - 1.5;
    yLeftMarker[iPhoto][1] = yLeftMarker2 - 1.5;
    xLeftMarker[iPhoto][2] = xLeftMarker3 - 1.5;
    yLeftMarker[iPhoto][2] = yLeftMarker3 - 1.5;
    xRightMarker[iPhoto][0] = xRightMarker1 - 1.5;
    yRightMarker[iPhoto][0] = yRightMarker1 - 1.5;
    xRightMarker[iPhoto][1] = xRightMarker2 - 1.5;
    yRightMarker[iPhoto][1] = yRightMarker2 - 1.5;
    xRightMarker[iPhoto][2] = xRightMarker3 - 1.5;
    yRightMarker[iPhoto][2] = yRightMarker3 - 1.5;
}
void CPDMFrameMarkers::GetMarkersPosition(int iPhoto, double& xLeftMarker1, double& yLeftMarker1, double& xLeftMarker2, double& yLeftMarker2, double& xLeftMarker3, double& yLeftMarker3, double& xRightMarker1, double& yRightMarker1, double& xRightMarker2, double& yRightMarker2, double& xRightMarker3, double& yRightMarker3)
{
    xLeftMarker1 = xLeftMarker[iPhoto][0] + 1.5;
    yLeftMarker1 = yLeftMarker[iPhoto][0] + 1.5;
    xLeftMarker2 = xLeftMarker[iPhoto][1] + 1.5;
    yLeftMarker2 = yLeftMarker[iPhoto][1] + 1.5;
    xLeftMarker3 = xLeftMarker[iPhoto][2] + 1.5;
    yLeftMarker3 = yLeftMarker[iPhoto][2] + 1.5;
    xRightMarker1 = xRightMarker[iPhoto][0] + 1.5;
    yRightMarker1 = yRightMarker[iPhoto][0] + 1.5;
    xRightMarker2 = xRightMarker[iPhoto][1] + 1.5;
    yRightMarker2 = yRightMarker[iPhoto][1] + 1.5;
    xRightMarker3 = xRightMarker[iPhoto][2] + 1.5;
    yRightMarker3 = yRightMarker[iPhoto][2] + 1.5;
}
void CPDMFrameMarkers::SetMarkersPosition(int iPhoto, bool bLeft, int iMarker, double xPoint, double yPoint, bool bEnableCorrection)
{
    double dCor = 0.;
    if (bEnableCorrection) {
        dCor = 1.5;
    }
    if (bLeft) {
        xLeftMarker[iPhoto][iMarker] = xPoint - dCor;
        yLeftMarker[iPhoto][iMarker] = yPoint - dCor;
    }
    else {
        xRightMarker[iPhoto][iMarker] = xPoint - dCor;
        yRightMarker[iPhoto][iMarker] = yPoint - dCor;
    }
}
void CPDMFrameMarkers::GetMarkersPosition(int iPhoto, bool bLeft, int iMarker, double& xPoint, double& yPoint, bool bEnableCorrection)
{
    double dCor = 0.;
    if (bEnableCorrection) {
        dCor = 1.5;
    }
    if (bLeft) {
        xPoint = xLeftMarker[iPhoto][iMarker] + dCor;
        yPoint = yLeftMarker[iPhoto][iMarker] + dCor;
    }
    else {
        xPoint = xRightMarker[iPhoto][iMarker] + dCor;
        yPoint = yRightMarker[iPhoto][iMarker] + dCor;
    }
}
void CPDMFrameMarkers::NewSession()
{
    int iPhoto;
    for (iPhoto = 0; iPhoto < 3; iPhoto++) {
        NewPhoto(iPhoto);
    }
}
void CPDMFrameMarkers::NewPhoto(int iPhoto)
{
    const double xCenter = 496.;
    const double yCenter = 308.;

    xLeftMarker[iPhoto][0] = xCenter - 275.;
    xLeftMarker[iPhoto][1] = xCenter - 200.;
    xLeftMarker[iPhoto][2] = xCenter - 60.;
    xRightMarker[iPhoto][0] = xCenter + 60.;
    xRightMarker[iPhoto][1] = xCenter + 200.;
    xRightMarker[iPhoto][2] = xCenter + 275.;
    yLeftMarker[iPhoto][0] = yLeftMarker[iPhoto][1] = yLeftMarker[iPhoto][2] = yCenter;
    yRightMarker[iPhoto][0] = yRightMarker[iPhoto][1] = yRightMarker[iPhoto][2] = yCenter;
    
    yLeftMarker[iPhoto][0] = -80.;
    yRightMarker[iPhoto][2] = -80.;
}

// ===================================================================================
CPDMSupport::CPDMSupport()
{
	SetSupportType(nAcepPlasticSupport);
}
PDMSupportType CPDMSupport::GetSupportType()
{
	return supType;
}
void CPDMSupport::SetSupportType(PDMSupportType supType)
{
	CPDMSupport::supType = supType;

	switch(supType) {
		case nAcepMetalSupport:
			// учитываем, что крайние точки Суппорта вынесены перед оправой на ...
			SupportDistance = 14.0;
			SupportDistanceForfard = 10.8;
			// расстояние между крайними маркерами Суппорта
			abDist = 110.;
			// размер короткой стороны крюка суппорта
			SupportHookA = 5.5;
			// размер длинной стороны крюка суппорта
			SupportHookB = 35.;
			// точка касания суппортом передней плоскости оправы
			xFastening = 50.;
			yFastening = SupportDistanceForfard;
			break;
		case nAcepPlasticSupport:
			// учитываем, что крайние точки Суппорта вынесены перед оправой на ...
			SupportDistance = 8.5;
			SupportDistanceForfard = 6.0;
			// расстояние между крайними маркерами Суппорта
			abDist = 108.5;
			// размер короткой стороны крюка суппорта
			SupportHookA = 4.;
			// размер длинной стороны крюка суппорта
			SupportHookB = 35.5;
			// точка касания суппортом передней плоскости оправы
			xFastening = 47.;
			yFastening = SupportDistanceForfard;
			break;
		case nZeissSupport:
			// учитываем, что крайние точки Суппорта вынесены перед оправой на ...
			SupportDistance = 9. + 3.;
			SupportDistanceForfard = 9.;
			// расстояние между крайними маркерами Суппорта
			abDist = 168.;
			// размер короткой стороны крюка суппорта
			SupportHookA = 0;
			// размер длинной стороны крюка суппорта
			SupportHookB = 13.;
			// точка касания суппортом передней плоскости оправы
			xFastening = 32.75;//37.5;
			yFastening = SupportDistanceForfard;
			break;
	}
	// половина расстояния между крайними маркерами Суппорта
	acDist = abDist / 2.;
}
