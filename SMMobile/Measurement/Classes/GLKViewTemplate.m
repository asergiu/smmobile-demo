//
//  GLKViewTemplate.m
//  Visioner_4.0
//
//  Created by Dgut on 12.12.13.
//
//

#import "GLKViewTemplate.h"
#import "SMColorTrackingCamera.h"
#import "GLKView_SmartMirror.h"

@interface GLKViewTemplate() <SMColorTrackingCameraDelegate>
{
    GLuint videoFrameTexture;
    GLKVector2 videoFrameTextureSize;
    
    BOOL videoMode;
    BOOL playing;
    
    NSTimer * timer;
    SMColorTrackingCamera * camera;
    
    NSString * cameraPreset;
}

@end

@implementation GLKViewTemplate

-( id )initWithCoder : ( NSCoder * )aDecoder
{
    self = [ super initWithCoder : aDecoder ];
    if( self )
    {
        EAGLContext * context = [ [ EAGLContext alloc ] initWithAPI : kEAGLRenderingAPIOpenGLES2 ];
        [ EAGLContext setCurrentContext : context ];
        
        self.context = context;
        
        glGenTextures( 1, &videoFrameTexture );
        glBindTexture( GL_TEXTURE_2D, videoFrameTexture );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
        
        glEnableVertexAttribArray( GLKVertexAttribPosition );
        glEnableVertexAttribArray( GLKVertexAttribTexCoord0 );
        
        glEnable( GL_BLEND );
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
        
        [ [ NSNotificationCenter defaultCenter ] addObserver : self
                                                    selector : @selector( applicationDidBecomeActive )
                                                        name : UIApplicationDidBecomeActiveNotification
                                                      object : nil ];
        
        cameraPreset = AVCaptureSessionPreset640x480;
    }
    return self;
}

-( void )setVideoMode : ( BOOL )mode
{
    BOOL restart = playing;
    
    if( restart )
        [ self stop ];
    
    self->videoMode = mode;
    
    if( restart )
        [ self play ];
}

-( BOOL )videoMode
{
    return videoMode;
}

-( void )setCameraPreset : ( NSString * )preset
{
    BOOL restart = playing && videoMode;
    
    if( restart )
        [ self stop ];
    
    cameraPreset = preset;
    
    if( restart )
        [ self play ];
}

-( void )play
{
    [ self stop ];
    
    playing = YES;
    
    if( videoMode )
    {
        camera = [ [ SMColorTrackingCamera alloc ] initWithPreset : cameraPreset camera : AVCaptureDevicePositionBack ];
        camera.delegate = self;
        [ camera startCamera ];
        [ camera setAutofocus ];
        
        NSError *error = nil;
        
        if (IS_IOS8_AND_LATER)
        {
            if ([camera.device lockForConfiguration:&error])
                [camera.device setExposureTargetBias:0 completionHandler:nil];
        }
    }
    else
    {
        timer = [ NSTimer scheduledTimerWithTimeInterval : 1.f / 30.f
                                                  target : self
                                                selector : @selector( display )
                                                userInfo : nil
                                                 repeats : YES ];
    }
}

-( void )stop
{
    playing = NO;
    
    if( videoMode )
    {
        [ camera stopCamera ];
        camera = nil;
    }
    else
    {
        [ timer invalidate ];
        timer = nil;
    }
}

-( void )processCameraFrame : ( CVImageBufferRef )cameraFrame;
{
    if( self.superview.hidden )
        return;
    
    [ EAGLContext setCurrentContext : self.context ];
    
	CVPixelBufferLockBaseAddress( cameraFrame, 0 );
    
    videoFrameTextureSize.x = CVPixelBufferGetWidth( cameraFrame );
	videoFrameTextureSize.y = CVPixelBufferGetHeight( cameraFrame );
    
	glBindTexture( GL_TEXTURE_2D, videoFrameTexture );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, videoFrameTextureSize.x, videoFrameTextureSize.y, 0, GL_BGRA, GL_UNSIGNED_BYTE, CVPixelBufferGetBaseAddress( cameraFrame ) );
    
	[ self display ];
    
	CVPixelBufferUnlockBaseAddress( cameraFrame, 0 );
}

-( void )applicationDidBecomeActive
{
    if( playing )
    {
        [ self stop ];
        [ self play ];
    }
}

-( GLuint )videoTexture
{
    return videoFrameTexture;
}

-( GLKVector2 )videoTextureSize
{
    return videoFrameTextureSize;
}

#pragma mark common stuff

-( GLKTextureInfo * )getTexture : ( NSString * )name
{
    NSString * bundlePath = [ [ NSBundle mainBundle ] pathForResource : name ofType : NULL ];
    NSDictionary * options = @{
                               GLKTextureLoaderOriginBottomLeft : [ NSNumber numberWithBool : YES ]
                               };
    
    return [ GLKTextureLoader textureWithContentsOfFile : bundlePath options : options error : NULL ];
}

-( BOOL )compileShader : ( GLuint * )shader type : ( GLenum )type file : ( NSString * )file
{
    GLint status;
    const GLchar * source;
    
    source = ( GLchar * )[ [ NSString stringWithContentsOfFile : file encoding : NSUTF8StringEncoding error : nil ] UTF8String ];
    if( !source )
    {
        NSLog( @"Failed to load vertex shader" );
        return NO;
    }
    
    *shader = glCreateShader( type );
    glShaderSource( *shader, 1, &source, NULL );
    glCompileShader( *shader );
    
#ifdef DEBUG
    GLint logLength;
    glGetShaderiv( *shader, GL_INFO_LOG_LENGTH, &logLength );
    if( logLength > 0 )
    {
        GLchar * log = ( GLchar * )malloc( logLength );
        glGetShaderInfoLog( *shader, logLength, &logLength, log );
        NSLog( @"Shader compile log:\n%s", log );
        free( log );
    }
#endif
    
    glGetShaderiv( *shader, GL_COMPILE_STATUS, &status );
    if( !status )
    {
        glDeleteShader( *shader );
        return NO;
    }
    
    return YES;
}

-( BOOL )linkProgram : ( GLuint )prog
{
    GLint status;
    glLinkProgram( prog );
    
#ifdef DEBUG
    GLint logLength;
    glGetProgramiv( prog, GL_INFO_LOG_LENGTH, &logLength );
    if( logLength > 0 )
    {
        GLchar * log = ( GLchar * )malloc( logLength );
        glGetProgramInfoLog( prog, logLength, &logLength, log );
        NSLog( @"Program link log:\n%s", log );
        free( log );
    }
#endif
    
    glGetProgramiv( prog, GL_LINK_STATUS, &status );
    if( !status )
        return NO;
    
    return YES;
}

-( GLuint )getProgram : ( NSString * )vertex : ( NSString * )fragment
{
    GLuint vs, fs;
    NSString * bundlePath;
    
    GLuint prog = glCreateProgram();
    
    bundlePath = [ [ NSBundle mainBundle ] pathForResource : vertex ofType : @"vsh" ];
    if( ![ self compileShader : &vs type:GL_VERTEX_SHADER file : bundlePath ] )
    {
        NSLog( @"Failed to compile vertex shader" );
        return 0;
    }
    
    bundlePath = [ [ NSBundle mainBundle ] pathForResource : fragment ofType : @"fsh" ];
    if( ![ self compileShader : &fs type : GL_FRAGMENT_SHADER file : bundlePath ] )
    {
        NSLog( @"Failed to compile fragment shader" );
        return 0;
    }
    
    glAttachShader( prog, vs );
    glAttachShader( prog, fs );
    
    glBindAttribLocation( prog, GLKVertexAttribPosition, "position" );
    glBindAttribLocation( prog, GLKVertexAttribTexCoord0, "texCoord0" );
    //glBindAttribLocation( prog, GLKVertexAttribTexCoord1, "texCoord1" );
    
    if( ![ self linkProgram : prog ] )
    {
        NSLog( @"Failed to link program: %d", prog );
        
        glDeleteShader( vs );
        glDeleteShader( fs );
        glDeleteProgram( prog );
        
        return 0;
    }
    
    glDetachShader( prog, vs );
    glDeleteShader( vs );
    
    glDetachShader( prog, fs );
    glDeleteShader( fs );
    
    return prog;
}

-( void )drawRectangle : ( CGRect )rect
{
    static float verts[ 8 ];
	static float tverts[ 8 ] = { 0.f, 0.f, 1.f, 0.f, 1.f, 1.f, 0.f, 1.f };
    
    glVertexAttribPointer( GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, 0, verts );
    glVertexAttribPointer( GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, 0, tverts );
    
	verts[ 0 ] = rect.origin.x;
	verts[ 1 ] = rect.origin.y;
    
	verts[ 2 ] = rect.origin.x + rect.size.width;
	verts[ 3 ] = rect.origin.y;
    
	verts[ 4 ] = rect.origin.x + rect.size.width;
	verts[ 5 ] = rect.origin.y + rect.size.height;
    
	verts[ 6 ] = rect.origin.x;
	verts[ 7 ] = rect.origin.y + rect.size.height;
    
	glDrawArrays( GL_TRIANGLE_FAN, 0, 4 );
}

-( void )dealloc
{
    [ EAGLContext setCurrentContext : self.context ];
    
    for( GLuint i = 1; i <= 64; i++ )
        glDeleteTextures( 1, &i );
    for( GLuint i = 1; i <= 64; i++ )
        glDeleteProgram( i );
    
    [ EAGLContext setCurrentContext : nil ];
    self.context = nil;
    
    [ self stop ];
    
    [ [ NSNotificationCenter defaultCenter ] removeObserver : self ];
}

@end
