#import "PDMEyeArea.h"
#import "PDMArrow.h"
#include <math.h>
#import "PDMDataManager.h"
#import "PDConfigManager.h"


const double lineLen = 150.0;
const double lineLenRect = 250.0;
const double lineOffset = 20.0;
const double arrowLineOffset = lineOffset - 6.0;
const double arrowLineOffset2 = lineOffset + 6.0;

@implementation PDMEyeArea
@synthesize leftEye;
@synthesize rightEye;
@synthesize leftRect;
@synthesize rightRect;
@synthesize leftPDString;
@synthesize rightPDString;
@synthesize leftHeightString;
@synthesize rightHeightString;
@synthesize bridgeString;

@synthesize leftPdLength;
@synthesize leftPdPosition;
@synthesize leftHeightLength;
@synthesize leftHeightPosition;
@synthesize rightPdLength;
@synthesize rightPdPosition;
@synthesize rightHeightLength;
@synthesize rightHeightPosition;
@synthesize bridgeLength;
@synthesize bridgePosition;

@synthesize currentZoomScale;
@synthesize scrollOffsetX;
@synthesize scrollOffsetY;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        currentZoomScale = 1.0;
        scrollOffsetX = 0.0;
        scrollOffsetY = 0.0;
    }
    return self;
}

- (void)refresh {
    BOOL needToCreate = self.subviews.count  == 0;
    
    double yCenterEye = ((leftEye.y + rightEye.y) / 2.);
    double dyLeftEye = leftEye.y - yCenterEye;
    double dyRightEye = rightEye.y - yCenterEye;
    double dyLeftRect = (leftRect.y - lineLenRect) - (leftEye.y - dyLeftEye - lineLen + 40);
    double dyRightRect = (rightRect.y - lineLenRect) - (rightEye.y - dyRightEye - lineLen + 40);
    double dxLeftRect = (leftRect.x - lineOffset + lineLenRect) - (leftEye.x - lineOffset + lineLen);
    double dxRightRect = (rightRect.x - lineLenRect + lineOffset) - (rightEye.x - lineLen + lineOffset);
    
    // left
    CGRect leftHorizFrame = CGRectMake((leftEye.x - lineOffset) * currentZoomScale - scrollOffsetX, 
                                       leftEye.y * currentZoomScale - scrollOffsetY, 
                                       lineLen * currentZoomScale,
                                       ARROW_HEIGHT);
    CGRect leftVertFrame = CGRectMake(leftEye.x * currentZoomScale - scrollOffsetX, 
                                      (leftEye.y - dyLeftEye - lineLen + lineOffset) * currentZoomScale - scrollOffsetY,
                                      ARROW_HEIGHT, 
                                      (lineLen + dyLeftEye) * currentZoomScale);
    CGRect leftHorizRectFrame = CGRectMake((leftRect.x - lineOffset)  * currentZoomScale - scrollOffsetX,
                                           leftRect.y  * currentZoomScale - scrollOffsetY, 
                                           (lineLenRect - dxLeftRect) * currentZoomScale,
                                           ARROW_HEIGHT);
    CGRect leftVertRectFrame = CGRectMake(leftRect.x  * currentZoomScale - scrollOffsetX - ARROW_HEIGHT, 
                                          (leftRect.y - lineLenRect - dyLeftRect + lineOffset)  * currentZoomScale - scrollOffsetY,
                                          ARROW_HEIGHT, 
                                          (lineLenRect + dyLeftRect) * currentZoomScale);
    // right
    CGRect rightHorizFrame = CGRectMake((rightEye.x - lineLen + lineOffset) * currentZoomScale - scrollOffsetX, 
                                        rightEye.y * currentZoomScale - scrollOffsetY, 
                                        lineLen * currentZoomScale, 
                                        ARROW_HEIGHT);
    CGRect rightVertFrame = CGRectMake(rightEye.x * currentZoomScale - scrollOffsetX,
                                       (rightEye.y - dyRightEye - lineLen + lineOffset) * currentZoomScale - scrollOffsetY,
                                       ARROW_HEIGHT, 
                                       (lineLen + dyRightEye) * currentZoomScale);
    CGRect rightHorizRectFrame = CGRectMake((rightRect.x - lineLenRect + lineOffset - dxRightRect)  * currentZoomScale - scrollOffsetX,
                                            rightRect.y  * currentZoomScale - scrollOffsetY, 
                                            (lineLenRect + dxRightRect) * currentZoomScale,
                                            ARROW_HEIGHT);
    CGRect rightVertRectFrame = CGRectMake(rightRect.x  * currentZoomScale - scrollOffsetX, 
                                           (rightRect.y - lineLenRect - dyRightRect + lineOffset) * currentZoomScale - scrollOffsetY,
                                           ARROW_HEIGHT, 
                                           (lineLenRect + dyRightRect) * currentZoomScale);
    // center
    CGRect centerVertFrame = CGRectMake((rightRect.x + (leftRect.x - rightRect.x) / 2) * currentZoomScale - scrollOffsetX, 
                                        leftVertFrame.origin.y,
                                        ARROW_HEIGHT, 
                                        (lineLen / 2) * currentZoomScale);

    
    if (needToCreate) {
        // left eye - the horizontal line
        leftHoriz = [[UIView alloc] initWithFrame:leftHorizFrame];
        [self addSubview:leftHoriz];
        // left eye - the vertical line
        leftVert = [[UIView alloc] initWithFrame:leftVertFrame];
        [self addSubview:leftVert];
        // left rectangle - the horizontal line
        leftHorizRect = [[UIView alloc] initWithFrame:leftHorizRectFrame];
        [self addSubview:leftHorizRect];
        // left rectangle - the vertical line
        leftVertRect = [[UIView alloc] initWithFrame:leftVertRectFrame];
        [self addSubview:leftVertRect];
        // right eye - the horizontal line
        rightHoriz = [[UIView alloc] initWithFrame:rightHorizFrame];
        [self addSubview:rightHoriz];
        // right eye - the vertical line
        rightVert = [[UIView alloc] initWithFrame:rightVertFrame];
        [self addSubview:rightVert];
        // right rectangle - the horizontal line
        rightHorizRect = [[UIView alloc] initWithFrame:rightHorizRectFrame];
        [self addSubview:rightHorizRect];
        // right rectangle - the vertical line
        rightVertRect = [[UIView alloc] initWithFrame:rightVertRectFrame];
        [self addSubview:rightVertRect];
        // average vertical line
        centerVert = [[UIView alloc] initWithFrame:centerVertFrame];
        [self addSubview:centerVert];
        
        // left PD
        leftPD = [[PDMArrow alloc] initWithType:PDMArrowTypeHorizontal];
        [self addSubview:leftPD];
        
        leftPDText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [leftPDText setBackgroundColor:[UIColor clearColor]];
        [leftPDText setTextAlignment:NSTextAlignmentCenter];
        [leftPDText setTextColor:[UIColor whiteColor]];
        [leftPDText setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:20.f]];
        [self addSubview:leftPDText];
        
        // right PD
        rightPD = [[PDMArrow alloc] initWithType:PDMArrowTypeHorizontal];
        [self addSubview:rightPD];
        
        rightPDText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [rightPDText setBackgroundColor:[UIColor clearColor]];
        [rightPDText setTextAlignment:NSTextAlignmentCenter];
        [rightPDText setTextColor:[UIColor whiteColor]];
        [rightPDText setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:20.f]];
        [self addSubview:rightPDText];
        
        // left Height
        leftHeight = [[PDMArrow alloc] initWithType:PDMArrowTypeVertical];
        [self addSubview:leftHeight];
        
        leftHeightText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [leftHeightText setBackgroundColor:[UIColor clearColor]];
        [leftHeightText setTextAlignment:NSTextAlignmentCenter];
        [leftHeightText setTextColor:[UIColor whiteColor]];
        [leftHeightText setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:20.f]];
        [self addSubview:leftHeightText];
        
        // right Height
        rightHeight = [[PDMArrow alloc] initWithType:PDMArrowTypeVertical];
        [self addSubview:rightHeight];
        
        rightHeightText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [rightHeightText setBackgroundColor:[UIColor clearColor]];
        [rightHeightText setTextAlignment:NSTextAlignmentCenter];
        [rightHeightText setTextColor:[UIColor whiteColor]];
        [rightHeightText setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:20.f]];
        [self addSubview:rightHeightText];
        
        // Bridge
        bridge = [[PDMArrow alloc] initWithType:PDMArrowTypeHorizontal];
        [self addSubview:bridge];
        
        bridgeText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [bridgeText setBackgroundColor:[UIColor clearColor]];
        [bridgeText setTextAlignment:NSTextAlignmentCenter];
        [bridgeText setTextColor:[UIColor whiteColor]];
        [bridgeText setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:20.f]];
        [self addSubview:bridgeText];
        
        [leftHoriz setBackgroundColor:[UIColor whiteColor]];
        [leftVert setBackgroundColor:[UIColor whiteColor]];
        [leftHorizRect setBackgroundColor:[UIColor whiteColor]];
        [leftVertRect setBackgroundColor:[UIColor whiteColor]];
        [rightHoriz setBackgroundColor:[UIColor whiteColor]];
        [rightVert setBackgroundColor:[UIColor whiteColor]];
        [rightHorizRect setBackgroundColor:[UIColor whiteColor]];
        [rightVertRect setBackgroundColor:[UIColor whiteColor]];
        [centerVert setBackgroundColor:[UIColor whiteColor]];
    } else {
        [leftHoriz setFrame:leftHorizFrame];
        [leftVert setFrame:leftVertFrame];
        [leftHorizRect setFrame:leftHorizRectFrame];
        [leftVertRect setFrame:leftVertRectFrame];
        [rightHoriz setFrame:rightHorizFrame];
        [rightVert setFrame:rightVertFrame];
        [rightHorizRect setFrame:rightHorizRectFrame];
        [rightVertRect setFrame:rightVertRectFrame];
        [centerVert setFrame:centerVertFrame];
    }
    
    leftPdPosition = CGPointMake(centerVert.frame.origin.x, centerVert.frame.origin.y + arrowLineOffset * currentZoomScale - leftPD.frame.size.height / 2);
    leftPdLength = leftVert.frame.origin.x - centerVert.frame.origin.x;
    [leftPD drawWithPoint:leftPdPosition andLen:leftPdLength];
    
    [leftPDText setText:[NSString stringWithFormat:@"%.1f", ([PD_MANAGER inExpertMode] ? [leftPDString floatValue] : [PD_MANAGER roundToHalf:[leftPDString floatValue]])]];
    [leftPDText setCenter:CGPointMake(leftPD.frame.origin.x + ((leftVert.frame.origin.x - centerVert.frame.origin.x) / 2), leftPD.frame.origin.y - 10)];
    
    rightPdPosition = CGPointMake(centerVert.frame.origin.x - (centerVert.frame.origin.x - rightVert.frame.origin.x), centerVert.frame.origin.y + arrowLineOffset * currentZoomScale);
    rightPdLength = centerVert.frame.origin.x - rightVert.frame.origin.x;
    [rightPD drawWithPoint:rightPdPosition andLen:rightPdLength];
    
    [rightPDText setText:[NSString stringWithFormat:@"%.1f", ([PD_MANAGER inExpertMode] ? [rightPDString floatValue] : [PD_MANAGER roundToHalf:[rightPDString floatValue]])]];
    [rightPDText setCenter:CGPointMake(rightPD.frame.origin.x + ((centerVert.frame.origin.x - rightVert.frame.origin.x) / 2), rightPD.frame.origin.y - 10)];

    leftHeightPosition = CGPointMake(leftHoriz.frame.origin.x + leftHoriz.frame.size.width - arrowLineOffset2 * currentZoomScale, leftHoriz.frame.origin.y);
    leftHeightLength = leftHorizRect.frame.origin.y - leftHoriz.frame.origin.y;
    [leftHeight drawWithPoint:leftHeightPosition andLen:leftHeightLength];
    
    [leftHeightText setText:[NSString stringWithFormat:@"%.1f", ([PD_MANAGER inExpertMode] ? [leftHeightString floatValue] : [PD_MANAGER roundToHalf:[leftHeightString floatValue]])]];
    [leftHeightText setCenter:CGPointMake(leftHeight.frame.origin.x + 30, leftHeight.frame.origin.y + ((leftHorizRect.frame.origin.y - leftHoriz.frame.origin.y) / 2))];
  
    rightHeightPosition = CGPointMake(rightHoriz.frame.origin.x + arrowLineOffset * currentZoomScale, rightHoriz.frame.origin.y);
    rightHeightLength = rightHorizRect.frame.origin.y - rightHoriz.frame.origin.y;
    [rightHeight drawWithPoint:rightHeightPosition andLen:rightHeightLength];
    
    [rightHeightText setText:[NSString stringWithFormat:@"%.1f", ([PD_MANAGER inExpertMode] ? [rightHeightString floatValue] : [PD_MANAGER roundToHalf:[rightHeightString floatValue]])]];
    [rightHeightText setCenter:CGPointMake(rightHeight.frame.origin.x - 30, rightHeight.frame.origin.y + ((rightHorizRect.frame.origin.y - rightHoriz.frame.origin.y) / 2))];

    double yBridgePos = fmin(leftHorizRect.frame.origin.y, rightHorizRect.frame.origin.y) - lineOffset * currentZoomScale;
    bridgePosition = CGPointMake(rightVertRect.frame.origin.x, yBridgePos);
    bridgeLength = leftVertRect.frame.origin.x - rightVertRect.frame.origin.x;
    [bridge drawWithPoint:bridgePosition andLen:bridgeLength];
    
    [bridgeText setText:[NSString stringWithFormat:@"%.1f", ([PD_MANAGER inExpertMode] ? [bridgeString floatValue] : [PD_MANAGER roundToHalf:[bridgeString floatValue]])]];
    [bridgeText setCenter:CGPointMake(bridge.frame.origin.x + ((leftVertRect.frame.origin.x - rightVertRect.frame.origin.x) / 2), bridge.frame.origin.y - 10)];
}

@end
