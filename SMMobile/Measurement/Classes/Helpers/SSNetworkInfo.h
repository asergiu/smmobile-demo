#import <arpa/inet.h>
#include <errno.h>
#include <net/if.h>
#include <net/if_dl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#import <sys/sysctl.h>
#import <sys/utsname.h>
#import <sys/times.h>
#import <sys/stat.h>
#import <sys/_structs.h>
#import <asl.h>
#import <ifaddrs.h>
#import <mach/mach.h>
#import <mach/mach_host.h>
#include <stdio.h>
#import <TargetConditionals.h>

@interface SSNetworkInfo : NSObject

// Network Information

// Get Current IP Address
+ (NSString *)CurrentIPAddress;

// Get Current MAC Address
+ (NSString *)CurrentMACAddress;

// Get Cell IP Address
+ (NSString *)CellIPAddress;

// Get Cell MAC Address
+ (NSString *)CellMACAddress;

// Get Cell Netmask Address
+ (NSString *)CellNetmaskAddress;

// Get Cell Broadcast Address
+ (NSString *)CellBroadcastAddress;

// Get WiFi IP Address
+ (NSString *)WiFiIPAddress;

// Get WiFi MAC Address
+ (NSString *)WiFiMACAddress;

// Get WiFi Netmask Address
+ (NSString *)WiFiNetmaskAddress;

// Get WiFi Broadcast Address
+ (NSString *)WiFiBroadcastAddress;

// Connected to WiFi?
+ (BOOL)ConnectedToWiFi;

// Connected to Cellular Network?
+ (BOOL)ConnectedToCellNetwork;

@end
