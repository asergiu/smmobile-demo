//
//  FSNetwork.h
//  frameselection
//
//  Created by Politov Alexey on 05.05.12.
//

#import <Foundation/Foundation.h>

@interface FSNetwork : NSObject

+(NSString *)getMacAddress;

@end
