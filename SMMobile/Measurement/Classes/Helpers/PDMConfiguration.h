#import <Foundation/Foundation.h>

// ENABLE OR DISABLE SHOW DEVICE ANGLE
// 0 - hidden
// 1 - showed
#define SHOW_DEVICE_ANGLE 0

// ENABLE OR DISABLE TEST CONTENT
// 0 - hidden
// 1 - showed
#define ENABLE_LIVE_SWITCHER 0
#define ENABLE_TEST_RESULT_SIZES 0
#define MAXIMUM_ALOWED_ANGLE 5

#define FAR_PD_DELAY_COUNT 1
#define NEAR_PD_DELAY_COUNT 3

// Content keys
// FAR
#define FAR_CENTER_SUPPORT_X @"FarCenterSupportX"
#define FAR_CENTER_SUPPORT_Y @"FarCenterSupportY"
#define FAR_LEFT_EYE_X @"FarLeftEyeX"
#define FAR_LEFT_EYE_Y @"FarLeftEyeY"
#define FAR_LEFT_RECT_HEIGHT @"FarLeftRectHeight"
#define FAR_LEFT_RECT_WIDTH @"FarLeftRectWidth"
#define FAR_LEFT_RECT_X @"FarLeftRectX"
#define FAR_LEFT_RECT_Y @"FarLeftRectY"
#define FAR_LEFT_SUPPORT_X @"FarLeftSupportX"
#define FAR_LEFT_SUPPORT_Y @"FarLeftSupportY"
#define FAR_RIGHT_EYE_X @"FarRightEyeX"
#define FAR_RIGHT_EYE_Y @"FarRightEyeY"
#define FAR_RIGHT_RECT_HEIGHT @"FarRightRectHeight"
#define FAR_RIGHT_RECT_WIDTH @"FarRightRectWidht"
#define FAR_RIGHT_RECT_X @"FarRightRectX"
#define FAR_RIGHT_RECT_Y @"FarRightRectY"
#define FAR_RIGHT_SUPPORT_X @"FarRightSupportX"
#define FAR_RIGHT_SUPPORT_Y @"FarRightSupportY"
#define FAR_SNAPPED_ANGLE @"FarSnappedAngle"
#define FAR_SNAPPED_ANGLE_Z @"FarSnappedAngleZ"

// VD & Wrap
#define VD_CENTER_SUPPORT_X @"VDCenterSupportX"
#define VD_CENTER_SUPPORT_Y @"VDrCenterSupportY"
#define VD_LEFT_EYE_X @"VDLeftEyeX"
#define VD_LEFT_EYE_Y @"VDLeftEyeY"
#define VD_LEFT_RECT_HEIGHT @"VDLeftRectHeight"
#define VD_LEFT_RECT_WIDTH @"VDLeftRectWidth"
#define VD_LEFT_RECT_X @"VDLeftRectX"
#define VD_LEFT_RECT_Y @"VDLeftRectY"
#define VD_LEFT_SUPPORT_X @"VDLeftSupportX"
#define VD_LEFT_SUPPORT_Y @"VDLeftSupportY"
#define VD_RIGHT_EYE_X @"VDRightEyeX"
#define VD_RIGHT_EYE_Y @"VDRightEyeY"
#define VD_RIGHT_RECT_HEIGHT @"VDRightRectHeight"
#define VD_RIGHT_RECT_WIDTH @"VDRightRectWidht"
#define VD_RIGHT_RECT_X @"VDRightRectX"
#define VD_RIGHT_RECT_Y @"VDRightRectY"
#define VD_RIGHT_SUPPORT_X @"VDRightSupportX"
#define VD_RIGHT_SUPPORT_Y @"VDRightSupportY"
#define VD_SNAPPED_ANGLE @"VDSnappedAngle"
#define VD_SNAPPED_ANGLE_Z @"VDSnappedAngleZ"

// VD TOP CORNERS POINTS
#define VD_RIGHT_SCREEN_POINT_X @"VdWrapRightScreenPointX"
#define VD_RIGHT_SCREEN_POINT_Y @"VdWrapRightScreenPointY"
#define VD_LEFT_SCREEN_POINT_X @"VdWrapLeftScreenPointX"
#define VD_LEFT_SCREEN_POINT_Y @"VdWrapLeftScreenPointY"

// FAR TOP CORNERS POINTS
#define FAR_RIGHT_SCREEN_POINT_X @"FarRightScreenPointX"
#define FAR_RIGHT_SCREEN_POINT_Y @"FarRightScreenPointY"
#define FAR_LEFT_SCREEN_POINT_X @"FarLeftScreenPointX"
#define FAR_LEFT_SCREEN_POINT_Y @"FarLeftScreenPointY"

// NEAR TOP CORNERS POINTS
#define NEAR_RIGHT_SCREEN_POINT_X @"NearRightScreenPointX"
#define NEAR_RIGHT_SCREEN_POINT_Y @"NearRightScreenPointY"
#define NEAR_LEFT_SCREEN_POINT_X @"NearLeftScreenPointX"
#define NEAR_LEFT_SCREEN_POINT_Y @"NearLeftScreenPointY"

//NEAR
#define NEAR_CENTER_SUPPORT_X @"NearCenterSupportX"
#define NEAR_CENTER_SUPPORT_Y @"NearCenterSupportY"
#define NEAR_DIST_TO_LEFT_X @"NearDistToLeftX"
#define NEAR_DIST_TO_LEFT_Y @"NearDistToLeftY"
#define NEAR_DIST_TO_RIGHT_X @"NearDistToRightX"
#define NEAR_DIST_TO_RIGHT_Y @"NearDistToRightY"
#define NEAR_LEFT_EYE_X @"NearLeftEyeX"
#define NEAR_LEFT_EYE_Y @"NearLeftEyeY"
#define NEAR_LEFT_RECT_HEIGHT @"NearLeftRectHeight"
#define NEAR_LEFT_RECT_WIDTH @"NearLeftRectWidth"
#define NEAR_LEFT_RECT_X @"NearLeftRectX"
#define NEAR_LEFT_RECT_Y @"NearLeftRectY"
#define NEAR_LEFT_SUPPORT_X @"NearLeftSupportX"
#define NEAR_LEFT_SUPPORT_Y @"NearLeftSupportY"
#define NEAR_RIGHT_EYE_X @"NearRightEyeX"
#define NEAR_RIGHT_EYE_Y @"NearRightEyeY"
#define NEAR_RIGHT_RECT_HEIGHT @"NearRightRectHeight"
#define NEAR_RIGHT_RECT_WIDTH @"NearRightRectWidth"
#define NEAR_RIGHT_RECT_X @"NearRightRectX"
#define NEAR_RIGHT_RECT_Y @"NearRightRectY"
#define NEAR_RIGHT_SUPPORT_X @"NearRightSupportX"
#define NEAR_RIGHT_SUPPORT_Y @"NearRightSupportY"
#define NEAR_SNAPPED_ANGLE @"NearSnappedAngle"
#define NEAR_SNAPPED_ANGLE_Z @"NearSnappedAngleZ"

// XML KEYS
#define FAR_PD_XML_KEY @"FarPD"
#define NEAR_PD_XML_KEY @"NearPD"

#define READING_DIST_XML_KEY @"ReadingDist"

#define INITIALIZED_XML_KEY @"Initialized"
#define LEFT_PD_XML_KEY @"LeftPD"
#define RIGHT_PD_XML_KEY @"RightPD"
#define LEFT_HEIGHT_XML_KEY @"LeftHeight"
#define RIGHT_HEIGHT_XML_KEY @"RightHeight"
#define BRIDGE_XML_KEY @"Bridge"
#define PROGRESSIVE_TYPE_XML_KEY @"ProgressiveType"

#define RIGHT_SUPPORT_MARKER_XML_KEY @"Right support marker"
#define CENTRAL_SUPPORT_MARKER_XML_KEY @"Central support marker"
#define LEFT_SUPPORT_MARKER_XML_KEY @"Left support marker"
#define RIGHT_CORNEAL_REFLECTION_XML_KEY @"Right corneal reflection"
#define LEFT_CORNEAL_REFLECTION_XML_KEY @"Left corneal reflection"
#define RIGHT_FRAME_BRIDGE_CORNER_XML_KEY @"Right frame bridge corner"
#define LEFT_FRAME_BRIDGE_CORNER_XML_KEY @"Left frame bridge corner"
#define FRAME_WRAP_ANGLE_XML_KEY @"Frame Wrap Angle"
#define IPAD_ANGLE_XML_KEY @"iPad angle"
#define DISTANCE_TO_IPAD_XML_KEY @"Distance to iPad"

#define FRAME_WIDTH_XML_KEY @"FrameWidth"
#define FRAME_HEIGHT_XML_KEY @"FrameHeight"
#define VERTEX_DISTANCE_XML_KEY @"VertexDistance"
#define WRAP_ANGLE_XML_KEY @"WrapAngle"
#define PANTO_XML_KEY @"Panto"
