
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PDMConfiguration.h"
#import "SMDetectShareHelper.h"

#define PDM_DATA [PDMDataManager sharedManager]

typedef NS_ENUM(NSInteger, PDMCurrentSessionType) {
    PDMSessionFar,
    PDMSessionProgressive,
    PDMSessionNear
};

typedef NS_ENUM(NSInteger, PDMCurrentPhotoStep) {
    PDMPhotoFar,
    PDMPhotoNear
};

typedef NS_ENUM(NSInteger, PDMCurrentIpadFlashType) {
    PDMFlashNormal,
    PDMFlashHigh
};

@interface PDMDataManager : NSObject

@property (strong, nonatomic) SMDetectShareHelper *analyticsHelper;

@property (strong, nonatomic) NSMutableDictionary *storage;
@property (strong, nonatomic) NSArray *farPdKeys;
@property (strong, nonatomic) NSArray *vdKeys;
@property (strong, nonatomic) NSArray *nearPdKeys;
@property (strong, nonatomic) NSArray *firstFarKeys;
@property (strong, nonatomic) NSArray *secondFarKeys;

@property (strong, nonatomic) UIImage *farOriginPhoto;
@property (strong, nonatomic) UIImage *nearOriginPhoto;

@property (strong, nonatomic) UIImage *farPDImage;
@property (strong, nonatomic) UIImage *nearPDImage;
@property (strong, nonatomic) UIImage *vdImage;
@property (strong, nonatomic) UIImage *farPDImageRotated;
@property (strong, nonatomic) UIImage *nearPDImageRotated;
@property (strong, nonatomic) UIImage *nearPDImageOriginal;
@property (strong, nonatomic) UIImage *vdImageRotated;

@property (nonatomic, assign) BOOL appModeVD;

@property (strong, nonatomic) NSNumber *topWrapValue;

@property (nonatomic, assign) PDMCurrentSessionType currentSessionType;
@property (nonatomic, assign) PDMCurrentPhotoStep currentPhotoStep;

+ (PDMDataManager *)sharedManager;

- (void)removeFarPDPositions;
- (void)removeNearPDPositions;
- (void)removeVDPositions;
- (void)removeFirstPDPositions;
- (void)removeSecondPDPositions;

- (void)clearImages;
- (void)clearStorage;

- (BOOL)hasSnapImage;
- (void)setRestoreImage:(UIImage *)image;
- (UIImage *)getRestoreImage;
- (void)clearRestoreImage;

- (void)setFarMode:(BOOL)mode;
- (BOOL)getFarMode;

- (void)clearCurrentFrameValues;
- (void)setCurrentFrameWidth:(double)width
                      height:(double)height
                      bridge:(double)bridge;

- (BOOL)isAvaliableFrameParams;
- (double)getCurrentFrameWidth;
- (double)getCurrentFrameHeight;
- (double)getCurrentFrameBridge;

// Support

- (void)loadCurrentSessionSupportType:(u_int)type;
- (void)setCurrentSessionSupportType;
- (u_int)getCurrentSessionSupportType;

// iPad Flash Type
- (PDMCurrentIpadFlashType)currentIpadFlashType;
- (void)setIpadFlashType:(PDMCurrentIpadFlashType)type;

- (NSInteger)getFlashType;
- (void)setFlashType:(NSInteger)type;

// PD Analytics

- (UIImage *)getOriginPhoto;

@end
