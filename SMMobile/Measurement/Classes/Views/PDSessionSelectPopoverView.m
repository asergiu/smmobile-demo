//
//  PDSessionSelectPopoverView.m
//  AcepSmartMirror
//
//  Created by Владимир Малашенков on 24.03.13.
//
//

#import "PDSessionSelectPopoverView.h"

@implementation PDSessionSelectPopoverView

- (id)init
{
    self = [super init];
    if (self) {
        [self addViewButton];
        [self addDeleteButton];
    }
    return self;
}

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//
//        [self addViewButton];
//        [self addDeleteButton];
//    }
//    return self;
//}


- (void) addViewButton
{
    UIButton * viewButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    viewButton.frame = CGRectMake(0, 0, self.view.frame.size.width, 44.0f);
    
    [viewButton setTitle:@"Load Session" forState:UIControlStateNormal];
    
    [self.view addSubview:viewButton];
}

- (void) addDeleteButton
{
    UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    deleteButton.frame = CGRectMake(50, 0, self.view.frame.size.width, 44.0f);
    deleteButton.tintColor = [UIColor redColor];
    
    [self.view addSubview:deleteButton];
}

@end
