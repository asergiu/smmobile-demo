#import <UIKit/UIKit.h>

#define ARROW_HEIGHT 2.0

typedef enum PDMArrowType {
    PDMArrowTypeHorizontal = 0,
    PDMArrowTypeVertical,
} PDMArrowType;

@interface PDMArrow : UIView

- (id)initWithType:(PDMArrowType)type;
- (void)drawWithPoint:(CGPoint)point andLen:(CGFloat)len;


@end
