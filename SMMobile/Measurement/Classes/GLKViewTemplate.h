//
//  GLKViewTemplate.h
//  Visioner_4.0
//
//  Created by Dgut on 12.12.13.
//
//

#import <GLKit/GLKit.h>

@interface GLKViewTemplate : GLKView

-( void )setVideoMode : ( BOOL ) mode;
-( BOOL )videoMode;

-( void )setCameraPreset : ( NSString * )preset;

-( void )play;
-( void )stop;

-( GLKTextureInfo * )getTexture : ( NSString * )name;
-( GLuint )getProgram : ( NSString * )vertex : ( NSString * )fragment;

-( GLuint )videoTexture;
-( GLKVector2 )videoTextureSize;

-( void )drawRectangle : ( CGRect )rect;

@end
