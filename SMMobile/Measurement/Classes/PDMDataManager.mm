
#import "PDMDataManager.h"
#import "PDConfigManager.h"
#import "PDMResults.h"

extern CPDMResults Results;

@interface PDMDataManager ()
{
    BOOL farMode;
    
    double A;
    double B;
    double DBL;
    
    BOOL frameParams;
    
    u_int supportType;
    
    PDMCurrentIpadFlashType iPadFlashType;
}

@property (strong, nonatomic) UIImage *restoreImage;

@end

@implementation PDMDataManager

@synthesize analyticsHelper;

@synthesize storage;
@synthesize farPdKeys;
@synthesize nearPdKeys;
@synthesize farPDImage;
@synthesize nearPDImage;
@synthesize firstFarKeys;
@synthesize secondFarKeys;
@synthesize vdKeys;
@synthesize vdImage;
@synthesize appModeVD;
@synthesize vdImageRotated;
@synthesize farOriginPhoto;
@synthesize nearOriginPhoto;
@synthesize farPDImageRotated;
@synthesize nearPDImageRotated;
@synthesize nearPDImageOriginal;
@synthesize topWrapValue;
@synthesize restoreImage;
@synthesize currentSessionType;
@synthesize currentPhotoStep;

+ (PDMDataManager *)sharedManager
{
    static dispatch_once_t onceToken = 0;
    static id _sharedInstance = nil;
    
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

- (void)clearImages
{
    farOriginPhoto = nil;
    nearOriginPhoto = nil;
    farPDImage = nil;
    nearPDImage = nil;
    vdImage = nil;
    farPDImageRotated = nil;
    nearPDImageRotated = nil;
    nearPDImageOriginal = nil;
    vdImageRotated = nil;
    restoreImage = nil;
}

- (void)clearStorage
{
    self.topWrapValue = nil;
    [storage removeAllObjects];
}

- (void)removeFarPDPositions
{
    [storage removeObjectsForKeys:farPdKeys];
    farPDImage = nil;
    self.topWrapValue = nil;
}

- (void)removeVDPositions
{
    [storage removeObjectsForKeys:vdKeys];
    vdImage = nil;
}

- (void)removeNearPDPositions
{
    [storage removeObjectsForKeys:nearPdKeys];
    nearPDImage = nil;
}

- (void)removeFirstPDPositions
{
    [storage removeObjectsForKeys:firstFarKeys];
}

- (void)removeSecondPDPositions
{
    [storage removeObjectsForKeys:secondFarKeys];
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self clearImages];
        [self clearCurrentFrameValues];
        
        farMode = YES;
        
        [self setCurrentSessionSupportType];
        
        storage = [NSMutableDictionary dictionary];
        farPdKeys = [NSArray arrayWithObjects:   FAR_CENTER_SUPPORT_X,
                     FAR_CENTER_SUPPORT_Y,
                     FAR_LEFT_EYE_X,
                     FAR_LEFT_EYE_Y,
                     FAR_LEFT_RECT_HEIGHT,
                     FAR_LEFT_RECT_WIDTH,
                     FAR_LEFT_RECT_X,
                     FAR_LEFT_RECT_Y,
                     FAR_LEFT_SUPPORT_X,
                     FAR_LEFT_SUPPORT_Y,
                     FAR_RIGHT_EYE_X,
                     FAR_RIGHT_EYE_Y,
                     FAR_RIGHT_RECT_HEIGHT,
                     FAR_RIGHT_RECT_WIDTH,
                     FAR_RIGHT_RECT_X,
                     FAR_RIGHT_RECT_Y,
                     FAR_RIGHT_SUPPORT_X,
                     FAR_RIGHT_SUPPORT_Y,
                     FAR_SNAPPED_ANGLE,
                     FAR_SNAPPED_ANGLE_Z, nil];
        
        
        
        firstFarKeys = [NSArray arrayWithObjects:
                        FAR_RIGHT_SCREEN_POINT_X,
                        FAR_RIGHT_SCREEN_POINT_Y,
                        FAR_LEFT_SCREEN_POINT_X,
                        FAR_LEFT_SCREEN_POINT_Y, nil];
        
        secondFarKeys = [NSArray arrayWithObjects:
                         VD_RIGHT_SCREEN_POINT_X,
                         VD_RIGHT_SCREEN_POINT_Y,
                         VD_LEFT_SCREEN_POINT_X,
                         VD_LEFT_SCREEN_POINT_Y, nil];
        
        nearPdKeys = [NSArray arrayWithObjects:   NEAR_CENTER_SUPPORT_X,
                      NEAR_CENTER_SUPPORT_Y,
                      NEAR_DIST_TO_LEFT_X,
                      NEAR_DIST_TO_LEFT_Y,
                      NEAR_DIST_TO_RIGHT_X,
                      NEAR_DIST_TO_RIGHT_Y,
                      NEAR_LEFT_EYE_X,
                      NEAR_LEFT_EYE_Y,
                      NEAR_LEFT_RECT_HEIGHT,
                      NEAR_LEFT_RECT_WIDTH,
                      NEAR_LEFT_RECT_X,
                      NEAR_LEFT_RECT_Y,
                      NEAR_LEFT_SUPPORT_X,
                      NEAR_LEFT_SUPPORT_Y,
                      NEAR_RIGHT_EYE_X,
                      NEAR_RIGHT_EYE_Y,
                      NEAR_RIGHT_RECT_HEIGHT,
                      NEAR_RIGHT_RECT_WIDTH,
                      NEAR_RIGHT_RECT_X,
                      NEAR_RIGHT_RECT_Y,
                      NEAR_RIGHT_SUPPORT_X,
                      NEAR_RIGHT_SUPPORT_Y,
                      NEAR_SNAPPED_ANGLE,
                      NEAR_SNAPPED_ANGLE_Z,
                      nil];
        
        vdKeys = [NSArray arrayWithObjects:   VD_CENTER_SUPPORT_X,
                  VD_CENTER_SUPPORT_Y,
                  VD_LEFT_EYE_X,
                  VD_LEFT_EYE_Y,
                  VD_LEFT_RECT_HEIGHT,
                  VD_LEFT_RECT_WIDTH,
                  VD_LEFT_RECT_X,
                  VD_LEFT_RECT_Y,
                  VD_LEFT_SUPPORT_X,
                  VD_LEFT_SUPPORT_Y,
                  VD_RIGHT_EYE_X,
                  VD_RIGHT_EYE_Y,
                  VD_RIGHT_RECT_HEIGHT,
                  VD_RIGHT_RECT_WIDTH,
                  VD_RIGHT_RECT_X,
                  VD_RIGHT_RECT_Y,
                  VD_RIGHT_SUPPORT_X,
                  VD_RIGHT_SUPPORT_Y,
                  VD_SNAPPED_ANGLE,
                  VD_SNAPPED_ANGLE_Z, nil];
    }
    
    analyticsHelper = [SMDetectShareHelper new];
    
    return self;
}

#pragma mark Restore Snap Image Data

- (BOOL)hasSnapImage
{
    return restoreImage != nil;
}

- (void)setRestoreImage:(UIImage *)image
{
    restoreImage = image;
}

- (void)clearRestoreImage
{
    restoreImage = nil;
}

- (UIImage *)getRestoreImage
{
    return restoreImage;
}

- (void)setFarMode:(BOOL)mode
{
    farMode = mode;
}

- (BOOL)getFarMode
{
    return farMode;
}

#pragma mark - Current measurement A, B, DBL values

- (BOOL)isAvaliableFrameParams
{
    return frameParams;
}

- (void)clearCurrentFrameValues
{
    A = 0;
    B = 0;
    DBL = 0;
    
    frameParams = NO;
}

- (void)setCurrentFrameWidth:(double)width
                      height:(double)height
                      bridge:(double)bridge
{
    if (width > 0
        && height > 0
        && bridge > 0)
    {
        A = width;
        B = height;
        DBL = bridge;
        
        frameParams = YES;
    }
}

- (double)getCurrentFrameWidth
{
    return A;
}

- (double)getCurrentFrameHeight
{
    return B;
}

- (double)getCurrentFrameBridge
{
    return DBL;
}

#pragma mark - Support Type

- (void)loadCurrentSessionSupportType:(u_int)type
{
    supportType = type;
}

- (u_int)getCurrentSessionSupportType
{
    return supportType;
}

- (void)setCurrentSessionSupportType
{
    supportType = [PD_MANAGER getSupportFREDType];
}

#pragma mark - PD Analytics

- (UIImage *)getOriginPhoto
{
    if (farMode)
        return farOriginPhoto;
    else
        return nearOriginPhoto;
}

#pragma mark - iPad Flash Type

- (PDMCurrentIpadFlashType)currentIpadFlashType
{
    return iPadFlashType;
}

- (void)setIpadFlashType:(PDMCurrentIpadFlashType)type
{
    iPadFlashType = type;
}

- (NSInteger)getFlashType
{
    return (NSInteger)Results.GetFlashType();
}

- (void)setFlashType:(NSInteger)type
{
    Results.SetFlashType((PDMFlashType)type);
}

@end
