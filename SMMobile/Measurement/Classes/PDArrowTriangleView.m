//
//  PDArrowTriangleView.m
//  PD-Measurement
//
//  Created by Pavel Stoma on 6/1/12.
//

#import "PDArrowTriangleView.h"

@interface PDArrowTriangleView ()

@property PDMTriangleType triangleType;

@end

@implementation PDArrowTriangleView

@synthesize triangleType;

- (id)initWithType:(PDMTriangleType)type {
    self = [super initWithFrame:CGRectMake(0, 0, 0, 0)];
    if (self) {
        triangleType = type;
        
    }
    
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
}

@end
