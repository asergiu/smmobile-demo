#include "stddef.h"
#include "RotatePicture.h"
#include <math.h>
#include <string.h>

CRotatePicture::CRotatePicture(void)
{
	bSetPhotoData = false;
	// Support data
	xLeftSupport = 0.;
	yLeftSupport = 0.;
	xRightSupport = 0.;
	yRightSupport = 0.;
	xCenterSupport = 0.;
	yCenterSupport = 0.;
	// Eyes data
	xRightEye = 0.;
	yRightEye = 0.;
	xLeftEye = 0.;
	yLeftEye = 0.;
	// Center photo
	xCenterPhoto = 0.;
	yCenterPhoto = 0.;
	// Window size
	dxWindow = 0;
	dyWindow = 0;

	pResPic = NULL;
    picLength = 0;
    // Rotation angle
    rotationAngle = 0.;

    detectionInitialized = 0;
}

CRotatePicture::~CRotatePicture(void)
{
	if(pResPic)
		delete [] pResPic;
}

void CRotatePicture::NewPhoto()
{
	if(pResPic)
		delete [] pResPic;
    pResPic = NULL;
	bSetPhotoData = false;
    detectionInitialized = 0;
    // Rotation angle
    rotationAngle = 0.;
}

void CRotatePicture::NewLoadedSession()
{
    NewPhoto();
    detectionInitialized = 2;
}

bool CRotatePicture::IsDataPresent()
{
    return bSetPhotoData;
}

void CRotatePicture::ClearBitmapData()
{
    if(pResPic)
        delete [] pResPic;
    pResPic = NULL;
    picLength = 0;
}

void CRotatePicture::SetPhotoData(double xLeftSupport, double yLeftSupport, double xRightSupport, double yRightSupport,
				  double xCenterSupport, double yCenterSupport, double xLeftEye, double yLeftEye,
				  double xRightEye, double yRightEye, double xCenterPhoto, double yCenterPhoto,
				  int dxWindow, int dyWindow)
{
    // Support data
    double * xSupport[3], * ySupport[3];
    xSupport[0] = &xLeftSupport;
    ySupport[0] = &yLeftSupport;
    xSupport[1] = &xRightSupport;
    ySupport[1] = &yRightSupport;
    xSupport[2] = &xCenterSupport;
    ySupport[2] = &yCenterSupport;
    int i = 0, ismin = 1, ismax = 0, iscenter = 2;
    for (i = 0; i < 3; i++) {
        if (*(xSupport[i]) < *(xSupport[ismin])) {
            ismin = i;
        }
        if (*(xSupport[i]) > *(xSupport[ismax])) {
            ismax = i;
        }
    }
    for (i = 0; i < 3; i++) {
        if (i != ismin && i != ismax) {
            iscenter = i;
            break;
        }
    }
    
    CRotatePicture::xLeftSupport = *(xSupport[ismax]);
    CRotatePicture::yLeftSupport = *(ySupport[ismax]);
    CRotatePicture::xRightSupport = *(xSupport[ismin]);
    CRotatePicture::yRightSupport = *(ySupport[ismin]);
    CRotatePicture::xCenterSupport = *(xSupport[iscenter]);
    CRotatePicture::yCenterSupport = *(ySupport[iscenter]);
    // Eyes data
    if (xLeftEye > xRightEye) {
        CRotatePicture::xRightEye = xRightEye;
        CRotatePicture::yRightEye = yRightEye;
        CRotatePicture::xLeftEye = xLeftEye;
        CRotatePicture::yLeftEye = yLeftEye;
    }
    else {
        CRotatePicture::xRightEye = xLeftEye;
        CRotatePicture::yRightEye = yLeftEye;
        CRotatePicture::xLeftEye = xRightEye;
        CRotatePicture::yLeftEye = yRightEye;
    }
	// Center photo
	CRotatePicture::xCenterPhoto = xCenterPhoto;
	CRotatePicture::yCenterPhoto = yCenterPhoto;
	// Window size
	CRotatePicture::dxWindow = dxWindow;
	CRotatePicture::dyWindow = dyWindow;

	bSetPhotoData = true;
}

void CRotatePicture::GetRotateData(double& xLeftSupport, double& yLeftSupport, double& xRightSupport, double& yRightSupport,
				   double& xCenterSupport, double& yCenterSupport, double& xLeftEye, double& yLeftEye,
				   double& xRightEye, double& yRightEye)
{
	RotatePoint(CRotatePicture::xLeftSupport, CRotatePicture::yLeftSupport, xLeftSupport, yLeftSupport);
	RotatePoint(CRotatePicture::xRightSupport, CRotatePicture::yRightSupport, xRightSupport, yRightSupport);
	RotatePoint(CRotatePicture::xCenterSupport, CRotatePicture::yCenterSupport, xCenterSupport, yCenterSupport);
	RotatePoint(CRotatePicture::xLeftEye, CRotatePicture::yLeftEye, xLeftEye, yLeftEye);
	RotatePoint(CRotatePicture::xRightEye, CRotatePicture::yRightEye, xRightEye, yRightEye);
}

void CRotatePicture::RotatePoint(double xPoint, double yPoint, double& xOutPoint, double& yOutPoint)
{
	xOutPoint = yOutPoint = 0.;
	if(bSetPhotoData) {
		double dxSupport = xLeftSupport - xRightSupport;
		double dySupport = yLeftSupport - yRightSupport;
		double PD = sqrt(dxSupport * dxSupport + dySupport * dySupport);
		double cosFi = dxSupport / PD;
		double sinFi = dySupport / PD;
		// Rotation
		double dx = xPoint - xCenterPhoto;
		double dy = yPoint - yCenterPhoto;
		double X = dx * cosFi + dy * sinFi;
		double Y = - dx * sinFi + dy * cosFi;
		// Move
		xOutPoint = X + xCenterPhoto;
		yOutPoint = Y + yCenterPhoto;
	}
}

void CRotatePicture::DeRotatePoint(double xPoint, double yPoint, double& xOutPoint, double& yOutPoint)
{
	xOutPoint = yOutPoint = 0.;
	if(bSetPhotoData) {
		double dxSupport = xLeftSupport - xRightSupport;
		double dySupport = yLeftSupport - yRightSupport;
		double PD = sqrt(dxSupport * dxSupport + dySupport * dySupport);
		double cosFi = dxSupport / PD;
		double sinFi = dySupport / PD;
		// Rotation
		double dx = xPoint - xCenterPhoto;
		double dy = yPoint - yCenterPhoto;
		double X = dx * cosFi - dy * sinFi;
		double Y = dx * sinFi + dy * cosFi;
		// Move
		xOutPoint = X + xCenterPhoto;
		yOutPoint = Y + yCenterPhoto;
	}
}

void CRotatePicture::GetBitmapData(unsigned char ** pBitmap, unsigned int * pLength)
{
    *pBitmap = NULL;
    *pLength = 0;
    if(pResPic && picLength) {
        *pBitmap = pResPic;
        *pLength = picLength;
    }
}

double CRotatePicture::GetRotationAngle()
{
    return rotationAngle;
}

unsigned char * CRotatePicture::MakeRotatePicture(unsigned char * pPicture, int dxPicture, int dyPicture, int bytePerPixel)
{
	if(pResPic)
		delete [] pResPic;
	pResPic = NULL;
    picLength = 0;
    unsigned char * pResPicData = NULL;
	if(pPicture && dxPicture && dyPicture && bSetPhotoData) {
		int res_size = picLength = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + dxPicture * dyPicture * bytePerPixel;
		pResPic = new unsigned char[res_size];
		memset(pResPic, 0, res_size);
		if(pResPic) {
            BITMAPFILEHEADER * bmfh = (BITMAPFILEHEADER *)pResPic;
            bmfh->bfType = 0x4D42;
            bmfh->bfSize = res_size;
            bmfh->bfReserved1 = 0;
            bmfh->bfReserved2 = 0;
            bmfh->bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
            
            BITMAPINFOHEADER * bmih = (BITMAPINFOHEADER *)(pResPic + sizeof(BITMAPFILEHEADER));
            bmih->biSize = sizeof(BITMAPINFOHEADER);
            bmih->biWidth = dxPicture;
            bmih->biHeight = -dyPicture;
            bmih->biPlanes = 1;
            bmih->biBitCount = 32;
            bmih->biCompression = 0;
            bmih->biSizeImage = dxPicture * dyPicture * bytePerPixel;
            bmih->biXPelsPerMeter = 2834;
            bmih->biYPelsPerMeter = 2834;
            bmih->biClrUsed = 0;
            bmih->biClrImportant = 0;
            
            pResPicData = pResPic + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
			double dxSupport = xLeftSupport - xRightSupport;
			double dySupport = yLeftSupport - yRightSupport;
			double PD = sqrt(dxSupport * dxSupport + dySupport * dySupport);
			double cosFi = dxSupport / PD;
			double sinFi = dySupport / PD;
            // Rotation angle
            rotationAngle = atan(dySupport / dxSupport);
			int iyTag, ixTag;
			double xCenter = xCenterPhoto * dxPicture / dxWindow;
			double yCenter = yCenterPhoto * dyPicture / dyWindow;
			for(iyTag = 0; iyTag < dyPicture; iyTag++) {
				for(ixTag = 0; ixTag < dxPicture; ixTag++) {
					unsigned char * pTagPoint = pResPicData + iyTag * dxPicture * bytePerPixel + ixTag * bytePerPixel;
					// Rotation
					double dx = ixTag - xCenter;
					double dy = iyTag - yCenter;
					double X = dx * cosFi - dy * sinFi;
					double Y = dx * sinFi + dy * cosFi;
					// Move
					double ixPic = X + xCenter;
					double iyPic = Y + yCenter;
					if(ixPic > -1 && ixPic < dxPicture && iyPic > -1 && iyPic < dyPicture) {
						double rr[4], gg[4], bb[4];
                        for (int ik = 0; ik < 4; ik++) {
                            rr[ik] = 192;
                            gg[ik] = 192;
                            bb[ik] = 192;
                        }
						int ix = (int)ixPic, iy = (int)iyPic;
                        if (ixPic < 0) {
                            ix = (int)(ixPic - 1);
                        }
                        if (iyPic < 0) {
                            iy = (int)(iyPic - 1);
                        }
						double _aa = ixPic - (double)ix;
						double _bb = iyPic - (double)iy;
						double aa_ = 1. - _aa;
						double bb_ = 1. - _bb;

						unsigned char * pPoint = NULL;
                        if (iy >= 0 && iy < dyPicture && ix >= 0 && ix < dxPicture) {
                            pPoint = pPicture + iy * dxPicture * bytePerPixel + ix * bytePerPixel;
                            rr[0] = (double)(*(pPoint + 0));
                            gg[0] = (double)(*(pPoint + 1));
                            bb[0] = (double)(*(pPoint + 2));
                            
                        }
                        if (iy >= 0 && iy < dyPicture && ix >= -1 && ix < dxPicture - 1) {
                            pPoint = pPicture + iy * dxPicture * bytePerPixel + (ix + 1) * bytePerPixel;
                            rr[1] = (double)(*(pPoint + 0));
                            gg[1] = (double)(*(pPoint + 1));
                            bb[1] = (double)(*(pPoint + 2));
                            
                        }
                        if (iy >= -1 && iy < dyPicture - 1 && ix >= -1 && ix < dxPicture - 1) {
                            pPoint = pPicture + (iy + 1) * dxPicture * bytePerPixel + (ix + 1) * bytePerPixel;
                            rr[2] = (double)(*(pPoint + 0));
                            gg[2] = (double)(*(pPoint + 1));
                            bb[2] = (double)(*(pPoint + 2));
                            
                        }
                        if (iy >= -1 && iy < dyPicture - 1 && ix >= 0 && ix < dxPicture) {
                            pPoint = pPicture + (iy + 1) * dxPicture * bytePerPixel + ix * bytePerPixel;
                            rr[3] = (double)(*(pPoint + 0));
                            gg[3] = (double)(*(pPoint + 1));
                            bb[3] = (double)(*(pPoint + 2));
                            
                        }
                        
						double Color[4];
						double ResColor;
						int iPoint;
						int Red, Green, Blue;
						for(int iColor = 0; iColor < 3; iColor++){
							switch(iColor){
								case 0:			// -> Red
									for(iPoint = 0; iPoint < 4; iPoint++) Color[iPoint] = rr[iPoint];
									break;
								case 1:			// -> Green
									for(iPoint = 0; iPoint < 4; iPoint++) Color[iPoint] = gg[iPoint];
									break;
								case 2:			// -> Blue
									for(iPoint = 0; iPoint < 4; iPoint++) Color[iPoint] = bb[iPoint];
									break;
							}

							ResColor = Color[0] * aa_ * bb_ + Color[1] * _aa * bb_ +
								Color[2] * _aa * _bb + Color[3] * aa_ * _bb;

							switch(iColor){
								case 0:			// -> Red
									Red = (int)ResColor;
									break;
								case 1:			// -> Green
									Green = (int)ResColor;
									break;
								case 2:			// -> Blue
									Blue = (int)ResColor;
									break;
							}
						}
						if(Red > 255 || Green > 255 || Blue > 255) {
							int a;
							a = 0;
						}

						*pTagPoint = Blue;
						pTagPoint++;
						*pTagPoint = Green;
						pTagPoint++;
						*pTagPoint = Red;
						if(bytePerPixel == 4) {
							pTagPoint++;
							*pTagPoint = 0;
						}
					}
					else {
						*pTagPoint = 192;
						pTagPoint++;
						*pTagPoint = 192;
						pTagPoint++;
						*pTagPoint = 192;
						if(bytePerPixel == 4) {
							pTagPoint++;
							*pTagPoint = 0;
						}
					}
				}
			}
		}
	}
	return pResPicData;
}
