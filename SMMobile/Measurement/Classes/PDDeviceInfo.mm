#import "PDDeviceInfo.h"
#import <sys/utsname.h>

@implementation PDDeviceInfo

- (PDDeviceDetails)deviceDetails
{
    //NOTE: adjust code when double digit model numbers come out
    PDDeviceDetails details;
    
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *systemInfoString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    //get data
    details.modelCode = [systemInfoString copy];
    if ([[systemInfoString substringToIndex:6] isEqualToString:@"iPhone"]) {
        details.family = PDDeviceFamilyiPhone;
        details.bigModel = [[systemInfoString substringWithRange:NSMakeRange(6, 1)] intValue];
        details.smallModel = [[systemInfoString substringWithRange:NSMakeRange(8, 1)] intValue];
        
        if (details.bigModel == 1) {
            if (details.smallModel == 1) {
                details.model = PDDeviceModeliPhone;
                details.modelString = @"iPhone 1";
            }
            else if (details.smallModel == 2) {
                details.model = PDDeviceModeliPhone3G;
                details.modelString = @"iPhone 3G";
            }
            else {
                details.model = PDDeviceModelUnknown;
                details.modelString = @"Unknown";
            }
        }
        else if (details.bigModel == 2) {
            details.model = PDDeviceModeliPhone3GS;
            details.modelString = @"iPhone 3GS";
        }
        else if (details.bigModel == 3) {
            details.model = PDDeviceModeliPhone4;
            details.modelString = @"iPhone 4";
        }
        else if (details.bigModel == 4) {
            details.model = PDDeviceModeliPhone4S;
            details.modelString = @"iPhone 4S";
        }
        else if (details.bigModel == 5) {
            if (details.smallModel <= 2) {
                details.model = PDDeviceModeliPhone5;
                details.modelString = @"iPhone 5";
            }
            else if (details.smallModel <= 4) {
                details.model = PDDeviceModeliPhone5C;
                details.modelString = @"iPhone 5C";
            }
        }
        else if (details.bigModel == 6) {
            details.model = PDDeviceModeliPhone5S;
            details.modelString = @"iPhone 5S";
        }
        else if (details.bigModel == 7) {
            if (details.smallModel == 1) {
                details.model = PDDeviceModeliPhone6Plus;
                details.modelString = @"iPhone 6 Plus";
            }
            else if (details.smallModel == 2) {
                details.model = PDDeviceModeliPhone6;
                details.modelString = @"iPhone 6";
            }
        }
        else {
            details.model = PDDeviceModelUnknown;
            details.modelString = @"Unknown";
        }
    }
    else if ([[systemInfoString substringToIndex:4] isEqualToString:@"iPad"]) {
        details.family = PDDeviceFamilyiPad;
        if ([systemInfoString length] > 7) {
            if ([systemInfoString isEqualToString:@"iPad11,3"]) {
                details.bigModel = 11;
                details.smallModel = 3;
                details.model = PDDeviceModeliPadAir2019;
                details.modelString = @"iPad_Air_2019";
            }
            else if ([systemInfoString isEqualToString:@"iPad11,4"]) {
                details.bigModel = 11;
                details.smallModel = 4;
                details.model = PDDeviceModeliPadAir2019;
                details.modelString = @"iPad_Air_2019";
            }
            else {
                details.model = PDDeviceModelUnknown;
                details.modelString = @"Unknown";
            }
        }
        else {
            details.bigModel = [[systemInfoString substringWithRange:NSMakeRange(4, 1)] intValue];
            details.smallModel = [[systemInfoString substringWithRange:NSMakeRange(6, 1)] intValue];
            
            if (details.bigModel == 1) {
                details.model = PDDeviceModeliPad;
                details.modelString = @"iPad";
            }
            else if (details.bigModel == 2) {
                if (details.smallModel <= 4) {
                    details.model = PDDeviceModeliPad2;
                    details.modelString = @"iPad_2";
                }
                else if (details.smallModel <= 7) {
                    details.model = PDDeviceModeliPadMini;
                    details.modelString = @"iPad_Mini";
                }
            }
            else if (details.bigModel == 3) {
                if (details.smallModel <= 3) {
                    details.model = PDDeviceModeliPad3;
                    details.modelString = @"iPad_3";
                }
                else if (details.smallModel <= 6) {
                    details.model = PDDeviceModeliPad4;
                    details.modelString = @"iPad_4";
                }
                else {
                    details.model = PDDeviceModelUnknown;
                    details.modelString = @"Unknown";
                }
            }
            else if (details.bigModel == 4) {
                if (details.smallModel <= 3) {
                    details.model = PDDeviceModeliPadAir;
                    details.modelString = @"iPad_Air";
                }
                else if (details.smallModel >= 4 || details.smallModel <= 6) {
                    details.model = PDDeviceModeliPadMiniRetina;
                    details.modelString = @"iPad_Mini_Retina";
                }
                else if (details.smallModel >= 7 || details.smallModel <= 9) {
                    details.model = PDDeviceModeliPadMini3;
                    details.modelString = @"iPad_Mini_3";
                }
                else {
                    details.model = PDDeviceModelUnknown;
                    details.modelString = @"Unknown";
                }
            }
            else if (details.bigModel == 5){
                if (details.smallModel == 1 || details.smallModel == 2) {
                    details.model = PDDeviceModeliPadMini4;
                    details.modelString = @"iPad_Mini_4";
                }
                else if (details.smallModel == 3 || details.smallModel == 4) {
                    details.model = PDDeviceModeliPadAir2;
                    details.modelString = @"iPad_Air_2";
                }
                else {
                    details.model = PDDeviceModelUnknown;
                    details.modelString = @"Unknown";
                }
            }
            else if (details.bigModel == 6){
                if (details.smallModel == 3 || details.smallModel == 4) {
                    details.model = PDDeviceModeliPadPro97;
                    details.modelString = @"iPad_Pro_97";
                }
                else if (details.smallModel == 7 || details.smallModel == 8) {
                    details.model = PDDeviceModeliPadPro129;
                    details.modelString = @"iPad_Pro_129";
                }
                else if (details.smallModel == 11 || details.smallModel == 12) {
                    details.model = PDDeviceModeliPad97_2017;
                    details.modelString = @"iPad_97_2017";
                }
                else {
                    details.model = PDDeviceModelUnknown;
                    details.modelString = @"Unknown";
                }
            }
            else if (details.bigModel == 7){
                if (details.smallModel == 1 || details.smallModel == 2) {
                    details.model = PDDeviceModeliPadPro129_2017;
                    details.modelString = @"iPad_Pro_129_2017";
                }
                else if (details.smallModel == 3 || details.smallModel == 4) {
                    details.model = PDDeviceModeliPadPro105;
                    details.modelString = @"iPad_Pro_105";
                }
                else {
                    details.model = PDDeviceModelUnknown;
                    details.modelString = @"Unknown";
                }
            }
            else if (details.bigModel == 8){
                if (details.smallModel == 1 || details.smallModel == 2 || details.smallModel == 3 || details.smallModel == 4) {
                    details.model = PDDeviceModeliPadPro11;
                    details.modelString = @"iPad_Pro_11";
                }
                else if (details.smallModel == 5 || details.smallModel == 6 || details.smallModel == 7 || details.smallModel == 8) {
                    details.model = PDDeviceModeliPadPro129_2018;
                    details.modelString = @"iPad_Pro_129_2018";
                }
                else {
                    details.model = PDDeviceModelUnknown;
                    details.modelString = @"Unknown";
                }
            }
            else {
                details.model = PDDeviceModelUnknown;
                details.modelString = @"Unknown";
            }
        }
    }
    else if ([[systemInfoString substringToIndex:4] isEqualToString:@"iPod"]) {
        details.family = PDDeviceFamilyiPod;
        details.bigModel = [[systemInfoString substringWithRange:NSMakeRange(4, 1)] intValue];
        details.smallModel = [[systemInfoString substringWithRange:NSMakeRange(6, 1)] intValue];
        
        switch (details.bigModel) {
            case 1:
                details.model = PDDeviceModeliPod;
                break;
                
            case 2:
                details.model = PDDeviceModeliPod2;
                break;
                
            case 3:
                details.model = PDDeviceModeliPod3;
                break;
                
            case 4:
                details.model = PDDeviceModeliPod4;
                break;
                
            case 5:
                details.model = PDDeviceModeliPod5;
                break;
                
            default:
                details.model = PDDeviceModelUnknown;
                break;
        }
    }
    else {
        details.family = PDDeviceFamilyUnknown;
        details.bigModel = 0;
        details.smallModel = 0;
        details.model = PDDeviceModelUnknown;
    }
    
    //get screen size
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    //ipad old
    if ((screenWidth == 768) && (screenHeight == 1024)) {
        details.display = PDDeviceDisplayiPad;
    }
    //iphone
    else if ((screenWidth == 320) && (screenHeight == 480)) {
        details.display = PDDeviceDisplayiPhone35Inch;
    }
    //iphone 4 inch
    else if ((screenWidth == 320) && (screenHeight == 568)) {
        details.display = PDDeviceDisplayiPhone4Inch;
    }
    //unknown
    else {
        details.display = PDDeviceDisplayUnknown;
    }
    
    //ios version
    NSArray *versionCompatibility = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if (versionCompatibility.count > 0) {
        NSInteger version = [versionCompatibility[0] integerValue];
        details.iOSVersion = version >= 0 ? (NSUInteger)version : 0;
    }
    else {
        details.iOSVersion = 0;
    }
    
    return details;
}

-(NSString *)rawSystemInfoString {
    struct utsname systemInfo;
    uname(&systemInfo);
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}

@end
