#pragma once

#define WIDTHBYTES(i) ((unsigned)((i+31)&(~31))/8) /* ULONG aligned ! */

typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned int DWORD;
typedef int LONG;
typedef double DOUBLE;
typedef double* LPDOUBLE;

#pragma pack(push, 1)

typedef struct tagRGBQUAD {
    BYTE    rgbBlue;
    BYTE    rgbGreen;
    BYTE    rgbRed;
    BYTE    rgbReserved;
} RGBQUAD;
typedef RGBQUAD* LPRGBQUAD;

typedef struct tagBITMAPFILEHEADER {
    WORD    bfType;
    DWORD   bfSize;
    WORD    bfReserved1;
    WORD    bfReserved2;
    DWORD   bfOffBits;
} BITMAPFILEHEADER;
typedef BITMAPFILEHEADER* LPBITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER{
    DWORD      biSize;
    LONG       biWidth;
    LONG       biHeight;
    WORD       biPlanes;
    WORD       biBitCount;
    DWORD      biCompression;
    DWORD      biSizeImage;
    LONG       biXPelsPerMeter;
    LONG       biYPelsPerMeter;
    DWORD      biClrUsed;
    DWORD      biClrImportant;
} BITMAPINFOHEADER;
typedef BITMAPINFOHEADER* LPBITMAPINFOHEADER;

#pragma pack(pop)

class CRotatePicture
{
public:
	CRotatePicture(void);
	~CRotatePicture(void);
    void NewPhoto();
    void NewLoadedSession();
    bool IsDataPresent();
    void ClearBitmapData();

	void SetPhotoData(double xLeftSupport, double yLeftSupport, double xRightSupport, double yRightSupport,
		double xCenterSupport, double yCenterSupport, double xLeftEye, double yLeftEye,
		double xRightEye, double yRightEye, double xCenterPhoto, double yCenterPhoto,
		int dxWindow, int dyWindow);
	unsigned char * MakeRotatePicture(unsigned char * pPicture, int dxPicture, int dyPicture, int bytePerPixel);
	void GetRotateData(double& xLeftSupport, double& yLeftSupport, double& xRightSupport, double& yRightSupport,
		double& xCenterSupport, double& yCenterSupport, double& xLeftEye, double& yLeftEye,
		double& xRightEye, double& yRightEye);
	void RotatePoint(double xPoint, double yPoint, double& xOutPoint, double& yOutPoint);
	void DeRotatePoint(double xPoint, double yPoint, double& xOutPoint, double& yOutPoint);
    void GetBitmapData(unsigned char ** pBitmap, unsigned int * pLength);
    double GetRotationAngle();

    int detectionInitialized;

protected:
	bool bSetPhotoData;
	// Support data
	double xLeftSupport;
	double yLeftSupport;
	double xRightSupport;
	double yRightSupport;
	double xCenterSupport;
	double yCenterSupport;
	// Eyes data
	double xRightEye;
	double yRightEye;
	double xLeftEye;
	double yLeftEye;
	// Center photo
	double xCenterPhoto;
	double yCenterPhoto;
	// Window size
	int dxWindow;
	int dyWindow;

	unsigned char * pResPic;
    unsigned int picLength;
    // Rotation angle
    double rotationAngle;
};
