//
//  PDCameraButton.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 12/30/14.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

typedef NS_ENUM (NSInteger, PDCameraButtonType) {
    PDCameraButtonTypeHydro,
    PDCameraButtonTypeSimple
};

typedef NS_ENUM (NSInteger, PDButtonPosition) {
    PDButtonPositionLeft,
    PDButtonPositionRight
};

@protocol PDCameraButtonPalDelegate <NSObject>

@end

@interface PDCameraButton : UIView
{
    PDCameraButtonType buttonType;
    PDButtonPosition buttonPosition;
}

@property (nonatomic, assign) id <PDCameraButtonPalDelegate> delegate;

- (id)initWithFrame:(CGRect)frame
               type:(PDCameraButtonType)type
           position:(PDButtonPosition)position;

- (void)setButtonType:(PDCameraButtonType)type
             position:(PDButtonPosition)position;

- (void)setButtonSelector:(SEL)selector
                   target:(id)target;

- (void)setSnapProcessState:(BOOL)state;
- (void)setButtonState:(BOOL)state;
- (void)processAccelerometerData:(CMAccelerometerData *)accelerometerData;

@end
