//
//  PDWrapView.m
//  Visioner_4.0
//
//  Created by Dgut on 12.12.13.
//
//

#import "PDWrapView.h"
#import <AVFoundation/AVFoundation.h>
#import "PDConfigManager.h"

typedef struct
{
    GLKVector2 position;
    GLKVector2 size;
    
    BOOL rotation;
    GLKVector2 axis;
} WrapPoint;

@interface PDWrapView()
{
    GLKTextureInfo * wrapVisor;
    GLKTextureInfo * wrapVisorMarkers;
    GLKTextureInfo * wrapPoint;
    GLKTextureInfo * wrapArrows;
    GLKTextureInfo * wrapLine;
    GLKTextureInfo * wrapLineGray;
    
    GLuint defaultProgram;
    GLuint wrapProgram;
    
    float rotate;
    
    WrapPoint points[ 4 ];
    WrapPoint * currentPoint;
    
    CGPoint dragPoint;
    
    GLKMatrix4 transform;
    
    GLKVector2 screenSize;
    GLKVector2 backgroundSize;
}

@end

@implementation PDWrapView

-( id )initWithCoder : ( NSCoder * )aDecoder
{
    self = [ super initWithCoder : aDecoder ];
    if( self )
    {
        wrapVisor = [ self getTexture : @"wrap-visor.png" ];
        wrapVisorMarkers = [ self getTexture : @"wrap-visor-markers.png" ];
        wrapPoint = [ self getTexture : @"wrap_controll.png" ];//[ self getTexture : @"screen3_control@x2.png" ];
        wrapArrows = [ self getTexture : @"wrap-arrows.png" ];
        wrapLine = [ self getTexture : @"wrap-line.png" ];
        wrapLineGray = [ self getTexture : @"wrap-line-gray.png" ];
        
        defaultProgram = [ self getProgram : @"Default" : @"Default" ];
        wrapProgram = [ self getProgram : @"Wrap" : @"Wrap" ];
        
        GLKMatrix4 modelViewProjectionMatrix = GLKMatrix4MakeOrtho( 0, self.bounds.size.width, 0, self.bounds.size.height, -99999., 99999. );
        
        glUseProgram( defaultProgram );
        glUniformMatrix4fv( glGetUniformLocation( defaultProgram, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
        
        glUseProgram( wrapProgram );
        glUniformMatrix4fv( glGetUniformLocation( wrapProgram, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
        
        [ self setVideoMode : YES ];
        [ self setCameraPreset : AVCaptureSessionPresetHigh ];
        
        points[ 0 ].position = GLKVector2Make( self.frame.size.width / 2.f - 48.f, 390 + 75 );
        points[ 1 ].position = GLKVector2Add( points[ 0 ].position, GLKVector2Make( -300, -26.5 ) );
        points[ 2 ].position = GLKVector2Make( self.frame.size.width / 2.f + 48.f, 390 + 75 );
        points[ 3 ].position = GLKVector2Add( points[ 2 ].position, GLKVector2Make( 300, -26.5 ) );
        
        points[ 0 ].size = points[ 1 ].size = points[ 2 ].size = points[ 3 ].size = GLKVector2Make( 100, 100 );
        
        //points[ 1 ].rotation = points[ 2 ].rotation = points[ 3 ].rotation = YES;
        
        transform = GLKMatrix4Identity;
    }
    return self;
}

-( void )drawRect : ( CGRect )rect
{
    points[ 1 ].axis = points[ 2 ].axis = points[ 3 ].axis = points[ 0 ].position;
    
    screenSize = GLKVector2Make( self.frame.size.width, self.frame.size.height );
    backgroundSize = self.videoTextureSize;//GLKVector2Make( self.videoFrameTextureSize.width, self.videoFrameTextureSize.height );
    float factor = screenSize.y * 1.5f / backgroundSize.y;
    backgroundSize = GLKVector2MultiplyScalar( backgroundSize, factor );
    
    if( self.videoMode )
        rotate = [ [ UIApplication sharedApplication ] statusBarOrientation] == UIInterfaceOrientationLandscapeLeft ? 1.f : 0.f;
    
    glUseProgram( wrapProgram );
    glUniform1f( glGetUniformLocation( wrapProgram, "rotate" ), rotate );
    glUniform2fv( glGetUniformLocation( wrapProgram, "screenSize" ), 1, screenSize.v );
    glUniform2fv( glGetUniformLocation( wrapProgram, "backgroundSize" ), 1, backgroundSize.v );
    glUniformMatrix4fv( glGetUniformLocation( wrapProgram, "transform" ), 1, GL_FALSE, transform.m );
    
    glBindTexture( GL_TEXTURE_2D, self.videoTexture );
    [ self drawRectangle : rect ];
    
    glUseProgram( defaultProgram );
    
    if( self.videoMode )
    {
        glBindTexture( wrapVisor.target, wrapVisor.name );
        [ self drawRectangle : rect ];
    }
    else
    {
        //glBindTexture( wrapVisorMarkers.target, wrapVisorMarkers.name );
        glBindTexture( wrapVisor.target, wrapVisor.name );
        [ self drawRectangle : rect ];
        
#define CLAMP( x, a, b ) MAX( a, MIN( b, x ) )
        
        static const GLKVector2 BOUNDS_MIN = { 118.f, 133.f };
        static const GLKVector2 BOUNDS_MAX = { 906.f, 633.f };
        
        GLKVector2 cross = GLKVector2Make( self.frame.size.width / 2.f, 390 + 75 );//points[ 0 ].position;
        cross.x = CLAMP( cross.x, BOUNDS_MIN.x, BOUNDS_MAX.x );
        cross.y = CLAMP( cross.y, BOUNDS_MIN.y, BOUNDS_MAX.y );
        
        //static const float WIDTH = 2.f;
        
        /*glBindTexture( wrapLineGray.target, wrapLineGray.name );
        [ self drawRectangle : CGRectMake( cross.x - WIDTH / 2.f, BOUNDS_MIN.y, WIDTH, BOUNDS_MAX.y - BOUNDS_MIN.y ) ];
        [ self drawRectangle : CGRectMake( BOUNDS_MIN.x, cross.y - WIDTH / 2.f, BOUNDS_MAX.x - BOUNDS_MIN.x, WIDTH ) ];*/
        
        GLKMatrix4 modelViewProjectionMatrix;
        
        for( int i = 0; i < 3; i += 2 )
        {
            GLKVector2 a = points[ i ].position;
            GLKVector2 b = points[ i + 1 ].position;
            GLKVector2 c = GLKVector2MultiplyScalar( GLKVector2Add( a, b ), 0.5f );
            
            float length = GLKVector2Distance( a, b );
            float angle = atan2f( a.y - b.y, a.x - b.x );
            
            modelViewProjectionMatrix = GLKMatrix4MakeOrtho( 0, self.bounds.size.width, 0, self.bounds.size.height, -99999., 99999. );
            modelViewProjectionMatrix = GLKMatrix4Translate( modelViewProjectionMatrix, c.x, c.y, 0.f );
            modelViewProjectionMatrix = GLKMatrix4Rotate( modelViewProjectionMatrix, angle, 0.f, 0.f, 1.f );
            modelViewProjectionMatrix = GLKMatrix4Translate( modelViewProjectionMatrix, -c.x, -c.y, 0.f );
            glUniformMatrix4fv( glGetUniformLocation( wrapProgram, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
            
            static const float WIDTH = 4.f;
            
            glBindTexture( wrapLine.target, wrapLine.name );
            [ self drawRectangle : CGRectMake( c.x - length / 2.f, c.y - WIDTH / 2.f, length, WIDTH ) ];
        }
        
        modelViewProjectionMatrix = GLKMatrix4MakeOrtho( 0, self.bounds.size.width, 0, self.bounds.size.height, -99999., 99999. );
        glUniformMatrix4fv( glGetUniformLocation( wrapProgram, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
        
        glBindTexture( wrapPoint.target, wrapPoint.name );
        for( int i = 0; i < 4; i++ )
            [ self drawRectangle : CGRectMake(
                                              points[ i ].position.x - wrapPoint.width / 4.f,
                                              points[ i ].position.y - wrapPoint.height / 4.f,
                                              wrapPoint.width / 2.f,
                                              wrapPoint.height / 2.f ) ];
        
        /*glBindTexture( wrapArrows.target, wrapArrows.name );
        [ self drawRectangle : CGRectMake(
                                          self.frame.size.width / 2.f - wrapArrows.width / 4.f,
                                          140.f - wrapArrows.height / 4.f,
                                          wrapArrows.width / 2.f,
                                          wrapArrows.height / 2.f ) ];*/
        
        
        
        float one = GLKVector2DotProduct( GLKVector2Make( -1.f, 0.f ), GLKVector2Normalize( GLKVector2Subtract( points[ 1 ].position, points[ 0 ].position ) ) );
        one = CLAMP( one, -1.f, 1.f );
        float two = GLKVector2DotProduct( GLKVector2Make( 1.f, 0.f ), GLKVector2Normalize( GLKVector2Subtract( points[ 3 ].position, points[ 2 ].position ) ) );
        two = CLAMP( two, -1.f, 1.f );
        
        GLKVector2 near[ 2 ], far[ 2 ];
        float nearDistance = FLT_MAX;
        
        for( int i = 0; i < 2; i++ )
            for( int j = 0; j < 2; j++ )
            {
                float distance = GLKVector2Distance( points[ i ].position, points[ 2 + j ].position );
                
                if( nearDistance > distance )
                {
                    nearDistance = distance;
                    
                    near[ 0 ] = points[ i ].position;
                    near[ 1 ] = points[ 2 + j ].position;
                    far[ 0 ] = points[ !i ].position;
                    far[ 1 ] = points[ 2 + !j ].position;
                }
            }
        
        /*float angle = GLKVector2DotProduct(
                                           GLKVector2Normalize( GLKVector2Subtract( points[ 1 ].position, points[ 0 ].position ) ),
                                           GLKVector2Normalize( GLKVector2Subtract( points[ 2 ].position, points[ 0 ].position ) ) );
        angle = CLAMP( angle, -1.f, 1.f );*/
        
        float dot_value = GLKVector2DotProduct(
                                               GLKVector2Normalize( GLKVector2Subtract( far[ 0 ], near[ 0 ] ) ),
                                               GLKVector2Normalize( GLKVector2Subtract( far[ 1 ], near[ 1 ] ) ) );
        
        //float angle = ( acos( one ) + acos( two ) ) / 2.f;
        
        float angle = ( M_PI - acos( dot_value ) ) / 2.f;
        
        self.resultAngle = angle * 180.f / M_PI;
        
        self.resultLabel.text = [NSString stringWithFormat:@"%.1lf°", ([PD_MANAGER inExpertMode] ? self.resultAngle : [PD_MANAGER roundToHalf:self.resultAngle])];
    }
}

-( void )shot
{
    [ self setVideoMode : NO ];
}

-( void )reset
{
    self.resultLabel.text = @"";
    
    [ self setVideoMode : YES ];
}

-( void )touchesBegan : ( NSSet * )touches withEvent : ( UIEvent * )event
{
	for( UITouch * touch in touches )
    {
        CGPoint point = [ touch locationInView : self ];
        point.y = self.bounds.size.height - point.y;
        
        for( int i = 0; i < 4; i++ )
            if( fabs( point.x - points[ i ].position.x ) < points[ i ].size.x / 2.f &&
                fabs( point.y - points[ i ].position.y ) < points[ i ].size.y / 2.f )
            {
                currentPoint = points + i;
                break;
            }
        
        dragPoint = point;
    }
}

-( void )touchesMoved : ( NSSet * )touches withEvent : ( UIEvent * )event
{
	for( UITouch * touch in touches )
    {
        CGPoint point = [ touch locationInView : self ];
        point.y = self.bounds.size.height - point.y;
        
        if( currentPoint )
        {
            if( currentPoint->rotation )
            {
                if( currentPoint == points + 3 )
                {
                    float oldAngle = atan2f( currentPoint->position.x - currentPoint->axis.x, currentPoint->position.y - currentPoint->axis.y );
                    float newAngle = atan2f( point.x - currentPoint->axis.x, point.y - currentPoint->axis.y );
                    
                    float delta = newAngle - oldAngle;
                    GLKVector2 axis = GLKVector2Subtract( GLKVector2Add( points[ 0 ].position, GLKVector2MultiplyScalar( backgroundSize, 0.5f ) ), GLKVector2MultiplyScalar( screenSize, 0.5f ) );
                    
                    transform = GLKMatrix4Translate( transform, axis.x, axis.y, 0.f );
                    transform = GLKMatrix4Rotate( transform, delta, 0.f, 0.f, 1.f );
                    transform = GLKMatrix4Translate( transform, -axis.x, -axis.y, 0.f );
                }
                
                GLKVector2 dir = GLKVector2Make( point.x - currentPoint->axis.x, point.y - currentPoint->axis.y );
                dir = GLKVector2Normalize( dir );
                
                float length = GLKVector2Distance( currentPoint->position, currentPoint->axis );
                
                currentPoint->position = GLKVector2Add( currentPoint->axis, GLKVector2MultiplyScalar( dir, length ) );
            }
            else
            {
                /*for( int i = 0; i < 3; i++ )
                {
                    points[ i ].position.x += point.x - dragPoint.x;
                    points[ i ].position.y += point.y - dragPoint.y;
                }*/
                currentPoint->position.x += point.x - dragPoint.x;
                currentPoint->position.y += point.y - dragPoint.y;
            }
        }
		
        dragPoint = point;
    }
}

-( void )touchesEnded : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    /*if( currentPoint == points + 3 )
        points[ 3 ].position = GLKVector2Add( GLKVector2Make( self.frame.size.width / 2.f, 390 ), GLKVector2Make( 0, -250 ) );*/
    
    currentPoint = NULL;
}

-( void )touchesCancelled : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    [ self touchesEnded : touches withEvent : event ];
}

@end
