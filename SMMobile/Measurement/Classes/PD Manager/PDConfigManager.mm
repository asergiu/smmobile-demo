//
//  PDDataManager.m
//
//  Created by Oleg Bogatenko on 6/18/14.
//
//

#import "PDConfigManager.h"
#import "PDMConfiguration.h"
#import "PDDeviceInfo.h"

@interface PDConfigManager ()
{
    PDDeviceDetails deviceDetails;
}

@end

@implementation PDConfigManager

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken = 0;
    static id _sharedInstance = nil;
    
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        newGenDevice = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
        && [[UIScreen mainScreen] respondsToSelector:@selector(scale)]
        && [UIScreen mainScreen].scale > 1.f;
        
        deviceDetails = [[PDDeviceInfo alloc] deviceDetails];
        if (deviceDetails.family == PDDeviceFamilyiPad) {
            switch (deviceDetails.model) {
                case PDDeviceModeliPad:
                case PDDeviceModeliPad2:
                    newGenDevice = NO;
                    break;
                default:
                    newGenDevice = YES;
                    break;
            }
        }
        
        currentScreen = PDTargetScreenCamera;
    }
    return self;
}

- (BOOL)theNewiPad
{
    return newGenDevice;
}

#pragma mark - IPad Flash

- (BOOL)useIPadFlash
{
    if (deviceDetails.family == PDDeviceFamilyiPad)
    {
        if (deviceDetails.model == PDDeviceModeliPadPro11 ||
            deviceDetails.model == PDDeviceModeliPadPro129_2018)
            return YES;
    }
    
    return NO;
}

- (BOOL)flashPowerIsHigh
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"flash_power_is_high"];
}

#pragma mark - Expert Mode

- (BOOL)inExpertMode
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"use_expert"];
}

#pragma mark - PD Configuration Params

- (BOOL)showDeviceAngle
{
    return (SHOW_DEVICE_ANGLE == 1);
}

- (BOOL)liveSwitcherEnabled
{
    return (ENABLE_LIVE_SWITCHER == 1);
}

- (float)getMaximumAllowedAngle
{
    return MAXIMUM_ALOWED_ANGLE;
}

#pragma mark Target Screen

- (void)resetTargetScreen
{
    currentScreen = PDTargetScreenCamera;
}

- (void)setTargetScreen:(PDTargetScreen)screen
{
    currentScreen = screen;
}

- (PDTargetScreen)targetScreen
{
    return currentScreen;
}

#pragma mark - Session

- (BOOL)isLoadedSession
{
    return loadedSession;
}

- (void)setLoadedState:(BOOL)state
{
    loadedSession = state;
}

#pragma mark - Results Settings

- (BOOL)useOpSysWeb
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"use_opsysweb"];
}

#pragma mark - Rounding down

- (float)roundToHalf:(float)value
{
    if (value == 0)
        return 0;
    
    int sign = fabs(value)/value;
    
    return (fabs(value) < 0.5f) ? sign * 0.5f : round(value * 2.f) / 2.f;
}

#pragma mark - Support FRED

- (NSString *)supportNameForType:(u_int64_t)type
{
    switch (type) {
        case 0:
            return NSLocalizedString(@"Type 1", nil);
            break;
        case 1:
            return NSLocalizedString(@"Type 2", nil);
            break;
        default:
            return NSLocalizedString(@"Type 1", nil);
            break;
    }
}

- (void)setSupportFREDType:(u_int64_t)type
{
    [[NSUserDefaults standardUserDefaults] setInteger:(NSInteger)type forKey:@"fred_type"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"change_fred_type"];
    [[NSUserDefaults standardUserDefaults] setObject:[self supportNameForType:type]
                                              forKey:@"fred_type_title"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (u_int)getSupportFREDType
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return (u_int)[[NSUserDefaults standardUserDefaults] integerForKey:@"fred_type"];
}

- (BOOL)needSupportChange
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"change_fred_type"];
}

@end
