//
//  PDDataManager.h
//
//  Created by Oleg Bogatenko on 6/18/14.
//
//

#import <Foundation/Foundation.h>

#define PD_MANAGER [PDConfigManager sharedInstance]

typedef NS_ENUM (NSInteger, PDTargetScreen) {
    PDTargetScreenCamera,
    PDTargetScreenMarkers,
    PDTargetScreenLines,
    PDTargetScreenResults
};

@interface PDConfigManager : NSObject
{
    BOOL newGenDevice;
    
    PDTargetScreen currentScreen;
    
    BOOL loadedSession;
}

+ (instancetype)sharedInstance;

- (BOOL)theNewiPad;

- (BOOL)useIPadFlash;
- (BOOL)flashPowerIsHigh;

- (BOOL)inExpertMode;

- (BOOL)showDeviceAngle;
- (BOOL)liveSwitcherEnabled;
- (float)getMaximumAllowedAngle;

- (void)resetTargetScreen;
- (void)setTargetScreen:(PDTargetScreen)screen;
- (PDTargetScreen)targetScreen;

- (BOOL)isLoadedSession;
- (void)setLoadedState:(BOOL)state;

- (BOOL)useOpSysWeb;

- (float)roundToHalf:(float)value;

// Support

- (void)setSupportFREDType:(u_int64_t)type;
- (u_int)getSupportFREDType;
- (BOOL)needSupportChange;

@end
