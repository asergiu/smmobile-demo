//
//  PDUDPSelectPopover.h
//  KodakLensIDS
//
//  Created by Oleg Bogatenko on 17/09/2013.
//
//

#import <UIKit/UIKit.h>

@protocol PDUDPSelectPopoverDelegate <NSObject>

@optional
- (void)udpPopoverDidSelectElementAtIndex:(NSInteger)index;
- (void)dismissUDPPopover;
@end

@interface PDUDPSelectPopover : UITableViewController
{
    __unsafe_unretained id <PDUDPSelectPopoverDelegate> delegate;
    NSDictionary *currentFtpDictonary;
}

@property (nonatomic,assign) id <PDUDPSelectPopoverDelegate> delegate;

- (id)initWithFTPDictonary:(NSDictionary *)dictonary;

@end
