#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface PDDeviceInfo : NSObject

//defs
typedef enum {
    PDDeviceModelUnknown = 0,
    PDDeviceModeliPhone,
    PDDeviceModeliPhone3G,
    PDDeviceModeliPhone3GS,
    PDDeviceModeliPhone4,
    PDDeviceModeliPhone4S,
    PDDeviceModeliPhone5,
    PDDeviceModeliPhone5C,
    PDDeviceModeliPhone5S,
    PDDeviceModeliPhone6,
    PDDeviceModeliPhone6Plus,
    PDDeviceModeliPad,
    PDDeviceModeliPad2,
    PDDeviceModeliPad3,
    PDDeviceModeliPad4,
    PDDeviceModeliPadMini,
    PDDeviceModeliPadMiniRetina,
    PDDeviceModeliPadMini3,
    PDDeviceModeliPadMini4,
    PDDeviceModeliPadAir,
    PDDeviceModeliPadAir2,
    PDDeviceModeliPadPro129,
    PDDeviceModeliPadPro97,
    PDDeviceModeliPadPro105,
    PDDeviceModeliPadPro129_2017,
    PDDeviceModeliPad97_2017,
    PDDeviceModeliPadAir2019,
    PDDeviceModeliPadPro11,
    PDDeviceModeliPadPro129_2018,
    PDDeviceModeliPod,
    PDDeviceModeliPod2,
    PDDeviceModeliPod3,
    PDDeviceModeliPod4,
    PDDeviceModeliPod5,
} PDDeviceModel;

typedef enum {
    PDDeviceFamilyUnknown = 0,
    PDDeviceFamilyiPhone,
    PDDeviceFamilyiPad,
    PDDeviceFamilyiPod,
} PDDeviceFamily;

typedef enum {
    PDDeviceDisplayUnknown = 0,
    PDDeviceDisplayiPad,
    PDDeviceDisplayiPhone35Inch,
    PDDeviceDisplayiPhone4Inch,
} PDDeviceDisplay;

typedef struct {
    PDDeviceModel           model;
    PDDeviceFamily          family;
    PDDeviceDisplay         display;
    NSUInteger              bigModel;
    NSUInteger              smallModel;
    NSUInteger              iOSVersion;
    NSString*               modelString;
    NSString*               modelCode;
} PDDeviceDetails;

//public API
- (PDDeviceDetails)deviceDetails;
- (NSString *)rawSystemInfoString;

@end
