//
//  PDUDPSelectPopover.m
//  KodakLensIDS
//
//  Created by Oleg Bogatenko on 17/09/2013.
//
//

#import "PDUDPSelectPopover.h"

@implementation PDUDPSelectPopover

@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithFTPDictonary:(NSDictionary *)dictonary
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    if (self)
    {
        currentFtpDictonary = [[NSDictionary alloc] initWithDictionary:dictonary];
        self.contentSizeForViewInPopover = CGSizeMake(320, 400);
        [self.tableView reloadData];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.navigationController setNavigationBarHidden:YES];
    
    [self.navigationController setToolbarHidden:NO];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", @"") style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonPressed)];
    
    [self.navigationItem setLeftBarButtonItem:cancelButton animated:NO];
    
    [self setToolbarItems:[NSArray arrayWithObject:cancelButton]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[currentFtpDictonary allKeys] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *key = [[currentFtpDictonary allKeys]objectAtIndex:indexPath.row];
    
    if (key.length)
        cell.textLabel.text = [@"Server : "stringByAppendingString:key];
    
    return cell;
}

- (void) cancelButtonPressed
{
    if (delegate && [delegate respondsToSelector:@selector(dismissUDPPopover)])
        [delegate dismissUDPPopover];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (delegate && [delegate respondsToSelector:@selector(udpPopoverDidSelectElementAtIndex:)])
        [delegate udpPopoverDidSelectElementAtIndex:indexPath.row];
}

@end
