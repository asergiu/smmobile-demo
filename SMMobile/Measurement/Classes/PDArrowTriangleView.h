//
//  PDArrowTriangleView.h
//  PD-Measurement
//
//  Created by Pavel Stoma on 6/1/12.
//

#import <UIKit/UIKit.h>
typedef enum PDMTriangleType {
    PDMTriangleTypeLeft = 0,
    PDMTriangleTypeRight,
    PDMTriangleTypeUp,
    PDMTriangleTypeDown
} PDMTriangleType;

@interface PDArrowTriangleView : UIView

- (id)initWithType:(PDMTriangleType)type;

@end
