#import "PDMLinesView.h"

static const float POINT_TOUCH_RADIUS = 30.f;
static const float NEAR_MODE_LINE_LENGTH = 100.f;
static const float BORDER_WIDTH = 1.f;
static const float MINIMUM_CORNERS_DISTANCE = 100.f;
static const float ARROWS_OFFSET_FROM_END = 50.f;
static const float TOUCH_SCALE = 1.7f;
//static const float SCALE_TIME_FACTOR = 0.4f;
static const float SCALE_DURATION = 0.4f;
static const float SCALE_SPEED = TOUCH_SCALE / SCALE_DURATION;
static const float WRAP_HANDLE_LENGTH = 50.f;
static const float DEFAULT_ANGLE = 1.5f;

//#define SHOW_WRAP_MARKERS

GLKVector2 GLKVector2LinesIntersection( GLKVector2 a0, GLKVector2 a1, GLKVector2 b0, GLKVector2 b1 )
{
    float A1, A2, B1, B2, C1, C2;
    
    A1 = a1.y - a0.y;
    B1 = a0.x - a1.x;
    C1 = a0.x * ( a1.y - a0.y ) + a0.y * ( a0.x - a1.x );
    
    A2 = b1.y - b0.y;
    B2 = b0.x - b1.x;
    C2 = b0.x * ( b1.y - b0.y ) + b0.y * ( b0.x - b1.x );
    
    float delta = A1 * B2 - A2 * B1;
    if( !delta )
        return GLKVector2Make( NAN, NAN );
    
    return GLKVector2Make( ( B2 * C1 - B1 * C2 ) / delta, ( A1 * C2 - A2 * C1 ) / delta );
}

float GLKVector2LineSegmentDistance( GLKVector2 a, GLKVector2 b, GLKVector2 p )
{
    const float length = GLKVector2Distance( a, b );
    if( !length )
        return GLKVector2Distance( a, p );
    
    const float t = GLKVector2DotProduct( GLKVector2Subtract( p, a ), GLKVector2Subtract( b, a ) ) / ( length * length );
    if( t < 0.f )
        return GLKVector2Distance( p, a );
    else if( t > 1.f )
        return GLKVector2Distance( p, b );
    
    const GLKVector2 projection = GLKVector2Add( a, GLKVector2MultiplyScalar( GLKVector2Subtract( b, a ), t ) );
    return GLKVector2Distance( p, projection );
}

class LineCorner
{
    int touched;
    
    GLKVector2 startPosition;
    GLKVector2 startTouch;
    
    void SetMarker( int index, GLKVector2 p )
    {
        GLKVector2 dir[ 2 ] = { GLKVector2Subtract( markers[ ( index + 1 ) % 4 ], markers[ index ] ), GLKVector2Subtract( markers[ ( index + 3 ) % 4 ], markers[ index ] ) };
        
        dir[ 0 ] = GLKVector2Normalize( dir[ 0 ] );
        dir[ 1 ] = GLKVector2Normalize( dir[ 1 ] );
        
        GLKVector2 a = GLKVector2LinesIntersection( p, GLKVector2Add( p, dir[ 0 ] ), markers[ ( index + 1 ) % 4 ], markers[ ( index + 2 ) % 4 ] );
        GLKVector2 b = GLKVector2LinesIntersection( p, GLKVector2Add( p, dir[ 1 ] ), markers[ ( index + 2 ) % 4 ], markers[ ( index + 3 ) % 4 ] );
        
        markers[ index ] = p;
        markers[ ( index + 1 ) % 4 ] = a;
        markers[ ( index + 3 ) % 4 ] = b;
        
        /*if( index % 2 )
        {
            markers[ ( index + 1 ) % 4 ].x += delta.x;
            markers[ ( index + 3 ) % 4 ].y += delta.y;
        }
        else
        {
            markers[ ( index + 1 ) % 4 ].y += delta.y;
            markers[ ( index + 3 ) % 4 ].x += delta.x;
        }*/
    }
public:
    __weak PDMLinesView * view;
    
    GLKVector2 markers[ 4 ];
    /*
     3-----2
     |     |
     |     |
     0-----1
     */
    
    float wrapMarker;
    
    void SetMarkers( GLKVector2 a, GLKVector2 b, float angle )
    {
        angle = -M_PI_2 + ( angle * M_PI / 180.f ) * ( a.x < b.x ? 1.f : -1.f );
        GLKVector2 dir = GLKVector2Make( cos( angle ), sin( angle ) );
        
        markers[ 0 ] = a;
        markers[ 1 ] = GLKVector2LinesIntersection( a, GLKVector2Make( a.x + 1.f, a.y ), b, GLKVector2Add( b, dir ) );
        markers[ 2 ] = b;
        markers[ 3 ] = GLKVector2Make( a.x, b.y );
        
        wrapMarker = 0.5f;
    }
    
    const GLKVector2 GetWrapMarkerPosition( void ) const
    {
        GLKVector2 dir = GLKVector2Normalize( GLKVector2Subtract( markers[ 3 ], markers[ 0 ] ) );
        return GLKVector2Add( markers[ 0 ], GLKVector2MultiplyScalar( dir, wrapMarker * GLKVector2Distance( markers[ 3 ], markers[ 0 ] ) ) );
    }
    
    bool TouchDown( GLKVector2 point )
    {
        startTouch = point;
        
#ifdef SHOW_WRAP_MARKERS
        {
            GLKVector2 wm = GetWrapMarkerPosition();
            GLKVector2 dir = GLKVector2Normalize( GLKVector2Subtract( markers[ 1 ], markers[ 0 ] ) );
            if( GLKVector2LineSegmentDistance( wm, GLKVector2Add( wm, GLKVector2MultiplyScalar( dir, WRAP_HANDLE_LENGTH ) ), point ) < POINT_TOUCH_RADIUS )
            {
                touched = 8;
                startPosition = wm;
                return true;
            }
        }
#endif
        
        for( int i = 0; i < 4; i++ )
        {
            if( !view.showTopCorners && i == 2 )
                continue;
            
            if( GLKVector2Distance( markers[ i ], point ) < POINT_TOUCH_RADIUS )
            {
                touched = i;
                startPosition = markers[ i ];
                return true;
            }
        }
        
        for( int i = 0; i < 4; i++ )
        {
            if( !view.showTopCorners && ( i == 1 || i == 2 ) )
                continue;
            
            if( GLKVector2LineSegmentDistance( markers[ i ], markers[ ( i + 1 ) % 4 ], point ) < POINT_TOUCH_RADIUS )
            {
                touched = i + 4;
                startPosition = markers[ i ];
                return true;
            }
        }
        
        return false;
    }
    
    void TouchMove( GLKVector2 point, GLKVector2 delta )
    {
        bool lockRight = false;
        bool lockTop = false;
        
        GLKVector2 position;
        
        if( touched < 4 )
        {
            lockRight = markers[ touched ].x < markers[ ( touched + 2 ) % 4 ].x;
            lockTop = markers[ touched ].y < markers[ ( touched + 2 ) % 4 ].y;
            
            position = GLKVector2Add( startPosition, GLKVector2Subtract( point, startTouch ) );
        }
        else if( touched < 8 )
        {
            lockRight = markers[ touched % 4 ].x < markers[ ( touched % 4 + 2 ) % 4 ].x;
            lockTop = markers[ touched % 4 ].y < markers[ ( touched % 4 + 2 ) % 4 ].y;
            
            GLKVector2 dir = touched % 2 ? GLKVector2Make( 1.f, 0.f ) : GLKVector2Make( 0.f, 1.f );
            GLKVector2 startProjection = GLKVector2Project( startTouch, dir );
            position = GLKVector2Add( startPosition, GLKVector2Subtract( GLKVector2Project( point, dir ), startProjection ) );
        }
        else
        {
            GLKVector2 dir = GLKVector2Normalize( GLKVector2Subtract( markers[ 3 ], markers[ 0 ] ) );
            float minimum = GLKVector2DotProduct( dir, markers[ 0 ] );
            float maximum = GLKVector2DotProduct( dir, markers[ 3 ] );
            float delta = GLKVector2DotProduct( dir, point ) - GLKVector2DotProduct( dir, startTouch );
            
            //delta = delta / ( maximum - minimum );
            
            wrapMarker = ( GLKVector2DotProduct( dir, startPosition ) + delta - minimum ) / ( maximum - minimum );
            
            if( wrapMarker < 0.f )
                wrapMarker = 0.f;
            if( wrapMarker > 1.f )
                wrapMarker = 1.f;
        }
        
        if( touched < 8 )
        {
            if( lockRight )
            {
                if( position.x > markers[ ( touched + 2 ) % 4 ].x - MINIMUM_CORNERS_DISTANCE )
                    position.x = markers[ ( touched + 2 ) % 4 ].x - MINIMUM_CORNERS_DISTANCE;
            }
            else
            {
                if( position.x < markers[ ( touched + 2 ) % 4 ].x + MINIMUM_CORNERS_DISTANCE )
                    position.x = markers[ ( touched + 2 ) % 4 ].x + MINIMUM_CORNERS_DISTANCE;
            }
            
            if( lockTop )
            {
                if( position.y > markers[ ( touched + 2 ) % 4 ].y - MINIMUM_CORNERS_DISTANCE )
                    position.y = markers[ ( touched + 2 ) % 4 ].y - MINIMUM_CORNERS_DISTANCE;
            }
            else
            {
                if( position.y < markers[ ( touched + 2 ) % 4 ].y + MINIMUM_CORNERS_DISTANCE )
                    position.y = markers[ ( touched + 2 ) % 4 ].y + MINIMUM_CORNERS_DISTANCE;
            }
            
            SetMarker( touched % 4, position );
        }
    }
    
    void Frame( void )
    {
    }
};

@interface PDMLinesView()
{
    GLKTextureInfo * background;
    
    GLKTextureInfo * pointTexture;
    GLKTextureInfo * arrowsTexture;
    GLKTextureInfo * wrapHandleLeftTexture;
    GLKTextureInfo * wrapHandleRightTexture;
    
    GLuint defaultProgram;
    GLuint borderProgram;
    
    LineCorner corners[ 2 ];
    
    LineCorner * touched;
    
    BOOL showTopCorners;
    
    CGPoint dragPoint;
    CGPoint dragScaledPoint;
    
    float scale;
    float wishScale;
    GLKVector2 scalePoint;
    
    BOOL leftBoxMoved;
    
    float leftAngle;
    float rightAngle;
}

@end

@implementation PDMLinesView

@synthesize controller;

-( id )initWithCoder : ( NSCoder * )aDecoder
{
    self = [ super initWithCoder : aDecoder ];
    if( self )
    {
        glEnable( GL_BLEND );
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
        
        leftAngle = DEFAULT_ANGLE;
        rightAngle = DEFAULT_ANGLE;
        
        defaultProgram = [ self getProgram : @"Default" : @"Default" ];
        borderProgram = [ self getProgram : @"PDMLinesBorder" : @"PDMLinesBorder" ];
        
        pointTexture = [ self getTexture : @"screen3_control@x2.png" ];
        arrowsTexture = [ self getTexture : @"doubleArrowVertical.png" ];
        wrapHandleLeftTexture = [ self getTexture : @"wrap_handle_left@x2.png" ];
        wrapHandleRightTexture = [ self getTexture : @"wrap_handle_right@x2.png" ];
        
        corners[ 0 ].view = self;
        corners[ 1 ].view = self;
        
        corners[ 0 ].SetMarkers( GLKVector2Make( self.frame.size.width / 2.f + 100.f, self.frame.size.height / 2.f - 50.f ), GLKVector2Make( self.frame.size.width / 2.f + 350.f, self.frame.size.height / 2.f + 150.f ), DEFAULT_ANGLE );
        corners[ 1 ].SetMarkers( GLKVector2Make( self.frame.size.width / 2.f - 100.f, self.frame.size.height / 2.f - 50.f ), GLKVector2Make( self.frame.size.width / 2.f - 350.f, self.frame.size.height / 2.f + 150.f ), DEFAULT_ANGLE );
        
        showTopCorners = YES;
        
        scale = wishScale = 1.f;
        
        [ self setFPS : 60.f ];
    }
    return self;
}

-( void )dealloc
{
    NSLog( @"lines view dealloc" );
}

-( void )drawRect : ( CGRect )rect
{
    /*scale = scale * ( 1.f - SCALE_TIME_FACTOR ) + wishScale * SCALE_TIME_FACTOR;
    if( fabs( scale - wishScale ) < 0.01f )
        scale = wishScale;*/
    
    {
        float sign = ( wishScale - scale );
        
        if( sign != 0 )
        {
            sign /= fabs( sign );
            
            scale += SCALE_SPEED * sign / self.fps;
            
            if( sign > 0.f && scale > wishScale )
                scale = wishScale;
            if( sign < 0.f && scale < wishScale )
                scale = wishScale;
        }
    }
    
    corners[ 0 ].Frame();
    corners[ 1 ].Frame();
    
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( background.target, background.name );
    
    GLKMatrix4 modelViewProjectionMatrix = GLKMatrix4MakeOrtho( 0, rect.size.width, 0, rect.size.height, -99999., 99999. );
    modelViewProjectionMatrix = GLKMatrix4Translate( modelViewProjectionMatrix, scalePoint.x, scalePoint.y, 0.f );
    modelViewProjectionMatrix = GLKMatrix4Scale( modelViewProjectionMatrix, scale, scale, 1.f );
    modelViewProjectionMatrix = GLKMatrix4Translate( modelViewProjectionMatrix, -scalePoint.x, -scalePoint.y, 0.f );
    
    GLKVector2 screenSize = GLKVector2Make( rect.size.width, rect.size.height );
    GLKVector2 backgroundSize = GLKVector2Make( background.width, background.height );
    
    {
        float s;
        
        float backScaleX = screenSize.x / backgroundSize.x;
        float backScaleY = screenSize.y / backgroundSize.y;
        
        if( backScaleX > backScaleY )
            s = backScaleX;
        else
            s = backScaleY;
        
        backgroundSize = GLKVector2MultiplyScalar( backgroundSize, s );
    }
    
    glUseProgram( defaultProgram );
    glUniformMatrix4fv( glGetUniformLocation( defaultProgram, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    /*glUniform2fv( glGetUniformLocation( defaultProgram, "screenSize" ), 1, screenSize.v );
    glUniform2fv( glGetUniformLocation( defaultProgram, "backgroundSize" ), 1, backgroundSize.v );*/
    
    [ self drawRectangle : CGRectMake( ( screenSize.x - backgroundSize.x ) * 0.5f, ( screenSize.y - backgroundSize.y ) * 0.5f, backgroundSize.x, backgroundSize.y ) ];
    
    glUseProgram( borderProgram );
    //glUniformMatrix4fv( glGetUniformLocation( borderProgram, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    glUniform2fv( glGetUniformLocation( borderProgram, "screenSize" ), 1, screenSize.v );
    glUniform2fv( glGetUniformLocation( borderProgram, "backgroundSize" ), 1, backgroundSize.v );
    
    for( int i = 0; i < 2; i++ )
        for( int j = 0; j < 4; j++ )
        {
            if( !showTopCorners && ( j == 1 || j == 2 ) )
                continue;
            [ self drawLineFrom : corners[ i ].markers[ j ] to : corners[ i ].markers[ ( j + 1 ) % 4 ] width : BORDER_WIDTH program : borderProgram matrix : modelViewProjectionMatrix ];
        }
    
    glUseProgram( defaultProgram );
    glBindTexture( pointTexture.target, pointTexture.name );
    for( int i = 0; i < 2; i++ )
        for( int j = 0; j < 4; j++ )
        {
            if( !showTopCorners && j == 2 )
                continue;
            [ self drawRectangle : CGRectMake( corners[ i ].markers[ j ].x - pointTexture.width / 4.f, corners[ i ].markers[ j ].y - pointTexture.height / 4.f, pointTexture.width / 2.f, pointTexture.height / 2.f ) ];
        }
    
    glUseProgram( defaultProgram );
    glBindTexture( arrowsTexture.target, arrowsTexture.name );
    for( int i = 0; i < 2; i++ )
        for( int j = 0; j < 4; j++ )
        {
            if( !showTopCorners && ( j == 1 || j == 2 ) )
                continue;
            
            GLKVector2 dir = GLKVector2Normalize( GLKVector2Subtract( corners[ i ].markers[ ( j + 1 ) % 4 ], corners[ i ].markers[ j ] ) );
            float distance = j % 2 ? ARROWS_OFFSET_FROM_END : GLKVector2Distance( corners[ i ].markers[ j ], corners[ i ].markers[ ( j + 1 ) % 4 ] ) - ARROWS_OFFSET_FROM_END;
            GLKVector2 c = GLKVector2Add( corners[ i ].markers[ j ], GLKVector2MultiplyScalar( dir, distance ) );
            float angle = atan2f( corners[ i ].markers[ j ].y - corners[ i ].markers[ ( j + 1 ) % 4 ].y, corners[ i ].markers[ j ].x - corners[ i ].markers[ ( j + 1 ) % 4 ].x );
            
            GLKMatrix4 modelViewProjectionMatrix_ = modelViewProjectionMatrix;
            
            GLKMatrix4 modelViewMatrix = GLKMatrix4MakeTranslation( c.x, c.y, 0.f );
            modelViewMatrix = GLKMatrix4Rotate( modelViewMatrix, angle, 0.f, 0.f, 1.f );
            modelViewMatrix = GLKMatrix4Translate( modelViewMatrix, -c.x, -c.y, 0.f );
            
            modelViewProjectionMatrix_ = GLKMatrix4Multiply( modelViewProjectionMatrix_, modelViewMatrix );
            
            glUniformMatrix4fv( glGetUniformLocation( defaultProgram, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix_.m );
            [ self drawRectangle : CGRectMake( c.x - arrowsTexture.width / 4.f, c.y - arrowsTexture.height / 4.f, arrowsTexture.width / 2.f, arrowsTexture.height / 2.f ) ];
        }
    
#ifdef SHOW_WRAP_MARKERS
    glUniformMatrix4fv( glGetUniformLocation( defaultProgram, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    
    glBindTexture( wrapHandleLeftTexture.target, wrapHandleLeftTexture.name );
    {
        GLKVector2 wm = corners[ 1 ].GetWrapMarkerPosition();
        [ self drawRectangle : CGRectMake( wm.x - wrapHandleLeftTexture.width / 4.f, wm.y - wrapHandleLeftTexture.height / 4.f, wrapHandleLeftTexture.width / 2.f, wrapHandleLeftTexture.height / 2.f ) ];
    }
    
    glBindTexture( wrapHandleRightTexture.target, wrapHandleRightTexture.name );
    {
        GLKVector2 wm = corners[ 0 ].GetWrapMarkerPosition();
        [ self drawRectangle : CGRectMake( wm.x - wrapHandleRightTexture.width / 4.f, wm.y - wrapHandleRightTexture.height / 4.f, wrapHandleRightTexture.width / 2.f, wrapHandleRightTexture.height / 2.f ) ];
    }
#endif
}

-( void )drawLineFrom : ( GLKVector2 )a to : ( GLKVector2 )b width : ( float )width program : ( GLuint )program matrix : ( GLKMatrix4 )matrix
{
    GLKVector2 c = GLKVector2MultiplyScalar( GLKVector2Add( a, b ), 0.5f );
    
    float length = GLKVector2Distance( a, b );
    float angle = atan2f( a.y - b.y, a.x - b.x );
    
    GLKMatrix4 modelViewProjectionMatrix = matrix;//GLKMatrix4MakeOrtho( 0, self.frame.size.width, 0, self.frame.size.height, -99999., 99999. );
    
    GLKMatrix4 modelViewMatrix = GLKMatrix4MakeTranslation( c.x, c.y, 0.f );
    modelViewMatrix = GLKMatrix4Rotate( modelViewMatrix, angle, 0.f, 0.f, 1.f );
    modelViewMatrix = GLKMatrix4Translate( modelViewMatrix, -c.x, -c.y, 0.f );
    
    modelViewProjectionMatrix = GLKMatrix4Multiply( modelViewProjectionMatrix, modelViewMatrix );
    glUniformMatrix4fv( glGetUniformLocation( program, "modelViewMatrix" ), 1, 0, modelViewMatrix.m );
    glUniformMatrix4fv( glGetUniformLocation( program, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    
    [ self drawRectangle : CGRectMake( c.x - length / 2.f, c.y - width / 2.f, length, width ) ];
}

-( void )touchesBegan : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    for( UITouch * touch in touches )
    {
        CGPoint point = [ touch locationInView : self ];
        point.y = self.bounds.size.height - point.y;
        
        for( int i = 0; i < 2; i++ )
            if( corners[ i ].TouchDown( GLKVector2Make( point.x, point.y ) ) )
            {
                leftBoxMoved = i;
                touched = corners + i;
                wishScale = TOUCH_SCALE;
                scalePoint = GLKVector2Make( point.x, point.y );
                break;
            }
        
        dragPoint = point;
        dragScaledPoint = point;
    }
}

-( void )touchesMoved : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    for( UITouch * touch in touches )
    {
        CGPoint point = [ touch locationInView : self ];
        point.y = self.bounds.size.height - point.y;
        
        CGPoint scaledPoint = CGPointMake( dragScaledPoint.x + ( point.x - dragPoint.x ) / scale, dragScaledPoint.y + ( point.y - dragPoint.y ) / scale );
        
        if( touched )
            touched->TouchMove( GLKVector2Make( scaledPoint.x, scaledPoint.y ), GLKVector2Make( scaledPoint.x - dragScaledPoint.x, scaledPoint.y - dragScaledPoint.y ) );
        
        dragPoint = point;
        dragScaledPoint = scaledPoint;
        
        if (controller && [controller respondsToSelector:@selector(leftTopPointMoved:)])
        {
            [controller leftTopPointMoved:leftBoxMoved];
        }
    }
}

-( void )touchesEnded : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    touched = 0;
    wishScale = 1.f;
}

-( void )touchesCancelled : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    [ self touchesEnded : touches withEvent : event ];
}

-( void )setBackgroundImage : ( UIImage * )image
{
    [ EAGLContext setCurrentContext : self.context ];
    
    GLuint t = background.name;
    glDeleteTextures( 1, &t );
    
    NSDictionary * options = @{ GLKTextureLoaderOriginBottomLeft : @( YES ), GLKTextureLoaderApplyPremultiplication : @( self.premultiplicationParameter ) };
    
    // Lifehack for flip image on iPad Air, iPad Air 2
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    UIImage *copyImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    background = [ GLKTextureLoader textureWithCGImage : copyImage.CGImage options : options error : NULL ];
}

/*-( void )setLeftCornerBottomPoint : ( CGPoint )bottom topPoint : ( CGPoint )top angle : ( float )angle
{
    corners[ 1 ].SetMarkers( GLKVector2Make( bottom.x, bottom.y ), GLKVector2Make( top.x, top.y ), angle );
}

-( void )setRightCornerBottomPoint : ( CGPoint )bottom topPoint : ( CGPoint )top angle : ( float )angle
{
    corners[ 0 ].SetMarkers( GLKVector2Make( bottom.x, bottom.y ), GLKVector2Make( top.x, top.y ), angle );
}*/

-( void )setLeftCornerBottomPoint : ( CGPoint )point
{
    corners[ 1 ].SetMarkers( GLKVector2Make( point.x, point.y ), corners[ 1 ].markers[ 2 ], leftAngle );
}

-( void )setRightCornerBottomPoint : ( CGPoint )point
{
    corners[ 0 ].SetMarkers( GLKVector2Make( point.x, point.y ), corners[ 0 ].markers[ 2 ], rightAngle );
}

-( void )setLeftCornerTopPoint : ( CGPoint )point
{
    corners[ 1 ].SetMarkers( corners[ 1 ].markers[ 0 ], GLKVector2Make( point.x, point.y ), leftAngle );
}

-( void )setRightCornerTopPoint : ( CGPoint )point
{
    corners[ 0 ].SetMarkers( corners[ 0 ].markers[ 0 ], GLKVector2Make( point.x, point.y ), rightAngle );
}

-( void )setLeftCornerAngle : ( float )angle
{
    corners[ 1 ].SetMarkers( corners[ 1 ].markers[ 0 ], corners[ 1 ].markers[ 2 ], angle );
    leftAngle = angle;
}

-( void )setRightCornerAngle : ( float )angle
{
    corners[ 0 ].SetMarkers( corners[ 0 ].markers[ 0 ], corners[ 0 ].markers[ 2 ], angle );
    rightAngle = angle;
}

-( CGPoint )leftCornerMarker : ( int )index
{
    return CGPointMake( corners[ 1 ].markers[ index ].x, corners[ 1 ].markers[ index ].y );
}

-( CGPoint )rightCornerMarker : ( int )index
{
    return CGPointMake( corners[ 0 ].markers[ index ].x, corners[ 0 ].markers[ index ].y );
}

-( float )leftWrapMarkerFraction
{
    return corners[ 1 ].wrapMarker;
}

-( float )rightWrapMarkerFraction
{
    return corners[ 0 ].wrapMarker;
}

-( void )setShowTopCorners : ( BOOL )show
{
    showTopCorners = show;
}

-( BOOL )showTopCorners
{
    return showTopCorners;
}

@end
