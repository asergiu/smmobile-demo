
#import <UIKit/UIKit.h>

@protocol PDMTouchViewDelegate <NSObject>

@optional
- (void)capturePressed;
@end

@interface PDMTouchView : UIView

@property (nonatomic, assign) id <PDMTouchViewDelegate> delegate;

- (void)snapProcess:(BOOL)snap;

@end
