//
//  PDCameraButton.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 12/30/14.
//

#import "PDCameraButton.h"
#import "PDHydroLevelButton.h"
#import "PDRightHydroLevelButton.h"
#import "PDConfigManager.h"

@interface PDCameraButton ()
{
    BOOL inSnapProcess;
    
    BOOL activeButtonState;
}

@property (nonatomic, strong) UIButton *simpleButton;
@property (nonatomic, strong) PDHydroLevelButton *hydroButton;

@property (nonatomic, assign) double device_angle;

@end

@implementation PDCameraButton

@synthesize delegate;
@synthesize simpleButton;
@synthesize hydroButton;
@synthesize device_angle;

const float BUTTON_WIDTH  = 105.f;
const float BUTTON_HEIGHT = 240.f;

const int btn_n_deque_size = 7;
const int btn_n_deque_size_bubble = 7;

double btn_angle_deque[7] = {0., 0., 0., 0., 0., 0., 0.};
double btn_angle_deque_bubble[7] = {0., 0., 0., 0., 0., 0., 0.};

double btn_previouse_angle = 0.;
double btn_previouse_angle_bubble = 0.;

int btn_angle_deque_size = 0;
int btn_angle_deque_size_bubble = 0;

int btn_i_angle_deque = 0;
int btn_i_angle_deque_bubble = 0;


- (id)initWithFrame:(CGRect)frame
               type:(PDCameraButtonType)type
           position:(PDButtonPosition)position
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        
        inSnapProcess = NO;
        activeButtonState = YES;
        buttonType = type;
        buttonPosition = position;
    }
    return self;
}

- (void)setButtonType:(PDCameraButtonType)type
             position:(PDButtonPosition)position
{
    inSnapProcess = NO;
    activeButtonState = YES;
    buttonType = type;
    buttonPosition = position;
}

- (void)drawRect:(CGRect)rect
{
    for (UIView *v in self.subviews)
        [v removeFromSuperview];
    
    if (buttonType == PDCameraButtonTypeHydro)
    {
        if (buttonPosition == PDButtonPositionLeft)
        {
            hydroButton = [[PDHydroLevelButton alloc] initWithFrame:CGRectMake(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT)];
        }
        else
        {
            hydroButton = [[PDRightHydroLevelButton alloc] initWithFrame:CGRectMake(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT)];
        }
        
        [self addSubview:hydroButton];
    }
    else
    {
        simpleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT)];
        
        if (buttonPosition == PDButtonPositionLeft)
        {
            [simpleButton setBackgroundImage:[UIImage imageNamed:@"PDShootLeft"] forState:UIControlStateNormal];
            [simpleButton setBackgroundImage:[UIImage imageNamed:@"PDShootLeft"] forState:UIControlStateHighlighted];
            [simpleButton setBackgroundImage:[UIImage imageNamed:@"PDShootLeft"] forState:UIControlStateSelected];
        }
        else
        {
            [simpleButton setBackgroundImage:[UIImage imageNamed:@"PDShootRight"] forState:UIControlStateNormal];
            [simpleButton setBackgroundImage:[UIImage imageNamed:@"PDShootRight"] forState:UIControlStateHighlighted];
            [simpleButton setBackgroundImage:[UIImage imageNamed:@"PDShootRight"] forState:UIControlStateSelected];
        }
        
        [self addSubview:simpleButton];
    }
    
    self.userInteractionEnabled = YES;
}

- (void)setButtonSelector:(SEL)selector target:(id)target
{
    if (buttonType == PDCameraButtonTypeHydro)
    {
        [hydroButton.button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [simpleButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)setSnapProcessState:(BOOL)state
{
    inSnapProcess = state;
}

- (void)setButtonState:(BOOL)state
{
    activeButtonState = state;
    
    if (buttonType == PDCameraButtonTypeHydro)
    {
        hydroButton.button.enabled = activeButtonState;
    }
    else
    {
        simpleButton.enabled = activeButtonState;
    }
}

- (void)processAccelerometerData:(CMAccelerometerData *)accelerometerData
{
    if (buttonType == PDCameraButtonTypeHydro)
    {
        // формула для вычисления угла устройства в общем случае выглядит так:
        double cur_device_angle = atan(accelerometerData.acceleration.z / sqrt(accelerometerData.acceleration.x * accelerometerData.acceleration.x + accelerometerData.acceleration.y * accelerometerData.acceleration.y)) * 180./M_PI;
        
        if (btn_i_angle_deque < btn_n_deque_size) {
            btn_angle_deque[btn_i_angle_deque] = cur_device_angle;
            btn_i_angle_deque++;
        }
        
        if (btn_i_angle_deque >= btn_n_deque_size) {
            btn_i_angle_deque = 0;
        }
        
        if (btn_angle_deque_size < btn_n_deque_size) {
            btn_angle_deque_size++;
        }
        
        if (btn_angle_deque_size > 0) {
            double angle_deque_sum = 0.;
            int i = 0;
            
            for (i = 0; i < btn_angle_deque_size; i++) {
                angle_deque_sum += btn_angle_deque[i];
            }
            
            device_angle = angle_deque_sum / btn_angle_deque_size;
        }
        
        //           bubble code
        double device_angle_z_l = accelerometerData.acceleration.z;
        
        if (device_angle_z_l < -1.0) {
            device_angle_z_l = -1.0;
        } else if (device_angle_z_l > 1.0) {
            device_angle_z_l = 1.0;
        }
        
        double angle = fabs(acos(device_angle_z_l) * 180.0/M_PI);
        double angle_bubble = 9. * (angle - 80.);
        
        if (angle_bubble < 0.) {
            angle_bubble = 0.;
        } else if (angle_bubble > 180.) {
            angle_bubble = 180.;
        }
        
        if (btn_i_angle_deque_bubble < btn_n_deque_size_bubble) {
            btn_angle_deque_bubble[btn_i_angle_deque] = angle_bubble;
            btn_i_angle_deque_bubble++;
        }
        
        if (btn_i_angle_deque_bubble >= btn_n_deque_size_bubble) {
            btn_i_angle_deque_bubble = 0;
        }
        
        if (btn_angle_deque_size_bubble < btn_n_deque_size_bubble) {
            btn_angle_deque_size_bubble++;
        }
        
        if (btn_angle_deque_size_bubble > 0) {
            double angle_deque_sum = 0.;
            int i = 0;
            
            for (i = 0; i < btn_angle_deque_size_bubble; i++) {
                angle_deque_sum += btn_angle_deque_bubble[i];
            }
            
            double cur_angle_bubble = angle_deque_sum / btn_angle_deque_size_bubble;
            
            if (cur_angle_bubble >= 0. && cur_angle_bubble <= 180.) {
                angle_bubble = cur_angle_bubble;
            }
        }
        
        if (btn_previouse_angle_bubble > 0.) {
            
            double delta = fabs(btn_previouse_angle_bubble - angle_bubble) / 3;
            
            if (delta < 1.) {
                delta = 1.;
            }
            
            if (btn_previouse_angle > angle_bubble + delta) {
                angle_bubble = btn_previouse_angle_bubble - delta;
            } else if (btn_previouse_angle_bubble < angle_bubble - delta) {
                angle_bubble = btn_previouse_angle_bubble + delta;
            }
        }
        
        if (angle_bubble < 0.) {
            angle_bubble = 0.;
        } else if (angle_bubble > 180.) {
            angle_bubble = 180.;
        }
        
        btn_previouse_angle_bubble = angle_bubble;
        
        if (fabs(90. - acos(accelerometerData.acceleration.z) * 180./M_PI) > [PD_MANAGER getMaximumAllowedAngle]
            || fabs(accelerometerData.acceleration.z) > 1.0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [hydroButton setAngle:angle_bubble];
                
                if (!inSnapProcess)
                {
                    hydroButton.button.enabled = NO;
                }
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [hydroButton setAngle:angle_bubble];
                
                if (!inSnapProcess)
                {
                    hydroButton.button.enabled = YES;
                }
                
                if (!activeButtonState)
                {
                    hydroButton.button.enabled = NO;
                }
            });
        }
    }
}

- (void)dealloc
{
    if (buttonType == PDCameraButtonTypeHydro)
    {
        [hydroButton removeFromSuperview];
        
        hydroButton = nil;
    }
    else
    {
        [simpleButton removeFromSuperview];
        
        simpleButton = nil;
    }
}

@end
