
#import "PDMTouchView.h"

@implementation PDMTouchView

@synthesize delegate;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    if (delegate && [delegate respondsToSelector:@selector(capturePressed)])
    {
        [delegate capturePressed];
    }
}

- (void)snapProcess:(BOOL)snap
{
    if (snap)
        self.backgroundColor = [UIColor whiteColor];
    else
        self.backgroundColor = [UIColor clearColor];
}

@end
