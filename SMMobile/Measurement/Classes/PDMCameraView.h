#import "GLKView_SmartMirror.h"

@interface PDMCameraView : GLKView_SmartMirror

//-( void )takePhoto : ( photoCallback )callback;

-( void )resetPhoto;

-( void )setFrontCamera : ( BOOL )frontCamera;
-( void )setCameraScaleForBack : ( float )scaleBack andForFront : ( float )scaleFront;

-( UIImage * )snapshot;

@end
