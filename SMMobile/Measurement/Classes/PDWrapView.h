//
//  PDWrapView.h
//  Visioner_4.0
//
//  Created by Dgut on 12.12.13.
//
//

#import "GLKViewTemplate.h"

@interface PDWrapView : GLKViewTemplate

-( void )shot;
-( void )reset;

@property ( nonatomic, weak ) IBOutlet UILabel * resultLabel;

@property (nonatomic, assign) float resultAngle;

@end
