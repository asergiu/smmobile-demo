//
//  PDFTPSelectPopover.h
//  PDMeasurement
//
//  Created by VS on 04.10.12.
//

#import <UIKit/UIKit.h>

@protocol PDFTPSelectPopoverDelegate<NSObject>
@optional
- (void)ftpPopoverDidSelectElementAtIndex:(NSInteger)index;
- (void) dismissFTPPopover;
@end

@interface PDFTPSelectPopover : UITableViewController
{
   __unsafe_unretained id <PDFTPSelectPopoverDelegate> delegate;
    NSDictionary * currentFtpDictonary;
}

@property (nonatomic,assign) id <PDFTPSelectPopoverDelegate> delegate;

- (id)initWithFTPDictonary:(NSDictionary*)dictonary;

@end
