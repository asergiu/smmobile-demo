//
//  PDPermeableTouchesView.m
//  PD-Measurement
//
//  Created by Pavel Stoma on 6/1/12.
//

#import "PDPermeableTouchesView.h"

@implementation PDPermeableTouchesView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    for (UIView *foundView in self.subviews) {
        if ([foundView isKindOfClass:[UIView class]]) {
            UIView *foundedView = foundView;
            if ([foundedView pointInside:[self convertPoint:point toView:foundedView] withEvent:event] && (foundedView.tag != 7)) {
                return YES;
            }
        }        
    }
    return NO;
}

@end
