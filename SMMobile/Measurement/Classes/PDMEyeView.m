#import "PDMEyeView.h"
#import <QuartzCore/QuartzCore.h>
#import "PDMArrow.h"

@interface PDMEyeView ()
@property (strong, nonatomic) UILabel *verticalLabel;
@property (strong, nonatomic) UILabel *horizontalLabel;
@property (strong, nonatomic) UIImageView *verticalLine;
@property (strong, nonatomic) UIImageView *verticalUpArrow;
@property (strong, nonatomic) UIImageView *verticalDownArrow;
@property (strong, nonatomic) UIImageView *horizontalLine;
@property (strong, nonatomic) UIImageView *horizontalLeftArrow;
@property (strong, nonatomic) UIImageView *horizontalRightArrow;
@property (strong, nonatomic) PDMArrow *lineVertical;
@property (strong, nonatomic) PDMArrow *lineHorizontal;

- (void)localInit;
@end

@implementation PDMEyeView
@synthesize verticalLabel;
@synthesize horizontalLabel;
@synthesize verticalLine;
@synthesize verticalUpArrow;
@synthesize verticalDownArrow;
@synthesize horizontalLine;
@synthesize horizontalLeftArrow;
@synthesize horizontalRightArrow;
@synthesize lineVertical;
@synthesize lineHorizontal;

@synthesize eyeType;
@synthesize eyePoint;
@synthesize convertK;
@synthesize eyePD;
@synthesize eyeHeight;

#pragma mark - Init Methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self localInit];
    }
    return self;
}

- (void)awakeFromNib {
    
    [self localInit];

}

#pragma mark - Draw Methods

- (void)drawRect:(CGRect)rect
{
//    CGFloat eyeX = eyePoint.x - self.frame.origin.x;
//    CGFloat eyeY = eyePoint.y - self.frame.origin.y;
////    CGFloat offset = 10.0;
//    
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetLineWidth(context, 1.0); 
//    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
//    
//    // Вертикальная линия
//    [lineVertical drawWithPoint:CGPointMake(eyeX - (verticalLine.frame.size.width / 2), eyeY) andLen:self.frame.size.height - eyeY];

//    [verticalLine setFrame:CGRectMake(eyeX - (verticalLine.frame.size.width / 2), eyeY + offset, verticalLine.frame.size.width, self.frame.size.height - eyeY - offset * 2)];
//
//    // Стрелка вверх
//    [verticalUpArrow setFrame:CGRectMake(eyeX - (verticalUpArrow.frame.size.width / 2), eyeY, verticalUpArrow.frame.size.width, verticalUpArrow.frame.size.height)];
//
//    // Стрелка вниз
//    [verticalDownArrow setFrame:CGRectMake(eyeX - (verticalDownArrow.frame.size.width / 2), self.frame.size.height - verticalDownArrow.frame.size.height, verticalDownArrow.frame.size.width, verticalDownArrow.frame.size.height)];
    
//    // Текст для вертикальной линии
//    verticalLabel.center = CGPointMake(eyeX - ((eyeType == PDMEyeTypeRight) ? 30 : -30), eyeY + (self.frame.size.height - eyeY) / 2);
//    verticalLabel.text = [NSString stringWithFormat:@"%.1f",eyeHeight];//(self.frame.size.height - eyeY) / convertK];
//    
    // Горизонтальная линия
//    [lineHorizontal drawWithPoint:CGPointMake(0, eyeY - (horizontalLine.frame.size.height / 2)) andLen:self.frame.size.width - eyeX];
    
//    [horizontalLine setFrame:CGRectMake(eyeX + ((eyeType == PDMEyeTypeRight) ? offset : -1 * offset), eyeY - (horizontalLine.frame.size.height / 2), (eyeType == PDMEyeTypeRight) ? (self.frame.size.width - eyeX) - offset * 2 : eyeX * -1 + offset, horizontalLine.frame.size.height)];
//
//    // Стрелка влево
//    [horizontalLeftArrow setFrame:CGRectMake(((eyeType == PDMEyeTypeRight) ? eyeX : 0), eyeY - (horizontalLeftArrow.frame.size.height / 2), horizontalLeftArrow.frame.size.width, horizontalLeftArrow.frame.size.height)];
//
//    // Стрелка вправо
//    [horizontalRightArrow setFrame:CGRectMake((eyeType == PDMEyeTypeRight) ? self.frame.size.width - horizontalRightArrow.frame.size.width : eyeX - horizontalRightArrow.frame.size.width, eyeY - (horizontalLeftArrow.frame.size.height / 2), horizontalLeftArrow.frame.size.width, horizontalLeftArrow.frame.size.height)];
//
//    
//    
//    // Текст для горизонтальной линии
//    horizontalLabel.center = CGPointMake(((eyeType == PDMEyeTypeRight) ? eyeX + (self.frame.size.width - eyeX) / 2 : eyeX / 2),  eyeY - 20);
//    NSLog(@"%@",NSStringFromCGPoint(horizontalLabel.center));
//    horizontalLabel.text = [NSString stringWithFormat:@"%.1f",eyePD];//(self.frame.size.width - eyeX) / convertK];

}

#pragma mark - Local init methods

- (void)localInit {
    
//    self.layer.borderColor = [UIColor yellowColor].CGColor;
//    self.layer.borderWidth = 1.0;    
//    
//    lineVertical = [[PDMArrow alloc] initWithType:PDMArrowTypeVertical];
//    [self addSubview:lineVertical];
//
//    lineHorizontal = [[PDMArrow alloc] initWithType:PDMArrowTypeHorizontal];
//    [self addSubview:lineHorizontal];
    
    
//    verticalLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lineVertical.png"]]; 
//    [verticalLine setFrame:CGRectMake(0, 0, 13.5, 1.5)];
//    [self addSubview:verticalLine];
//
//    horizontalLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lineHorizontal.png"]]; 
//    [horizontalLine setFrame:CGRectMake(0, 0, 1.5, 13.5)];
//    [self addSubview:horizontalLine];
//    
//    verticalUpArrow = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 13.5, 13.5)];
//    [verticalUpArrow setImage:[UIImage imageNamed:@"arrowUp@x2.png"]];
//    [self addSubview:verticalUpArrow];
//    
//    verticalDownArrow = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 13.5, 13.5)];
//    [verticalDownArrow setImage:[UIImage imageNamed:@"arrowDown@x2.png"]];
//    [self addSubview:verticalDownArrow];
//
//    horizontalLeftArrow = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 13.5, 13.5)];
//    [horizontalLeftArrow setImage:[UIImage imageNamed:@"arrowLeft@x2.png"]];
//    [self addSubview:horizontalLeftArrow];
//
//    horizontalRightArrow = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 13.5, 13.5)];
//    [horizontalRightArrow setImage:[UIImage imageNamed:@"arrowRight@x2.png"]];
//    [self addSubview:horizontalRightArrow];
    
    
    
//    verticalLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
//    [verticalLabel setBackgroundColor:[UIColor clearColor]];
//    [verticalLabel setTextAlignment:UITextAlignmentCenter];
//    [verticalLabel setText:@""];
//    [verticalLabel setShadowColor:[UIColor blackColor]];
//    [verticalLabel setTextColor:[UIColor whiteColor]];
//    [verticalLabel setFont:[UIFont boldSystemFontOfSize:22.0]];
//    [self addSubview:verticalLabel];
//    
//    horizontalLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
//    [horizontalLabel setBackgroundColor:[UIColor clearColor]];
//    [horizontalLabel setTextAlignment:UITextAlignmentCenter];
//    [horizontalLabel setText:@""];
//    [horizontalLabel setShadowColor:[UIColor blackColor]];
//    [horizontalLabel setTextColor:[UIColor whiteColor]];
//    [horizontalLabel setFont:[UIFont boldSystemFontOfSize:22.0]];
//    [self addSubview:horizontalLabel];   
    
}

@end