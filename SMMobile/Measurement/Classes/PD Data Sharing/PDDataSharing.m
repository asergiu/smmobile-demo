//
//  PDDataSharing.m
//
//  Created by Oleg Bogatenko on 21/05/2014.
//
//

#import "PDDataSharing.h"
#import <UIKit/UIKit.h>

NSString *PDReadPasteboardDataQuery = @"ReadPasteboardData";

NSString *const PDDataSharingErrorDomain = @"AppDataSharingErrorDomain";
NSString *const LPTAppURL = @"lptapp";

@interface PDDataSharing ()

+ (NSURL *)sendPasteboardDataURLForScheme:(NSString *)scheme pasteboardName:(NSString *)pasteboardName;

@end

@implementation PDDataSharing

+ (void)handleSendPasteboardDataURL:(NSURL *)sendPasteboardDataURL
                  completionHandler:(PDDataSharingHandler)completionHandler
{
    NSError *error = nil;
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    
    NSData *data;
    
    if (pasteboard)
    {
        data = [pasteboard dataForPasteboardType:UIPasteboardNameGeneral];
        
        if (!data)
        {
            NSDictionary *errorInfoDictionary = @{ NSLocalizedDescriptionKey: [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"No data found on pasteboard with name:", nil), UIPasteboardNameGeneral] };
            
            error = [NSError errorWithDomain:PDDataSharingErrorDomain
                                        code:PDNoDataFound
                                    userInfo:errorInfoDictionary];
        }
        
        [pasteboard setData:nil forPasteboardType:UIPasteboardNameGeneral];
    }
    else
    {
        NSDictionary *errorInfoDictionary = @{ NSLocalizedDescriptionKey: [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"No pasteboard found for name:", nil), UIPasteboardNameGeneral] };
        
        error = [NSError errorWithDomain:PDDataSharingErrorDomain
                                    code:PDNoPasteboard
                                userInfo:errorInfoDictionary];
    }
    
    completionHandler(data, error);
}

+ (void)sendDataToApplicationWithScheme:(NSString *)scheme
                            dataPackage:(NSData *)data
                      completionHandler:(PDDataSharingSendDataHandler)completionHandler
{
    NSError *error = nil;
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    
    [pasteboard setPersistent:YES];
    
    [pasteboard setData:data forPasteboardType:UIPasteboardNameGeneral];
    
    NSURL *sendingURL = [[self class] sendPasteboardDataURLForScheme:scheme pasteboardName:UIPasteboardNameGeneral];
    
    if ([[UIApplication sharedApplication] canOpenURL:sendingURL])
    {
        completionHandler(YES, nil);
        
        [[UIApplication sharedApplication] openURL:sendingURL];
    }
    else
    {
        [pasteboard setData:nil forPasteboardType:UIPasteboardNameGeneral];
        [pasteboard setPersistent:NO];
        
        NSDictionary *errorInfoDictionary = @{ NSLocalizedDescriptionKey: [NSString stringWithFormat:@"%@ %@",
                                                                          NSLocalizedString(@"No application was found to handle the url:", nil), sendingURL] };
        
        error = [NSError errorWithDomain:PDDataSharingErrorDomain
                                    code:PDNoApplication
                                userInfo:errorInfoDictionary];
    }
    
    completionHandler(NO, error);
}

#pragma mark - URLs

+ (NSURL *)sendPasteboardDataURLForScheme:(NSString *)scheme pasteboardName:(NSString *)pasteboardName
{
    NSString *urlString = [NSString stringWithFormat:@"%@://?%@#%@", scheme, PDReadPasteboardDataQuery, pasteboardName];
    
    return [NSURL URLWithString:urlString];
}

+ (NSString *)targetScheme
{
    return LPTAppURL;
}

@end
