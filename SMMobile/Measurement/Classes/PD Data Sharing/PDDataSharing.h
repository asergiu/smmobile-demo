//
//  PDDataSharing.h
//
//  Created by Oleg Bogatenko on 21/05/2014.
//
//

#import <Foundation/Foundation.h>

typedef void(^PDDataSharingSendDataHandler)(BOOL sent, NSError *error);
typedef void(^PDDataSharingHandler)(NSData *data, NSError *error);

typedef NS_ENUM(NSInteger, PDDataSharingErrorType)
{
    PDNoApplication = 100,
    PDNoPasteboard  = 200,
    PDNoDataFound   = 300
};

extern NSString *PDReadPasteboardDataQuery;

@interface PDDataSharing : NSObject

+ (void)handleSendPasteboardDataURL:(NSURL *)sendPasteboardDataURL
                  completionHandler:(PDDataSharingHandler)completionHandler;

+ (void)sendDataToApplicationWithScheme:(NSString *)scheme
                            dataPackage:(NSData *)data
                      completionHandler:(PDDataSharingSendDataHandler)completionHandler;

+ (NSString *)targetScheme;

@end
