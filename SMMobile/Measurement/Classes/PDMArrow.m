#import "PDMArrow.h"
#import "UIImage+Border.h"
#import <QuartzCore/QuartzCore.h>

static CGFloat offset = 12.0;
static int arrowMultiplier = 1;

@interface PDMArrow ()

@property PDMArrowType arrowType;
@property (strong, nonatomic) UIView *imageViewBackground;
@property (strong, nonatomic) UIImageView *imageViewArrowStart;
@property (strong, nonatomic) UIImageView *imageViewArrowEnd;

@property (nonatomic, strong) UIView *upView;
@property (nonatomic, strong) UIView *downView;
@property (nonatomic, strong) UIView *leftView;
@property (nonatomic, strong) UIView *rightView;

@end

@implementation PDMArrow
@synthesize arrowType;
@synthesize imageViewBackground;
@synthesize imageViewArrowStart;
@synthesize imageViewArrowEnd;

@synthesize upView;
@synthesize downView;
@synthesize leftView;
@synthesize rightView;


- (id)initWithType:(PDMArrowType)type {
    self = [super initWithFrame:CGRectMake(0, 0, 0, 0)];
    if (self) {
        arrowType = type;
        
        if (arrowType == PDMArrowTypeVertical) {
            
            // Стрелка в начале
            imageViewArrowStart = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10 * arrowMultiplier, 14 * arrowMultiplier)];
            [imageViewArrowStart setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"arrowUpResults@2x.png" ofType:nil]]];
            [self addSubview:imageViewArrowStart];
            
            // Стрелка в конце
            imageViewArrowEnd = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10 * arrowMultiplier, 14 * arrowMultiplier)];
            [imageViewArrowEnd setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"arrowDownResults@2x.png" ofType:nil]]];
            [self addSubview:imageViewArrowEnd];
            
            // Фон
            //            imageViewBackground = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"lineVertical@2x.png" ofType:nil]]]; 
            //            [imageViewBackground setFrame:CGRectMake(0, 0, 10, 2)];
            //            [self addSubview:imageViewBackground];
            
        } else {
            
            // Стрелка в начале
            imageViewArrowStart = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 14 * arrowMultiplier, 10 * arrowMultiplier)];
            [imageViewArrowStart setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"arrowLeftResults@2x.png" ofType:nil]]];
            [self addSubview:imageViewArrowStart];
            
            // Стрелка в конце
            imageViewArrowEnd = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 14 * arrowMultiplier, 10 * arrowMultiplier)];
            [imageViewArrowEnd setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"arrowRightResults@2x.png" ofType:nil]]];
            [self addSubview:imageViewArrowEnd];
            
            // Фон
            
            //            imageViewBackground = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"lineHorizontal@2x.png" ofType:nil]]]; 
            //            [imageViewBackground setFrame:CGRectMake(0, 0, 2, 10)];
            //            [self addSubview:imageViewBackground];
        }
        imageViewBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 2 * arrowMultiplier, 2 * arrowMultiplier)];
        [imageViewBackground setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:imageViewBackground];
    }
    return self;
}

- (void)drawWithPoint:(CGPoint)point andLen:(CGFloat)len {
    
    CGRect rect = self.frame;
    rect.origin = arrowType == PDMArrowTypeVertical ? CGPointMake(point.x, point.y + ARROW_HEIGHT) : CGPointMake(point.x + ARROW_HEIGHT, point.y);
    //    rect.size = arrowType == PDMArrowTypeVertical ? CGSizeMake(rect.size.width, rect.size.height - ARROW_HEIGHT) : CGSizeMake(rect.size.width - ARROW_HEIGHT, rect.size.height);
    [self setFrame:rect];
    
    len -= ARROW_HEIGHT;
    
    if (arrowType == PDMArrowTypeVertical) {
        
        [imageViewBackground setFrame:CGRectMake(4 * arrowMultiplier, offset + ARROW_HEIGHT - 6, imageViewBackground.frame.size.width, len - 7 * 2)];
        
        // Стрелка в конце
        [imageViewArrowEnd setFrame:CGRectMake(0, len - imageViewArrowEnd.frame.size.height, imageViewArrowEnd.frame.size.width, imageViewArrowEnd.frame.size.height)];
    } else {
        [imageViewBackground setFrame:CGRectMake(offset + ARROW_HEIGHT - 6, 4 * arrowMultiplier, len - 7 * 2, imageViewBackground.frame.size.height)];
        
        // Стрелка в конце
        [imageViewArrowEnd setFrame:CGRectMake(len - imageViewArrowEnd.frame.size.width, 0, imageViewArrowEnd.frame.size.width, imageViewArrowEnd.frame.size.height)];
        
    }
}

@end
