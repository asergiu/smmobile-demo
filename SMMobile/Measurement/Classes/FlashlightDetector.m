//
//  FlashlightDetector.m
//  FlashLightDetection
//
//  Created by VS on 10.12.12.
//

#import "FlashlightDetector.h"

@interface FlashlightDetector ()
{
    BOOL headsetInOutConnected;
    BOOL withMicrophone;
}

@end

@implementation FlashlightDetector

@synthesize delegate;
@synthesize currentType;

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        headsetInOutConnected = NO;
        withMicrophone = NO;
        
        [self checkAudioConnection:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(checkAudioConnection:)
                                                     name:AVAudioSessionRouteChangeNotification
                                                   object:nil];
    }
    return self;
}

- (void)checkAudioConnection:(NSNotification *)notification
{
    AVAudioSessionRouteDescription *route = [[AVAudioSession sharedInstance] currentRoute];
    
    for (AVAudioSessionPortDescription *desc in [route outputs])
    {
        if ([[desc portType] isEqualToString:AVAudioSessionPortHeadphones])
        {
            headsetInOutConnected = YES;
        }
        
        if ([[desc portType] isEqualToString:AVAudioSessionPortBuiltInSpeaker])
        {
            headsetInOutConnected = NO;
        }
    }
    
    [self checkMicConnection];
}

- (void)checkMicConnection
{
    NSArray *availableInputs = [[AVAudioSession sharedInstance] availableInputs];
    
    for (AVAudioSessionPortDescription *desc in availableInputs)
    {
        if ([desc.portType isEqualToString:AVAudioSessionPortBuiltInMic])
        {
            withMicrophone = NO;
        }
        
        if ([desc.portType isEqualToString:AVAudioSessionPortHeadsetMic])
        {
            withMicrophone = YES;
        }
    }
}

- (void)detectFlashlightType:(BOOL)onlyFlashTypeCheck
{
    currentType = FlashlightTypeNotConnected;
    
    if ([self flashLightConnected])
    {
        if ([self inputWithMicrophone])
            currentType = FlashlightTypeV2;
        else
            currentType = FlashlightTypeV1;
        
        [self performDelegateWithType:currentType];
    }
    else
    {
        if (!onlyFlashTypeCheck)
        {
            [self performDelegateWithType:FlashlightTypeNotConnected];
        }
    }
}

- (BOOL)flashLightConnected
{
    return headsetInOutConnected;
}

- (BOOL)inputWithMicrophone
{
    [self checkMicConnection];
    
    return withMicrophone;
}

- (void)performDelegateWithType:(FlashlightType)type
{
    if (delegate && [delegate respondsToSelector:@selector(flashligtTypeDetected:)])
    {
        [delegate flashligtTypeDetected:type];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
