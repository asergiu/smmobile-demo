//
//  PDMResults.h
//  PD-Measurement
//

#ifndef PD_Measurement_PDMResults_h
#define PD_Measurement_PDMResults_h

//#define  _USE_MATH_DEFINES
//#define fmax max
#include <math.h>

// 110 мм - размер соответствует kFarPD и kNearPD на расстоянии 1000 мм.
const double kPDmm = 110.;
// радиус глазного яблока
const double EyeRadius = 12.5;
// максимальная величина Wrap angle
const double maxWrapAngle = 35.;

const double minLimitWrapAngle = 1. * M_PI / 180.;
const double maxLimitWrapAngle = 23. * M_PI / 180.;
const double minLimitVertex = 9.5;
const double maxLimitVertex = 20.;

const double CorneaRadius = 7.8; // radius of a cornea of an eye
const double DistBetweenCorneaEyeCenters = 4.7; // distance between centers
// Lens base curve
// 4 Base curve: wrap angles from  0-12 degrees
// 6 Base curve: wrap angles from 12-18 degrees
// 8 Base Curve: wrap angles from 15-22 degrees

//const double lensRadius = 250.; // radius lens base curve 4
//const double lensRadius = 167.; // radius lens base curve 6
//const double lensRadius = 125.; // radius lens base curve 8
const double lensBaseRadius[3] = {250., 167., 125.};
const double lensWrapAngle[3] = {12. * M_PI / 180., 18. * M_PI / 180., 22. * M_PI / 180.};

typedef enum PDMPhotoMode
{
    nNoData = 0,
    nFarPD = 1,
    nNearPD = 2,
    nNearPD180 = 3
} PDMPhotoMode;

typedef enum PDMiPadDevice
{
    niPad = 0,
    niPadNew = 1
} PDMiPadDevice;

typedef enum PDMFlashType
{
	nFlashType1 = 0,
	nFlashType2 = 1,
    nFlashType_iPadAir2 = 2,
    nFlashType_iPadPro10 = 3,
    nFlashType_iPadPro12 = 4,
    nFlashType_iPadMini = 5,
    nFlashType_iPadPro11SelfNormal = 6,
    nFlashType_iPadPro11SelfHigh = 7,
    nFlashType_iPadPro12SelfNormal = 8,
    nFlashType_iPadPro12SelfHigh = 9
} PDMFlashType;

typedef enum PDMDevice
{
    nSupport = 0,
    nSonar = 1
} PDMDevice;

typedef enum PDMSupportType
{
	nAcepMetalSupport = 0,
	nAcepPlasticSupport = 1,
	nZeissSupport = 2
} PDMSupportType;

class CPDMSupport
{
public:
	CPDMSupport();
	void SetSupportType(PDMSupportType supType);
	PDMSupportType GetSupportType();
protected:
	PDMSupportType supType;

public:
	// Суппорт вынесен перед оправой
	double SupportDistance;
	double SupportDistanceForfard;
	// расстояние между крайними маркерами Суппорта
	double abDist;
	// половина расстояния между крайними маркерами Суппорта
	double acDist;
	// размер короткой стороны крюка суппорта
	double SupportHookA;
	// размер длинной стороны крюка суппорта
	double SupportHookB;
	// точка крепления суппорта
	double xFastening, yFastening;
};

class xyzPoint
{
public:
	xyzPoint() {
		x = y = z = 0.;
	};
	xyzPoint(const xyzPoint& Data) {
		x = Data.x;
		y = Data.y;
		z = Data.z;
	};
	xyzPoint(double xData, double yData, double zData) {
		x = xData;
		y = yData;
		z = zData;
	};
	double x, y, z;
};

class CPDMPhotoData
{
public:
    CPDMPhotoData();
    // Device type
    PDMiPadDevice nDevice;
    // Photo mode
    PDMPhotoMode nPhotoMode;
    // Support data
    double xLeftSupport;
    double yLeftSupport;
    double xRightSupport;
    double yRightSupport;
    double xCenterSupport;
    double yCenterSupport;
    // Eyes data
    double xRightEye;
    double yRightEye;
    double xLeftEye;
    double yLeftEye;
    // Frame data
    double xLeftRect;
    double yLeftRect;
    double xRightRect;
    double yRightRect;
    double xLeftUpRect;
    double yLeftUpRect;
    double xRightUpRect;
    double yRightUpRect;
    // Device angle
    double DeviceAngle;
    // Support type
    CPDMSupport sup;
    
protected:
    void checkPhotoData();
    double convertToCM(double pixel, double PanAngle, double Dist);
    double convertToPixel(double mm, double Dist);
	double PhotoDistance(double PanAngle);
    double correctPD(double srcPD, double Dist, double VD);
    double CalcHeadTiltAngle();
    double CalcHeadPanAngle();
    double CalcPantoskopicheskyCorner(double PanAngle, double Dist);
    double CalcPantoskopicheskyCorner3(double PanAngle, double Dist);
    double CalcPantoskopicheskyCornerHor(double PanAngle, double Dist, double PantoskAngle);
    double CalcParallax(double PanAngle, double Dist);
    double CalcWrapAngle(double PanAngle, double Dist);
    double CalcCenterPoint(double RightPoint, double LeftPoint, double Dist, double PanAngle);
    bool CalcPointOfIntersection(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double& x, double& y);
    
	double CalcHeadPanAngle2(double PanAngle, double PantoAngle);
    double CalcHeadPanAngle3(double PanAngle, double PantoAngle);
    
    double kFarPD, kNearPD;

public:
    friend class CPDMResults;
    friend class CPDMLineAngle;
};

class CPDMDataResults
{
public:
    CPDMDataResults();
    // Data results presents
    bool bDataResults;
    // alfaTilt - угол наклона головы "вправо - влево"
    double alfaTilt;
    // alfaPan - угол поворота головы "вправо - влево"
    double alfaPan;
    // alfaPantosk - пантоскопический угол
    double alfaPantosk;
    // alfaPantoskHor - пантоскопический угол относительно отвеса
    double alfaPantoskHor;
    // alfaWrap - угол сгиба оправы
    double alfaWrap;
    // Dist - расстояние съемки
    double Dist;
    // PD - межцентровое расстояние
    double LeftPD;
    double RightPD;
    // Высоты
    double LeftHeight;
    double RightHeight;
    // Расстояние между линзами
    double Bridge;
    // Размеры оправы
    double FrameWidth;
    double FrameHeight;
    // VD - vertex distance
    double LeftVD;
    double RightVD;
    // Head rotation correction
    bool bHeadRotation;
    // default Wrap angle
    double defWrapAngle;
    // default Vertex
    double defVertex;
    // Lens center points shift
    double dxLeft, dyLeft, dxRight, dyRight;
    // pixel in mm
    double pixelInMM_dxLeft, pixelInMM_dyLeft, pixelInMM_dxRight, pixelInMM_dyRight;
protected:
    void checkResultsData(int iPhoto);

public:
    friend class CPDMResults;
};

class CPDMDataResults2
{
public:
    CPDMDataResults2();
    // Data results presents
    bool bDataResults;
    // VD - vertex distance
    double LeftVD;
    double RightVD;
    // Wrap angle - угол сгиба оправы
    double WrapAngle;
    double WrapAngleMarkers[5];
	// Размеры оправы
	double FrameWidth;
	double FrameHeight;
    // Progressive corridor
    double Upfit;
    // Inset
    double LeftInset;
    double RightInset;
    // Diameter
    bool bDataDiameter;
    double LeftDiameter;
    double RightDiameter;
    bool bProgressiveDiameter;

	// Data work points presents
	bool bPointsResults;
	// Eyes center points position
	xyzPoint ptEyeCenter[2];
	// Lenses points position
	xyzPoint ptLensCenter[2], ptLensNose[2], ptLensTemple[2], ptWrapMarker[2];

    // Calculate WRAP angle status
    bool bWrapCalc;
    // Calculate Vertex status
    bool bVertexCalc;
protected:
    void checkResults2Data();
    
public:
    friend class CPDMResults;
};

class CPDMDevice
{
public:
	CPDMDevice();
	void SetFlashType(PDMFlashType useFlashType);
    PDMFlashType GetFlashType();
    void SetPhotoMode(PDMPhotoMode nPhotoMode);
    void UpdateData();
    // Flash type
    PDMFlashType nFlashType;
    double zFarFlash, yFarFlash, xFarFlash;
	double zNearFlash, yNearFlash;
	double zRedCircle;
	double zDeviceCenter;
    // Photo mode
    PDMPhotoMode nFarPhotoMode;
    PDMPhotoMode nNearPhotoMode;
};

class CPDMResults
{
public:
    CPDMResults();
    
    void NewSession();
    void SetPhotoData(int iPhoto, PDMiPadDevice nDevice, PDMPhotoMode nPhotoMode, double xLeftSupport, double yLeftSupport, double xRightSupport, double yRightSupport, double xCenterSupport, double yCenterSupport, double xRightEye, double yRightEye, double xLeftEye, double yLeftEye, double xLeftRect, double yLeftRect, double xRightRect, double yRightRect, double dxLeftRect, double dyLeftRect, double dxRightRect, double dyRightRect, double xLeftUpRect, double yLeftUpRect, double xRightUpRect, double yRightUpRect, double dxLeftUpRect, double dyLeftUpRect, double dxRightUpRect, double dyRightUpRect, double DeviceAngle, PDMSupportType supType, bool bEnableCorrection = true);
    bool CalcPhotoResults(int iPhoto);
    CPDMDataResults GetPhotoResults(int iPhoto);
    bool CalcPhotoResults2(bool bUseNearPhoto = false);
    CPDMDataResults2 GetPhotoResults2();
    double convertToCM(int iPhoto, double pixel);
    CPDMPhotoData GetPhotoData(int iPhoto);
    void SetHeadRotationCorrection(bool bHeadRotation);
    void SetNearCorrection(bool bNearCorrection);
	void SetNearEmulation(bool bNearEmulation);
    void SetFlashType(PDMFlashType nFlashType);
    PDMFlashType GetFlashType();
    double GetFcoef(int iPhoto);

	static void CalcLineCoefficients(xyzPoint ptFirst, xyzPoint ptSecond, double& kX, double& bX, double& kZ, double& bZ);
	xyzPoint ConvertPointToProjectionPlane(xyzPoint pt, double PantoAngle);
    xyzPoint CalcMinDistPoint(xyzPoint pt11, xyzPoint pt12, xyzPoint pt21, xyzPoint pt22);

    void SetDiameter(bool bLeft, double Diam);
    double GetDiameter(bool bLeft);
    bool GetDiameterStatus();
    void SetProgressiveDiameterStatus(bool bProgressiveStatus);
    bool GetProgressiveDiameterStatus();

    // WRAP angle functional
    void SetWrapAngle(bool bWrapCalc, double WrapAngle);
    // Vertex functional
    void SetVertex(bool bVertexCalc, double dblVertex);
    // default Vertex
    void SetDefVertex(double dblVertex);
    
    // Corrected Up fit
    double GetCorrectedUpfit(double minNearHeight);

protected:
    bool CalcPhotoResults_xy(int iPhoto);
    bool CalcPhotoResults_xyz(int iPhoto, bool bCalculation = false);

protected:
    CPDMPhotoData Photo[3];
    CPDMDataResults Results[3];
    CPDMDataResults2 Results2;
	CPDMDevice Device;
    // Head rotation correction
    bool bHeadRotation;
    // Near correction using Far result
    bool bNearCorrection;
	// Near emulation using Far result
	bool bNearEmulation;

    bool CalcVD2();
    bool CalcWrapAngle2();
    bool CalcFrameSize(int iPhoto);

	bool CalcVD3_far_near();
    bool CalcWrapAngle3_far_near();
    
    bool CalcCorneaReflexCorrection_far(double& leftCor, double& rightCor);
	double GetLensBaseRadius(double angleWrap);
};

class CPDMLineAngle
{
public:
    CPDMLineAngle();
    
    void SetPhotoData(PDMiPadDevice nDevice, PDMPhotoMode nPhotoMode, double xLeftSupport, double yLeftSupport, double xRightSupport, double yRightSupport, double xCenterSupport, double yCenterSupport, PDMSupportType supType, double DeviceAngle, bool bEnableCorrection = true);
    double CalcLineAngle(double xPoint, double yPoint);
    double GetPantoskHor();
    double GetPantoskAngle();
    double GetPanAngle();
    void SetPDMDevice(PDMDevice pdm_device);

protected:
    bool CalcPhotoResults();
    
	CPDMSupport sup;
	// Data results presents
    bool bDataResults;
    // alfaTilt - угол наклона головы "вправо - влево"
    double alfaTilt;
    // alfaPan - угол поворота головы "вправо - влево"
    double alfaPan;
    // alfaPantosk - пантоскопический угол
    double alfaPantosk;
    // alfaPantoskHor - пантоскопический угол относительно отвеса
    double alfaPantoskHor;
    // Dist - расстояние съемки
    double Dist;
    // pdmDevice - инструмент для измерения
    PDMDevice pdmDevice;
    
    double xObjective, yObjective;
    double xCP1, yCP1, xCP4, yCP4;
    double xlSupport, ylSupport, xrSupport, yrSupport;

    CPDMPhotoData Photo;
};

class CPDMFrameMarkers
{
public:
    CPDMFrameMarkers();
    void SetMarkersPosition(int iPhoto, double xLeftMarker1, double yLeftMarker1, double xLeftMarker2, double yLeftMarker2, double xLeftMarker3, double yLeftMarker3, double xRightMarker1, double yRightMarker1, double xRightMarker2, double yRightMarker2, double xRightMarker3, double yRightMarker3);
    void GetMarkersPosition(int iPhoto, double& xLeftMarker1, double& yLeftMarker1, double& xLeftMarker2, double& yLeftMarker2, double& xLeftMarker3, double& yLeftMarker3, double& xRightMarker1, double& yRightMarker1, double& xRightMarker2, double& yRightMarker2, double& xRightMarker3, double& yRightMarker3);
    void SetMarkersPosition(int iPhoto, bool bLeft, int iMarker, double xPoint, double yPoint, bool bEnableCorrection = true);
    void GetMarkersPosition(int iPhoto, bool bLeft, int iMarker, double& xPoint, double& yPoint, bool bEnableCorrection = true);
    void NewSession();
    void NewPhoto(int iPhoto);
protected:
    double xLeftMarker[3][3];
    double yLeftMarker[3][3];
    double xRightMarker[3][3];
    double yRightMarker[3][3];
    
public:
    friend class CPDMResults;
};

extern CPDMFrameMarkers frameMarkers;

class CPDMCalibrationData
{
public:
    CPDMCalibrationData()
    {
        frontZoom = backZoom = 0.;
        dxPhotoImage = dyPhotoImage = 0.;
        dxCropPhoto = dyCropPhoto = 0.;
        dxPhoto = dyPhoto = 0.;
        deviceModel = bigNumber = smallNumber = 0;
    };
    double frontZoom, backZoom;
    double dxPhotoImage, dyPhotoImage;
    double dxCropPhoto, dyCropPhoto;
    double dxPhoto, dyPhoto;
    int deviceModel, bigNumber, smallNumber;
};

#endif
