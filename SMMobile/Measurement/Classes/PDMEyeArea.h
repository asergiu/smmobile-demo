#import <UIKit/UIKit.h>
#import "PDMArrow.h"

@interface PDMEyeArea : UIView {
@private
    UIView *leftHoriz;
    UIView *leftVert;
    UIView *leftHorizRect;
    UIView *leftVertRect;
    UIView *rightHoriz;
    UIView *rightVert;
    UIView *rightHorizRect;
    UIView *rightVertRect;
    UIView *centerVert;
    PDMArrow *leftPD;
    UILabel *leftPDText;
    PDMArrow *rightPD;
    UILabel *rightPDText;
    PDMArrow *leftHeight;
    UILabel *leftHeightText;
    PDMArrow *rightHeight;
    UILabel *rightHeightText;
    PDMArrow *bridge;
    UILabel *bridgeText;
}

@property CGPoint leftEye;
@property CGPoint rightEye;
@property CGPoint leftRect;
@property CGPoint rightRect;

@property CGFloat currentZoomScale;
@property CGFloat scrollOffsetX;
@property CGFloat scrollOffsetY;

@property (strong, nonatomic) NSString *leftPDString;
@property (strong, nonatomic) NSString *rightPDString;
@property (strong, nonatomic) NSString *leftHeightString;
@property (strong, nonatomic) NSString *rightHeightString;
@property (strong, nonatomic) NSString *bridgeString;

@property (assign, nonatomic) CGPoint leftPdPosition;
@property (assign, nonatomic) CGFloat leftPdLength;

@property (assign, nonatomic) CGPoint rightPdPosition;
@property (assign, nonatomic) CGFloat rightPdLength;

@property (assign, nonatomic) CGPoint leftHeightPosition;
@property (assign, nonatomic) CGFloat leftHeightLength;

@property (assign, nonatomic) CGPoint rightHeightPosition;
@property (assign, nonatomic) CGFloat rightHeightLength;

@property (assign, nonatomic) CGPoint bridgePosition;
@property (assign, nonatomic) CGFloat bridgeLength;

- (void)refresh;
@end
