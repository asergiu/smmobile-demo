//
//  DoubleTextAlertView.h
//  123123
//
//  Created by Владимир Малашенков on 05.04.13.
//

#import <UIKit/UIKit.h>
#import "ParametrizedAlert.h"

@interface DoubleTextAlertView : ParametrizedAlert

@property (nonatomic, retain) UITextField *textFieldFirstName;
@property (nonatomic, retain) UITextField *textFieldLastName;

@end
