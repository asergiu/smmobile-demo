//
//  SingleTextWithButtonAlertView.m
//  SeikoMultiTool
//
//  Created by Владимир Малашенков on 27.08.13.
//
//

#import "SingleTextWithButtonAlertView.h"

@implementation SingleTextWithButtonAlertView

- (void) createView
{
    [self addFields];
}

- (void) addFields
{
    _textFieldStoreId = [self createTextField];
    [_textFieldStoreId setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    
	[self addSubview:_textFieldStoreId];
    
    [self addLabelButton];
}

- (void) addLabelButton
{
    _labelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _labelButton.frame = CGRectMake(12.0, _lastFrameBottomY, 260.0, 25.0);
    
    [_labelButton.titleLabel setNumberOfLines:2];
    
    [_labelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_labelButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    _labelButton.titleLabel.font = [UIFont systemFontOfSize:13.0];
    
    [_labelButton addTarget:self action:@selector(dismissAlert) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:_labelButton];
}

- (void) dismissAlert
{
    [self dismissWithClickedButtonIndex:self.cancelButtonIndex animated:YES];
}

- (float) labelHeight
{
    return 75.0;
}

@end
