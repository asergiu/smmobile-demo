//
//  SingleTextWithButtonAlertView.h
//  SeikoMultiTool
//
//  Created by Владимир Малашенков on 27.08.13.
//
//

#import "ParametrizedAlert.h"

@interface SingleTextWithButtonAlertView : ParametrizedAlert

@property (nonatomic, retain) UITextField *textFieldStoreId;
@property (nonatomic, retain) UIButton *labelButton;

@end
