//
//  ParametrizedAlert.m
//  SeikoMultiTool
//
//  Created by Владимир Малашенков on 26.08.13.
//
//

#import "ParametrizedAlert.h"

#define kElementVerticalShift 10.0f

@implementation ParametrizedAlert

- (id) initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    self = [super initWithTitle:title message:@"\n\n\n" delegate:delegate cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles, nil];
    if (self)
    {
        _lastFrameBottomY = [self labelHeight];
        
        [self createView];
    }
    return self;
}

- (void) createView
{
    
}

- (float) labelHeight
{
    return 50.0;
}

- (UITextField*) createTextField
{
    UITextField * textField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, _lastFrameBottomY, 260.0, 25.0)];
	[textField setBackgroundColor:[UIColor whiteColor]];
    
    [self setLastFrameBottomYForView:textField];
    
    return textField;
}

- (void) setLastFrameBottomYForView:(UIView*)view
{
    _lastFrameBottomY = view.frame.origin.y + view.frame.size.height + kElementVerticalShift;
}

@end
