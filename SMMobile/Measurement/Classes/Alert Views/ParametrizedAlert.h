//
//  ParametrizedAlert.h
//  SeikoMultiTool
//
//  Created by Владимир Малашенков on 26.08.13.
//
//

#import <UIKit/UIKit.h>

@interface ParametrizedAlert : UIAlertView
{
    float _lastFrameBottomY;
}

- (UITextField*) createTextField;

@end
