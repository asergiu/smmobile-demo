//
//  DoubleTextAlertView.m
//  123123
//
//  Created by Владимир Малашенков on 05.04.13.
//

#import "DoubleTextAlertView.h"

@implementation DoubleTextAlertView
@synthesize textFieldFirstName, textFieldLastName;

- (void) createView
{
    [self addFields];
}

- (void) addFields
{    
    textFieldFirstName = [self createTextField];
	[self addSubview:textFieldFirstName];
	
	textFieldLastName = [self createTextField];
	[self addSubview:textFieldLastName];
}

@end
