//
//  FlashlightDetector.h
//  FlashLightDetection
//
//  Created by VS on 10.12.12.
//

#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AudioToolbox/AudioToolbox.h>
#import <UIKit/UIKit.h>

typedef int FlashlightType;

enum
{
    FlashlightTypeNotConnected,
    FlashlightTypeV1,
    FlashlightTypeV2
};

@protocol FlashlightDetectorDelegate <NSObject>

@optional
- (void) flashligtTypeDetected:(FlashlightType)type;
@end

@interface FlashlightDetector : NSObject

@property (nonatomic , assign) id<FlashlightDetectorDelegate> delegate;

@property (nonatomic, assign) FlashlightType currentType;

- (BOOL)flashLightConnected;
- (void)detectFlashlightType:(BOOL)onlyFlashTypeCheck;

@end
