#import <UIKit/UIKit.h>

typedef enum PDMEyeType {
    PDMEyeTypeLeft = 0,
    PDMEyeTypeRight
} PDMEyeType;

@interface PDMEyeView : UIView

@property PDMEyeType eyeType;
@property CGPoint eyePoint;
@property CGFloat convertK;
@property CGFloat eyePD;
@property CGFloat eyeHeight;

@end
