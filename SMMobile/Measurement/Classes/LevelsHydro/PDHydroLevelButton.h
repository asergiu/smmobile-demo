//
//  PDHydroLevelButton.h
//  PDMeasurement
//
//  Created by Vitalii Bogdan on 15.11.12.
//
//

#import <UIKit/UIKit.h>

@interface PDHydroLevelButton : UIView
{
   UIImageView * _background;
}

@property (nonatomic, strong) UIImageView * background;
@property (nonatomic, strong) UIImageView * bubble;
@property (nonatomic, strong) UIImageView * topBound;
@property (nonatomic, strong) UIImageView * bottomBound;
@property (nonatomic, strong) UIButton * button;

- (void)setAngle:(NSInteger)angle;

- (CGFloat)allowAngleRange;

@end
