//
//  PDRightHydroLevelButton.m
//  PDMeasurement
//
//  Created by Vitalii Bogdan on 15.11.12.
//
//

#import "PDRightHydroLevelButton.h"
#import "UIView+Additions.h"

static NSInteger kPaddingTopHydroLevel = 58;
static NSInteger kHeightHydroLevel = 140;

@implementation PDRightHydroLevelButton

- (UIImageView *)background {
   if ( !_background ) {
      _background = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"PDHydrolevelRightBackground"]];
      _background.frame = CGRectMake(0, 0, 105, 242);
      _background.userInteractionEnabled= YES;
   }
   return _background;
}


- (void)layoutSubviews {
   [super layoutSubviews];
   
   
   self.background.width = self.width;
   self.background.height = self.height;
   
   self.button.width = 55;
   self.button.height = 169;
   self.button.top = 45;
   self.button.left = self.width - self.button.width - 8;
   
   self.topBound.left = self.bottomBound.left = 8;
   self.topBound.height = self.bottomBound.height = 2;
   self.topBound.width = self.bottomBound.width = 24;
   
   self.topBound.top = ceilf(kPaddingTopHydroLevel + kHeightHydroLevel / 2 - ceilf([self allowAngleRange] * 100 / 180 ) / 2);
   self.bottomBound.top = self.topBound.top + ceilf([self allowAngleRange] * 100 / 180 );
   
   self.bubble.left = self.bottomBound.left + 1;
   self.bubble.top = self.topBound.top - 10;
   
}

@end
