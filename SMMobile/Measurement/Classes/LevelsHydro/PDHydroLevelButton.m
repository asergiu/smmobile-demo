//
//  PDHydroLevelButton.m
//  PDMeasurement
//
//  Created by Vitalii Bogdan on 15.11.12.
//
//

#import "PDHydroLevelButton.h"
#import "UIView+Additions.h"

static NSInteger kPaddingTopHydroLevel = 58;
static NSInteger kHeightHydroLevel = 140;

@implementation PDHydroLevelButton {
   NSInteger _angle;
}

@synthesize background = _background;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if ( self ) {
       [self addSubview:self.background];
       [self.background addSubview:self.button];
       [self.background addSubview:self.bubble];
       [self.background addSubview:self.topBound];
       [self.background addSubview:self.bottomBound];
    }
    return self;
}


- (CGFloat)allowAngleRange {
   return 70;
}


- (void)setAngle:(NSInteger)angle {
   _angle = angle;
   
   [UIView animateWithDuration:0.2 animations:^{
      _bubble.top = kPaddingTopHydroLevel + ceilf(angle * (kHeightHydroLevel - _bubble.height) / 180);
   }];

}


- (UIImageView *)background {
   if ( !_background ) {
      _background = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"PDHydrolevelLeftBackground"]];
      _background.frame = CGRectMake(0, 0, 105, 242);
      _background.userInteractionEnabled= YES;
   }
   return _background;
}


- (UIButton *)button {
   if ( !_button ) {
      _button = [UIButton buttonWithType:UIButtonTypeCustom];
      _button.frame = CGRectMake(0, 0, 100, 100);
      [_button setBackgroundImage:[UIImage imageNamed:@"PDOkBackground"] forState:UIControlStateNormal];
   }
   return _button;
}


- (UIImageView *)topBound {
   if ( !_topBound ) {
      _topBound = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"PDHydrolevelRamkaBackground"]];
      _topBound.frame = CGRectMake(0, 0, 34, 2);
   }
   return _topBound;
}


- (UIImageView *)bubble {
   if ( !_bubble ) {
      _bubble = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"PDBubbleBackground"]];
      _bubble.frame = CGRectMake(0, 0, 22, 22);
   }
   return _bubble;
}


- (UIImageView *)bottomBound {
   if ( !_bottomBound ) {
      _bottomBound = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"PDHydrolevelRamkaBackground"]];
      _bottomBound.frame = CGRectMake(0, 0, 34, 2);
   }
   return _bottomBound;
}


- (void)layoutSubviews {
   [super layoutSubviews];

   
   self.background.width = self.width;
   self.background.height = self.height;
   
   self.button.width = 55;
   self.button.height = 169;
   self.button.top = 45;
   self.button.left = 8;
   
   self.topBound.left = self.bottomBound.left = 72;
   self.topBound.height = self.bottomBound.height = 2;
   self.topBound.width = self.bottomBound.width = 24;
   
   self.topBound.top = ceilf(kPaddingTopHydroLevel + kHeightHydroLevel / 2 - ceilf([self allowAngleRange] * 100 / 180 ) / 2);
   self.bottomBound.top = self.topBound.top + ceilf([self allowAngleRange] * 100 / 180 );
   
   self.bubble.left = self.bottomBound.left + 1;
   self.bubble.top = self.topBound.top - 10;
   
}


@end
