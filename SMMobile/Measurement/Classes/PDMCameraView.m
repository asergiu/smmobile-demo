#import "PDMCameraView.h"

#import "SMColorTrackingCamera.h"

static const int FBO_WIDTH = 320;
static const int FBO_HEIGHT = 240;

@interface PDMCameraView()
{
    GLKTextureInfo * mask;
    GLKTextureInfo * mask_portrait;
    GLKTextureInfo * empty;
    
    GLuint positionRenderTexture0;
    GLuint positionFramebuffer0;
    
    GLuint positionRenderTexture1;
    GLuint positionFramebuffer1;
    
    GLuint gaussianBlurHorizontal;
    GLuint gaussianBlurVertical;
    
    GLuint cameraProgram;
    
    //BOOL photoReady;
    
    float frontCameraScale;
    float backCameraScale;
    
    BOOL shooting;
}

@end

@implementation PDMCameraView

-( id )initWithCoder : ( NSCoder * )aDecoder
{
    self = [ super initWithCoder : aDecoder ];
    if( self )
    {
        self.contentScaleFactor = 1.f;
        
        mask = [ self getTexture : @"wrap-visor-mask.png" ];
        mask_portrait = [ self getTexture : @"wrap-visor-mask-portrait.png" ];
        empty = [ self getTexture : @"empty.png" ];
        
        positionRenderTexture0 = [ self genTexture ];
        positionFramebuffer0 = [ self genFramebufferWithTexture : positionRenderTexture0 width : FBO_WIDTH height : FBO_HEIGHT ];
        
        positionRenderTexture1 = [ self genTexture ];
        positionFramebuffer1 = [ self genFramebufferWithTexture : positionRenderTexture1 width : FBO_WIDTH height : FBO_HEIGHT ];
        
        gaussianBlurHorizontal = [ self getProgram : @"GaussianBlur" : @"GaussianBlurHorizontal2" ];
        gaussianBlurVertical = [ self getProgram : @"GaussianBlur" : @"GaussianBlurVertical2" ];
        
        cameraProgram = [ self getProgram : @"PDMCamera" : @"PDMCamera" ];
        
        glUseProgram( cameraProgram );
        glUniform1i( glGetUniformLocation( cameraProgram, "image" ), 0 );
        glUniform1i( glGetUniformLocation( cameraProgram, "blur" ), 1 );
        glUniform1i( glGetUniformLocation( cameraProgram, "mask" ), 2 );
        
        //cameraScale = 3.2f;
        
        [ self setCameraPreset : AVCaptureSessionPresetHigh ];
    }
    return self;
}

-( void )dealloc
{
    NSLog( @"camera view dealloc" );
}

-( void )drawGaussian
{
    GLKMatrix4 modelViewProjectionMatrix = GLKMatrix4MakeOrtho( 0, self.frame.size.width, 0, self.frame.size.height, -99999., 99999. );
    
    glBindFramebuffer( GL_FRAMEBUFFER, positionFramebuffer0 );
    glViewport( 0, 0, FBO_WIDTH, FBO_HEIGHT );
    glActiveTexture( GL_TEXTURE0 );
    /*if( photoReady )
        glBindTexture( GL_TEXTURE_2D, self.photoTexture );
    else*/
        glBindTexture( GL_TEXTURE_2D, self.videoFrameTexture );
    
    glUseProgram( gaussianBlurHorizontal );
    glUniform1i( glGetUniformLocation( gaussianBlurHorizontal, "background" ), 0 );
    glUniformMatrix4fv( glGetUniformLocation( gaussianBlurHorizontal, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    glUniform1f( glGetUniformLocation( gaussianBlurHorizontal, "dx" ), 1.f / FBO_WIDTH );
    [ self drawRectangle : CGRectMake( 0, 0, self.frame.size.width, self.frame.size.height ) ];
    
    glBindFramebuffer( GL_FRAMEBUFFER, positionFramebuffer1 );
    glViewport( 0, 0, FBO_WIDTH, FBO_HEIGHT );
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, positionRenderTexture0 );
    
    glUseProgram( gaussianBlurVertical );
    glUniform1i( glGetUniformLocation( gaussianBlurVertical, "background" ), 0 );
    glUniformMatrix4fv( glGetUniformLocation( gaussianBlurVertical, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    glUniform1f( glGetUniformLocation( gaussianBlurVertical, "dy" ), 1.f / FBO_HEIGHT );
    [ self drawRectangle : CGRectMake( 0, 0, self.frame.size.width, self.frame.size.height ) ];
}

-( void )drawRect : ( CGRect )rect
{
    [ self drawGaussian ];
    
    [ self bindDrawable ];
    
    glActiveTexture( GL_TEXTURE0 );
    /*if( photoReady )
        glBindTexture( GL_TEXTURE_2D, self.photoTexture );
    else*/
        glBindTexture( GL_TEXTURE_2D, self.videoFrameTexture );
    glActiveTexture( GL_TEXTURE1 );
    glBindTexture( GL_TEXTURE_2D, positionRenderTexture1 );
    glActiveTexture( GL_TEXTURE2 );
    if( shooting )
    {
        glBindTexture( empty.target, empty.name );
        shooting = NO;
    }
    else if( self.frontCamera )
        glBindTexture( mask_portrait.target, mask_portrait.name );
    else
        glBindTexture( mask.target, mask.name );
    
    GLKMatrix4 modelViewProjectionMatrix = GLKMatrix4MakeOrtho( 0, rect.size.width, 0, rect.size.height, -99999., 99999. );
    GLKVector2 screenSize = GLKVector2Make( rect.size.width, rect.size.height );
    GLKVector2 backgroundSize = /*photoReady ? GLKVector2Make( self.photoTextureSize.width, self.photoTextureSize.height ) : */GLKVector2Make( self.videoFrameTextureSize.width, self.videoFrameTextureSize.height );
    
    if( self.frontCamera )
    {
        float temp = backgroundSize.x;
        backgroundSize.x = backgroundSize.y;
        backgroundSize.y = temp;
    }
    
    {
        float scale;
        
        float backScaleX = screenSize.x / backgroundSize.x;
        float backScaleY = screenSize.y / backgroundSize.y;
        
        if( backScaleX > backScaleY )
            scale = backScaleX;
        else
            scale = backScaleY;
        
        backgroundSize = GLKVector2MultiplyScalar( backgroundSize, scale );
    }
    
    if( !self.frontCamera )
        backgroundSize = GLKVector2MultiplyScalar( backgroundSize, backCameraScale );
    else
        backgroundSize = GLKVector2MultiplyScalar( backgroundSize, frontCameraScale );
    
    float swap = 0.f;
    float swapX = 0.f;
    float swapY = 0.f;
    
    if( [ UIApplication sharedApplication ].statusBarOrientation == UIInterfaceOrientationLandscapeLeft )
        swapX = 1.f;
    if( [ UIApplication sharedApplication ].statusBarOrientation == UIInterfaceOrientationLandscapeRight )
        swapY = 1.f;
    if( [ UIApplication sharedApplication ].statusBarOrientation == UIInterfaceOrientationPortrait )
    {
        swap = 1.f;
        swapY = 1.f;
    }
    
    glUseProgram( cameraProgram );
    glUniformMatrix4fv( glGetUniformLocation( cameraProgram, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    glUniform2fv( glGetUniformLocation( cameraProgram, "screenSize" ), 1, screenSize.v );
    glUniform2fv( glGetUniformLocation( cameraProgram, "backgroundSize" ), 1, backgroundSize.v );
    glUniform1f( glGetUniformLocation( cameraProgram, "backgroundSwap" ), swap );
    glUniform1f( glGetUniformLocation( cameraProgram, "backgroundSwapX" ), swapX );
    glUniform1f( glGetUniformLocation( cameraProgram, "backgroundSwapY" ), swapY );
    
    [ self drawRectangle : rect ];
}

/*-( void )takePhoto : ( photoCallback )callback
{
    [ super takePhoto : ^
    {
        photoReady = YES;
        
        if( callback )
            callback();
        
        [ self setNeedsDisplay ];
        [ self stop ];
    } ];
}*/

-( void )resetPhoto
{
    //photoReady = NO;
    [ self play ];
}

-( void )setFrontCamera : ( BOOL )frontCamera
{
    [ super setFrontCamera : frontCamera ];
    
    //cameraScale = frontCamera ? 1.f : /*3.2f*//*3.0672f*/2.4f;
    
    /*if( !frontCamera )
        [ self.camera setFocus : 0.8 ];*/
}

-( void )setCameraScaleForBack : ( float )scaleBack andForFront : ( float )scaleFront;
{
    frontCameraScale = scaleFront;
    backCameraScale = scaleBack;
}

-( UIImage * )snapshot
{
    shooting = YES;
    return [ super snapshot ];
}

@end
