#import "GLKView_SmartMirror.h"

@protocol PDMLinesViewDelegate <NSObject>

@optional
- (void)leftTopPointMoved:(BOOL)left;
@end

@interface PDMLinesView : GLKView_SmartMirror

@property (nonatomic, assign) id <PDMLinesViewDelegate> controller;

-( void )setBackgroundImage : ( UIImage * )image;

//-( void )setLeftCornerBottomPoint : ( CGPoint )bottom topPoint : ( CGPoint )top angle : ( float )angle;
//-( void )setRightCornerBottomPoint : ( CGPoint )bottom topPoint : ( CGPoint )top angle : ( float )angle;

-( void )setLeftCornerBottomPoint : ( CGPoint )point;
-( void )setRightCornerBottomPoint : ( CGPoint )point;

-( void )setLeftCornerTopPoint : ( CGPoint )point;
-( void )setRightCornerTopPoint : ( CGPoint )point;

-( void )setLeftCornerAngle : ( float )angle;
-( void )setRightCornerAngle : ( float )angle;

-( CGPoint )leftCornerMarker : ( int )index;
-( CGPoint )rightCornerMarker : ( int )index;

-( float )leftWrapMarkerFraction;
-( float )rightWrapMarkerFraction;

-( void )setShowTopCorners : ( BOOL )show;
-( BOOL )showTopCorners;

@end
