/*
 *  detection_types.h
 *  detection
 *
 *  Created by Administrator on 3/16/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */
/*! \addtogroup ENUMS
 *
 *  @{
 */

#include "opencvheaders.h"
#ifndef DETECTION_TYPES
#define DETECTION_TYPES

/*! Marker points type: A - left, B - right, C1 - middle (screen orientation) */
typedef enum SUPPORT_POINT_TYPE {
    SUPPORT_POINT_A = 0,        /**< Marker point A (lefmost point in screen coordinates). */
    SUPPORT_POINT_B,            /**< Marker point B (rightmost point in screen coordinates). */
    SUPPORT_POINT_C1,           /**< Marker point C1 (middle point found on support extension). */
    SUPPORT_POINT_D,            /**< Marker point D (point added for testing purposes under pooint A). */
    SUPPORT_POINT_A1,            /**< Marker point A1 - placed under support marker A */
    SUPPORT_POINT_B1,           /**< Marker point B1 - placed under support marker B */
    SUPPORT_ESS_LEFT_RIM,       /**< Circular marker point for Essilor support (leftmost in screen coordinates) */
    SUPPORT_ESS_RIGHT_RIM,      /**< Circular marker point for Essilor support (rightmost in screen coordinates) */
    SUPPORT_POINT_UNDEFINED,    /**< Undefined marker point, can be used for markers with unsure type. */
    MAX_SUPPORT_POINTS          /**< The total number of marker point types. */
    
} SUPPORT_POINT_TYPE;

/*! Marker object type */
typedef enum SUPPORT_TYPE {
    SUPPORT_CLASSIC_3M = 0,     /**< Classic 3-Marker support. **/
    SUPPORT_BRIDGE_4M,          /**< Classic + 4th Marker on frames bridge. **/
    SUPPORT_VERTICAL_4M,         /**< Classic + 4th Marker on support leg, under A point. **/
    SUPPORT_ESS,                /**< Essilor support */
    SUPPORT_ZEISS,              /**< Zeiss support - 5 marker support*/
    SUPPORT_ACEP_PATTERN        /** ACEP support with 3 'biohazard' markers **/
} SUPPORT_TYPE;


/*! Eye type - using human orientation */
typedef enum EYE_POINT_TYPE {
    EYE_POINT_RIGHT = 0,        /**< The right eye. **/
    EYE_POINT_LEFT,             /**< The left eye **/
    MAX_EYE_POINTS              /**< The total number of eye types. **/
} EYE_POINT_TYPE;


/*! Frame boxing type */
typedef enum BOXING_POINT_TYPE {
    BOXING_POINT_TOP = 0,       /**< Top boxing point, upper frames limit. **/
    BOXING_POINT_BOTTOM,        /**< Bottom boxing point, lower frames limit. **/
    BOXING_POINT_BRIDGE,        /**< Bridge boxing point, inner frames limit near the nose of the pacient. **/
    BOXING_POINT_EXT,           /**< Exterior boxing point, frame limit towards the arms of the eye-glasses. **/
    BOXING_POINT_TOPBRIDGE,     /**< Top-bridge boxing point, only one per eye-glasses. Found in the middle of the bridge of the glasses, on the nose. **/
    MAX_BOXING_POINTS           /**< The total number of boxing points. **/
} BOXING_POINT_TYPE;


/*! Iris type */
enum IRIS_POINT_TYPE {
    IRIS_POINT_CENTER = 0,      /**< The center of the iris circle. **/
    IRIS_POINT_UPPERLID,        /**< The upper boundary of the iris, possibly delimited by the upper eye lid. **/
    IRIS_POINT_LOWERLID,        /**< The lower boundary of the iris, possibly delimited by the lower eye lid. **/
    IRIS_POINT_RADIUSLEFT,      /**< The point situated on the same horizontal line as the iris center, on the left boundary of the iris. The radius of the iris circle can be computed as the difference on X between the center of the iris and this point. **/
    MAX_IRIS_POINTS             /**< The total number of iris points. **/
};

/** Describes Support Detection strategies.
 Represents the number of iterations the marker support detection will perform for detection. It
 is therefore directly related to the speed and accuracy of the detection. These three enum values are
 considered 3 standard detection values. They start from slow (and most accurate) to fast (less accurate).
 Due to later improvements of the algorithm the SLOW detection in recomended for all cases (both single
 image detection and in-video detection).
 
 @see
 For more information on the particular algorithm used for support detection, seee @link DetectionCV::detectSupport() @endlink.
 **/
enum SUPPORT_TIME_TYPE{
    SUPPORT_TIME_SLOW = 6,      /**< Accurate and slow marker detection. Currently best approach for both single image detection and in-video detection. **/
    SUPPORT_TIME_NORMAL = 3,    /**< Moderately accurate and fast detection. **/
    SUPPORT_TIME_FAST = 1       /**< Very fast and much less accurate detection. Should be used only on very slow machines. Has low detection accuracy. **/
};


/**
 Describes Eyes Detection strategies.
 There are currently two different eye detection strategies:
 - A set of algorithms that run slower yet have a higher chance of finding the eye zones. This strategy is best used for single
 image detection where speed is not such an important factor.
 - A set of algorithms that run faster yet have lower chance of finding the eye zones and lower accuracy. This strategy is best
 used for in-video detection.
 @see
 For more information on the particular algorithms used for the slow/fast detection, see @link DetectionCV::detectEyes() @endlink.
 **/
typedef enum EYE_DETECTION_TYPE {
    EYE_DETECTION_SLOW = 5,     /**< Slower yet more accurate eye detection: fast left/right eye Haar detection + fast eye pair Haar detection **/
    EYE_DETECTION_NORMAL = 3,   /**< Currently not used. **/
    EYE_DETECTION_FAST = 1      /**< Faster yet less accurate eye detection: fast left/right eye Haar detection + slower (on larger images) left/right eye Haar detection **/
} EYE_DETECTION_TYPE;

#endif

/*!  @} */ //end of enum goup
