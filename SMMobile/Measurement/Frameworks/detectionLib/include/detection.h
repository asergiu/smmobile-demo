
#ifndef __detection__detection__
#define __detection__detection__

/*!
\defgroup DETECTION_IOS Detection library for ios

@defgroup DETECTION_IOS_D Detection functions
 @{
 \ingroup DETECTION_IOS
 @}

 @defgroup DETECTION_IOS_EYE Eye detection
 @{
 \ingroup DETECTION_IOS_D
 @}

 @defgroup DETECTION_IOS_SUPPORT Support detection
 @{
 \ingroup DETECTION_IOS_D
 @}

 @defgroup DETECTION_IOS_BOXING Boxing detection
 @{
 \ingroup DETECTION_IOS_D
 @}

 @defgroup DETECTION_IOS_IRIS Iris detection
 @{
 \ingroup DETECTION_IOS_D
 @}

 @defgroup DETECTION_IOS_IRIS Frames detection
 @{
 \ingroup DETECTION_IOS_D
 @}

*/
#include "opencvheaders.h"

#include <vector>
#include <list>
using namespace std;

#include "detection_types.h"

class DetectionCV;
/// Eye, marker, boxing, frames and iris detection for ios.
class Detection {
public:
    Detection();
    ~Detection();
    
    /* IMAGE loading **/
    bool loadImage(cv::Mat image, bool isSeq = false);
    void rotate(double angle, cv::Point2d rotationOrigin);
    bool isImageLoaded();

    bool loadHaarClasifiers(std::string directoryPath);
    bool loadHaarClassifiers(std::string leftEyeClassifier, std::string rightEyeClassifier, std::string eyePairClassifier);
    bool loadHaarClassifiers(cv::CascadeClassifier leftEyeClassifier, cv::CascadeClassifier rightEyeClassifier, cv::CascadeClassifier eyePairClassifier);
    int  numberOfHaarClassifiersLoaded();
    
    /* EYE DETECTION **/
    bool detectEyes(EYE_DETECTION_TYPE detectionType, bool adjustResults);
    bool hasEyesDetected();
    bool hasEyePoint(EYE_POINT_TYPE eyeType);
    cv::Point2d getEyePoint (EYE_POINT_TYPE eyeType);
    cv::Point getEyePointI(EYE_POINT_TYPE eyeType);
    cv::Rect getEyeRect(EYE_POINT_TYPE eyeType);
    bool setEyePoint(EYE_POINT_TYPE eyeType, cv::Point2d eyePoint);


    /* SUPPORT DETECTION: **/

    bool detectSupport(SUPPORT_TYPE supportType = SUPPORT_CLASSIC_3M);
    bool detectSupportACEP(SUPPORT_TIME_TYPE detectionMethod = SUPPORT_TIME_SLOW);
    bool detectSupportZeiss(cv::Mat tmpl = cv::Mat());
    bool detectSupportACEPPatterns();

    int numberSupportPointsDetected();
    bool hasSupportPoint(SUPPORT_POINT_TYPE supportType);
    bool hasSupportAB();
    bool hasSupportDetectedFull();
    bool setSupportPoint(SUPPORT_POINT_TYPE supportType, cv::Point2d supportPoint);
    cv::Point2d getSupportPoint(SUPPORT_POINT_TYPE supportType);

    bool loadSupportClassifier(std::string filepath);
    SUPPORT_TYPE detectSupportWithType();

    /* BOXING **/
    int detectBoxing();
    
    bool hasBoxingDetected(EYE_POINT_TYPE eyeType);
    bool hasBoxingDetectedFull(EYE_POINT_TYPE eyeType);
    bool hasBoxingPoint(EYE_POINT_TYPE eyeType, BOXING_POINT_TYPE boxingPointType);
    cv::Point2d getBoxingPointF(EYE_POINT_TYPE eyeType, BOXING_POINT_TYPE boxingPointType);
    cv::Point getBoxingPoint (EYE_POINT_TYPE eyeType, BOXING_POINT_TYPE boxingPointType);


    void clearDetectionData();
private:
    DetectionCV* detection;

};



#endif /* defined(__detection__detection__) */



