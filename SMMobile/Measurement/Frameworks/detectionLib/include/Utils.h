//
//  Utils.h
//  detection
//
//  Created by admin on 1/14/13.
//  Copyright (c) 2013 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "opencvheaders.h"

/// Conversion function between OpenCV image types and Obj-C image type
@interface Utils : NSObject

+(UIImage *)UIImageFromIplImage:(IplImage *)image;
+(IplImage *)IplImageFromUIImage:(UIImage *)image;

+(cv::Mat)UIImageToMat:(UIImage*)image;
+(UIImage *)UIImageFromCVMat:(cv::Mat)cvMat;

@end
