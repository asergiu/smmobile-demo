//
//  NSObject+EncryptVideoPAL.h
//  SMMobile
//
//  Created by Work Inteleks on 5/7/15.
//  Copyright (c) 2015 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (EncryptVideoPAL)

- (NSURL *)encryptFile:(NSString *)fileName fileExtantion:(NSString *)extantion;

@end
