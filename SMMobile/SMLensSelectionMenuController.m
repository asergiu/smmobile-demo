//
//  SMLensSelectionMenuController.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 8/7/14.
//

#import "SMLensSelectionMenuController.h"
#import "UIView+Effects.h"
#import "SMInfoViewController.h"
#import "SMCoatingsViewController.h"
#import "SMAppManager.h"
#import "SMProvisionAlertViewController.h"

int globalLensShape = 0;

@interface SMLensSelectionMenuController () <SMInfoViewControllerDelegate, SMProvisionAlertViewControllerDelegate>
{
    SMInfoViewController *infoContoller;
    UIView *backgroundView;
    
    IBOutletCollection(UIVisualEffectView) NSArray *blurViews;
    IBOutletCollection(UIView) NSArray *buttonContainers;
    
    SMProvisionAlertViewController *provisionController;
}

- (void)hideMenuItems;
- (void)showMenuItems;
- (void)showItem:(UIButton *)item;
- (void)lensMenuEntered;
@end

@implementation SMLensSelectionMenuController

@synthesize shapeButtons;
@synthesize moduleButtons;

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self hideMenuItems];
    
    [mainBackground addParalaxEffect];
    
    [self localizations];
    
    for (UIVisualEffectView *view in blurViews)
    {
        view.layer.cornerRadius = 10.0;
        view.clipsToBounds = YES;
    }
    
    for (UIButton *btn in moduleButtons)
    {
        btn.layer.cornerRadius = 10.0;
        btn.clipsToBounds = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self showMenuItems];
    
    [self lensMenuEntered];
    
    if ([APP_MANAGER isSoonExpirationDate])
        [self showProvisionAlert];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark Localizations

- (void)localizations
{
    for (UIButton *button in moduleButtons)
    {
        [button.titleLabel setTextAlignment:NSTextAlignmentCenter];
    }
}

#pragma mark Animate View Elements

- (void)hideMenuItems
{
    for (UIView *item in buttonContainers)
    {
        item.transform = CGAffineTransformMakeScale(0, 0.5);
    }
}

- (void)showMenuItems
{
    float time = 0.0;
    
    for (UIButton *button in buttonContainers)
    {        
        time = (button.tag <= 3) ? button.tag * 0.2f : 1.f + ((int)button.tag - (int)buttonContainers.count) * 0.2;
        
        [self performSelector:@selector(showItem:) withObject:button afterDelay:time];
    }
}

- (void)showItem:(UIButton *)item
{
    [item flipView];
}

#pragma mark Autorotate

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

#pragma mark Back View For Popovers

- (void)placeBackgoundView
{
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024.f, 1024.f)];
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0;
    [self.view addSubview:backgroundView];
    
    [UIView animateWithDuration:0.4f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        backgroundView.alpha = 0.4f;
    } completion:nil];
}

- (void)removeBackgoundView
{
    [UIView animateWithDuration:0.4f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        backgroundView.alpha = 0;
    } completion:^(BOOL finished){
        [backgroundView removeFromSuperview];
        backgroundView = nil;
    }];
}

#pragma mark Actions

- (void)showInfo:(id)sender
{
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//
    [self placeBackgoundView];
    
//    if (!infoContoller)
//    {
//        infoContoller = [storyboard instantiateViewControllerWithIdentifier:@"infoViewController"];
//        
//        infoContoller.delegate = self;
//        
//        infoContoller.view.frame = CGRectMake((self.view.bounds.size.width - 540.f)/2, (self.view.bounds.size.height - 650.f)/2, 540.f, 650.f);
//        
//        [self.view addSubview:infoContoller.view];
//        
//        [infoContoller.view showInfoViewInPoint:CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2)];
//    }
}

- (void)dismissInfo
{
    [infoContoller.view hideInfoViewWithAnimation];
    infoContoller = nil;
    
    [self removeBackgoundView];
}

- (void)back:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

#pragma mark -

- (void)dealloc
{
    mainBackground = nil;
    moduleNameLabel = nil;
    moduleButtons = nil;
    shapeButtons = nil;
}

#pragma mark Google Analytics

- (void)lensMenuEntered
{
    [APP_MANAGER sendAnalyticsEnteredModuleWithName:@"SMLensMenu"];
}

#pragma mark - Provision Expiration Date Checking

- (void)showProvisionAlert
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [self placeBackgoundView];
    
    if (!provisionController)
    {
        provisionController = [storyboard instantiateViewControllerWithIdentifier:@"ProvisionAlertViewController"];
        provisionController.delegate = self;
        provisionController.view.frame = CGRectMake((self.view.bounds.size.width - 440.f) / 2,
                                                    (self.view.bounds.size.height - 180.f) / 2,
                                                    440.f,
                                                    180.f);
        
        [self.view addSubview:provisionController.view];
        
        [provisionController.view showInfoViewInPoint:CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2)];
    }
}

- (void)dismissProvisionAlert
{
    [provisionController.view hideInfoViewWithAnimation];
    provisionController = nil;
    
    [self removeBackgoundView];
}

@end
