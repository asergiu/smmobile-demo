//
//  UIVisualEffectView+Effect.h
//  SMMobile
//
//  Created by Sergej Bogatenko on 2/28/18.
//  Copyright © 2018 ACEP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIVisualEffectView (Effect)

- (void)setupBlurEffectWithCornerRadius:(float)cornerRadius;

@end
