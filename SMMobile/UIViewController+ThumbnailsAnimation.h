//
//  UIViewController+ThumbnailsAnimation.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 8/26/14.
//

#import <UIKit/UIKit.h>
#import "UIView+Effects.h"

@interface UIViewController (ThumbnailsAnimation)

- (void)expandThumbnails:(NSArray *)group
            withPosition:(float)position
                    step:(float)step;

- (void)hideThumbnails:(NSArray *)group
          withPosition:(float)position
                  step:(float)step;

- (void)slideViews:(NSArray *)group
         xPosition:(float)xPosition
         yPosition:(float)yPosition
              step:(float)step;

@end
