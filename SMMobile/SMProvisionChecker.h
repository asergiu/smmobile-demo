//
//  SMExpirationDateChecker.h
//  SMMobile
//
//  Created by Sergej Bogatenko on 3/3/18.
//  Copyright © 2018 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMProvisionChecker : NSObject

- (BOOL)isSoonExpirationDate;

- (void)setupIsNeedCheckNewProfileExpirationDate;
- (void)setupIsNeedShowAgainExpirationDateAlert:(BOOL)isNeedShowAgain;
- (void)setupIsNeedDelayExpirationDateAlert;

- (BOOL)dontShowAgainExpirationDateAlert;

@end
