//
//  NSstring+CheckStrings.h
//  
//
//  Created by Sergej Bogatenko on 9/08/18.
//
//

#import <Foundation/Foundation.h>

@interface NSString (CheckStrings)

- (BOOL)stringIsNotNil:(NSString *)string;
- (BOOL)validEmail:(NSString *)emailString;

- (float)getTextWidth:(float)fontSize;

- (NSString *)removeNotAlphaNumericSymbols;
- (NSString *)cutDeviceIDForFirebaseAnalytics;

@end
