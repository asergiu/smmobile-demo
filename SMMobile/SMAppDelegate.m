//
//  SMAppDelegate.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 8/5/14.
//

#import "SMAppDelegate.h"
#import "SMNetworkManager.h"
#import "GAI.h"
#import "SMAppManager.h"

@implementation SMAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{    
    [self setupAppVersion];
    [self setBundleID];
    [self setupGoogleAnalytics];
    [self registerAPN];
    [self registerDefaultsFromSettingsBundle];
    [self setLenssNamesForLocale];
    
    application.applicationIconBadgeNumber = 0;
    
    return YES;
}

- (void)registerDefaultsFromSettingsBundle
{
    NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    if(!settingsBundle)
    {
        NSLog(@"Could not find Settings.bundle");
        return;
    }
    
    NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
    NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
    
    NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
    for(NSDictionary *prefSpecification in preferences)
    {
        NSString *key = [prefSpecification objectForKey:@"Key"];
        if(key)
        {
            if ([prefSpecification objectForKey:@"DefaultValue"] != nil) {
                [defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
            }
        }
    }
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
}

- (void)setLenssNamesForLocale
{
    NSString *standardLens = [[NSUserDefaults standardUserDefaults] objectForKey:@"standart_lens"];
    NSString *premiumLens  = [[NSUserDefaults standardUserDefaults] objectForKey:@"premium_lens"];
    NSString *eliteLens    = [[NSUserDefaults standardUserDefaults] objectForKey:@"elite_lens"];
    NSString *tailorLens   = [[NSUserDefaults standardUserDefaults] objectForKey:@"tailor_lens"];
    NSString *bifocalLens  = [[NSUserDefaults standardUserDefaults] objectForKey:@"bifocal_lens"];
    NSString *readingLens  = [[NSUserDefaults standardUserDefaults] objectForKey:@"reading_lens"];
    NSString *computerLens = [[NSUserDefaults standardUserDefaults] objectForKey:@"computer_lens"];
    
    [[NSUserDefaults standardUserDefaults] setObject:NSLocalizedString(standardLens, nil) forKey:@"standart_lens"];
    [[NSUserDefaults standardUserDefaults] setObject:NSLocalizedString(premiumLens, nil) forKey:@"premium_lens"];
    [[NSUserDefaults standardUserDefaults] setObject:NSLocalizedString(eliteLens, nil) forKey:@"elite_lens"];
    [[NSUserDefaults standardUserDefaults] setObject:NSLocalizedString(tailorLens, nil) forKey:@"tailor_lens"];
    [[NSUserDefaults standardUserDefaults] setObject:NSLocalizedString(bifocalLens, nil) forKey:@"bifocal_lens"];
    [[NSUserDefaults standardUserDefaults] setObject:NSLocalizedString(readingLens, nil) forKey:@"reading_lens"];
    [[NSUserDefaults standardUserDefaults] setObject:NSLocalizedString(computerLens, nil) forKey:@"computer_lens"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    NSNotification *notif = [NSNotification notificationWithName:@"refreshLensName" object:self];
    [[NSNotificationCenter defaultCenter] postNotification:notif];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [NETWORK_MANAGER sendToServerClientStatus:StoreClientStateIdle
                               withCompletion:nil];
}

#pragma mark Core Motion

- (CMMotionManager *)motionManager
{
    if (!motionManager)
        motionManager = [CMMotionManager new];
    
    return motionManager;
}

#pragma mark Google Analytics

- (void)setupGoogleAnalytics
{    
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelNone];
    
    [GAI sharedInstance].dispatchInterval = 120;
    
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:[APP_MANAGER getAnalyticsTrackerID]];
    
    NSLog(@"Created Google tracker: %@", tracker);
    
    [APP_MANAGER generateAnalyticDeviceID];
}

#pragma mark -

- (void)setupAppVersion
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *build   = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    NSString *versionString = [NSString stringWithFormat:@"%@ (%@)", version, build];
    
    [[NSUserDefaults standardUserDefaults] setObject:versionString forKey:@"currentVersion"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setBundleID
{
    [[NSUserDefaults standardUserDefaults] setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:@"bundle"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark Push Notifications

- (void)registerAPN
{
    if (IS_IOS8_AND_LATER)
    {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge)];
    }
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}
#endif

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    application.applicationIconBadgeNumber = 1;
    
    NSLog(@"Received notification: %@", userInfo);
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *token = [deviceToken description];
    token = [token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"My token is: %@", token);
    
    [SMNetworkManager tokenRequest:token];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Failed to get token, error: %@", error.description);
}

#pragma mark - Open App URL Scheme

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(nullable NSString *)sourceApplication
         annotation:(id)annotation
{
    NSLog(@"SESSION Share URL: %@", url);
    
    if ([url.scheme isEqualToString:@"smmobile2"])
    {
        [APP_MANAGER parseUrlScheme:url];
    }
    
    return YES;
}

@end
