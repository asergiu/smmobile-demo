//
//  Thickness3DView.h
//  Thickness
//
//  Created by Dgut on 05.08.14.
//

#import "GLKView_SmartMirror.h"

class SMModel;
class SMShader;
class SMBuffer;

SMModel * const thickness3DGetModel( const char * name );
SMShader * const thickness3DGetShader( const char * name );
GLKTextureInfo * const thickness3DGetTexture( const char * name );
GLKTextureInfo * const thickness3DGetCubemap( const char * name );

SMBuffer * const thickness3DGetBuffer( void );
SMBuffer * const thickness3DGetInternalBuffer( void );
SMBuffer * const thickness3DGetBackgroundBuffer( void );

@interface SMThickness3DView : GLKView_SmartMirror

@property ( nonatomic, weak ) IBOutlet SMThickness3DView * otherView;

-( void )setPower : ( float )power cyl : ( float )cyl axis : ( long )axis;
-( void )setLeftDesign : ( NSString * )design;
-( void )setRightDesign : ( NSString * )design;

-( void )setTestValue0 : ( float )test0;
-( void )setTestValue1 : ( float )test1;

//-( void )setFrameWidth : ( float )frameWidth;
-( void )setPupillaryDistance : ( float )pupillaryDistance;
-( void )showPerson : ( bool )showPerson;
-( void )showFrame : ( bool )showFrame;

-( void )setPerson : ( int )index;
-( void )setDynamicGlasses : ( BOOL )dynamic;

-( void )setDBL : ( float )dbl;
-( void )setA : ( float )a;
-( void )setB : ( float )b;
-( void )reset;

-( void )setSplitMode : ( BOOL )split;
-( BOOL )splitMode;

-( SMModel * const )getModel : ( const char * )name;
-( SMShader * const )getShader : ( const char * )name;
-( GLKTextureInfo * const )getTexture : ( const char * )name;
-( GLKTextureInfo * const )getCubemap : ( const char * )name;

-( SMBuffer * const )getBuffer;
-( SMBuffer * const )getInternalBuffer;
-( SMBuffer * const )getBackgroundBuffer;

-( void )setYaw : ( float )yaw;

-( void )setOtherDragging : ( BOOL )dragging;

+( void )setCurrentView : ( SMThickness3DView * )currentView;
+( SMThickness3DView * )currentView;

-( void )loadThings;
-( void )clearThings;

@end
