//
//  SMMainMenuViewController.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 8/5/14.
//

#import <UIKit/UIKit.h>

@interface SMMainMenuViewController : UIViewController
{
    IBOutlet UIImageView *mainBackground;
}

@property (nonatomic, assign) BOOL showMenuButtons;

- (void)enterPDModule;

@end
