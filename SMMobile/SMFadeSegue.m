//
//  SMFadeSegue.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 8/7/14.
//

#import "SMFadeSegue.h"

@implementation SMFadeSegue

- (void)perform
{
    UIViewController *sourceController = (UIViewController *)self.sourceViewController;
    UIViewController *destinationController = (UIViewController *)self.destinationViewController;
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.7f;
    transition.type = kCATransitionFade;
    
    [sourceController.navigationController.view.layer addAnimation:transition forKey:kCATransitionFade];
    
    [sourceController.navigationController presentViewController:destinationController animated:NO completion:NULL];
}

@end
