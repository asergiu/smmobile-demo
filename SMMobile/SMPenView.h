#import "GLKView_SmartMirror.h"

@protocol SMPenViewDelegate <NSObject>

@optional
- (void)touchBeganView;
- (void)touchesEndedView;

@end

@interface SMPenView : GLKView_SmartMirror

@property (nonatomic, assign) id <SMPenViewDelegate> delegatePen;

@property ( nonatomic, assign ) BOOL penActivated;
@property ( nonatomic, assign ) int penColor;
@property ( nonatomic, assign ) float penWidth;

@end
