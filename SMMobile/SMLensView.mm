//
//  SMLensView.m
//  ACEPSmartMirror
//
//  Created by Dgut on 02.05.12.
//

#import "SMLensView.h"

#include "vector"

using namespace std;

@interface SMLensView()
{
    GLKTextureInfo * background;
    
    GLKTextureInfo * mask;
    GLKTextureInfo * lens;
    
    GLKTextureInfo * lens_rect;
    GLKTextureInfo * mask_rect;
    GLKTextureInfo * lens_ellipse;
    GLKTextureInfo * mask_ellipse;
    
    vector< GLKTextureInfo * > backgrounds;
    
    GLuint progText;
    GLuint lensProgram;
    GLuint defaultProgram;
    
    GLuint designTexture;
    
    float backgroundPosition[ 2 ];
    float backgroundSize[ 2 ];
    
    float lensPosition[ 2 ];
    float lensSize[ 2 ];
    
    float b0;
    float F3;
    float F3_cyl;
    //float F5;
    float axis;
    
    BOOL drag;
    CGPoint dragPoint;
    
    int design;
    float index;
    
    GLKVector2 screenSize;
}

@end

@implementation SMLensView

-( id )initWithCoder : ( NSCoder * )decoder
{
    self = [ super initWithCoder : decoder ];
    if( self )
    {
        EAGLContext * context = [ [ EAGLContext alloc ] initWithAPI : kEAGLRenderingAPIOpenGLES2 ];
        [ EAGLContext setCurrentContext : context ];
        
        self.context = context;
        //self.contentScaleFactor = 1.f;
        
        lens_rect = [ self getTexture : @"big_lens.png" ];
        mask_rect = [ self getTexture : @"big_mask_no_ar.png" ];
        lens_ellipse = [ self getTexture : @"ellipse_big_lens.png" ];
        mask_ellipse = [ self getTexture : @"ellipse_big_mask_no_ar.png" ];
        
        mask = mask_rect;
        lens = lens_rect;
        
        backgrounds.resize( 2 );
        backgrounds[ 0 ] = [ self getTexture : @"asph_dots" ];
        backgrounds[ 1 ] = [ self getTexture : @"asph_columns" ];
        
        designTexture = [ self genTexture ];
        
        progText = [ self getProgram : @"TextRect" : @"TextRect" ];
        lensProgram = [ self getProgram : @"Lens" : @"Lens" ];
        defaultProgram = [ self getProgram : @"Default" : @"Default" ];
        
        glEnableVertexAttribArray( GLKVertexAttribPosition );
        glEnableVertexAttribArray( GLKVertexAttribTexCoord0 );
        
        glEnable( GL_BLEND );
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
        
        backgroundPosition[ 0 ] = self.bounds.size.width / 2.f;
        backgroundPosition[ 1 ] = self.bounds.size.height / 2.f;
        
        backgroundSize[ 0 ] = 877.f / 2;
        backgroundSize[ 1 ] = 660.f / 2;
        
        lensSize[ 0 ] = lens.width / 4;
        lensSize[ 1 ] = lens.height / 4;
        
        lensPosition[ 0 ] = self.bounds.size.width / 2.f - lensSize[ 0 ] / 2.f;
        lensPosition[ 1 ] = self.bounds.size.height / 2.f - lensSize[ 1 ] / 2.f;
        
        screenSize = GLKVector2Make( self.frame.size.width, self.frame.size.height );
        
        [ self setPower : 2 ];
        
        drag = false;
    }
    return self;
}

- (void)renderDesignView
{
    [self renderInterfaceView:self.designView toTexture:designTexture];
}

-( void )renderInterfaceView : ( UIView * )view toTexture : ( GLuint )texture
{
    view.hidden = NO;
    
    CGFloat scale = [ UIScreen mainScreen ].scale;
    
    const int width = view.frame.size.width * scale;
    const int height = view.frame.size.height * scale;
    
    const int dataSize = width * height * 4;
    uint8_t * data = ( uint8_t * )malloc( dataSize );
    memset( data, 0, dataSize );
    CGColorSpace * colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContext * context = CGBitmapContextCreate( data, width, height, 8, width * 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big );
    CGContextConcatCTM( context, CGAffineTransformMakeScale( scale, scale ) );
    CGColorSpaceRelease( colorSpace );
    
    [ view.layer renderInContext : context ];
    
    [ EAGLContext setCurrentContext : self.context ];
    
    glBindTexture( GL_TEXTURE_2D, texture );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data );
    
    CGContextRelease( context );
    free( data );
    
    view.hidden = YES;
}

-( void )drawRect : ( CGRect )rect
{    
    glClearColor( 1.f, 1.f, 1.f, 1.f );
    glClear( GL_COLOR_BUFFER_BIT );
    
    GLKMatrix4 modelViewProjectionMatrix = GLKMatrix4MakeOrtho( 0, self.bounds.size.width, 0, self.bounds.size.height, -99999., 99999. );
    
    float scale;
    
    float backScaleX = self.frame.size.width / background.width;
    float backScaleY = self.frame.size.height / background.height;
    
    if( backScaleX > backScaleY )
        scale = backScaleX;
    else
        scale = backScaleY;
    
    backgroundSize[ 0 ] = background.width * scale;
    backgroundSize[ 1 ] = background.height * scale;
    
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( background.target, background.name );
    glActiveTexture( GL_TEXTURE1 );
    glBindTexture( mask.target, mask.name );
    
    glUseProgram( defaultProgram );
    
    glUniformMatrix4fv( glGetUniformLocation( defaultProgram, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    
    [ self drawRectangle : CGRectMake( backgroundPosition[ 0 ] - backgroundSize[ 0 ] / 2.f, backgroundPosition[ 1 ] - backgroundSize[ 1 ] / 2.f, backgroundSize[ 0 ], backgroundSize[ 1 ] ) ];
    
    glUseProgram( lensProgram );
    
    GLKVector2 axis = GLKVector2Make( cosf( self->axis * M_PI / 180.f ), sinf( self->axis * M_PI / 180.f ) );
    
    float powerFactor;
    
    if( design == 2 )
        powerFactor = 0.3f;
    else if( design == 1 )
        powerFactor = 0.5f;
    else
        powerFactor = 1.f;
    
    glUniform1i( glGetUniformLocation( lensProgram, "background" ), 0 );
    glUniform1i( glGetUniformLocation( lensProgram, "mask" ), 1 );
    glUniformMatrix4fv( glGetUniformLocation( lensProgram, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    glUniform2fv( glGetUniformLocation( lensProgram, "backgroundPosition" ), 1, backgroundPosition );
    glUniform2fv( glGetUniformLocation( lensProgram, "backgroundSize" ), 1, backgroundSize );
    glUniform2fv( glGetUniformLocation( lensProgram, "lensPosition" ), 1, lensPosition );
    glUniform2fv( glGetUniformLocation( lensProgram, "screenSize" ), 1, screenSize.v );
    glUniform1f( glGetUniformLocation( lensProgram, "flip" ), 0 );
    glUniform1f( glGetUniformLocation( lensProgram, "b0" ), b0/*0.2f*/ );
    glUniform1f( glGetUniformLocation( lensProgram, "F3" ), F3 * powerFactor * ( 2.5f - index )/*0.000005f*/ );
    glUniform1f( glGetUniformLocation( lensProgram, "F3_cyl" ), F3_cyl * powerFactor * ( 2.5f - index )/*0.000005f*/ );
    glUniform2fv( glGetUniformLocation( lensProgram, "axis" ), 1, axis.v );
    //glUniform1f( glGetUniformLocation( lensProgram, "F5" ), F5 );
    /*glUniform1f( glGetUniformLocation( lensProgram, "factor" ), 30.f );
     glUniform1f( glGetUniformLocation( lensProgram, "power" ), -2.f );*/
    
    
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( background.target, background.name );
    glActiveTexture( GL_TEXTURE1 );
    glBindTexture( mask.target, mask.name );
    
    [ self drawRectangle : CGRectMake( lensPosition[ 0 ], lensPosition[ 1 ], lensSize[ 0 ], lensSize[ 1 ] ) ];
    
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( lens.target, lens.name );
    
    glUseProgram( defaultProgram );
    
    glUniformMatrix4fv( glGetUniformLocation( defaultProgram, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    
    [ self drawRectangle : CGRectMake( lensPosition[ 0 ], lensPosition[ 1 ], lensSize[ 0 ], lensSize[ 1 ] ) ];
    
    glUseProgram( progText );
    glUniformMatrix4fv( glGetUniformLocation( progText, "modelViewProjectionMatrix" ), 1, 0, modelViewProjectionMatrix.m );
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, designTexture );
    CGRect frame = CGRectMake(150.f,
                              230.f,
                              self.designView.frame.size.width,
                              self.designView.frame.size.height);
    frame.origin.x += lensPosition[0];
    frame.origin.y += lensPosition[1];
    [ self drawRectangle : frame ];
}

-( void )setShape : ( NSInteger )aShape
{
    if( aShape )
    {
        mask = mask_ellipse;
        lens = lens_ellipse;
    }
    else
    {
        mask = mask_rect;
        lens = lens_rect;
    }
}

-( void )addBackground : ( NSString * )name
{
    backgrounds.push_back( [ self getTexture : name ] );
}

-( void )setBackground : ( int )index
{
    background = backgrounds[ index ];
    
    [ self display ];
}

-( void )setPower : ( float )power
{
    b0 = 1.f;
    F3 = 0.0000003f * -power;
    
    [ self display ];
}

-( void )setCyl : ( float )cyl
{
    F3_cyl = 0.0000003f * -cyl;
    
    [ self display ];
}

-( void )setAxis : ( float )axis
{
    self->axis = axis;
    
    [ self display ];
}

-( void )setDesign : ( int )design
{
    self->design = design;
}

-( void )setIndex : ( float )index
{
    self->index = index;
}

-( void )touchesBegan : ( NSSet * )touches withEvent : ( UIEvent * )event
{    
	for( UITouch * touch in touches )
    {
        CGPoint point = [ touch locationInView : self ];
        point.y = self.bounds.size.height - point.y;
        
        if( abs( lensPosition[ 0 ] - point.x ) < lensSize[ 0 ] && abs( lensPosition[ 1 ] - point.y ) < lensSize[ 1 ] )
        {
            drag = YES;
            dragPoint = point;
        }
		
		/*lensPosition[ 0 ] = point.x;
         lensPosition[ 1 ] = self.bounds.size.height - point.y;*/
    }
    
    //[ self touchesMoved : touches withEvent : event ];
}

-( void )touchesMoved : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    if( !drag )
        return;
    
	for( UITouch * touch in touches )
    {
        CGPoint point = [ touch locationInView : self ];
        point.y = self.bounds.size.height - point.y;
		
		lensPosition[ 0 ] += point.x - dragPoint.x;
		lensPosition[ 1 ] += point.y - dragPoint.y;
        
        dragPoint = point;
    }
    
    [ self display ];
}

-( void )touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if( drag )
    {
        lensPosition[ 0 ] = self.bounds.size.width / 2.f - lensSize[ 0 ] / 2.f;
        lensPosition[ 1 ] = self.bounds.size.height / 2.f - lensSize[ 1 ] / 2.f;
        
        drag = NO;
        
        [ self display ];
    }
}

-( void )touchesCancelled : ( NSSet * )touches withEvent : ( UIEvent * )event
{
    [ self touchesEnded : touches withEvent : event ];
}

@end
