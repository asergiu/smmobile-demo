#import <GLKit/GLKit.h>
#import <AVFoundation/AVFoundation.h>

typedef void ( ^ photoCallback )( BOOL );

@class SMColorTrackingCamera;

@interface GLKView_SmartMirror : GLKView

-( BOOL )playing;

-( void )setVideoMode : ( BOOL )videoMode;
-( BOOL )videoMode;

-( void )setFrontCamera : ( BOOL )frontCamera;
-( BOOL )frontCamera;

-( void )setCameraPreset : ( NSString * )cameraPreset;

-( GLuint )videoFrameTexture;
-( CGSize )videoFrameTextureSize;
-( void )setVideoFrameTextureSize : ( CGSize )size;

-( GLuint )photoTexture;
-( CGSize )photoTextureSize;

-( BOOL )premultiplicationParameter;
-( void )setPremultiplicationParameter : ( BOOL )premultiplication;

-( GLuint )genTexture;
-( GLuint )genFramebufferWithTexture : ( GLuint )texture width : ( GLsizei )width height : ( GLsizei )height;

-( void )setFPS : ( float )fps;
-( float )fps;

- (BOOL)isRunningCamera;

-( void )play;
-( void )stopWithDeallocCam:( BOOL )deallocCam;
- (void)stop;
-( void )killCamera;

-( void )takePhoto : ( photoCallback )callback;
- (void)showFlash:(BOOL)show;

-( void )processCameraFrame : ( CVImageBufferRef )cameraFrame;
-( void )processPhoto : ( CVImageBufferRef )imageData;

-( SMColorTrackingCamera * )camera;
-( AVCaptureDevice * )cameraDevice;

-( void )checkError;

-( GLKTextureInfo * )getTexture : ( NSString * )name;
-( GLKTextureInfo * )getTextureFromAssets : ( NSString * )name;
-( GLuint )getProgram : ( NSString * )vertex : ( NSString * )fragment;
-( void )drawRectangle : ( CGRect )rect;

-( void )renderInterfaceView : ( UIView * )view toTexture : ( GLuint )texture;

@end
