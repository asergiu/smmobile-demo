//
//  SMMainMenuViewController.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 8/5/14.
//

#import "SMMainMenuViewController.h"
#import "UIView+Effects.h"
#import "SMInfoViewController.h"
#import "SMAppManager.h"
#import "SMNetworkManager.h"
#import "SMSwordViewController.h"
#import <MessageUI/MessageUI.h>
#import "SMLogSaver.h"
#import "SVProgressHUD.h"
#import "SMProvisionAlertViewController.h"

@interface SMMainMenuViewController () <SMInfoViewControllerDelegate, SMSwordViewControllerDelegate, MFMailComposeViewControllerDelegate, SMProvisionAlertViewControllerDelegate>
{
    NSTimer *waitTimer;
    
    SMInfoViewController *infoContoller;
    
    SMSwordViewController *swordController;
    UIView *backgroundView;
    
    SMModule module;
    
    IBOutlet UIButton *followUsButton;
    
    IBOutletCollection(UIVisualEffectView) NSArray *blurViews;
    
    SMProvisionAlertViewController *provisionController;
}

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *buttonContainers;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;

- (IBAction)showInfo:(id)sender;
- (IBAction)enterModule:(UIButton *)sender;

- (void)showMainMenuItems;
- (float)closeMainMenuItems;
- (void)showMenuItem:(UIButton *)item;
- (void)closeMenuItem:(UIButton *)item;
- (void)mainMenuEntered;

- (IBAction)sendCrashReport:(id)sender;

@end

@implementation SMMainMenuViewController

@synthesize buttons;
@synthesize buttonContainers;
@synthesize showMenuButtons;

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if ([NETWORK_MANAGER isAuthenticated])
    {
        [NETWORK_MANAGER loadCurrentStone];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    showMenuButtons = YES;
    
    module = SMModuleUnknown;
    
    [mainBackground addParalaxEffect];
    
    [self hideMenuItems];
    
    for (UIButton *button in buttons)
    {
        [button.titleLabel setTextAlignment:NSTextAlignmentCenter];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(settingsChanged)
                                                 name:NSUserDefaultsDidChangeNotification
                                               object:nil];
    
    for (UIVisualEffectView *view in blurViews)
    {
        view.layer.cornerRadius = 10.0;
        view.clipsToBounds = YES;
    }
    
    for (UIButton *btn in buttons)
    {
        btn.layer.cornerRadius = 10.0;
        btn.clipsToBounds = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    followUsButton.hidden = !showMenuButtons;
    
    [APP_MANAGER setMainMenuPointer:self];
    
    if ([SMStone sessionTypeStone])
    {
        [NETWORK_MANAGER exitModule];
    }
    
    [NETWORK_MANAGER checkExpiry];
    
    [NETWORK_MANAGER getBlockTag];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (showMenuButtons)
    {
        waitTimer = [NSTimer scheduledTimerWithTimeInterval:20.f
                                                     target:self
                                                   selector:@selector(animateRandomButton)
                                                   userInfo:nil
                                                    repeats:YES];
        [self showMainMenuItems];
        
        [self mainMenuEntered];
        
        [NETWORK_MANAGER checkEmail];
        
        [self performSelector:@selector(mustSendAnalyticsTargetEmail)
                   withObject:nil
                   afterDelay:0.8f];
    }
    
    if ([APP_MANAGER isSoonExpirationDate])
        [self showProvisionAlert];
}

- (void)mustSendAnalyticsTargetEmail
{
    if (![NETWORK_MANAGER isAuthenticated])
        return;
    
    [APP_MANAGER setupAnalyticsEmail];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [waitTimer invalidate];
    
    waitTimer = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark Animate View Elements

- (void)animateRandomButton
{
    int number = rand() % 4;
    
    [buttonContainers[number] scaleAnimation];
}

- (void)hideMenuItems
{
    for (UIView *item in buttonContainers)
    {
        item.transform = CGAffineTransformMakeScale(0, 0.5f);
    }
}

- (void)showMainMenuItems
{
    float time = 0.4f;
    
    for (UIView *item in buttonContainers)
    {
        [self performSelector:@selector(showMenuItem:) withObject:item afterDelay:time];
        
        time += 0.2f;
    }
}

- (float)closeMainMenuItems
{
    float time = 0.0;
    
    for (UIView *item in buttonContainers)
    {
        [self performSelector:@selector(closeMenuItem:) withObject:item afterDelay:time];
        
        time += 0.2f;
    }
    
    return time;
}

- (void)showMenuItem:(UIButton *)item
{
    [item flipView];
}

- (void)closeMenuItem:(UIButton *)item
{
    [item closeFlipView];
}

#pragma mark UI Actions

- (void)setButtonsState:(BOOL)state
{
    for (UIButton *btn in buttons)
    {
        btn.enabled = state;
    }
}

#pragma mark - Actions

- (void)enterPDModule
{
    module = SMModulePD;
    
    [self openModule];
}

- (void)enterModule:(UIButton *)sender
{
    module = sender.tag;
    
    if ([NETWORK_MANAGER isAuthenticatedForModule:module])
    {
        if ([NETWORK_MANAGER isEmailCheckNeed])
        {
            [NETWORK_MANAGER checkEmailConfirmationWithAlert:YES];
        }
        else
        {
            [self performSelector:@selector(openModule)
                       withObject:nil
                       afterDelay:[self closeMainMenuItems]];
        }
    }
    else
        [self showSwordDialog];
}

- (void)showInfo:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [self placeBackgoundView];
    
    if (!infoContoller)
    {
        infoContoller = [storyboard instantiateViewControllerWithIdentifier:@"infoViewController"];
        
        infoContoller.delegate = self;
        
        infoContoller.view.frame = CGRectMake((self.view.bounds.size.width - 540.f)/2, (self.view.bounds.size.height - 620.f)/2, 540.f, 620.f);
        
        [self.view addSubview:infoContoller.view];
        
        [infoContoller.view showInfoViewInPoint:self.view.center];
    }
}

- (void)dismissInfo
{
    [infoContoller.view hideInfoViewWithAnimation];
    infoContoller = nil;
    
    [self removeBackgoundView];
}

- (void)openModule
{
    if ([SMStone sessionTypeStone])
    {
        [self setButtonsState:NO];
        
        [NETWORK_MANAGER enterModule:module withFillCompletion:^(BOOL state, NSString *reason) {
            
            [self setButtonsState:YES];
            
            if (state)
            {
                [self startSegue];
            }
            else
            {
                [NETWORK_MANAGER showAlert:NSLocalizedString(@"Report", nil) description:reason];
                
                [self showMainMenuItems];
            }
        }];
    }
    else
    {
        if ([SMStone storeTypeStone])
        {
            [NETWORK_MANAGER checkMacAdress];
        }
        
        [self startSegue];
    }
}

- (void)startSegue
{
    switch (module) {
        case SMModuleFS:
            [self performSegueWithIdentifier:@"toFrameSelection" sender:nil];
            break;
        case SMModuleSM:
            [self performSegueWithIdentifier:@"toLensSelection" sender:nil];
            break;
        case SMModulePD:
            [self performSegueWithIdentifier:@"toMeasurementSelection" sender:nil];
            break;
        case SMModuleVS:
        case SMModuleUnknown:
            break;
    }
}

- (void)sendCrashReport:(id)sender
{
    MFMailComposeViewController *picker = [MFMailComposeViewController new];
    
    picker.mailComposeDelegate = self;
    
    [picker setSubject:NSLocalizedString(@"SM Mobile crash report", nil)];
    
    [picker addAttachmentData:[NSData dataWithContentsOfFile:[SMLogSaver getLogFilePath]]
                     mimeType:@"text/plain"
                     fileName:@"crash.log"];
    @try {
        [self presentViewController:picker animated:YES completion:nil];
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

#pragma mark Sword Controller Actions

- (void)showSwordDialog
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [self placeBackgoundView];
    
    if (!swordController)
    {
        swordController = [storyboard instantiateViewControllerWithIdentifier:@"SMSwordViewController"];
        
        swordController.delegate = self;
        
        swordController.enterModule = module;
        
        [self.view addSubview:swordController.view];
        
        [swordController.view showInfoViewInPoint:CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2)];
    }
}

- (void)succesfullyLoggedIn
{
    [self cancelSword];
    
    float time = [self closeMainMenuItems];
    
    [self performSelector:@selector(openModule) withObject:nil afterDelay:time];
}

- (void)cancelSword
{
    [swordController.view hideInfoViewWithAnimation];
    
    swordController = nil;
    
    [self removeBackgoundView];
}

#pragma mark - Settings

- (void)settingsChanged
{
    if (!swordController)
    {
        [NETWORK_MANAGER checkEmail];
    }
}

#pragma mark - Support Mail (Mail Delegate)

- (void)sendMailPressed
{
    [self cancelSword];
    
    MFMailComposeViewController *picker = [MFMailComposeViewController new];
    
    picker.mailComposeDelegate = self;
    
    [picker setSubject:NSLocalizedString(@"Acep SmartMirror Contact", nil)];
    [picker setToRecipients:[NSArray arrayWithObject:@"ipad@opticvideo.com"]];
    
    @try {
        [self presentViewController:picker animated:YES completion:nil];
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (result == MFMailComposeResultSent)
    {
        [SMLogSaver removeLogFile];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:NSLocalizedString(@"Your email has been sent to …", @"")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark Back View For Popovers

- (void)placeBackgoundView
{
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024.f, 1024.f)];
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0;
    [self.view addSubview:backgroundView];
    
    [UIView animateWithDuration:0.4f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        backgroundView.alpha = 0.4f;
    } completion:nil];
}

- (void)removeBackgoundView
{
    [UIView animateWithDuration:0.4f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        backgroundView.alpha = 0;
    } completion:^(BOOL finished){
        [backgroundView removeFromSuperview];
        backgroundView = nil;
    }];
}

#pragma mark Autorotate

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

#pragma mark Google Analytics

- (void)mainMenuEntered
{
    [APP_MANAGER sendAnalyticsEnteredModuleWithName:@"SMMainMenu"];
}

#pragma mark Follow Alert

- (IBAction)followAction:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Follow us", @"")
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                          otherButtonTitles:NSLocalizedString(@"Facebook", @""),
                          NSLocalizedString(@"Twitter", @""),
                          NSLocalizedString(@"LinkedIn", @""), nil];
    alert.tag = 314;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 314)
    {
        switch (buttonIndex) {
            case 0:
                //Cancel
                break;
            case 1:
                //Facebook
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://facebook.com/Smart-Mirror-675207752610138"]];
                break;
            case 2:
                //Twitter
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://twitter.com/SmartMirrorACEP"]];
                break;
            case 3:
                //LinkedIn
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.linkedin.com/company/smart-mirror"]];
                break;
        }
    }
}

#pragma mark - Provision Expiration Date Checking

- (void)showProvisionAlert
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [self placeBackgoundView];
    
    if (!provisionController)
    {
        provisionController = [storyboard instantiateViewControllerWithIdentifier:@"ProvisionAlertViewController"];
        provisionController.delegate = self;
        provisionController.view.frame = CGRectMake((self.view.bounds.size.width - 440.f) / 2,
                                                    (self.view.bounds.size.height - 180.f) / 2,
                                                    440.f,
                                                    180.f);
        
        [self.view addSubview:provisionController.view];
        
        [provisionController.view showInfoViewInPoint:CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2)];
    }
}

- (void)dismissProvisionAlert
{
    [provisionController.view hideInfoViewWithAnimation];
    provisionController = nil;
    
    [self removeBackgoundView];
}

@end
