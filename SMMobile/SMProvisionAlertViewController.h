//
//  SMProvisionAlertViewController.h
//  SMMobile
//
//  Created by Sergej Bogatenko on 3/3/18.
//  Copyright © 2018 ACEP. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SMProvisionAlertViewControllerDelegate <NSObject>

@optional
- (void)dismissProvisionAlert;
@end

@interface SMProvisionAlertViewController : UIViewController

@property (nonatomic, weak) id <SMProvisionAlertViewControllerDelegate> delegate;

@end
