//
//  SMAppManager.h
//
//  Created by Oleg Bogatenko on 10/8/15.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SMMainMenuViewController.h"

#define SMMOBILE3_URL_SCHEME @"smmobile3://"

#define APP_MANAGER [SMAppManager sharedManager]

typedef NS_ENUM (NSInteger, URLAction) {
    URLAction_Undefined      = 0,
    URLAction_ImportSessions = 1,
    URLAction_ShareLogin     = 2
};

@interface SMAppManager : NSObject

+ (instancetype)sharedManager;

- (void)setupAnalyticsEmail;

- (BOOL)isAnalyticsEmailNotifyActivated;
- (void)startAnalyticsEmailNotifications;
- (void)stopAnalyticsEmailNotifications;

- (void)saveAnalyticsEmail:(NSString *)email;
- (void)resetAnalyticsEmail;

- (void)getCameraPermissions;
- (BOOL)isCameraCanBeAvaliable;

- (void)showPermissionsAlertWithMessage:(NSString *)msg;

// ACEP Measurement Web Service Settings Data
- (BOOL)isQueueEntryModeOnDevice;
- (BOOL)isNeedToBeExportedCustomerPic;

- (NSString *)getMeasureWebServiceToken;
- (NSString *)getMeasureWedServiceStoreID;

- (void)generateAnalyticDeviceID;

- (NSString *)getAnalyticsTrackerID;
- (NSString *)getAnalyticDeviceID;
- (NSString *)getAnalyticsECPID;
- (NSString *)getLicenseForAnalyticsStoreID;
- (NSString *)getAnalyticsEmail;

- (void)sendAnalyticsEnteredModuleWithName:(NSString *)name;

- (BOOL)isMeasurementsAnalyticsEnabled;

- (void)setMainMenuPointer:(SMMainMenuViewController *)ptr;
- (void)openMLCController:(BOOL)mlc;
- (void)backToPDModule;

// Provision Expiration Date Checking
- (BOOL)isSoonExpirationDate;

- (void)setupIsNeedCheckNewProfileExpirationDate;
- (void)setupIsNeedShowAgainExpirationDateAlert:(BOOL)isNeedShowAgain;
- (BOOL)dontShowAgainExpirationDateAlert;

// Url Scheme
- (void)parseUrlScheme:(NSURL *)url;

@end
