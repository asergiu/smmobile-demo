//
//  SMSwordViewController.m
//
//  Created by Oleg Bogatenko on 10/7/14.
//

#import "SMSwordViewController.h"
#import "UIView+Effects.h"

#define RADIANS(degrees) ((degrees * M_PI) / 180.0)

@interface SMSwordViewController ()
{
    IBOutlet UIView *background;
    
    IBOutlet UITextField *swordField;
    IBOutlet UITextField *princessField;
    IBOutlet UITextField *emailField;
    
    IBOutlet UIButton *swordButton;
    IBOutlet UILabel *errorView;
    IBOutlet UIActivityIndicatorView *indicatorView;
}

- (IBAction)cancel:(id)sender;
- (IBAction)swordPressed:(id)sender;
- (IBAction)supportPressed:(id)sender;
- (void)loggedIn;

- (void)shakeView:(UIView *)view;
- (void)shakeEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context;

@end

@implementation SMSwordViewController

const float WINDOW_WIDTH  = 400.f;
const float WINDOW_HEIGHT = 420.f;
const float CORNER_RADIUS = 10.f;

@synthesize delegate;
@synthesize enterModule;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    errorView.text = NSLocalizedString(@"Please enter your login and password", nil);
    
    background.layer.cornerRadius = CORNER_RADIUS;
    self.view.layer.cornerRadius = CORNER_RADIUS;
    self.view.frame = CGRectMake(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
    
    errorView.clipsToBounds = YES;
    errorView.layer.cornerRadius = 3.f;
    errorView.alpha = 0.8f;
    
    swordField.autocorrectionType = UITextAutocorrectionTypeNo;
    emailField.autocorrectionType = UITextAutocorrectionTypeNo;
    princessField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    emailField.keyboardType = UIKeyboardTypeEmailAddress;
    
    [swordField addTarget:self
                   action:@selector(textChanged)
         forControlEvents:UIControlEventEditingChanged];
    
    [princessField addTarget:self
                      action:@selector(textChanged)
            forControlEvents:UIControlEventEditingChanged];
    
    [emailField addTarget:self
                   action:@selector(textChanged)
         forControlEvents:UIControlEventEditingChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    [self indicatorWithStatus:NO];
    
    [self textChanged];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)indicatorWithStatus:(BOOL)status
{
    indicatorView.hidden = !status;
}

- (void)swordButtonWithStatus:(BOOL)status
{
    swordButton.enabled = !status;
}

- (BOOL)stringIsNotNil:(NSString *)string
{
    return [string stringByReplacingOccurrencesOfString:@" " withString:@""].length;
}

- (void)textChanged
{
    [self setTextViewWithReason:NSLocalizedString(@"Please enter your login and password", nil) error:NO];
    
    [self swordButtonWithStatus:!([self stringIsNotNil:swordField.text] &&
                                  [self stringIsNotNil:princessField.text] &&
                                  [self stringIsNotNil:emailField.text])];
}

- (void)keyboardWasShown:(NSNotification *)aNotification
{
    [self animateChangeFrame:self.view withOffset:170.f];
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    [self animateChangeFrame:self.view withOffset:0];
}

- (void)animateChangeFrame:(UIView *)view withOffset:(CGFloat)offset
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3f];
    view.transform = CGAffineTransformMakeTranslation(0, -offset);
    [UIView commitAnimations];
}

#pragma mark Shake Sword Button

- (void)shakeView:(UIView *)view
{
    CGAffineTransform leftShake = CGAffineTransformMakeTranslation(-5, 0);
    CGAffineTransform rightShake = CGAffineTransformMakeTranslation(5, 0);
    
    view.transform = leftShake;
    
    [UIView beginAnimations:@"shake_button" context:(__bridge void *)(view)];
    [UIView setAnimationRepeatAutoreverses:YES];
    [UIView setAnimationRepeatCount:5];
    [UIView setAnimationDuration:0.06];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(shakeEnded:finished:context:)];
    
    view.transform = rightShake;
    
    [UIView commitAnimations];
}

- (void)shakeEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([finished boolValue]) {
        UIView *item = (__bridge UIView *)context;
        item.transform = CGAffineTransformIdentity;
    }
}

- (void)setTextViewWithReason:(NSString *)text
                        error:(BOOL)err
{
    [errorView setText:text];
    
    errorView.font = [UIFont fontWithName:@"TrebuchetMS" size:14.f];
    errorView.textAlignment = NSTextAlignmentCenter;
    
    if (err)
    {
        [errorView setTextColor:[UIColor whiteColor]];
        [errorView setBackgroundColor:[UIColor redColor]];
        [self shakeView:errorView];
    }
    else
    {
        [errorView setTextColor:[UIColor whiteColor]];
        [errorView setBackgroundColor:[UIColor lightGrayColor]];
    }
}

#pragma mark Actions

- (void)swordPressed:(id)sender
{
    [self.view endEditing:YES];
    
    if (![NETWORK_MANAGER networkAvaliable])
    {
        [NETWORK_MANAGER isNotOnlineAlert];
        return;
    }
    
    if (![NETWORK_MANAGER isValidEmail:emailField.text])
    {
        [self setTextViewWithReason:NSLocalizedString(@"Please check your email address!", nil) error:YES];
        return;
    }
    
    ShowNetworkActivityIndicator();
    [self indicatorWithStatus:YES];
    [self swordButtonWithStatus:YES];
    
    [NETWORK_MANAGER swordWithName:swordField.text
                          princess:princessField.text
                             email:emailField.text
                            module:enterModule
                    fillCompletion:^(BOOL state, NSString *reason) {
        
        HideNetworkActivityIndicator();
        [self indicatorWithStatus:NO];
        [self swordButtonWithStatus:NO];
        
        if (state)
            [self loggedIn];
        else
            [self setTextViewWithReason:reason error:YES];
    }];
}

#pragma mark - Delegate Actions

- (void)loggedIn
{    
    if (delegate && [delegate respondsToSelector:@selector(succesfullyLoggedIn)])
        [delegate succesfullyLoggedIn];
}

- (void)cancel:(id)sender
{
    [self.view endEditing:YES];
    
    if (delegate && [delegate respondsToSelector:@selector(cancelSword)])
        [delegate cancelSword];
}

- (void)supportPressed:(id)sender
{
    [self.view endEditing:YES];
    
    if (delegate && [delegate respondsToSelector:@selector(sendMailPressed)])
        [delegate sendMailPressed];
}

#pragma mark Dealloc

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

@end
