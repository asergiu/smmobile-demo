#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "SMMoviePLayerHelper.h"

#define degreesToRadians(x) (M_PI * x / 180.0)

@interface SMStorage : NSObject

@property (nonatomic, strong) SMMoviePLayerHelper *moviePlayerHelper;
@property (nonatomic, strong) SMMoviePLayerHelper *dublicateMoviePlayerHelper;

+ (SMStorage *)sharedInstance;

@end
