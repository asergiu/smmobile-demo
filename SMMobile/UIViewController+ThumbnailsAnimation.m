//
//  UIViewController+ThumbnailsAnimation.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 8/26/14.
//

#import "UIViewController+ThumbnailsAnimation.h"

@implementation UIViewController (ThumbnailsAnimation)

- (void)expandThumbnails:(NSArray *)group withPosition:(float)position step:(float)step
{
    [self setEnabledButtonsGroup:group
                         enabled:NO];
    
    __block int groupCounter = (int)group.count;
    
    for (UIButton *button in group)
    {
        [button slideAnimateToPosition:CGPointMake(button.frame.origin.x, button.frame.origin.y - position)
                           withHandler:^(BOOL complete) {
                               
                               if (complete)
                               {
                                   groupCounter--;
                                   
                                   if (group.count > 2 && groupCounter < 2)
                                       [self setEnabledButtonsGroup:group
                                                            enabled:YES];
                                   else if (!groupCounter)
                                       [self setEnabledButtonsGroup:group
                                                            enabled:YES];
                               }
                           }];
        
        position += step;
    }
}

- (void)hideThumbnails:(NSArray *)group withPosition:(float)position step:(float)step
{
    [self setEnabledButtonsGroup:group
                         enabled:NO];
    
    __block int groupCounter = (int)group.count;
    
    for (UIButton *button in group)
    {
        [button slideAnimateToPosition:CGPointMake(button.frame.origin.x, button.frame.origin.y + position)
                           withHandler:^(BOOL complete) {
                               
                               if (complete)
                               {
                                   groupCounter--;
                                   
                                   if (group.count > 2 && groupCounter < 2)
                                       [self setEnabledButtonsGroup:group
                                                            enabled:YES];
                                   else if (!groupCounter)
                                       [self setEnabledButtonsGroup:group
                                                            enabled:YES];
                               }
                           }];
        
        position += step;
    }
}

- (void)slideViews:(NSArray *)group xPosition:(float)xPosition yPosition:(float)yPosition step:(float)step
{
    for (UIButton *button in group)
    {
        [button slideAnimateToPosition:CGPointMake(button.frame.origin.x + xPosition, button.frame.origin.y + yPosition)
                           withHandler:nil];
        yPosition += step;
    }
}

#pragma mark - Block Views Group

- (void)setEnabledButtonsGroup:(NSArray *)group
                       enabled:(BOOL)enabled
{
    for (UIButton *button in group)
    {
        button.userInteractionEnabled = enabled;
    }
}

@end
