//
//  NSObject+EncryptVideoPAL.m
//  SMMobile
//
//  Created by Work Inteleks on 5/7/15.
//  Copyright (c) 2015 ACEP. All rights reserved.
//

#import "NSObject+EncryptVideoPAL.h"
#import "SMImageLoader.h"

@implementation NSObject (EncryptVideoPAL)

- (NSURL *)encryptFile:(NSString *)fileName fileExtantion:(NSString *)extantion
{
    NSData *videoData = [SM_LOADER getFileDataForName:fileName];
    
    NSString *appFile = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", fileName, extantion]];
    
    NSError *error = nil;
    
    if ([[NSFileManager defaultManager] isDeletableFileAtPath:appFile])
    {
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:appFile error:&error];
        
        if (!success)
        {
            NSLog(@"Error removing file at path: %@", error.localizedDescription);
        }
        else
        {
            NSLog(@"Removed!");
        }
    }
    else
    {
        NSLog(@"No media file!");
    }
    
    [videoData writeToFile:appFile atomically:YES];
    
    NSURL *url = [ NSURL fileURLWithPath : appFile ];
    
    return url;
}

@end
