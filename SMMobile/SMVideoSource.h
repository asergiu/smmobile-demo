//
//  SMVideoSource.h
//  activescreen
//
//  Created by Dgut on 09.10.13.
//

#import <UIKit/UIKit.h>

@interface SMVideoSource : NSObject

- (id)initWithFile:(NSString *)file decrypt:(BOOL)decrypt;
-( void )restart;
-( BOOL )readNext;
-( BOOL )readNextWithBlock : ( void ( ^ )( void * ptr, int width, int height ) )block;
-( float )frameRate;
-( CGSize )size;

@end
