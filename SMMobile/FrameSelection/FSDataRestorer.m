//
//  FSDataRestorer.m
//  AcepSmartMirror
//
//  Created by Oleg Bogatenko on 6/18/14.
//
//

#import "FSDataRestorer.h"

@implementation FSDataRestorer

@synthesize movies;
@synthesize photos;
@synthesize previews;
@synthesize videoMode;
@synthesize frontCamera;

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken = 0;
    static id _sharedInstance = nil;
    
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        movies   = [NSMutableDictionary dictionary];
        photos   = [NSMutableDictionary dictionary];
        previews = [NSMutableDictionary dictionary];
        
        frontCamera = YES;
    }
    return self;
}

- (void)clearData
{
    [movies removeAllObjects];
    [photos removeAllObjects];
    [previews removeAllObjects];
    
    videoMode = NO;
    frontCamera = YES;
}

@end
