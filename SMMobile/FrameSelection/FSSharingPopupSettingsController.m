//
//  FSSharingPopupSettingsController.m
//  frameselection
//
//  Created by Pavel Stoma on 2/29/12.
//

#import "FSSharingPopupSettingsController.h"

@implementation FSSharingPopupSettingsController
@synthesize cancelButton;
@synthesize saveButton;
@synthesize titleLabel;
@synthesize titleBodyTextField;
@synthesize descriptionLabel;
@synthesize descriptionTextView;
@synthesize titleNavigationBar;
@synthesize navigationTitleItem;
@synthesize noLogoLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _ecpLogoView.layer.cornerRadius = 5.0;
    
    [_addLogoButton.titleLabel setText:NSLocalizedString(@"Add logo", @"")];
    [_removeLogoButton.titleLabel setText:NSLocalizedString(@"Remove logo", @"")];
    [noLogoLabel setText:NSLocalizedString(@"No Logo", @"")];
    
    [saveButton setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateNormal];
    [saveButton setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateHighlighted];
    [saveButton setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateSelected];
    
    [cancelButton setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateNormal];
    [cancelButton setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateHighlighted];
    [cancelButton setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateSelected];
}

- (void)dealloc
{
    [self setNoLogoLabel:nil];
    [self setCancelButton:nil];
    [self setSaveButton:nil];
    [self setTitleLabel:nil];
    [self setTitleBodyTextField:nil];
    [self setDescriptionLabel:nil];
    [self setDescriptionTextView:nil];
    [self setTitleNavigationBar:nil];
    [self setNavigationTitleItem:nil];
    [self setAddLogoButton:nil];
    [self setRemoveLogoButton:nil];
    [self setEcpLogoView:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
