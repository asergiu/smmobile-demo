//
//  UIZoomImageView.h
//  KodakLensIDS
//
//  Created by Dgut on 08.04.14.
//
//

#import <UIKit/UIKit.h>

@protocol UIZoomImageViewDelegate <NSObject>

@optional
- (void)swipePhotoWithForwardDirection:(BOOL)forward;

@end

@interface UIZoomImageView : UIImageView

@property (nonatomic,assign) id <UIZoomImageViewDelegate> delegate;

-( void )saveTransform;
-( void )reset;

@end
