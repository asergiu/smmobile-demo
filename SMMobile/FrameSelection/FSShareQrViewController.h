//
//  FSShareQrViewController.h
//
//
//  Created by Evgeny Eschenko on 12/04/16.
//
//

#import <UIKit/UIKit.h>

@class FSNewViewController;

@protocol FSShareQrViewControllerDelegate <NSObject>

@optional
- (void)dismissQrSharePreview;
@end

@interface FSShareQrViewController : UIViewController

@property (nonatomic, assign) id <FSShareQrViewControllerDelegate> delegate;

@property (nonatomic, strong) FSNewViewController *activityController;

@end
