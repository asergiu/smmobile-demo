//
//  FSDataRestorer.h
//  AcepSmartMirror
//
//  Created by Oleg Bogatenko on 6/18/14.
//
//

#import <Foundation/Foundation.h>

#define FS_RESTORER [FSDataRestorer sharedInstance]

@interface FSDataRestorer : NSObject

@property (nonatomic, strong) NSMutableDictionary *movies;
@property (nonatomic, strong) NSMutableDictionary *photos;
@property (nonatomic, strong) NSMutableDictionary *previews;

@property (nonatomic, assign) BOOL videoMode;
@property (nonatomic, assign) BOOL frontCamera;

+ (instancetype)sharedInstance;

- (void)clearData;

@end
