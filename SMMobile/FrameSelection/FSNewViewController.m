#import "FSNewViewController.h"
#import "FSNewCamera.h"
#import "FSNewPlayer.h"
#import "UIZoomImageView.h"
#import "FSSharingPopupSettingsController.h"
#import "FSPopoverControllerTable.h"
#import "FSDataRestorer.h"
#import "FSAlertsHelper.h"
#import "FSShareChooseViewController.h"
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIView+Effects.h"
#import "AsyncUdpSocket.h"
#import "SSNetworkInfo.h"
#import "SMNetworkManager.h"
#import "NSString+CWAddition.h"
#import "SMAppManager.h"
#import "SMAmazonShareHelper.h"
#import "SMShareImage.h"
#import "FSShareQrViewController.h"
#import "FSShareProcessController.h"

#define ecpSaved   [[NSUserDefaults standardUserDefaults] boolForKey:@"ecpLogo"]
#define ecpPath    [[NSUserDefaults standardUserDefaults] stringForKey:@"ecpLogoPath"]
#define ecpLogoUse [[NSUserDefaults standardUserDefaults] boolForKey:@"ecpLogoUse"]

#define kUDPUpdatePeriodInSeconds 2

@interface FSNewViewController() <UIPopoverControllerDelegate, FSShareChooseViewControllerDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, FSShareQrViewControllerDelegate, FSShareProcessControllerDelegate>
{
    FSNewCamera *camera;
    
    NSTimer *udpRequestsTimer;
    AsyncUdpSocket *udpSocket;
    
    NSString *m_ftpStr;
    NSString *m_ftpAddrr;
    NSString *m_ftpSword;
    NSString *m_ftpPassw;
    
    UIImage *photos[4];
    NSURL *movies[4];
    UIImage *previews[4];
    NSURL *movieUrls[4];
    
    int currentMedia;
    int photoCount;
    int videoCount;
    float yyyyy;
    
    FSSharingPopupSettingsController *sharingPopover;
    UIView *backgroundView;
    NSString *sharingTitleBodyText;
    NSString *sharingTextDescription;
    
    IBOutlet UISwitch *useLogoSwitcher;
    IBOutlet UILabel *uselLogoLabel;
    IBOutlet UIImageView *logoOverImageView;
    IBOutlet UILabel *penToolLabel;
    
    NSArray *pickerData;
    
    SMAmazonShareHelper *amazonShare;
}

@property (nonatomic, strong) FSShareChooseViewController *sharePopover;
@property (nonatomic, strong) FSShareQrViewController *qrPopover;
@property (nonatomic, strong) FSShareProcessController *uploadPopover;

@property (nonatomic, strong) FSAlertsHelper *alertHelper;
@property (nonatomic, strong) UIPopoverController *popOverLibrary;

@end

@implementation FSNewViewController

@synthesize alertHelper;
@synthesize sharePopover;
@synthesize qrPopover;
@synthesize uploadPopover;
@synthesize urlsToQR;
@synthesize selectedMedia;
@synthesize popoverController;
@synthesize popoverTableController;
@synthesize tabBarButtons;
@synthesize tabBarImageBackground;
@synthesize videoModePicker;
@synthesize hideThumbsSwitch;
@synthesize controlPanel;
@synthesize showControlView;
@synthesize currentIndex;
@synthesize allMedia;
@synthesize mediaViews;

@synthesize settingCameraView;
@synthesize frameImage;
@synthesize sliderExposure;
@synthesize contentScrollView;
@synthesize colorButtons;
@synthesize penView;
@synthesize videoIndicatorImageView;

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        movieUrls[0] = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"fsvideo0.mov"]];
        movieUrls[1] = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"fsvideo1.mov"]];
        movieUrls[2] = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"fsvideo2.mov"]];
        movieUrls[3] = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"fsvideo3.mov"]];
        
        currentMedia = -1;
    }
    return self;
}

- (IBAction)hideThumbs
{
    for (UIImageView *borderImg in self.thumbnailBorders)
    {
        borderImg.hidden = hideThumbsSwitch.isOn;
    }
        
    for (UIImageView *thumbImg in self.thumbnailImages)
    {
        thumbImg.hidden = hideThumbsSwitch.isOn;
    }
    
    for (UIButton *thumbBtn in self.thumbnailButtons)
    {
        thumbBtn.hidden = hideThumbsSwitch.isOn;
    }
}

- (void)unloadCamera
{
    [camera stopCamera];
    camera = nil ;
}

- (void)loadCamera
{
    camera = [FSNewCamera new];
    
    [camera setPreviewContainer:self.cameraPreviewContainer];
    
    [camera setFrontCamera:!useFrontCamera];
    
    NSError *error = nil;
    
    if (IS_IOS8_AND_LATER)
    {
        if ([camera.device lockForConfiguration:&error])
            [camera.device setExposureTargetBias:0 completionHandler:nil];
    }
    
    if (FS_RESTORER.videoMode) {
        [self videoModeChanged:0];
    }
}

- (void)restoreData
{
    for (int i =0; i < 4; i++) {
        
        if (FS_RESTORER.movies[@(i)])
            movies[i] = FS_RESTORER.movies[@(i)];
        
        if (FS_RESTORER.previews[@(i)])
            previews[i] = FS_RESTORER.previews[@(i)];
        
        if (FS_RESTORER.photos[@(i)])
            photos[i] = FS_RESTORER.photos[@(i)];
    }
    
    useFrontCamera = FS_RESTORER.frontCamera;
}

#pragma mark ViewDidLoad

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    penView.hidden = YES;
    
    [sliderExposure setThumbImage:[UIImage imageNamed:@"ThumbSun"] forState:UIControlStateNormal];
    [sliderExposure setMinimumTrackImage:[UIImage imageNamed:@"sliderTrack"] forState:UIControlStateNormal];
    [sliderExposure setMaximumTrackImage:[UIImage imageNamed:@"sliderTrack"] forState:UIControlStateNormal];
    
    [penView setPenActivated:penActivated];
    currentColor = 0;
    
    currentIndex = 0;
    photoCount = 0;
    videoCount = 0;
    
    pickerData = @[NSLocalizedString(@"Photo", @""), NSLocalizedString(@"Video", @"")];
    
    videoModePicker.frame = CGRectMake(-6.0, 519.0, 76.0, 160.0);
    videoModePicker.transform = CGAffineTransformMakeScale(0.8f, 0.8f);
    
    useFrontCamera = YES;
    
    logoOverImageView.hidden = YES;
    
    if (ecpSaved)
        [logoOverImageView setImage:[UIImage imageWithContentsOfFile:ecpPath]];

    [self showUseLogoSwitcherWithState];
    [self loadPlistInDocuments];
    [self restoreData];
    
    [self loadCamera];
    
    uselLogoLabel.text = NSLocalizedString(@"Use logo", nil);
    
    self.cameraPreviewContainer.userInteractionEnabled = YES;
    self.cameraPreviewContainer.multipleTouchEnabled = YES;
    
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinch:)];
    [self.cameraPreviewContainer addGestureRecognizer:pinchRecognizer];
    
    for(FSNewPlayer *player in self.multiMoviePreview)
        [player buildPlayer : NO];
    
    [zoomSlider addTarget:self
                   action:@selector(sliderDidEndSliding:)
         forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchUpOutside)];
    
    [zoomSlider addTarget:self
                   action:@selector(sliderDidStartSliding:)
         forControlEvents:(UIControlEventTouchDown | UIControlEventTouchDragOutside)];
    
    [self refreshThumbnails];
    
    alertHelper = [[FSAlertsHelper alloc] initWithOwner:self];
    
    UISwipeGestureRecognizer *gesture1 =[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesture:)];
    [gesture1 setDirection:UISwipeGestureRecognizerDirectionLeft];
    [showControlView addGestureRecognizer:gesture1];
    
    UISwipeGestureRecognizer *gesture2 =[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesture:)];
    [gesture2 setDirection:UISwipeGestureRecognizerDirectionRight];
    [showControlView addGestureRecognizer:gesture2];
    
    UISwipeGestureRecognizer *gesture3 =[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesture:)];
    [gesture3 setDirection:UISwipeGestureRecognizerDirectionRight];
    [controlPanel addGestureRecognizer:gesture3];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    contentScrollView.delegate = self;
    
    UITapGestureRecognizer *cameraTap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showExposureView:)];
    [self.cameraPreviewContainer addGestureRecognizer:cameraTap];
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(changeExposure:)];
    [self.cameraPreviewContainer addGestureRecognizer:panRecognizer];
    
    yyyyy = 0;
    
    videoIndicatorImageView.hidden = YES;
}

#pragma mark - Change Exposure

- (void)changeExposure:(UIGestureRecognizer *)recognizer
{
   [self sliderExposureBegan:nil];
    
    CGPoint tappedPoint = [recognizer locationInView:self.view];
    CGFloat yCoordinate = tappedPoint.y;
    
    float value = sliderExposure.value;
    
    if (settingCameraView.alpha == 1) {
        
        if (yyyyy > yCoordinate) {
            [sliderExposure setValue:value + 0.2 animated:YES];
            [self sliderExposureAction:sliderExposure];
            yyyyy = yCoordinate;
        } else {
            [sliderExposure setValue:value - 0.2 animated:YES];
            [self sliderExposureAction:sliderExposure];
            yyyyy = yCoordinate;
        }
    }
    
    [self sliderExposureEnded:nil];
}

#pragma mark ScrollView Delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    currentIndex = (contentScrollView.contentOffset.x / 1024.f) - 1;
    
    [self workWithSlideContent];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    currentIndex = (contentScrollView.contentOffset.x / 1024.f) - 1;
    
    [self workWithSlideContent];
}

- (void)workWithSlideContent
{
    if (contentScrollView.contentOffset.x > 1024.f * allMedia.count)
    {
        [contentScrollView scrollRectToVisible:CGRectMake(1024.0, 0.0, 1024.0, 768.0) animated:NO];
        currentIndex = (contentScrollView.contentOffset.x / 1024.f) - 1;
    }
    
    if (contentScrollView.contentOffset.x < 1024.f)
    {
        [contentScrollView scrollRectToVisible:CGRectMake(1024.0 * allMedia.count, 0.0, 1024.0, 768.0) animated:NO];
        currentIndex = (contentScrollView.contentOffset.x / 1024.f) - 1;
    }

    if ([[allMedia objectAtIndex:currentIndex] objectForKey:@"photo"])
    {
        if (camera.videoMode)
        {
            [self videoModeChanged:0];
            
            [videoModePicker selectRow:0 inComponent:0 animated:YES];
            
            penView.hidden = NO;
        }
        
        self.cameraPreviewContainer.hidden = YES;
        
        [((UIZoomImageView *)[mediaViews objectAtIndex:currentIndex]) reset];
        
    } else {
        
        if (!camera.videoMode)
        {
            [self videoModeChanged:1];
            
            [videoModePicker selectRow:1 inComponent:0 animated:YES];
        }
        
        self.cameraPreviewContainer.hidden = YES;
        
        for (UIView *view in contentScrollView.subviews)
        {
            if ([view isKindOfClass:[FSNewPlayer class]])
            {
                [((FSNewPlayer *)view) stop];
            }
        }
        
        if ([[mediaViews objectAtIndex:currentIndex] isKindOfClass:[FSNewPlayer class]])
        {
            [((FSNewPlayer *)[mediaViews objectAtIndex:currentIndex]) play];
        }
    }
    
    int ind = [[[allMedia objectAtIndex:currentIndex] objectForKey:@"index"] intValue];
    currentMedia = ind;
    [self highlightBorder:ind];
    
    if (ecpLogoUse)
        logoOverImageView.hidden = NO;
}

#pragma mark Photo Swipe With Direction

- (void)swipePhotoWithForwardDirection:(BOOL)forward
{
    if (!penActivated)
    {
        float offcet = forward ? 1024.f : -1024.f;

        [contentScrollView scrollRectToVisible:CGRectMake (contentScrollView.contentOffset.x + offcet, 0.0, 1024.0, 768.0) animated:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [self addMediaToArray];
    [[videoModePicker.subviews objectAtIndex:1] setHidden:YES];
    [[videoModePicker.subviews objectAtIndex:2] setHidden:YES];
    
    [self moduleEntered];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerData.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return pickerData[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self videoModeChanged:(int)row];
}

- (NSAttributedString *)pickerView:(UIPickerView *)thePickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *value;
    
    value = pickerData[row];
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:value attributes:@{ NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"TrebuchetMS" size:15.f] }];
    
    return attString;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40.f;
}

- (void)swipeGesture:(UISwipeGestureRecognizer *)swipeGes
{
    if (!penActivated) {
    
        if (swipeGes.direction == UISwipeGestureRecognizerDirectionLeft) {
            [controlPanel showSwipeAnimation];
        } else if (swipeGes.direction == UISwipeGestureRecognizerDirectionRight) {
            [controlPanel hideSwipeAnimation];
        }
    }
}

- (void)sliderDidStartSliding:(NSNotification *)notification
{
    [timer invalidate];
    timer = nil;
}

- (void)sliderDidEndSliding:(NSNotification *)notification
{
    [self starTimerHiddenSlider];
}

- (void)starTimerHiddenSlider
{
    timer = [NSTimer scheduledTimerWithTimeInterval:3.0
                                             target:self
                                           selector:@selector(hideSlider)
                                           userInfo:nil
                                            repeats:NO];
}

- (void)showSlider
{
    [UIView animateWithDuration:0.2 animations:^{
        sliderView.alpha = 1;
    } completion: ^(BOOL finished) {
        sliderView.hidden = !finished;
    }];
}

- (void)hideSlider
{
    [UIView animateWithDuration:0.4 animations:^{
        sliderView.alpha = 0;
    } completion: ^(BOOL finished) {
        sliderView.hidden = finished;
    }];
}

- (void)focusWithMode:(AVCaptureFocusMode)focusMode exposeWithMode:(AVCaptureExposureMode)exposureMode atDevicePoint:(CGPoint)point monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange
{
    AVCaptureDevice *device = camera.device;
    NSError *error = nil;
    
    if ([device lockForConfiguration:&error]) {
        
        if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:focusMode]) {
            
            [device setFocusMode:focusMode];
            [device setFocusPointOfInterest:point];
        }
        
        if ([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:exposureMode]) {
            
            [device setExposureMode:exposureMode];
            [device setExposurePointOfInterest:point];
        }
        
        [device setSubjectAreaChangeMonitoringEnabled:monitorSubjectAreaChange];
        [device unlockForConfiguration];
    
    } else {
        NSLog(@"%@", error);
    }
}

#pragma mark - Show & Setup Exposure View

- (void)showExposureView:(UIGestureRecognizer *)recognizer
{
    if (!(IS_IOS8_AND_LATER))
    {
        return;
    }
    
    [self sliderExposureBegan:nil];
    
    CGPoint tappedPoint = [recognizer locationInView:self.view];
    CGFloat xCoordinate = tappedPoint.x;
    CGFloat yCoordinate = tappedPoint.y;
    
    // Don't show exposure control near thumbs and home button
    if (xCoordinate < 248.f) {
        return;
    }
    
    self.sliderExposure.minimumValue = camera.device.minExposureTargetBias / 2.f;
    self.sliderExposure.maximumValue = camera.device.maxExposureTargetBias / 2.6f;
    
    self.sliderExposure.value = camera.device.exposureTargetBias;

    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI_2 * 3);
    sliderExposure.transform = trans;
    
    settingCameraView.hidden = YES;
    
    settingCameraView.frame = CGRectMake(xCoordinate - (settingCameraView.frame.size.width / 2),
                                         yCoordinate - (settingCameraView.frame.size.height / 2),
                                         settingCameraView.frame.size.width,
                                         settingCameraView.frame.size.height);
    
    
    if (camera.device.focusMode != AVCaptureFocusModeLocked && camera.device.exposureMode != AVCaptureExposureModeCustom) {
        
        CGPoint devicePoint = [recognizer locationInView:self.view];
        
        [self focusWithMode:AVCaptureFocusModeContinuousAutoFocus
             exposeWithMode:AVCaptureExposureModeContinuousAutoExposure
              atDevicePoint:devicePoint monitorSubjectAreaChange:YES];
    }
    
    [self showExposureSliderView];
    
    [self sliderExposureEnded:nil];
}

- (void)showExposureSliderView
{
    [UIView animateWithDuration:0.2 animations:^{
        settingCameraView.alpha = 1;
    } completion: ^(BOOL finished) {
        settingCameraView.hidden = !finished;
    }];
}

- (void)hideExposureSliderView
{
    [UIView animateWithDuration:0.4 animations:^{
        settingCameraView.alpha = 0;
    } completion: ^(BOOL finished) {
        settingCameraView.hidden = finished;
    }];
}

- (IBAction)sliderExposureBegan:(id)sender;
{
    [timerHideExposure invalidate];
    timerHideExposure = nil;
}

- (IBAction)sliderExposureEnded:(id)sender
{
    [self starSliderCameraTimerHidden];
}

- (void)starSliderCameraTimerHidden
{
    timerHideExposure = [NSTimer scheduledTimerWithTimeInterval:3.0
                                             target:self
                                           selector:@selector(hideExposureSliderView)
                                           userInfo:nil
                                            repeats:NO];
}

- (IBAction)sliderExposureAction:(UISlider *)sender
{
    NSError *error = nil;
    
    if (IS_IOS8_AND_LATER)
    {
        if ([camera.device lockForConfiguration:&error])
            [camera.device setExposureTargetBias:sender.value / 2.f completionHandler:nil];
    }

    [self sliderExposureBegan:nil];
}

- (void)pinch:(UIPinchGestureRecognizer *)sender
{
    static float scale = 1.f;
    
    switch (sender.state) {
            
        case UIGestureRecognizerStateBegan:
        {
            scale = camera.scale;
            
            if (!camera.videoMode || [camera deviceScaleSupport]) {
                [self showSlider];
            }
        }
            break;
            
        case UIGestureRecognizerStateChanged:
            camera.scale = scale * sender.scale;
            [zoomSlider setValue:(camera.scale - 1) animated:YES];
            [self sliderDidStartSliding:nil];
            break;
            
        case UIGestureRecognizerStateEnded:
            camera.scale = scale * sender.scale;
            [self sliderDidEndSliding:nil];
            break;
            
        default:
            break;
    }
}

- (IBAction)pressTabBarButton:(UIButton *)sender
{
    if (penActivated)
        [self closeColors:nil];
    
    switch (sender.tag)
    {
        case 0:
            
            [self shareTabPressed];
            break;
            
        case 1:
            
            [self compare];
            break;
            
        case 2:
            
            [self showDeleteStartOverAlert];
            break;
            
        case 3:
            
            [self deleteCurrent];
            break;
            
        case 4:
            
            [self settingsItemTabPressed];
            break;
            
        default:
            break;
    }
}

- (void)showDeleteStartOverAlert
{
    if ([self checkImagesArrayIsNotNil] || [self checkMoviesArrayIsNotNil])
    {
        [alertHelper showAlert:FSAlertTypeDeleteContent];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{    
    if (alertView.tag == FSAlertTypeDeleteContent) {
        
        switch (buttonIndex) {
            case 0:
                penView.hidden = YES;
                [self reset];
                break;
            case 1:
                break;
        }
    }
}

- (BOOL)checkImagesArrayIsNotNil
{
    for (int i = 0; i < 4; i++) {
        
        if (photos[i] != nil)
            return YES;
    }
    
    return NO;
}

- (BOOL)checkMoviesArrayIsNotNil
{
    for (int i = 0; i < 4; i++) {
        
        if (movies[i] != nil)
            return YES;
    }
    
    return NO;
}

- (void)shareTabPressed
{
    if ([self checkImagesArrayIsNotNil] && ![self videoMode]) {
        
        [self sharePressed];
    
    } else if ([self checkMoviesArrayIsNotNil] && [self videoMode]) {
        
        [self sharePressed];
    
    } else {
        
        [alertHelper showAlert:FSAlertTypeNoContentForMode];
    }
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}

- (void)settingsItemTabPressed
{
    sharingPopover = [[FSSharingPopupSettingsController alloc] initWithNibName:@"FSSharingPopupSettingsController" bundle:nil];
    
    backgroundView = [[UIView alloc] init];
    backgroundView.frame = CGRectMake(0.0, 0.0, 1024.0, 1024.0);
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0.4;
    [self.view addSubview:backgroundView];
    
    sharingPopover.view.alpha = 0.0;
    sharingPopover.view.frame = CGRectMake(sharingPopover.view.frame.origin.x, self.view.frame.size.height, sharingPopover.view.frame.size.width, sharingPopover.view.frame.size.height);
    [self.view addSubview:sharingPopover.view];
    [self configureSharingTexts];
    
    sharingPopover.view.center = CGPointMake(512, 768 + sharingPopover.view.frame.size.height / 2);
    
    [sharingPopover.view slideViewWithAnimationToPoint:CGPointMake(512.f, 384.f)];
    
    [sharingPopover.addLogoButton setTitle:NSLocalizedString(@"Add logo", @"") forState:UIControlStateNormal];
    [sharingPopover.removeLogoButton setTitle:NSLocalizedString(@"Remove logo", @"") forState:UIControlStateNormal];
    
    [sharingPopover.removeLogoButton addTarget:self action:@selector(removeECPLogo) forControlEvents:UIControlEventTouchUpInside];
    [sharingPopover.addLogoButton addTarget:self action:@selector(addECPLogo) forControlEvents:UIControlEventTouchUpInside];
    
    if (ecpSaved) {
        [sharingPopover.ecpLogoView setImage:[UIImage imageWithContentsOfFile:ecpPath]];
        sharingPopover.noLogoLabel.hidden = YES;
    }
    
    [sharingPopover.cancelButton addTarget:self action:@selector(cancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [sharingPopover.saveButton addTarget:self action:@selector(saveButtonPressed) forControlEvents:UIControlEventTouchUpInside];
}

- (IBAction)segControlClicked:(id)sender
{
    sharingTitleBodyText = [NSString stringWithString:sharingPopover.titleBodyTextField.text];
    sharingTextDescription = [NSString stringWithString:sharingPopover.descriptionTextView.text];
    [self configureSharingTexts];
}

- (void)cancelButtonPressed
{
    [self.view endEditing:YES];
    
    [self configureSharingTexts];
    
    [UIView animateWithDuration:0.2 animations:^{
        backgroundView.alpha = 0.0;
    } completion:^(BOOL finished){
        [backgroundView removeFromSuperview];
        backgroundView = nil;
    }];
    
    [sharingPopover.view hideViewWithAnimation];
    
    sharingPopover = nil;
}

- (void)saveButtonPressed
{
    [self.view endEditing:YES];
    
    if ([self checkStringIsNotNil:sharingPopover.titleBodyTextField.text] && [self checkStringIsNotNil:sharingPopover.descriptionTextView.text])
    {
        [FSNewViewController changeLocalizationValue:sharingPopover.titleBodyTextField.text forKey:@"MAIL_TITLE"];
        [FSNewViewController changeLocalizationValue:sharingPopover.descriptionTextView.text forKey:@"MAIL_BODY"];
    }
    
    if ([self checkStringIsNotNil:sharingTitleBodyText] && [self checkStringIsNotNil:sharingTextDescription])
    {
        [FSNewViewController changeLocalizationValue:sharingTitleBodyText forKey:@"MAIL_TITLE"];
        [FSNewViewController changeLocalizationValue:sharingTextDescription forKey:@"MAIL_BODY"];
    }
    
    [self cancelButtonPressed];
}

- (BOOL)checkStringIsNotNil:(NSString *)string
{
    NSString *stringWithoutSpaces = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    return stringWithoutSpaces.length;
}

- (void)configureSharingTexts
{
    sharingPopover.cancelButton.titleLabel.text = NSLocalizedString(@"CANCEL", "");
    sharingPopover.saveButton.titleLabel.text = NSLocalizedString(@"SAVE", "");

    sharingPopover.titleLabel.text = NSLocalizedString(@"SUBJECT_TITLE_LABEL", "");
    sharingPopover.descriptionLabel.text = NSLocalizedString(@"MESSAGE_BODY_LABEL", "");
    
    sharingPopover.titleBodyTextField.text = [FSNewViewController fetchLocalizationValueForKey:@"MAIL_TITLE"];
    sharingPopover.descriptionTextView.text = [FSNewViewController fetchLocalizationValueForKey:@"MAIL_BODY"];
    [sharingPopover.view.layer setCornerRadius:6.0f];
    [sharingPopover.view setNeedsLayout];
}

- (IBAction)dismissView:(id)sender
{
    [self stopAllVideo];
    
    [self unloadCamera];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)changeCamera:(id)sender
{
    camera.scale = 1.f;
    
    if (useFrontCamera) {
        [ camera setFrontCamera : useFrontCamera ];
        useFrontCamera = NO;
    } else {
        [ camera setFrontCamera : useFrontCamera ];
        useFrontCamera = YES;
    }
    
    FS_RESTORER.frontCamera = useFrontCamera;
}

- (void)addMediaToArray
{
    for (UIView *view in contentScrollView.subviews)
    {
        if ([view isKindOfClass:[FSNewPlayer class]])
            [((FSNewPlayer *)view) stop];

        [view removeFromSuperview];
    }
    
    if (!allMedia)
        allMedia = [NSMutableArray new];
    else
        [allMedia removeAllObjects];
    
    if (!mediaViews)
        mediaViews = [NSMutableArray new];
    else
        [mediaViews removeAllObjects];
    
    photoCount = 0;
    
    for (int i = 0; i < 4; i++) {
        
        if (photos[i] != nil)
        {
            NSMutableDictionary *item = [NSMutableDictionary new];
            [item setObject:photos[i] forKey:@"photo"];
            [item setObject:@(i) forKey:@"index"];
            [allMedia addObject:item];
            
            photoCount++;
        }
    }
    
    videoCount = 0;
    
    for (int i = 0; i < 4; i++) {
        
        if (movies[i] != nil)
        {
            NSMutableDictionary *item = [NSMutableDictionary new];
            [item setObject:movies[i] forKey:@"video"];
            [item setObject:@(i) forKey:@"index"];
            [allMedia addObject:item];
            
            videoCount++;
        }
    }
    
    if (allMedia.count > 0) {
        
        contentScrollView.frame = CGRectMake(0.0, 0.0, 1024.f, 768.f);
        contentScrollView.contentSize = CGSizeMake(1024.f * (allMedia.count + 2), 768.f);
        contentScrollView.backgroundColor = [UIColor blackColor];
        contentScrollView.hidden = YES;
    
        for (int i = 0; i < allMedia.count; i++)
        {
            if ([[allMedia objectAtIndex:i] objectForKey:@"photo"]) {
                [self createImageViewToScroll:1024.0 * (i + 1) index:i addArray:YES];
            } else {
                [self createVideoViewToScroll:1024.0 * (i + 1) index:i addArray:YES];
            }
        }
        
        if ([[allMedia firstObject]objectForKey:@"photo"]) {
            [self createImageViewToScroll:1024.0 * (allMedia.count + 1) index:0 addArray:NO];
        } else {
            [self createVideoViewToScroll:1024.0 * (allMedia.count + 1) index:0 addArray:NO];
        }
        
        if ([[allMedia lastObject]objectForKey:@"photo"]) {
            [self createImageViewToScroll:0.0 index:(int)(allMedia.count - 1) addArray:NO];
        } else {
            [self createVideoViewToScroll:0.0 index:(int)(allMedia.count - 1) addArray:NO];
        }
        
        [contentScrollView setBlindZoneWithRect:CGRectMake(0, 768.f - 65.f, contentScrollView.contentSize.width, 65.f)];
    }
}

- (void)createImageViewToScroll:(float)size index:(int)i addArray:(BOOL)add
{
    UIView *frameView = [[UIView alloc] initWithFrame:CGRectMake(size, 0.0, 1024.0, 768.0)];
    frameView.clipsToBounds = YES;
    [contentScrollView addSubview:frameView];
    
    UILabel *numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(910.0, 640.0, 100.0, 100.0)];
    numberLabel.text = [NSString stringWithFormat:@"%d", [[[allMedia objectAtIndex:i] objectForKey:@"index"] intValue] + 1];
    [numberLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:50]];
    numberLabel.textColor = [UIColor whiteColor];
    numberLabel.shadowColor = [UIColor darkGrayColor];
    numberLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    
    UIZoomImageView *viewPhoto = [[UIZoomImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 1024.0, 768.0)];
    viewPhoto.delegate = self;
    [viewPhoto saveTransform];
    [frameView addSubview:viewPhoto];
    viewPhoto.image = [[allMedia objectAtIndex:i] objectForKey:@"photo"];
    
    [viewPhoto addSubview:numberLabel];
    
    if (add) {
        [mediaViews addObject:viewPhoto];
    }
}

- (void)createVideoViewToScroll:(float)size index:(int)i addArray:(BOOL)add
{
    FSNewPlayer *viewVideo = [[FSNewPlayer alloc] initWithFrame:CGRectMake(size, 0.0, 1024.0, 768.0)];
    [viewVideo buildPlayer:YES];
    [contentScrollView addSubview:viewVideo];

    [viewVideo loadURL:[[allMedia objectAtIndex:i] objectForKey:@"video"]];
    [viewVideo setContentMode: UIViewContentModeScaleAspectFill];
    
    UILabel *numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(910.0, 640.0, 100.0, 100.0)];
    numberLabel.text = [NSString stringWithFormat:@"%d", [[[allMedia objectAtIndex:i] objectForKey:@"index"] intValue] + 1];
    [numberLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:50]];
    numberLabel.textColor = [UIColor whiteColor];
    numberLabel.shadowColor = [UIColor darkGrayColor];
    numberLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    
    [viewVideo addSubview:numberLabel];
    
    if (add) {
        [mediaViews addObject:viewVideo];
    }
}

- (IBAction)thumbnailTouched:(UIButton *)sender
{
    compareMode = NO;
    
    [self stopAllVideo];
    
    self.singleViewContainer.hidden = NO;
    self.multiViewContainer.hidden = YES;
    
    if (camera.videoMode) {
        
        if (camera.isRecording)
            return;
        
        if ([self showMovie:(int)sender.tag])
            return;
        
        self.cameraPreviewContainer.hidden = NO;
        
        [self setECPLogoHidden:YES];
        
        if (currentMedia != -1) {
            
            [self highlightBorder:-1];
            currentMedia = -1;
            return;
        }
        
        if ([camera takeVideo:movieUrls[sender.tag]:^(NSURL *url) {
            
            [self highlightBorder:-1];
            [self playSound:@"stop_video3.wav"];
            [self stopAnimationIndicator];
            
            movies[sender.tag] = url;
            
            FS_RESTORER.movies[@(sender.tag)] = url;
            
            UIImage *preview = [self getMoviePreview:url time:CMTimeMake(0, 30)];
            previews[sender.tag] = preview;
            
            FS_RESTORER.previews[@(sender.tag)] = preview;
            
            [self refreshThumbnails];
            
            [self addMediaToArray];
            
            if (previews[0] && previews[1] && previews[2] && previews[3])
                [self showMovie:(int)sender.tag];
        } ] )
        {
            [self highlightBorder:(int)sender.tag];
            [self playSound:@"stop_video.wav"];
            [self startAnimationIndicator];
        }
    
    } else {
        
        if ([self showPhoto:(int)sender.tag]) {
            
            [self setECPLogoHidden:NO];
            
            penToolButton.hidden = NO;
            penToolLabel.hidden = NO;
            
            penView.hidden = NO;
        
            return;
        }
        
        penView.hidden = YES;
        
        self.cameraPreviewContainer.hidden = NO;
        
        if (currentMedia != -1) {
            
            [self highlightBorder:-1];
            currentMedia = -1;
            
            [self setECPLogoHidden:YES];
            
            return;
        }
        
        [self highlightBorder:(int)sender.tag];
    
        [camera takePhoto: ^( UIImage * image ) {
            
            [ self highlightBorder : -1 ];
            
            photos[ sender.tag ] = image;
            
            FS_RESTORER.photos[@(sender.tag)] = image;
            
            [ self refreshThumbnails ];
            
            [self addMediaToArray];
            
            if (photos[ 0 ] && photos[ 1 ] && photos[ 2 ] && photos[ 3 ] ) {
                
                [self showPhoto:(int)sender.tag];
                
                [self setECPLogoHidden:NO];
                
                penView.hidden = NO;
            }
            
        } ];
    }
}

-( IBAction )compareThumbnailTouched : ( UIButton * )sender
{
    compareMode = NO;
    
    [self setECPLogoHidden:NO];
    
    if( camera.videoMode )
        [ self showMovie : (int)sender.tag ];
    else
    {
        [ self showPhoto : (int)sender.tag ];
        
        penToolButton.hidden = NO;
        penToolLabel.hidden = NO;
        
        penView.hidden = NO;
    }
}

-( void )highlightBorder : ( int )tag
{
    for( UIImageView * border in self.thumbnailBorders )
        border.highlighted = tag == border.tag;
}

- (UIImage *)getMoviePreviewForUrl:(NSURL *)url
{
    return [self getMoviePreview:url time:CMTimeMake( 0, 30 )];
}

-( UIImage * )getMoviePreview : ( NSURL * )url time : ( CMTime )time
{
    AVURLAsset * asset = [ [ AVURLAsset alloc ] initWithURL : url options : nil ];
    AVAssetImageGenerator * generate = [ [ AVAssetImageGenerator alloc ] initWithAsset : asset ];
    generate.appliesPreferredTrackTransform = YES;
    NSError * err = NULL;
    CGImageRef image = [ generate copyCGImageAtTime : time actualTime : NULL error : &err ];
    
    return [ [ UIImage alloc ] initWithCGImage : image ];
}

-( void )playSound : ( NSString * )filename
{
    NSURL * url = [ NSURL fileURLWithPath : [ [ NSBundle mainBundle ].resourcePath stringByAppendingPathComponent : filename ] ];
    SystemSoundID soundID;
    AudioServicesCreateSystemSoundID( ( __bridge_retained CFURLRef )url, &soundID );
    AudioServicesPlaySystemSound( soundID );
}

-( IBAction )videoModeChanged:(int)mode
{
    if (mode == 1)
    {
        if (penActivated)
            [self closeColors:nil];
        
        penToolButton.hidden = YES;
        penToolLabel.hidden = YES;
        videoIndicatorImageView.hidden = NO;
    }
    else
    {
        penToolButton.hidden = NO;
        penToolLabel.hidden = NO;
        videoIndicatorImageView.hidden = YES;
    }
    
    penView.hidden = YES;
    
    [self hideSlider];
    
    compareMode = NO;
    currentMedia = -1;
    
    camera.scale = 1.f;
    
    camera.videoMode = mode;
    
    [ camera setVideoMode : camera.videoMode ];
    
    self.singleViewContainer.hidden = NO;
    self.multiViewContainer.hidden = YES;
    self.cameraPreviewContainer.hidden = NO;
    
    for( int i = 0; i < 4; i++ )
    {
        UIView * player = self.multiMoviePreview[ i ];
        player.hidden = !camera.videoMode;
    }
    
    if (hideThumbsSwitch.isOn)
    {
        [self hideThumbs];
    }
    
    [ self highlightBorder : -1 ];
    [ self refreshThumbnails ];
    
    [self setECPLogoHidden:YES];
    
    FS_RESTORER.videoMode = camera.videoMode;
}

-( IBAction )sliderAction : ( UISlider * )sender
{
    [ camera setScale : sender.value ];
}

-( void )reset
{
    compareMode = NO;
    
    camera.scale = 1.f;
    
    [self setECPLogoHidden:YES];
    
    for( int i = 0; i < 4; i++ )
    {
        [ [ NSFileManager defaultManager ] removeItemAtURL : movies[ i ] error : NULL ];
        
        photos[ i ] = nil;
        movies[ i ] = nil;
        previews[ i ] = nil;
    }
    
    self.singleViewContainer.hidden = NO;
    self.multiViewContainer.hidden = YES;
    self.cameraPreviewContainer.hidden = NO;
    
    [ self refreshThumbnails ];
    [ self highlightBorder : -1 ];
    currentMedia = -1;
    
    [FS_RESTORER clearData];
    
    [self addMediaToArray];
}

-( void )compare
{
    if( camera.videoMode )
    {
        if ([self checkMoviesArrayIsNotNil]) {
        
            int firstIndex = -1;
        
            for( int i = 0; i < 4; i++ )
            {
                FSNewPlayer * player = self.multiMoviePreview[ i ];
                [ player loadURL : movies[ i ] ];
            
                if( movies[ i ] && firstIndex == -1 ){
                    firstIndex = i;
                }
            }
        
            if( firstIndex != -1 )
            {
                FSNewPlayer * player = self.multiMoviePreview[ firstIndex ];
                [ player play ];
            }
            
            self.singleViewContainer.hidden = YES;
            self.multiViewContainer.hidden = NO;
            self.cameraPreviewContainer.hidden = YES;
            
            [ self highlightBorder : -1 ];
            currentMedia = 0;
        }
        else
            return;
    }
    else
    {
        if ([self checkImagesArrayIsNotNil]) {
        
            for( int i = 0; i < 4; i++ )
            {
                UIImageView * image = self.multiImagePreview[ i ];
                image.image = photos[ i ];
            }
            
            self.singleViewContainer.hidden = YES;
            self.multiViewContainer.hidden = NO;
            self.cameraPreviewContainer.hidden = YES;
            
            [ self highlightBorder : -1 ];
            currentMedia = 0;
        }
        else
            return;
    }
    
    compareMode = YES;
    
    [self setECPLogoHidden:YES];
    
    penToolButton.hidden = YES;
    penToolLabel.hidden = YES;
}

- (void)stopAllVideo
{
    for (FSNewPlayer *player in self.multiMoviePreview)
    {
        [player stop];
    }
}

-( void )deleteCurrent
{
    if (compareMode)
        return;

    if( currentMedia == -1 )
        return;
    
    if( camera.videoMode )
    {
        movies[ currentMedia ] = nil;
        previews[ currentMedia ] = nil;
        
        [FS_RESTORER.movies removeObjectForKey:@(currentMedia)];
        [FS_RESTORER.previews removeObjectForKey:@(currentMedia)];
    }
    else
    {
        photos[ currentMedia ] = nil;
        
        [FS_RESTORER.photos removeObjectForKey:@(currentMedia)];
    }
    
    [self setECPLogoHidden:YES];
    
    self.singleViewContainer.hidden = NO;
    self.multiViewContainer.hidden = YES;
    self.cameraPreviewContainer.hidden = NO;
    
    [ self refreshThumbnails ];
    [ self highlightBorder : -1 ];
    currentMedia = -1;
    
    [self addMediaToArray];
    
    penView.hidden = YES;
}

-( void )refreshThumbnails
{
    BOOL video = camera.videoMode;
    
    for( int i = 0; i < 4; i++ )
    {
        UIImageView * thumbnail = self.thumbnailImages[ i ];
        thumbnail.image = ( video ? previews : photos )[ i ];
    }
}

-( int )selectedMediaIndex
{
    return currentMedia;
}

-( UIImage * )getPhoto : ( int )index
{
    return photos[ index ];
}

-( NSURL * )getMovieURL : ( int )index
{
    return movies[ index ];
}

-( BOOL )videoMode
{
    return camera.videoMode;
}

-( BOOL )showPhoto : ( int )index
{
    if( !photos[ index ] )
        return NO;
    
    self.singleViewContainer.hidden = NO;
    self.multiViewContainer.hidden = YES;
    self.cameraPreviewContainer.hidden = YES;
    
    [ self highlightBorder : index ];

    currentMedia = index;
    
    NSMutableArray *allScrollIndex = [[NSMutableArray alloc] init];
    
    NSNumber *n = [[NSNumber alloc] initWithFloat:1024.0];
    NSNumber *s = [[NSNumber alloc] initWithFloat:0.0];
    
    for (int i = 0; i < 4; i++) {
        
        if (!photos[i]) {
            [allScrollIndex addObject:s];
        } else {
            [allScrollIndex addObject:n];
        }
    }
    
    if (index == 0) {
        [contentScrollView scrollRectToVisible:CGRectMake ([[allScrollIndex objectAtIndex:0]floatValue], 0.0, 1024.0, 768.0) animated:NO];
    
    } else if (index == 1) {
        [contentScrollView scrollRectToVisible:CGRectMake ([[allScrollIndex objectAtIndex:0]floatValue] +
                                                           [[allScrollIndex objectAtIndex:1]floatValue], 0.0, 1024.0, 768.0) animated:NO];
        
    } else if (index == 2) {
        [contentScrollView scrollRectToVisible:CGRectMake ([[allScrollIndex objectAtIndex:0]floatValue] +
                                                           [[allScrollIndex objectAtIndex:1]floatValue] +
                                                           [[allScrollIndex objectAtIndex:2]floatValue], 0.0, 1024.0, 768.0) animated:NO];
        
    } else if (index == 3) {
        [contentScrollView scrollRectToVisible:CGRectMake ([[allScrollIndex objectAtIndex:0]floatValue] +
                                                           [[allScrollIndex objectAtIndex:1]floatValue] +
                                                           [[allScrollIndex objectAtIndex:2]floatValue] +
                                                           [[allScrollIndex objectAtIndex:3]floatValue], 0.0, 1024.0, 768.0) animated:NO];
    }
    
    allScrollIndex = nil;

    contentScrollView.hidden = NO;
    
    return YES;
}

-( BOOL )showMovie : ( int )index
{
    if( !movies[ index ] )
        return NO;
    
    [ self highlightBorder : index ];
    
    currentMedia = index;
    
    self.singleViewContainer.hidden = NO;
    self.multiViewContainer.hidden = YES;
    self.cameraPreviewContainer.hidden = YES;
    
    self.substrateImage.image = [ UIImage imageNamed : [ NSString stringWithFormat : @"substrate%i", index + 1 ] ];
    
    if (ecpLogoUse)
        logoOverImageView.hidden = NO;
    
    int scrollIndex = 0;
    
    for (int i = (photoCount != 0 ? (photoCount - 1) : 0); i < allMedia.count; i++) {
        
        if ([[allMedia objectAtIndex:i] objectForKey:@"video"]) {
            
            if ([[[allMedia objectAtIndex:i] objectForKey:@"index"]integerValue] == index) {
                
                scrollIndex = i;
            }
        }
    }
    
    [contentScrollView scrollRectToVisible:CGRectMake (1024.0 + (1024.f * (scrollIndex)), 0.0, 1024.0, 768.0) animated:NO];
    contentScrollView.hidden = NO;
    
    [self performSelector:@selector(playAfterScroll:) withObject:@((scrollIndex)) afterDelay:0.1f];
    
    return YES;
}

- (void)playAfterScroll:(NSNumber *)index
{
    if ([[mediaViews objectAtIndex:([index intValue])] isKindOfClass:[FSNewPlayer class]])
    {
        [((FSNewPlayer *)[mediaViews objectAtIndex:([index intValue])]) play];
    }
}

- (void)newPlayerFinished:(FSNewPlayer *)player
{
    if( player == self.singleMoviePreview )
    {
        [ player rewind ];
        [ player play ];
    }
    else
    {
        int next = (int)player.tag + 1;
        
        if (next >= 4)
            next = 0;
        
        bool bFound = false;
        for (int i = next; i < 4; i++)
        {
            if (movies[i] != nil) {
                next = i;
                bFound = true;
                break;
            }
        }
        if (!bFound) {
            for (int i = 0; i < next; i++)
            {
                if (movies[i] != nil) {
                    next = i;
                    bFound = true;
                    break;
                }
            }
        }
        
        if( next < 4 )
        {
            FSNewPlayer * player = self.multiMoviePreview[ next ];
            [ player play ];
        }
    }
}

#pragma mark ECP Logo Methods

- (void)setECPLogoHidden:(BOOL)state
{
    if (ecpLogoUse && ![self videoMode])
        logoOverImageView.hidden = state;
    else
        logoOverImageView.hidden = YES;
}

- (void)showUseLogoSwitcherWithState
{
    BOOL hide = ecpSaved ? NO : YES;
    useLogoSwitcher.hidden = hide;
    uselLogoLabel.hidden   = hide;
    [useLogoSwitcher setOn:ecpLogoUse];
}

- (IBAction)changeLogoMode:(UISwitch *)sender
{
    if (sender.on)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ecpLogoUse"];
        
        if (!compareMode && currentMedia != -1 )
        {
            logoOverImageView.hidden = NO;
        }
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ecpLogoUse"];
        logoOverImageView.hidden = YES;
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)addECPLogo
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *picker =[[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        _popOverLibrary = [[UIPopoverController alloc] initWithContentViewController:picker];
        _popOverLibrary.delegate = self;
        [_popOverLibrary presentPopoverFromRect:CGRectMake(0.0, 0.0, 345.0, 1050.0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage : (UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    BOOL theNewIPad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && [[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [UIScreen mainScreen].scale > 1;
    float ecpSize;
    
    if (theNewIPad) {
        ecpSize = 250.0;
    } else {
        ecpSize = 100.0;
    }
    
    CGSize newLogoSize = CGSizeMake(ecpSize, (image.size.height*ecpSize)/image.size.width);
    
    UIGraphicsBeginImageContext(newLogoSize);
    [image drawInRect:CGRectMake(0, 0, newLogoSize.width, newLogoSize.height)];
    UIImage *logoInSize = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImage *newImage = logoInSize;
    
    // Write logo to Documents directory
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:@"ecp_logo.png"];
    
    NSError *error = nil;
    
    if (ecpSaved) {
        if (![fileManager removeItemAtPath:filePath error:&error]) {
            NSLog(@"Delete failed:%@", error);
        } else {
            NSLog(@"Image removed: %@", filePath);
        }
    }
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    
    dispatch_async(queue, ^{
        NSData *pngData = UIImagePNGRepresentation(newImage);
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [pngData writeToFile:filePath atomically:YES];
        });
    });
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:YES forKey:@"ecpLogo"];
    [userDefaults setBool:YES forKey:@"ecpLogoUse"];
    [userDefaults setValue:filePath forKey:@"ecpLogoPath"];
    [userDefaults synchronize];
    
    logoOverImageView.image = newImage;
    sharingPopover.noLogoLabel.hidden = YES;
    sharingPopover.ecpLogoView.image = newImage;
    
    [self showUseLogoSwitcherWithState];
    
    [_popOverLibrary dismissPopoverAnimated:YES];
    
    if (!compareMode && !camera.videoMode && currentMedia != -1)
    {
        logoOverImageView.hidden = NO;
    }
}

- (void)removeECPLogo
{
    NSError *error = nil;
    
    if(![[NSFileManager defaultManager] removeItemAtPath:ecpPath error:&error]) {
        NSLog(@"Delete failed:%@", error);
    } else {
        NSLog(@"Image removed: %@", ecpPath);
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"ecpPath"];
    [userDefaults setBool:NO forKey:@"ecpLogo"];
    [userDefaults synchronize];
    
    [logoOverImageView setImage:nil];
    sharingPopover.noLogoLabel.hidden = NO;
    [sharingPopover.ecpLogoView setImage:[UIImage imageNamed:@"no-logo.png"]];
    
    [self showUseLogoSwitcherWithState];
}

#pragma mark - Back View For Popovers

- (void)placeBackgoundView
{
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024.f, 1024.f)];
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0;
    [self.view addSubview:backgroundView];
    
    [UIView animateWithDuration:0.4f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        backgroundView.alpha = 0.4f;
    } completion:nil];
}

- (void)removeBackgoundView
{
    [UIView animateWithDuration:0.4f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        backgroundView.alpha = 0;
    } completion:^(BOOL finished){
        [backgroundView removeFromSuperview];
        backgroundView = nil;
    }];
}

#pragma mark - Share Popover

- (void)sharePressed
{
    [self startRequestTimer];
    
    if (!sharePopover)
    {
        [self placeBackgoundView];
        
        sharePopover = [FSShareChooseViewController new];
        
        sharePopover.activityController = self;
        sharePopover.delegate = self;
        
        [self.view addSubview:sharePopover.view];
        
        [sharePopover.view slideViewWithAnimationToPoint:CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0)];
    }
}

- (void)dismissSharePreview
{
    [self removeBackgoundView];
    
    [sharePopover.view hideViewWithAnimation];

    sharePopover = nil;
}

- (void)sharePressed:(NSArray *)selected
{
    [self dismissSharePreview];
    
    __weak __typeof(self)weakSelf = self;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Share", @"")
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *email = [UIAlertAction actionWithTitle:NSLocalizedString(@"Email", @"")
                                                    style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action) {
                                                      
                                                      if (selected.count)
                                                          [weakSelf shareSelected:selected];
                                                      else
                                                          [weakSelf performSelector:@selector(shareNoSelected) withObject:nil afterDelay:0.2f];
                                                  }];
    
    UIAlertAction *Ecolumn = [UIAlertAction actionWithTitle:NSLocalizedString(@"E-column", @"")
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        
                                                        if (selected.count)
                                                            [weakSelf shareFTPSelected:selected];
                                                        else
                                                            [weakSelf performSelector:@selector(shareNoSelected) withObject:nil afterDelay:0.2f];
                                                    }];
    
    UIAlertAction *socialShare = [UIAlertAction actionWithTitle:NSLocalizedString(@"Social share", @"")
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *action) {
                                                            
                                                            if (selected.count)
                                                                [weakSelf shareAmazonSelected:selected];
                                                            else
                                                                [weakSelf performSelector:@selector(shareNoSelected) withObject:nil afterDelay:0.2f];
                                                        }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"")
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    
    [alert addAction:email];
    [alert addAction:Ecolumn];
    if (![self videoMode])
        [alert addAction:socialShare];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)shareNoSelected
{
    [alertHelper showAlert:FSAlertTypeNoContentForSharing];
}

#pragma mark - Social Share QR Popover

- (void)showQrPopover
{
    if (!qrPopover)
    {
        [self placeBackgoundView];
        
        qrPopover = [FSShareQrViewController new];
        qrPopover.activityController = self;
        qrPopover.delegate = self;
        [self.view addSubview:qrPopover.view];
        
        [qrPopover.view slideViewWithAnimationToPoint:CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0)];
    }
}

- (void)dismissQrSharePreview
{
    [self removeBackgoundView];
    
    [qrPopover.view hideViewWithAnimation];
    
    urlsToQR = nil;
    qrPopover = nil;
}

#pragma mark - Social Share Upload Popover

- (void)showUploadPopover
{
    if (!uploadPopover)
    {
        [self placeBackgoundView];
        
        uploadPopover = [FSShareProcessController new];
        
        uploadPopover.delegate = self;
        
        [self.view addSubview:uploadPopover.view];
        
        [uploadPopover.view slideViewWithAnimationToPoint:CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0)];
    }
}

- (void)dismissUploadPopover
{
    if (uploadPopover)
    {
        [amazonShare cancel];
        amazonShare = nil;
        
        [self removeBackgoundView];
        
        [uploadPopover.view hideViewWithAnimation];
        uploadPopover = nil;
    }
}

#pragma mark - Send Broadcast for Column

- (NSString *)getWiFiBroadcastAddress
{
    // Get the WiFi Broadcast Address
    NSString *String = [SSNetworkInfo WiFiBroadcastAddress];
    // Validate it
    if (String == nil || String.length <= 0) {
        // Error, no value returned
        return nil;
    }
    // Successful
    return String;
}

- (void)startRequestTimer
{
    if (!udpRequestsTimer)
    {
        udpRequestsTimer = [NSTimer scheduledTimerWithTimeInterval:kUDPUpdatePeriodInSeconds
                                                            target:self
                                                          selector:@selector(sendRequest)
                                                          userInfo:nil
                                                           repeats:YES];
    }
}

- (void)stopRequestTimer
{
    [udpRequestsTimer invalidate];
    if (udpSocket != nil) {
        [udpSocket setDelegate:nil];
    }
    udpSocket = nil;
}

- (void)sendRequest
{
    udpSocket = [[AsyncUdpSocket alloc] initWithDelegate:self];
    [udpSocket setDelegate:self];
    
    NSError *error = nil;
    
    if (![udpSocket bindToPort:0 error:&error])
    {
        NSLog(@"Error binding: %@", error);
        return;
    }
    
    if (![udpSocket enableBroadcast:YES error:&error])
    {
        NSLog(@"Error binding: %@", error);
        return;
    }
    
    [udpSocket receiveWithTimeout:-1 tag:0];
    
    NSString *host = [self getWiFiBroadcastAddress];
    
    if (!host) {
        host = @"255.255.255.255";
    }
    
    int port = 22222;
    
    NSString *msg = @"<message><key>ftp</key></message>";
    
    if ([msg length] == 0)
    {
        NSLog(@"Message required");
        
        return;
    }
    
    NSData *data = [msg dataUsingEncoding:NSUTF8StringEncoding];
    
    [udpSocket sendData:data toHost:host port:port withTimeout:-1 tag:1];
}

- (BOOL)onUdpSocket:(AsyncUdpSocket *)sock
     didReceiveData:(NSData *)data
            withTag:(long)tag
           fromHost:(NSString *)host
               port:(UInt16)port
{
    NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    if (msg)
    {
        m_ftpStr = msg;
        
        [self formFtpSetupWithUrl:(NSString *)host];
    }
    
    [udpSocket receiveWithTimeout:-1 tag:0];
    
    return YES;
}

#pragma mark - Setup FTP Info

- (void)formFtpSetupWithUrl:(NSString *)url
{
    NSString *path = [m_ftpStr stringBetweenString:@"<path>" andString:@"</path>"];
    
    m_ftpSword = [m_ftpStr stringBetweenString:@"<user>" andString:@"</user>"];
    m_ftpPassw = [m_ftpStr stringBetweenString:@"<password>" andString:@"</password>"];
    m_ftpAddrr = [url stringByAppendingString:path];
}

- (void)clearFTPInfo
{
    m_ftpStr = nil;
    m_ftpAddrr = nil;
    m_ftpSword = nil;
    m_ftpPassw = nil;
}

#pragma mark - Send to Column

- (void)shareFTPSelected:(NSArray *)selected
{    
    selectedMedia = selected;
    
    for (NSNumber *number in selectedMedia)
    {
        NSData *attachmentData;
        
        NSString *fileName = ![self videoMode] ? [NSString stringWithFormat:@"FrameSelectionPhoto%d.jpg", [number intValue]] : [NSString stringWithFormat:@"FrameSelectionVideo%d.mp4", [number intValue]];
        
        if( [self videoMode] )
            attachmentData = [ NSData dataWithContentsOfURL : [ self getMovieURL : [number intValue] ] ];
        else
        {
            if (ecpLogoUse)
            {
                UIImage *image = [ self getPhoto : [number intValue] ];
                
                UIImage *logoInSize = [UIImage imageWithContentsOfFile:ecpPath];
                
                logoInSize = [UIImage imageWithCGImage:logoInSize.CGImage scale:logoInSize.scale orientation:UIImageOrientationUp];
                
                UIGraphicsBeginImageContext(image.size);
                
                [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
                
                [logoInSize drawInRect:CGRectMake(image.size.width - logoInSize.size.width - logoInSize.size.width/10.0, image.size.height - image.size.height + logoInSize.size.height/10.0, logoInSize.size.width, logoInSize.size.height)];
                
                UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
                
                UIGraphicsEndImageContext();
                
                image = result;
                
                attachmentData = UIImageJPEGRepresentation( image, 0.8f );
            }
            else
            {
                attachmentData = UIImageJPEGRepresentation( [ self getPhoto : [number intValue] ], 0.8f );
            }
        }

        NSString *filePath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        
        [attachmentData writeToFile:[filePath stringByAppendingPathComponent:fileName] atomically:YES];
        
        [NETWORK_MANAGER addToFTPQeue:[filePath stringByAppendingPathComponent:fileName]];
    }
    
    if ([self checkStringIsNotNil:m_ftpAddrr]
        && [self checkStringIsNotNil:m_ftpSword]
        && [self checkStringIsNotNil:m_ftpPassw])
    {
        [NETWORK_MANAGER startFTPSendWithCompletion:^(BOOL success){
            [self showFTPAlertWithMessage:NSLocalizedString(@"FTP send success!", nil)];
        }];
    }
    else
    {
        [self clearFTPInfo];
        [self showFTPAlertWithMessage:NSLocalizedString(@"FTP send error!", nil)];
    }
}

- (void)showFTPAlertWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)shareAmazonSelected:(NSArray *)selected
{
    if (![NETWORK_MANAGER networkAvaliable])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No Internet connection", @"")
                                                        message:NSLocalizedString(@"Internet is down. Turn ON internet on the device and try again", @"")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                              otherButtonTitles:nil];
        [alert show];
    }

    [self showUploadPopover];
    
    amazonShare = [SMAmazonShareHelper new];
    urlsToQR = [NSMutableArray new];
    
    NSMutableArray *imagesArray = [NSMutableArray new];
    
    for (NSNumber *number in selected)
        [imagesArray addObject:[self getPhoto:[number intValue]]];
    
    [amazonShare uploadImages:imagesArray withCompletion:^(BOOL status, NSArray *response) {
        
        if (status)
        {
            for (SMShareImage *imgInfo in response)
            {
                if (imgInfo.success)
                    [urlsToQR addObject:[imgInfo.presignedURL absoluteString]];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self dismissUploadPopover];
                
                [self showQrPopover];
                
            });
        }
        else
        {
            [self dismissUploadPopover];
            NSLog(@"Response %@", response);
        }
    }];
}

- (void)shareSelected:(NSArray *)selected
{
    selectedMedia = selected;
    
    MFMailComposeViewController *picker = [MFMailComposeViewController new];
    
    picker.mailComposeDelegate = self;
    
    [picker setMessageBody:[FSNewViewController fetchLocalizationValueForKey:@"MAIL_BODY"] isHTML:NO];
    [picker setSubject:[FSNewViewController fetchLocalizationValueForKey:@"MAIL_TITLE"]];
    
    NSString *mimeType = ![self videoMode] ? @"image/jpeg" : @"video/mp4";
    
    for (NSNumber *number in selectedMedia)
    {
        NSData *attachmentData;
        
        NSString *fileName = ![self videoMode] ? [NSString stringWithFormat:@"FrameSelectionPhoto%d.jpg", [number intValue]] : [NSString stringWithFormat:@"FrameSelectionVideo%d.mp4", [number intValue]];
        
        if( [self videoMode] )
            attachmentData = [ NSData dataWithContentsOfURL : [ self getMovieURL : [number intValue] ] ];
        else
        {
            if (ecpLogoUse)
            {
                UIImage *image = [ self getPhoto : [number intValue] ];
                
                UIImage *logoInSize = [UIImage imageWithContentsOfFile:ecpPath];
                
                logoInSize = [UIImage imageWithCGImage:logoInSize.CGImage scale:logoInSize.scale orientation:UIImageOrientationUp];
                
                UIGraphicsBeginImageContext(image.size);
                
                [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
                
                [logoInSize drawInRect:CGRectMake(image.size.width - logoInSize.size.width - logoInSize.size.width/10.0, image.size.height - image.size.height + logoInSize.size.height/10.0, logoInSize.size.width, logoInSize.size.height)];
                
                UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
                
                UIGraphicsEndImageContext();
                
                image = result;
                
                attachmentData = UIImageJPEGRepresentation( image, 0.8f );
            }
            else
            {
                attachmentData = UIImageJPEGRepresentation( [ self getPhoto : [number intValue] ], 0.8f );
            }
        }
        
        [picker addAttachmentData:attachmentData mimeType:mimeType fileName:fileName];
    }
    
    picker.navigationBar.barStyle = UIBarStyleDefault;
    
    @try {
        [self presentViewController:picker animated:YES completion:nil];
    }
    @catch (NSException *exception) {
        //
    }
    @finally {
        //
    }
}

#pragma mark Mail Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent: {
            [alertHelper showAlert:FSAlertTypeMediaSuccesfullySent];
            break;
        }
        case MFMailComposeResultFailed:
            break;
            
        default:
        {
            [alertHelper showAlert:FSAlertTypeSendEmailFailed];
        }
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -

- (void)loadPlistInDocuments
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *errorFileManager = nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentsPlistPath = [documentsDirectory stringByAppendingPathComponent:@"CustomStrings.plist"];
    
    BOOL success = [fileManager fileExistsAtPath:documentsPlistPath];
    
    if (!success)
    {
        NSString *resourcePlistPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"CustomStrings.plist"];
        [fileManager removeItemAtPath:documentsPlistPath error:&errorFileManager];
        success = [fileManager copyItemAtPath:resourcePlistPath toPath:documentsPlistPath error:&errorFileManager];
        
        if (!success) {
            NSLog(@"Failed to copy plist file with message '%@'.", [errorFileManager localizedDescription]);
        }
    }
}

+ (void)changeLocalizationValue:(NSString *)value forKey:(NSString *)key
{
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"CustomStrings.plist"];
    
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization
                                          propertyListFromData:plistXML
                                          mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                          format:&format
                                          errorDescription:&errorDesc];
    if (!temp) {
        NSLog(@"Error reading plist: %@, format: %lu", errorDesc, (unsigned long)format);
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile: plistPath];
    NSString *enLang = @"";
    NSString *frLang = @"";
    
    if ([dict objectForKey:key] != nil)
    {
        NSArray *splitArray = [[dict objectForKey:key] componentsSeparatedByString:@"|"];
        enLang = [splitArray objectAtIndex:0];
		frLang = [splitArray objectAtIndex:1];
    }
    
    NSString *currentLocaleIdentifier = [[NSLocale currentLocale] localeIdentifier];
    
    if (![currentLocaleIdentifier isEqualToString:@"fr_FR"])
        enLang = value;
    else
        frLang = value;
    
    [dict setValue:[NSString stringWithFormat:@"%@|%@", enLang, frLang] forKey:key];
    
    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:dict
                                                                   format:NSPropertyListXMLFormat_v1_0
                                                         errorDescription:nil];
    if(plistData)
        [plistData writeToFile:plistPath atomically:YES];
}

+ (NSString *)fetchLocalizationValueForKey:(NSString *)key
{
    NSLog(@"fetchLocalizationValueForKey");
    
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSString *plistPath;
    
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    plistPath = [rootPath stringByAppendingPathComponent:@"CustomStrings.plist"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        plistPath = [[NSBundle mainBundle] pathForResource:@"CustomStrings"
                                                    ofType:@"plist"];
    }
    
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization
                                          propertyListFromData:plistXML
                                          mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                          format:&format
                                          errorDescription:&errorDesc];
    if (!temp) {
        NSLog(@"Error reading plist: %@, format: %lu", errorDesc, (unsigned long)format);
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile: plistPath];
    NSString *currentLocaleIdentifier = [[NSLocale currentLocale] localeIdentifier];
    
    if ([dict objectForKey:key] != nil) {
        NSArray *splitArray = [[dict objectForKey:key] componentsSeparatedByString:@"|"];
        NSString *enLang = [splitArray objectAtIndex:0];
		NSString *frLang = [splitArray objectAtIndex:1];
        
        if (![currentLocaleIdentifier isEqualToString:@"fr_FR"] && ![enLang isEqualToString:@""]) {
            return NSLocalizedString(enLang, nil);
        } else if (![frLang isEqualToString:@""]) {
            return NSLocalizedString(frLang, nil);
        }
    }
    
    return NSLocalizedString(key, nil);
}

#pragma mark Show & Hide Keyboard

- (void)animateChangeFrame:(UIView *)view withOffset:(CGFloat)offset
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.4f];
    view.transform = CGAffineTransformMakeTranslation(0, -offset);
    [UIView commitAnimations];
}

- (void)keyboardWasShown:(NSNotification *)aNotification
{
    CGFloat offset = 0;
    
    if ([sharingPopover.descriptionTextView isFirstResponder] || [sharingPopover.titleBodyTextField isFirstResponder])
    {
        offset = 180.f;
    }
    
    [self animateChangeFrame:sharingPopover.view withOffset:offset];
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    [self animateChangeFrame:sharingPopover.view withOffset:0];
}

- (void)viewDidDisappear:(BOOL)animated
{
    alertHelper.ownerController = nil;
    alertHelper.alertView = nil;
    alertHelper = nil;
    
    sharingPopover = nil;
    sharePopover = nil;
    qrPopover = nil;
    uploadPopover = nil;
    _popOverLibrary = nil;
    
    [self stopRequestTimer];
    
    // remove all "FSNewPlayer Objects"
    for (UIView *view in contentScrollView.subviews)
    {
        if ([view isKindOfClass:[FSNewPlayer class]])
        {
            [((FSNewPlayer *)view) stop];
            [view removeFromSuperview];
        }
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

#pragma mark - Pen Tool

- (void)penToolPressed:(id)sender
{
    penToolButton.userInteractionEnabled = NO;
    
    if (penActivated)
    {
        [self closeColors:nil];
    }
    else
    {
        [penToolButton setImage:[UIImage imageNamed:@"PenToolBtnActive"] forState:UIControlStateNormal];
        
        [self hideColorButtons:NO];
        
        [self slideViews:colorButtons
               xPosition:-65.f
               yPosition:65.f
                    step:-38.f];
        
        penActivated = YES;
        [penView setPenActivated:penActivated];
        [penView setPenColor:currentColor];
    }
    
    [self performSelector:@selector(unlockPenToolButton)
               withObject:nil afterDelay:0.3f];
}

- (void)hideColorButtons:(BOOL)hide
{
    for (UIButton *button in colorButtons)
        [button fadeView:hide];
}

- (void)closeColors:(id)sender
{
    [self hideColorButtons:YES];
    
    [penToolButton setImage:[UIImage imageNamed:@"PenToolBtn"] forState:UIControlStateNormal];
    
    [self slideViews:colorButtons
           xPosition:65.f
           yPosition:-65.f
                step:38.f];
    
    penActivated = NO;
    [penView setPenActivated:penActivated];
}

- (void)selectPenColor:(UIButton *)sender
{
    for (UIButton *button in colorButtons)
        button.selected = NO;
    
    currentColor = (int)sender.tag;
    
    [penView setPenColor:currentColor];
    
    sender.selected = YES;
}

- (void)unlockPenToolButton
{
    penToolButton.userInteractionEnabled = YES;
}

#pragma mark -

- (void)startAnimationIndicator
{
    NSArray *myImages = [NSArray arrayWithObjects:[UIImage imageNamed:@"videoRecord1"], [UIImage imageNamed:@"videoRecord2"], nil];
    
    videoIndicatorImageView.animationImages = myImages;
    videoIndicatorImageView.animationDuration = 1;
    videoIndicatorImageView.animationRepeatCount = 0;
    [videoIndicatorImageView startAnimating];
}

- (void)stopAnimationIndicator
{
    [videoIndicatorImageView stopAnimating];
}

#pragma mark Autorotate Methods

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [camera.videoPreviewLayer.connection setVideoOrientation:(AVCaptureVideoOrientation)[UIDevice currentDevice].orientation];
}

#pragma mark Google Analytics

- (void)moduleEntered
{
    [APP_MANAGER sendAnalyticsEnteredModuleWithName:@"SMFrameSelection"];
}

@end
