#import "FSNewCamera.h"

#import "UIImage+Extra.h"

@interface FSNewCamera()
{
    AVCaptureSession * session;
    AVCaptureVideoPreviewLayer * videoPreviewLayer;
    AVCaptureStillImageOutput * stillImageOutput;
    AVCaptureMovieFileOutput * movieFileOutput;
    
    AVCaptureConnection * photoConnection;
    AVCaptureConnection * videoConnection;
    
    UIView * previewView;
    
    BOOL front;
    BOOL videoMode;
    
    float scale;
    
    void( ^takeVideoHandler )( NSURL * url );
}

@end

@implementation FSNewCamera

@synthesize device;

-( id )init
{
    self = [ super init ];
    if( self )
    {
        session = [ [ AVCaptureSession alloc ] init ];
        
        NSArray * devices = [ AVCaptureDevice devicesWithMediaType : AVMediaTypeVideo ];
        
        for( AVCaptureDevice * d in devices )
            if( d.position == AVCaptureDevicePositionBack )
                device = d;
        
        NSError * error = nil;
        AVCaptureDeviceInput * videoInput = [ [ AVCaptureDeviceInput alloc ] initWithDevice : device error : &error ];
        [ session addInput : videoInput ];
        
        videoPreviewLayer = [ [ AVCaptureVideoPreviewLayer alloc ] initWithSession : session ];
        
        [[videoPreviewLayer connection] setVideoOrientation:(AVCaptureVideoOrientation)[ UIApplication sharedApplication ].statusBarOrientation];
        
        videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        
        previewView = [ [ UIView alloc ] init ];
        [ previewView.layer addSublayer : videoPreviewLayer ];
        
        stillImageOutput = [ [ AVCaptureStillImageOutput alloc ] init ];
        stillImageOutput.outputSettings = @{ AVVideoCodecKey : AVVideoCodecJPEG };
        [ session addOutput : stillImageOutput ];
        photoConnection = [ self getVideoConnection : stillImageOutput ];
        
        movieFileOutput = [ [ AVCaptureMovieFileOutput alloc ] init ];
        movieFileOutput.maxRecordedDuration = CMTimeMake( 3, 1 );
        movieFileOutput.minFreeDiskSpaceLimit = 0;
        [ session addOutput : movieFileOutput ];
        videoConnection = [ self getVideoConnection : movieFileOutput ];
        
        session.sessionPreset = AVCaptureSessionPresetPhoto;//AVCaptureSessionPresetHigh;//AVCaptureSessionPreset640x480;
        
        [ session startRunning ];
        
        scale = 1.f;
    }
    return self;
}

-( void )dealloc
{
    [ session stopRunning ];
    session = nil;
}

-( AVCaptureVideoPreviewLayer * )videoPreviewLayer
{
    return videoPreviewLayer;
}

-( void )setPreviewContainer : ( UIView * )container
{
    [ previewView removeFromSuperview ];
    
    CGRect bounds = container.layer.bounds;
    
    videoPreviewLayer.bounds = bounds;
    videoPreviewLayer.position = CGPointMake( CGRectGetMidX( bounds ), CGRectGetMidY( bounds ) );
    
    previewView.frame = CGRectMake( 0, 0, container.frame.size.width, container.frame.size.height );
    [ container addSubview : previewView ];
}

-( void )setFrontCamera : ( BOOL )aFront
{
    if( self->front == aFront )
        return;
    
    self->front = aFront;
    
    [ session beginConfiguration ];
    
    AVCaptureInput * currentInput = [ session.inputs objectAtIndex : 0 ];
    [ session removeInput : currentInput ];
    
    NSArray * devices = [ AVCaptureDevice devicesWithMediaType : AVMediaTypeVideo ];
    
	for( AVCaptureDevice * d in devices )
		if( d.position == ( aFront ? AVCaptureDevicePositionFront : AVCaptureDevicePositionBack ) )
			device = d;
    
    AVCaptureDeviceInput * newInput = [ [ AVCaptureDeviceInput alloc ] initWithDevice : device error : nil ];
    [ session addInput : newInput ];
    
    [ session commitConfiguration ];
}

-( BOOL )frontCamera
{
    return front;
}

-( void )setVideoMode : ( BOOL )aVideoMode
{
    self->videoMode = aVideoMode;
    [ self setScale : self->scale ]; // clamp scale
    
    [ session beginConfiguration ];
    session.sessionPreset = aVideoMode ? AVCaptureSessionPresetHigh : AVCaptureSessionPresetPhoto;
    [ session commitConfiguration ];
}

-( BOOL )videoMode
{
    return videoMode;
}

-( AVCaptureConnection * )getVideoConnection : ( AVCaptureOutput * )output
{
    for( AVCaptureConnection * connection in output.connections )
        for( AVCaptureInputPort * port in connection.inputPorts )
            if( [ port.mediaType isEqual : AVMediaTypeVideo ] )
                return connection;
    
    return nil;
}

-( void )refreshVideoConnection : ( AVCaptureConnection * )aVideoConnection
{
    aVideoConnection.videoOrientation = (AVCaptureVideoOrientation)[ UIApplication sharedApplication ].statusBarOrientation;
    //videoConnection.videoMirrored = front;
}

/*-( AVCaptureConnection * )refreshOrientation : ( AVCaptureOutput * )output
{
    NSLog( @"%@", output.connections );
    
    AVCaptureConnection * videoConnection = nil;
    for( AVCaptureConnection * connection in output.connections )
    {
        for( AVCaptureInputPort * port in connection.inputPorts )
        {
            if( [ port.mediaType isEqual : AVMediaTypeVideo ] )
            {
                videoConnection = connection;
                break;
            }
        }
        if( videoConnection )
            break;
    }
    
    videoConnection.videoOrientation = [ UIApplication sharedApplication ].statusBarOrientation;
    videoConnection.videoMirrored = front;
    
    return videoConnection;
}*/

-( void )takePhoto : ( void( ^ )( UIImage * image ) )handler
{
    //AVCaptureConnection * videoConnection = [ self getVideoConnection : stillImageOutput ];
    photoConnection = [ self getVideoConnection : stillImageOutput ];
    [ self refreshVideoConnection : photoConnection ];
    photoConnection.videoScaleAndCropFactor = scale;
    
    [ stillImageOutput captureStillImageAsynchronouslyFromConnection : photoConnection completionHandler :  ^( CMSampleBufferRef imageSampleBuffer, NSError * error )
    {
        NSData * imageData = [ AVCaptureStillImageOutput jpegStillImageNSDataRepresentation : imageSampleBuffer ];
        UIImage * image = [ [ UIImage alloc ] initWithData : imageData ];
        
        UIImageOrientation orientation;
        
        if (photoConnection.videoOrientation == AVCaptureVideoOrientationLandscapeLeft)
            orientation = UIImageOrientationUpMirrored;
        else
            orientation = UIImageOrientationDownMirrored;
        
        if (front) {
            UIImage *flippedImage = [UIImage imageWithCGImage:image.CGImage
                                                        scale:image.scale
                                                  orientation:orientation];
            handler( flippedImage );
        } else {
            handler( image );
        }
        
    } ];
}

- (BOOL)takeVideo:(NSURL *)url :(void(^)(NSURL *url))handler
{
    if (movieFileOutput.isRecording) {
        return NO;
    }
    
    takeVideoHandler = handler;
    
    //AVCaptureConnection * videoConnection = [ self getVideoConnection : stillImageOutput ];
    videoConnection = [self getVideoConnection : movieFileOutput ];
    [ self refreshVideoConnection : videoConnection ];
    
    if( scale > videoConnection.videoMaxScaleAndCropFactor )
        videoConnection.videoScaleAndCropFactor = videoConnection.videoMaxScaleAndCropFactor;
    else
        videoConnection.videoScaleAndCropFactor = scale;
    
    //NSURL * url = [ NSURL fileURLWithPath : [ NSTemporaryDirectory() stringByAppendingPathComponent : @"fsvideo0.mov" ] ];
    
    if (front)
    videoConnection.videoMirrored = YES;
    
    [ [ NSFileManager defaultManager ] removeItemAtURL : url error : NULL ];
    [ movieFileOutput startRecordingToOutputFileURL : url recordingDelegate : self ];
    
    return YES;
}

-( void )setScale : ( float )aScale
{
    if( aScale < 1.f )
        aScale = 1.f;
    
    if( aScale > 10.f)
        aScale = 10.f;
    
    if( videoMode )
    {
        if ([self deviceScaleSupport] && !front)
        {
            if( aScale > device.activeFormat.videoMaxZoomFactor)
                aScale = device.activeFormat.videoMaxZoomFactor;
            
            NSError *error = nil;
            
            if ([device lockForConfiguration:&error])
            {
                [device setVideoZoomFactor:aScale];
                [device unlockForConfiguration];
            }
        }
        else
        {
            aScale = 1.f;
        }
    }
    
    self->scale = aScale;
    
    float realScale = videoMode ? aScale - ((aScale - 1.0) / 1.02f) : aScale;
    
    previewView.transform = CGAffineTransformMakeScale( realScale, realScale );
}

- (BOOL)deviceScaleSupport
{
    return device.activeFormat.videoMaxZoomFactor > 1.f;
}

-( float )scale
{
    return scale;
}

-( BOOL )isRecording
{
    return movieFileOutput.isRecording;
}

-( void )captureOutput : ( AVCaptureFileOutput * )captureOutput didFinishRecordingToOutputFileAtURL : ( NSURL * )outputFileURL fromConnections : ( NSArray * )connections error : ( NSError * )error
{
    /*NSLog( @"error: %@", error );
    UISaveVideoAtPathToSavedPhotosAlbum([outputFileURL path],nil,nil,nil);*/
    takeVideoHandler( outputFileURL );
}

- (void)stopCamera
{
    [ session stopRunning ];
    session = nil;
}

@end
