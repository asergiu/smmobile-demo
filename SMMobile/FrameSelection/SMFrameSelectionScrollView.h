//
//  SMFrameSelectionScrollView.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 3/10/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMFrameSelectionScrollView : UIScrollView

- (void)setBlindZoneWithRect:(CGRect)rect;

@end
