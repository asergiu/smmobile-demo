#import <Foundation/Foundation.h>
#import "FSNewViewController.h"

typedef enum _FSAlertType {
	FSAlertTypeNoContentForSharing,
    FSAlertTypeDeleteContent,
    FSAlertTypeNewSessionCantStart,
    FSAlertTypeCameraDisabled,
    FSAlertTypeSendEmailFailed,
    FSAlertTypeNoCamera,
    FSAlertTypeCameraNotWorking,
    FSAlertTypeNoContentForShowing,
    FSAlertTypeMediaSuccesfullySent,
    FSAlertTypeNoContentForMode,
    FSAlertTypeNoContentSelected
} FSAlertType;

@interface FSAlertsHelper : NSObject <UIAlertViewDelegate>

@property (strong, nonatomic) UIViewController *ownerController;
@property (strong, nonatomic) UIAlertView *alertView;

- (id)initWithOwner:(UIViewController *)owner;
- (void)showAlert:(FSAlertType)type;
- (void)showAlert:(FSAlertType)type withValue:(NSInteger)value;

@end
