#define _use_iPad

#import "FSPopoverControllerTable.h"
#import "FSNewViewController.h"
#import "FSAlertsHelper.h"
#ifdef _use_iPad
#endif

#define ecpLogoUse [[NSUserDefaults standardUserDefaults] boolForKey:@"ecpLogoUse"]
#define ecpPath    [[NSUserDefaults standardUserDefaults] stringForKey:@"ecpLogoPath"]

@interface FSPopoverControllerTable ()

- (void) shareFBVideo;

@end

@implementation FSPopoverControllerTable

@synthesize popoverValues;
@synthesize activityController;

#define degreesToRadians(x) (M_PI * x / 180.0)
@synthesize youtubeLink;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.popoverValues = [NSArray arrayWithObjects:@"E-mail", /*@"Facebook",*/ nil];
    self.tableView.scrollEnabled = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.popoverValues.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        if (indexPath.row == 1) {
            cell.imageView.image = [UIImage imageNamed:@"facebook_logo.png"];
        } else if (indexPath.row == 0) {
            cell.imageView.image = [UIImage imageNamed:@"mail.png"];
        }
    }
    cell.textLabel.text = [self.popoverValues objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)showAlertWith:(FSAlertType)alertType
{
    FSAlertsHelper *alertHelper = [[FSAlertsHelper alloc] initWithOwner:NULL];
    [alertHelper showAlert:alertType];
    
    [self.activityController.popoverController dismissPopoverAnimated:YES];
}

- (UIImage *)image: (UIImage *)image rotatedByDegrees:(CGFloat)degrees {
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,image.size.width, image.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(degreesToRadians(degrees));
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    CGFloat rotateAngel = (CGFloat)degrees + 90.0f;
    CGContextRotateCTM(bitmap, degreesToRadians(rotateAngel));
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    
    if (degrees == 90 || degrees == 270) {
        CGContextDrawImage(bitmap, CGRectMake(-rotatedSize.width / 2, -rotatedSize.height / 2, rotatedSize.width, rotatedSize.height), [image CGImage]);
    } else {
        CGContextDrawImage(bitmap, CGRectMake(-image.size.height / 2, -image.size.width / 2, image.size.height, image.size.width), [image CGImage]);
    }
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)showEmailModalView
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    [picker setMessageBody:[FSNewViewController fetchLocalizationValueForKey:@"MAIL_BODY"] isHTML:NO];
    [picker setSubject:[FSNewViewController fetchLocalizationValueForKey:@"MAIL_TITLE"]];
    
    FSNewViewController * newvc = ( FSNewViewController * )activityController;
    
    NSString *mimeType = !newvc.videoMode ? @"image/jpeg" : @"video/mp4";
    
    for (NSNumber *number in activityController.selectedMedia)
    {
        NSData *attachmentData;
        
        NSString *fileName = !newvc.videoMode ? [NSString stringWithFormat:@"FrameSelectionPhoto%d.jpg", [number intValue]] : [NSString stringWithFormat:@"FrameSelectionVideo%d.mp4", [number intValue]];
        
        if( newvc.videoMode )
        attachmentData = [ NSData dataWithContentsOfURL : [ newvc getMovieURL : [number intValue] ] ];
        else
        {
            if (ecpLogoUse)
            {
                UIImage *image = [ newvc getPhoto : [number intValue] ];
                
                UIImage *logoInSize = [UIImage imageWithContentsOfFile:ecpPath];
                
                logoInSize = [UIImage imageWithCGImage:logoInSize.CGImage scale:logoInSize.scale orientation:UIImageOrientationUp];
                
                UIGraphicsBeginImageContext(image.size);
                
                [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
                
                [logoInSize drawInRect:CGRectMake(image.size.width - logoInSize.size.width - logoInSize.size.width/10.0, image.size.height - image.size.height + logoInSize.size.height/10.0, logoInSize.size.width, logoInSize.size.height)];
                
                UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
                
                UIGraphicsEndImageContext();
                
                image = result;
                
                attachmentData = UIImageJPEGRepresentation( image, 1.0 );
            }
            else
            {
                attachmentData = UIImageJPEGRepresentation( [ newvc getPhoto : [number intValue] ], 1.0 );
            }
        }
        
        [picker addAttachmentData:attachmentData mimeType:mimeType fileName:fileName];
    }
    
    picker.navigationBar.barStyle = UIBarStyleBlack;
    
    @try {
        [self presentViewController:picker animated:YES completion:nil];
    }
    @catch (NSException *exception) {
        //
    }
    @finally {
        //
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
        break;
        case MFMailComposeResultSaved:
        break;
        case MFMailComposeResultSent:
        {
            FSAlertsHelper *alertHelper = [[FSAlertsHelper alloc] initWithOwner:NULL];
            [alertHelper showAlert:FSAlertTypeMediaSuccesfullySent];
            break;
        }
        case MFMailComposeResultFailed:
        break;
        
        default:
        {
            [self showAlertWith:FSAlertTypeSendEmailFailed];
        }
        break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self hidePopover];
}

- (UIImage*) getImageForSharing
{
    NSInteger imageTag = activityController.selectedPictureIndexPortrait;
    NSString *keyForData = [NSString stringWithFormat:@"%@%i", activityController.cameraModePhoto ? @"Photo" : @"Video", imageTag];
    NSString *keyAngelForData = [NSString stringWithFormat:@"Angel%i", imageTag];
    NSInteger angel = ((NSNumber *)[activityController.allContentDictionary objectForKey:keyAngelForData]).integerValue;
    UIImage *rotatedImage = activityController.cameraModePhoto ?
    [self image:[activityController.allContentDictionary objectForKey:keyForData] rotatedByDegrees:angel] :
    [activityController.allContentDictionary objectForKey:keyForData];
    
    return rotatedImage;
}

- (void)hidePopover
{
    [self.activityController.popoverController dismissPopoverAnimated:YES];
    self.activityController.popoverController = nil;
}

- (void)shareFBVideo
{
}

- (void) share
{
    FSNewViewController * newvc = ( FSNewViewController * )activityController;
    
    NSString *mimeType = !newvc.videoMode ? @"image/jpeg" : @"video/mp4";
    NSString *fileName = !newvc.videoMode ? @"FrameSelectionPhoto.jpg" : @"FrameSelectionVideo.mp4";
    
    NSData *attachmentData;
    
    if( newvc.videoMode )
    attachmentData = [ NSData dataWithContentsOfURL : [ newvc getMovieURL : newvc.selectedMediaIndex ] ];
    else
    attachmentData = UIImageJPEGRepresentation( [ newvc getPhoto : newvc.selectedMediaIndex ], 1.0 );
    
    NSString *body = [FSNewViewController fetchLocalizationValueForKey:@"description"];
    NSString *title = [FSNewViewController fetchLocalizationValueForKey:@"title"];
    NSString * graphPath = !newvc.videoMode ? @"me/photos" : @"me/videos";
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   attachmentData, fileName,
                                   mimeType, @"contentType",
                                   title, @"title",
                                   body, @"description",
                                   nil];
    if (!newvc.videoMode)
    [params setObject:body forKey:@"message"];
}

- (void)facebookShareResource
{
    [self shareFBVideo];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0: {
            [self shareFBVideo];
            break;
        }
        
        case 1: {
            [self fbLogOut];
            [self shareFBVideo];
            break;
        }
        
        default:
        break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0: {
            [self showEmailModalView];
            break;
        }
        case 1: {
            [self hidePopover];
            [self facebookShareResource];
            break;
        }
        
        default:
        break;
    }
    
}

#pragma mark - FB logInOut methods

- (void)performFBLogin
{

}

- (void) fbLogOut
{

}

@end
