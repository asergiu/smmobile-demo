#import <UIKit/UIKit.h>

@class FSNewPlayer;

@protocol FSNewPlayerDelegate <NSObject>

-( void )newPlayerFinished : ( FSNewPlayer * )player;

@end

@interface FSNewPlayer : UIView

@property ( nonatomic, weak ) IBOutlet id< FSNewPlayerDelegate > delegate;

-( void )buildPlayer : ( BOOL )controller_;
-( void )loadURL : ( NSURL * )url;
-( void )play;
-( void )stop;
-( void )rewind;

@end
