//
//  FSShareQrViewController.m
//
//
//  Created by Evgeny Eschenko on 12/04/16.
//
//

#import "FSShareQrViewController.h"
#import "FSNewViewController.h"

@interface FSShareQrViewController ()
{
    IBOutlet UIButton *cancelButton;
    IBOutlet UILabel *mainLabel;
}

@property (strong, nonatomic) IBOutletCollection (UIImageView) NSArray *qrPreviews;

- (IBAction)close:(id)sender;

@end

@implementation FSShareQrViewController

@synthesize delegate;
@synthesize qrPreviews;
@synthesize activityController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.layer.cornerRadius = 10.f;
    
    mainLabel.text = NSLocalizedString(@"Scan QR to get photos for share. URLs is avaliable for 1 hour.", nil);
    
    [cancelButton setTitle:NSLocalizedString(@"Close", nil) forState:UIControlStateNormal];
    
    for (int i = 0; i < [activityController.urlsToQR count]; i++)
    {
        NSData *stringData = [[activityController.urlsToQR objectAtIndex:i] dataUsingEncoding: NSISOLatin1StringEncoding];
        CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
        [qrFilter setValue:stringData forKey:@"inputMessage"];
        [[qrPreviews objectAtIndex:i] setImage:[[UIImage alloc] initWithCIImage:qrFilter.outputImage]];
    }
}

- (void)close:(id)sender
{
    if (delegate && [delegate respondsToSelector:@selector(dismissQrSharePreview)])
        [delegate dismissQrSharePreview];
}

- (void)dealloc
{
    NSLog(@"dealloc QRController");
    activityController = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
