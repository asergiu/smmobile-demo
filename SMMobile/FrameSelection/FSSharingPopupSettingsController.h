//
//  FSSharingPopupSettingsController.h
//  frameselection
//
//  Created by Pavel Stoma on 2/29/12.
//

#import <UIKit/UIKit.h>

@interface FSSharingPopupSettingsController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITextField *titleBodyTextField;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (strong, nonatomic) IBOutlet UINavigationBar *titleNavigationBar;
@property (strong, nonatomic) IBOutlet UINavigationItem *navigationTitleItem;

@property (strong, nonatomic) IBOutlet UIButton *addLogoButton;
@property (strong, nonatomic) IBOutlet UIButton *removeLogoButton;
@property (strong, nonatomic) IBOutlet UIImageView *ecpLogoView;
@property (strong, nonatomic) IBOutlet UILabel *noLogoLabel;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;

@end
