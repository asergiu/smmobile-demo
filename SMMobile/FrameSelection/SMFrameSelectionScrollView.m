//
//  SMFrameSelectionScrollView.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 3/10/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import "SMFrameSelectionScrollView.h"

@interface SMFrameSelectionScrollView ()
{
    CGRect blindZoneRect;
}

@end

@implementation SMFrameSelectionScrollView

- (void)setBlindZoneWithRect:(CGRect)rect
{
    blindZoneRect = rect;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *result = [super hitTest:point withEvent:event];
    
    if (CGRectContainsPoint(blindZoneRect, point))
        self.scrollEnabled = NO;
    else
        self.scrollEnabled = YES;
    
    return result;
}

@end
