#import "FSNewPlayer.h"

//#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface FSNewPlayer()
{
    AVPlayer * player;
    AVPlayerLayer * layer;
    
    MPMoviePlayerController * controller;
    
    UIView * panel;
    UISlider * slider;
    UIButton * button;
    UILabel * leftLabel;
    UILabel * rightLabel;
    
    NSTimer * timer;
}

@end

@implementation FSNewPlayer

@synthesize delegate;

-( id )initWithCoder : ( NSCoder * )aDecoder
{
    self = [ super initWithCoder : aDecoder ];
    if( self )
    {
    }
    return self;
}

static const float PANEL_HEIGHT = 44.f;
static const float BUTTON_WIDTH = 50.f;
static const float LABEL_WIDTH = 60.f;

-( void )buildPlayer : ( BOOL )controller_
{
    if( controller_ )
    {
        self->controller = [ [ MPMoviePlayerController alloc ] init ];
        self->controller.view.frame = CGRectMake( 0, 0, self.frame.size.width, self.frame.size.height );
        self->controller.scalingMode = MPMovieScalingModeAspectFill;
        self->controller.repeatMode = MPMovieRepeatModeOne;
        self->controller.controlStyle = MPMovieControlStyleNone;
        
        [ [ NSNotificationCenter defaultCenter ] addObserver : self
                                                    selector : @selector( willExitFullScreen : )
                                                        name : MPMoviePlayerDidExitFullscreenNotification
                                                      object : nil ];
        
        [ self addSubview : self->controller.view ];
        
        panel = [ [ UIView alloc ] initWithFrame : CGRectMake( 0, self.frame.size.height - PANEL_HEIGHT, self.frame.size.width, PANEL_HEIGHT ) ];
        panel.backgroundColor = [ UIColor grayColor ];
        [ self addSubview : panel ];
        
        slider = [ [ UISlider alloc ] initWithFrame : CGRectMake( BUTTON_WIDTH + LABEL_WIDTH, 0, self.frame.size.width - BUTTON_WIDTH - LABEL_WIDTH * 2, PANEL_HEIGHT ) ];
        slider.continuous = YES;
        slider.highlighted = YES;
        
        slider.minimumTrackTintColor = [UIColor whiteColor];
        
        [slider setThumbImage:[UIImage imageNamed:@"FS_player_thumb"] forState:UIControlStateNormal];
        [slider setThumbImage:[UIImage imageNamed:@"FS_player_thumb"] forState:UIControlStateHighlighted];
        [slider setThumbImage:[UIImage imageNamed:@"FS_player_thumb"] forState:UIControlStateSelected];
        
        [slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        [slider addTarget:self action:@selector(sliderDidEndSliding:)
         forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchUpOutside)];
        
        UITapGestureRecognizer * gesture = [ [ UITapGestureRecognizer alloc ] initWithTarget : self action : @selector( handleSingleTap : ) ];
        [ slider addGestureRecognizer : gesture ];
        [ panel addSubview : slider ];
        
        button = [ UIButton buttonWithType : UIButtonTypeCustom ];
        button.frame = CGRectMake( 0, 0, BUTTON_WIDTH, PANEL_HEIGHT );
        [ button addTarget : self action : @selector( onButton ) forControlEvents : UIControlEventTouchUpInside ];
        [ panel addSubview : button ];
        
        leftLabel = [ [ UILabel alloc ] initWithFrame : CGRectMake( BUTTON_WIDTH, 0, LABEL_WIDTH, PANEL_HEIGHT ) ];
        leftLabel.text = @"0:00";
        leftLabel.textAlignment = NSTextAlignmentCenter;
        leftLabel.font = [UIFont fontWithName:@"TrebuchetMS" size:14.f];
        [ panel addSubview : leftLabel ];
        
        rightLabel = [ [ UILabel alloc ] initWithFrame : CGRectMake( self.frame.size.width - LABEL_WIDTH, 0, LABEL_WIDTH, PANEL_HEIGHT ) ];
        rightLabel.text = @"0:00";
        rightLabel.textAlignment = NSTextAlignmentCenter;
        rightLabel.font = [UIFont fontWithName:@"TrebuchetMS" size:14.f];
        [ panel addSubview : rightLabel ];
    }
    else
    {
        layer = [ AVPlayerLayer layer ];
        
        layer.frame = CGRectMake( 0, 0, self.frame.size.width, self.frame.size.height );
        layer.backgroundColor = [ UIColor blackColor ].CGColor;
        layer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        
        [ self.layer addSublayer : layer ];
    }
}

-( void )willExitFullScreen : ( NSNotification * )notification
{
    self->controller.scalingMode = MPMovieScalingModeFill;
    self->controller.scalingMode = MPMovieScalingModeAspectFill;
}

-( void )loadURL : ( NSURL * )url
{    
    if( controller )
    {
        [ controller stop ];
        [ controller setContentURL : url ];
        
        return;
    }
    
    [ [ NSNotificationCenter defaultCenter ] removeObserver : self ];
    
    if( !url )
    {
        player = nil;
        layer.player = nil;
        return;
    }
    
    player = [ AVPlayer playerWithURL : url ];
    layer.player = player;
    
    player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    [ [ NSNotificationCenter defaultCenter ] addObserver : self
                                                selector : @selector( playerItemDidReachEnd : )
                                                    name : AVPlayerItemDidPlayToEndTimeNotification
                                                  object : player.currentItem ];
}

-( void )stop
{
    if( controller )
    {
        [ controller stop ];
    
        [ timer invalidate ];
        timer = nil;
        
        [ button setBackgroundImage : [ UIImage imageNamed : @"PlayFS_btn" ] forState : UIControlStateNormal ];
    }
    
    [ player pause ];
}

-( void )pause
{
    if( controller )
    {
        [ controller pause ];
        
        [ timer invalidate ];
        timer = nil;
        
        [ button setBackgroundImage : [ UIImage imageNamed : @"PlayFS_btn" ] forState : UIControlStateNormal ];
    }
    
    [ player pause ];
}

-( void )play
{    
    if( controller )
    {
        [ controller play ];
        
        timer = [ NSTimer scheduledTimerWithTimeInterval : 0.02f
                                                  target : self
                                                selector : @selector( updateControls )
                                                userInfo : nil
                                                 repeats : YES ];
        
        [ button setBackgroundImage : [ UIImage imageNamed : @"PauseFS_btn" ] forState : UIControlStateNormal ];
        
        return;
    }
    
    [ self rewind ];
    
    [ player play ];
}

- (void)sliderValueChanged:(UISlider *)sender
{
    [self pause];
    
    controller.currentPlaybackTime = sender.value * controller.duration;
    
    leftLabel.text = [ self secondsToMMSS : controller.currentPlaybackTime ];
    rightLabel.text = [ NSString stringWithFormat : @"-%@", [ self secondsToMMSS : controller.duration - controller.currentPlaybackTime ] ];
}

- (void)sliderDidEndSliding:(UISlider *)sender
{
    [self play];
}

-( void )rewind
{
    [ player.currentItem seekToTime : kCMTimeZero ];
}

-( void )updateControls
{
    float value = controller.currentPlaybackTime / controller.duration;
    slider.value = value;
    
    leftLabel.text = [ self secondsToMMSS : controller.currentPlaybackTime ];
    rightLabel.text = [ NSString stringWithFormat : @"-%@", [ self secondsToMMSS : controller.duration - controller.currentPlaybackTime ] ];
}

-( NSString * )secondsToMMSS : ( double )seconds
{
    NSInteger time = floor( seconds );
    NSInteger hh = time / 3600;
    NSInteger mm = ( time / 60 ) % 60;
    NSInteger ss = time % 60;
    
    if( hh > 0 )
        return  [ NSString stringWithFormat : @"%d:%02i:%02i", hh, mm, ss ];
    else
        return  [ NSString stringWithFormat : @"%02i:%02i", mm, ss ];
}

-( void )onButton
{
    if( controller.playbackState == MPMoviePlaybackStatePlaying )
        [ self pause ];
    else
        [ self play ];
}

-( void )playerItemDidReachEnd : ( NSNotification * )notification
{
    /*if( !self.nextPlayer )
        return;
     
    [ self.nextPlayer->player.currentItem seekToTime : kCMTimeZero ];
    [ self.nextPlayer play ];*/
    
    if (delegate && [delegate respondsToSelector:@selector(newPlayerFinished:)])
        [delegate newPlayerFinished:self];
}

-( void )handleSingleTap : ( UITapGestureRecognizer * )recognizer
{
    CGPoint point = [ recognizer locationInView : recognizer.view ];
    float value = point.x / recognizer.view.frame.size.width;
    controller.currentPlaybackTime = value * controller.duration;
    [ self updateControls ];
}

-( void )dealloc
{
    [ [ NSNotificationCenter defaultCenter ] removeObserver : self ];
}

@end
