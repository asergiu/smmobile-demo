#import <UIKit/UIKit.h>

#import "FSNewPlayer.h"
#import "UIZoomImageView.h"
#import "SMPenView.h"
#import "UIViewController+ThumbnailsAnimation.h"
#import "SMFrameSelectionScrollView.h"

@class FSPopoverControllerTable;

@interface FSNewViewController: UIViewController < UITabBarDelegate, FSNewPlayerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIZoomImageViewDelegate, UIScrollViewDelegate>
{
    BOOL useFrontCamera;
    BOOL compareMode;
    BOOL penActivated;
    NSTimer *timer;
    NSTimer *timerHideExposure;
    IBOutlet UIView *sliderView;
    IBOutlet UISlider *zoomSlider;
    IBOutlet UIButton *penToolButton;
    int currentColor;
}

@property (nonatomic, strong) IBOutlet UIView *cameraPreviewContainer;
@property (nonatomic, strong) IBOutlet UIView *controlPanel;
@property (nonatomic, strong) IBOutlet UIView *showControlView;
@property (nonatomic, strong) IBOutlet UISlider *videoModeSlider;
@property (nonatomic, strong) IBOutlet UISwitch *hideThumbsSwitch;
@property (nonatomic, strong) IBOutlet UIPickerView *videoModePicker;

@property (nonatomic, strong) IBOutlet UIImageView *testImageView;
@property (nonatomic, strong) IBOutlet UIImageView *videoIndicatorImageView;

@property (nonatomic, strong) IBOutlet UIView *singleViewContainer;
@property (nonatomic, strong) IBOutlet UIView *multiViewContainer;

@property (nonatomic, strong) IBOutlet UIImageView *tabBarImageBackground;

@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *multiImagePreview;

@property (nonatomic, strong) IBOutletCollection(FSNewPlayer) NSArray *multiMoviePreview;

@property (nonatomic, strong) IBOutlet FSNewPlayer *singleMoviePreview;

@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *thumbnailButtons;
@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *thumbnailImages;
@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *thumbnailBorders;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *tabBarButtons;

@property (nonatomic, strong) IBOutlet UIImageView *substrateImage;
@property (nonatomic, strong) IBOutlet UIImageView *videoRecordingStatus;

@property (nonatomic, assign) NSInteger selectedPictureIndexPortrait;
@property (nonatomic, assign) NSInteger selectedPictureIndexLandscape;
@property (strong, nonatomic) NSMutableDictionary *allContentDictionary;
@property (nonatomic, assign) BOOL cameraModePhoto;
@property (nonatomic, assign) int currentIndex;
@property (nonatomic, strong) NSURL *storeURL;

@property (nonatomic, strong) UIPopoverController *popoverController;
@property (nonatomic, strong) FSPopoverControllerTable *popoverTableController;

@property (nonatomic, strong) NSArray *selectedMedia;
@property (nonatomic, strong) NSMutableArray *allMedia;
@property (nonatomic, strong) NSMutableArray *mediaViews;

@property (nonatomic, strong) IBOutlet UIView *settingCameraView;
@property (nonatomic, strong) IBOutlet UIImageView *frameImage;
@property (nonatomic, strong) IBOutlet UISlider *sliderExposure;

@property (nonatomic, strong) IBOutlet SMFrameSelectionScrollView *contentScrollView;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *colorButtons;
@property ( nonatomic, strong ) IBOutlet SMPenView *penView;

@property (nonatomic, strong) NSMutableArray *urlsToQR;

+ (void)changeLocalizationValue:(NSString *)value forKey:(NSString *)key;
+ (NSString *)fetchLocalizationValueForKey:(NSString *)key;

- (IBAction)pressTabBarButton:(UIButton *)sender;
- (IBAction)changeCamera:(id)sender;
- (IBAction)thumbnailTouched:(UIButton *)sender;
- (IBAction)compareThumbnailTouched:(UIButton *)sender;
- (IBAction)videoModeChanged:(int)mode;
- (IBAction)hideThumbs;

- (IBAction)sliderAction:(UISlider *)sender;
- (IBAction)sliderExposureAction:(UISlider *)sender;

- (void)showExposureSliderView;
- (void)hideExposureSliderView;

- (IBAction)sliderExposureBegan:(id)sender;
- (IBAction)sliderExposureEnded:(id)sender;

- (void)reset;
- (void)refreshThumbnails;

- (int)selectedMediaIndex;
- (UIImage *)getPhoto:(int)index;
- (NSURL *)getMovieURL:(int)index;
- (UIImage *)getMoviePreviewForUrl:(NSURL *)url;
- (BOOL)videoMode;

- (IBAction)penToolPressed:(id)sender;
- (IBAction)closeColors:(id)sender;
- (IBAction)selectPenColor:(UIButton *)sender;

@end
