//
//  UIZoomImageView.m
//  KodakLensIDS
//
//  Created by Dgut on 08.04.14.
//
//

#import "UIZoomImageView.h"

@interface UIZoomImageView()
{
    float scale;
    CGPoint shift;
    
    CGAffineTransform transform;
    CGSize size;
}

@end

@implementation UIZoomImageView

@synthesize delegate;

-( id )initWithCoder : ( NSCoder * )aDecoder
{
    self = [ super initWithCoder : aDecoder ];
    if( self )
    {
        self.userInteractionEnabled = YES;
        self.multipleTouchEnabled = YES;
        
        UIPinchGestureRecognizer * pinchRecognizer = [ [ UIPinchGestureRecognizer alloc ] initWithTarget : self action : @selector( pinch : ) ];
        [ self addGestureRecognizer : pinchRecognizer ];
        
        UIPanGestureRecognizer * panRecognizer = [ [ UIPanGestureRecognizer alloc ] initWithTarget : self action : @selector( pan : ) ];
        [ self addGestureRecognizer : panRecognizer ];
        
        scale = 1.f;
        shift = CGPointZero;
        
        UISwipeGestureRecognizer *gestureL =[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeInView)];
        [gestureL setDirection:UISwipeGestureRecognizerDirectionLeft];
        [self addGestureRecognizer:gestureL];
        
        UISwipeGestureRecognizer *gestureR =[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeInView)];
        [gestureR setDirection:UISwipeGestureRecognizerDirectionRight];
        [self addGestureRecognizer:gestureR];
        
        
        [panRecognizer requireGestureRecognizerToFail:gestureL];
        [panRecognizer requireGestureRecognizerToFail:gestureR];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.userInteractionEnabled = YES;
        self.multipleTouchEnabled = YES;
        
        UIPinchGestureRecognizer * pinchRecognizer = [ [ UIPinchGestureRecognizer alloc ] initWithTarget : self action : @selector( pinch : ) ];
        [ self addGestureRecognizer : pinchRecognizer ];
        
        UIPanGestureRecognizer * panRecognizer = [ [ UIPanGestureRecognizer alloc ] initWithTarget : self action : @selector( pan : ) ];
        [ self addGestureRecognizer : panRecognizer ];
        
        scale = 1.f;
        shift = CGPointZero;
        
        UISwipeGestureRecognizer *gestureL =[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeInView)];
        [gestureL setDirection:UISwipeGestureRecognizerDirectionLeft];
        [self addGestureRecognizer:gestureL];
        
        UISwipeGestureRecognizer *gestureR =[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeInView)];
        [gestureR setDirection:UISwipeGestureRecognizerDirectionRight];
        [self addGestureRecognizer:gestureR];
        
        
        [panRecognizer requireGestureRecognizerToFail:gestureL];
        [panRecognizer requireGestureRecognizerToFail:gestureR];
    }
    
    return self;
}

- (void)leftSwipeInView
{
    if (delegate && [delegate respondsToSelector:@selector(swipePhotoWithForwardDirection:)])
        [delegate swipePhotoWithForwardDirection:YES];
}

- (void)rightSwipeInView
{
    if (delegate && [delegate respondsToSelector:@selector(swipePhotoWithForwardDirection:)])
        [delegate swipePhotoWithForwardDirection:NO];
}

-( void )dealloc
{
}

-( void )saveTransform
{
    transform = self.transform;
    size = self.frame.size;
}

-( void )reset
{
    scale = 1.f;
    shift = CGPointZero;
    
    super.transform = transform;
}

-( void )pinch : ( UIPinchGestureRecognizer * )sender
{
    switch( sender.state )
    {
        case UIGestureRecognizerStateBegan:
            break;
            
        case UIGestureRecognizerStateChanged:
            {
                float s = scale * sender.scale;
                
                if( s < 1.f )
                    s = 1.f;
                
                float xabs = floorf( size.width * ( s - 1.f ) / 2.f / s );
                float yabs = floorf( size.height * ( s - 1.f ) / 2.f / s );
                
                if( shift.x > xabs )
                    shift.x = xabs;
                if( shift.x < -xabs )
                    shift.x = -xabs;
                
                if( shift.y > yabs )
                    shift.y = yabs;
                if( shift.y < -yabs )
                    shift.y = -yabs;
                
                super.transform = CGAffineTransformTranslate( CGAffineTransformScale( transform, s, s ), shift.x, shift.y );
            }
            break;
            
        case UIGestureRecognizerStateEnded:
            {
                scale *= sender.scale;
                
                if( scale < 1.f )
                    scale = 1.f;
                
                float xabs = floorf( size.width * ( scale - 1.f ) / 2.f / scale );
                float yabs = floorf( size.height * ( scale - 1.f ) / 2.f / scale );
                
                if( shift.x > xabs )
                    shift.x = xabs;
                if( shift.x < -xabs )
                    shift.x = -xabs;
                
                if( shift.y > yabs )
                    shift.y = yabs;
                if( shift.y < -yabs )
                    shift.y = -yabs;
                
                super.transform = CGAffineTransformTranslate( CGAffineTransformScale( transform, scale, scale ), shift.x, shift.y );
            }
            break;
            
        default:
            break;
    }
}

-( void )pan : ( UIPanGestureRecognizer * )sender
{
    CGPoint transition = [ sender translationInView : self ];
    
    switch( sender.state )
    {
        case UIGestureRecognizerStateBegan:
            break;
            
        case UIGestureRecognizerStateChanged:
            {
                CGPoint t = CGPointMake( shift.x + transition.x, shift.y + transition.y );
                
                float xabs = floorf( size.width * ( scale - 1.f ) / 2.f / scale );
                float yabs = floorf( size.height * ( scale - 1.f ) / 2.f / scale );
                
                if( t.x > xabs )
                    t.x = xabs;
                if( t.x < -xabs )
                    t.x = -xabs;
                
                if( t.y > yabs )
                    t.y = yabs;
                if( t.y < -yabs )
                    t.y = -yabs;
                
                super.transform = CGAffineTransformTranslate( CGAffineTransformScale( transform, scale, scale ), t.x, t.y );
            }
            break;
            
        case UIGestureRecognizerStateEnded:
            {
                shift.x += transition.x;
                shift.y += transition.y;
                
                float xabs = floorf( size.width * ( scale - 1.f ) / 2.f / scale );
                float yabs = floorf( size.height * ( scale - 1.f ) / 2.f / scale );
                
                if( shift.x > xabs )
                    shift.x = xabs;
                if( shift.x < -xabs )
                    shift.x = -xabs;
                
                if( shift.y > yabs )
                    shift.y = yabs;
                if( shift.y < -yabs )
                    shift.y = -yabs;
                
                super.transform = CGAffineTransformTranslate( CGAffineTransformScale( transform, scale, scale ), shift.x, shift.y );
            }
            break;
            
        default:
            break;
    }
}

@end
