//
//  UIImage+Extra.h
//  frameselection
//
//  Created by Pavel Stoma on 2/25/12.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extras)
- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize;
- (UIImage *)fixOrientationOfImage;
@end;

