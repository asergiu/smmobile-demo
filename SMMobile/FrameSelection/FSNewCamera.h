#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@interface FSNewCamera : NSObject< AVCaptureFileOutputRecordingDelegate >

@property (retain, nonatomic) AVCaptureDevice *device;

-( AVCaptureVideoPreviewLayer * )videoPreviewLayer;

-( void )setPreviewContainer : ( UIView * )container;

-( void )setFrontCamera : ( BOOL )front;
-( BOOL )frontCamera;

-( void )setVideoMode : ( BOOL )videoMode;
-( BOOL )videoMode;

-( void )takePhoto : ( void( ^ )( UIImage * image ) )handler;
-( BOOL )takeVideo : ( NSURL * )url : ( void( ^ )( NSURL * url ) )handler;

-( void )setScale : ( float )aScale;
-( float )scale;

-( BOOL )isRecording;

- (void)stopCamera;

- (BOOL)deviceScaleSupport;

@end
