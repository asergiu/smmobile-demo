//
//  FSShareProcessController.h
//
//
//  Created by Evgeny Eschenko on 12/04/16.
//
//

#import <UIKit/UIKit.h>

@protocol FSShareProcessControllerDelegate <NSObject>

@optional
- (void)dismissUploadPopover;
@end

@interface FSShareProcessController : UIViewController

@property (nonatomic, assign) id <FSShareProcessControllerDelegate> delegate;

@end
