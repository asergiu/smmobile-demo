//
//  FSShareProcessController.m
//
//
//  Created by Evgeny Eschenko on 12/04/16.
//
//

#import "FSShareProcessController.h"

@interface FSShareProcessController ()
{
    IBOutlet UIButton *cancelButton;
    IBOutlet UILabel *mainLabel;
    IBOutlet UIActivityIndicatorView *activityIndicator;
}

- (IBAction)close:(id)sender;

@end

@implementation FSShareProcessController

@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mainLabel.text = NSLocalizedString(@"Sending...", nil);
    
    [cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    
    [activityIndicator startAnimating];
    
    self.view.layer.cornerRadius = 25.f;
}

- (void)close:(id)sender
{
    if (delegate && [delegate respondsToSelector:@selector(dismissUploadPopover)])
        [delegate dismissUploadPopover];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
