//
//  FSShareChooseViewController.m
//  Visioner_4.0
//
//  Created by Oleg Bogatenko on 9/18/14.
//
//

#import "FSShareChooseViewController.h"
#import "FSNewViewController.h"
#import "FSAlertsHelper.h"
#import "UIWindow+VisibleController.h"

@interface FSShareChooseViewController () 
{
    NSMutableArray *movieURLs;
    NSMutableArray *selectedIndexes;
    
    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *shareButton;
    IBOutlet UILabel *mainLabel;
}

@property (strong, nonatomic) IBOutletCollection (UIView) NSArray *containers;
@property (strong, nonatomic) IBOutletCollection (UIImageView) NSArray *previews;
@property (strong, nonatomic) IBOutletCollection (UIButton) NSArray *buttons;

- (IBAction)close:(id)sender;
- (IBAction)share:(id)sender;
- (IBAction)previewTapped:(UIButton *)sender;

@end

@implementation FSShareChooseViewController

@synthesize delegate;
@synthesize containers;
@synthesize previews;
@synthesize buttons;
@synthesize activityController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.layer.cornerRadius = 10.f;
    
    for (UIView *container in containers)
        container.layer.cornerRadius = 5.f;
    
    for (UIImageView *preview in previews)
        preview.layer.cornerRadius = 5.f;
    
    selectedIndexes = [NSMutableArray new];
    
    if (activityController.videoMode)
    {
        movieURLs = [NSMutableArray new];
        
        for (UIButton *oneButton in buttons)
        {
            NSURL *movieURL = [activityController getMovieURL:(int)oneButton.tag];
            
            if (movieURL)
            {
                [movieURLs addObject:[activityController getMovieURL:(int)oneButton.tag]];
                
                [((UIImageView *)previews[(int)oneButton.tag]) setImage:[activityController getMoviePreviewForUrl:movieURL]];
            }
            else
                oneButton.enabled = NO;
        }
        
        mainLabel.text = NSLocalizedString(@"Please select videos to share", nil);
    }
    else
    {
        for (UIButton *oneButton in buttons)
        {
            if ([activityController getPhoto:(int)oneButton.tag])
                [((UIImageView *)previews[(int)oneButton.tag]) setImage:[activityController getPhoto:(int)oneButton.tag]];
            else
                oneButton.enabled = NO;
        }
        
        mainLabel.text = NSLocalizedString(@"Please select photos to share", nil);
    }
    
    [cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    [shareButton setTitle:NSLocalizedString(@"Share", nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)close:(id)sender
{
    if (delegate && [delegate respondsToSelector:@selector(dismissSharePreview)])
        [delegate dismissSharePreview];
}

- (void)previewTapped:(UIButton *)sender
{
    sender.selected = !sender.isSelected;
    
    if (sender.isSelected)
        [selectedIndexes addObject:@(sender.tag)];
    else
        [selectedIndexes removeObject:@(sender.tag)];
}

- (void)dealloc
{
    NSLog(@"dealloc shareController");
    selectedIndexes = nil;
    activityController = nil;
}

- (void)share:(id)sender
{
    if (delegate && [delegate respondsToSelector:@selector(sharePressed:)])
        [delegate sharePressed:selectedIndexes];
}

@end
