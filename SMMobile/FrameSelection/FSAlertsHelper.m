#import "FSAlertsHelper.h"
#import "UIWindow+VisibleController.h"

static NSMutableDictionary *alerts = nil;

@implementation FSAlertsHelper

@synthesize alertView;
@synthesize ownerController;

- (id)initWithOwner:(FSNewViewController *)owner
{
    self = [super init];
    if (self) {
        //self.ownerController = owner;
    }
    return  self;
}

- (NSString *)stringifyType:(FSAlertType)type
{
	switch (type) {
		case FSAlertTypeDeleteContent: {
			return @"DeleteContent";
		}			
        case FSAlertTypeNoContentForSharing: {
            return @"NoContentForSharing";
        }
        case FSAlertTypeNewSessionCantStart: {
            return @"NewSessionCantStarting";
        }
        case FSAlertTypeCameraDisabled: {
            return @"CameraDisabled";
        }
        case FSAlertTypeSendEmailFailed: {
            return @"SendingEmailFailed";
        }
        case FSAlertTypeNoCamera: {
            return @"NoCamera";
        }
        case FSAlertTypeCameraNotWorking: {
            return @"CameraNotWorking";
        }
        case FSAlertTypeNoContentForShowing: {
            return @"NoContentForShowing";
        }
        case FSAlertTypeMediaSuccesfullySent: {
            return @"MediaSuccesfullySent";
        }
        case FSAlertTypeNoContentForMode: {
            return @"NoContentForMode";
        }
        case FSAlertTypeNoContentSelected: {
            return @"NoContentSelected";
        }
            
		default: {
			return nil;
		}
	}
}

- (void)showAlert:(FSAlertType)type
{
	[self showAlert:type withValue:0];
}

- (void)showAlert:(FSAlertType)type withValue:(NSInteger)value
{
	NSString *alertKey = [self stringifyType:type];
    
	if (alerts == nil) {
		NSString *filePath = [[NSBundle mainBundle] pathForResource:@"FSAlerts" ofType:@"plist"];
		alerts = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
	}
	
	NSString* message = [alerts objectForKey:alertKey];
	
    if (message) {
		NSArray *splitArray = [message componentsSeparatedByString:@"|"];
		NSString *body = [splitArray objectAtIndex:1];
        
		NSRange range = [body rangeOfString:@"%i"];
		if (range.location != NSNotFound) {
			body = [NSString stringWithFormat:body, value];
		}
		
        NSString *buttonNameString = NSLocalizedString(@"OK", nil);
        NSString *cancelNameString = nil;
        if (type == FSAlertTypeDeleteContent) {
            cancelNameString = NSLocalizedString(@"Cancel", nil) ;
            buttonNameString = NSLocalizedString(@"Delete", nil);
        }
        
		alertView = [[UIAlertView alloc] initWithTitle:[splitArray objectAtIndex:0]
                                               message:body
                                              delegate:[UIApplication.sharedApplication.windows[0] visibleViewController]
                                     cancelButtonTitle:nil
                                     otherButtonTitles:buttonNameString, cancelNameString, nil ];
        
        alertView.tag = type;
        
		[alertView show];
	}
}

@end
