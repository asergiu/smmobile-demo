//
//  FSShareChooseViewController.h
//  Visioner_4.0
//
//  Created by Oleg Bogatenko on 9/18/14.
//
//

#import <UIKit/UIKit.h>

@class FSNewViewController;

@protocol FSShareChooseViewControllerDelegate <NSObject>

@optional
- (void)dismissSharePreview;
- (void)sharePressed:(NSArray *)selected;

@end

@interface FSShareChooseViewController : UIViewController

@property (nonatomic, assign) id <FSShareChooseViewControllerDelegate> delegate;

@property (nonatomic, strong) FSNewViewController *activityController;

@end
