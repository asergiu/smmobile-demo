#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>

@class FSNewViewController;

@interface FSPopoverControllerTable : UITableViewController <MFMailComposeViewControllerDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) NSArray *popoverValues;
@property (nonatomic, strong) FSNewViewController *activityController;
@property (nonatomic, strong) NSString *youtubeLink;

- (UIImage *)getImageForSharing;

@end
