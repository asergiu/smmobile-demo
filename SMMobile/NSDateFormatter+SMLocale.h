//
//  NSDateFormatter+SMLocale.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 10/6/15.
//  Copyright © 2015 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (SMLocale)

- (id)initWithSafeLocale;

@end
