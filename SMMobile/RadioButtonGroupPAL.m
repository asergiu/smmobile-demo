//
//  RadioButtonGroup.m
//  thickness
//
//  Created by Vladimir Malashenkov on 05.05.12.
//

#import "RadioButtonGroupPAL.h"


#define kVerticalSeparator 30.0f

@implementation RadioButtonGroupPAL

@synthesize groupTitle;
@synthesize delegate;
@synthesize selectedItem;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
       self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (int)selectedItem
{
    if (!buttonList) {
        return -1;
    }
    
    for (UIButton* insButton in buttonList)
    {
        if ([insButton state] == UIControlStateDisabled) {
            return -1;
        }
        if (insButton.selected)
            return (int)[buttonList indexOfObject:insButton];
    }
    
    return 0;
}
- (void)but:(UIButton *)sender
{
    [sender setBackgroundColor:[UIColor clearColor]];
}

- (NSArray *)setLabelsForRadioButtons:(NSString *)labels,...;
{
    allLabels = [[NSMutableArray alloc] initWithCapacity:1];
    va_list args;
    va_start(args, labels);
    
    for (NSString *arg = labels; arg != nil; arg = va_arg(args, NSString*))
    {
        [allLabels addObject:arg];
    }
    
    va_end(args);
    
    return allLabels;
}

- (void)drawRect:(CGRect)rect
{
    self.userInteractionEnabled = YES;
    
    buttonList = [[NSMutableArray alloc] initWithCapacity:1];
    
    if (groupTitle != nil)
    {
        UILabel *_groupTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
        _groupTitle.text = self.groupTitle;
        _groupTitle.font = [UIFont boldSystemFontOfSize:15.0f];
        _groupTitle.backgroundColor = [UIColor clearColor];
        _groupTitle.textColor = [UIColor blackColor];
        
        [self addSubview:_groupTitle];
        
        for (int i=0; i < allLabels.count; i++)
        {
            UIButton * radioButtonItem = [UIButton buttonWithType:UIButtonTypeCustom];
            
            radioButtonItem.backgroundColor = [UIColor clearColor];
            [radioButtonItem addTarget:self action:@selector(checkboxButton:) forControlEvents:UIControlEventTouchDown];
            radioButtonItem.tag = i;
            radioButtonItem.frame = CGRectMake(0.0, (i+1) * kVerticalSeparator, 30, 30);
            
            UILabel* buttonLabel= [[UILabel alloc] initWithFrame:CGRectMake(35.0, (i+1) * kVerticalSeparator, 350, 30)];
            buttonLabel.backgroundColor=[UIColor clearColor];
            buttonLabel.text = [allLabels objectAtIndex:i];
            buttonLabel.font = [UIFont boldSystemFontOfSize:15.0f];
            buttonLabel.textColor= [UIColor blackColor];
            
            [radioButtonItem setImage:[UIImage imageNamed:@"radio_btn"] forState:UIControlStateNormal];
            [radioButtonItem setImage:[UIImage imageNamed:@"radio_btn_active"] forState:UIControlStateSelected];
            [radioButtonItem setImage:[UIImage imageNamed:@"radio_btn_disable"] forState:UIControlStateDisabled];
            
            [buttonList addObject:radioButtonItem];
            
            [self addSubview:buttonLabel];
            [self addSubview:radioButtonItem];
        }
    }
    
    [self setSelectedItemIndex:self.selectedItem];
	
    if (delegate && [delegate respondsToSelector:@selector(clearSelection)]) {
		[delegate clearSelection];
	}
}

- (void)checkboxButton:(UIButton *)button
{
    NSInteger indexOfSelectedButton = (NSInteger)[buttonList indexOfObject:button];
    [self setSelectedItemIndex: indexOfSelectedButton];
	
    if (delegate && [delegate respondsToSelector:@selector(didSelectRadiobutton:ItemAtIndex:)]) {
		[delegate didSelectRadiobutton:self ItemAtIndex:indexOfSelectedButton];
	}
}

- (void)disableItemAtIndex:(NSInteger)index
{
    if ([[buttonList objectAtIndex:index] isSelected])
    {
        [[buttonList objectAtIndex:index] setSelected:NO];
        [[buttonList objectAtIndex:0] setSelected:YES];
    }
    
    [[buttonList objectAtIndex:index] setEnabled:NO];
}

- (void)enableItemAtIndex:(NSInteger)index
{
    if ([[buttonList objectAtIndex:index] isSelected])
    {
        [[buttonList objectAtIndex:index] setSelected:YES];
        [[buttonList objectAtIndex:0] setSelected:NO];
    }
    
    [[buttonList objectAtIndex:index] setEnabled:YES];
}

- (void)setStatusForAllItemsInRadioGroupEnabled:(BOOL)enabled
{
    if (enabled) {
        for (int i = 0; i<3; i++) {
            [self enableItemAtIndex:i];
        }
        [self setSelectedItemIndex:1];
    } else {
        for (int i = 0; i<3; i++) {
            [self disableItemAtIndex:i];
        }
        [self setSelectedItemIndex:-1];
    }
}

- (void)enableAllItems
{
    for (UIButton *insButton in buttonList)
    {
        [insButton setEnabled:YES];
    }
}

- (void)setSelectedItemIndex:(NSInteger)index
{
    for (UIButton* insButton in buttonList)
    {
        [insButton setSelected:NO];
    }
    
    if (index != -1) {
        [[buttonList objectAtIndex:(int)index] setSelected:YES];
        self.selectedItem = (int)index;
    }
    
    if (index == -1) {
        self.selectedItem = -1;
    }
}

@end
