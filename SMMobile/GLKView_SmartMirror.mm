#import "GLKView_SmartMirror.h"
#import "SMColorTrackingCamera.h"
#import "SMImageLoader.h"
#import "SMLogSaver.h"

@interface GLKView_SmartMirror()< SMColorTrackingCameraDelegate >
{
    BOOL premultiplicationParameter;
    
    BOOL playing;
    BOOL videoMode;
    BOOL frontCamera;
    
    NSString * cameraPreset;
    
    float fps;
    
    NSTimer * timer;
    SMColorTrackingCamera * camera;
    
    GLuint videoFrameTexture;
    CGSize videoFrameTextureSize;
    
    GLuint photoTexture;
    CGSize photoTextureSize;
    
    photoCallback photoCallback;
    
    NSLock *_frameLock;
}

@end

@implementation GLKView_SmartMirror

#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

-( id )initWithCoder : ( NSCoder * )aDecoder
{
    self = [ super initWithCoder : aDecoder ];
    if( self )
    {
        EAGLContext * context = [ [ EAGLContext alloc ] initWithAPI : kEAGLRenderingAPIOpenGLES2 ];
        [ EAGLContext setCurrentContext : context ];
        
        self.context = context;
        
        fps = 30.f;
        
        videoFrameTexture = [ self genTexture ];
        photoTexture = [ self genTexture ];
        
        _frameLock = [NSLock new];
        
        glEnableVertexAttribArray( GLKVertexAttribPosition );
        glEnableVertexAttribArray( GLKVertexAttribNormal );
        glEnableVertexAttribArray( GLKVertexAttribTexCoord0 );
        
        [ self checkError ];
        
        [ [ NSNotificationCenter defaultCenter ] addObserver : self
                                                    selector : @selector( applicationDidBecomeActive )
                                                        name : UIApplicationDidBecomeActiveNotification
                                                      object : nil ];
        
        [[NSNotificationCenter defaultCenter ] addObserver:self
                                                  selector:@selector(applicationWillResignActive)
                                                      name:UIApplicationWillResignActiveNotification
                                                    object:nil];
        
        if( SYSTEM_VERSION_LESS_THAN( @"7.0" ) )
            premultiplicationParameter = NO;
        else if( SYSTEM_VERSION_LESS_THAN( @"7.0.3" ) )
            premultiplicationParameter = YES;
        else
            premultiplicationParameter = NO;
        
        cameraPreset = AVCaptureSessionPreset640x480;
    }
    return self;
}

- (void)applicationWillResignActive
{
    [self stop];
}

- (void)applicationDidBecomeActive
{
    if (playing)
    {
        playing = NO;
        [self play];
    }
}

-( BOOL )playing
{
    return playing;
}

- (void)stop
{
    if (playing)
    {
        if (videoMode)
        {
            [camera stopCamera];
            camera = nil;
        }
        else
        {
            [timer invalidate];
            timer = nil;
        }
    }
}

-( BOOL )videoMode
{
    return self->videoMode;
}

-( void )setVideoMode : ( BOOL )videoMode
{
    if( self->videoMode == videoMode )
        return;
    
    BOOL oldPlaying = playing;
    
    if( oldPlaying )
        [ self stopWithDeallocCam:NO ];
    
    self->videoMode = videoMode;
    
    if( oldPlaying )
        [ self play ];
}

-( BOOL )frontCamera
{
    return frontCamera;
}

-( void )setFrontCamera : ( BOOL )frontCamera
{
    if( self->frontCamera == frontCamera )
        return;
    
    self->frontCamera = frontCamera;
    
    if( playing && videoMode )
    {
        [ self stopWithDeallocCam:YES ];
        [ self play ];
    }
    else
        camera = nil;
}

-( void )setCameraPreset : ( NSString * )cameraPreset
{
    if( self->cameraPreset == cameraPreset )
        return;
    
    self->cameraPreset = cameraPreset;
    
    if( playing && videoMode )
    {
        [ self stopWithDeallocCam:YES ];
        [ self play ];
    }
    else
        camera = nil;
}

-( GLuint )videoFrameTexture
{
    return videoFrameTexture;
}

-( CGSize )videoFrameTextureSize
{
    return videoFrameTextureSize;
}

-( void )setVideoFrameTextureSize : ( CGSize )size
{
    videoFrameTextureSize = size;
}

-( GLuint )photoTexture
{
    return photoTexture;
}

-( CGSize )photoTextureSize
{
    return photoTextureSize;
}

-( GLuint )genTexture
{
    [ EAGLContext setCurrentContext : self.context ];
    
    GLuint texture;
    
    glGenTextures( 1, &texture );
    glBindTexture( GL_TEXTURE_2D, texture );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    
    return texture;
}

-( GLuint )genFramebufferWithTexture : ( GLuint )texture width : ( GLsizei )width height : ( GLsizei )height
{
    [ EAGLContext setCurrentContext : self.context ];
    
    GLuint framebuffer, renderbuffer;
    
    glGenFramebuffers( 1, &framebuffer );
    glBindFramebuffer( GL_FRAMEBUFFER, framebuffer );
    glGenRenderbuffers( 1, &renderbuffer );
    glBindRenderbuffer( GL_RENDERBUFFER, renderbuffer );
    glRenderbufferStorage( GL_RENDERBUFFER, GL_RGBA8_OES, width, height );
    glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, renderbuffer );
    glBindTexture( GL_TEXTURE_2D, texture );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0 );
    glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0 );
    
    return framebuffer;
}

-( void )setFPS : ( float )fps
{
    self->fps = fps;
}

-( float )fps
{
    return fps;
}

-( void )play
{
    if( playing )
        return;
    
    playing = YES;
    
    if( videoMode )
    {
        if( !camera )
        {
            camera = [ [ SMColorTrackingCamera alloc ] initWithPreset : cameraPreset
                                                               camera : frontCamera ? AVCaptureDevicePositionFront : AVCaptureDevicePositionBack ];
            camera.delegate = self;
        }
        
        [ camera startCamera ];
    }
    else
    {
        timer = [ NSTimer scheduledTimerWithTimeInterval : 1.f / fps
                                                  target : self
                                                selector : @selector( display )
                                                userInfo : nil
                                                 repeats : YES ];
    }
}

-( void )stopWithDeallocCam:(BOOL)deallocCam
{
    playing = NO;
    
    if( videoMode )
    {
        [ camera stopCamera ];
        
        if (deallocCam)
            camera = nil;
    }
    else
    {
        [ timer invalidate ];
        timer = nil;
    }
}

-( void )killCamera
{
    if( videoMode )
    {
        [ self stopWithDeallocCam:YES ];
    }
}

- (BOOL)isRunningCamera
{
    if (camera && [camera isActiveSession])
    {
        return YES;
    }
    
    return NO;
}

-( void )takePhoto : ( photoCallback )callback
{
    self->photoCallback = [callback copy];
    
    [ camera takePhoto ];
}

- (void)showFlash:(BOOL)show
{
    [camera showFlash:show];
}

-( void )processCameraFrame : ( CVImageBufferRef )cameraFrame
{
    if( self.superview.hidden )
    {
        return;
    }
    
    [_frameLock lock];

    [ EAGLContext setCurrentContext : self.context ];
    
	CVPixelBufferLockBaseAddress( cameraFrame, 0 );
    
    void * data = CVPixelBufferGetBaseAddress( cameraFrame );
	u_int height = (u_int) CVPixelBufferGetHeight( cameraFrame );
	u_int width =  (u_int) CVPixelBufferGetWidth( cameraFrame );
    
    u_int row = (u_int) CVPixelBufferGetBytesPerRow(cameraFrame);
    
    videoFrameTextureSize.width  = width;
    videoFrameTextureSize.height = height;
    
    u_int extraBytes = row - width * 4;
    
	glBindTexture( GL_TEXTURE_2D, videoFrameTexture );
    
    if (extraBytes != 0)
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, row / 4, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, data );
    else
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, data );
    
    CVPixelBufferUnlockBaseAddress( cameraFrame, 0 );
    
	[ self display ];
    
    [_frameLock unlock];
}

-( void )processPhoto : ( CVImageBufferRef )imageData
{
    [_frameLock lock];
    
    [ EAGLContext setCurrentContext : self.context ];
    
    CVPixelBufferLockBaseAddress( imageData, 0 );
    
    void * data = CVPixelBufferGetBaseAddress( imageData );
    int bufferHeight = ( int )CVPixelBufferGetHeight( imageData );
    int bufferWidth = ( int )CVPixelBufferGetWidth( imageData );
    
    photoTextureSize.width = bufferWidth;
    photoTextureSize.height = bufferHeight;
    
    glBindTexture( GL_TEXTURE_2D, photoTexture );
    
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, bufferWidth, bufferHeight, 0, GL_BGRA, GL_UNSIGNED_BYTE, data );
    
    CVPixelBufferUnlockBaseAddress( imageData, 0 );
    
    if( photoCallback )
    {
        [LOG saveLogData:@{ @"action:" : @"GLKView_SmartMirror - processPhoto" }];
        
        if (camera.photo)
            photoCallback(YES);
        else
            photoCallback(NO);
        
        photoCallback = NULL;
    }
    
    [_frameLock unlock];
}

-( SMColorTrackingCamera * )camera
{
    return camera;
}

-( AVCaptureDevice * )cameraDevice
{
    return camera.device;
}

-( void )checkError
{
    [ EAGLContext setCurrentContext : self.context ];
    
    GLenum error = glGetError();
    switch( error )
    {
        case GL_NO_ERROR:
            NSLog( @"GL_NO_ERROR" );
            break;
            
        case GL_INVALID_ENUM:
            NSLog( @"GL_INVALID_ENUM" );
            break;
            
        case GL_INVALID_VALUE:
            NSLog( @"GL_INVALID_VALUE" );
            break;
            
        case GL_INVALID_OPERATION:
            NSLog( @"GL_INVALID_OPERATION" );
            break;
            
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            NSLog( @"GL_INVALID_FRAMEBUFFER_OPERATION" );
            break;
            
        case GL_OUT_OF_MEMORY:
            NSLog( @"GL_OUT_OF_MEMORY" );
            break;
    }
}

/*-( UIImage * )getImage
{
    [ EAGLContext setCurrentContext : self.context ];
    
    int width = self.drawableWidth;
    int height = self.drawableHeight;
    
    int size = width * height * 4;
    
    GLubyte * buffer = ( GLubyte * )malloc( size );
    glReadPixels( 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer );
    
    // make data provider with data.
    CGDataProviderRef providerRef = CGDataProviderCreateWithData( NULL, buffer, size, NULL );
    
    // prep the ingredients
    int bitsPerComponent = 8;
    int bitsPerPixel = 32;
    int bytesPerRow = 4 * width;
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    
    // make the cgimage
    CGImageRef imageRef = CGImageCreate( width, height, bitsPerComponent, bitsPerPixel, bytesPerRow, colorSpaceRef, bitmapInfo, providerRef, NULL, NO, renderingIntent );
    
    // then make the uiimage from that
    UIImage * image = [ [ UIImage alloc ] initWithCGImage : imageRef ];
    CGImageRelease( imageRef );
    CGDataProviderRelease( providerRef );
    CGColorSpaceRelease( colorSpaceRef );
    free( buffer );
    //free(buffer2);
    
    return image;
}*/

-( BOOL )premultiplicationParameter
{
    return premultiplicationParameter;
}

-( void )setPremultiplicationParameter : ( BOOL )premultiplication
{
    premultiplicationParameter = premultiplication;
}

-( GLKTextureInfo * )getTexture : ( NSString * )name
{
    [EAGLContext setCurrentContext:self.context];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], GLKTextureLoaderOriginBottomLeft, [NSNumber numberWithBool:premultiplicationParameter], GLKTextureLoaderApplyPremultiplication, nil];
    
    NSArray *components = [name componentsSeparatedByString:@"."];
    
    if (components.count > 1){
        
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:name ofType:NULL];
        
        return [ GLKTextureLoader textureWithContentsOfFile : bundlePath options : options error : NULL ];
        
    } else {
        
        return [GLKTextureLoader textureWithCGImage:[SM_LOADER getImageForName:name].CGImage options:options error:NULL];
    }
}

-( GLKTextureInfo * )getTextureFromAssets : ( NSString * )name
{
    [EAGLContext setCurrentContext:self.context];
    
    NSDictionary *options = @{ GLKTextureLoaderApplyPremultiplication : @(premultiplicationParameter),
                               GLKTextureLoaderOriginBottomLeft : @(YES) };
    
    UIImage *img = [UIImage imageNamed:name];

    return [GLKTextureLoader textureWithCGImage:img.CGImage options:options error:NULL];
}

-( BOOL )compileShader : ( GLuint * )shader type : ( GLenum )type file : ( NSString * )file
{
    GLint status;
    const GLchar * source;
    
    source = ( GLchar * )[ [ NSString stringWithContentsOfFile : file encoding : NSUTF8StringEncoding error : nil ] UTF8String ];
    if( !source )
    {
        NSLog( @"Failed to load vertex shader" );
        return NO;
    }
    
    *shader = glCreateShader( type );
    glShaderSource( *shader, 1, &source, NULL );
    glCompileShader( *shader );
    
#ifdef DEBUG
    GLint logLength;
    glGetShaderiv( *shader, GL_INFO_LOG_LENGTH, &logLength );
    if( logLength > 0 )
    {
        GLchar * log = ( GLchar * )malloc( logLength );
        glGetShaderInfoLog( *shader, logLength, &logLength, log );
        NSLog( @"Shader compile log:\n%s", log );
        free( log );
    }
#endif
    
    glGetShaderiv( *shader, GL_COMPILE_STATUS, &status );
    if( !status )
    {
        glDeleteShader( *shader );
        return NO;
    }
    
    return YES;
}

-( BOOL )linkProgram : ( GLuint )prog
{
    GLint status;
    glLinkProgram( prog );
    
#ifdef DEBUG
    GLint logLength;
    glGetProgramiv( prog, GL_INFO_LOG_LENGTH, &logLength );
    if( logLength > 0 )
    {
        GLchar * log = ( GLchar * )malloc( logLength );
        glGetProgramInfoLog( prog, logLength, &logLength, log );
        NSLog( @"Program link log:\n%s", log );
        free( log );
    }
#endif
    
    glGetProgramiv( prog, GL_LINK_STATUS, &status );
    if( !status )
        return NO;
    
    return YES;
}

-( GLuint )getProgram : ( NSString * )vertex : ( NSString * )fragment
{
    GLuint vs, fs;
    NSString * bundlePath;
    
    GLuint prog = glCreateProgram();
    
    bundlePath = [ [ NSBundle mainBundle ] pathForResource : vertex ofType : @"vsh" ];
    if( ![ self compileShader : &vs type:GL_VERTEX_SHADER file : bundlePath ] )
    {
        NSLog( @"Failed to compile vertex shader" );
        return 0;
    }
    
    bundlePath = [ [ NSBundle mainBundle ] pathForResource : fragment ofType : @"fsh" ];
    if( ![ self compileShader : &fs type : GL_FRAGMENT_SHADER file : bundlePath ] )
    {
        NSLog( @"Failed to compile fragment shader" );
        return 0;
    }
    
    glAttachShader( prog, vs );
    glAttachShader( prog, fs );
    
    glBindAttribLocation( prog, GLKVertexAttribPosition, "position" );
    glBindAttribLocation( prog, GLKVertexAttribTexCoord0, "texCoord0" );
    //glBindAttribLocation( prog, GLKVertexAttribTexCoord1, "texCoord1" );
    
    if( ![ self linkProgram : prog ] )
    {
        NSLog( @"Failed to link program: %d", prog );
        
        glDeleteShader( vs );
        glDeleteShader( fs );
        glDeleteProgram( prog );
        
        return 0;
    }
    
    glDetachShader( prog, vs );
    glDeleteShader( vs );
    
    glDetachShader( prog, fs );
    glDeleteShader( fs );
    
    return prog;
}

-( void )drawRectangle : ( CGRect )rect
{
    float verts[ 8 ];
	float tverts[ 8 ] = { 0.f, 0.f, 1.f, 0.f, 1.f, 1.f, 0.f, 1.f };
    
    glVertexAttribPointer( GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, 0, verts );
    glVertexAttribPointer( GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, 0, tverts );
    
	verts[ 0 ] = rect.origin.x;
	verts[ 1 ] = rect.origin.y;
    
	verts[ 2 ] = rect.origin.x + rect.size.width;
	verts[ 3 ] = rect.origin.y;
    
	verts[ 4 ] = rect.origin.x + rect.size.width;
	verts[ 5 ] = rect.origin.y + rect.size.height;
    
	verts[ 6 ] = rect.origin.x;
	verts[ 7 ] = rect.origin.y + rect.size.height;
    
	glDrawArrays( GL_TRIANGLE_FAN, 0, 4 );
}

-( void )renderInterfaceView : ( UIView * )view toTexture : ( GLuint )texture
{
    view.hidden = NO;
    
    CGFloat scale = [ UIScreen mainScreen ].scale;
    
    const int width = view.frame.size.width * scale;
    const int height = view.frame.size.height * scale;
    
    const int dataSize = width * height * 4;
    uint8_t * data = ( uint8_t * )malloc( dataSize );
    memset( data, 0, dataSize );
    CGColorSpace * colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContext * context = CGBitmapContextCreate( data, width, height, 8, width * 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big );
    CGContextConcatCTM( context, CGAffineTransformMakeScale( scale, scale ) );
    CGColorSpaceRelease( colorSpace );
    
    [ view.layer renderInContext : context ];
    
    [ EAGLContext setCurrentContext : self.context ];
    
    glBindTexture( GL_TEXTURE_2D, texture );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data );
    
    CGContextRelease( context );
    free( data );
    
    view.hidden = YES;
}

-( void )dealloc
{
    [ EAGLContext setCurrentContext : self.context ];
    
    for( GLuint i = 1; i <= 256; i++ )
        glDeleteTextures( 1, &i );
    for( GLuint i = 1; i <= 256; i++ )
        glDeleteProgram( i );
    for( GLuint i = 1; i <= 256; i++ )
        glDeleteFramebuffers( 1, &i );
    for( GLuint i = 1; i <= 256; i++ )
        glDeleteRenderbuffers( 1, &i );
    for( GLuint i = 1; i <= 256; i++ )
        glDeleteBuffers( 1, &i );
    
    [ EAGLContext setCurrentContext : nil ];
    self.context = nil;
    
    [ [ NSNotificationCenter defaultCenter ] removeObserver : self ];
}

@end
