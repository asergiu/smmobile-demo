//
//  RadioButtonGroupHorizontal.m
//  PalAdviser
//
//  Created by Sergey Vishnyov on 24.05.12.
//

#import "RadioButtonGroupHorizontal.h"

#define kHorizontalSeparator 137.0f

@implementation RadioButtonGroupHorizontal

- (void)drawRect:(CGRect)rect
{
    self.userInteractionEnabled = YES;
    
    buttonList = [[NSMutableArray alloc] initWithCapacity:1];
    
    if (super.groupTitle != nil)
    {
        for (int i=0; i < allLabels.count; i++) {
            
            UIButton *radioButtonItem = [UIButton buttonWithType:UIButtonTypeCustom];
            
            radioButtonItem.backgroundColor = [UIColor clearColor]; 
            [radioButtonItem addTarget:self action:@selector(checkboxButton:) forControlEvents:UIControlEventTouchDown];
            radioButtonItem.tag = i;
            radioButtonItem.frame = CGRectMake(i * kHorizontalSeparator, 0.0, 45.f, 45.f);
                        
            UILabel *buttonLabel= [[UILabel alloc] initWithFrame:CGRectMake(i * kHorizontalSeparator + 40.f, 0, 350.f, 45.f)];
            buttonLabel.backgroundColor = [UIColor clearColor];
            buttonLabel.text = [allLabels objectAtIndex:i];
            buttonLabel.font = [UIFont boldSystemFontOfSize:15.0f];
            buttonLabel.textColor = [UIColor whiteColor];

            [radioButtonItem setImage:[UIImage imageNamed:@"radio_btn"] forState:UIControlStateNormal];
            [radioButtonItem setImage:[UIImage imageNamed:@"radio_btn_active"] forState:UIControlStateSelected];
            [radioButtonItem setImage:[UIImage imageNamed:@"radio_btn_disable"] forState:UIControlStateDisabled];
            
            [buttonList addObject:radioButtonItem];
            [self addSubview:buttonLabel];
            [self addSubview:radioButtonItem];
        }
    }
    [super setSelectedItemIndex:-1];
}

- (void)disableItemAtIndex:(NSInteger)index
{
    [super disableItemAtIndex:index];
    [super setSelectedItemIndex:-1];
}

- (void)checkboxButton:(UIButton *)button
{    
    [super checkboxButton:button];
}

@end
