#import "SMMoviePLayerHelper.h"

@interface SMMoviePLayerHelper() 

@property (nonatomic, strong) NSURL *itemURL;
@property (nonatomic, strong) NSNumber *seekStartPoint;
@property (nonatomic, strong) UIView *playHolder;
@property (nonatomic, assign) BOOL isVisionTypeController;
@end

@implementation SMMoviePLayerHelper

@synthesize mpMovieController;
@synthesize moviePlayer;
@synthesize itemURL;
@synthesize seekStartPoint;
@synthesize playHolder;
@synthesize isVisionTypeController;

- (instancetype)init
{
    if (self = [super init]) {
        self.mpMovieController = [MPMoviePlayerViewController alloc];
    }
    
    return self;
}

- (void)visionTypemoviePlayBackDidFinish:(NSNotification *)notification
{
    NSNumber *reason = [[notification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey]; 
	
    switch ([reason integerValue])
	{
            /* The end of the movie was reached. */
		case MPMovieFinishReasonPlaybackEnded:
            NSLog(@"time seeked to %f", self.seekStartPoint.floatValue);
            self.moviePlayer.initialPlaybackTime = seekStartPoint.floatValue - 0.5;
            self.moviePlayer.currentPlaybackTime = seekStartPoint.floatValue;
            [self.moviePlayer play];
			break;
  		default:
			break;
	}
}

-(void)installMovieNotificationObservers
{
    MPMoviePlayerController *player = [self moviePlayer];
    
	[[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(visionTypemoviePlayBackDidFinish:) 
                                                 name:MPMoviePlayerPlaybackDidFinishNotification 
                                               object:player];
}

- (void)setParamsForPLayer:(NSDictionary *)params
{
    self.playHolder = [params objectForKey:@"view"];
    self.seekStartPoint = [params objectForKey:@"seekPoint"];
    self.itemURL = [params objectForKey:@"firstUrl"];
    
    NSString *controller = [params objectForKey:@"controller"];
    if ([controller isEqualToString:@"VisionType"]) {
        self.isVisionTypeController = YES;
        [self installMovieNotificationObservers];
    } else {
        self.isVisionTypeController = NO;
    }
}

- (void)setUpMovieControllerWith:(NSDictionary *)params
{
    self.mpMovieController = [[MPMoviePlayerViewController alloc] init];
    self.moviePlayer = [self.mpMovieController moviePlayer];
    [self setParamsForPLayer:params];
    
    self.mpMovieController.view.frame = CGRectMake(0, 0, self.playHolder.frame.size.width , 
                                                   self.playHolder.frame.size.height);
    [self.mpMovieController shouldAutorotate];

    if (self.mpMovieController.view.superview != self.playHolder) {
        [self.playHolder addSubview:self.mpMovieController.view];    
    }
    
    [self playVideo];
}

- (void)playVideo
{
    [self.moviePlayer setControlStyle:MPMovieControlStyleDefault];
    if (isVisionTypeController) {
        [self.moviePlayer setControlStyle:MPMovieControlStyleNone];
    }
    
    [self.moviePlayer setScalingMode:MPMovieScalingModeAspectFit];
    self.moviePlayer.repeatMode = MPMovieRepeatModeNone;
    self.moviePlayer.contentURL = nil;
    self.moviePlayer.contentURL = self.itemURL;
    [self.moviePlayer prepareToPlay];
    [self.moviePlayer play];
}

- (void)endSession
{
    [[NSNotificationCenter defaultCenter] removeObserver:self  
                                                    name:MPMoviePlayerPlaybackDidFinishNotification  
                                                  object:[self moviePlayer]];
    [[self moviePlayer] stop];
    [self.mpMovieController.view removeFromSuperview];
    self.mpMovieController = nil;
}

@end
