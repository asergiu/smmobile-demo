//
//  NSDateFormatter+SMLocale.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 10/6/15.
//  Copyright © 2015 ACEP. All rights reserved.
//

#import "NSDateFormatter+SMLocale.h"

@implementation NSDateFormatter (SMLocale)

- (id)initWithSafeLocale
{
    static NSLocale *en_US_POSIX = nil;
    self = [self init];
    if (en_US_POSIX == nil) {
        en_US_POSIX = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    }
    [self setLocale:en_US_POSIX];
    
    return self;
}

@end
