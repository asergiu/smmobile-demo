//
//  NSString+CWAddition.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 2/20/15.
//

#import <Foundation/Foundation.h>

@interface NSString (CWAddition)

- (NSString *)stringBetweenString:(NSString *)start andString:(NSString *)end;

@end
