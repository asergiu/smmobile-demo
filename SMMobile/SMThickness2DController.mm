//
//  SMThickness2DController.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 9/10/14.
//

#import "SMThickness2DController.h"
#import "UIViewController+ThumbnailsAnimation.h"
#import "SMInfoViewController.h"
#import "SMAppManager.h"
#import "ThiknessGraficView.h"


#import "SMThickness3DView.h"
#import "SMAsphericView.h"
#import "SMLensView.h"

#include "PDMResults.h"

#import "PDDeviceInfo.h"
#import "PDMDataManager.h"

#import "SMContourGetter.h"

//#define kUseDynamicFrames [[NSUserDefaults standardUserDefaults] boolForKey:@"use_dynamic"]
#define kUseDynamicFrames YES

typedef NS_ENUM (NSInteger, SVMode) {
    SVMode2D,
    SVMode3D,
    SVModeAsph,
    SVModeReal
};

extern CPDMResults Results;
extern int globalLensShape;

@interface SMThickness2DController () <SMInfoViewControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
{
    IBOutlet UILabel *moduleNameLabel;
    IBOutlet UIImageView *mainBackground;
    
    IBOutlet UILabel *leftLabel;
    IBOutlet UILabel *rightLabel;
    
    SMInfoViewController *infoContoller;
    UIView *backgroundView;
    
    IBOutlet UIView *graphicContainer;
    IBOutlet UIView *onRealPersonContainer;
    
    ThiknessGraficView *viewGraphic;
    
    float power;
    float cyl;
    long axis;
    bool leftPrecalibrated;
    bool rightPrecalibrated;
    
    ThicknessCurveData *thicknessCurveDataLeftR1;
    ThicknessCurveData *thicknessCurveDataRightR1;
    ThicknessCurveData *thicknessCurveDataLeftR2;
    ThicknessCurveData *thicknessCurveDataRightR2;
    
    ThicknessLensDesignType *leftLensDesign;
    ThicknessLensDesignType *rightLensDesign;
    
    SMContourGetter contourGetter;
    
    IBOutlet UILabel *leftThicknessLabel;
    IBOutlet UILabel *rightThicknessLabel;
    
    IBOutlet UILabel *leftThickValueLabel;
    IBOutlet UILabel *rightThickValueLabel;
    IBOutlet UILabel *diffThickValueLabel;
    IBOutlet UILabel *weightThickValueLabel;
    
    NSArray *indicesSpherical;
    NSArray *indicesAspheric;
    NSArray *indicesDAspheric;
    
    NSArray *designs;
    
    IBOutlet UILabel *powerLabel;
    IBOutlet UILabel *cylLabel;
    IBOutlet UILabel *axisLabel;
    
    IBOutlet UIPickerView *powerPickerView;
    IBOutlet UIPickerView *cylPickerView;
    IBOutlet UIPickerView *axisPickerView;
    
    NSMutableArray *arrayPowers;
    NSMutableArray *arrayCyl;
    NSMutableArray *arrayAxis;
    NSMutableArray *arrayDiameters;
    
    IBOutlet UIPickerView *leftIndexesView;
    IBOutlet UIPickerView *rightIndexesView;
    IBOutlet UIPickerView *leftDesignsView;
    IBOutlet UIPickerView *rightDesignsView;
    
    IBOutlet UIPickerView *diameterViewLeft;
    IBOutlet UIPickerView *diameterViewRight;
    
    IBOutlet SMThickness3DView * thickness3DView;
    //IBOutlet SMThickness3DView * thickness3DViewSecond;
    
    IBOutlet SMAsphericView * asphericView;
    
    IBOutlet SMLensView * leftLensView;
    IBOutlet SMLensView * rightLensView;
    
    IBOutlet UIView * container3D;
    IBOutlet UIView * containerAspheric;
    
    IBOutlet UIButton *singleViewButton;
    IBOutlet UIButton *doubleViewButton;
    
    IBOutlet UIButton *femaleButton;
    IBOutlet UIButton *maleButton;
    
    IBOutlet UIView * sphericCaptionView;
    IBOutlet UIView * asphericCaptionView;
    
    IBOutlet UILabel * designLeftLensView;
    IBOutlet UILabel * designRightLensView;
    
    IBOutlet UIView *thumbsContainer;
    
    BOOL thumbsExpanded;
    
    IBOutlet UIView *rightDesignView;
    IBOutlet UIView *rightIndexView;
    
    IBOutlet UIView *leftDesignView;
    IBOutlet UIView *leftIndexView;
    
    IBOutlet UIImageView *rightPickerBg;
    IBOutlet UIImageView *leftPickerBg;
    
    IBOutlet UILabel *rightDesignLabel;
    IBOutlet UILabel *rightIndexLabel;
    IBOutlet UILabel *rightDiameterLabel;
    
    IBOutlet UILabel *leftDesignLabel;
    IBOutlet UILabel *leftIndexLabel;
    IBOutlet UILabel *leftDiameterLabel;
    
    IBOutlet UIImageView *rightTitleBG;
    IBOutlet UIImageView *leftTitleBG;
    
    IBOutlet UISwitch * dynamicGlassesSwitch;
    IBOutlet UISwitch * oneToOneSwitch;
    IBOutlet UILabel * dynamicGlassesLabel;
    IBOutlet UILabel * oneToOneLabel;
    
    IBOutlet UISwitch * lensesOnlySwitch;
    IBOutlet UILabel * lensesOnlyLabel;
    
    IBOutlet UISwitch * sideBySideSwitch;
    IBOutlet UILabel * sideBySideLabel;
    
    IBOutlet UISwitch * videoModeSwitch;
    IBOutlet UILabel * videoModeLabel;
    
    IBOutlet UIButton *singleButton;
    IBOutlet UIButton *doubleButton;
    
    SVMode currentMode;
    
    IBOutlet UIImageView *photoImageView;
}

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *modeButtons;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *pickerContainers;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *optionsContainers;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *pickerLabels;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *sceneButtons;

@property ( strong, nonatomic ) IBOutletCollection( UIButton ) NSArray * shapeButtons;

@property ( strong, nonatomic ) IBOutletCollection( UIView ) NSArray * hideOniPad2;

- (IBAction)homePressed:(id)sender;
- (IBAction)showInfo:(id)sender;
- (IBAction)changeMode:(UIButton *)sender;

- (IBAction)setSingleView:(id)sender;
- (IBAction)setDoubleView:(id)sender;
- (IBAction)showPerson:(UISwitch*)sender;

- (IBAction)changePerson:(UIButton*)sender;

- (IBAction)showAspheric:(UISwitch*)sender;

-( IBAction )changeVideoMode : ( UISwitch * )sender;

-( IBAction )chooseShape : ( UIButton * )sender;

-(IBAction)setTestValue0:(UISlider*)sender;
-(IBAction)setTestValue1:(UISlider*)sender;

-( IBAction )setDynamicGlasses : ( UISwitch * )sender;

- (void)setButtonSelected:(UIButton *)button;

@end

@implementation SMThickness2DController

@synthesize modeButtons;
@synthesize pickerContainers;
@synthesize optionsContainers;
@synthesize pickerLabels;
@synthesize sceneButtons;

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    contourGetter.GetContour( false, 5, 3 );
    contourGetter.GetContour( true, 4, 2 );
    
    [self setDynamicFramesState];
    
    if( [ PDDeviceInfo new ].deviceDetails.model == PDDeviceModeliPad2 )
    {
        for( UIView * v in self.hideOniPad2 )
            v.hidden = YES;
    }
    
    power = -3.f;
    cyl = 0.f;
    axis = 0;
    leftPrecalibrated = false;
    rightPrecalibrated = false;
    
    [ thickness3DView setPower : power cyl : cyl axis : axis ];
    [ asphericView setPower : power ];
    [ asphericView setCyl : cyl ];
    [ asphericView setAxis : axis ];
    [ asphericView setBackground : 1 ];
    [ asphericView setFPS : 60.f ];
    
    //[ leftLensView addBackground : @"asph_dots" ];
    //[ leftLensView addBackground : @"asph_columns" ];
    [ leftLensView setBackground : 1 ];
    
    [ leftLensView setPower : power ];
    [ leftLensView setCyl : cyl ];
    [ leftLensView setAxis : axis ];
    
    //[ rightLensView addBackground : @"asph_dots" ];
    //[ rightLensView addBackground : @"asph_columns" ];
    [ rightLensView setBackground : 1 ];
    
    [ rightLensView setPower : power ];
    [ rightLensView setCyl : cyl ];
    [ rightLensView setAxis : axis ];
    
    [mainBackground addParalaxEffect];
    
    indicesSpherical = @[@1.50, @1.53, @1.56, @1.59, @1.60, @1.67, @1.74];
    indicesAspheric = @[@1.50, @1.53, @1.56, @1.59, @1.60, @1.67, @1.74];
    indicesDAspheric = @[@1.50, @1.53, @1.56, @1.59, @1.60, @1.67, @1.74];
    
    designs = @[NSLocalizedString(@"Spherical", @""), NSLocalizedString(@"Aspheric", @""), NSLocalizedString(@"Double Aspheric", @"")];
    
    leftLensDesign = ThicknessLensDesignSpherical15;
    rightLensDesign = ThicknessLensDesignSpherical15;
    
    arrayPowers = [NSMutableArray new];
    arrayCyl  = [NSMutableArray new];
    arrayAxis = [NSMutableArray new];
    
    for (float value = 8.f; value >= -8.f; value -= 0.5f)
        [arrayPowers addObject:[NSNumber numberWithFloat:value]];

    for (float value = 6.f; value >= -6.f; value -= 0.5f)
        [arrayCyl addObject:[NSNumber numberWithFloat:value]];

    for (float value = 0.f; value <= 180.f; value += 10.f)
        [arrayAxis addObject:[NSNumber numberWithFloat:value]];
    
    arrayDiameters = [ [ NSMutableArray alloc ] initWithObjects : NSLocalizedString(@"Regular", @""),  NSLocalizedString(@"Pre-calibrated", @""), nil ];
    
    [self setupPickers];
    
    [self updateGraphView];
    
    for( UIButton * button in self.shapeButtons )
        button.selected = button.tag == globalLensShape;
    
    sphericCaptionView.layer.cornerRadius = 4.f;
    asphericCaptionView.layer.cornerRadius = 4.f;
    
    designLeftLensView.layer.cornerRadius = 4.f;
    designRightLensView.layer.cornerRadius = 4.f;
    
    designLeftLensView.hidden = YES;
    designRightLensView.hidden = YES;
    
    [ leftLensView renderDesignView ];
    [ rightLensView renderDesignView ];
    
    [self setButtonSelected:sceneButtons[0]];
    
    currentMode = SVMode2D;
    
    [asphericView setShape:globalLensShape];
    [leftLensView setShape:globalLensShape];
    [rightLensView setShape:globalLensShape];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setDynamicFramesState)
                                                 name:NSUserDefaultsDidChangeNotification
                                               object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self customizePickers];
    
    [self moduleEntered];
}

-( void )viewWillDisappear : ( BOOL )animated
{
    [ super viewWillDisappear : animated ];
    
    [ thickness3DView stopWithDeallocCam:NO ];
    [ asphericView stopWithDeallocCam : NO ];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NSUserDefaultsDidChangeNotification
                                                  object:nil];
}

- (void)setDynamicFramesState
{
    BOOL bHideDynamicFrames = (kUseDynamicFrames)? NO : YES;
    oneToOneSwitch.hidden = bHideDynamicFrames;
    dynamicGlassesSwitch.hidden = YES;//bHideDynamicFrames;
    oneToOneLabel.hidden = bHideDynamicFrames;
    dynamicGlassesLabel.hidden = YES;//bHideDynamicFrames;

    oneToOneSwitch.enabled = ([PDM_DATA isAvaliableFrameParams] && kUseDynamicFrames);
    dynamicGlassesSwitch.enabled = kUseDynamicFrames;
}

- (void)setupPickers
{
    powerPickerView.frame = CGRectMake(-20.f, -20.f, 92.f, 186.f);
    cylPickerView.frame = CGRectMake(-20.f, -20.f, 92.f, 186.f);
    axisPickerView.frame = CGRectMake(-15.f, -20.f, 90.f, 186.f);
    
    leftIndexesView.frame = CGRectMake(-33.f, -10.f, 125.f, 162.f);
    rightIndexesView.frame = CGRectMake(-33.f, -10.f, 125.f, 162.f);
    leftDesignsView.frame = CGRectMake(-30.f, -10.f, 210.f, 162.f);
    rightDesignsView.frame = CGRectMake(-30.f, -10.f, 210.f, 162.f);
    
    diameterViewLeft.frame = CGRectMake(diameterViewLeft.frame.origin.x - 30.f,
                                    diameterViewLeft.frame.origin.y + 56.f,
                                    165.f,
                                    162.f);
    diameterViewRight.frame = CGRectMake(diameterViewRight.frame.origin.x - 5.f,
                                        diameterViewRight.frame.origin.y + 56.f,
                                        165.f,
                                        162.f);
    
    powerPickerView.transform = CGAffineTransformMakeScale(0.7f, 0.7f);
    cylPickerView.transform = CGAffineTransformMakeScale(0.7f, 0.7f);
    axisPickerView.transform = CGAffineTransformMakeScale(0.7f, 0.7f);
    
    leftIndexesView.transform = CGAffineTransformMakeScale(0.65f, 0.65f);
    rightIndexesView.transform = CGAffineTransformMakeScale(0.65f, 0.65f);
    leftDesignsView.transform = CGAffineTransformMakeScale(0.65f, 0.65f);
    rightDesignsView.transform = CGAffineTransformMakeScale(0.65f, 0.65f);
    diameterViewLeft.transform = CGAffineTransformMakeScale(0.65f, 0.65f);
    diameterViewRight.transform = CGAffineTransformMakeScale(0.65f, 0.65f);
    
    [powerPickerView selectRow:22 inComponent:0 animated:NO];
    [cylPickerView selectRow:12 inComponent:0 animated:NO];
    [axisPickerView selectRow:0 inComponent:0 animated:NO];
    [diameterViewLeft selectRow:0 inComponent:0 animated:NO];
    [diameterViewRight selectRow:0 inComponent:0 animated:NO];
    
    rightDiameterLabel.hidden = YES;
    rightPickerBg.hidden = YES;
    diameterViewRight.hidden = YES;
    
    leftDiameterLabel.hidden = YES;
    leftPickerBg.hidden = YES;
    diameterViewLeft.hidden = YES;
}

- (void)customizePickers
{
    // UIPickerView's lifehack )
    [[powerPickerView.subviews objectAtIndex:1] setHidden:YES];
    [[powerPickerView.subviews objectAtIndex:2] setHidden:YES];
    [[cylPickerView.subviews objectAtIndex:1] setHidden:YES];
    [[cylPickerView.subviews objectAtIndex:2] setHidden:YES];
    [[axisPickerView.subviews objectAtIndex:1] setHidden:YES];
    [[axisPickerView.subviews objectAtIndex:2] setHidden:YES];
    [[leftIndexesView.subviews objectAtIndex:1] setHidden:YES];
    [[leftIndexesView.subviews objectAtIndex:2] setHidden:YES];
    [[rightIndexesView.subviews objectAtIndex:1] setHidden:YES];
    [[rightIndexesView.subviews objectAtIndex:2] setHidden:YES];
    [[leftDesignsView.subviews objectAtIndex:1] setHidden:YES];
    [[leftDesignsView.subviews objectAtIndex:2] setHidden:YES];
    [[rightDesignsView.subviews objectAtIndex:1] setHidden:YES];
    [[rightDesignsView.subviews objectAtIndex:2] setHidden:YES];
    [[diameterViewLeft.subviews objectAtIndex:1] setHidden:YES];
    [[diameterViewLeft.subviews objectAtIndex:2] setHidden:YES];
    [[diameterViewRight.subviews objectAtIndex:1] setHidden:YES];
    [[diameterViewRight.subviews objectAtIndex:2] setHidden:YES];
}

-( void )refreshDesigns
{
    int leftDesign = ( int )[ leftDesignsView selectedRowInComponent : 0 ];
    NSArray * leftIndices;
    
    if( leftDesign == 2 )
        leftIndices = indicesDAspheric;
    else if( leftDesign == 1 )
        leftIndices = indicesAspheric;
    else
        leftIndices = indicesSpherical;
    
    float leftIndex = [ leftIndices[ [ leftIndexesView selectedRowInComponent : 0 ] ] floatValue ];
    
    int rightDesign = ( int )[ rightDesignsView selectedRowInComponent : 0 ];
    NSArray * rightIndices;
    
    if( rightDesign == 2 )
        rightIndices = indicesDAspheric;
    else if( rightDesign == 1 )
        rightIndices = indicesAspheric;
    else
        rightIndices = indicesSpherical;
    
    float rightIndex = [ rightIndices[ [ rightIndexesView selectedRowInComponent : 0 ] ] floatValue ];
    
#define EQ( x, y ) ( fabs( ( x ) - ( y ) ) < 0.005f )
    
    if( leftDesign == 2 )
    {
        if( EQ( leftIndex, 1.5f ) )
            leftLensDesign = ThicknessLensDesignDAspheric15;
        else if( EQ( leftIndex, 1.53f ) )
            leftLensDesign = ThicknessLensDesignDAspheric153;
        else if( EQ( leftIndex, 1.56f ) )
            leftLensDesign = ThicknessLensDesignDAspheric156;
        else if( EQ( leftIndex, 1.59f ) )
            leftLensDesign = ThicknessLensDesignDAspheric159;
        else if( EQ( leftIndex, 1.60f ) )
            leftLensDesign = ThicknessLensDesignDAspheric16;
        else if( EQ( leftIndex, 1.67f ) )
            leftLensDesign = ThicknessLensDesignDAspheric167;
        else if( EQ( leftIndex, 1.74f ) )
            leftLensDesign = ThicknessLensDesignDAspheric174;
    }
    else if( leftDesign == 1 )
    {
        if( EQ( leftIndex, 1.5f ) )
            leftLensDesign = ThicknessLensDesignAspheric15;
        else if( EQ( leftIndex, 1.53f ) )
            leftLensDesign = ThicknessLensDesignAspheric153;
        else if( EQ( leftIndex, 1.56f ) )
            leftLensDesign = ThicknessLensDesignAspheric156;
        else if( EQ( leftIndex, 1.59f ) )
            leftLensDesign = ThicknessLensDesignAspheric159;
        else if( EQ( leftIndex, 1.60f ) )
            leftLensDesign = ThicknessLensDesignAspheric16;
        else if( EQ( leftIndex, 1.67f ) )
            leftLensDesign = ThicknessLensDesignAspheric167;
        else if( EQ( leftIndex, 1.74f ) )
            leftLensDesign = ThicknessLensDesignAspheric174;
    }
    else
    {
        if( EQ( leftIndex, 1.5f ) )
            leftLensDesign = ThicknessLensDesignSpherical15;
        else if( EQ( leftIndex, 1.53f ) )
            leftLensDesign = ThicknessLensDesignSpherical153;
        else if( EQ( leftIndex, 1.56f ) )
            leftLensDesign = ThicknessLensDesignSpherical156;
        else if( EQ( leftIndex, 1.59f ) )
            leftLensDesign = ThicknessLensDesignSpherical159;
        else if( EQ( leftIndex, 1.60f ) )
            leftLensDesign = ThicknessLensDesignSpherical16;
        else if( EQ( leftIndex, 1.67f ) )
            leftLensDesign = ThicknessLensDesignSpherical167;
        else if( EQ( leftIndex, 1.74f ) )
            leftLensDesign = ThicknessLensDesignSpherical174;
    }
    
    if( rightDesign == 2 )
    {
        if( EQ( rightIndex, 1.5f ) )
            rightLensDesign = ThicknessLensDesignDAspheric15;
        else if( EQ( rightIndex, 1.53f ) )
            rightLensDesign = ThicknessLensDesignDAspheric153;
        else if( EQ( rightIndex, 1.56f ) )
            rightLensDesign = ThicknessLensDesignDAspheric156;
        else if( EQ( rightIndex, 1.59f ) )
            rightLensDesign = ThicknessLensDesignDAspheric159;
        else if( EQ( rightIndex, 1.60f ) )
            rightLensDesign = ThicknessLensDesignDAspheric16;
        else if( EQ( rightIndex, 1.67f ) )
            rightLensDesign = ThicknessLensDesignDAspheric167;
        else if( EQ( rightIndex, 1.74f ) )
            rightLensDesign = ThicknessLensDesignDAspheric174;
    }
    else if( rightDesign == 1 )
    {
        if( EQ( rightIndex, 1.5f ) )
            rightLensDesign = ThicknessLensDesignAspheric15;
        else if( EQ( rightIndex, 1.53f ) )
            rightLensDesign = ThicknessLensDesignAspheric153;
        else if( EQ( rightIndex, 1.56f ) )
            rightLensDesign = ThicknessLensDesignAspheric156;
        else if( EQ( rightIndex, 1.59f ) )
            rightLensDesign = ThicknessLensDesignAspheric159;
        else if( EQ( rightIndex, 1.60f ) )
            rightLensDesign = ThicknessLensDesignAspheric16;
        else if( EQ( rightIndex, 1.67f ) )
            rightLensDesign = ThicknessLensDesignAspheric167;
        else if( EQ( rightIndex, 1.74f ) )
            rightLensDesign = ThicknessLensDesignAspheric174;
    }
    else
    {
        if( EQ( rightIndex, 1.5f ) )
            rightLensDesign = ThicknessLensDesignSpherical15;
        else if( EQ( rightIndex, 1.53f ) )
            rightLensDesign = ThicknessLensDesignSpherical153;
        else if( EQ( rightIndex, 1.56f ) )
            rightLensDesign = ThicknessLensDesignSpherical156;
        else if( EQ( rightIndex, 1.59f ) )
            rightLensDesign = ThicknessLensDesignSpherical159;
        else if( EQ( rightIndex, 1.60f ) )
            rightLensDesign = ThicknessLensDesignSpherical16;
        else if( EQ( rightIndex, 1.67f ) )
            rightLensDesign = ThicknessLensDesignSpherical167;
        else if( EQ( rightIndex, 1.74f ) )
            rightLensDesign = ThicknessLensDesignSpherical174;
    }
}

- (void)hiddenDiameterView
{
    BOOL hideDiameters = ( power + cyl ) <= 0.f;
    
    if (!hideDiameters && currentMode == SVMode2D)
    {
        
        for (UIView *container in optionsContainers)
        {
            if (container.tag == 0)
                [container setFrame:CGRectMake(122.0, 490.0, 250.0 + 130, 168.0)];
            
            if (container.tag == 1)
                [container setFrame:CGRectMake(652.0 - 130, 490.0, 250.0 + 130.0, 168.0)];
        }
        
        rightDiameterLabel.hidden = NO;
        rightPickerBg.hidden = NO;
        diameterViewRight.hidden = NO;
        
        rightDesignView.frame = CGRectMake(135.f, 28.f, rightDesignView.frame.size.width, rightDesignView.frame.size.height);
        rightIndexView.frame = CGRectMake(298.f, 28.f, rightIndexView.frame.size.width, rightIndexView.frame.size.height);
        
        rightDesignLabel.frame = CGRectMake(182.f, 39.f, rightDesignLabel.frame.size.width, rightDesignLabel.frame.size.height);
        rightIndexLabel.frame = CGRectMake(295.f, 39.f, rightIndexLabel.frame.size.width, rightIndexLabel.frame.size.height);
        
        rightLabel.frame = CGRectMake(130.0 + 8.0, 4.0, rightLabel.frame.size.width, rightLabel.frame.size.height);
        
        rightTitleBG.frame = CGRectMake(0.0, 1.0, 250.0 + 130, 30.0);
        
        rightDiameterLabel.frame = CGRectMake(35.f, 39.f, rightDiameterLabel.frame.size.width, rightDiameterLabel.frame.size.height);
        rightPickerBg.frame = CGRectMake(15.f, 84.f, rightPickerBg.frame.size.width, rightPickerBg.frame.size.height);
        diameterViewRight.frame = CGRectMake(13.f, 46.f, diameterViewRight.frame.size.width, diameterViewRight.frame.size.height);
        
        leftDiameterLabel.hidden = NO;
        leftPickerBg.hidden = NO;
        diameterViewLeft.hidden = NO;
        
        leftDesignView.frame = CGRectMake(91.f, 28.f, leftDesignView.frame.size.width, leftDesignView.frame.size.height);
        leftIndexView.frame = CGRectMake(15.f, 28.f, leftIndexView.frame.size.width, leftIndexView.frame.size.height);
        
        leftDesignLabel.frame = CGRectMake(136.f, 39.f, leftDesignLabel.frame.size.width, leftDesignLabel.frame.size.height);
        leftIndexLabel.frame = CGRectMake(15.f, 39.f, leftIndexLabel.frame.size.width, leftIndexLabel.frame.size.height);
        
        leftTitleBG.frame = CGRectMake(0.0, 1.0, 250.0 + 130, 30.0);
        
        leftDiameterLabel.frame = CGRectMake(280.f, 39.f, leftDiameterLabel.frame.size.width, leftDiameterLabel.frame.size.height);
        leftPickerBg.frame = CGRectMake(260.f, 84.f, leftPickerBg.frame.size.width, leftPickerBg.frame.size.height);
        diameterViewLeft.frame = CGRectMake(258.f, 46.f, diameterViewLeft.frame.size.width, diameterViewLeft.frame.size.height);
    }
    else
    {
        for (UIView *container in optionsContainers)
        {
            if (container.tag == 0)
                [container setFrame:CGRectMake(container.frame.origin.x, container.frame.origin.y, 250.0, 200.0)];
            
            if (container.tag == 1)
                [container setFrame:CGRectMake(652.f, container.frame.origin.y, 250.0, 200.0)];
        }
        
        rightDiameterLabel.hidden = YES;
        rightPickerBg.hidden = YES;
        diameterViewRight.hidden = YES;
        
        rightDesignView.frame = CGRectMake(5.f, 28.f, rightDesignView.frame.size.width, rightDesignView.frame.size.height);
        rightIndexView.frame = CGRectMake(168.f, 28.f, rightIndexView.frame.size.width, rightIndexView.frame.size.height);
        
        rightDesignLabel.frame = CGRectMake(52.f, 39.f, rightDesignLabel.frame.size.width, rightDesignLabel.frame.size.height);
        rightIndexLabel.frame = CGRectMake(165.f, 39.f, rightIndexLabel.frame.size.width, rightIndexLabel.frame.size.height);
        
        rightLabel.frame = CGRectMake(8.0, 4.0, rightLabel.frame.size.width, rightLabel.frame.size.height);
        
        rightTitleBG.frame = CGRectMake(0.0, 1.0, 250.0, 30.0);
        
        leftDiameterLabel.hidden = YES;
        leftPickerBg.hidden = YES;
        diameterViewLeft.hidden = YES;
        
        leftDesignView.frame = CGRectMake(91.f, 28.f, leftDesignView.frame.size.width, leftDesignView.frame.size.height);
        leftIndexView.frame = CGRectMake(15.f, 28.f, leftIndexView.frame.size.width, leftIndexView.frame.size.height);
        
        leftDesignLabel.frame = CGRectMake(136.f, 39.f, leftDesignLabel.frame.size.width, leftDesignLabel.frame.size.height);
        leftIndexLabel.frame = CGRectMake(15.f, 39.f, leftIndexLabel.frame.size.width, leftIndexLabel.frame.size.height);
        
        leftTitleBG.frame = CGRectMake(0.0, 1.0, 250.0, 30.0);
    }
}

-( void )refreshData
{
    power = [ arrayPowers[ [ powerPickerView selectedRowInComponent : 0 ] ] floatValue ];
    cyl   = [ arrayCyl[ [ cylPickerView selectedRowInComponent : 0 ] ] floatValue ];
    axis  = [ arrayAxis[ [ axisPickerView selectedRowInComponent : 0 ] ] longValue ];
    
    [self hiddenDiameterView];
    
    if( power + cyl <= 0.f )
    {
        [ diameterViewLeft selectRow : 0 inComponent : 0 animated : NO ];
        [ diameterViewRight selectRow : 0 inComponent : 0 animated : NO ];
    }
    
    leftPrecalibrated  = [ diameterViewLeft selectedRowInComponent : 0 ] == 1;
    rightPrecalibrated  = [ diameterViewRight selectedRowInComponent : 0 ] == 1;

    NSLog( @"power %f cyl %f axis %ld", power, cyl, axis );
    NSLog( @"leftPrecalibrated %d, rightPrecalibrated %d", leftPrecalibrated, rightPrecalibrated );
    
    [ self refreshDesigns ];
    
    //[ thickness3DView setLeftDesign : leftLensDesign ];
    //[ thickness3DView setRightDesign : rightLensDesign ];
    [ thickness3DView setLeftDesign : rightLensDesign ];
    [ thickness3DView setRightDesign : leftLensDesign ];

    [ thickness3DView setPower : power cyl : cyl axis : axis ];
    
    [ asphericView setPower : power ];
    [ asphericView setCyl : cyl ];
    [ asphericView setAxis : axis ];
    
    [ leftLensView setPower : power ];
    [ leftLensView setCyl : cyl ];
    [ leftLensView setAxis : axis ];
    
    [ rightLensView setPower : power ];
    [ rightLensView setCyl : cyl ];
    [ rightLensView setAxis : axis ];
}

- (void)setButtonSelected:(UIButton *)button
{
    for (UIButton *button in sceneButtons)
        button.layer.borderColor = [UIColor clearColor].CGColor;
    
    [thumbsContainer bringSubviewToFront:button];
    
    button.layer.borderColor = [UIColor whiteColor].CGColor;
    button.layer.borderWidth = 2.f;
}

#pragma mark Actions

- (void)homePressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [APP_MANAGER backToPDModule];
}

- (void)changeMode:(UIButton *)sender
{
    if (sender.selected)
    {
        return;
    }
    
    if (sender.tag == 0 || sender.tag == 2)
    {
        [self setDefaultFrameLeftLensOption];
    }
    
    for (UIButton *button in modeButtons)
        button.selected = button == sender;
    
    for (UIView *container in optionsContainers)
    {
        CGRect origin = container.frame;
        
        if (sender.tag == 0 || origin.origin.y < 590.f)
        {
            origin.origin.y += (sender.tag) ? 100.f : -100.f;
            container.frame = origin;
        }
    }
    
    if( sender.tag == 0 )
    {
        graphicContainer.hidden = NO;
        mainBackground.hidden = NO;
    }
    else
    {
        graphicContainer.hidden = YES;
        mainBackground.hidden = YES;
    }
    
    if( sender.tag == 1 )
    {
        container3D.hidden = NO;
        [ thickness3DView loadThings ];

        [ thickness3DView setNeedsDisplay ];
        //[ thickness3DViewSecond setNeedsDisplay ];
        
        if (singleViewButton.selected == NO)
            [self setOtherFrameLeftLensOption];
        else
            [self setDefaultFrameLeftLensOption];
    }
    else
    {
        container3D.hidden = YES;
        
        [ thickness3DView stopWithDeallocCam:NO ];
        
        if (@available(iOS 12.0, *)) {
            
        } else {
            [ thickness3DView clearThings ];
        }
    }
    
    if( sender.tag == 2 )
    {
        containerAspheric.hidden = NO;
        [ asphericView loadThings ];
        [ asphericView renderViews ];
        [ asphericView play ];
    }
    else
    {
        containerAspheric.hidden = YES;
        [ asphericView clearThings ];
        [ asphericView stopWithDeallocCam:NO ];
    }
    
    onRealPersonContainer.hidden = sender.tag == 3 ? NO : YES;
    
    currentMode = (SVMode)sender.tag;
    
    /*for (UIView *container in pickerContainers)
    {
        container.hidden = (currentMode == SVModeAsph && container.tag < 2);
    }
    
    cylLabel.hidden = (currentMode == SVModeAsph);
    axisLabel.hidden = (currentMode == SVModeAsph);*/
    
    [self hiddenDiameterView];
}

- (IBAction)setSingleView:(id)sender
{
    singleViewButton.selected = YES;
    doubleViewButton.selected = NO;
    
    /*thickness3DView.frame = CGRectMake( 0, 0, 1024, 768 );
    thickness3DViewSecond.hidden = YES;
    //[ thickness3DViewSecond stop ];
    
    [ self refreshData ];*/
    
    [ thickness3DView setSplitMode : NO ];
    //[ thickness3DView showFrame : YES ];
    
    [ thickness3DView setNeedsDisplay ];
    //[ thickness3DViewSecond setNeedsDisplay ];
    
    [self setDefaultFrameLeftLensOption];
    
    //lensesOnlySwitch.hidden = YES;
    //lensesOnlyLabel.hidden = YES;
}

- (IBAction)setDoubleView:(id)sender
{
    singleViewButton.selected = NO;
    doubleViewButton.selected = YES;
    
    /*thickness3DView.frame = CGRectMake( 0, 0, 1024, 384 );
    thickness3DViewSecond.hidden = NO;
    //[ thickness3DViewSecond play ];
    
    [ self refreshData ];*/
    
    [ thickness3DView setSplitMode : YES ];
    //[ thickness3DView showFrame : !lensesOnlySwitch.on ];
    
    [ thickness3DView setNeedsDisplay ];
    //[ thickness3DViewSecond setNeedsDisplay ];
    
    [self setOtherFrameLeftLensOption];
    
    //lensesOnlySwitch.hidden = NO;
    //lensesOnlyLabel.hidden = NO;
}

- (void)setOtherFrameLeftLensOption
{
    for (UIView *container in optionsContainers)
    {
        if (container.tag == 0)
            [container setFrame:CGRectMake(652.0, 210.0, container.frame.size.width, container.frame.size.height)];
    }
    
    [leftLabel setTextAlignment:NSTextAlignmentCenter];
    [rightLabel setTextAlignment:NSTextAlignmentCenter];
}

- (void)setDefaultFrameLeftLensOption
{
    for (UIView *container in optionsContainers)
    {
        if (container.tag == 0)
            [container setFrame:CGRectMake(125.0, 590.0, container.frame.size.width, container.frame.size.height)];
    }
    
    [leftLabel setTextAlignment:NSTextAlignmentLeft];
    [rightLabel setTextAlignment:NSTextAlignmentRight];
}

- (IBAction)showPerson:(UISwitch*)sender
{
    [ thickness3DView showPerson : !sender.on ];
    //[ thickness3DViewSecond showPerson : sender.on ];
    
    [ thickness3DView setNeedsDisplay ];
    //[ thickness3DViewSecond setNeedsDisplay ];
}

- (IBAction)showFrame:(UISwitch*)sender
{
    [ thickness3DView showFrame : !sender.on ];
    [ thickness3DView setNeedsDisplay ];
}

- (IBAction)changePerson:(UIButton*)sender
{
    femaleButton.selected = femaleButton == sender;
    maleButton.selected = maleButton == sender;
    
    [ thickness3DView setPerson : (int)sender.tag ];
    //[ thickness3DViewSecond setPerson : (int)sender.tag ];
    
    [ thickness3DView setNeedsDisplay ];
    //[ thickness3DViewSecond setNeedsDisplay ];
}

- (IBAction)showAspheric:(UISwitch*)sender
{
    if( sender.on )
    {
        graphicContainer.hidden = YES;
        mainBackground.hidden = YES;
        container3D.hidden = YES;
        
        containerAspheric.hidden = NO;
        
        [ asphericView renderViews ];
        [ asphericView display ];
        [ asphericView play ];
    }
    else
    {
        graphicContainer.hidden = NO;
        mainBackground.hidden = NO;
        container3D.hidden = YES;
        
        containerAspheric.hidden = YES;
        [ asphericView stopWithDeallocCam:NO ];
    }
}

-( IBAction )changeVideoMode : ( UISwitch * )sender
{
    asphericView.videoMode = sender.on;
    thumbsContainer.hidden = sender.on;
    
    sideBySideLabel.hidden = sender.on;
    sideBySideSwitch.hidden = sender.on;
}

-( void )chooseShape : ( UIButton * )sender
{
    if( globalLensShape == sender.tag )
        return;
    
    for( UIButton * button in self.shapeButtons )
        button.selected = button == sender;
    
    globalLensShape = (int)sender.tag;
    
    [ asphericView setShape : globalLensShape ];
    
    [leftLensView setShape:globalLensShape];
    [rightLensView setShape:globalLensShape];
    
    [leftLensView display];
    [rightLensView display];
}

-(IBAction)setTestValue0:(UISlider*)sender
{
    [thickness3DView setTestValue0:sender.value];
    NSLog( @"test0 %f", sender.value );
}

-(IBAction)setTestValue0text:(UITextField*)sender
{
    float step = [ sender.text floatValue ];
    [thickness3DView setTestValue0:step];
    NSLog( @"test0 %f", step );
}

-(IBAction)setTestValue1:(UISlider*)sender
{
    [thickness3DView setTestValue1:sender.value];
    NSLog( @"test1 %f", sender.value );
}

-( IBAction )setDynamicGlasses : ( UISwitch * )sender
{
    /*[ thickness3DView setDBL : 2.f ];
    [ thickness3DView setA : 3.f ];
    [ thickness3DView setB : 4.f ];*/
    
    [ thickness3DView setDynamicGlasses : sender.on ];
    [ thickness3DView showFrame : !sender.on ];
    [ thickness3DView setNeedsDisplay ];
}

-( IBAction )setOneToOne : ( UISwitch * )sender
{
    if( sender.on )
    {
        //dynamicGlassesSwitch.on = YES;
        //dynamicGlassesSwitch.enabled = NO;
        
        [ thickness3DView setDBL : [ PDM_DATA getCurrentFrameBridge ] / 10.f ];
        [ thickness3DView setA : [ PDM_DATA getCurrentFrameWidth ] / 10.f ];
        [ thickness3DView setB : [ PDM_DATA getCurrentFrameHeight ] / 10.f ];
    }
    else
    {
        //dynamicGlassesSwitch.enabled = YES;
    
        [ thickness3DView reset ];
    }
    
    //[ thickness3DView setDynamicGlasses : YES ];
    [ thickness3DView setDynamicGlasses : sender.on ];
    [ thickness3DView showFrame : !sender.on ];
    [ thickness3DView setNeedsDisplay ];
}

- (IBAction)setSideBySide:(UIButton *)sender
{
    singleButton.selected = ![@(sender.tag) boolValue];
    doubleButton.selected = [@(sender.tag) boolValue];
    
    asphericView.hidden = [@(sender.tag) boolValue];
    leftLensView.hidden = ![@(sender.tag) boolValue];
    rightLensView.hidden = ![@(sender.tag) boolValue];
    
    videoModeLabel.hidden = [@(sender.tag) boolValue];
    videoModeSwitch.hidden = [@(sender.tag) boolValue];
    
    if ([@(sender.tag) boolValue])
    {
        [leftLensView display];
        [rightLensView display];
    }
}

- (IBAction)thumbPressed:(UIButton *)sender
{
    if (thumbsExpanded)
    {
        [self setButtonSelected:sender];
        [self hideThumbnails:sceneButtons withPosition:0 step:88.f];
        
        [asphericView setBackground:(int)sender.tag];
        [ leftLensView setBackground : ( int )sender.tag ];
        [ rightLensView setBackground : ( int )sender.tag ];
    }
    else
        [self expandThumbnails:sceneButtons withPosition:0 step:88.f];
    
    thumbsExpanded = !thumbsExpanded;
}

#pragma mark Graphic View

- (void)updateGraphView
{
    // diameter
    int diam = 65;
    if (Results.GetDiameterStatus()) {
        diam = fmax(Results.GetDiameter(false), Results.GetDiameter(true));
    }
    
    [MainDataSource updateDataForDrawWithPower:power andCyl:cyl andLeftLensType:leftLensDesign andRightLensType:rightLensDesign andPrecalibratedLeft:leftPrecalibrated andPrecalibratedRight:rightPrecalibrated andPrecalibratedDiam:diam];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){

        UIImage *Photo = [MainDataSource updatePhotoForDrawWithPower:power andCyl:cyl andAxis:axis
                                                     andLeftLensType:rightLensDesign
                                                    andRightLensType:leftLensDesign];

        dispatch_async(dispatch_get_main_queue(), ^(void){

            photoImageView.image = Photo;
        });
    });
    
    float left1ThicknessCenter = [MainDataSource getThicknessDataForDraw2D:YES forLeftSide:YES forCenterPoint:YES];
    float left1ThicknessEdge = [MainDataSource getThicknessDataForDraw2D:YES forLeftSide:YES forCenterPoint:NO];
    float right1ThicknessCenter = [MainDataSource getThicknessDataForDraw2D:YES forLeftSide:NO forCenterPoint:YES];
    float right1ThicknessEdge = [MainDataSource getThicknessDataForDraw2D:YES forLeftSide:NO forCenterPoint:NO];
    float left2ThicknessCenter = [MainDataSource getThicknessDataForDraw2D:NO forLeftSide:YES forCenterPoint:YES];
    float left2ThicknessEdge = [MainDataSource getThicknessDataForDraw2D:NO forLeftSide:YES forCenterPoint:NO];
    float right2ThicknessCenter = [MainDataSource getThicknessDataForDraw2D:NO forLeftSide:NO forCenterPoint:YES];
    float right2ThicknessEdge = [MainDataSource getThicknessDataForDraw2D:NO forLeftSide:NO forCenterPoint:NO];
    
    double leftLensWeight = [MainDataSource getLensWeightForDraw2D:YES];
    double rightLensWeight = [MainDataSource getLensWeightForDraw2D:NO];

    BOOL bShowCenter = (power >= 0)? YES : NO;
    if (cyl != 0.) {
        bShowCenter = (left2ThicknessCenter >= left2ThicknessEdge)? YES : NO;
    }
    
    NSString *center = NSLocalizedString(@"CENTER THICKNESS (mm)", @"");
    NSString *edge = NSLocalizedString(@"EDGE THICKNESS (mm)", @"");
    
    if (bShowCenter)
    {
        leftThicknessLabel.text = center;
        rightThicknessLabel.text = center;
    }
    else
    {
        leftThicknessLabel.text = edge;
        rightThicknessLabel.text = edge;
    }
    
    float leftThickness;
    float rightThickness;
    
    if (cyl != 0.) {
        leftThickness = (bShowCenter) ? left2ThicknessCenter : left2ThicknessEdge;
        rightThickness = (bShowCenter) ? right2ThicknessCenter : right2ThicknessEdge;
    }
    else {
        leftThickness = (bShowCenter) ? left1ThicknessCenter : left1ThicknessEdge;
        rightThickness = (bShowCenter) ? right1ThicknessCenter : right1ThicknessEdge;
    }
    
    float difference = (rightThickness - leftThickness) * 100.f / leftThickness;
    
    leftThickValueLabel.text = (leftThickness && !isnan(leftThickness) && leftThickness!=INFINITY) ? [NSString stringWithFormat:@"%.1f", leftThickness] : @"";
    rightThickValueLabel.text = (rightThickness && !isnan(rightThickness) && rightThickness!=INFINITY) ? [NSString stringWithFormat:@"%.1f", rightThickness] : @"";
    diffThickValueLabel.text = (difference!=-100 && !isnan(difference) && difference!=INFINITY) ? [NSString stringWithFormat:@"%.1f", difference]: @"";

//    [self updateData];
    
    for (ThiknessGraficView *sub in [graphicContainer subviews])
    {
        if (sub.tag == 777)
            [sub removeFromSuperview];
    }
    
    CGRect graficRect = CGRectMake(68.f, 250.f, 592.f, 259.f);
    
    viewGraphic = [[ThiknessGraficView alloc] initWithFrame:graficRect];
    
    [viewGraphic setBackgroundColor:[UIColor clearColor]];
    
    viewGraphic.tag = 777;
    
    [graphicContainer addSubview:viewGraphic];

    if (leftLensWeight == 0. || leftLensWeight == INFINITY || isnan(leftLensWeight) ||
        rightLensWeight == 0. || rightLensWeight == INFINITY || isnan(rightLensWeight)) {
        weightThickValueLabel.text = @"";
    }
    else {
        float weightDiff = (rightLensWeight - leftLensWeight) * 100.f / leftLensWeight;
        weightThickValueLabel.text = [ NSString stringWithFormat : @"%.1f", weightDiff ];
    }
    
    /*bool leftAspheric = [ leftDesignsView selectedRowInComponent : 0 ];
    bool rightAspheric = [ rightDesignsView selectedRowInComponent : 0 ];*/
    
    int leftDesign = ( int )[ leftDesignsView selectedRowInComponent : 0 ];
    int rightDesign = ( int )[ rightDesignsView selectedRowInComponent : 0 ];
    
    int leftIndexRow = ( int )[ leftIndexesView selectedRowInComponent : 0 ];
    int rightIndexRow = ( int )[ rightIndexesView selectedRowInComponent : 0 ];
    
    NSArray * leftIndices;
    
    if( leftDesign == 2 )
        leftIndices = indicesDAspheric;
    else if( leftDesign == 1 )
        leftIndices = indicesAspheric;
    else
        leftIndices = indicesSpherical;
    
    NSString * leftIndex = [ leftIndices[ leftIndexRow ] stringValue ];
    
    NSArray * rightIndices;
    
    if( rightDesign == 2 )
        rightIndices = indicesDAspheric;
    else if( rightDesign == 1 )
        rightIndices = indicesAspheric;
    else
        rightIndices = indicesSpherical;
    
    NSString * rightIndex = [ rightIndices[ rightIndexRow ] stringValue ];
    
    ( ( UILabel * )sphericCaptionView ).text = [ NSString stringWithFormat : @"%@ %@", designs[ leftDesign ], leftIndex ];
    ( ( UILabel * )asphericCaptionView ).text = [ NSString stringWithFormat : @"%@ %@", designs[ rightDesign ], rightIndex ];
    
    designLeftLensView.text = [ NSString stringWithFormat : @"%@ %@", designs[ leftDesign ], leftIndex ];
    designRightLensView.text = [ NSString stringWithFormat : @"%@ %@", designs[ rightDesign ], rightIndex ];
    
    [ asphericView setLeftDesign : leftDesign ];
    [ asphericView setRightDesign : rightDesign ];
    
    [ asphericView setLeftIndex : [ leftIndex floatValue ] ];
    [ asphericView setRightIndex : [ rightIndex floatValue ] ];
    
    [ asphericView renderViews ];
    
    [ leftLensView renderDesignView ];
    [ rightLensView renderDesignView ];
    
    [ leftLensView setDesign : leftDesign ];
    [ leftLensView setIndex : [ leftIndex floatValue ] ];
    [ leftLensView display ];
    
    [ rightLensView setDesign : rightDesign ];
    [ rightLensView setIndex : [ rightIndex floatValue ] ];
    [ rightLensView display ];
}

- (void)updateData
{
//    [ MainDataSource updateDataForDrawWithPower : power andCyl : cyl andLeftLensType : leftLensDesign andRightLensType : rightLensDesign ];
    /*[MainDataSource refreshDataWithThicknessCurveData1:thicknessCurveDataLeftR1
                                                 data2:thicknessCurveDataLeftR2
                                                 data3:thicknessCurveDataRightR1 data4:thicknessCurveDataRightR2];*/
}

#pragma mark UIPickerViewDelegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    if (thePickerView == diameterViewLeft)
        return arrayDiameters.count;
    if (thePickerView == diameterViewRight)
        return arrayDiameters.count;
    if (thePickerView == powerPickerView)
        return arrayPowers.count;
    if (thePickerView == cylPickerView)
        return arrayCyl.count;
    if (thePickerView == axisPickerView)
        return arrayAxis.count;
    if (thePickerView == leftIndexesView)
    {
        if( [ leftDesignsView selectedRowInComponent : 0 ] == 1 )
            return indicesAspheric.count;
        else
            return indicesSpherical.count;
    }
    if (thePickerView == rightIndexesView)
    {
        if( [ rightDesignsView selectedRowInComponent : 0 ] == 1 )
            return indicesAspheric.count;
        else
            return indicesSpherical.count;
    }
    if (thePickerView == leftDesignsView || thePickerView == rightDesignsView)
        return designs.count;
    return 0;
}

- (NSAttributedString *)pickerView:(UIPickerView *)thePickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *value;
    
    if (thePickerView == diameterViewLeft)
        value = arrayDiameters[ row ];
    
    if (thePickerView == diameterViewRight)
        value = arrayDiameters[ row ];
    
    if (thePickerView == powerPickerView)
    {
        float v = [arrayPowers[row] floatValue];
        value = [ NSString stringWithFormat : @"%s%.2f", v > 0 ? "+" : "", v ];
    }
    
    if (thePickerView == cylPickerView)
    {
        float v = [arrayCyl[row] floatValue];
        value = [ NSString stringWithFormat : @"%s%.2f", v > 0 ? "+" : "", v ];
    }
    
    if (thePickerView == axisPickerView)
        value = [arrayAxis[row] stringValue];
    /*if (thePickerView == leftIndexesView || thePickerView == rightIndexesView)
        value = [indexes[row] stringValue];*/
    
    if (thePickerView == leftIndexesView)
    {
        if( [ leftDesignsView selectedRowInComponent : 0 ] == 1 )
        {
            if ([[ indicesAspheric[ row ] stringValue] isEqualToString:@"1.53"])
                value = [NSString stringWithFormat:@"%@ Trivex", [ indicesAspheric[ row ] stringValue ]];
            else if ([[ indicesAspheric[ row ] stringValue] isEqualToString:@"1.59"])
                value = [NSString stringWithFormat:@"%@ Poly", [ indicesAspheric[ row ] stringValue ]];
            else
                value = [ indicesAspheric[ row ] stringValue ];
        }
        else
        {
            if ([[ indicesSpherical[ row ] stringValue] isEqualToString:@"1.53"])
                value = [NSString stringWithFormat:@"%@ Trivex", [ indicesSpherical[ row ] stringValue ]];
            else if ([[ indicesSpherical[ row ] stringValue] isEqualToString:@"1.59"])
                value = [NSString stringWithFormat:@"%@ Poly", [ indicesSpherical[ row ] stringValue ]];
            else
                value = [ indicesSpherical[ row ] stringValue ];
        }
    }
    
    if (thePickerView == rightIndexesView)
    {
        if( [ rightDesignsView selectedRowInComponent : 0 ] == 1 )
        {
            if ([[ indicesAspheric[ row ] stringValue] isEqualToString:@"1.53"])
                value = [NSString stringWithFormat:@"%@ Trivex", [ indicesAspheric[ row ] stringValue ]];
            else if ([[ indicesAspheric[ row ] stringValue] isEqualToString:@"1.59"])
                value = [NSString stringWithFormat:@"%@ Poly", [ indicesAspheric[ row ] stringValue ]];
            else
                value = [ indicesAspheric[ row ] stringValue ];
        }
        else
        {
            if ([[ indicesSpherical[ row ] stringValue] isEqualToString:@"1.53"])
                value = [NSString stringWithFormat:@"%@ Trivex", [ indicesSpherical[ row ] stringValue ]];
            else if ([[ indicesSpherical[ row ] stringValue] isEqualToString:@"1.59"])
                value = [NSString stringWithFormat:@"%@ Poly", [ indicesSpherical[ row ] stringValue ]];
            else
                value = [ indicesSpherical[ row ] stringValue ];
        }
    }
    
    if (thePickerView == leftDesignsView || thePickerView == rightDesignsView)
        value = designs[row];
    
    NSMutableParagraphStyle * style = [ [ NSMutableParagraphStyle alloc ] init ];
    style.alignment = NSTextAlignmentRight;
    style.tailIndent = -8.;
    
    NSAttributedString *attString;
    
    if( thePickerView == powerPickerView || thePickerView == cylPickerView )
        attString = [[NSAttributedString alloc] initWithString:value attributes:@{ NSForegroundColorAttributeName:[UIColor whiteColor], NSParagraphStyleAttributeName:style }];
    else
        attString = [[NSAttributedString alloc] initWithString:value attributes:@{ NSForegroundColorAttributeName:[UIColor whiteColor] }];
    
    return attString;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40.f;
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (thePickerView == leftDesignsView)
    {
        [ leftIndexesView reloadAllComponents ];
    }
    else if (thePickerView == rightDesignsView)
    {
        [ rightIndexesView reloadAllComponents ];
    }
    
    [ self refreshData ];
    
    [ self updateGraphView ];
    
    [ thickness3DView setNeedsDisplay ];
    //[ thickness3DViewSecond setNeedsDisplay ];
}

#pragma mark Back View For Popovers

- (void)placeBackgoundView
{
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024.f, 1024.f)];
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0;
    [self.view addSubview:backgroundView];
    
    [UIView animateWithDuration:0.4f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        backgroundView.alpha = 0.4f;
    } completion:nil];
}

#undef BOOL

- (void)removeBackgoundView
{
    [UIView animateWithDuration:0.4f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        backgroundView.alpha = 0;
    } completion:^(BOOL finished){
        [backgroundView removeFromSuperview];
        backgroundView = nil;
    }];
}

#pragma mark Show Info

- (void)showInfo:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [self placeBackgoundView];
    
    if (!infoContoller)
    {
        infoContoller = [storyboard instantiateViewControllerWithIdentifier:@"infoViewController"];
        
        infoContoller.delegate = self;
        
        infoContoller.view.frame = CGRectMake((self.view.bounds.size.width - 540.f)/2, (self.view.bounds.size.height - 650.f)/2, 540.f, 650.f);
        
        [infoContoller setInfoTextForType:SMInfoAspheric];
        
        [self.view addSubview:infoContoller.view];
        
        [infoContoller.view showInfoViewInPoint:CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2)];
    }
}

- (void)dismissInfo
{
    [infoContoller.view hideInfoViewWithAnimation];
    infoContoller = nil;
    
    [self removeBackgoundView];
}

#pragma mark Autorotate

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

#pragma mark Google Analytics

- (void)moduleEntered
{
    [APP_MANAGER sendAnalyticsEnteredModuleWithName:@"SMThickness"];
}

@end
