//
//  NSDictionary+KeysRotation.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 5/16/18.
//  Copyright © 2018 ACEP. All rights reserved.
//

#import "NSDictionary+KeysRotation.h"

@implementation NSDictionary (KeysRotation)

- (NSDictionary *)rotateKeys:(NSDictionary *)keys
{
    NSMutableDictionary *source = [[NSMutableDictionary alloc] initWithDictionary:self];
    
    for (NSString *key in keys.allKeys)
    {
        if (source[key])
        {
            [source setObject:source[key] forKey:keys[key]];
            [source removeObjectForKey:key];
        }
    }
    
    return (NSDictionary *)source;
}

@end
