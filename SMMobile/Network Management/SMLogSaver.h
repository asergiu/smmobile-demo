//
//  SMCrashReporter.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 7/23/15.
//  Copyright (c) 2015 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LOG [SMLogSaver sharedManager]

@interface SMLogSaver : NSObject

+ (instancetype)sharedManager;

+ (NSString *)getLogFileName;
+ (NSString *)getLogFilePath;
+ (NSString *)getDocumentsDirectory;

+ (void)removeLogFile;

- (void)saveLogData:(NSDictionary *)dict;

@end
