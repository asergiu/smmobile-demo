//
//  SMAmazonShareHelper.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 3/3/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMAmazonShareHelper : NSObject

- (void)uploadImages:(NSArray *)images
      withCompletion:(void (^)(BOOL status, NSArray *response))completion;

- (void)cancel;

@end
