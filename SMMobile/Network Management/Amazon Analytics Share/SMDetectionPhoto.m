//
//  SMDetectionPhoto.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 5/16/17.
//  Copyright © 2017 ACEP. All rights reserved.
//

#import "SMDetectionPhoto.h"

@interface SMDetectionPhoto ()
{
    NSString *photoName;
    
    NSURL *tempFileURL;
    
    SMDetectPhotoType photoType;
    
    CGSize photoFrameSize;
}

@end

@implementation SMDetectionPhoto

@synthesize photo;

+ (SMDetectionPhoto *)createPhotoForImage:(UIImage *)img
                                  farMode:(BOOL)mode
{
    SMDetectionPhoto *photo = [SMDetectionPhoto new];
    photo.photo = img;
    
    photo->photoType = mode ? SMDetectPhotoTypeFar : SMDetectPhotoTypeNear;
    
    return photo;
}

- (NSString *)getPhotoName
{
    return photoName;
}

- (void)setPhotoName:(NSString *)name
{
    photoName = [NSString stringWithFormat:@"%@.jpg", name];
}

- (void)setPhotoFrame:(CGSize)frame
{
    photoFrameSize = frame;
}

- (NSDictionary *)getPhotoDict
{
    return @{ @"photo_type" : @(photoType),
              @"photo_name" : photoName,
              @"photo_width" : @(photoFrameSize.width),
              @"photo_height" : @(photoFrameSize.height) };
}

#pragma mark - Prepare Photo (temp directory)

- (NSURL *)getTempPhotoURL
{
    return tempFileURL;
}

- (NSString *)tempDirectoryPath
{
    return [NSTemporaryDirectory() stringByAppendingPathComponent:@"pd_analytics_share"];
}

- (void)chackAndCreateTempDirectory
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self tempDirectoryPath] isDirectory:NULL])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:[self tempDirectoryPath]
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }
}

- (void)preparePhotoForSending
{
    [self chackAndCreateTempDirectory];
    
    tempFileURL = [self saveTempImageData:UIImageJPEGRepresentation(photo, 1.f)
                                     name:photoName];
}

- (NSURL *)saveTempImageData:(NSData *)data
                        name:(NSString *)name
{
    NSString *path = [[self tempDirectoryPath] stringByAppendingPathComponent:name];
    
    [data writeToFile:path atomically:YES];
    
    return [[NSURL alloc] initFileURLWithPath:path];
}

- (void)cleanup
{
    NSError *error = nil;
    
    [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@", [self tempDirectoryPath], photoName]
                                               error:&error];
    tempFileURL = nil;
}

@end
