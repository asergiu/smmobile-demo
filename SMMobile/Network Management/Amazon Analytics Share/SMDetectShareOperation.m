//
//  SMDetectShareOperation.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 5/17/17.
//  Copyright © 2017 ACEP. All rights reserved.
//

#import "SMDetectShareOperation.h"
#import "SMDetectionPhoto.h"
#import "SMAmazonConfig.h"
#import <AWSCore/AWSCore.h>
#import <AWSS3/AWSS3.h>
#import "SMNetworkManager.h"

@interface SMDetectShareOperation ()
{
    NSDictionary *currentData;
    
    SMDetectionPhoto *originPhoto;
    SMDetectionPhoto *detectPhoto;
    
    BOOL finished;
    BOOL executing;
    
    BOOL isFarType;
    
    void (^_handler)(BOOL success, NSString *reason, BOOL isFarType);
    
    AWSS3TransferManager *transferManager;
}

@end

@implementation SMDetectShareOperation

- (id)initWithData:(NSDictionary *)data
       detectPhoto:(SMDetectionPhoto *)photo
       originPhoto:(SMDetectionPhoto *)origin
    isFarOperation:(BOOL)isFar
         doneBlock:(void (^)(BOOL, NSString *, BOOL))block
{
    self = [super init];
    
    if (self)
    {
        _handler = [block copy];
        
        currentData = data;
        
        isFarType = isFar;
        
        originPhoto = origin;
        detectPhoto = photo;
        
        executing = NO;
        finished = NO;
    }
    return self;
}

- (BOOL)isAsynchronous
{
    return YES;
}

- (BOOL)isConcurrent
{
    return YES;
}

- (BOOL)isExecuting
{
    return executing;
}

- (BOOL)isFinished
{
    return finished;
}

- (void)start
{
    [self willChangeValueForKey:@"isExecuting"];
    executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
    [originPhoto preparePhotoForSending];
    [detectPhoto preparePhotoForSending];

    [self uploadDetectPhoto];
}

- (void)uploadDetectPhoto
{
    __weak __typeof(self)weakSelf = self;
    
    [self uploadPhoto:detectPhoto completion:^(BOOL success, NSString *reason) {
        
        if (success)
            [weakSelf uploadOriginPhoto];
        else
        {
            [detectPhoto cleanup];
            
            [weakSelf completeOperation];
            
            _handler(success, reason, isFarType);
        }
    }];
}

- (void)uploadOriginPhoto
{
    __weak __typeof(self)weakSelf = self;
    
    [self uploadPhoto:originPhoto completion:^(BOOL success, NSString *reason) {
        
        if (success)
            [weakSelf sendData];
        else
        {
            [originPhoto cleanup];
            
            [weakSelf completeOperation];
            
            _handler(success, reason, isFarType);
        }
    }];
}

- (void)uploadPhoto:(SMDetectionPhoto *)photoData
         completion:(void(^)(BOOL success, NSString *reason))handler
{
    AWSS3TransferManagerUploadRequest *uploadReq = [AWSS3TransferManagerUploadRequest new];
    
    uploadReq.bucket = kS3PDAnalyticsBucket;
    uploadReq.ACL = AWSS3ObjectCannedACLPrivate;
    uploadReq.key = [photoData getPhotoName];
    uploadReq.contentType = @"image/jpeg";
    uploadReq.body = [photoData getTempPhotoURL];
    
    uploadReq.uploadProgress =^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
        });
    };
    
    transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    [[transferManager upload:uploadReq] continueWithBlock:^id(AWSTask *task) {
        
        if (task.error || [self isCancelled])
            handler(NO, [task.error localizedDescription]);
        else
            handler(YES, nil);
        
        return nil;
    }];
}

- (void)sendData
{
    __weak __typeof(self)weakSelf = self;
    
    [NETWORK_MANAGER sendPDAnalyticsData:currentData
                          withCompletion:^(BOOL status, NSString *reason) {
                              
                              [detectPhoto cleanup];
                              [originPhoto cleanup];
                              
                              [weakSelf completeOperation];
                              
                              _handler(status, reason, isFarType);
                          }];
}

- (void)completeOperation
{
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    
    executing = NO;
    finished = YES;
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

- (void)cancel
{
    [super cancel];
    
    if (transferManager)
        [transferManager cancelAll];
}

@end
