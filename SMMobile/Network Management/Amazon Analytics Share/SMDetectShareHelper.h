//
//  SMDetectShareHelper.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 5/17/17.
//  Copyright © 2017 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMDetectShareHelper : NSObject

- (void)initUnitedDetectSession;

#pragma mark - Support

- (void)setDetectionPhoto:(UIImage *)img
                frameSize:(CGSize)frame
               forFarMode:(BOOL)mode;

- (void)setOriginPhoto:(UIImage *)img
             frameSize:(CGSize)frame
            forFarMode:(BOOL)mode;

- (void)setBoxingPhotoRotateAngle:(double)angle;

- (void)setUserSupportPointA_XValue:(CGFloat)val;
- (void)setUserSupportPointA_YValue:(CGFloat)val;
- (void)setUserSupportPointB_XValue:(CGFloat)val;
- (void)setUserSupportPointB_YValue:(CGFloat)val;
- (void)setUserSupportPointC1_XValue:(CGFloat)val;
- (void)setUserSupportPointC1_YValue:(CGFloat)val;

- (void)setSupportPointA:(CGPoint)aPoint
                  pointB:(CGPoint)bPoint
                 pointC1:(CGPoint)c1Point;

- (void)setSupportsWasMoved:(BOOL)moved;

- (void)compareSupportPointA:(CGPoint)aPoint
                      pointB:(CGPoint)bPoint
                     pointC1:(CGPoint)c1Point;

#pragma mark - Eyes

- (void)setUserLeftEye_XValue:(CGFloat)val;
- (void)setUserLeftEye_YValue:(CGFloat)val;
- (void)setUserRightEye_XValue:(CGFloat)val;
- (void)setUserRightEye_YValue:(CGFloat)val;

- (void)setEyePointLeft:(CGPoint)leftPoint
                  right:(CGPoint)rightPoint;

- (void)setEyePointsWasMoved:(BOOL)moved;

- (void)compareEyePointLeft:(CGPoint)leftPoint
                      right:(CGPoint)rightPoint;

#pragma mark - Boxes

- (void)setBoxesEyePointLeft:(CGPoint)leftEyePoint
          boxesEyePointRight:(CGPoint)rightEyePoint;

- (void)setBoxesSupportPointA:(CGPoint)aPoint
                       pointB:(CGPoint)bPoint
                      pointC1:(CGPoint)c1Point;

- (void)setUserBoxesTopLeftPoint:(CGPoint)topLeft;
- (void)setUserBoxesTopRightPoint:(CGPoint)topRight;
- (void)setUserBoxesBottomLeftPoint:(CGPoint)bottomLeft;
- (void)setUserBoxesBottomRightPoint:(CGPoint)bottomRight;

- (void)setBoxesTopLeft:(CGPoint)topLeft
          boxesTopRight:(CGPoint)topRight
        boxesBottomLeft:(CGPoint)bottomLeft
       boxesBottomRight:(CGPoint)bottomRight
        boxesBridgeLeft:(CGPoint)bridgeLeft
       boxesBridgeRight:(CGPoint)bridgeRight
           boxesExtLeft:(CGPoint)extLeft
          boxesExtRight:(CGPoint)extRight;

- (void)boxingIsMoved:(BOOL)moved;

- (void)processData;

#pragma mark - Clear

- (void)clear;

@end
