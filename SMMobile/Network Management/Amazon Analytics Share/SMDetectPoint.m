//
//  SMDetectPoint.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 5/16/17.
//  Copyright © 2017 ACEP. All rights reserved.
//

#import "SMDetectPoint.h"

#define kDiffFactorValue 0.002f

@interface SMDetectPoint ()
{
    SMDetectionType pointForDetectType;
    
    SUPPORT_POINT_TYPE supportPointType;
    EYE_POINT_TYPE eyePointType;
    BoxingType boxingPointType;
    
    NSString *pointName;
    
    BOOL successDetection;
}

- (void)setPointName;

@end

@implementation SMDetectPoint

@synthesize x;
@synthesize y;

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        successDetection = NO;
    }
    return self;
}

#pragma mark - Support Points

+ (SMDetectPoint *)createZeroSupportPointWithType:(SUPPORT_POINT_TYPE)type
{
    return [SMDetectPoint createSupportPointWithType:type X:0 Y:0];
}

+ (SMDetectPoint *)createSupportPointWithType:(SUPPORT_POINT_TYPE)type
                                            X:(double)x
                                            Y:(double)y
{
    SMDetectPoint *point = [SMDetectPoint new];
    
    point->pointForDetectType = SMDetectionTypeSupport;
    point->supportPointType = type;
    
    point.x = x;
    point.y = y;
    
    [point setPointName];
    
    [point setSuccessState];
    
    return point;
}

#pragma mark - Eye Points

+ (SMDetectPoint *)createZeroEyePointWithType:(EYE_POINT_TYPE)type
{
    return [SMDetectPoint createEyePointWithType:type X:0 Y:0];
}

+ (SMDetectPoint *)createEyePointWithType:(EYE_POINT_TYPE)type
                                        X:(double)x
                                        Y:(double)y
{
    SMDetectPoint *point = [SMDetectPoint new];
    
    point->pointForDetectType = SMDetectionTypeEyes;
    point->eyePointType = type;
    
    point.x = x;
    point.y = y;
    
    [point setPointName];
    
    [point setSuccessState];
    
    return point;
}

#pragma mark - Boxing Points

+ (SMDetectPoint *)createZeroBoxingPointWithType:(BoxingType)type
{
    return [SMDetectPoint createBoxingPointWithType:type X:0 Y:0];
}

+ (SMDetectPoint *)createBoxingPointWithType:(BoxingType)type
                                           X:(double)x
                                           Y:(double)y
{
    SMDetectPoint *point = [SMDetectPoint new];
    
    point->pointForDetectType = SMDetectionTypeBoxes;
    point->boxingPointType = type;
    
    point.x = x;
    point.y = y;
    
    [point setPointName];
    
    [point setSuccessState];
    
    return point;
}

#pragma mark - Comparison

- (BOOL)matchPoint:(SMDetectPoint *)point
{
    if(fabs(x - point.x) > kDiffFactorValue)
        return NO;
    
    if(fabs(y - point.y) > kDiffFactorValue)
        return NO;
    
    return YES;
}

#pragma mark -

- (void)setSuccessState
{
    if (x >= 0 && y >= 0)
        successDetection = YES;
}

- (void)setPointName
{
    if (pointForDetectType == SMDetectionTypeSupport)
    {
        switch (supportPointType) {
            case SUPPORT_POINT_A:
                pointName = @"support_point_a";
                break;
            case SUPPORT_POINT_B:
                pointName = @"support_point_b";
                break;
            case SUPPORT_POINT_C1:
                pointName = @"support_point_c1";
                break;
            default:
                break;
        }
    }
    
    if (pointForDetectType == SMDetectionTypeEyes)
    {
        switch (eyePointType) {
            case EYE_POINT_LEFT:
                pointName = @"eye_point_left";
                break;
            case EYE_POINT_RIGHT:
                pointName = @"eye_point_right";
                break;
            default:
                break;
        }
    }
    
    if (pointForDetectType == SMDetectionTypeBoxes)
    {
        switch (boxingPointType) {
            case BoxingType_leftEye:
                pointName = @"eye_point_left";
                break;
            case BoxingType_rightEye:
                pointName = @"eye_point_right";
                break;
            case BoxingType_supportAPoint:
                pointName = @"support_point_a";
                break;
            case BoxingType_supportBPoint:
                pointName = @"support_point_b";
                break;
            case BoxingType_supportC1Point:
                pointName = @"support_point_c1";
                break;
            case BoxingType_leftBoxingTop:
                pointName = @"left_boxing_top";
                break;
            case BoxingType_rightBoxingTop:
                pointName = @"right_boxing_top";
                break;
            case BoxingType_leftBoxingBottom:
                pointName = @"left_boxing_bottom";
                break;
            case BoxingType_rightBoxingBottom:
                pointName = @"right_boxing_bottom";
                break;
            case BoxingType_leftBoxingBridge:
                pointName = @"left_boxing_bridge";
                break;
            case BoxingType_rightBoxingBridge:
                pointName = @"right_boxing_bridge";
                break;
            case BoxingType_leftBoxingExt:
                pointName = @"left_boxing_ext";
                break;
            case BoxingType_rightBoxingExt:
                pointName = @"right_boxing_ext";
                break;
            default:
                break;
        }
    }
}

- (NSDictionary *)getPointDict
{
    return @{ pointName : @{ @"x" : [NSNumber numberWithDouble:x],
                             @"y" : [NSNumber numberWithDouble:y],
                             @"success" : [NSNumber numberWithBool:successDetection] } };
}

#pragma mark - Description

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ => %.4lf;%.4lf", pointName, x, y];
}

@end
