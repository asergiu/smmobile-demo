//
//  SMDetectionDevice.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 1/17/18.
//  Copyright © 2018 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMDetectionDevice : NSObject

- (NSDictionary *)getDictionary;

@end
