//
//  SMDetectionPhoto.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 5/16/17.
//  Copyright © 2017 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM (NSInteger, SMDetectPhotoType) {
    SMDetectPhotoTypeNear,
    SMDetectPhotoTypeFar
};

@interface SMDetectionPhoto : NSObject

@property (nonatomic, strong) UIImage *photo;

+ (SMDetectionPhoto *)createPhotoForImage:(UIImage *)img
                                  farMode:(BOOL)mode;

- (NSString *)getPhotoName;
- (void)setPhotoName:(NSString *)name;

- (void)setPhotoFrame:(CGSize)frame;

- (void)preparePhotoForSending;
- (void)cleanup;

- (NSURL *)getTempPhotoURL;
- (NSDictionary *)getPhotoDict;

@end
