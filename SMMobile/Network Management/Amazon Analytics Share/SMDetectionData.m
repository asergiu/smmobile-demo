//
//  SMDetectionData.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 5/16/17.
//  Copyright © 2017 ACEP. All rights reserved.
//

#import "SMDetectionData.h"

@interface SMDetectionData ()
{
    SMDetectionType detectionType;
    
    SUPPORT_TYPE supportType;
    
    EYE_DETECTION_TYPE eyeDetectType;
    
    // For SMDetectionTypeSupport
    SMDetectPoint *detectSupportAPoint;  // Point support_point_a after detection
    SMDetectPoint *detectSupportBPoint;  // Point support_point_b after detection
    SMDetectPoint *detectSupportC1Point; // Point support_point_c after detection
    
    SMDetectPoint *supportAPoint;        // Final point support_point_a position
    SMDetectPoint *supportBPoint;        // Final point support_point_b position
    SMDetectPoint *supportC1Point;       // Final point support_point_c position
    
    // For SMDetectionTypeEyes
    SMDetectPoint *detectLeftEyePoint;   // Point left_eye after detection
    SMDetectPoint *detectRightEyePoint;  // Point right_eye after detection
    
    SMDetectPoint *leftEyePoint;         // Final point left_eye position
    SMDetectPoint *rightEyePoint;        // Final point right_eye position
    
    // For SMDetectionTypeBoxes
    SMDetectPoint *leftBoxingEyePoint;
    SMDetectPoint *rightBoxingEyePoint;
    
    SMDetectPoint *supportBoxingAPoint;
    SMDetectPoint *supportBoxingBPoint;
    SMDetectPoint *supportBoxingC1Point;
    
    // Points after detection
    SMDetectPoint *detectLeftBoxingTopPoint;
    SMDetectPoint *detectRightBoxingTopPoint;
    SMDetectPoint *detectLeftBoxingBottomPoint;
    SMDetectPoint *detectRightBoxingBottomPoint;
    SMDetectPoint *detectLeftBoxingBridgePoint;
    SMDetectPoint *detectRightBoxingBridgePoint;
    SMDetectPoint *detectLeftBoxingExtPoint;
    SMDetectPoint *detectRightBoxingExtPoint;
    
    // Final point positions
    SMDetectPoint *leftBoxingTopPoint;
    SMDetectPoint *rightBoxingTopPoint;
    SMDetectPoint *leftBoxingBottomPoint;
    SMDetectPoint *rightBoxingBottomPoint;
    
    SMDetectPoint *leftBoxingControlPoint;
    SMDetectPoint *rightBoxingControlPoint;
    SMDetectPoint *leftBoxingTopControlPoint;
    SMDetectPoint *rightBoxingTopControlPoint;
    
    BOOL dataMustBeSent;
    BOOL workedOut;
}

- (void)setDetectionType:(SMDetectionType)type;

@end

@implementation SMDetectionData

+ (SMDetectionData *)createNewWithType:(SMDetectionType)type
{
    SMDetectionData *detectSess = [SMDetectionData new];
    
    [detectSess setDetectionType:type];
    [detectSess initUserPointsState];
    
    return detectSess;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        dataMustBeSent = NO;
        workedOut = NO;
    }
    return self;
}

- (void)setDetectionType:(SMDetectionType)type
{
    detectionType = type;
}

- (void)initUserPointsState
{
    if (detectionType == SMDetectionTypeSupport)
    {
        supportAPoint  = [SMDetectPoint createZeroSupportPointWithType:SUPPORT_POINT_A];
        supportBPoint  = [SMDetectPoint createZeroSupportPointWithType:SUPPORT_POINT_B];
        supportC1Point = [SMDetectPoint createZeroSupportPointWithType:SUPPORT_POINT_C1];
    }
    
    if (detectionType == SMDetectionTypeEyes)
    {
        leftEyePoint  = [SMDetectPoint createZeroEyePointWithType:EYE_POINT_LEFT];
        rightEyePoint = [SMDetectPoint createZeroEyePointWithType:EYE_POINT_RIGHT];
    }
    
    if (detectionType == SMDetectionTypeBoxes)
    {
        leftBoxingTopPoint  = [SMDetectPoint createZeroBoxingPointWithType:BoxingType_leftBoxingTop];
        rightBoxingTopPoint = [SMDetectPoint createZeroBoxingPointWithType:BoxingType_rightBoxingTop];
        
        leftBoxingBottomPoint  = [SMDetectPoint createZeroBoxingPointWithType:BoxingType_leftBoxingBottom];
        rightBoxingBottomPoint = [SMDetectPoint createZeroBoxingPointWithType:BoxingType_rightBoxingBottom];
    }
}

- (BOOL)isNeedBeSended
{
    return !workedOut;
}

- (void)markAsWorkedOut
{
    dataMustBeSent = NO;
    workedOut = YES;
}

#pragma mark - Support type

- (void)setSupportType:(SUPPORT_TYPE)type
{
    supportType = type;
}

#pragma mark - Eye Detection Type

- (void)setEyeDetectionType:(EYE_DETECTION_TYPE)type
{
    eyeDetectType = type;
}

#pragma mark - Support points

- (void)setUserNewValue:(CGFloat)value
                 forKey:(NSString *)key
       supportPointType:(SUPPORT_POINT_TYPE)type
{
    switch (type) {
        case SUPPORT_POINT_A:
            [supportAPoint setValue:@(value) forKey:key];
            break;
        case SUPPORT_POINT_B:
            [supportBPoint setValue:@(value) forKey:key];
            break;
        case SUPPORT_POINT_C1:
            [supportC1Point setValue:@(value) forKey:key];
            break;
        default:
            break;
    }
}

- (void)setDetectedSupportPointsA:(SMDetectPoint *)aPoint
                                B:(SMDetectPoint *)bPoint
                               C1:(SMDetectPoint *)c1Point
{
    if (detectionType != SMDetectionTypeSupport)
        return;
    
    detectSupportAPoint  = aPoint;
    detectSupportBPoint  = bPoint;
    detectSupportC1Point = c1Point;
}

- (void)compareSupportPointsA:(SMDetectPoint *)newAPoint
                            B:(SMDetectPoint *)newBPoint
                           C1:(SMDetectPoint *)newC1Point
{
    if (detectionType != SMDetectionTypeSupport)
        return;
    
    if (![detectSupportAPoint matchPoint:newAPoint])
        dataMustBeSent = YES;
    
    if (![detectSupportBPoint matchPoint:newBPoint])
        dataMustBeSent = YES;
    
    if (![detectSupportC1Point matchPoint:newC1Point])
        dataMustBeSent = YES;
}

- (void)supportsIsNeedToBeSent
{
    if (detectionType != SMDetectionTypeSupport)
        return;
    
    if (!workedOut)
        dataMustBeSent = YES;
}

#pragma mark - Eye points

- (void)setUserNewValue:(CGFloat)value
                 forKey:(NSString *)key
           eyePointType:(EYE_POINT_TYPE)type
{
    switch (type) {
        case EYE_POINT_LEFT:
            [leftEyePoint setValue:@(value) forKey:key];
            break;
        case EYE_POINT_RIGHT:
            [rightEyePoint setValue:@(value) forKey:key];
            break;
        default:
            break;
    }
}

- (void)setDetectedEyePointsLeft:(SMDetectPoint *)leftPoint
                           right:(SMDetectPoint *)rightPoint
{
    if (detectionType != SMDetectionTypeEyes)
        return;
    
    detectLeftEyePoint  = leftPoint;
    detectRightEyePoint = rightPoint;
}

- (void)compareEyePointsLeft:(SMDetectPoint *)newLeftPoint
                       right:(SMDetectPoint *)newRightPoint
{
    if (detectionType != SMDetectionTypeEyes)
        return;
    
    if (![detectLeftEyePoint matchPoint:newLeftPoint])
        dataMustBeSent = YES;
    
    if (![detectRightEyePoint matchPoint:newRightPoint])
        dataMustBeSent = YES;
}

- (void)eyePointsIsNeedToBeSent
{
    if (detectionType != SMDetectionTypeEyes)
        return;
    
    if (!workedOut)
        dataMustBeSent = YES;
}

#pragma mark - Boxing points

- (void)setBoxingSupportPointsA:(SMDetectPoint *)aPoint
                              B:(SMDetectPoint *)bPoint
                             C1:(SMDetectPoint *)c1Point
{
    if (detectionType != SMDetectionTypeBoxes)
        return;
    
    supportBoxingAPoint  = aPoint;
    supportBoxingBPoint  = bPoint;
    supportBoxingC1Point = c1Point;
}

- (void)setEyeBoxingPointsLeft:(SMDetectPoint *)leftPoint
                         right:(SMDetectPoint *)rightPoint
{
    if (detectionType != SMDetectionTypeBoxes)
        return;
    
    leftBoxingEyePoint = leftPoint;
    rightBoxingEyePoint = rightPoint;
}

- (void)setUserNewPoint:(SMDetectPoint *)point
          forBoxingType:(BoxingType)type
{
    switch (type) {
        case BoxingType_leftBoxingTop:
            leftBoxingTopPoint = point;
            break;
        case BoxingType_rightBoxingTop:
            rightBoxingTopPoint = point;
            break;
        case BoxingType_leftBoxingBottom:
            leftBoxingBottomPoint = point;
            break;
        case BoxingType_rightBoxingBottom:
            rightBoxingBottomPoint = point;
            break;
        default:
            break;
    }
}

- (void)setDetectedBoxingTopPointsLeft:(SMDetectPoint *)leftBoxingTop
                  boxingTopPointsright:(SMDetectPoint *)rightBoxingTop
                boxingBottomPointsLeft:(SMDetectPoint *)leftBoxingBottom
               boxingBottomPointsRight:(SMDetectPoint *)rightBoxingBottom
                boxingBridgePointsLeft:(SMDetectPoint *)leftBoxingBridge
               boxingBridgePointsRight:(SMDetectPoint *)rightBoxingBridge
                   boxingExtPointsLeft:(SMDetectPoint *)leftBoxingExt
                  boxingExtPointsRight:(SMDetectPoint *)rightBoxingExt;
{
    if (detectionType != SMDetectionTypeBoxes)
        return;
    
    detectLeftBoxingTopPoint = leftBoxingTop;
    detectRightBoxingTopPoint = rightBoxingTop;
    detectLeftBoxingBottomPoint = leftBoxingBottom;
    detectRightBoxingBottomPoint = rightBoxingBottom;
    detectLeftBoxingBridgePoint = leftBoxingBridge;
    detectRightBoxingBridgePoint = rightBoxingBridge;
    detectLeftBoxingExtPoint = leftBoxingExt;
    detectRightBoxingExtPoint = rightBoxingExt;
}

- (void)boxingIsNeedToBeSent
{
    if (detectionType != SMDetectionTypeBoxes)
        return;
    
    if (!workedOut)
        dataMustBeSent = YES;
}

#pragma mark - 

- (NSDictionary *)getDataDict
{
    NSMutableDictionary *detectionDict = [NSMutableDictionary new];
    detectionDict[@"detection_type"] = @(detectionType);
    detectionDict[@"support_type"] = @(supportType);
    
    if (detectionType == SMDetectionTypeSupport)
    {
        NSMutableDictionary *points = [NSMutableDictionary new];
        [points addEntriesFromDictionary:[detectSupportAPoint getPointDict]];
        [points addEntriesFromDictionary:[detectSupportBPoint getPointDict]];
        [points addEntriesFromDictionary:[detectSupportC1Point getPointDict]];
        
        detectionDict[@"support_points"] = points;
        
        if (dataMustBeSent)
        {
            NSMutableDictionary *user_points = [NSMutableDictionary new];
            [user_points addEntriesFromDictionary:[supportAPoint getPointDict]];
            [user_points addEntriesFromDictionary:[supportBPoint getPointDict]];
            [user_points addEntriesFromDictionary:[supportC1Point getPointDict]];
            
            detectionDict[@"user_support_points"] = user_points;
        }
    }
    
    if (detectionType == SMDetectionTypeEyes)
    {
        detectionDict[@"eye_detect_type"] = @(eyeDetectType);

        NSMutableDictionary *points = [NSMutableDictionary new];
        [points addEntriesFromDictionary:[detectLeftEyePoint getPointDict]];
        [points addEntriesFromDictionary:[detectRightEyePoint getPointDict]];
        
        detectionDict[@"eye_points"] = points;

        if (dataMustBeSent)
        {
            NSMutableDictionary *user_points = [NSMutableDictionary new];
            [user_points addEntriesFromDictionary:[leftEyePoint getPointDict]];
            [user_points addEntriesFromDictionary:[rightEyePoint getPointDict]];
            
            detectionDict[@"user_eye_points"] = user_points;
        }
    }
    
    if (detectionType == SMDetectionTypeBoxes)
    {
        NSMutableDictionary *boxing_points = [NSMutableDictionary new];
        [boxing_points addEntriesFromDictionary:[detectLeftBoxingTopPoint getPointDict]];
        [boxing_points addEntriesFromDictionary:[detectRightBoxingTopPoint getPointDict]];
        [boxing_points addEntriesFromDictionary:[detectLeftBoxingBottomPoint getPointDict]];
        [boxing_points addEntriesFromDictionary:[detectRightBoxingBottomPoint getPointDict]];
        [boxing_points addEntriesFromDictionary:[detectLeftBoxingBridgePoint getPointDict]];
        [boxing_points addEntriesFromDictionary:[detectRightBoxingBridgePoint getPointDict]];
        [boxing_points addEntriesFromDictionary:[detectLeftBoxingExtPoint getPointDict]];
        [boxing_points addEntriesFromDictionary:[detectRightBoxingExtPoint getPointDict]];
        
        detectionDict[@"boxing_points"] = boxing_points;
        
        if (dataMustBeSent)
        {
            NSMutableDictionary *user_boxing_points = [NSMutableDictionary new];
            [user_boxing_points addEntriesFromDictionary:[leftBoxingTopPoint getPointDict]];
            [user_boxing_points addEntriesFromDictionary:[rightBoxingTopPoint getPointDict]];
            [user_boxing_points addEntriesFromDictionary:[leftBoxingBottomPoint getPointDict]];
            [user_boxing_points addEntriesFromDictionary:[rightBoxingBottomPoint getPointDict]];
            
            detectionDict[@"user_boxing_points"] = user_boxing_points;
        }
        
        NSMutableDictionary *eye_points = [NSMutableDictionary new];
        [eye_points addEntriesFromDictionary:[leftBoxingEyePoint getPointDict]];
        [eye_points addEntriesFromDictionary:[rightBoxingEyePoint getPointDict]];
        
        detectionDict[@"eye_points"] = eye_points;
        
        NSMutableDictionary *support_points = [NSMutableDictionary new];
        [support_points addEntriesFromDictionary:[supportBoxingAPoint getPointDict]];
        [support_points addEntriesFromDictionary:[supportBoxingBPoint getPointDict]];
        [support_points addEntriesFromDictionary:[supportBoxingC1Point getPointDict]];
        
        detectionDict[@"support_points"] = support_points;
    }
    
    return (NSDictionary *)detectionDict;
}

@end
