//
//  SMDetectionData.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 5/16/17.
//  Copyright © 2017 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMDetectPoint.h"

#define X_VALUE @"x"
#define Y_VALUE @"y"

@interface SMDetectionData : NSObject

+ (SMDetectionData *)createNewWithType:(SMDetectionType)type;

/*
 *	nAcepMetalSupport = 0,
 *	nAcepPlasticSupport = 1,
 *	nZeissSupport = 2
 */
- (void)setSupportType:(SUPPORT_TYPE)type;

/*
 *  For SMDetectionTypeEyes only
 */
- (void)setEyeDetectionType:(EYE_DETECTION_TYPE)type;


- (void)setUserNewValue:(CGFloat)value
                 forKey:(NSString *)key
       supportPointType:(SUPPORT_POINT_TYPE)type;

- (void)setDetectedSupportPointsA:(SMDetectPoint *)aPoint
                                B:(SMDetectPoint *)bPoint
                               C1:(SMDetectPoint *)c1Point;

- (void)compareSupportPointsA:(SMDetectPoint *)newAPoint
                            B:(SMDetectPoint *)newBPoint
                           C1:(SMDetectPoint *)newC1Point;

- (void)setDetectedEyePointsLeft:(SMDetectPoint *)leftPoint
                           right:(SMDetectPoint *)rightPoint;

- (void)setUserNewValue:(CGFloat)value
                 forKey:(NSString *)key
           eyePointType:(EYE_POINT_TYPE)type;

- (void)compareEyePointsLeft:(SMDetectPoint *)newLeftPoint
                       right:(SMDetectPoint *)newRightPoint;

- (void)setBoxingSupportPointsA:(SMDetectPoint *)aPoint
                              B:(SMDetectPoint *)bPoint
                             C1:(SMDetectPoint *)c1Point;

- (void)setEyeBoxingPointsLeft:(SMDetectPoint *)leftPoint
                         right:(SMDetectPoint *)rightPoint;

- (void)setUserNewPoint:(SMDetectPoint *)point
          forBoxingType:(BoxingType)type;

- (void)setDetectedBoxingTopPointsLeft:(SMDetectPoint *)leftBoxingTop
                  boxingTopPointsright:(SMDetectPoint *)rightBoxingTop
                boxingBottomPointsLeft:(SMDetectPoint *)leftBoxingBottom
               boxingBottomPointsRight:(SMDetectPoint *)rightBoxingBottom
                boxingBridgePointsLeft:(SMDetectPoint *)leftBoxingBridge
               boxingBridgePointsRight:(SMDetectPoint *)rightBoxingBridge
                   boxingExtPointsLeft:(SMDetectPoint *)leftBoxingExt
                  boxingExtPointsRight:(SMDetectPoint *)rightBoxingExt;

- (void)supportsIsNeedToBeSent;
- (void)eyePointsIsNeedToBeSent;
- (void)boxingIsNeedToBeSent;

- (BOOL)isNeedBeSended;
- (void)markAsWorkedOut;

- (NSDictionary *)getDataDict;

@end
