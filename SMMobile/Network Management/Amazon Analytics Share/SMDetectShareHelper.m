//
//  SMDetectShareHelper.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 5/17/17.
//  Copyright © 2017 ACEP. All rights reserved.
//

#import "SMDetectShareHelper.h"
#import "SMDetectionData.h"
#import "SMDetectionPhoto.h"
#import "SMDetectionDevice.h"
#import <AWSCore/AWSCore.h>
#import <AWSS3/AWSS3.h>
#import "SMAmazonConfig.h"
#import "SMDetectShareOperation.h"
#import "PDMDataManager.h"
#import "SMAppManager.h"

#define DATASET_VERSION 4

@interface SMDetectShareHelper ()
{
    NSString *uuid;
    
    SMDetectionPhoto *originPhoto;
    SMDetectionPhoto *detectPhoto;
    
    double photo_rotate_angle;
    
    SMDetectionDevice *device;

    SMDetectionData *far_supportDetectData;
    SMDetectionData *far_eyesDetectData;
    SMDetectionData *far_boxesDetectData;
    
    SMDetectionData *near_supportDetectData;
    SMDetectionData *near_eyesDetectData;
    SMDetectionData *near_boxesDetectData;
    
    NSOperationQueue *dataQueue;
}

@end

@implementation SMDetectShareHelper

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        AWSStaticCredentialsProvider *credProvider = [[AWSStaticCredentialsProvider alloc] initWithAccessKey:kS3AccessKey
                                                                                                   secretKey:kS3SecretKey];
        
        AWSServiceConfiguration *config = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionEUWest1
                                                                      credentialsProvider:credProvider];
        
        [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = config;

        dataQueue = [NSOperationQueue new];
        dataQueue.maxConcurrentOperationCount = 3;
    }
    return self;
}

- (void)initUnitedDetectSession
{
    uuid = [[NSUUID UUID] UUIDString];
    
    device = [SMDetectionDevice new];
}

#pragma mark - Photo

- (void)setDetectionPhoto:(UIImage *)img
                frameSize:(CGSize)frame
               forFarMode:(BOOL)mode
{
    detectPhoto = [SMDetectionPhoto createPhotoForImage:img
                                                farMode:mode];
    
    [detectPhoto setPhotoFrame:frame];
    
    [detectPhoto setPhotoName:uuid];
}

- (void)setOriginPhoto:(UIImage *)img
             frameSize:(CGSize)frame
            forFarMode:(BOOL)mode
{
    originPhoto = [SMDetectionPhoto createPhotoForImage:img
                                                farMode:mode];
    
    [originPhoto setPhotoFrame:frame];
    
    [originPhoto setPhotoName:[NSString stringWithFormat:@"%@_origin", uuid]];
}

- (void)setBoxingPhotoRotateAngle:(double)angle
{
    photo_rotate_angle = angle;
}

#pragma mark - Support

/*
 *  User support points
 */

- (SMDetectionData *)getSupportDetectData
{
    if ([PDM_DATA getFarMode])
        return far_supportDetectData;
    else
        return near_supportDetectData;
}

- (void)setUserSupportPointA_XValue:(CGFloat)val
{
    if (![self getSupportDetectData])
        return;
    
    [[self getSupportDetectData] setUserNewValue:val
                                          forKey:X_VALUE
                                supportPointType:SUPPORT_POINT_A];
}

- (void)setUserSupportPointA_YValue:(CGFloat)val
{
    if (![self getSupportDetectData])
        return;
    
    [[self getSupportDetectData] setUserNewValue:val
                                          forKey:Y_VALUE
                                supportPointType:SUPPORT_POINT_A];
}

- (void)setUserSupportPointB_XValue:(CGFloat)val
{
    if (![self getSupportDetectData])
        return;
    
    [[self getSupportDetectData] setUserNewValue:val
                                          forKey:X_VALUE
                                supportPointType:SUPPORT_POINT_B];
}

- (void)setUserSupportPointB_YValue:(CGFloat)val
{
    if (![self getSupportDetectData])
        return;
    
    [[self getSupportDetectData] setUserNewValue:val
                                          forKey:Y_VALUE
                                supportPointType:SUPPORT_POINT_B];
}

- (void)setUserSupportPointC1_XValue:(CGFloat)val
{
    if (![self getSupportDetectData])
        return;
    
    [[self getSupportDetectData] setUserNewValue:val
                                          forKey:X_VALUE
                                supportPointType:SUPPORT_POINT_C1];
}

- (void)setUserSupportPointC1_YValue:(CGFloat)val
{
    if (![self getSupportDetectData])
        return;
    
    [[self getSupportDetectData] setUserNewValue:val
                                          forKey:Y_VALUE
                                supportPointType:SUPPORT_POINT_C1];
}

- (void)setSupportPointA:(CGPoint)aPoint
                  pointB:(CGPoint)bPoint
                 pointC1:(CGPoint)c1Point
{
    if (![APP_MANAGER isMeasurementsAnalyticsEnabled])
        return;
    
    if ([PDM_DATA getFarMode])
        far_supportDetectData = [SMDetectionData createNewWithType:SMDetectionTypeSupport];
    else
        near_supportDetectData = [SMDetectionData createNewWithType:SMDetectionTypeSupport];
    
    [[self getSupportDetectData] setSupportType:[PDM_DATA getCurrentSessionSupportType]];
    
    SMDetectPoint *aSupportPoint = [SMDetectPoint createSupportPointWithType:SUPPORT_POINT_A
                                                                           X:aPoint.x
                                                                           Y:aPoint.y];
    SMDetectPoint *bSupportPoint = [SMDetectPoint createSupportPointWithType:SUPPORT_POINT_B
                                                                           X:bPoint.x
                                                                           Y:bPoint.y];
    SMDetectPoint *c1SupportPoint = [SMDetectPoint createSupportPointWithType:SUPPORT_POINT_C1
                                                                            X:c1Point.x
                                                                            Y:c1Point.y];
    [[self getSupportDetectData] setDetectedSupportPointsA:aSupportPoint
                                                         B:bSupportPoint
                                                        C1:c1SupportPoint];
}

- (void)compareSupportPointA:(CGPoint)aPoint
                      pointB:(CGPoint)bPoint
                     pointC1:(CGPoint)c1Point
{
    if (![self getSupportDetectData])
        return;
    
    SMDetectPoint *aSupportPoint = [SMDetectPoint createSupportPointWithType:SUPPORT_POINT_A
                                                                           X:aPoint.x
                                                                           Y:aPoint.y];
    SMDetectPoint *bSupportPoint = [SMDetectPoint createSupportPointWithType:SUPPORT_POINT_B
                                                                           X:bPoint.x
                                                                           Y:bPoint.y];
    SMDetectPoint *c1SupportPoint = [SMDetectPoint createSupportPointWithType:SUPPORT_POINT_C1
                                                                            X:c1Point.x
                                                                            Y:c1Point.y];
    [[self getSupportDetectData] compareSupportPointsA:aSupportPoint
                                                     B:bSupportPoint
                                                    C1:c1SupportPoint];
}

- (void)setSupportsWasMoved:(BOOL)moved
{
    if (![self getSupportDetectData])
        return;
    
    if (moved)
        [[self getSupportDetectData] supportsIsNeedToBeSent];
}

#pragma mark - Eyes

/*
 *  User eye points
 */

- (SMDetectionData *)getEyesDetectData
{
    if ([PDM_DATA getFarMode])
        return far_eyesDetectData;
    else
        return near_eyesDetectData;
}

- (void)setUserLeftEye_XValue:(CGFloat)val
{
    if (![self getEyesDetectData])
        return;
    
    [[self getEyesDetectData] setUserNewValue:val
                                       forKey:X_VALUE
                                 eyePointType:EYE_POINT_LEFT];
}

- (void)setUserLeftEye_YValue:(CGFloat)val
{
    if (![self getEyesDetectData])
        return;
    
    [[self getEyesDetectData] setUserNewValue:val
                                       forKey:Y_VALUE
                                 eyePointType:EYE_POINT_LEFT];
}

- (void)setUserRightEye_XValue:(CGFloat)val
{
    if (![self getEyesDetectData])
        return;
    
    [[self getEyesDetectData] setUserNewValue:val
                                       forKey:X_VALUE
                                 eyePointType:EYE_POINT_RIGHT];
}

- (void)setUserRightEye_YValue:(CGFloat)val
{
    if (![self getEyesDetectData])
        return;
    
    [[self getEyesDetectData] setUserNewValue:val
                                       forKey:Y_VALUE
                                 eyePointType:EYE_POINT_RIGHT];
}

- (void)setEyePointLeft:(CGPoint)leftPoint
                  right:(CGPoint)rightPoint
{
    if (![APP_MANAGER isMeasurementsAnalyticsEnabled])
        return;
    
    if ([PDM_DATA getFarMode])
        far_eyesDetectData = [SMDetectionData createNewWithType:SMDetectionTypeEyes];
    else
        near_eyesDetectData = [SMDetectionData createNewWithType:SMDetectionTypeEyes];
    
    [[self getEyesDetectData] setSupportType:[PDM_DATA getCurrentSessionSupportType]];
    
    [[self getEyesDetectData] setEyeDetectionType:EYE_DETECTION_FAST];
    
    SMDetectPoint *aLeftPoint = [SMDetectPoint createEyePointWithType:EYE_POINT_LEFT
                                                                    X:leftPoint.x
                                                                    Y:leftPoint.y];
    SMDetectPoint *aRightPoint = [SMDetectPoint createEyePointWithType:EYE_POINT_RIGHT
                                                                     X:rightPoint.x
                                                                     Y:rightPoint.y];
    [[self getEyesDetectData] setDetectedEyePointsLeft:aLeftPoint
                                                 right:aRightPoint];
}

- (void)compareEyePointLeft:(CGPoint)leftPoint
                      right:(CGPoint)rightPoint
{
    if (![self getEyesDetectData])
        return;
    
    SMDetectPoint *aLeftPoint = [SMDetectPoint createEyePointWithType:EYE_POINT_LEFT
                                                                    X:leftPoint.x
                                                                    Y:leftPoint.y];
    SMDetectPoint *aRightPoint = [SMDetectPoint createEyePointWithType:EYE_POINT_RIGHT
                                                                     X:rightPoint.x
                                                                     Y:rightPoint.y];
    
    [[self getEyesDetectData] compareEyePointsLeft:aLeftPoint right:aRightPoint];
}

- (void)setEyePointsWasMoved:(BOOL)moved
{
    if (![self getEyesDetectData])
        return;
    
    if (moved)
        [[self getEyesDetectData] eyePointsIsNeedToBeSent];
}

#pragma mark - Boxes

- (SMDetectionData *)getBoxesDetectData
{
    if ([PDM_DATA getFarMode])
        return far_boxesDetectData;
    else
        return near_boxesDetectData;
}

- (void)setUserBoxesTopLeftPoint:(CGPoint)topLeft
{
    if (![self getBoxesDetectData])
        return;
    
    SMDetectPoint *boxPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_leftBoxingTop
                                                                     X:topLeft.x
                                                                     Y:topLeft.y];
    [[self getBoxesDetectData] setUserNewPoint:boxPoint
                                 forBoxingType:BoxingType_leftBoxingTop];
}

- (void)setUserBoxesTopRightPoint:(CGPoint)topRight
{
    if (![self getBoxesDetectData])
        return;
    
    SMDetectPoint *boxPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_rightBoxingTop
                                                                     X:topRight.x
                                                                     Y:topRight.y];
    [[self getBoxesDetectData] setUserNewPoint:boxPoint
                                 forBoxingType:BoxingType_rightBoxingTop];
}

- (void)setUserBoxesBottomLeftPoint:(CGPoint)bottomLeft
{
    if (![self getBoxesDetectData])
        return;
    
    SMDetectPoint *boxPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_leftBoxingBottom
                                                                     X:bottomLeft.x
                                                                     Y:bottomLeft.y];
    [[self getBoxesDetectData] setUserNewPoint:boxPoint
                                 forBoxingType:BoxingType_leftBoxingBottom];
}

- (void)setUserBoxesBottomRightPoint:(CGPoint)bottomRight
{
    if (![self getBoxesDetectData])
        return;
    
    SMDetectPoint *boxPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_rightBoxingBottom
                                                                     X:bottomRight.x
                                                                     Y:bottomRight.y];
    [[self getBoxesDetectData] setUserNewPoint:boxPoint
                                 forBoxingType:BoxingType_rightBoxingBottom];
}

- (void)setBoxesEyePointLeft:(CGPoint)leftEyePoint
          boxesEyePointRight:(CGPoint)rightEyePoint
{
    if (![APP_MANAGER isMeasurementsAnalyticsEnabled])
        return;
    
    if ([PDM_DATA getFarMode])
        far_boxesDetectData = [SMDetectionData createNewWithType:SMDetectionTypeBoxes];
    else
        near_boxesDetectData = [SMDetectionData createNewWithType:SMDetectionTypeBoxes];

    [[self getBoxesDetectData] setSupportType:[PDM_DATA getCurrentSessionSupportType]];
    
    SMDetectPoint *eyeLeftPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_leftEye
                                                                         X:leftEyePoint.x
                                                                         Y:leftEyePoint.y];
    
    SMDetectPoint *eyeRightPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_rightEye
                                                                          X:rightEyePoint.x
                                                                          Y:rightEyePoint.y];
    
    [[self getBoxesDetectData] setEyeBoxingPointsLeft:eyeLeftPoint
                                                right:eyeRightPoint];
}

- (void)setBoxesSupportPointA:(CGPoint)aPoint
                       pointB:(CGPoint)bPoint
                      pointC1:(CGPoint)c1Point
{
    if (![self getBoxesDetectData])
        return;
    
    SMDetectPoint *aSupportPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_supportAPoint
                                                                          X:aPoint.x
                                                                          Y:aPoint.y];
    SMDetectPoint *bSupportPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_supportBPoint
                                                                          X:bPoint.x
                                                                          Y:bPoint.y];
    SMDetectPoint *c1SupportPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_supportC1Point
                                                                           X:c1Point.x
                                                                           Y:c1Point.y];
    [[self getBoxesDetectData] setBoxingSupportPointsA:aSupportPoint
                                                     B:bSupportPoint
                                                    C1:c1SupportPoint];
}

- (void)setBoxesTopLeft:(CGPoint)topLeft
          boxesTopRight:(CGPoint)topRight
        boxesBottomLeft:(CGPoint)bottomLeft
       boxesBottomRight:(CGPoint)bottomRight
        boxesBridgeLeft:(CGPoint)bridgeLeft
       boxesBridgeRight:(CGPoint)bridgeRight
           boxesExtLeft:(CGPoint)extLeft
          boxesExtRight:(CGPoint)extRight
{
    if (![self getBoxesDetectData])
        return;

    SMDetectPoint *boxingTopLeftPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_leftBoxingTop
                                                                               X:topLeft.x
                                                                               Y:topLeft.y];
    SMDetectPoint *boxingTopRightPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_rightBoxingTop
                                                                                X:topRight.x
                                                                                Y:topRight.y];
    
    SMDetectPoint *boxingBottomLeftPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_leftBoxingBottom
                                                                                  X:bottomLeft.x
                                                                                  Y:bottomLeft.y];
    SMDetectPoint *boxingBottomRightPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_rightBoxingBottom
                                                                                   X:bottomRight.x
                                                                                   Y:bottomRight.y];
    
    SMDetectPoint *boxingBridgeLeftPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_leftBoxingBridge
                                                                                  X:bridgeLeft.x
                                                                                  Y:bridgeLeft.y];
    SMDetectPoint *boxingBridgeRightPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_rightBoxingBridge
                                                                                   X:bridgeRight.x
                                                                                   Y:bridgeRight.y];
    
    SMDetectPoint *boxingExtLeftPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_leftBoxingExt
                                                                               X:extLeft.x
                                                                               Y:extLeft.y];
    SMDetectPoint *boxingExtRightPoint = [SMDetectPoint createBoxingPointWithType:BoxingType_rightBoxingExt
                                                                                X:extRight.x
                                                                                Y:extRight.y];
    [[self getBoxesDetectData] setDetectedBoxingTopPointsLeft:boxingTopLeftPoint
                                         boxingTopPointsright:boxingTopRightPoint
                                       boxingBottomPointsLeft:boxingBottomLeftPoint
                                      boxingBottomPointsRight:boxingBottomRightPoint
                                       boxingBridgePointsLeft:boxingBridgeLeftPoint
                                      boxingBridgePointsRight:boxingBridgeRightPoint
                                          boxingExtPointsLeft:boxingExtLeftPoint
                                         boxingExtPointsRight:boxingExtRightPoint];
}

- (void)boxingIsMoved:(BOOL)moved
{
    if (![self getBoxesDetectData])
        return;
    
    if (moved)
        [[self getBoxesDetectData] boxingIsNeedToBeSent];
}

#pragma mark - Process Data

- (BOOL)isDataNeedToBeSended
{
    if ([self getEyesDetectData] && [[self getEyesDetectData] isNeedBeSended])
        return YES;
    
    if ([self getSupportDetectData] && [[self getSupportDetectData] isNeedBeSended])
        return YES;
    
    if ([self getBoxesDetectData] && [[self getBoxesDetectData] isNeedBeSended])
        return YES;
    
    return NO;
}

- (NSDictionary *)prepareDataDict
{
    NSMutableDictionary *detectData = [NSMutableDictionary new];
    
    detectData[@"uuid"] = uuid;
    
    detectData[@"device"] = [device getDictionary];
    
    detectData[@"photo"] = [detectPhoto getPhotoDict];
    detectData[@"origin_photo"] = [originPhoto getPhotoDict];
    
    detectData[@"angle_deg"] = @(photo_rotate_angle);
    
    if ([self getEyesDetectData])
    {
        detectData[@"eyes_detection"] = [[self getEyesDetectData] getDataDict];
    }
    
    if ([self getSupportDetectData])
    {
        detectData[@"support_detection"] = [[self getSupportDetectData] getDataDict];
    }
    
    if ([self getBoxesDetectData])
    {
        detectData[@"boxes_detection"] = [[self getBoxesDetectData] getDataDict];
    }

    detectData[@"version"] = @(DATASET_VERSION);
    
    detectData[@"timestamp"] = @((uint)[[NSDate date] timeIntervalSince1970]);
    
    return (NSDictionary *)detectData;
}

- (void)processData
{
    if (![self isDataNeedToBeSended])
        return;
    
    __weak __typeof(self)weakSelf = self;

    SMDetectShareOperation *op = [[SMDetectShareOperation alloc] initWithData:[self prepareDataDict]
                                                                  detectPhoto:detectPhoto
                                                                  originPhoto:originPhoto
                                                               isFarOperation:[PDM_DATA getFarMode]
                                                                    doneBlock:^(BOOL success, NSString *reason, BOOL isFar) {

                                                                        NSLog(@"PD ANALYTICS OP: %d : %@", success, reason);

                                                                        if (success)
                                                                            [weakSelf markDataAsWorkedOutIsFar:isFar];
                                                                    }];
    [dataQueue addOperation:op];
}

#pragma mark -

- (void)markDataAsWorkedOutIsFar:(BOOL)isFar
{
    if (isFar)
    {
        if (far_supportDetectData)
            [far_supportDetectData markAsWorkedOut];
        
        if (far_eyesDetectData)
            [far_eyesDetectData markAsWorkedOut];
        
        if (far_boxesDetectData)
            [far_boxesDetectData markAsWorkedOut];
    }
    else
    {
        if (near_supportDetectData)
            [near_supportDetectData markAsWorkedOut];
        
        if (near_eyesDetectData)
            [near_eyesDetectData markAsWorkedOut];
        
        if (near_boxesDetectData)
            [near_boxesDetectData markAsWorkedOut];
    }
}

#pragma mark - Cleanup

- (void)clear
{
    uuid = nil;
    
    photo_rotate_angle = 0;
    
    far_supportDetectData = nil;
    far_eyesDetectData = nil;
    far_boxesDetectData = nil;
    
    near_supportDetectData = nil;
    near_eyesDetectData = nil;
    near_boxesDetectData = nil;
}

@end
