//
//  SMDetectionDevice.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 1/17/18.
//  Copyright © 2018 ACEP. All rights reserved.
//

#import "SMDetectionDevice.h"
#import "PDDeviceInfo.h"

@interface SMDetectionDevice()
{
    NSString *deviceName;
    NSString *modelCode;
}

@end

@implementation SMDetectionDevice

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        PDDeviceInfo *deviceInfo = [PDDeviceInfo new];

        deviceName = deviceInfo.deviceDetails.modelString;

        modelCode = [NSString stringWithFormat:@"%lu.%lu",
                     (unsigned long)deviceInfo.deviceDetails.bigModel,
                     (unsigned long)deviceInfo.deviceDetails.smallModel];
    }
    return self;
}

- (NSDictionary *)getDictionary
{
    return @{ @"model_name" : deviceName,
              @"model_code" : modelCode };
}

@end
