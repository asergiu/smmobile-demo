//
//  SMDetectShareOperation.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 5/17/17.
//  Copyright © 2017 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SMDetectionPhoto;

@interface SMDetectShareOperation : NSOperation

- (id)initWithData:(NSDictionary *)data
       detectPhoto:(SMDetectionPhoto *)photo
       originPhoto:(SMDetectionPhoto *)origin
    isFarOperation:(BOOL)isFar
         doneBlock:(void (^)(BOOL, NSString *, BOOL))block;
@end
