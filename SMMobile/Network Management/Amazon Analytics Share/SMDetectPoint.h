//
//  SMDetectPoint.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 5/16/17.
//  Copyright © 2017 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "detection_types.h"

typedef NS_ENUM (NSInteger, SMDetectionType) {
    SMDetectionTypeSupport,
    SMDetectionTypeEyes,
    SMDetectionTypeBoxes
};

typedef NS_ENUM (NSInteger, BoxingType) {
    BoxingType_leftEye,
    BoxingType_rightEye,
    BoxingType_supportAPoint,
    BoxingType_supportBPoint,
    BoxingType_supportC1Point,
    BoxingType_leftBoxingTop,
    BoxingType_rightBoxingTop,
    BoxingType_leftBoxingBottom,
    BoxingType_rightBoxingBottom,
    BoxingType_leftBoxingBridge,
    BoxingType_rightBoxingBridge,
    BoxingType_leftBoxingExt,
    BoxingType_rightBoxingExt
};

@interface SMDetectPoint : NSObject

@property (nonatomic, assign) double x;
@property (nonatomic, assign) double y;

+ (SMDetectPoint *)createZeroSupportPointWithType:(SUPPORT_POINT_TYPE)type;

+ (SMDetectPoint *)createSupportPointWithType:(SUPPORT_POINT_TYPE)type
                                            X:(double)x
                                            Y:(double)y;

+ (SMDetectPoint *)createZeroEyePointWithType:(EYE_POINT_TYPE)type;

+ (SMDetectPoint *)createEyePointWithType:(EYE_POINT_TYPE)type
                                        X:(double)x
                                        Y:(double)y;

+ (SMDetectPoint *)createZeroBoxingPointWithType:(BoxingType)type;

+ (SMDetectPoint *)createBoxingPointWithType:(BoxingType)type
                                           X:(double)x
                                           Y:(double)y;

- (BOOL)matchPoint:(SMDetectPoint *)point;

- (NSDictionary *)getPointDict;

@end
