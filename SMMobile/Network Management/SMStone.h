//
//  SMStone.h
//
//  Created by Oleg Bogatenko on 10/14/14.
//

#import <Foundation/Foundation.h>

@interface SMStone : NSObject

@property (nonatomic, assign) BOOL multiStone;
@property (nonatomic, assign) BOOL needCheckEmail;

@property (nonatomic, assign) unsigned int authID;
@property (nonatomic, assign) unsigned int expiry;
@property (nonatomic, assign) unsigned int registration;
@property (nonatomic, assign) unsigned int multiType;
@property (nonatomic, assign) unsigned int stoneID;
@property (nonatomic, strong) NSString *stoneSword;
@property (nonatomic, strong) NSString *stoneEmail;

+ (void)saveInUserDefaults:(SMStone *)stone;
+ (NSString *)getCurrentStoneId;
+ (SMStone *)getCurrentStone;
+ (BOOL)storeTypeStone;
+ (BOOL)sessionTypeStone;
+ (void)killCurrentStone;
+ (BOOL)isMultitype;

@end
