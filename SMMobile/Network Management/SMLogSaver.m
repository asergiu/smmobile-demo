//
//  SMCrashReporter.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 7/23/15.
//  Copyright (c) 2015 ACEP. All rights reserved.
//

#import "SMLogSaver.h"

//#define SAVE_DEBUG_LOGS

@interface SMLogSaver ()
{
    NSString *logFileName;
    NSString *logFilePath;
    
    dispatch_queue_t logQueue;
}

@end

@implementation SMLogSaver

+ (instancetype)sharedManager
{
    static dispatch_once_t onceToken = 0;
    static id _sharedManager = nil;
    
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

+ (NSString *)getLogFileName
{
    return [NSString stringWithFormat:@"%@_sessions.log", [[[UIDevice currentDevice] identifierForVendor] UUIDString]];
}

+ (NSString *)getDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

+ (NSString *)getLogFilePath
{
    return [[SMLogSaver getDocumentsDirectory] stringByAppendingPathComponent:[SMLogSaver getLogFileName]];
}

+ (NSDictionary *)getAppInfo
{
    return @{ @"OS version" : [UIDevice currentDevice].systemVersion,
              @"Bundle ID" : [[NSBundle mainBundle] bundleIdentifier],
              @"App version" : [[NSUserDefaults standardUserDefaults] stringForKey:@"currentVersion"],
              @"Device name" : [UIDevice currentDevice].name,
              @"Model" : [UIDevice currentDevice].model,
              @"Localized model" : [UIDevice currentDevice].localizedModel,
              @"Device timezone" : [NSTimeZone localTimeZone].name,
              @"Language" : [NSLocale preferredLanguages][0] };
}

+ (void)removeLogFile
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[SMLogSaver getLogFilePath]])
    {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:[SMLogSaver getLogFilePath] error:&error];
    }
}

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        logFileName = [SMLogSaver getLogFileName];
        logFilePath = [SMLogSaver getLogFilePath];
        
        logQueue = dispatch_queue_create("com.smlogsqueue", NULL);
    }
    return self;
}

- (NSDateFormatter *)timeFormatterForSaveDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat: @"yyyy-MM-dd HH:mm:ss zzz"];
    return formatter;
}

- (NSString *)dateString
{
    NSDateFormatter *formatter = [self timeFormatterForSaveDate];
    
    NSString *timeString = [formatter stringFromDate:[NSDate new]];
    
    return timeString;
}

- (void)saveLogData:(NSDictionary *)dict
{
#ifdef SAVE_DEBUG_LOGS
    
    BOOL firstLog = NO;
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:logFilePath])
    {
        firstLog = YES;
        
        [[NSFileManager defaultManager] createFileAtPath:logFilePath contents:nil attributes:nil];
    }
    
    NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:logFilePath];
    
    NSMutableString *content = [NSMutableString new];
    
    if (firstLog)
    {
        NSDictionary *infoDict = [SMLogSaver getAppInfo];
        
        for (NSString *key in infoDict.allKeys)
        {
            [content appendString:[NSString stringWithFormat:@"%@ : %@\n", key, infoDict[key]]];
        }
    }
    
    for (NSString *key in dict.allKeys)
    {
        [content appendString:[NSString stringWithFormat:@"%@ :: %@ : %@\n", [self dateString], key, dict[key]]];
    }
    
    dispatch_async(logQueue,
                   ^{
                       [fileHandler seekToEndOfFile];
                       [fileHandler writeData:[content dataUsingEncoding:NSUTF8StringEncoding]];
                       [fileHandler closeFile];
                   });
#endif
}

@end
