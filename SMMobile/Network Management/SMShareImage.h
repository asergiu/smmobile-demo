//
//  SMShareImage.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 3/3/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMShareImage : NSObject

@property (nonatomic, strong) NSURL *fileURL;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSURL *presignedURL;

@property (nonatomic, assign) BOOL success;
@property (nonatomic, strong) NSString *errorMessage;

@end
