//
//  SMAmazonShareHelper.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 3/3/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import "SMAmazonShareHelper.h"
#import <AWSCore/AWSCore.h>
#import <AWSS3/AWSS3.h>
#import "SMAmazonConfig.h"
#import "SMNetworkManager.h"
#import "SMShareOperation.h"

@interface SMAmazonShareHelper ()
{
    NSMutableArray *shareImages;
    NSMutableArray *resultImages;
    
    NSOperationQueue *uploadsQueue;
    
    void (^_uploadCompletion)(BOOL, NSArray *);
    
    BOOL cancelledAll;
}

@end

@implementation SMAmazonShareHelper

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        AWSStaticCredentialsProvider *credentialsProvider = [[AWSStaticCredentialsProvider alloc] initWithAccessKey:kS3AccessKey
                                                                                                          secretKey:kS3SecretKey];
        
        AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionEUWest1
                                                                             credentialsProvider:credentialsProvider];
        
        [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
        
        [self createTempDirectory];
        
        uploadsQueue = [NSOperationQueue new];
        uploadsQueue.maxConcurrentOperationCount = 4;
    }
    return self;
}

- (void)uploadImages:(NSArray *)images
      withCompletion:(void (^)(BOOL, NSArray *))completion
{
    _uploadCompletion = [completion copy];
    
    if (![NETWORK_MANAGER networkAvaliable])
    {
        _uploadCompletion(NO, nil);
        
        return;
    }
    
    cancelledAll = NO;
    
    [self saveInTempDirectoryImages:images];
    
    resultImages = [NSMutableArray new];
    
    for (SMShareImage *img in shareImages)
    {
        SMShareOperation *operation = [[SMShareOperation alloc] initWithImage:img doneBlock:^(BOOL success, SMShareImage *image) {
            
            if (success)
                [resultImages addObject:image];
            
            [self checkCompletion];
        }];
        
        [uploadsQueue addOperation:operation];
    }
}

- (void)cancel
{
    [uploadsQueue cancelAllOperations];
    
    cancelledAll = YES;
}

#pragma mark - Temp Data

- (void)saveInTempDirectoryImages:(NSArray *)images
{
    [self clearTempDirectory];

    shareImages = [NSMutableArray new];
    
    for (UIImage *img in images)
    {
        SMShareImage *shareImage = [SMShareImage new];
        
        NSString *imageName = [self generateFileName];
        shareImage.fileURL = [self saveTempImageData:UIImageJPEGRepresentation(img, 0.8) name:imageName];
        shareImage.name = imageName;
        
        [shareImages addObject:shareImage];
    }
}

- (NSString *)tempDirectoryPath
{
    return [NSTemporaryDirectory() stringByAppendingPathComponent:@"fs_share"];
}

- (void)createTempDirectory
{
    BOOL isDirectory = NO;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self tempDirectoryPath] isDirectory:&isDirectory])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:[self tempDirectoryPath]
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }
}

- (void)clearTempDirectory
{
    NSError *error = nil;
    
    NSArray *tempContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[self tempDirectoryPath]
                                                                               error:&error];
    if (error)
        return;
    
    for (NSString *file in tempContent)
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@", [self tempDirectoryPath], file]
                                                   error:&error];
}

- (NSString *)generateFileName
{
    return [NSString stringWithFormat:@"%@.jpg", [[NSUUID UUID] UUIDString]];
}

- (NSURL *)saveTempImageData:(NSData *)data name:(NSString *)name
{
    NSString *path = [[self tempDirectoryPath] stringByAppendingPathComponent:name];
    
    [data writeToFile:path atomically:YES];
    
    return [[NSURL alloc] initFileURLWithPath:path];
}

- (void)checkCompletion
{
    if (!uploadsQueue.operationCount)
    {
        if (cancelledAll)
            _uploadCompletion(NO, nil);
        else
            _uploadCompletion(YES, resultImages);
    }
}

- (void)dealloc
{
    [self cancel];
}

@end
