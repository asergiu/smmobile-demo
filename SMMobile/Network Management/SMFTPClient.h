//
//  SMFTPClient.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 2/13/15.
//

#import <Foundation/Foundation.h>

@protocol SMFTPClientDelegate <NSObject>

@optional
- (void)successSended;
- (void)stopWithError:(NSString *)error;
@end

@interface SMFTPClient : NSObject

@property (nonatomic, assign) id <SMFTPClientDelegate> delegate;

@property (nonatomic, strong) NSString *filePath;

- (instancetype)initWithHost:(NSString *)host
                        name:(NSString *)name
                    password:(NSString *)password;

- (void)send;

@end
