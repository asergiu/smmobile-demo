//
//  SMShareOperation.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 3/4/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import "SMShareOperation.h"
#import "SMAmazonConfig.h"
#import <AWSCore/AWSCore.h>
#import <AWSS3/AWSS3.h>

@interface SMShareOperation ()
{
    SMShareImage *currentImage;
    
    void (^_handler)(BOOL success, SMShareImage *image);
    
    BOOL finished;
    BOOL executing;
    
    AWSS3TransferManager *transferManager;
    AWSTask *getURLTask;
}

@end

@implementation SMShareOperation

static uint const kPreSignInterval = 3600; // 1 hour

- (id)initWithImage:(SMShareImage *)image
          doneBlock:(void (^)(BOOL, SMShareImage *))block
{
    self = [super init];
    if (self)
    {
        _handler = [block copy];
        
        currentImage = image;
        
        executing = NO;
        finished = NO;
    }
    return self;
}

- (BOOL)isAsynchronous
{
    return YES;
}

- (BOOL)isConcurrent
{
    return YES;
}

- (BOOL)isExecuting
{
    return executing;
}

- (BOOL)isFinished
{
    return finished;
}

- (void)start
{    
    [self willChangeValueForKey:@"isExecuting"];
    executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
    [self sendImage];
}

- (void)sendImage
{
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    
    uploadRequest.bucket = [self sharedPicturesBucket];
    uploadRequest.ACL = AWSS3ObjectCannedACLPrivate;
    uploadRequest.key = currentImage.name;
    uploadRequest.contentType = @"image/jpeg";
    uploadRequest.body = currentImage.fileURL;
    
    uploadRequest.uploadProgress =^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
        });
    };
    
    transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    [[transferManager upload:uploadRequest] continueWithBlock:^id(AWSTask *task) {
        
        if (task.error || [self isCancelled])
        {
            currentImage.errorMessage = [task.error localizedDescription];
            currentImage.success = NO;
            
            [self completeOperation];
            
            _handler(NO, currentImage);
        }
        else
            [self getPreSignedURLForImage];
        
        return nil;
    }];
}

- (void)getPreSignedURLForImage
{
    AWSS3GetPreSignedURLRequest *getPreSignedURLRequest = [AWSS3GetPreSignedURLRequest new];
    
    getPreSignedURLRequest.bucket = [self sharedPicturesBucket];
    getPreSignedURLRequest.key = currentImage.name;
    getPreSignedURLRequest.HTTPMethod = AWSHTTPMethodGET;
    getPreSignedURLRequest.expires = [NSDate dateWithTimeIntervalSinceNow:kPreSignInterval];
    
    getURLTask = [[AWSS3PreSignedURLBuilder defaultS3PreSignedURLBuilder] getPreSignedURL:getPreSignedURLRequest];
                  
    [getURLTask continueWithBlock:^id(AWSTask *task) {
         
         if (task.error || [self isCancelled])
         {
             currentImage.success = NO;
             currentImage.errorMessage = [task.error localizedDescription];
         }
         else
         {
             currentImage.presignedURL = task.result;
             currentImage.success = YES;
         }
         
         [self completeOperation];
         
         _handler(currentImage.success, currentImage);
         
         return nil;
     }];
}

- (void)completeOperation
{
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    
    executing = NO;
    finished = YES;
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

- (void)cancel
{
    [super cancel];
    
    if (transferManager)
        [transferManager cancelAll];
}

#pragma mark - Common Stuff

- (NSString *)sharedPicturesBucket
{
    return kS3ShareBucket;
}

@end
