//
//  SMNetworkManager.m
//
//  Created by Oleg Bogatenko on 10/7/14.
//

#import "SMNetworkManager.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "NSString+Hash.h"
#import "Reachability.h"
#import "SMFTPClient.h"
#import "NSString+Hash.h"
#import "SMLogSaver.h"
#import "SMAppManager.h"
#import "SMStoreClient.h"

#define kSendNewEmailTag 100
#define kConfirmEmailTag 101
#define kConfirmStep2Tag 102
#define kConfirmStepLast 103

@interface SMNetworkManager () <SMFTPClientDelegate, UIAlertViewDelegate, NSXMLParserDelegate>
{
    Reachability *internetReachable;
    BOOL _upNetwork;
    
    UIView *blockView;
    UILabel *messageLabel;
    BOOL blocked;
    NSTimer *timer;
    
    int stoneType;
    
    UIBackgroundTaskIdentifier backgroundTask;
    
    SMFTPClient *client;
    NSMutableArray *ftpQueue;
    
    NSString *ftp_address;
    NSString *ftp_login;
    NSString *ftp_password;
    
    BOOL isNewEmail;
    
    void (^_sendCompletion)(BOOL);
    
    void (^_soapCompletion)(BOOL status, NSDictionary *data, NSString *message);
    
    NSXMLParser *xmlParser;
    NSString *currentKey;
    NSMutableDictionary *serverResponse;
    
    SMStoreClient *currentClient;
}

@property (strong, nonatomic) NSString *macString;
@property (strong, nonatomic) SMStone *stone;

@end

@implementation SMNetworkManager

static double const timerInterval = 1200.f;

// Period to check email confirmation
static uint const emailCheckInterval = 1209600; // 14 days in unixtime

@synthesize stone;
@synthesize macString;
@synthesize currentModule;

+ (instancetype)sharedManager
{
    static dispatch_once_t onceToken = 0;
    static id _sharedManager = nil;
    
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] initWithBaseURL:[NSURL URLWithString:SERVER_URL]];
    });
    
    return _sharedManager;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self)
    {
        [self setDefaultSerializers];
        
        blocked = NO;
        isNewEmail = NO;
        
        if (!stone)
            stone = [SMStone new];
        
        currentModule = SMModuleUnknown;
        
        internetReachable = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [internetReachable currentReachabilityStatus];
        
        _upNetwork = (networkStatus == ReachableViaWiFi || networkStatus == ReachableViaWWAN);
        
        [internetReachable startNotifier];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(checkNetworkStatus:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(checkNetworkStatus:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(enteredBackground)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(enteredInactive)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
    }
    return self;
}

- (void)setDefaultSerializers
{
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [self.requestSerializer clearAuthorizationHeader];
}

#pragma mark - Reachability Check Network

- (void)checkNetworkStatus:(NSNotification *)notice
{
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    
    switch (internetStatus)
    {
        case NotReachable:
        {
            NSLog(@"Internet is down...");
            
            if (!blocked && [SMStone storeTypeStone])
                [self blockAppWithSession:NO];
            
            _upNetwork = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"Internet is working via WIFI!");
            _upNetwork = YES;
            
            [self checkMacAdress];
            
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"Internet is working via WWAN...");
            
            if (!blocked && [SMStone storeTypeStone])
                [self blockAppWithSession:NO];
            
            _upNetwork = YES;
            break;
        }
    }
}

- (void)startCheck
{
    if (stone)
    {
        [self checkNetworkStatus:nil];
    }
}

#pragma mark Timer

- (void)startTimer
{
    if (!timer && currentModule)
    {
        timer = [NSTimer scheduledTimerWithTimeInterval:timerInterval
                                                 target:self
                                               selector:@selector(checkTime)
                                               userInfo:nil
                                                repeats:YES];
    }
}

- (void)checkTime
{
    [self checkMacAdress];
}

- (void)stopTimer
{
    [timer invalidate];
    timer = nil;
}

- (BOOL)networkAvaliable
{
    return _upNetwork;
}

#pragma mark Check MAC Address

- (void)checkMacAdress
{
    if (![SMStone isMultitype])
    {
        [self unblockApp];
        
        return;
    }
    
    [self setDefaultSerializers];
    
    NSString *url = [NSString stringWithFormat:@"/hwCheck?license_id=%u&mac=%@", stone.stoneID, [SMNetworkManager currentRouterMacAddress]];
    
    [self GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (responseObject && [[responseObject objectForKey:@"error"] intValue] == 0)
        {
            NSLog(@"Recieved data without error: %@", responseObject);
            
            if (currentModule && [SMStone sessionTypeStone])
                [self sendAsyncDeviceStatus:YES forModule:currentModule returnCompletion:NO];
            else
                [self unblockApp];
        }
        else
        {
            NSLog(@"Recieved data with error: %@", responseObject);
            
            if (!blocked)
                [self blockAppWithSession:NO];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"Recieved error: %@", [error localizedDescription]);
        
        if (!blocked)
        {
            [self blockAppWithSession:NO];
            
            [self performSelector:@selector(checkNetworkStatus:) withObject:nil afterDelay:2.0];
        }
    }];
}

#pragma mark Device Sleep Task

- (void)enteredInactive
{
    if ([SMStone sessionTypeStone] && currentModule != SMModuleUnknown)
    {
        if (!blocked)
            [self blockAppWithSession:YES];
        
        [self stopTimer];
        [self sendAsyncDeviceStatus:NO forModule:currentModule returnCompletion:NO];
    }
}

#pragma mark Background Task

- (void)enteredBackground
{
    if ([SMStone sessionTypeStone] && currentModule != SMModuleUnknown)
    {
        if (!blocked)
            [self blockAppWithSession:YES];
        
        [self stopTimer];
        
        UIApplication *app = [UIApplication sharedApplication];
        
        backgroundTask = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:backgroundTask];
            backgroundTask = UIBackgroundTaskInvalid;
        }];
        
        [self sendAsyncDeviceStatus:NO forModule:currentModule returnCompletion:NO];
    }
}

#pragma mark Multistone requests

- (void)resumeSession
{
    [self enterModule:currentModule withFillCompletion:^(BOOL state, NSString *reason) {
        
        if (state)
            [self unblockApp];
        else
            [messageLabel setText:NSLocalizedString(reason, @"")];
    }];
}

- (void)enterAfterSwordInModule:(SMModule)module
{
    currentModule = module;
    
    if (!stone)
        stone = [SMStone getCurrentStone];
}

- (void)exitModule
{
    [self sendAsyncDeviceStatus:NO forModule:currentModule returnCompletion:NO];
    
    [self stopTimer];
    
    currentModule = SMModuleUnknown;
}

- (void)enterModule:(SMModule)module withFillCompletion:(void (^)(BOOL status, NSString *reason))completion
{
    _requestCompletion = [completion copy];
    
    [self sendAsyncDeviceStatus:YES forModule:module returnCompletion:YES];
}

- (void)sendAsyncDeviceStatus:(BOOL)active
                    forModule:(SMModule)module
             returnCompletion:(BOOL)completion
{
    [self setDefaultSerializers];
    
    currentModule = module;
    
    NSString *deviceString = [SMNetworkManager getDeviceIdentifier];
    
    NSString *token = [SMNetworkManager generateToken];
    
    NSDictionary *params = @{ @"uuid" : deviceString,
                              @"license_id" : @(stone.stoneID),
                              @"activate" : [NSString stringWithFormat:@"%d", active],
                              @"token" : token };
    
    [self POST:@"/multiSession" parameters:params constructingBodyWithBlock:nil
      progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          
          if ([[responseObject objectForKey:@"error"] boolValue]) {
              
              NSLog(@"Recieved one session data with error: %@", responseObject);
              
              if ([[responseObject objectForKey:@"status"] intValue] == SMDeviceExistsBlocked)
              {
                  NSLog(@"Device with UUID: %@ blocked on server!", deviceString);
                  
                  [SMStone killCurrentStone];
                  
                  [self cleanCurrentStone];
              }
              
              if (completion)
                  _requestCompletion(NO, [SMNetworkManager getMessageForStatus:[[responseObject objectForKey:@"status"] intValue]]);
              else
              {
                  if ([[responseObject objectForKey:@"status"] intValue] != SMDeviceExistsBlocked)
                  {
                      [self blockAppWithSession:YES];
                  }
              }
              
          } else {
              
              NSLog(@"Recieved one session data without error: %@", responseObject);
              
              if (completion)
              {
                  _requestCompletion([self checkToken:token withBackToken:[responseObject objectForKey:@"token"]], @"");
                  
                  [self startTimer];
              }
              else
                  [self unblockApp];
          }
          
      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          
          if (completion)
              _requestCompletion(NO, NSLocalizedString(@"Server connection error!", @""));
          else
          {
              if (!blocked)
                  [self blockAppWithSession:NO];
              
              [self performSelector:@selector(checkNetworkStatus:) withObject:nil afterDelay:2.f];
          }
      }];
}

#pragma mark Lock & Unlock App

- (void)blockAppWithSession:(BOOL)session
{
    if (timer)
    {
        [self stopTimer];
    }
    
    UIView *currentView = [[[[UIApplication sharedApplication] keyWindow] subviews] lastObject];
    
    blockView = [[UIView alloc] initWithFrame:currentView.bounds];
    
    blockView.backgroundColor = [UIColor blackColor];
    blockView.alpha = 0.7f;
    
    float width = currentView.bounds.size.width;
    float height = currentView.bounds.size.height;
    
    NSString *message;
    
    if (session) {
        message = NSLocalizedString(@"Working session for this device is suspended. To resume, press button.", @"");
    } else {
        message = NSLocalizedString(@"Please, come back to the store or check your Wi-Fi connection!", @"");
    }
    
    messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(width/2 - 450.f, height/2 - 50.f, 900.f, 100.f)];
    
    [messageLabel setText:message];
    [messageLabel setTextColor:[UIColor whiteColor]];
    [messageLabel setTextAlignment:NSTextAlignmentCenter];
    [messageLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [messageLabel setNumberOfLines:2];
    [messageLabel setFont:[UIFont systemFontOfSize:27.f]];
    
    [blockView addSubview:messageLabel];
    
    if (session)
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        button.frame = CGRectMake(width/2 - 67.f, height/2 + 40.f, 134.f, 38.f);
        
        [button setBackgroundImage:[UIImage imageNamed:@"PD_btn_Edit"] forState:UIControlStateNormal];
        [button setTitle:NSLocalizedString(@"Resume", @"") forState:UIControlStateNormal];
        [button addTarget:self action:@selector(resumeSession) forControlEvents:UIControlEventTouchUpInside];
        
        [blockView addSubview:button];
    }
    
    [currentView addSubview:blockView];
    
    blocked = YES;
}

- (void)unblockApp
{
    if (blockView)
    {
        [blockView removeFromSuperview];
        
        blockView = nil;
        
        blocked = NO;
        
        if ([SMStone sessionTypeStone])
        {
            [self startTimer];
        }
    }
}

#pragma mark - Authorization Process

- (void)swordWithName:(NSString *)name
             princess:(NSString *)princess
                email:(NSString *)email
               module:(SMModule)module
       fillCompletion:(void (^)(BOOL status, NSString *reason))completion
{
    [self setDefaultSerializers];
    
    _requestCompletion = [completion copy];
    
    macString = [SMNetworkManager currentRouterMacAddress];
    
    currentModule = module;
    
    NSString *url = [NSString stringWithFormat:@"/account?login=%@&password=%@", name, princess];
    
    [self GET:url parameters:nil
     progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         [self log:responseObject];
         
         if ([[responseObject objectForKey:@"error"] intValue] == 0)
         {
             stone.stoneSword = name;
             stone.stoneEmail = email;
             
             [self softRequest];
         }
         else
         {
             _requestCompletion(NO, [SMNetworkManager getMessageForStatus:SMAccessDenied]);
         }
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         _requestCompletion(NO, [SMNetworkManager getMessageForStatus:SMUnknownError]);
     }];
}

#pragma mark Request (/softRequest)

- (void)softRequest
{
    [self setDefaultSerializers];
    
    [self GET:@"/softRequest" parameters:nil
     progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         [self log:responseObject];
         
         if ([[responseObject objectForKey:@"error"] intValue] == 0)
         {
             if ([self checkSoftwareEqual:[responseObject objectForKey:@"bundle"]])
             {
                 [self getStoneDateStatus];
             }
             else
             {
                 [self resetSession];
                 _requestCompletion(NO, [SMNetworkManager getMessageForStatus:SMWrongSoftware]);
             }
         }
         else
         {
             [self resetSession];
             _requestCompletion(NO, [SMNetworkManager getMessageForStatus:SMWrongSoftware]);
         }
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         
        _requestCompletion(NO, [SMNetworkManager getMessageForStatus:SMUnknownError]);
     }];
}

#pragma mark Request (/checkExpirationDate)

- (void)getStoneDateStatus
{
    [self setDefaultSerializers];
    
    [self GET:@"/checkExpirationDate" parameters:nil
     progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         [self log:responseObject];
         
         if ([[responseObject objectForKey:@"error"] intValue] == 0)
         {
             stone.expiry = [[responseObject objectForKey:@"date"] intValue];
             
             [self getStoneType];
         }
         else
         {
             [self resetSession];
             _requestCompletion(NO, [SMNetworkManager getMessageForStatus:SMStoneExpired]);
         }
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         
         _requestCompletion(NO, [SMNetworkManager getMessageForStatus:SMUnknownError]);
     }];
}

#pragma mark Request (/getStoneType)

- (void)getStoneType
{
    [self setDefaultSerializers];
    
    [self GET:@"/getLicenseType" parameters:nil
     progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         [self log:responseObject];
         
         stoneType = [[responseObject objectForKey:@"type"] intValue];
         
         if (currentModule == SMModulePD && stoneType == 1)
         {
             [self resetSession];
             
             _requestCompletion(NO, [SMNetworkManager getMessageForStatus:SMAccessDenied]);
         }
         else
             [self multistoneRequest];
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         
         _requestCompletion(NO, [SMNetworkManager getMessageForStatus:SMUnknownError]);
     }];
}

#pragma mark Request (/multistone?mac=%@&uuid=%@)

- (void)multistoneRequest
{
    [self setDefaultSerializers];
    
    NSString *url = [NSString stringWithFormat:@"/multilicense?mac=%@&uuid=%@", macString, [SMNetworkManager getDeviceIdentifier]];
    
    [self GET:url parameters:nil
     progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         [self log:responseObject];
         
         if ([[responseObject objectForKey:@"error"] intValue] == 1)
         {
             [self resetSession];
             _requestCompletion(NO, [SMNetworkManager getMessageForStatus:[[responseObject objectForKey:@"status"] intValue]]);
         }
         else
         {
             if ([[responseObject objectForKey:@"status"] intValue] == 14)
             {
                 stone.multiStone = NO;
             }
             else
             {
                 stone.multiStone = YES;
                 stone.multiType = [[responseObject objectForKey:@"type"] intValue];
                 stone.stoneID = [[responseObject objectForKey:@"license_id"] intValue];
             }
             
             [self uniqueIdentificatorRequest];
         }
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         
         _requestCompletion(NO, [SMNetworkManager getMessageForStatus:SMUnknownError]);
     }];
}

#pragma mark Request (/checkAuthorization?uuid=%@)

- (void)uniqueIdentificatorRequest
{
    [self setDefaultSerializers];
    
    NSString *url = [NSString stringWithFormat:@"/checkAuthorization?uuid=%@&os=%@&email=%@", [SMNetworkManager getDeviceIdentifier],
                                                                                              [SMNetworkManager getOSVesrion],
                                                                                              stone.stoneEmail];
    [self GET:url parameters:nil
     progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         [self log:responseObject];
         
         if ([[responseObject objectForKey:@"error"] intValue] == 1)
         {
             [self resetSession];
             _requestCompletion(NO, [SMNetworkManager getMessageForStatus:[[responseObject objectForKey:@"status"] intValue]]);
         }
         else
         {
             stone.authID = [[responseObject objectForKey:@"auth_id"] intValue];
             
             stone.registration = (uint)[[NSDate date] timeIntervalSince1970];
             stone.needCheckEmail = YES;
             
             [self saveAuthenticationKey];
             
             _requestCompletion(YES, nil);
             
             [self resetSession];
         }
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         
         _requestCompletion(NO, [SMNetworkManager getMessageForStatus:SMUnknownError]);
     }];
}

#pragma mark Request (/resetSession)

- (void)resetSession
{
    [self setDefaultSerializers];
    
    [self GET:@"/resetSession" parameters:nil
     progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         NSLog(@"responseObject=%@", responseObject);
         
     } failure:nil];
}

#pragma mark - Email Check

- (void)sendEmailRequestWithCompletion:(void (^)(BOOL status, NSString *reason))completion
{
    [self setDefaultSerializers];
    
    _requestCompletion = [completion copy];
    
    NSString *email   = [SMNetworkManager getEmailAddress];
    NSString *auth_id = [NSString stringWithFormat:@"%d", stone.authID];
    NSString *token   = [[SMNetworkManager getDeviceIdentifier] MD5];
    
    NSDictionary *params = @{ @"auth_id" : auth_id,
                                @"email" : email,
                                @"token" : token };
    
    [self POST:@"/changeAuthEmail" parameters:params
      progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          
          if ([[responseObject objectForKey:@"error"] intValue] == 1)
          {
              _requestCompletion(NO, [SMNetworkManager getMessageForStatus:[[responseObject objectForKey:@"status"] intValue]]);
          }
          else
          {
              if ([[responseObject objectForKey:@"status"] intValue] == SMEmailChanged)
              {
                  _requestCompletion(YES, [SMNetworkManager getMessageForStatus:[[responseObject objectForKey:@"status"] intValue]]);
              }
          }
          
      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          _requestCompletion(NO, [SMNetworkManager getMessageForStatus:SMUnknownError]);
      }];
}

- (void)isEmailConfirmed:(void (^)(BOOL status, NSString *reason))completion
{
    [self setDefaultSerializers];
    
    _requestCompletion = [completion copy];
    
    NSString *auth_id = [NSString stringWithFormat:@"%d", stone.authID];
    NSString *token   = [[SMNetworkManager getDeviceIdentifier] MD5];
    
    NSDictionary *params = @{ @"auth_id" : auth_id,
                                @"token" : token };
    
    [self POST:@"/isConfirmedEmail" parameters:params
      progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          
          if ([[responseObject objectForKey:@"error"] intValue] == 1)
          {
              _requestCompletion(NO, [SMNetworkManager getMessageForStatus:[[responseObject objectForKey:@"status"] intValue]]);
          }
          else
          {
              if ([[responseObject objectForKey:@"status"] intValue] == SMEmailConfirmed)
              {
                  _requestCompletion(YES, [SMNetworkManager getMessageForStatus:[[responseObject objectForKey:@"status"] intValue]]);
              }
              
              if ([[responseObject objectForKey:@"status"] intValue] == SMEmailNotConfirmed)
              {
                  _requestCompletion(NO, [SMNetworkManager getMessageForStatus:[[responseObject objectForKey:@"status"] intValue]]);
              }
          }
          
      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          _requestCompletion(NO, [SMNetworkManager getMessageForStatus:SMUnknownError]);
      }];
}

#pragma mark - Silent Check

- (void)getBlockTag
{
    if (![self isAuthenticated])
    {
        return;
    }
    
    if (![self isCheckNeeded])
    {
        return;
    }
    
    [self setDefaultSerializers];
    
    NSString *deviceString = [SMNetworkManager getDeviceIdentifier];
    
    NSString *urlString;
    
    if (stone)
    {
        urlString = [NSString stringWithFormat:@"/silentCheck?uuid=%@&bundle=%@&auth_id=%d", deviceString, [SMNetworkManager getBundleId], stone.authID];
    }
    else
        urlString = [NSString stringWithFormat:@"/silentCheck?uuid=%@&bundle=%@", deviceString, [SMNetworkManager getBundleId]];
    
    [self GET:urlString parameters:nil
     progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         if (responseObject && [[responseObject objectForKey:@"tag"] isEqual:@"block"])
         {
             [self removeAuthorizationKeys];
         }
         
         [self setCheckDate];
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"Error: %@", error.description);
     }];
}

#pragma mark Router MAC Addr

+ (NSString *)currentRouterMacAddress
{
    NSString *bssid = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    
#ifdef TARGET_IPHONE_SIMULATOR
    bssid = @"00:00:00:00:00:00";
#endif
    
    for (NSString *ifnam in ifs)
    {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        
        if (info[@"BSSID"])
        {
            bssid = info[@"BSSID"];
        }
    }
    
    return bssid;
}

#pragma mark - Get Localized Status

+ (NSString *)getMessageForStatus:(int)status
{
    switch (status)
    {
        case SMAccessDenied:
        case SMWrongSoftware:
        {
            return NSLocalizedString(@"Login or password is not correct!", @"");
        }
            break;
        case SMStoneExpired:
        {
            return NSLocalizedString(@"Sorry! Your license has expired!", @"");
        }
            break;
        case SMStoneHasExpired:
        {
            return NSLocalizedString(@"Sorry! Your license has expired!", @"");
        }
            break;
        case SMToManyDevices:
        {
            return NSLocalizedString(@"Too many authorizations for license!", @"");
        }
            break;
        case SMWrongParams:
        {
            return NSLocalizedString(@"Multi-license error: incorrect network!", @"");
        }
            break;
        case SMDeviceExistsBlocked:
        {
            return NSLocalizedString(@"Your license is not authorized", @"");
        }
            break;
        case SMToManyInSession:
        {
            return NSLocalizedString(@"Permitted number of devices exceeded! Please retry after finishing use on the other device(s)", @"");
        }
            break;
        case SMEmailChanged:
        {
            return NSLocalizedString(@"Mail has been changed.", @"");
        }
            break;
        case SMEmailConfirmed:
        {
            return NSLocalizedString(@"Mail has been confirmed.", @"");
        }
            break;
        case SMEmailNotConfirmed:
        {
            return NSLocalizedString(@"Mail is not confirmed!", @"");
        }
            break;
        case SMUnknownError:
        {
            return NSLocalizedString(@"License server error!", @"");
        }
            break;
        default:
        {
            return NSLocalizedString(@"License server error!", @"");
        }
            break;
    }
    return nil;
}

#pragma mark - Current stone

- (BOOL)loadCurrentStone
{
    if ([self isAuthorizedCompletely] || [self isAuthorizedSMOnly])
    {
        stone = [SMStone getCurrentStone];
        
        if (stone)
        {
            NSLog(@"Loaded stone: %@", stone);
            
            return YES;
        }
    }
    
    return NO;
}

- (void)cleanCurrentStone
{
    stone = nil;
}

- (void)fillCurrentWithStone:(SMStone *)aStone
{
    stone = aStone;
}

- (void)rewriteCurrentStone
{
    [SMStone saveInUserDefaults:stone];
}

#pragma mark - Save Authentication Methods

- (BOOL)isAuthenticatedForModule:(SMModule)module
{    
    if (module == SMModulePD && [self isAuthorizedCompletely])
    {
        return YES;
    }
    
    if (module != SMModulePD && ([self isAuthorizedCompletely] || [self isAuthorizedSMOnly]))
    {
        return YES;
    }
    
    return NO;
}

- (BOOL)isAuthenticated
{
    return ([self isAuthorizedCompletely] || [self isAuthorizedSMOnly]);
}

- (BOOL)isAuthorizedCompletely
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"pdKeySuccessful"];
}

- (BOOL)isAuthorizedSMOnly
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"smKeySuccessful"];
}

- (float)getLastCheckDate
{
    return [[NSUserDefaults standardUserDefaults] floatForKey:@"checkDate"];
}

- (float)getUnixDate
{
    NSDate *currentDate = [NSDate date];
    
    return [currentDate timeIntervalSince1970];
}

- (BOOL)isCheckNeeded
{
    return [self getUnixDate] > ([self getLastCheckDate] + 604800)  ? YES : NO;
}

- (void)setCheckDate
{
    float today = [self getUnixDate];
    
    [[NSUserDefaults standardUserDefaults] setFloat:today forKey:@"checkDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)saveAuthenticationKey
{
    [[NSUserDefaults standardUserDefaults] setObject:stone.stoneEmail forKey:@"user_email"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:(stoneType == 2 ? @"pdKeySuccessful" : @"smKeySuccessful")];
    [[NSUserDefaults standardUserDefaults] setBool:stone.multiStone forKey:@"multistone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [SMStone saveInUserDefaults:stone];
}

- (void)removeAuthorizationKeys
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"pdKeySuccessful"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"smKeySuccessful"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"multistone"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"stone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Expiration

- (void)checkExpiry
{
    if ([self isAuthenticated])
    {
        NSDate *todayNow = [NSDate date];
        
        int unixtimeNow = [todayNow timeIntervalSince1970];
        
        uint expiryDate = [self getExpiration];
        
        if (unixtimeNow > expiryDate && expiryDate != 0)
        {
            [self cleanCurrentStone];
            
            [SMStone killCurrentStone];
        }
    }
}

- (uint)getExpiration
{
    if ([self isAuthenticated])
    {
        return stone.expiry;
    }
    
    return 0;
}

#pragma mark - Email Methods

- (BOOL)isEmailCheckNeed
{
    
/* Commented 31.08.2015 (work without necessarily email confirmation) */

//    if (!stone.needCheckEmail)
//    {
//        return NO;
//    }
//    
//    uint unixtimeNow = (uint)[[NSDate date] timeIntervalSince1970];
//    
//    return ((stone.registration + emailCheckInterval) <= unixtimeNow);
    
    return NO;
}

- (void)checkEmailConfirmationWithAlert:(BOOL)withAlert
{
    if ([self isAuthenticated] && stone)
    {
        if (stone.needCheckEmail)
        {
            [self isEmailConfirmed:^(BOOL status, NSString *reason) {
                
                if (status)
                {
                    stone.needCheckEmail = NO;
                    
                    [SMStone saveInUserDefaults:stone];
                }
                else
                {
                    if (withAlert)
                        [self showEmailNotConfirmedYet];
                }
            }];
        }
    }
}

- (void)checkEmail
{
    if ([self isAuthenticated] && stone)
    {
        if (![[SMNetworkManager getEmailAddress] isEqualToString:stone.stoneEmail]
            && [self isValidEmail:[SMNetworkManager getEmailAddress]])
        {
            isNewEmail = YES;
            
            [self sendEmailConfirmationRequest];
        }
        else
            [self checkEmailConfirmationWithAlert:NO];
    }
}

- (void)setNewEmail:(NSString *)email
{
    if (email)
    {
        [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"user_email"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (NSString *)getEmailAddress
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"user_email"];
}

- (BOOL)isValidEmail:(NSString *)emailString
{
    if ([emailString stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0)
    {
        return NO;
    }
    
    NSString *regExpPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExpPattern
                                                                      options:NSRegularExpressionCaseInsensitive
                                                                        error:nil];
    
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString
                                                     options:0
                                                       range:NSMakeRange(0, [emailString length])];
    
    return regExMatches;
}

- (void)sendEmailConfirmationRequest
{
    [self setDefaultSerializers];
    
    [self sendEmailRequestWithCompletion:^(BOOL status, NSString *reason) {
        
        if (status)
        {
            if (!stone.needCheckEmail)
            {
                stone.registration = (uint)[[NSDate date] timeIntervalSince1970];
            }
            
            stone.stoneEmail = [SMNetworkManager getEmailAddress];
            stone.needCheckEmail = YES;
            
            dispatch_async(dispatch_get_main_queue(), ^{

                NSString *message;
                
                if (isNewEmail)
                {
                    message = [NSString stringWithFormat:NSLocalizedString(@"Please confirm your new e-mail. Confirmation e-mail sent to %@. Please check your mail box.", nil), [SMNetworkManager getEmailAddress]];
                }
                else
                {
                    message = [NSString stringWithFormat:NSLocalizedString(@"Confirmation e-mail sent to %@. Please check your mail box.", nil), [SMNetworkManager getEmailAddress]];
                }
                
                isNewEmail = NO;
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                message:message
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                      otherButtonTitles:nil];
                alert.tag = kConfirmStepLast;
                
                [alert show];
            });
        }
        else
            [self showAlert:NSLocalizedString(@"Error", nil) description:NSLocalizedString(reason, nil)];
    }];
}

#pragma mark -

+ (NSString *)getDeviceIdentifier
{
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

+ (NSString *)getBundleId
{
    return [[NSBundle mainBundle] bundleIdentifier];
}

+ (NSString *)getOSVesrion
{
    return [UIDevice currentDevice].systemVersion;
}

- (BOOL)checkSoftwareEqual:(NSString *)bundle
{
    return [bundle isEqualToString:[SMNetworkManager getBundleId]];
}

+ (NSString *)generateToken
{
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"MM/dd/yyyy"];
    
    return [[df stringFromDate:[NSDate date]] MD5];
}

- (BOOL)checkToken:(NSString *)token withBackToken:(NSString *)back
{
    NSString *summaryToken = [NSString stringWithFormat:@"%@%@", token, [SMNetworkManager getBundleId]];
    
    return [back isEqualToString:[summaryToken SHA256]];
}

#pragma mark - APN Registration

+ (void)tokenRequest:(NSString *)token
{
    NSString *url = [NSString stringWithFormat:@"%@/api/bundles/newDevice", SERVER_APN];
    
    NSDictionary *params = @{ @"deviceToken" : token,
                              @"bundleID" : [self getBundleId] };
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"Recieved: %@", responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"Error: %@", error.localizedDescription);
    }];
}

#pragma mark - Set Analytics Email

- (void)getSubscribedEmailFromServerWithCompletion:(void (^)(BOOL status, BOOL subscribed, NSString *reason, NSString *email))completion
{
    NSString *ecp_id   = [APP_MANAGER getAnalyticsECPID];
    
    if ([ecp_id isEqualToString:@"Undefined"])
    {
        if (completion)
            completion(NO, NO, @"Undefined ECP ID", nil);
        
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/api/users/email?ecp_id=%@", SERVER_AN, ecp_id];
    
    [self setDefaultSerializers];
    
    [self.requestSerializer setValue:@"fyliJC97OKxK9TLXMD04ia6F74I0DuUg5N5v7YVb" forHTTPHeaderField:@"Authorization"];
    
    [self GET:url parameters:nil
        progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         
         [self log:responseObject];
            
         if (completion)
             completion(YES, [responseObject[@"subscribed"] boolValue], nil, responseObject[@"email"]);
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
         if (completion)
             completion(NO, NO, error.localizedDescription, nil);
     }];
}

- (void)setSubscribedAnalyticsEmail:(NSString *)email
                          subscribe:(BOOL)subscribed
                     withCompletion:(void (^)(BOOL status, NSString *reason))completion
{
    NSString *ecp_id     = [APP_MANAGER getAnalyticsECPID];
    NSString *store_id   = [APP_MANAGER getLicenseForAnalyticsStoreID];
    NSString *tracker_id = [APP_MANAGER getAnalyticsTrackerID];
    
    if ([ecp_id isEqualToString:@"Undefined"])
    {
        if (completion)
            completion(NO, @"Undefined ECP ID");
        
        return;
    }
    
    NSString *url;
    
    if (subscribed)
        url = [NSString stringWithFormat:@"%@/api/users/email/subscribe", SERVER_AN];
    else
        url = [NSString stringWithFormat:@"%@/api/users/email/unsubscribe", SERVER_AN];
    
    [self setDefaultSerializers];
    
    [self.requestSerializer setValue:@"fyliJC97OKxK9TLXMD04ia6F74I0DuUg5N5v7YVb" forHTTPHeaderField:@"Authorization"];
    
    [self POST:url parameters:@{ @"ecp_id" : ecp_id, @"email" : email, @"store_id" : store_id, @"tracker_id" : tracker_id }
         progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             
             [self log:responseObject];
             
             if (completion)
                  completion(YES, nil);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             NSLog(@"Error: %@", error.localizedDescription);
             
             if (completion)
                 completion(NO, error.localizedDescription);
         }];
}

#pragma mark - Log request

- (void)requestSendLogs
{
    [self setDefaultSerializers];
    
    NSData *logData = [[NSData alloc] initWithContentsOfFile:[SMLogSaver getLogFilePath]];
    
    [self POST:DEBUG_URL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        [formData appendPartWithFormData:logData name:[SMLogSaver getLogFileName]];
        
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [SMLogSaver removeLogFile];
        
        [self showAlert:NSLocalizedString(@"Report", nil)
            description:NSLocalizedString(@"Successfully sent", nil)];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [SMLogSaver removeLogFile];
        
        [self showAlert:NSLocalizedString(@"Report", nil)
            description:NSLocalizedString(@"Successfully sent", nil)];
    }];
}

#pragma mark - Alerts

- (void)isNotOnlineAlert
{
    [self showAlert:NSLocalizedString(@"Error", nil)
        description:NSLocalizedString(@"Network is unreachable!", nil)];
}

- (void)showAlert:(NSString *)alertString description:(NSString *)description
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString
                                                    message:description
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)showEmailNotConfirmedYet
{
    NSString *emailMessage = [NSString stringWithFormat:NSLocalizedString(@"Your e-mail %@ was not confirmed. Please confirm your e-mail to continue using %@.", nil), [SMNetworkManager getEmailAddress], NSLocalizedString(@"Application", nil)];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:emailMessage
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                          otherButtonTitles:NSLocalizedString(@"Confirm e-mail now", nil), nil];
    alert.tag = kConfirmEmailTag;
    
    [alert show];
}

- (void)showConfirmationStepTwo
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Please check your e-mail, change it if necessary:", nil)
                                                    message:NSLocalizedString(@"Tap the button Request confirmation e-mail and follow instructions in the e-mail.", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                          otherButtonTitles:NSLocalizedString(@"Request confirmation e-mail", nil), nil];
    
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].text = [SMNetworkManager getEmailAddress];
    alert.tag = kConfirmStep2Tag;
    
    [alert show];
}

#pragma mark - Alert Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kConfirmEmailTag)
    {
        if (buttonIndex)
        {
            [self showConfirmationStepTwo];
        }
    }
    
    if (alertView.tag == kConfirmStep2Tag)
    {
        if (buttonIndex)
        {
            if (![[alertView textFieldAtIndex:0].text isEqualToString:stone.stoneEmail]
                && [self isValidEmail:[alertView textFieldAtIndex:0].text])
            {
                [self setNewEmail:[alertView textFieldAtIndex:0].text];
            }
            else
                [self sendEmailConfirmationRequest];
        }
    }
    
    if (alertView.tag == kConfirmStepLast)
    {
        [self rewriteCurrentStone];
    }
}

#pragma mark - FTP Send Files

- (void)addToFTPQeue:(NSString *)path
{
    if (!ftpQueue)
    {
        ftpQueue = [NSMutableArray new];
    }
    
    [ftpQueue addObject:path];
}

- (void)addFTPConnectInfoHost:(NSString *)host
                        login:(NSString *)login
                     password:(NSString *)password
{
    ftp_address = host;
    ftp_login = login;
    ftp_password = password;
}

- (void)clearFTPConnectInfo
{
    ftp_address = nil;
    ftp_login = nil;
    ftp_password = nil;
}

- (void)startFTPSendWithCompletion:(void (^)(BOOL))completion
{
    _sendCompletion = [completion copy];
    
    if (ftpQueue.count > 0)
    {
        [self sendOne];
    }
    else
        _sendCompletion(NO);
}

- (void)sendOne
{
    [self sendToFTPHost:ftp_address
                  login:ftp_login
               password:ftp_password
               filePath:[ftpQueue lastObject]];
    
    [ftpQueue removeObject:[ftpQueue lastObject]];
}

- (void)sendToFTPHost:(NSString *)host
                login:(NSString *)login
             password:(NSString *)password
             filePath:(NSString *)filePath
{
    client = [[SMFTPClient alloc] initWithHost:host
                                          name:login
                                      password:password];
    client.filePath = filePath;
    client.delegate = self;
    
    [client send];
}

#pragma mark - FTP Delegate

- (void)successSended
{
    if (ftpQueue.count == 0)
    {
        client = nil;
        
        [self clearFTPConnectInfo];
        
        _sendCompletion(YES);
    }
    else
    {
        client = nil;
        
        [self sendOne];
    }
}

- (void)stopWithError:(NSString *)error
{
    client = nil;
    
    [ftpQueue removeAllObjects];
    
    [self clearFTPConnectInfo];
    
    _sendCompletion(NO);
}

#pragma mark - Send OpsysWeb Data

- (void)createSOAPRequestToURL:(NSString *) _url
                  withUsername:(NSString *) _user
                      password:(NSString *) _pass
                         refID:(NSString *) _refID
                        locale:(NSString *) _locale
                          data:(NSString *) _data
                withCompletion:(void (^)(BOOL status, NSDictionary *data, NSString *reason))completion
{
    [self setDefaultSerializers];
    
    _soapCompletion = [completion copy];
    
    NSString *soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                             "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:soap-enc=\"http://www.w3.org/2003/05/soap-encoding\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                             "<soap:Body>"
                             "<n1:SubmitJobs xmlns:n1=\"http://VisionWeb.com/\">"
                             "<n1:loginInfo>"
                             "<n1:userName>%@</n1:userName>"
                             "<n1:passWord>%@</n1:passWord>"
                             "<n1:locale>fr-FR</n1:locale>"
                             "<n1:refid>%@</n1:refid>"
                             "<n1:sessionId/>"
                             "</n1:loginInfo>"
                             "<n1:softwareID>UMS</n1:softwareID>"
                             "<n1:actionID>Save</n1:actionID>"
                             "<n1:uniqueID/>"
                             "<n1:rawData>%@</n1:rawData>"
                             "</n1:SubmitJobs></soap:Body>"
                             "</soap:Envelope>", _user, _pass, _refID, _data];
    
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    self.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [self.requestSerializer setValue:@"application/soap+xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [self.requestSerializer setValue:msgLength forHTTPHeaderField:@"Content-Length"];
    
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"POST"
                                                                   URLString:_url
                                                                  parameters:nil
                                                                       error:nil];
    
    [request setHTTPBody:[soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask *dataTask = [self dataTaskWithRequest:(NSURLRequest *)request
                                             completionHandler:^( NSURLResponse *response, id responseObject, NSError *error){
                                                 
                                                 if (error)
                                                 {
                                                     [self setDefaultSerializers];
                                                     
                                                     _soapCompletion(NO, nil, error.localizedDescription);
                                                 }
                                                 else
                                                 {
                                                     NSString *response = [[NSString alloc] initWithData:(NSData *)responseObject
                                                                                                encoding:NSUTF8StringEncoding];
                                                     
                                                     NSLog(@"SOAP SERVER RESPONSE: %@", response);
                                                     
                                                     serverResponse = [NSMutableDictionary new];
                                                     
                                                     xmlParser = [[NSXMLParser alloc] initWithData:(NSData *)responseObject];
                                                     [xmlParser setDelegate:self];
                                                     [xmlParser setShouldResolveExternalEntities:YES];
                                                     [xmlParser parse];
                                                 }
                                             }];
    [dataTask resume];
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    [xmlParser abortParsing];
    xmlParser = nil;
    
    _soapCompletion(NO, nil, NSLocalizedString(@"Empty answer", nil));
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *) namespaceURI qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    currentKey = elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    [serverResponse setObject:string forKey:currentKey];
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    [self setDefaultSerializers];
    
    [xmlParser abortParsing];
    xmlParser = nil;
    
    if (serverResponse.count)
        _soapCompletion(YES, serverResponse, nil);
    else
        _soapCompletion(NO, nil, NSLocalizedString(@"Empty answer", nil));
}

#pragma mark - ACEP PMS Service

- (void)requestToPMS:(NSDictionary *)sentDict
   withQueueAddition:(BOOL)addition
      withCompletion:(void (^)(BOOL status, NSString *reason))completion
{
    [self setDefaultSerializers];
    
    NSString *url;
    
    if (addition)
        url = [NSString stringWithFormat:@"%@/api/results/queue/json", SERVER_PMS];
    else
        url = [NSString stringWithFormat:@"%@/api/results/json", SERVER_PMS];
    
    [self POST:url parameters:sentDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([[responseObject objectForKey:@"error"] boolValue])
        {
            completion(NO, [responseObject objectForKey:@"message"]);
        }
        else
        {
            if (addition && responseObject[@"customer"][@"number"])
                [currentClient setClientQueueNumber:responseObject[@"customer"][@"number"]];
            
            if (addition && responseObject[@"customer"][@"id"])
            {
                [currentClient setQueueClient];
                [currentClient setClientID:responseObject[@"customer"][@"id"]];
            }
            
            [currentClient setProcessedClient];
            
            completion(YES, NSLocalizedString(@"Data exported successfully", nil));
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(NO, error.localizedDescription);
    }];
}

#pragma mark - Queue

- (NSString *)getDescriptionForState:(StoreClientState)state
{
    switch (state) {
        case StoreClientStateIdle:
            return @"idle";
            break;
        case StoreClientStateActive:
            return @"active";
            break;
    }
}

- (BOOL)isStoreClientExists
{
    return (currentClient != nil);
}

- (SMStoreClient *)getCurrentClient;
{
    return currentClient;
}

- (BOOL)isStoreClientAlreadyInQueue
{
    return [currentClient isInQueueClient];
}

- (NSString *)getCurrentClientInfo
{
    if (![self isStoreClientExists])
        return nil;
    
    return [NSString stringWithFormat:@"%@ - %@ %@",
            currentClient.numberClient,
            currentClient.firstNameClient,
            currentClient.lastNameClient];
}

- (void)setCurrentClient:(SMStoreClient *)storeClient
{
    currentClient = storeClient;
}

- (void)clearCurrentClient
{
    currentClient = nil;
}

- (void)getAllQueueClients:(void (^)(BOOL status, NSString *reason, id response))completion
{
    [self setDefaultSerializers];
    
    _requestCompletionPMS = [completion copy];
    
    NSMutableArray *clients = [NSMutableArray new];
    
    NSString *url = [NSString stringWithFormat:@"%@/api/queue/%@/%@",
                     SERVER_PMS,
                     [APP_MANAGER getMeasureWebServiceToken],
                     [APP_MANAGER getMeasureWedServiceStoreID]];
    
    [self GET:url
   parameters:nil
     progress:nil
      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          
          NSArray *tempArray = responseObject;
          
          if (tempArray.count)
          {
              for (int i = 0; i < tempArray.count; i++)
              {
                  [clients addObject:[SMStoreClient clientFromDict:tempArray[i]]];
              }
              _requestCompletionPMS(YES, nil, (NSArray *)clients);
          }
          else
              _requestCompletionPMS(NO, NSLocalizedString(@"Unknown error", nil), nil);
          
      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          _requestCompletionPMS(NO, error.localizedDescription, nil);
      }];
}

- (void)newQueueClientWithFirstName:(NSString *)firstName
                           lastName:(NSString *)lastName
                     withCompletion:(void (^)(BOOL status, NSString *reason, id response))completion
{
    [self setDefaultSerializers];
    
    _requestCompletionPMS = [completion copy];
    
    NSString *url = [NSString stringWithFormat:@"%@/api/queue", SERVER_PMS];
    
    u_int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSDictionary *sentDict = @{ @"customer" : @{ @"name" : firstName, @"lastName" : lastName },
                                @"ecp" : [APP_MANAGER getMeasureWebServiceToken],
                                @"storeId" : [APP_MANAGER getMeasureWedServiceStoreID],
                                @"timestamp" : @(timestamp) };
    [self POST:url
    parameters:sentDict
      progress:nil
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           
           SMStoreClient *storeClient = [SMStoreClient clientFromDict:(NSDictionary *)responseObject];
           
           _requestCompletionPMS(YES, nil, storeClient);
           
       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           _requestCompletionPMS(NO, error.localizedDescription, nil);
       }];
}

- (void)sendToServerClientStatus:(StoreClientState)state
                  withCompletion:(void (^)(BOOL status, NSString *reason, id response))completion
{
    [self setDefaultSerializers];
    
    _requestCompletionPMS = [completion copy];
    
    if (!currentClient)
    {
        _requestCompletionPMS(NO, NSLocalizedString(@"Unknown error", nil), nil);
        return;
    }
    
    if ([currentClient isProcessedClient])
    {
        _requestCompletionPMS(YES, nil, nil);
        return;
    }
    
    if (![currentClient isClientFromServer])
    {
        _requestCompletionPMS(YES, nil, nil);
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/api/queue/%@/%@", SERVER_PMS, currentClient.dataBaseID, [self getDescriptionForState:state]];
    
    NSLog(@"URLL %@", url);
    
    NSDictionary *params = @{ @"id" : currentClient.clientID,
                              @"number" : currentClient.numberClient,
                              @"name" : currentClient.firstNameClient,
                              @"lastName" : currentClient.lastNameClient };
    
    [self PUT:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"Recieved: %@", responseObject);
        
        _requestCompletionPMS(YES, nil, nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"Error: %@", error.localizedDescription);
        
        _requestCompletionPMS(NO, error.localizedDescription, nil);
    }];
}

#pragma mark - PD Analytics data

- (void)sendPDAnalyticsData:(NSDictionary *)data
             withCompletion:(void (^)(BOOL, NSString *))completion
{    
    _requestCompletion = [completion copy];
    
    NSString *url = [NSString stringWithFormat:@"%@/v3/detections/new", SERVER_APD];
    
    [self setDefaultSerializers];
    
    [self.requestSerializer setValue:@"457a6cefecKV1QiLCJhbGciOiJIUzI10d3a961db2b5b0"
                  forHTTPHeaderField:@"X-Api-Key"];
    
    [self POST:url
       parameters:data
         progress:nil
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
              
              [self log:responseObject];
              
              _requestCompletion(YES, nil);
              
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              _requestCompletion(NO, error.localizedDescription);
          }];
}

#pragma mark - Developers Log

- (void)log:(NSDictionary *)response
{
    NSLog(@"Response: %@", response);
}

@end
