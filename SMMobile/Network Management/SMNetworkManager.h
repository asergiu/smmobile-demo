//
//  SMNetworkManager.h
//
//  Created by Oleg Bogatenko on 10/7/14.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "SMStone.h"

#define NETWORK_MANAGER [SMNetworkManager sharedManager]
#define ShowNetworkActivityIndicator() [UIApplication sharedApplication].networkActivityIndicatorVisible = YES
#define HideNetworkActivityIndicator() [UIApplication sharedApplication].networkActivityIndicatorVisible = NO

#define SERVER_URL @"https://license.opticvideo.com"
#define SERVER_APN @"https://notification.opticvideo.com"
#define SERVER_PMS @"https://dataweb.opticvideo.com"
#define SERVER_AN  @"https://analytics.opticvideo.com"
#define DEBUG_URL  @"http://52.18.53.226"
#define SERVER_APD @"http://ec2-34-242-221-118.eu-west-1.compute.amazonaws.com"

@class SMStone;
@class SMStoreClient;

typedef NS_ENUM (NSInteger, SMModule) {
    SMModuleUnknown,
    SMModuleVS,
    SMModuleFS,
    SMModuleSM,
    SMModulePD
};

typedef NS_ENUM (NSInteger, SMRecievedStatus) {
    SMAccessGranted       = 0,
    SMAccessDenied        = 1,
    SMSoftPresent         = 2,
    SMWrongSoftware       = 3,
    SMStoneActive         = 4,
    SMStoneExpired        = 5,
    SMSessionEnded        = 6,
    SMDeviceExists        = 7,
    SMToManyDevices       = 8,
    SMRecordWasAdded      = 9,
    SMWrongParams         = 10,
    SMEmptySoftwareTag    = 11,
    SMTagPresented        = 12,
    SMEmptyAuthTag        = 13,
    SMNotMultistone       = 14,
    SMMultistoneAdded     = 15,
    SMMacIsCorrect        = 16,
    SMDeviceIsDeactivated = 17,
    SMDeviceIsActivated   = 18,
    SMDeviceIsReactivated = 19,
    SMDeviceExistsBlocked = 20,
    SMStoneHasExpired     = 21,
    SMToManyInSession     = 22,
    SMEmailChanged        = 23,
    SMEmailConfirmed      = 24,
    SMEmailNotConfirmed   = 25,
    SMUnknownError        = 40
};

typedef NS_ENUM (NSInteger, StoreClientState) {
    StoreClientStateIdle,
    StoreClientStateActive
};

@interface SMNetworkManager : AFHTTPSessionManager
{
    void (^_requestCompletion)(BOOL status, NSString *reason);
    void (^_requestCompletionPMS)(BOOL status, NSString *reason, id response);
}

@property (nonatomic, assign) SMModule currentModule;

+ (instancetype)sharedManager;

+ (NSString *)getMessageForStatus:(int)status;
+ (NSString *)currentRouterMacAddress;
+ (NSString *)getDeviceIdentifier;
+ (NSString *)getBundleId;
+ (NSString *)generateToken;
+ (NSString *)getOSVesrion;
+ (NSString *)getEmailAddress;

//
// Send “device token” for for remote (push) notifications
//
+ (void)tokenRequest:(NSString *)token;

- (BOOL)networkAvaliable;

//
// Start check network status
//
- (void)startCheck;

//
// Check current router MAC addr
//
- (void)checkMacAdress;

- (BOOL)loadCurrentStone;
- (void)fillCurrentWithStone:(SMStone *)aStone;

- (BOOL)isAuthenticated;
- (BOOL)isAuthenticatedForModule:(SMModule)module;
- (BOOL)isAuthorizedCompletely;
- (BOOL)isAuthorizedSMOnly;

//
// Opening authorization session with completion
//
- (void)swordWithName:(NSString *)name
             princess:(NSString *)princess
                email:(NSString *)email
               module:(SMModule)module
       fillCompletion:(void (^)(BOOL status, NSString *reason))completion;

//
// Events sending
//
- (void)sendAsyncDeviceStatus:(BOOL)active
                    forModule:(SMModule)module
             returnCompletion:(BOOL)completion;

- (void)enterModule:(SMModule)module withFillCompletion:(void (^)(BOOL status, NSString *reason))completion;
- (void)enterAfterSwordInModule:(SMModule)module;

- (void)exitModule;

- (void)checkExpiry;

- (BOOL)isCheckNeeded;
- (void)getBlockTag;

- (void)isNotOnlineAlert;

- (void)showAlert:(NSString *)alertString
      description:(NSString *)description;

- (void)requestToPMS:(NSDictionary *)sentDict
   withQueueAddition:(BOOL)addition
      withCompletion:(void (^)(BOOL status, NSString *reason))completion;

- (void)addToFTPQeue:(NSString *)path;

- (void)addFTPConnectInfoHost:(NSString *)host
                        login:(NSString *)login
                     password:(NSString *)password;

- (void)startFTPSendWithCompletion:(void (^)(BOOL))completion;

- (BOOL)isEmailCheckNeed;

- (void)checkEmailConfirmationWithAlert:(BOOL)withAlert;

- (void)checkEmail;

- (BOOL)isValidEmail:(NSString *)emailString;

- (void)sendEmailRequestWithCompletion:(void (^)(BOOL status, NSString *reason))completion;
- (void)isEmailConfirmed:(void (^)(BOOL status, NSString *reason))completion;

- (void)requestSendLogs;

// Analytics email requests
- (void)getSubscribedEmailFromServerWithCompletion:(void (^)(BOOL status, BOOL subscribed, NSString *reason, NSString *email))completion;

- (void)setSubscribedAnalyticsEmail:(NSString *)email
                          subscribe:(BOOL)subscribed
                     withCompletion:(void (^)(BOOL status, NSString *reason))completion;

// OpsysWeb Data SOAP Request
- (void)createSOAPRequestToURL:(NSString *) _url
                  withUsername:(NSString *) _user
                      password:(NSString *) _pass
                         refID:(NSString *) _refID
                        locale:(NSString *) _locale
                          data:(NSString *) _data
                withCompletion:(void (^)(BOOL status, NSDictionary *data, NSString *reason))completion;

// Queue

- (BOOL)isStoreClientExists;
- (BOOL)isStoreClientAlreadyInQueue;

- (void)setCurrentClient:(SMStoreClient *)storeClient;

- (SMStoreClient *)getCurrentClient;

- (NSString *)getCurrentClientInfo;

- (void)clearCurrentClient;

- (void)getAllQueueClients:(void (^)(BOOL status, NSString *reason, id response))completion;

- (void)newQueueClientWithFirstName:(NSString *)firstName
                           lastName:(NSString *)lastName
                     withCompletion:(void (^)(BOOL status, NSString *reason, id response))completion;

- (void)sendToServerClientStatus:(StoreClientState)state
                  withCompletion:(void (^)(BOOL status, NSString *reason, id response))completion;

// PD Data analytic request
- (void)sendPDAnalyticsData:(NSDictionary *)data
             withCompletion:(void (^)(BOOL status, NSString *reason))completion;

@end
