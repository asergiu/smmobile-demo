//
//  SMFTPClient.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 2/13/15.
//

#import "SMFTPClient.h"
#import "NetworkManager.h"

#define BUFFER_SIZE 32768

@interface SMFTPClient () <NSStreamDelegate>
{    
    NSOutputStream *networkStream;
    NSInputStream *fileStream;
    
    NSString *fileName;
    
    NSString *hostURL;
    NSString *swordFTP;
    NSString *passwordFTP;
    
    size_t bufferOffset;
    size_t bufferLimit;
    
    uint8_t _buffer[BUFFER_SIZE];
}

@end

@implementation SMFTPClient

@synthesize delegate;
@synthesize filePath;

- (instancetype)initWithHost:(NSString *)host
                          name:(NSString *)name
                      password:(NSString *)password
{
    self = [super init];
    
    if(self)
    {
        hostURL = host;
        swordFTP = name;
        passwordFTP = password;
    }
    
    return self;
}

- (NSString *)getFileName
{
    if (!filePath)
    {
        return @"TEST";
    }
    
    return [[filePath componentsSeparatedByString:@"/"] lastObject];
}

- (uint8_t *)buffer
{
    return self->_buffer;
}

- (BOOL)isSending
{
    return (networkStream != nil);
}

- (void)send
{
    BOOL success;
    
    if (!filePath)
    {
        [self stopSendWithStatus:@"Sending error!"];
        
        return;
    }
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        [self stopSendWithStatus:@"Sending error!"];
        
        return;
    }
    
    if (networkStream || fileStream)
    {
        [self stopSendWithStatus:@"Sending error!"];
        
        return;
    }
    
    // First get and check the URL.
    NSURL *url = [[NetworkManager sharedInstance] smartURLForString:hostURL];
    
    if (!url)
    {
        [self stopSendWithStatus:@"File read error"];
        
        return;
    }
        
    url = CFBridgingRelease(CFURLCreateCopyAppendingPathComponent(NULL, (__bridge CFURLRef) url, (__bridge CFStringRef) [self getFileName], false));

    if (!url)
    {
        [self stopSendWithStatus:@"File read error"];
        
        return;
    }
   
    fileStream = [NSInputStream inputStreamWithFileAtPath:filePath];
    
    assert(fileStream != nil);

    [fileStream open];
    
    // Open a CFFTPStream for the URL.
    networkStream = CFBridgingRelease(CFWriteStreamCreateWithFTPURL(NULL, (__bridge CFURLRef) url));
    
    assert(networkStream != nil);
    
    if ([swordFTP length] != 0)
    {
        success = [networkStream setProperty:swordFTP forKey:(id)kCFStreamPropertyFTPUserName];
        assert(success);
        success = [networkStream setProperty:passwordFTP forKey:(id)kCFStreamPropertyFTPPassword];
        assert(success);
    }
    
    networkStream.delegate = self;
    
    [networkStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [networkStream open];
    
    [self sendDidStart];
}

- (void)sendDidStart
{
    [[NetworkManager sharedInstance] didStartNetworkOperation];
}

- (void)sendDidStopWithStatus:(NSString *)statusString
{
    if (!statusString)
    {
        if (delegate && [delegate respondsToSelector:@selector(successSended)])
        {
            [delegate successSended];
        }
    }
    else
    {
        if (delegate && [delegate respondsToSelector:@selector(stopWithError:)])
        {
            [delegate stopWithError:statusString];
        }
    }
    
    [[NetworkManager sharedInstance] didStopNetworkOperation];
}

- (void)stopSendWithStatus:(NSString *)statusString
{
    if (networkStream != nil)
    {
        [networkStream removeFromRunLoop:[NSRunLoop currentRunLoop]
                                 forMode:NSDefaultRunLoopMode];
        
        networkStream.delegate = nil;
        
        [networkStream close];
        
        networkStream = nil;
    }
    
    if (fileStream != nil)
    {
        [fileStream close];
        
        fileStream = nil;
    }
    
    [self sendDidStopWithStatus:statusString];
}

- (void)updateStatus:(NSString *)statusString
{
    assert(statusString != nil);
}

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode
{
#pragma unused(aStream)
    assert(aStream == networkStream);
    
    switch (eventCode) {
        case NSStreamEventOpenCompleted:
        {
            [self updateStatus:@"Opened connection"];
        }
            break;
        case NSStreamEventHasBytesAvailable:
        {
            assert(NO);     // should never happen for the output stream
        }
            break;
        case NSStreamEventHasSpaceAvailable:
        {
            [self updateStatus:@"Sending"];
            
            if (bufferOffset == bufferLimit) {
                NSInteger   bytesRead;
                
                bytesRead = [fileStream read:self.buffer maxLength:BUFFER_SIZE];
                
                if (bytesRead == -1) {
                    [self stopSendWithStatus:@"File read error"];
                } else if (bytesRead == 0) {
                    [self stopSendWithStatus:nil];
                } else {
                    bufferOffset = 0;
                    bufferLimit  = bytesRead;
                }
            }
            
            if (bufferOffset != bufferLimit) {
                NSInteger   bytesWritten;
                bytesWritten = [networkStream write:&self.buffer[bufferOffset]
                                          maxLength:bufferLimit - bufferOffset];
                
                assert(bytesWritten != 0);
                
                if (bytesWritten == -1) {
                    [self stopSendWithStatus:@"Network write error"];
                } else {
                    bufferOffset += bytesWritten;
                }
            }
        }
            break;
        case NSStreamEventErrorOccurred:
        {
            [self stopSendWithStatus:@"Stream open error"];
        }
            break;
        case NSStreamEventEndEncountered:
        {
            // ignore
        } break;
        default:
        {
            assert(NO);
        }
            break;
    }
}

#pragma mark - Dealloc

- (void)dealloc
{
    networkStream = nil;
    fileStream = nil;
    filePath = nil;
    delegate = nil;
}

@end
