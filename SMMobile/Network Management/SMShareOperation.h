//
//  SMShareOperation.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 3/4/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMShareImage.h"

@interface SMShareOperation : NSOperation

- (id)initWithImage:(SMShareImage *)image
          doneBlock:(void (^)(BOOL, SMShareImage *))block;

@end
