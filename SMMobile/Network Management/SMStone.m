//
//  SMStone.m
//
//  Created by Oleg Bogatenko on 10/14/14.
//

#import "SMStone.h"

@implementation SMStone

@synthesize authID;
@synthesize expiry;
@synthesize multiType;
@synthesize stoneID;
@synthesize multiStone;
@synthesize stoneSword;
@synthesize stoneEmail;
@synthesize registration;
@synthesize needCheckEmail;

#pragma mark Init

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        self.multiStone = NO;
        self.needCheckEmail = NO;
    }
    return self;
}

#pragma mark Serialization

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:[NSNumber numberWithInt:authID] forKey:@"authID"];
    [encoder encodeObject:[NSNumber numberWithInt:expiry] forKey:@"expiry"];
    [encoder encodeObject:[NSNumber numberWithInt:registration] forKey:@"registration"];
    [encoder encodeObject:[NSNumber numberWithBool:needCheckEmail] forKey:@"needCheckEmail"];
    [encoder encodeObject:[NSNumber numberWithInt:multiType] forKey:@"type"];
    [encoder encodeObject:[NSNumber numberWithInt:stoneID] forKey:@"stoneID"];
    [encoder encodeObject:stoneSword forKey:@"stoneSword"];
    [encoder encodeObject:stoneEmail forKey:@"stoneEmail"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init])
    {
        self.authID = [[decoder decodeObjectForKey:@"authID"] unsignedIntValue];
        self.expiry = [[decoder decodeObjectForKey:@"expiry"] unsignedIntValue];
        self.registration = [[decoder decodeObjectForKey:@"registration"] unsignedIntValue];
        self.needCheckEmail = [[decoder decodeObjectForKey:@"needCheckEmail"] boolValue];
        self.multiType = [[decoder decodeObjectForKey:@"type"] unsignedIntValue];
        self.stoneID = [[decoder decodeObjectForKey:@"stoneID"] unsignedIntValue];
        self.stoneSword = [decoder decodeObjectForKey:@"stoneSword"];
        self.stoneEmail = [decoder decodeObjectForKey:@"stoneEmail"];
    }
    return self;
}

#pragma mark Helpers

+ (void)saveInUserDefaults:(SMStone *)stone
{
    NSLog(@"Saved stone: %@", stone);
    
    NSData *encodedStone = [NSKeyedArchiver archivedDataWithRootObject:stone];
    [[NSUserDefaults standardUserDefaults] setObject:encodedStone forKey:@"stone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (SMStone *)getCurrentStone
{
    NSData *dataStone = [[NSUserDefaults standardUserDefaults] objectForKey:@"stone"];
    
    if (dataStone)
    {
        return (SMStone *)[NSKeyedUnarchiver unarchiveObjectWithData:dataStone];
    }
    
    return nil;
}

+ (NSString *)getCurrentStoneId
{
    SMStone *stone = [SMStone getCurrentStone];
    
    if (stone)
        return stone.stoneSword;
    
    return nil;
}

+ (BOOL)isMultitype
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"multistone"];
}

+ (BOOL)storeTypeStone
{
    if (![SMStone isMultitype])
    {
        return NO;
    }
    
    return [SMStone getCurrentStone].multiType == 2;
}

+ (BOOL)sessionTypeStone
{
    if (![SMStone isMultitype])
    {
        return NO;
    }
    
    return [SMStone getCurrentStone].multiType == 1;
}

+ (void)killCurrentStone
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"pdKeySuccessful"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"smKeySuccessful"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"multistone"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"stone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark Stone Log

- (NSString *)description
{
    return [NSString stringWithFormat:@"License: %@, exp: %u, multi: %@ withMultiType: %u, authID: %u, email: %@, registration: %d", stoneSword, expiry, (multiStone ? @"YES" : @"NO"), multiType, authID, stoneEmail, registration];
}


@end
