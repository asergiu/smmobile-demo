//
//  SMShareImage.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 3/3/16.
//  Copyright © 2016 ACEP. All rights reserved.
//

#import "SMShareImage.h"

@implementation SMShareImage

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.success = NO;
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Image: %@ %@ %@", self.name, self.fileURL, self.presignedURL];
}

@end
