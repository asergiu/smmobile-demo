//
//  SMProvisionAlertViewController.m
//  SMMobile
//
//  Created by Sergej Bogatenko on 3/3/18.
//  Copyright © 2018 ACEP. All rights reserved.
//

#import "SMProvisionAlertViewController.h"
#import "SMAppManager.h"

@interface SMProvisionAlertViewController ()
{
    IBOutlet UIButton *okBtn;
    IBOutlet UIButton *showAgainBtn;
    
    IBOutlet UILabel *showAgainLabel;
    
    IBOutlet UIImageView *checkBtnImageView;
    
    IBOutlet UIView *backgroundView;
}

- (IBAction)showAgainPressed:(id)sender;
- (IBAction)okPressed:(id)sender;

@end

@implementation SMProvisionAlertViewController

@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    showAgainLabel.adjustsFontSizeToFitWidth = YES;
    
    backgroundView.layer.cornerRadius = 10.f;
    
    showAgainBtn.selected = ![APP_MANAGER dontShowAgainExpirationDateAlert];

    [self setupCheckImage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupCheckImage
{
    checkBtnImageView.image = [UIImage imageNamed:showAgainBtn.selected ? @"show_again_btn Check" : @"show_again_btn"];
}

#pragma mark - Action

- (void)showAgainPressed:(UIButton *)sender
{
    showAgainBtn.selected = !showAgainBtn.selected;
    
    [self setupCheckImage];
    
    [APP_MANAGER setupIsNeedShowAgainExpirationDateAlert:showAgainBtn.isSelected];
}

- (void)okPressed:(UIButton *)sender
{
    [APP_MANAGER setupIsNeedCheckNewProfileExpirationDate];
    
    if (delegate && [delegate respondsToSelector:@selector(dismissProvisionAlert)])
        [delegate dismissProvisionAlert];
}

@end
