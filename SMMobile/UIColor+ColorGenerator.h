//
//  UIColor+ColorGenerator.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 8/5/14.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorGenerator)

+ (UIColor *)R:(NSUInteger)red
             G:(NSUInteger)green
             B:(NSUInteger)blue;

+ (UIColor *)R:(NSUInteger)red
             G:(NSUInteger)green
             B:(NSUInteger)blue
             A:(NSUInteger)alpha;

@end
