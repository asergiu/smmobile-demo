//
//  NSString+Hash.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 12/19/14.
//

#import <Foundation/Foundation.h>

@interface NSString (Hash)

- (NSString *)MD5;

- (NSString *)SHA256;

- (NSString *)noSpaceString;

@end
