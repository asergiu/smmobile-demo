//
//  NSDictionary+KeysRotation.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 5/16/18.
//  Copyright © 2018 ACEP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (KeysRotation)

- (NSDictionary *)rotateKeys:(NSDictionary *)keys;

@end
