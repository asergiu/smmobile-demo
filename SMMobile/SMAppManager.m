//
//  SMAppManager.m
//
//  Created by Oleg Bogatenko on 10/8/15.
//
//

#import "SMAppManager.h"
#import <AVFoundation/AVFoundation.h>
#import "GAIDictionaryBuilder.h"
#import "NSString+Hash.h"
#import "NSString+CheckStrings.h"
#import "UIWindow+VisibleController.h"
#import "SMNetworkManager.h"
#import "SVProgressHUD.h"
#import "SMProvisionChecker.h"

#import "SMStone.h"

#import "SessionManager.h"
#import "PDShareSessionsViewController.h"

#import "AESCrypt.h"

#define kPermissionsAlert 333

@interface SMAppManager ()
{
    BOOL isActiveEmailNotifications;
    BOOL inProgress;
    
    BOOL old_subscribe;
    NSString *old_ecp_id;
    NSString *old_email;
    
    SMMainMenuViewController *mainMenuCtrl;
    BOOL mustBackToPDModule;
    
    SMProvisionChecker *provisionChecker;
    
    NSDictionary *parseUrlDict;
}

@end

@implementation SMAppManager

+ (instancetype)sharedManager
{
    static dispatch_once_t onceToken = 0;
    static id _sharedManager = nil;
    
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (id)init
{
    if (self = [super init])
    {
        provisionChecker = [SMProvisionChecker new];
        
        isActiveEmailNotifications = NO;
        inProgress = NO;
        mustBackToPDModule = NO;
        
        if (IS_IOS8)
        {
            old_ecp_id    = [[NSUserDefaults standardUserDefaults] stringForKey:@"analytics_ecp_id"];
            old_subscribe = [[NSUserDefaults standardUserDefaults] boolForKey:@"change_subscribed_email"];
            old_email     = [[NSUserDefaults standardUserDefaults] stringForKey:@"analytics_email_textField"];
        }
    }
    
    return self;
}

#pragma mark - Analytics Email

- (BOOL)isAnalyticsSubscribed
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"change_subscribed_email"];
}

- (void)resetAnalyticsEmail
{
    [self stopAnalyticsEmailNotifications];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"change_subscribed_email"];
    [[NSUserDefaults standardUserDefaults] setObject:@"Analytics Subscription E-mail not defined." forKey:@"analytics_email"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"analytics_email_textField"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"Unsubscribed");
    
    [self startAnalyticsEmailNotifications];
}

- (void)saveAnalyticsEmail:(NSString *)email
{
    [self stopAnalyticsEmailNotifications];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"change_subscribed_email"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"analytics_email_textField"];
    [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"analytics_email"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"Subscribed with email: %@", email);
    
    [self startAnalyticsEmailNotifications];
}

- (BOOL)isAnalyticsEmailNotifyActivated
{
    return isActiveEmailNotifications;
}

- (BOOL)isInputEmailFieldEmpty
{
    return ([[NSUserDefaults standardUserDefaults] objectForKey:@"analytics_email_textField"] == nil);
}

- (NSString *)getInputEmailFieldContent
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"analytics_email_textField"];
}

- (void)startAnalyticsEmailNotifications
{
    if (!isActiveEmailNotifications)
    {
        isActiveEmailNotifications = YES;
        
        if (IS_IOS8)
        {
            old_ecp_id    = [[NSUserDefaults standardUserDefaults] stringForKey:@"analytics_ecp_id"];
            old_subscribe = [[NSUserDefaults standardUserDefaults] boolForKey:@"change_subscribed_email"];
            old_email     = [[NSUserDefaults standardUserDefaults] stringForKey:@"analytics_email_textField"];
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(defaultsChanged)
                                                         name:NSUserDefaultsDidChangeNotification
                                                       object:nil];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] addObserver:self
                                                    forKeyPath:@"analytics_email_textField"
                                                       options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                                                       context:nil];
            
            [[NSUserDefaults standardUserDefaults] addObserver:self
                                                    forKeyPath:@"change_subscribed_email"
                                                       options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                                                       context:nil];
            
            [[NSUserDefaults standardUserDefaults] addObserver:self
                                                    forKeyPath:@"analytics_ecp_id"
                                                       options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                                                       context:nil];
        }
    }
}

- (void)stopAnalyticsEmailNotifications
{
    if (isActiveEmailNotifications)
    {
        isActiveEmailNotifications = NO;
        
        if (IS_IOS8)
        {
            [[NSNotificationCenter defaultCenter] removeObserver:self
                                                            name:NSUserDefaultsDidChangeNotification
                                                          object:nil];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] removeObserver:self
                                                       forKeyPath:@"analytics_email_textField"];
            
            [[NSUserDefaults standardUserDefaults] removeObserver:self
                                                       forKeyPath:@"change_subscribed_email"];
            
            [[NSUserDefaults standardUserDefaults] removeObserver:self
                                                       forKeyPath:@"analytics_ecp_id"];
        }
    }
}



- (void)defaultsChanged
{
    if (IS_IOS8)
    {
        [self stopAnalyticsEmailNotifications];
        
        [self performSelector:@selector(startSubscribeLogic)
                   withObject:nil afterDelay:2.f];
    }
}

- (void)startSubscribeLogic
{
    NSString *new_ecp_id = [[NSUserDefaults standardUserDefaults] stringForKey:@"analytics_ecp_id"];
    BOOL new_subscribe = [[NSUserDefaults standardUserDefaults] boolForKey:@"change_subscribed_email"];
    NSString *new_email = [[NSUserDefaults standardUserDefaults] stringForKey:@"analytics_email_textField"];
    
    NSLog(@"New subscribe state: %d", new_subscribe);
    NSLog(@"Old subscribe state: %d", old_subscribe);
    
    if (new_subscribe && !old_subscribe)
    {
        if (![self isInputEmailFieldEmpty])
            [self subscribeEmail];
        else
            [self resetAnalyticsEmail];
        
        return;
    }
    
    if (!new_subscribe && old_subscribe)
    {
        [self unsubscribeEmail];
        
        return;
    }
    
    if (new_email.length || old_email.length)
    {
        if (![new_email isEqualToString:old_email])
        {
            [self subscribeEmail];
        }
    }
    
    if (new_ecp_id.length || old_ecp_id.length)
    {
        if (![new_ecp_id isEqualToString:old_ecp_id])
        {
            [self getSubscribedEmailWithHud:YES];
        }
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (object == [NSUserDefaults standardUserDefaults])
    {
        if ([keyPath isEqualToString:@"change_subscribed_email"])
        {
            NSLog(@"Switcher change state: %@", change);
            
            if (![change[@"new"] boolValue] && [change[@"old"] boolValue])
            {
                [self unsubscribeEmail];
                
                return;
            }
            
            if ([change[@"new"] boolValue])
            {
                if (![self isInputEmailFieldEmpty])
                    [self subscribeEmail];
                else
                    [self resetAnalyticsEmail];
                
                return;
            }
        }
        
        if ([keyPath isEqualToString:@"analytics_email_textField"])
        {
            if (![change[@"new"] isEqual:change[@"old"]])
            {
                [self subscribeEmail];
            }
            
            return;
        }
        
        if ([keyPath isEqualToString:@"analytics_ecp_id"])
        {
            if ([self isInputEmailFieldEmpty])
                [self getSubscribedEmailWithHud:YES];
        }
    }
    else
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

- (void)setupAnalyticsEmail
{
    if (![[self getAnalyticsECPID] isEqualToString:@"Undefined"])
    {
        [self getSubscribedEmailWithHud:NO];
    }
    else
    {
        if (![self isAnalyticsEmailNotifyActivated])
            [self startAnalyticsEmailNotifications];
    }
}

- (void)getSubscribedEmailWithHud:(BOOL)withHud
{
    if (inProgress)
        return;
    
    inProgress = YES;
    
    if (withHud)
    {
        [SVProgressHUD setFont:[UIFont fontWithName:@"TrebuchetMS" size:14.f]];
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait. Updating Analytics Subscription Settings...", nil)];
    }
    
    [NETWORK_MANAGER getSubscribedEmailFromServerWithCompletion:^(BOOL status, BOOL subscribed, NSString *reason, NSString *email) {
        
        [self performSelector:@selector(dismissHUD)
                   withObject:nil afterDelay:1.5f];
        
        if (status)
        {
            if (subscribed)
                [self saveAnalyticsEmail:email];
            else
                [self resetAnalyticsEmail];
        }
        
        inProgress = NO;
        
        if (![self isAnalyticsEmailNotifyActivated])
            [self startAnalyticsEmailNotifications];
    }];
}

- (void)subscribeEmail
{
    if (inProgress)
        return;
    
    inProgress = YES;
    
    NSLog(@"Start subscribe request...");
    
    NSString *email = [self getInputEmailFieldContent];
    
    [SVProgressHUD setFont:[UIFont fontWithName:@"TrebuchetMS" size:14.f]];
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait. Updating Analytics Subscription Settings...", nil)];
    
    [NETWORK_MANAGER setSubscribedAnalyticsEmail:email
                                       subscribe:YES
                                  withCompletion:^(BOOL status, NSString *reason) {
                                      
                                      [self performSelector:@selector(dismissHUD)
                                                 withObject:nil afterDelay:1.5f];
                                      
                                      if (status)
                                          [self saveAnalyticsEmail:email];
                                      else
                                          [self showAnalyticsServiceErrorAlertWithMessage:reason];
                                      
                                      inProgress = NO;
                                      
                                      if (![self isAnalyticsEmailNotifyActivated])
                                          [self startAnalyticsEmailNotifications];
                                  }];
}

- (void)unsubscribeEmail
{
    if (inProgress)
        return;
    
    inProgress = YES;
    
    NSLog(@"Start unsubscribe request...");
    
    [SVProgressHUD setFont:[UIFont fontWithName:@"TrebuchetMS" size:14.f]];
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait. Updating Analytics Subscription Settings...", nil)];
    
    [NETWORK_MANAGER setSubscribedAnalyticsEmail:[self getAnalyticsEmail]
                                       subscribe:NO
                                  withCompletion:^(BOOL status, NSString *reason) {
                                      
                                      [self performSelector:@selector(dismissHUD)
                                                 withObject:nil afterDelay:1.5f];
                                      
                                      if (status)
                                          [self resetAnalyticsEmail];
                                      else
                                          [self showAnalyticsServiceErrorAlertWithMessage:reason];
                                      
                                      inProgress = NO;
                                      
                                      if (![self isAnalyticsEmailNotifyActivated])
                                          [self startAnalyticsEmailNotifications];
                                  }];
}

- (void)dismissHUD
{
    [SVProgressHUD dismiss];
}

- (void)showAnalyticsServiceErrorAlertWithMessage:(NSString *)msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kPermissionsAlert)
    {
        if (buttonIndex)
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

#pragma mark - Camera Permissions Methods

- (void)getCameraPermissions
{
    if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)])
    {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            
            if (granted)
            {
                NSLog(@"Can use camera");
            }
        }];
    }
}

- (BOOL)isCameraCanBeAvaliable
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if (authStatus == AVAuthorizationStatusAuthorized
        || authStatus == AVAuthorizationStatusNotDetermined)
    {
        return YES;
    }
    
    if (authStatus == AVAuthorizationStatusDenied)
    {
        return NO;
    }
    
    return NO;
}

- (void)showPermissionsAlertWithMessage:(NSString *)msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil)
                                                    message:msg
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                          otherButtonTitles:NSLocalizedString(@"Settings", nil), nil];
    alert.tag = kPermissionsAlert;
    [alert show];
}

#pragma mark - ACEP Measurement Web Service Settings Data

- (NSString *)getMeasureWebServiceToken
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"usr_token"];
    
    if ([token stringByReplacingOccurrencesOfString:@" " withString:@""].length)
        return token;
    
    return nil;
}

- (NSString *)getMeasureWedServiceStoreID
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *store_id = [[NSUserDefaults standardUserDefaults] stringForKey:@"usr_store_id"];
    
    if (store_id)
        return [store_id stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    return nil;
}

- (BOOL)isQueueEntryModeOnDevice
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"queue_entry_mode"];
}

- (BOOL)isNeedToBeExportedCustomerPic
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"with_customer_pic_mode"];
}

#pragma mark - Analytics

- (NSString *)getAnalyticsTrackerID
{
    return @"UA-62151334-1";
}

- (void)generateAnalyticDeviceID
{
    NSString *device_id = [self getAnalyticDeviceID];
    
    if (!device_id)
    {
        device_id = [[[[UIDevice currentDevice] identifierForVendor] UUIDString] MD5];
        
        [[NSUserDefaults standardUserDefaults] setObject:device_id forKey:@"device_id"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    NSLog(@"Analytics device ID is: %@", device_id);
}

- (NSString *)getAnalyticDeviceID
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *device_id = [[NSUserDefaults standardUserDefaults] stringForKey:@"device_id"];
    
    if ([device_id stringByReplacingOccurrencesOfString:@" " withString:@""].length)
    {
        return [NSString stringWithFormat:@"%@.%@", device_id, [self getLicenseForAnalyticsStoreID]];
    }
    
    return nil;
}

- (NSString *)getAnalyticsECPID
{
    NSString *license_id = [SMStone getCurrentStoneId];
    
    if ([license_id stringByReplacingOccurrencesOfString:@" " withString:@""].length)
        return license_id;
    
    return [SMNetworkManager getDeviceIdentifier];
}

- (NSString *)getLicenseForAnalyticsStoreID
{
    NSString *license_id = [SMStone getCurrentStoneId];
    
    if ([license_id stringByReplacingOccurrencesOfString:@" " withString:@""].length)
        return license_id;
    
    return @"Undefined";
}

- (NSString *)getAnalyticsEmail
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"analytics_email"];
}

- (void)sendAnalyticsEnteredModuleWithName:(NSString *)name
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker set:[GAIFields customDimensionForIndex:1]
           value:[self getAnalyticsECPID]];
    
    [tracker set:[GAIFields customDimensionForIndex:2]
           value:[self getLicenseForAnalyticsStoreID]];
    
    [tracker set:[GAIFields customDimensionForIndex:3]
           value:[self getAnalyticDeviceID]];
    
    [tracker set:kGAIScreenName value:name];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - Measurements Analytics

- (BOOL)isMeasurementsAnalyticsEnabled
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"pd_analytics"];
}

#pragma mark - Jump to Single Vision or MLC

- (void)setMainMenuPointer:(SMMainMenuViewController *)ptr
{
    if (ptr)
        mainMenuCtrl = ptr;
}

- (void)openMLCController:(BOOL)mlc
{
    if (!mainMenuCtrl)
        return;
    
    mainMenuCtrl.showMenuButtons = NO;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *vc;
    
    if (mlc)
        vc = [storyboard instantiateViewControllerWithIdentifier:@"MLCController"];
    else
        vc = [storyboard instantiateViewControllerWithIdentifier:@"SingleVisionController"];
    
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    mustBackToPDModule = YES;
    
    [mainMenuCtrl presentViewController:vc animated:NO completion:nil];
}

- (void)backToPDModule
{
    if (mustBackToPDModule)
    {
        mustBackToPDModule = NO;
        
        [mainMenuCtrl enterPDModule];
        
        [self performSelector:@selector(setMenuButtonsState)
                   withObject:nil afterDelay:1.5f];
    }
}

- (void)setMenuButtonsState
{
    mainMenuCtrl.showMenuButtons = YES;
}

#pragma mark - Provision Expiration Date Checking

- (BOOL)isSoonExpirationDate
{
    return [provisionChecker isSoonExpirationDate];
}

- (void)setupIsNeedCheckNewProfileExpirationDate;
{
    [provisionChecker setupIsNeedCheckNewProfileExpirationDate];
    [provisionChecker setupIsNeedDelayExpirationDateAlert];
}

- (void)setupIsNeedShowAgainExpirationDateAlert:(BOOL)isNeedShowAgain
{
    [provisionChecker setupIsNeedShowAgainExpirationDateAlert:isNeedShowAgain];
}

- (BOOL)dontShowAgainExpirationDateAlert
{
    return [provisionChecker dontShowAgainExpirationDateAlert];
}

#pragma mark - Url Scheme

- (void)parseUrlScheme:(NSURL *)url
{
    NSString *host = [url.host stringByRemovingPercentEncoding];
    
    if ([host stringIsNotNil:host])
    {
        NSArray *components = [host componentsSeparatedByString:@"&"];
        
        NSMutableDictionary *parameters = [NSMutableDictionary new];
        
        if (components.count)
        {
            for (NSString *component in components)
            {
                NSArray *subcomponents = [component componentsSeparatedByString:@"="];
                
                if (subcomponents.count > 1)
                {
                    [parameters setObject:[subcomponents[1] stringByRemovingPercentEncoding]
                                   forKey:[subcomponents[0] stringByRemovingPercentEncoding]];
                }
            }
        }
        
        parseUrlDict = parameters;
        
        [self checkUrlDict];
    }
}

- (void)checkUrlDict
{
    if (parseUrlDict)
    {
        if ([parseUrlDict.allKeys containsObject:@"action"])
        {
            URLAction action = [self getActionType:parseUrlDict[@"action"]];
            
            if (action == URLAction_ImportSessions)
                [self performSelector:@selector(startSharePDSessions)
                           withObject:nil afterDelay:0.5];
            
            if (action == URLAction_ShareLogin)
                [self performSelector:@selector(showShareLoginHUD)
                           withObject:nil afterDelay:0.5];
        }
    }
}

- (URLAction)getActionType:(NSString *)action
{
    if ([action isEqualToString:@"importSessions"])
        return URLAction_ImportSessions;
    
    if ([action isEqualToString:@"shareLogin"])
        return URLAction_ShareLogin;
    
    return URLAction_Undefined;
}

- (void)startSharePDSessions
{
    if (![SESSION_MANAGER isHaveSessionsForShare])
    {
        [self showNoSessionsForShare];
        return;
    }
    
    UIViewController *rootController = [UIApplication.sharedApplication.windows[0] visibleViewController];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PDShareSessionsViewController"];
    [(PDShareSessionsViewController *)vc setAppName:parseUrlDict[@"appName"]];
    
    [rootController addChildViewController:vc];
    [rootController.view addSubview:vc.view];
}

- (void)showNoSessionsForShare
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:NSLocalizedString(@"No session to import", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];

    [alert addAction:okButton];

    [[UIApplication.sharedApplication.windows[0] visibleViewController] presentViewController:alert
                                                                                     animated:YES
                                                                                   completion:nil];
}

- (void)showShareLoginHUD
{
    [SVProgressHUD setFont:[UIFont fontWithName:@"TrebuchetMS" size:14.f]];
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait. Search Login...", nil)];
    
    [self performSelector:@selector(hideShareLoginHUD)
               withObject:nil afterDelay:2.5];
}

- (void)hideShareLoginHUD
{
    [SVProgressHUD dismiss];
    
    [self startShareLogin:[NETWORK_MANAGER isAuthenticated]];
}

- (void)startShareLogin:(BOOL)isAuthenticated
{
    NSString *fullUrl;
    
    if (isAuthenticated)
    {
        NSString *bundleId = [SMNetworkManager getBundleId];
        NSString *authId = [NSString stringWithFormat:@"%i", [SMStone getCurrentStone].authID];
        
        fullUrl = [NSString stringWithFormat:@"%@action=shareLogin&bundleId=%@&authId=%@",
                                SMMOBILE3_URL_SCHEME, bundleId, authId];
    }
    else
    {
        fullUrl = [NSString stringWithFormat:@"%@action=shareLogin&error=notAuthenticated", SMMOBILE3_URL_SCHEME];
    }
    
    if (@available(iOS 10.0, *))
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fullUrl]
                                           options:@{}
                                 completionHandler:nil];
    }
    else
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fullUrl]];
}

@end
