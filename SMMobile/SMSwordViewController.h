//
//  SMSwordViewController.h
//
//  Created by Oleg Bogatenko on 10/7/14.
//

#import <UIKit/UIKit.h>
#import "SMNetworkManager.h"

@protocol SMSwordViewControllerDelegate <NSObject>

@optional
- (void)cancelSword;
- (void)sendMailPressed;
- (void)succesfullyLoggedIn;
@end

@interface SMSwordViewController : UIViewController

@property (nonatomic, assign) SMModule enterModule;

@property (nonatomic, assign) id <SMSwordViewControllerDelegate> delegate;

@end
