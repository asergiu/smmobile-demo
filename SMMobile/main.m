//
//  main.m
//  SMMobile
//
//  Created by Oleg Bogatenko on 8/5/14.
//

#import <UIKit/UIKit.h>

#import "SMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SMAppDelegate class]));
    }
}
