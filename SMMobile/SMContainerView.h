//
//  SMContainerView.h
//  SMMobile
//
//  Created by Oleg Bogatenko on 9/3/14.
//

#import <UIKit/UIKit.h>

@interface SMContainerView : UIView

@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *buttons;

@end
