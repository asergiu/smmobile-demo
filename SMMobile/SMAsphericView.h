//
//  SMCoatingView.h
//  Optovision
//
//  Created by Oleg Bogatenko on 30.05.13.
//
//

#import "GLKView_SmartMirror.h"

@interface SMAsphericView : GLKView_SmartMirror

-( void )setBackground : ( int )index;
-( void )setShape : ( int )shape;
-( void )setPower : ( float )power;
-( void )setCyl : ( float )cyl;
-( void )setAxis : ( float )axis;

-( void )setLeftDesign : ( int )leftDesign;
-( void )setRightDesign : ( int )rightDesign;

-( void )setLeftIndex : ( float )index;
-( void )setRightIndex : ( float )index;

-( void )renderViews;

-( void )loadThings;
-( void )clearThings;

@property ( nonatomic, weak ) IBOutlet UIView * sphericView;
@property ( nonatomic, weak ) IBOutlet UIView * asphericView;

@end
